<?php
//$__functions['install'][]	='_site_install_modul' ;


function init_local_setting()
{
    // настройки, определяющее поведение и вид страниц сайта

    $_SESSION['check_code_img'][0] = array('digit_count' => 4,'width' => 90,'height' => 40,'back' => 'FFFFFF','color'=>'000000','back_space' => '/images/back_space.png','font_name' => '/images/check_font.ttf','font_size' => 16,'x' => 0,'y' => 30,'step_x' => 15,'dev_x' => 0,'dev_y' => 3,'dev_angle' => 0);


    $_SESSION['img_pattern']['Товары'][70]['resize']=array('width'=>80,'resampled'=>1,'quality'=>100);
    $_SESSION['img_pattern']['Товары'][73]['resize']=array('width'=>73,'height'=>73,'fields'=>0,'resampled'=>1,'quality'=>100);
    $_SESSION['img_pattern']['Товары'][125]['resize']=array('width'=>125,'height'=>125,'fields'=>1,'resampled'=>1,'quality'=>100,'back_color'=>'#FFFFFF');  //  показ товара в блоке "аналогичеые товары"
    $_SESSION['img_pattern']['Товары']['small']['no_photo']='/images/nophoto_100.png' ;
    $_SESSION['img_pattern']['Товары'][125]['no_photo']='/images/nophoto_125.jpg' ;
    $_SESSION['img_pattern']['Товары'][150]['resize']=array('width'=>150,'height'=>150,'fields'=>1,'resampled'=>1,'quality'=>100,'back_color'=>'#FFFFFF');  //  показ товара в блоке "аналогичеые товары"
    $_SESSION['img_pattern']['Товары'][150]['no_photo']='/images/nophoto_150x150.png' ;

    $_SESSION['img_pattern']['Товары'][175]['resize']=array('width'=>175,'height'=>181,'fields'=>0,'resampled'=>1,'quality'=>100,'back_color'=>'#FFFFFF');  //  показ товара в блоке "аналогичеые товары"
    $_SESSION['img_pattern']['Товары'][175]['no_photo']='/images/nophoto_175x181.png' ;

    $_SESSION['img_pattern']['Товары']['good_of_day']['resize']=array('width'=>218,'height'=>233,'fields'=>0,'resampled'=>1,'quality'=>100,'back_color'=>'dce1e3');
    $_SESSION['img_pattern']['Товары']['good_of_day']['no_photo']='/images/nophoto_225.jpg' ;

  $_SESSION['img_pattern']['Товары'][225]['resize']=array('width'=>225,'height'=>170,'fields'=>1,'resampled'=>1,'quality'=>100,'back_color'=>'dce1e3');
    $_SESSION['img_pattern']['Товары'][225]['no_photo']='/images/nophoto_225.jpg' ;
    $_SESSION['img_pattern']['Товары'][250]['resize']=array('width'=>250,'height'=>167,'fields'=>1,'resampled'=>1,'quality'=>100,'back_color'=>'dce1e3');
    $_SESSION['img_pattern']['Товары'][250]['no_photo']='/images/nophoto_250.jpg' ;
    $_SESSION['img_pattern']['Товары']['250w']['resize']=array('width'=>250,'resampled'=>1,'quality'=>100);
    $_SESSION['img_pattern']['Товары']['250w']['no_photo']='/images/nophoto_225.jpg' ;
    $_SESSION['img_pattern']['Товары'][450]['resize']=array('width'=>450,'height'=>450,'resampled'=>1,'quality'=>80);
    $_SESSION['img_pattern']['Товары'][450]['no_photo']='/images/nophoto_500.jpg' ;
    $_SESSION['img_pattern']['Товары'][500]['resize']=array('width'=>500,'resampled'=>1,'quality'=>100);
    $_SESSION['img_pattern']['Товары'][500]['no_photo']='/images/nophoto_500.jpg' ;

    $_SESSION['img_pattern']['Товары сортамент'][450]['resize']=array('width'=>450,'height'=>450,'fields'=>0,'resampled'=>1,'quality'=>80);

    $_SESSION['img_pattern']['Товары сортамент']['small']['no_photo']='/images/nophoto_125.jpg' ;


    $_SESSION['init_options']['goods']['root_dir']	='catalog' ;
    $_SESSION['init_options']['goods']['no_root_dir']=1 ;
    $_SESSION['init_options']['goods']['path_to_level']=1 ;
    $_SESSION['init_options']['goods']['tree_fields']='*' ;
    $_SESSION['init_options']['goods']['tree']['clss']='1,10' ;
    //$_SESSION['init_options']['goods']['usl_show_items']='enabled=1 and _enabled=1 and clss=200 and (is_sortament=0 or is_sortament is null) and (stock>0 or stock2=1)' ;
//    $_SESSION['init_options']['goods']['usl_show_items']='enabled=1 and _enabled=1 and clss=200 and (is_sortament=0 or is_sortament is null)' ;

    $_SESSION['init_options']['default']['pages']['options']=array('pages_num_format'=>"%'1d",'page_max_number_count'=>100,'page_number_rasdel'=>'&nbsp;&nbsp;|&nbsp;&nbsp;','page_image_prev'=>'/images/arr_left_dark.gif','page_image_next'=>'/images/arr_right_dark.gif') ;
    $_SESSION['init_options']['default']['pages']['options']['page_mode']=0 ;
    $_SESSION['init_options']['goods']['pages']['size']=array('16'=>'16','20'=>'20','all'=>'Показать все товары') ;
    $_SESSION['init_options']['goods']['pages']['sort_type']=array() ;
    $_SESSION['init_options']['goods']['pages']['sort_type'][1]=array('name'=>'По новизне',	'order_by'=>'indx asc') ;
    $_SESSION['init_options']['goods']['pages']['sort_type'][2]=array('name'=>'По цене',		'order_by'=>'price') ;


    $_SESSION['init_options']['goods']['pages']['sort_mode']=array() ;
    //$_SESSION['init_options']['goods']['pages']['sort_mode'][1]='asc' ;
    //$_SESSION['init_options']['goods']['pages']['sort_mode'][2]='desc' ;

    //$_SESSION['init_options']['art']['no_root_dir']=1 ;

    $_SESSION['VL_arr']=array() ;
    $_SESSION['VL_arr'][0]=array('name'=>'р','ue'=>'р.','var'=>'LS_kurs_rur','round'=>0,'dec'=>2,'dec_point'=>'.','title'=>'В рублях','YA'=>'RUR') ;
    //$_SESSION['VL_arr'][1]=array('name'=>'$','ue'=>' $ ','var'=>'LS_kurs_dol','round'=>0,'dec'=>2,'dec_point'=>'.','title'=>'В долларах','YA'=>'USD') ;
    //$_SESSION['VL_arr'][2]=array('name'=>'Э','ue'=>' eur. ','var'=>'LS_kurs_eur','round'=>0,'dec'=>2,'dec_point'=>'.','title'=>'В евро','YA'=>'EUR') ;
    $_SESSION['VL_kurs']=0 ; // валюта, относительно которой заданы курсы в настройках сайта
    $_SESSION['VL_def_DB']=0 ; // валюта, используемая по умолчанию в товарной таблиц (если поле val==null)
    $_SESSION['VL_main']=0 ; // основная валюта на сайте, оформление заказа и цены позиций заказах хранятся именно в этой валюте

    $_SESSION['ARR_ACCOUNT_STATUS']=array('1'=>'Клиент','2'=>'Фотограф','3'=>'Администратор') ;
    $_SESSION['ARR_roll']=array() ;
    $_SESSION['ARR_roll'][1]=array('obj_name'=>'Администратор',          '_index_page'=>'/cab/') ;


    $_SESSION['serialize_fieds_names']['status']='Должность' ;
    $_SESSION['serialize_fieds_names']['site']='Сайт' ;
    $_SESSION['serialize_fieds_names']['country']='Страна' ;
    $_SESSION['serialize_fieds_names']['region']='Регион' ;
    $_SESSION['serialize_fieds_names']['zip']='Индекс' ;
}

function init_admin_setting()
{


  $_SESSION['menu_admin']['Контент']['file_manager']=array('name'=>'Файловый проводник','href'=>'file_explorer.php','target'=>'popup','icon'=>'menu/files.jpg','id'=>'file_explorer','onclick'=>'onclick="window.open(\'file_explorer.php\',\'popup\',\'width=1200,height=647\',\'menubar=no\',\'location=no\'); return false;"') ;
//  $_SESSION['menu_admin']['Контент']['file_manager']=array('name'=>'Файловый проводник','href'=>'../AddOns/elFinder-2.1.22/elfinder.php','target'=>'_blank','icon'=>'menu/files.jpg','id'=>'file_explorer') ;
//  $_SESSION['menu_admin']['Контент'][]=array('name'=>'Навигация сайта','href'=>'editor_site_menu.php','icon'=>'menu/site_menu.jpg') ;
    //$_SESSION['menu_admin_site']['Модули'][]=array('name'=>'Каталогизатор','href'=>'editor_catalog.php') ;

    // ext_panel_register(array('frame'=>array('table_code'=>'goods','is_root'=>1),'menu_item'=>array('name'=>'Импорт start1.ru','script'=>'start1.php','cmd'=>'parsing_site/import_start1'))) ;          
    //$_SESSION['menu_admin_site']['Модули'][]=array('name'=>'Товары новая','href'=>'editor_goods2.php') ;

//    _DOT('obj_site_goods')->rules[]=array('to_pkey'=>1,'only_clss'=>62) ;  // в таблицу товаров в корень добавлять только бренды
//    _DOT('obj_site_list')->rules[]=array('to_pkey'=>95,'only_clss'=>121) ;  // в таблицe "списки" в объект "Типы товаров" можно добавлять только "типы товаров"


	//===============================================================================================================================================
	// описание вывода классов сайта
	//===============================================================================================================================================

//    unset($_SESSION['descr_clss'][10]['list']['field']['href']) ;
//    unset($_SESSION['descr_clss'][10]['details']['field']['href']) ;
    $_SESSION['descr_clss'][1]['fields']['url_dir']             =array('type'=>'varchar(255)') ;
    $_SESSION['descr_clss'][1]['list']['field']['url_dir']      ='Директория' ;
    $_SESSION['descr_clss'][1]['details']['field']['url_dir']   ='Директория' ;
    $_SESSION['descr_clss'][1]['menu'][]                        =array("name" => "Добавить",				"type" => 'structure') ;
    $_SESSION['descr_clss'][1]['child_to']                      =array(0,1) ;
    $_SESSION['descr_clss'][1]['parent_to']                     =array(3,5,10,11,22) ;

//
//    // 13 - директория в редакторе страниц
//    $_SESSION['descr_clss'][13]                                 =array() ;
//    $_SESSION['descr_clss'][13]['name']                         ='Директрия' ;
//   	$_SESSION['descr_clss'][13]['parent']                       =1 ;
//   	$_SESSION['descr_clss'][13]['fields']                       =array('dir'=>'varchar(255)') ;
//    $_SESSION['descr_clss'][13]['child_to']                     =array(1,13) ;
//    $_SESSION['descr_clss'][13]['parent_to']                    =array(13,26,260) ;
//    $_SESSION['descr_clss'][13]['menu'][]                       =array("name" => "Свойства",				"type" => 'prop') ;
//    $_SESSION['descr_clss'][13]['menu'][]                       =array("name" => "Добавить",				"type" => 'structure') ;
//    $_SESSION['descr_clss'][13]['menu'][]                       =array("name" => "Операции", "action" => 3,'icon'=>'obj_operat.gif') ;
////    $_SESSION['descr_clss'][13]['menu'][]                       =array("name" => "Информация","action" => 4,'icon'=>'obj_operat.gif') ;
//
//   	$_SESSION['descr_clss'][13]['list']['field']['pkey']		    ='Код' ;
//   	$_SESSION['descr_clss'][13]['list']['field']['enabled']		  ='Сост.' ;
//   	$_SESSION['descr_clss'][13]['list']['field']['obj_name']	  =array('title'=>'Наименование','edit_element'=>'input','class'=>'big','td_class'=>'left') ;
//    $_SESSION['descr_clss'][13]['list']['field']['dir']         =array('title'=>'Директрия','td_class'=>'left') ;
//
//
//    // страница сайта
//    $_SESSION['descr_clss'][26]                                 =array() ;
//    $_SESSION['descr_clss'][26]['name']                         ='Страница сайта' ;
//    $_SESSION['descr_clss'][26]['parent']                       =0 ;
//    $_SESSION['descr_clss'][26]['fields']                       =array('value'=>'longtext','href'=>'varchar(255)','obj_reffer'=>'any_object','type'=>'int(11)','url_name'=>'varchar(255)') ;
//    $_SESSION['descr_clss'][26]['child_to']                     =array(13) ;
//    $_SESSION['descr_clss'][26]['parent_to']                    =array(70,71,3) ;
//    $_SESSION['descr_clss'][26]['SEO_tab']=1 ;
//
//    $_SESSION['descr_clss'][26]['icons']                        =_PATH_TO_BASED_CLSS_IMG.'/7.png' ;
//
//    $_SESSION['descr_clss'][26]['list']['field']['pkey']				='id' ;
//    $_SESSION['descr_clss'][26]['list']['field']['enabled']			='Сост.' ;
//    $_SESSION['descr_clss'][26]['list']['field']['obj_name']		=array('title'=>'Наименование','td_class'=>'left','edit_element'=>'memo','class'=>'small') ;
//    $_SESSION['descr_clss'][26]['list']['field']['href']				=array('title'=>'Имя страницы','td_class'=>'left','edit_element'=>'memo','class'=>'small') ;
//    $_SESSION['descr_clss'][26]['list']['field'][]						  =array('title'=>'Ссылка на сайт','on_view'=>'clss_26_url_view','td_class'=>'left') ;
//
//    $_SESSION['descr_clss'][26]['details']['field']             =$_SESSION['descr_clss'][26]['list']['field'] ;
//    $_SESSION['descr_clss'][26]['details']['field']['value']    =array('title'=>'Текст','class'=>'big','use_HTML_editor'=>1) ;

    
//    // 260 - составная странице в редакторе страниц
//    $_SESSION['descr_clss'][260]                                =array() ;
//    $_SESSION['descr_clss'][260]['name']                        ='Составная страница' ;
//    $_SESSION['descr_clss'][260]['parent']                      =1 ;
//    $_SESSION['descr_clss'][260]['fields']                      =array('dir'=>'varchar(255)') ;
//    $_SESSION['descr_clss'][260]['child_to']                    =array(13) ;
//    $_SESSION['descr_clss'][260]['parent_to']                   =array(3,7,131) ;
//    $_SESSION['descr_clss'][260]['menu'][]                      =array("name" => "Добавить",				"type" => 'structure') ;
//    $_SESSION['descr_clss'][260]['SEO_tab']                     =1 ;
//    $_SESSION['descr_clss'][260]['icons']                       =_PATH_TO_BASED_CLSS_IMG.'/7plus.png' ;
//
//	  $_SESSION['descr_clss'][260]['list']['field']['pkey']		    ='Код' ;
//	  $_SESSION['descr_clss'][260]['list']['field']['enabled']	  ='Сост.' ;
//	  $_SESSION['descr_clss'][260]['list']['field']['obj_name']	  =array('title'=>'Наименование','edit_element'=>'input','class'=>'big','td_class'=>'left') ;
//    $_SESSION['descr_clss'][260]['list']['field']['href']				=array('title'=>'Имя страницы','td_class'=>'left','edit_element'=>'memo','class'=>'small') ;
//    $_SESSION['descr_clss'][260]['list']['field'][]						  =array('title'=>'Ссылка на сайт','on_view'=>'clss_26_url_view','td_class'=>'left') ;
//    $_SESSION['descr_clss'][260]['list']['field']['code']				='ШАБЛОН'  ;
//
//       // 131 - раздел в составной странице в редакторе страниц
//    $_SESSION['descr_clss'][131]                                =array() ;
//    $_SESSION['descr_clss'][131]['name']                        ='Раздел страницы' ;
//    $_SESSION['descr_clss'][131]['parent']                      =1 ;
//    $_SESSION['descr_clss'][131]['fields']                      =array('dir'=>'varchar(255)') ;
//    $_SESSION['descr_clss'][131]['child_to']                    =array(260,131) ;
//    $_SESSION['descr_clss'][131]['parent_to']                   =array(3,7,131) ;
//    $_SESSION['descr_clss'][131]['menu'][]                      =array("name" => "Добавить",				"type" => 'structure') ;
//
//	  $_SESSION['descr_clss'][131]['list']['field']['pkey']		    ='Код' ;
//	  $_SESSION['descr_clss'][131]['list']['field']['enabled']	  ='Сост.' ;
//	  $_SESSION['descr_clss'][131]['list']['field']['obj_name']	  =array('title'=>'Наименование','edit_element'=>'input','class'=>'big','td_class'=>'left') ;
//    $_SESSION['descr_clss'][131]['list']['field']['code']				='ШАБЛОН'  ;
//
//     // 7 - информация в составной странице в редакторе страниц
//    $_SESSION['descr_clss'][7]                                  =array() ;
//	  $_SESSION['descr_clss'][7]['name']                          ='Информация' ;
//	  $_SESSION['descr_clss'][7]['parent']                        =0 ;
//	  $_SESSION['descr_clss'][7]['fields']                        =array('value'=>'text','code'=>'varchar(32)','is_HTML'=>'int(1)') ;
//	  $_SESSION['descr_clss'][7]['parent_to']                     =array(3) ;
//    $_SESSION['descr_clss'][7]['icons']                         =_PATH_TO_BASED_CLSS_IMG.'/21.png' ;
//
//	  $_SESSION['descr_clss'][7]['list']['field']['enabled']  ='Сост.' ;
//	  $_SESSION['descr_clss'][7]['list']['field']['pkey']			='ID' ;
//	  $_SESSION['descr_clss'][7]['list']['field']['code']			='Код блока<br>в шаблоне' ;
//	  $_SESSION['descr_clss'][7]['list']['field']['obj_name']	='Наименование' ;
//	  $_SESSION['descr_clss'][7]['list']['field']['is_HTML']	=array('title'=>'HTML')  ;
//    $_SESSION['descr_clss'][7]['list']['field']['value']		=array('title'=>'Содержание','td_class'=>'justify','class'=>'big','use_HTML_editor'=>1) ;
//
//	  $_SESSION['descr_clss'][7]['details']                   =$_SESSION['descr_clss'][7]['list'] ;
//	  $_SESSION['descr_clss'][7]['details']['field']['value']	=array('title'=>'Текст','td_class'=>'justify','class'=>'big','no_generate_element'=>1) ;
//    $_SESSION['descr_clss'][7]['details']['field'][]				=array('title'=>'Код php','edit_element'=>'clss_7_code','td_class'=>'justify') ;




      // 10 - раздел с описанием
    $_SESSION['descr_clss'][10]=array() ;

    $_SESSION['descr_clss'][10]['parent_to']=array(3,5,200) ;
    $_SESSION['descr_clss'][10]['child_to']=array(1,10) ;

    $_SESSION['descr_clss'][10]['name']='Раздел каталога' ;
    $_SESSION['descr_clss'][10]['parent']=1 ;

    $_SESSION['descr_clss'][10]['fields']=array('intro'=>'text','manual'=>'longtext','href'=>'varchar(255)') ;
    $_SESSION['descr_clss'][10]['fields']['subdomain']='varchar(255)' ;
    $_SESSION['descr_clss'][10]['fields']['to_index']               =array('type'=>'int(1)') ;
    $_SESSION['descr_clss'][10]['fields']['no_index_yandex']        =array('type'=>'int(1)') ;
    $_SESSION['descr_clss'][10]['fields']['no_index_google']        =array('type'=>'int(1)') ;
    $_SESSION['descr_clss'][10]['fields']['url_dir']                =array('type'=>'varchar(255)') ;
    $_SESSION['descr_clss'][10]['fields']['view_mode']              =array('type'=>'int(1)') ;

    $_SESSION['descr_clss'][10]['fields']['def_obj_name']              =array('type'=>'varchar(255)') ;
    $_SESSION['descr_clss'][10]['fields']['def_price']              =array('type'=>'float(10,2)') ;
    $_SESSION['descr_clss'][10]['fields']['def_sezon']              =array('type'=>'text') ;
    $_SESSION['descr_clss'][10]['fields']['def_spec_cnt']              =array('type'=>'int(11)') ;
    $_SESSION['descr_clss'][10]['fields']['def_stock2']              =array('type'=>'int(1)') ;
    $_SESSION['descr_clss'][10]['fields']['name_of_destr']              =array('type'=>'varchar(255)') ;
    $_SESSION['descr_clss'][10]['fields']['baner_group']              =array('type'=>'int(11)') ;

    $_SESSION['descr_clss'][10]['SEO_tab']=1 ;
    $_SESSION['descr_clss'][10]['menu'][]=array("name" => "Добавить", "type" => 'structure') ;
    $_SESSION['descr_clss'][10]['menu'][]=array("name" => "Свойства", "type" => 'prop') ;
    $_SESSION['descr_clss'][10]['menu'][]=array("name" => "Операции", "cmd" => 'panel_goods_section_operation', 'script'=>'mod/goods/i_goods_section_operation.php','icon'=>'obj_operat.gif') ;
//    $_SESSION['descr_clss'][10]['menu'][]=array("name" => "Свойства товара по умолчанию", "cmd" => '', '','icon'=>'obj_operat.gif') ;
    //$_SESSION['descr_clss'][10]['menu'][]=array("name" => "Поиск",  "cmd" => 'child_search','icon'=>'find_icon.gif') ;

    $_SESSION['descr_clss'][10]['list']['field']['pkey']		        ='Код' ;
    $_SESSION['descr_clss'][10]['list']['field']['enabled']		      ='Сост.' ;
    $_SESSION['descr_clss'][10]['list']['field']['indx']		        ='Позиция'  ;
    $_SESSION['descr_clss'][10]['list']['field']['obj_name']	      =array('title'=>'Наименование','edit_element'=>'input','class'=>'big','td_class'=>'left') ;
    $_SESSION['descr_clss'][10]['list']['field']['url_dir']         ='Директория' ;
    $_SESSION['descr_clss'][10]['list']['field']['to_index']        ='На главную' ;
    $_SESSION['descr_clss'][10]['list']['field']['no_index_yandex'] ='Ya' ;
    $_SESSION['descr_clss'][10]['list']['field']['no_index_google'] ='Gg' ;
    $_SESSION['descr_clss'][10]['list']['field']['baner_group'] ='группа баннеров' ;


	  $_SESSION['descr_clss'][10]['details']=$_SESSION['descr_clss'][10]['list'] ;
	  $_SESSION['descr_clss'][10]['details']['field']['obj_name']	    =array('title'=>'Наименование','edit_element'=>'input','class'=>'big','td_class'=>'left') ;
	  $_SESSION['descr_clss'][10]['details']['field']['name_of_destr']	    =array('title'=>'Наименование описания','edit_element'=>'input','class'=>'small','td_class'=>'left','default'=>'Описание') ;
    $_SESSION['descr_clss'][10]['details']['field']['baner_group'] ='группа баннеров' ;
    $_SESSION['descr_clss'][10]['details']['field'][]		 	          =array('title'=>'Путь в каталоге',					'edit_element'=>'clss_patch') ;
    $_SESSION['descr_clss'][10]['details']['field']['intro']	      =array('title'=>'Краткое описание','class'=>'small','use_HTML_editor'=>1) ;
    $_SESSION['descr_clss'][10]['details']['field']['manual']	      =array('title'=>'Описание',		'class'=>'big','use_HTML_editor'=>1) ;



    $_SESSION['descr_clss'][10]['default_for_child_200']['field']['def_obj_name']		    =array('title'=>'Наименование',		'class'=>'small') ;
    $_SESSION['descr_clss'][10]['default_for_child_200']['field']['def_price']           =array('title'=>'Цена - стандарт') ;
    $_SESSION['descr_clss'][10]['default_for_child_200']['field']['def_sezon']					  =array('title'=>'Сезонность') ;
    $_SESSION['descr_clss'][10]['default_for_child_200']['field']['def_spec_cnt']        =array('title'=>'Количество в упаковке') ;           // характеристика - количество в упаковке
    $_SESSION['descr_clss'][10]['default_for_child_200']['field']['def_stock2']			    ='Наличие на складе' ;

//              пункты для меню, возможность выбора источника
  $_SESSION['descr_clss'][97]['fields']['pkey_istochik']='varchar(255)' ;
  $_SESSION['descr_clss'][97]['list']['field']['pkey_istochik']		='источник списка' ;



    $_SESSION['descr_clss'][200]['parent_to']                           =array(11,3,4,5,65) ;
    $_SESSION['descr_clss'][200]['child_to']                            =array(1,10,201,210) ;

   // товар
    $_SESSION['descr_clss'][200]                                        =array() ;

    $_SESSION['descr_clss'][200]['name']                                ='Товар' ;
    $_SESSION['descr_clss'][200]['parent']                              =2 ;
    $_SESSION['descr_clss'][200]['fields']['val']                       =array('type'=>'indx_select','array'=>'VL_arr','indx_field'=>'YA') ; // валюта товара
    $_SESSION['descr_clss'][200]['fields']['sales_type']                =array('type'=>'indx_select','array'=>'list_type_sales') ; // тип скидки
    $_SESSION['descr_clss'][200]['fields']['icon_obj_name']		          =array('type'=>'text') ;   // наименование на иконках
    $_SESSION['descr_clss'][200]['fields']['icon_obj_comment']	        =array('type'=>'text') ;   // наименование на иконках
    $_SESSION['descr_clss'][200]['fields']['manual']                    =array('type'=>'longtext') ;
    $_SESSION['descr_clss'][200]['fields']['intro']                     =array('type'=>'text') ;
    $_SESSION['descr_clss'][200]['fields']['art']                     =array('type'=>'varchar(255)') ;
    $_SESSION['descr_clss'][200]['fields']['brand']                     =array('type'=>'int(11)') ;
    //$_SESSION['descr_clss'][200]['fields']['size']                     =array('type'=>'varchar(12)') ;
    //$_SESSION['descr_clss'][200]['fields']['cnt_up']                   =array('type'=>'int(11)','default'=>1) ;
    //$_SESSION['descr_clss'][200]['fields']['ed_izm']                   =array('type'=>'varchar(12)') ;
    $_SESSION['descr_clss'][200]['fields']['no_index_yandex']           =array('type'=>'int(1)','default'=>1) ;
    $_SESSION['descr_clss'][200]['fields']['no_index_google']           =array('type'=>'int(1)','default'=>1) ;
    $_SESSION['descr_clss'][200]['fields']['goods_of_day']              =array('type'=>'int(1)') ;
    $_SESSION['descr_clss'][200]['fields']['new']                       =array('type'=>'int(11)') ;
    $_SESSION['descr_clss'][200]['fields']['top']                       =array('type'=>'int(11)') ;
//    $_SESSION['descr_clss'][200]['fields']['is_sortament']              =array('type'=>'int(1)') ;
    $_SESSION['descr_clss'][200]['fields']['stock2']                    =array('type'=>'int(1)','default'=>1) ;       // наличие на складе
//    $_SESSION['descr_clss'][200]['fields']['count_of_day']              =array('type'=>'int(11)');          // товар дня - количество
//    $_SESSION['descr_clss'][200]['fields']['goods_of_day']              =array('type'=>'int(1)');           // товар дня - выбор
//    $_SESSION['descr_clss'][200]['fields']['day']                       =array('type' => 'timedata','datetimepicker' => 1,'data_format' => 'd.m.Y H:i','edit_element' => 1);      // товар дня - дата
    $_SESSION['descr_clss'][200]['fields']['cnt_sort']                  =array('type'=>'int(11)') ; // число позиций сортамента в товаре
    $_SESSION['descr_clss'][200]['fields']['spec_material']             =array('type'=>'text') ;     // характеристика - материал
    $_SESSION['descr_clss'][200]['fields']['spec_size']                 =array('type'=>'text') ;          // характеристика - размер
    $_SESSION['descr_clss'][200]['fields']['spec_color']                =array('type'=>'text') ;          // характеристика - цвет
    $_SESSION['descr_clss'][200]['fields']['spec_cnt']                  =array('type'=>'int(11)') ;           // характеристика - минимальный заказ
    $_SESSION['descr_clss'][200]['fields']['spec_min']                  =array('type'=>'int(11)') ;           // характеристика - количество в упаковке
    $_SESSION['descr_clss'][200]['fields']['spec_name_1']               =array('type'=>'varchar(255)','default'=>'Материал') ;           // характеристика название 1
    $_SESSION['descr_clss'][200]['fields']['spec_name_2']               =array('type'=>'varchar(255)','default'=>'Размер') ;           // характеристика название 2
    $_SESSION['descr_clss'][200]['fields']['spec_name_3']               =array('type'=>'varchar(255)','default'=>'Цвет') ;           // характеристика название 3
    $_SESSION['descr_clss'][200]['fields']['spec_name_4']               =array('type'=>'varchar(255)') ;           // характеристика название 4
    $_SESSION['descr_clss'][200]['fields']['spec_1']                    =array('type'=>'text') ;           // характеристика 1
    $_SESSION['descr_clss'][200]['fields']['spec_2']                    =array('type'=>'text') ;           // характеристика 2
    $_SESSION['descr_clss'][200]['fields']['spec_3']                    =array('type'=>'text') ;           // характеристика 3
    $_SESSION['descr_clss'][200]['fields']['spec_4']                    =array('type'=>'text') ;           // характеристика 4
    $_SESSION['descr_clss'][200]['fields']['sezon']                     =array('type'=>'indx_select','array'=>'IL_sezon') ; // сезонность
    $_SESSION['descr_clss'][200]['fields']['sortament_type']            =array('type'=>'indx_select','array'=>'IL_sortament_type') ; // тип сортамента
    $_SESSION['descr_clss'][200]['fields']['sortament_parametr_name']   =array('type'=>'text') ;
    $_SESSION['descr_clss'][200]['fields']['url_dir']                   =array('type'=>'varchar(255)') ;
//    $_SESSION['descr_clss'][200]['fields']['price_opt']                 =array('type'=>'float(10,2)') ;
//    $_SESSION['descr_clss'][200]['fields']['price_opt_big']             =array('type'=>'float(10,2)') ;
//    $_SESSION['descr_clss'][200]['fields']['price_opt_vip']             =array('type'=>'float(10,2)') ;
    $_SESSION['descr_clss'][200]['fields']['img_name_upload']           =array('type'=>'text') ;           // поля для ручного прописования названия фото
//    $_SESSION['descr_clss'][200]['fields']['vendor_art']           =array('type'=>'text') ;           // поля для ручного прописования названия фото
    $_SESSION['descr_clss'][200]['fields']['brand_id']           =array('type'=>'text') ;           // поля для ручного прописования названия фото
    $_SESSION['descr_clss'][200]['fields']['sezon_id']           =array('type'=>'text') ;           // поля для ручного прописования названия фото


    $_SESSION['descr_clss'][200]['list']['field']['enabled']			      ='Сост.' ;

    $_SESSION['descr_clss'][200]['list']['field']['pkey']				        ='Код' ;
    $_SESSION['descr_clss'][200]['list']['field']['indx']				        ='Позиция' ;
    $_SESSION['descr_clss'][200]['list']['field']['art']				        ='Артикул' ;
    $_SESSION['descr_clss'][200]['list']['field']['obj_name']			      =array('title'=>'Наименование',		'class'=>'small') ;     //наименование общее (карточка товара)
    $_SESSION['descr_clss'][200]['list']['field']['icon_obj_name']		  =array('title'=>'Название',		'class'=>'small') ;   // наименование на иконках
    $_SESSION['descr_clss'][200]['list']['field']['icon_obj_comment']   =array('title'=>'Коментарий',		'class'=>'small') ;   // наименование на иконках
    $_SESSION['descr_clss'][200]['list']['field']['spec_cnt']           =array('title'=>'Мин. заказ') ;           // характеристика - количество в упаковке
    $_SESSION['descr_clss'][200]['list']['field']['price']              =array('title'=>'Цена') ;
    $_SESSION['descr_clss'][200]['list']['field']['sortament_type']			=array('title'=>'Тип сортамента') ;
    $_SESSION['descr_clss'][200]['list']['field']['stock2']			        =array('title'=>'Наличие') ;
    $_SESSION['descr_clss'][200]['list']['field']['no_index_yandex'] 		='Y' ;
    $_SESSION['descr_clss'][200]['list']['field']['no_index_google'] 		='G' ;
    $_SESSION['descr_clss'][200]['list']['field']['top']				        ='Хит' ;
    $_SESSION['descr_clss'][200]['list']['field']['new']				        ='Новинка' ;
//    $_SESSION['descr_clss'][200]['list']['field']['img_name_upload']		='фото для загрузки' ;

    $_SESSION['descr_clss'][200]['details']['field']['pkey']			      ='Код' ;
    $_SESSION['descr_clss'][200]['details']['field']['enabled']			    ='Состояние' ;
    $_SESSION['descr_clss'][200]['details']['field']['art']			        ='Артикул' ;
    $_SESSION['descr_clss'][200]['details']['field']['obj_name']		    =array('title'=>'Наименование',		'class'=>'small') ;
//    $_SESSION['descr_clss'][200]['details']['field'][]					        =array('title'=>'Путь в каталоге',	'edit_element'=>'clss_patch') ;
    $_SESSION['descr_clss'][200]['details']['field']['url_name']		    =array('title'=>'URL',		'class'=>'small') ;

    $_SESSION['descr_clss'][200]['details']['field']['price']           =array('title'=>'Цена') ;
//    $_SESSION['descr_clss'][200]['details']['field']['price_opt']       =array('title'=>'Цена - оптовая') ;
//    $_SESSION['descr_clss'][200]['details']['field']['price_opt_big']   =array('title'=>'Цена - крупный опт') ;
//    $_SESSION['descr_clss'][200]['details']['field']['price_opt_vip']   =array('title'=>'Цена - VIP') ;
    $_SESSION['descr_clss'][200]['details']['field']['koof']			      ='Коф.нац.' ;
    $_SESSION['descr_clss'][200]['details']['field']['sales_type']		  ='Тип скидки';
    $_SESSION['descr_clss'][200]['details']['field']['sales_value']		  ='Размер скидки' ;
    $_SESSION['descr_clss'][200]['details']['field'][]					        =array('title'=>'Цена с учетом скидки',				'edit_element'=>'clss_all_prices') ;

    $_SESSION['descr_clss'][200]['details']['field']['sezon']					  =array('title'=>'Сезонность') ;
    $_SESSION['descr_clss'][200]['details']['field']['sortament_type']	=array('title'=>'Тип сортамента') ;
    $_SESSION['descr_clss'][200]['details']['field']['sortament_parametr_name']	=array('title'=>'Название параметра сортамента') ;
    $_SESSION['descr_clss'][200]['details']['field']['spec_cnt']        =array('title'=>'Минимальный заказ') ;           // характеристика - количество в упаковке
    $_SESSION['descr_clss'][200]['details']['field']['spec_min']        =array('title'=>'Количество в упаковке') ;           // характеристика - количество в упаковке
    $_SESSION['descr_clss'][200]['details']['field']['stock2']			    ='Наличие на складе' ;

    $_SESSION['descr_clss'][200]['details']['field']['icon_obj_name']		=array('title'=>'Вывод каталога - Название',		'class'=>'small') ;   // наименование на иконках
    $_SESSION['descr_clss'][200]['details']['field']['icon_obj_comment']=array('title'=>'Вывод каталога - Коментарий',		'class'=>'small') ;   // наименование на иконках
//    $_SESSION['descr_clss'][200]['details']['field']['spec_material']   =array('title'=>'Вывод каталога - Материал') ;     // характеристика - материал
//    $_SESSION['descr_clss'][200]['details']['field']['spec_size']       =array('title'=>'Вывод каталога - Размер') ;          // характеристика - размер
//    $_SESSION['descr_clss'][200]['details']['field']['spec_color']      =array('title'=>'Вывод каталога - Цвет') ;          // характеристика - цвет

    $_SESSION['descr_clss'][200]['details']['field']['spec_name_1']      =array('title'=>'Вывод каталога - название 1') ;          // характеристика - название 1
    $_SESSION['descr_clss'][200]['details']['field']['spec_name_2']      =array('title'=>'Вывод каталога - название 2') ;          // характеристика - название 2
    $_SESSION['descr_clss'][200]['details']['field']['spec_name_3']      =array('title'=>'Вывод каталога - название 3') ;          // характеристика - название 3
    $_SESSION['descr_clss'][200]['details']['field']['spec_name_4']      =array('title'=>'Вывод каталога - название 4') ;          // характеристика - название 4
    $_SESSION['descr_clss'][200]['details']['field']['spec_1']          =array('title'=>'Вывод каталога - Параметр 1') ;     // характеристика - материал
    $_SESSION['descr_clss'][200]['details']['field']['spec_2']          =array('title'=>'Вывод каталога - Параметр 2') ;          // характеристика - размер
    $_SESSION['descr_clss'][200]['details']['field']['spec_3']          =array('title'=>'Вывод каталога - Параметр 3') ;          // характеристика - цвет
    $_SESSION['descr_clss'][200]['details']['field']['spec_4']          =array('title'=>'Вывод каталога - Параметр 4') ;          // характеристика - цвет



//    $_SESSION['descr_clss'][200]['details']['field']['no_index'] 				='Запрет индексации' ;
    $_SESSION['descr_clss'][200]['details']['field']['no_index_yandex'] 	='индексировать в Yandex' ;
    $_SESSION['descr_clss'][200]['details']['field']['no_index_google'] 	='индексировать в Google' ;
    $_SESSION['descr_clss'][200]['details']['field']['top']				      ='Добавить в Хиты' ;
    $_SESSION['descr_clss'][200]['details']['field']['new']				      ='Добавить в Новинки' ;
    $_SESSION['descr_clss'][200]['details']['field']['vendor_art']	 ='артикул производителя' ;

    $_SESSION['descr_clss'][200]['details']['field']['intro']		        =array('title'=>'Краткое описание','use_HTML_editor'=>1) ;
    $_SESSION['descr_clss'][200]['details']['field']['manual']			    =array('title'=>'Описание',			'class'=>'big','use_HTML_editor'=>1) ;
//    $_SESSION['descr_clss'][200]['details']['field']['img_name_upload']		='фото для загрузки' ;

    //    $_SESSION['descr_clss'][200]['details']['field']['goods_of_day']       = 'Товар дня';
    //    $_SESSION['descr_clss'][200]['details']['field']['day']                = 'Дата активации ТД';
    //    $_SESSION['descr_clss'][200]['details']['field']['count_of_day']       = 'Количество для товара дня';



     //Сортамент товара
    $_SESSION['descr_clss'][65]    =array() ;
    $_SESSION['descr_clss'][65]['name']='Сортамент товара' ;
    $_SESSION['descr_clss'][65]['table_code']='sortament' ;
    $_SESSION['descr_clss'][65]['parent']=0 ;
    $_SESSION['descr_clss'][65]['fields']['goods_id']=array('type'=>'int(11)') ;                           // код товара
    $_SESSION['descr_clss'][65]['fields']['art']=array('type'=>'varchar(255)') ;                            // fартикул
    $_SESSION['descr_clss'][65]['fields']['1c_id']=array('type'=>'varchar(73)') ;                          // код-1С
    $_SESSION['descr_clss'][65]['fields']['code']=array('type'=>'varchar(32)') ;                           // штрих-код
    $_SESSION['descr_clss'][65]['fields']['stock']=array('type'=>'int(11)') ;                              // склад
    $_SESSION['descr_clss'][65]['fields']['stock2']=array('type'=>'int(1)') ;                              // склад - принудительно
    $_SESSION['descr_clss'][65]['fields']['price']=array('type'=>'float(10,2)') ;                          // цена
    $_SESSION['descr_clss'][65]['fields']['vendor_art']=array('type'=>'text') ;                            // артикул производителя
    $_SESSION['descr_clss'][65]['fields']['categor']=array('type'=>'indx_select','array'=>'IL_categor') ;  //  мужской / женский / детский
    $_SESSION['descr_clss'][65]['fields']['size']=array('type'=>'varchar(10)') ;                           // размер
    $_SESSION['descr_clss'][65]['fields']['color']=array('type'=>'indx_select','array'=>'IL_colors') ;     // цвет
    $_SESSION['descr_clss'][65]['fields']['color_name']=array('type'=>'varchar(255)') ;                    // цвет
    $_SESSION['descr_clss'][65]['fields']['img_id']=array('type'=>'int(11)') ;                             // id изображения

    $_SESSION['descr_clss'][65]['child_to']=array(200) ;
    $_SESSION['descr_clss'][65]['parent_to']=array(3) ;

    $_SESSION['descr_clss'][65]['list']['field']['pkey']=array('title'=>'Код') ;
    $_SESSION['descr_clss'][65]['list']['field']['enabled']=array('title'=>'Состояние') ;
    $_SESSION['descr_clss'][65]['list']['field']['obj_name']=array('title'=>'Наименование') ;
    $_SESSION['descr_clss'][65]['list']['field']['art']=array('title'=>'Артикул') ;
    $_SESSION['descr_clss'][65]['list']['field']['vendor_art']=array('title'=>'Артикул производителя') ;
    $_SESSION['descr_clss'][65]['list']['field']['indx']=array('title'=>'Позиция') ;
    $_SESSION['descr_clss'][65]['list']['field']['price']=array('title'=>'Цена') ;
    $_SESSION['descr_clss'][65]['list']['field']['code']=array('title'=>'Штрих-код') ;
    $_SESSION['descr_clss'][65]['list']['field']['1c_id']=array('title'=>'код-1С') ;
    $_SESSION['descr_clss'][65]['list']['field']['stock2']=array('title'=>'Склад свой') ;
    $_SESSION['descr_clss'][65]['details']=$_SESSION['descr_clss'][65]['list'] ;
    $_SESSION['descr_clss'][65]['details']['field']['1c_id']=array('title'=>'Код 1С') ;








   	//Клиент
   	$_SESSION['descr_clss'][89]['name']='Клиент' ;
   	$_SESSION['descr_clss'][89]['parent']=86 ;
   	$_SESSION['descr_clss'][89]['fields']=array('phone'=>'varchar(255)','subscript'=>'int(1)','account_reffer'=>'any_object','sales_value'=>'int(11)','city'=>'varchar(32)') ;
    $_SESSION['descr_clss'][89]['icons']=_PATH_TO_BASED_CLSS_IMG.'/98.png' ;

   	//Частное лицо
   	$_SESSION['descr_clss'][87]['name']='Частное лицо' ;
   	$_SESSION['descr_clss'][87]['parent']=89 ;
   	$_SESSION['descr_clss'][87]['fields']=array('adres'=>'text','id_discount_card'=>'text') ;
    $_SESSION['descr_clss'][87]['fields']['rol']=array('type'=>'indx_select','array'=>'ARR_roll') ;
    $_SESSION['descr_clss'][87]['menu'][]=array("name" => "Свойства",				"type" => 'prop') ;
    $_SESSION['descr_clss'][87]['menu'][]=array("name" => "Заказы",				    "cmd" => 'panel_account_orders') ;
    $_SESSION['descr_clss'][87]['menu'][]=array("name" => "Тикеты",				    "cmd" => 'panel_account_support') ;
    $_SESSION['descr_clss'][87]['menu'][]=array("name" => "История",				 "cmd" => 'panel_account_history') ;
    $_SESSION['descr_clss'][87]['icons']=_PATH_TO_BASED_CLSS_IMG.'/98.png' ;

   	$_SESSION['descr_clss'][87]['list']['field']['enabled']				=array('title'=>'Сост.') ;
   	$_SESSION['descr_clss'][87]['list']['field']['obj_name']			=array('title'=>'Фамилия Имя','td_class'=>'left') ;
    $_SESSION['descr_clss'][87]['list']['field']['login']		    	=array('title'=>'Логин') ;
    $_SESSION['descr_clss'][87]['list']['field']['rol']	      =array('title'=>'Роль') ;
   	$_SESSION['descr_clss'][87]['list']['field']['sales_value']		=array('title'=>'Скидка','td_class'=>'left') ;
   	$_SESSION['descr_clss'][87]['list']['field']['subscript']			=array('title'=>'Подписка') ;
   	$_SESSION['descr_clss'][87]['list']['field']['email']				=array('title'=>'email','td_class'=>'left') ;
   	$_SESSION['descr_clss'][87]['list']['field']['phone']				=array('title'=>'Тел.','td_class'=>'left') ;
   	$_SESSION['descr_clss'][87]['list']['field']['adres']				=array('title'=>'Адрес<br>доставки','td_class'=>'left') ;
   	$_SESSION['descr_clss'][87]['list']['field']['id_discount_card']	=array('title'=>'Номер дисконтной карты','edit_element'=>'input','td_class'=>'left') ;

   	//$_SESSION['descr_clss'][87]['list']['field']['account_reffer']		=array('title'=>'Аккаунт') ; // менеджер


   	$_SESSION['descr_clss'][87]['details']['field']['enabled']			=array('title'=>'Сост.') ;
   	$_SESSION['descr_clss'][87]['details']['field']['obj_name']			=array('title'=>'Фамилия Имя','class'=>'small') ;
   	$_SESSION['descr_clss'][87]['details']['field']['sales_value']		=array('title'=>'Скидка') ;
   	$_SESSION['descr_clss'][87]['details']['field']['subscript']		=array('title'=>'Подписка') ;
   	$_SESSION['descr_clss'][87]['details']['field']['id_discount_card'] =array('title'=>'Номер дисконтной карты','edit_element'=>'input') ;
   	$_SESSION['descr_clss'][87]['details']['field']['__group_title1']	=array('title'=>'Данные для авторизации') ;
    $_SESSION['descr_clss'][87]['details']['field']['login']			=array('title'=>'Логин') ;
    $_SESSION['descr_clss'][87]['details']['field']['password']			=array('title'=>'Пароль') ;
   	$_SESSION['descr_clss'][87]['details']['field']['__group_title2']	=array('title'=>'Контактные данные') ;
   	$_SESSION['descr_clss'][87]['details']['field']['email']			=array('title'=>'e-mail') ;
   	$_SESSION['descr_clss'][87]['details']['field']['phone']			=array('title'=>'Тел.') ;
   	$_SESSION['descr_clss'][87]['details']['field']['adres']			=array('title'=>'Адрес<br>доставки','td_class'=>'left') ;
    $_SESSION['descr_clss'][87]['details']['field']['__group_title2']	=array('title'=>'Дополнительная информация') ;
    $_SESSION['descr_clss'][87]['details']['field']['info']				=array('title'=>'Дополнительная информация') ;

   	//Юр.лицо
   	$_SESSION['descr_clss'][88]['name']='Организация' ;
   	$_SESSION['descr_clss'][88]['parent']=89 ;
   	$_SESSION['descr_clss'][88]['fields']=array('contact_name'=>'varchar(255)','adres1'=>'text','adres2'=>'text','rekvezit'=>'text','inn'=>'varchar(255)','kpp'=>'varchar(255)','rs'=>'varchar(255)','ks'=>'varchar(255)','bik'=>'varchar(255)','fax'=>'varchar(255)','site'=>'varchar(255)') ;

    $_SESSION['descr_clss'][88]['menu'][]=array("name" => "Свойства",				"type" => 'prop') ;
    //$_SESSION['descr_clss'][88]['menu'][]=array("name" => "Заказы",				    "cmd" => 'account_admin/panel_account_orders') ;
    //$_SESSION['descr_clss'][88]['menu'][]=array("name" => "Тикеты",				    "cmd" => 'account_admin/panel_account_support') ;
    //$_SESSION['descr_clss'][88]['menu'][]=array("name" => "История",				 "cmd" => 'account_admin/panel_account_history') ;
    $_SESSION['descr_clss'][88]['menu'][]=array("name" => "Заказы",				    "cmd" => 'panel_account_orders') ;
    $_SESSION['descr_clss'][88]['menu'][]=array("name" => "Тикеты",				    "cmd" => 'panel_account_support') ;
    $_SESSION['descr_clss'][88]['menu'][]=array("name" => "История",				 "cmd" => 'panel_account_history') ;


    $_SESSION['descr_clss'][88]['icons']=_PATH_TO_BASED_CLSS_IMG.'/98.png' ;
  $_SESSION['descr_clss'][88]['fields']['rol']=array('type'=>'indx_select','array'=>'ARR_roll') ;

   	$_SESSION['descr_clss'][88]['list']['field']['enabled']					=array('title'=>'Сост.') ;
   	$_SESSION['descr_clss'][88]['list']['field']['obj_name']				=array('title'=>'Организация','td_class'=>'left') ;
   	$_SESSION['descr_clss'][88]['list']['field']['contact_name']			=array('title'=>'Контактное лицо','td_class'=>'left') ;
       $_SESSION['descr_clss'][88]['list']['field']['login']			=array('title'=>'Логин') ;
   	$_SESSION['descr_clss'][88]['list']['field']['sales_value']				=array('title'=>'Скидка') ;
   	$_SESSION['descr_clss'][88]['list']['field']['subscript']				=array('title'=>'Подписка') ;
   	$_SESSION['descr_clss'][88]['list']['field']['email']					=array('title'=>'email','td_class'=>'left','read_only'=>1) ;
   	$_SESSION['descr_clss'][88]['list']['field']['phone']					=array('title'=>'Тел.','td_class'=>'left') ;

   	$_SESSION['descr_clss'][88]['details']['field']['enabled']				=array('title'=>'Сост.') ;
   	$_SESSION['descr_clss'][88]['details']['field']['obj_name']				=array('title'=>'Организация','class'=>'small') ;
   	$_SESSION['descr_clss'][88]['details']['field']['sales_value']			=array('title'=>'Скидка') ;
   	$_SESSION['descr_clss'][88]['details']['field']['subscript']			=array('title'=>'Подписка') ;

   	$_SESSION['descr_clss'][88]['details']['field']['__group_title1']		=array('title'=>'Данные для авторизации') ;
   	$_SESSION['descr_clss'][88]['details']['field']['login']				=array('title'=>'Логин','read_only'=>1) ;
      	$_SESSION['descr_clss'][88]['details']['field']['password']			    =array('title'=>'Пароль','read_only'=>1) ;

   	$_SESSION['descr_clss'][88]['details']['field']['__group_title2']		=array('title'=>'Контактные данные') ;
       $_SESSION['descr_clss'][88]['details']['field']['contact_name']		    =array('title'=>'Контактное лицо','class'=>'left') ;
   	$_SESSION['descr_clss'][88]['details']['field']['email']				=array('title'=>'email','read_only'=>1) ;
   	$_SESSION['descr_clss'][88]['details']['field']['phone']				=array('title'=>'Тел.') ;

   	$_SESSION['descr_clss'][88]['details']['field']['__group_title3']		=array('title'=>'Данные по организации') ;
   	$_SESSION['descr_clss'][88]['details']['field']['adres1']				=array('title'=>'Юр.адрес','class'=>'big') ;
   	$_SESSION['descr_clss'][88]['details']['field']['adres2']				=array('title'=>'Факт.адрес','class'=>'big') ;
   	$_SESSION['descr_clss'][88]['details']['field']['rekvezit']				=array('title'=>'Реквизиты','class'=>'big') ;

       $_SESSION['descr_clss'][88]['details']['field']['__group_title2']	=array('title'=>'Дополнительная информация') ;
       $_SESSION['descr_clss'][88]['details']['field']['ext_info']				=array('title'=>'Дополнительная информация') ;



   	// 90 - ранее ПБОЮЛ
       // 91 - магазин
   	$_SESSION['descr_clss'][91]['name']='Магазин' ;
   	$_SESSION['descr_clss'][91]['parent']=89 ;
   	$_SESSION['descr_clss'][91]['fields']=array('adres'=>'text','city'=>'varchar(255)','region'=>'varchar(255)','vip'=>'int(1)','info'=>'serialize') ;

       $_SESSION['descr_clss'][91]['icons']=_PATH_TO_BASED_CLSS_IMG.'/98.png' ;
  $_SESSION['descr_clss'][91]['fields']['rol']=array('type'=>'indx_select','array'=>'ARR_roll') ;

   	$_SESSION['descr_clss'][91]['list']['field']['enabled']					=array('title'=>'Сост.') ;
   	$_SESSION['descr_clss'][91]['list']['field']['account_reffer']		    =array('title'=>'Аккаунт') ;
   	$_SESSION['descr_clss'][91]['list']['field']['obj_name']				=array('title'=>'Наименование','td_class'=>'left') ;
   	$_SESSION['descr_clss'][91]['list']['field']['vip']						=array('title'=>'VIP','td_class'=>'left') ;
   	$_SESSION['descr_clss'][91]['list']['field']['region']					=array('title'=>'Регион','td_class'=>'left') ;
   	$_SESSION['descr_clss'][91]['list']['field']['city']					=array('title'=>'Город','td_class'=>'left') ;
       $_SESSION['descr_clss'][91]['list']['field']['adres']					=array('title'=>'Адрес','td_class'=>'left') ;
   	$_SESSION['descr_clss'][91]['list']['field']['email']					=array('title'=>'email','td_class'=>'left') ;
   	$_SESSION['descr_clss'][91]['list']['field']['phone']					=array('title'=>'Тел.','td_class'=>'left') ;

   	$_SESSION['descr_clss'][91]['details']=$_SESSION['descr_clss'][91]['list'] ;
   	$_SESSION['descr_clss'][91]['details']['field']['worktime']				=array('title'=>'Время работы') ;
   	$_SESSION['descr_clss'][91]['details']['field']['ICQ']					=array('title'=>'ICQ') ;
   	$_SESSION['descr_clss'][91]['details']['field']['web']					=array('title'=>'web') ;
   	$_SESSION['descr_clss'][91]['details']['field']['dostavka']				=array('title'=>'Наличие доставки') ;
   	$_SESSION['descr_clss'][91]['details']['field']['dostavka_text']		=array('title'=>'Описание доставки') ;
   	$_SESSION['descr_clss'][91]['details']['field']['kredit']				=array('title'=>'Наличие кредита') ;
   	$_SESSION['descr_clss'][91]['details']['field']['info']		            =array('title'=>'Доп информация') ;

    $_SESSION['descr_clss'][216]['name']='Права доступа' ;
    $_SESSION['descr_clss'][216]['table_code']='permit' ;
    $_SESSION['descr_clss'][216]['parent']=0 ;
    $_SESSION['descr_clss'][216]['fields']['obj_name']=array('type'=>'varchar(255)') ; //  название скрипта
    $_SESSION['descr_clss'][216]['fields']['rol_id']=array('type'=>'int(11)') ; //  доступ



}



?>