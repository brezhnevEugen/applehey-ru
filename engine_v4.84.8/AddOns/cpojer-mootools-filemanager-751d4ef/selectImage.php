<?php

include(_PATCH_TO_MOOTOOLS_FILEMANAGER.'/Assets/Connector/FileManager.php');

$browser = new FileManager(array(
  'directory' => '/public/',
  'thumbnailPath' => '/public/Thumbnails/',
  'assetBasePath' => _PATCH_TO_MOOTOOLS_FILEMANAGER.'/Assets',
  'chmod' => 0777
));

$browser->fireEvent(!empty($_GET['event']) ? $_GET['event'] : null);