<?php
include_once('../../../ini/patch.php') ;
error_reporting(E_ALL);
header('Content-Type:text/html; charset=UTF-8') ;
$_POST['directory']=str_replace('..','',$_POST['directory']) ;
include('Assets/Connector/FileManager.php');

// Please add your own authentication here
function UploadIsAuthenticated($get)
{
  if(!empty($get['session'])) return true;
  return false;
}

$browser = new FileManager(
array( 'directory' => _PATH_TO_PUBLIC,
       'thumbnailPath' => _PATH_TO_PUBLIC,
       'assetBasePath' => 'Assets',
       'chmod' => 0777
));

$browser->fireEvent(!empty($_GET['event']) ? $_GET['event'] : null);

?>