<?

function macros_apply(&$text,$rec=array())
{   if (!$text) return ;  // return ;
    for ($i=1;$i<=2;$i++)
    { macros_apply_templates($text,$rec) ;
      macros_apply_info_id($text,$rec) ;
      if (_USE_SUBDOMAIN) macros_convert_url_to_absolute($text,$rec) ;
    }
}

 // [*122*]
  function macros_apply_info_id(&$text)
  { preg_match_all('#\[\*(.*?)\*\]#i',$text,$match) ;
    if (sizeof($match[1])) foreach($match[1] as $indx=>$info_id)
    { $out = _get_id($info_id);
      $text=str_replace($match[0][$indx],$out,$text) ;
    }
  }

  function macros_apply_templates(&$text)
  {  preg_match_all('#\[(.*?)\]#i',$text,$match) ; //damp_array($match) ;
     if (sizeof($match[1])) foreach($match[1] as $indx=>$tempate_str)
      { $arr_params=explode(',',$tempate_str) ;
        $tempate_str=$match[0][$indx] ;
        // шаблоны нового образца
        if (isset($_SESSION['info_system']->templates_names[addslashes($arr_params[0])])) macros_exec_template_extended($tempate_str,$arr_params,$text) ;
        // разбираем шаблоны старого образца
        if (sizeof($arr_params)==1 and
            mb_strpos($arr_params[0],'template=')!==false)                                macros_exec_template_only($tempate_str,$arr_params,$text) ;
        if (sizeof($arr_params)==2 and
            mb_strpos($arr_params[0],'template=')!==false and
            mb_strpos($arr_params[1],'class=')!==false)                                   macros_exec_template_class($tempate_str,$arr_params,$text) ;
        if (sizeof($arr_params)==3 and
            mb_strpos($arr_params[0],'catalog=')!==false and
            mb_strpos($arr_params[1],'select_usl=')!==false and
            mb_strpos($arr_params[2],'template=')!==false)                                macros_exec_catalog_usl_template($tempate_str,$arr_params,$text) ;
        if (sizeof($arr_params)==3 and
            mb_strpos($arr_params[0],'catalog=')!==false and
            mb_strpos($arr_params[1],'view=')!==false and
            mb_strpos($arr_params[2],'template=')!==false)                                macros_exec_catalog_view_template($tempate_str,$arr_params,$text) ;
      }
  }

  // preg_match_all('#\[template\=(.*?)\]#i',$text,$match) ; //damp_array($match) ;
  function macros_exec_template_only($tempate_str,$arr_params=array(),&$text)
  {  list($temp,$template_name)=explode('=',$arr_params[0]) ;
     ob_start();
     print_template($template_name) ;
     $out = ob_get_clean();
     $text=str_replace($tempate_str,$out,$text) ;
  }

  // шаблон: [template=panel_right_float,class=services]
  function macros_exec_template_class($tempate_str,$arr_params=array(),&$text)
  {  list($temp,$template_name)=explode('=',$arr_params[0]) ;
     list($temp,$class_name)=explode('=',$arr_params[1]) ;
     ob_start();
     print_template($template_name,array('class'=>$class_name)) ;
     $out = ob_get_clean();
     $text=str_replace($tempate_str,$out,$text) ;
  }

  // preg_match_all('#\[catalog\=(.*?),select_usl\=(.*?),template\=(.*?)\]#i',$text,$match) ;
  // [catalog=art,select_usl="parent=80 and top>0",template=formulapereezda/list_items_clients_table_img]
  function macros_exec_catalog_usl_template($tempate_str,$arr_params=array(),&$text)
  {  //echo '$tempate_str='.$tempate_str.'<br>' ;
     //echo '$arr_params='.print_r($arr_params,true).'<br>' ;

     list($temp,$catalog_name)=explode('=',$arr_params[0]) ;
     $usl_text=str_replace('select_usl=','',$arr_params[1]) ;
     list($temp,$template_name)=explode('=',$arr_params[2]) ;
     //echo '$usl_text='.$usl_text.'<br>' ;
     ob_start();
     $system_name=$catalog_name.'_system' ;
     $select_usl=str_replace('"','',html_entity_decode($usl_text)) ;    // !!! HTML-editor заменяет спецсиммоволы HTML-последовательносятми
     if (is_object($_SESSION[$system_name])) $_SESSION[$system_name]->show_list_items($select_usl,$template_name,array('debug'=>0)) ; // выводим товары
     $out = ob_get_clean();
     $text=str_replace($tempate_str,$out,$text) ;
  }

  // шаблон: [catalog=goods,view=39,template=list_art_a_name]
  function macros_exec_catalog_view_template($tempate_str,$arr_params=array(),&$text)
  {  list($temp,$catalog_name)=explode('=',$arr_params[0]) ;
     list($temp,$view)=explode('=',$arr_params[1]) ;
     list($temp,$template_name)=explode('=',$arr_params[2]) ;
     ob_start();
     $system_name=$catalog_name.'_system' ;
     if (is_object($_SESSION[$system_name])) $_SESSION[$system_name]->show_list_items($view,$template_name,array('debug'=>0)) ; // выводим товары
     $out = ob_get_clean();
     $text=str_replace($tempate_str,$out,$text) ;
  }

  function macros_exec_template_extended($tempate_str,$arr_params=array(),&$text)
  {   $template_name=$arr_params[0] ;
      $template_id=$_SESSION['info_system']->templates_names[addslashes($template_name)] ;
      $rec_template=$_SESSION['info_system']->templates[$template_id] ;

      // преобразовываем в массив опции по умолчанию
      $arr_options=explode("\n",$rec_template['options_text']) ;
      $options=array() ;
      if (sizeof($arr_options)) foreach($arr_options as $line)
      {  unset($param,$value) ;
         list($name,$value)=explode('=',$line) ;
         if ($name and isset($value)) $options[$name]=$value ;
      }

      // преобразовываем все остальные переданные элементы в опции  - кроме первого элемента
      if (sizeof($arr_params)) foreach($arr_params as $indx=>$params_str) if ($indx)
      { unset($param,$value) ;
        list($param,$value)=explode('=',$params_str) ;
        if ($param and isset($value)) $options[$param]=$value  ;
      }

      $obj_rec=array() ;
      $func_name=$rec_template['script'] ;

      if ($rec_template['system'])
      {  $system_name=$_SESSION['list_created_sybsystems'][$rec_template['system']] ;
         //echo 'Шаблон будет выводить объекты из подсистемы <strong>'.$system_name.'</strong><br>' ;
         if (!is_object($_SESSION[$system_name])) {echo 'Подсистема не существует' ; return ;}
      }

      ob_start() ;
        if ($rec_template['template_obj']==1) // используем текущий объект
        { $obj_id=$GLOBALS['obj_info']['pkey'] ;
          //echo '$obj_id='.$obj_id.'<br>' ;
          if (!$obj_id)
          { $links=get_arr_links($rec_template['pkey'],$rec_template['tkey']) ;
            if (sizeof($links))
            { list($link_obj_reffer,$link_obj_rec)=each($links) ;
              list($obj_pkey,$obj_tkey)=explode('.',$link_obj_reffer) ;
              if ($obj_tkey==$_SESSION[$system_name]->tkey) $obj_id=$obj_pkey ;
              //$GLOBALS['obj_info']=get_obj_info($link_obj_reffer) ;
            }

          }
        }

        if ($rec_template['template_obj']==2) // используем слинкованный объект в качестве основного
        {  // запоминаем текущий cur_page и obj_info
           $obj_info_current=$GLOBALS['obj_info'] ;
           $cur_page_seo_info_current=_CUR_PAGE()->SEO_info ;

           $links=get_arr_links($rec_template['pkey'],$rec_template['tkey']) ;
           if (sizeof($links))
           { list($link_obj_reffer,$link_obj_rec)=each($links) ;
             _CUR_PAGE()->SEO_info=execSQL_van('select * from '.TM_SITEMAP.' where obj_reffer="'.$link_obj_reffer.'"') ;
             $GLOBALS['obj_info']=get_obj_info($link_obj_reffer) ;
           }
        }

        if ($rec_template['template_obj']==3) // использует объект, код которого передан вторым параметром шаблона
        { $obj_id=html_entity_decode($arr_params[1]) ;

        }

        //echo '$obj_id='.$obj_id.'<br>' ;
      //damp_array($options) ;

        switch($rec_template['template_type'])
          { case 1: if ($obj_id) $_SESSION[$system_name]->show_list_items($obj_id,$func_name,$options) ; break ;
            case 2: if ($obj_id) $_SESSION[$system_name]->show_list_section($obj_id,$func_name,$options) ; break ;
            case 3: print_template($func_name,$options) ; break ;
            case 4: if ($obj_id) $_SESSION[$system_name]->show_item($obj_id,$func_name,$options) ; break ;
          }

        if ($rec_template['template_obj']==2)
        {  // возвращаем обратно текущий cur_page и obj_info
           $GLOBALS['obj_info']=$obj_info_current ;
           _CUR_PAGE()->SEO_info=$cur_page_seo_info_current ;
        }

      $out=ob_get_clean() ;
      //echo $tempate_str.'<br>' ;
      //trace() ;
      $text=str_replace($tempate_str,$out,$text) ;
  }


  function macros_convert_url_to_absolute(&$text)
  { $a = array();
    if (preg_match_all('/(href|src)=([\"\']?([^\"\'\s >]*)[\"\'])/is',$text,$matches,PREG_SET_ORDER)) foreach($matches as $match) { array_push($a,array($match[2],$match[3]));}

    $arr_from=array() ;  $arr_to=array() ;
    if (sizeof($a)) foreach($a as $math)
     { $url=str_replace('//www','http://www',$math[1]) ;
       $arr=parse_url($url) ; //if (strpos($url,'google')!==false) damp_array($arr,1,-1) ;
       if (!$arr['scheme'] and !$arr['host'] and $arr['scheme']!='mailto' and $arr['scheme']!='MAILTO' and $arr['path'])
       { $arr_from[]=$from=$math[0] ;
         if (!$arr['scheme']) $arr['scheme']='http' ;
         //$arr_to[]=$to='"'.$arr['scheme'].'://'._MAIN_DOMAIN.$arr['path'].'"' ;
         $arr_to[]=$to='"'.$arr['scheme'].'://'._MAIN_DOMAIN.$arr['path'].'"' ;
         //echo $from.' => '.$to.'<br>' ;
       }
     }
    $text=str_replace($arr_from,$arr_to,$text) ;
  }



?>