<?php
  //global $dir_to_engine ;
  //include_once($dir_to_engine.'admin/m_admin_system.php') ;


//*************************************************************************************************************************************/
//
// фукции обработки изображений
//
//*************************************************************************************************************************************/

// создать клон $clone фото $fname из таблицы $tkey
 // или создать все клоны

 function create_clone_image($tkey,$fname,$options=array())
 {  global $descr_obj_tables;
    $dir=$descr_obj_tables[$tkey]->dir_to_image ;
    $options['tkey']=$tkey ;
    if (!$options['clone'])
    { $list_clone=$descr_obj_tables[$tkey]->list_clone_img ;
      $list_clone['small']['resize']=array('width'=>100,'height'=>100,'fields'=>1,'resampled'=>1,'back_color'=>'FFFFFF','quality'=>100);

      if (sizeof($list_clone)) foreach($list_clone as $indx=>$rec)
	  {
        $options_res=array_merge($rec,$options) ;
	    $options_res['output_fname']=$dir.$indx.'/'.$fname ;
        if ($options_res['output_fname'] and !$options['no_comment']) echo 'Создаем клон '.$options_res['resize']['width'].'x'.$options_res['resize']['height'].' ' ;
        if ($fname) $res=working_image($dir.'source/'.$fname,$options_res) ;

        if (!$options['no_comment']) echo ($res)? ', <span class=green> размер=<b>'.round($res/1024).'</b> kb </span>качество '.$options_res['resize']['quality'].'<br>':'<span class="red bold"> Не удалось создать клон (несовместимый формат, файл поврежден, нет директрии или места на диске, нет прав) </span><br>' ;

	  }
	}
	else
	{   if ($options['clone']=='small') $options_res['resize']=array('width'=>100,'height'=>100,'fields'=>1,'resampled'=>1,'back_color'=>'FFFFFF','quality'=>100);
	    						   else $options_res=array_merge($descr_obj_tables[$tkey]->list_clone_img[$options['clone']],$options) ;
	    $options_res['output_fname']=$dir.$options['clone'].'/'.$fname ;
        if ($options_res['output_fname']) echo 'Создаем клон '.$options_res['resize']['width'].'x'.$options_res['resize']['height'].' для '.$dir.'source/'.$fname ;
        if ($fname) $res=working_image($dir.'source/'.$fname,$options_res) ;
        echo (($res)? ', <span class=green> размер=<b>'.round($res/1024).'</b> kb </span>качество '.$options_res['resize']['quality'].'<br>':'<span class="red bold"> Не удалось создать клон (несовместимый формат,файл поврежден, нет директрии или места на диске, нет прав) </span><br>') ;
	}
	return($res) ;
 }


 // функция обработки изображений

// *** опции масштабирования изображения ****
// $options['resize']['width']   	- ширина нового изображения
// $options['resize']['height']  	- высота нового изображения
// $options['resize']['fields']  	- масштабировать с полями (1) или без (0)
// $options['resize']['resampled']- ресамп изображения при резайсе

// *** опции наложения логотипа ****
// логотипов может быть несколько

// $options['logo'][]['fname'] 		- имя накладываемого изображения (с путем)
// $options['logo'][]['x']  	 	- смещение по X
// $options['logo'][]['y']	 		- смещение по Y
// $options['logo'][]['transparent']- прозрачность, 1...100
// $options['logo'][]['moment'] 	- на что накладывать логотип - на исходник ('before_resize') или на ресайс ('after_resize')

// *** опции вывода ****
// $options['output_fname']			- имя, куда сохранить изображение (с путем)

// при использовании функции необходимо обращать внимание на то, что

function working_image($fname,$options=array())
 {  $res=0 ;
    $source=open_image($fname) ;
	if ($source!=null)
	{
         if (sizeof($options['logo'])) foreach($options['logo'] as $logo_inf) if ($logo_inf['moment']=='before_resize') image_add_logo($source,$logo_inf) ;

         if ($options['resize']) resize_image($source,$options['resize']) ;

         if (sizeof($options['logo'])) foreach($options['logo'] as $logo_inf) if ($logo_inf['moment']!='before_resize') image_add_logo($source,$logo_inf) ;

  		 $res=put_image($source,$options) ;

         $source->clear();
         $source->destroy();
    }
    return($res) ;
 }

 // преобразуем изображение в заданный тип
 function convert_image($dir,$fname,$to_type)
 {  $source=open_image($dir.$fname) ;
    $res=put_image($source,strtoupper($to_type),array('output_fname'=>$dir.$fname.'.'.$to_type)) ;
    if ($res) return($fname.'.'.$to_type) ; else return($fname) ;
 }

 function open_image($fname)
 {   $source=null ;
     $id_type=exif_imagetype($fname) ;
     if (!$id_type) echo '<span class="red">Файл "'.$fname.' не является изображенем. Обработка отменена.<br></span>' ;
     else
     { $source = new Imagick();
       $file_handle_for_viewing_image = fopen($fname, 'a+');
       $source->readImageFile($file_handle_for_viewing_image);
       fclose($file_handle_for_viewing_image);
       if (!$source)
        { echo '<span class="red">Не удалось открыть изображение для обработки. Сохраните изображение в другом формате и попробуйте еще раз.<br></span>' ;
         $source=null ;
        }
     }
     return($source) ;
 }

 function put_image(&$image,$options=array())
  { if ($options['output_fname']) 	{ $fname=$options['output_fname'] ;
    								  if (is_file($fname)) unlink($fname) ;
    								} else $fname='' ;

    //if ($options['resize']['quality']) $quality=$options['resize']['quality'] ; else $quality=100 ;

    if (!$fname)
    { $type=$image->getImageFormat() ;
      switch($type)
         { case 'JPG' : header('Content-type: image/jpeg'); break ;
           case 'JPEG' : header('Content-type: image/jpeg'); break ;
           case 'GIF' : header('Content-type: image/gif'); break ;
           case 'PNG' : header('Content-type: image/png'); break ;
           case 'BMP' : header('Content-type: image/wbmp'); break ;
         }
    }

    //устанавливаем степень сжатия
    $image->setImageCompressionQuality(90);

    $image->setImageFormat($type);

    if ($options['output_fname']) $image->writeImage($options['output_fname']);
    else                          echo $image->getImageBlob();


    $size=(file_exists($fname))? filesize($fname):0 ;
    return($size) ;
  }

 // масштабирование изображения
 // по умолчанию - с полями, цвет задника - белый, без ресампла

 function resize_image(&$source,$options)
 { $options['fields']=($options['fields'])? 1:0 ;
   $options['back_color']=($options['back_color'])? $options['back_color']:'ffffff' ;
   $options['resampled']=($options['resampled'])? 1:0 ;

   $geo=$source->getImageGeometry();

   $w1=$geo['width'];
   $h1=$geo['height'];

   $w2=$options['width'] ; $h2=$options['height'] ;
   if ($w2 or $h2) // масштабируем фото к размерам w2 и h2
 	{	$k_w=$w2/$w1 ; $k_h=$h2/$h1 ;

        if (!$h2) { if ($w2<$w1) { $h2=$h1*$k_w ; $k_h=$k_w ; } else { $h2=$h1 ; $k_h=1 ; } }// если не задана высота, то выбираем высоту пропорционально ширине
 	    if (!$w2) { if ($h2<$h1) { $w2=$w1*$k_h ; $k_w=$k_h ; } else { $w2=$w1 ; $k_w=1 ; } }// если не задана ширина, то выбираем ширину пропорционально высоте

 	    $k1=$w1/$h1 ; $k2=$w2/$h2 ;

	 	$dw=0 ; $dh=0 ; $w=0 ; $h=0 ;

         //echo 'w1 - h1: '.$w1.' - '.$h1.'<br>' ;
         //echo 'w2 - h2: '.$w2.' - '.$h2.'<br>' ;
         //echo 'k1: '.$k1.'<br>' ;
         //echo 'k2: '.$k2.'<br>' ;

        if ($w1<$w2 and $h1<$h2) // если масштабиремое фото меньше конечного, то просто разместить его по центру изображения
        { $h1_new=$h1 ; $w1_new=$w1 ; $dw=($w2-$w1_new)/2 ;  $dh=($h2-$h1_new)/2 ;
          $im_convas  = new Imagick();
          $im_convas->newPseudoImage($w2,$h2,'canvas:#FFFFFF');
          $im_convas->compositeImage($source, Imagick::COMPOSITE_DEFAULT, $dw, $dh);
          $source->clear();
          $source->destroy();
          $source=$im_convas ;
          //echo '+<br>' ;
        }
        else if ($options['fields']) // масштабируем с полями
        {
          //echo '<strong>масштабируем с полями</strong><br>' ;
          if (($k1<=1 and $k2>=1) or ($k1>1 and $k2>1)) // будем добавлять поля по ширине
          {  $w1_new=$w1*$k_h ; $h1_new=$h2 ;
             //echo '$w1_new='.$w1_new.'<br>' ;
             //echo '$h1_new='.$h1_new.'<br>' ;
             $source->resizeImage($w1_new,$h1_new,Imagick::FILTER_LANCZOS,1,0);
             $dw=($w2-$w1_new)/2 ;
             $im_convas  = new Imagick();
             $im_convas->newPseudoImage($w2,$h2,'canvas:#FFFFFF');
             $im_convas->compositeImage($source, Imagick::COMPOSITE_DEFAULT, $dw, 0);
             $source->clear();
             $source->destroy();
             $source=$im_convas ;

          }
          if (($k1<=1 and $k2<1) or ($k1>1 and $k2<=1)) // будем добавлять поля по высоте
          {  $w1_new=$w2 ; $h1_new=$h1*$k_w ;
             $source->resizeImage($w1_new,$h1_new,Imagick::FILTER_LANCZOS,1,0);
             $dh=($h2-$h1_new)/2 ;
             $im_convas  = new Imagick();
             $im_convas->newPseudoImage($w2,$h2,'canvas:FFFFFF');
             $im_convas->compositeImage($source, Imagick::COMPOSITE_DEFAULT, 0, $dh);
             $source->clear();
             $source->destroy();
             $source=$im_convas ;
          }


        }
        else // масштабируем без полей
        { $w1_new=$w2 ; $h1_new=$h1*$k_w ; $dw=0 ; $dh=($h2-$h1_new)/2 ;
          echo '<strong>масштабируем без полей</strong><br>' ;
          //echo '$w1='.$w1.'<br>' ;
          //echo '$h1='.$h1.'<br>' ;
          //echo '$w1_new='.$w1_new.'<br>' ;
          //echo '$h1_new='.$h1_new.'<br>' ;
          //echo '$dw='.$dw.'<br>' ;
          //echo '$dh='.$dh.'<br>' ;
          //$source->resizeImage($w1_new,$h1_new,Imagick::FILTER_POINT,1,0);
          $source->resizeImage($w2,$h2,Imagick::FILTER_LANCZOS,1,0);
        }

        //if ($options['resampled'])
        //     imagecopyresampled($im_convas, $source,$dw, $dh, 0, 0,$w1_new,$h1_new,$w1,$h1);
        //else imagecopyresized($im_convas, $source,$dw, $dh, 0, 0,$w1_new,$h1_new,$w1,$h1);

        //$source->adaptiveResizeImage($w2,$h2,false);

        //
    }
 }

// добавление логотипа на рисунок
 // по умолчанию - непрозрачность - 100, x=0, y=0
 function image_add_logo(&$source,$logo_inf)
 {   if (!$logo_inf['x']) $logo_inf['x']=0 ;
     if (!$logo_inf['y']) $logo_inf['y']=0 ;
     if (!$logo_inf['transparent']) $logo_inf['transparent']=100 ;

     $logo = new Imagick();
     $logo->readImage($logo_inf['fname']);

    //накладываем изображения
     //$source->compositeImage($logo, Imagick::COMPOSITE_DEFAULT, $logo_inf['x']+20, $logo_inf['y']+20);
     $source->compositeImage($logo, Imagick::COMPOSITE_MULTIPLY, $logo_inf['x'], $logo_inf['y']);
     //$source->compositeImage($logo, Imagick::COMPOSITE_OVER, $logo_inf['x']+20, $logo_inf['y']+20);
    //соединяем слои
     $source->flattenImages();
 }

 function get_list_img_for_dir($dir_image)
 {
   $dir_list_images=scandir($dir_image) ;
   // чистим массив исходников от не-фоток
   if (sizeof($dir_list_images)) foreach ($dir_list_images as $indx=>$fname)
     if (!(stripos($fname,'.jpg')!==false or stripos($fname,'.gif')!==false or stripos($fname,'.png')!==false or stripos($fname,'.bmp')!==false)) unset($dir_list_images[$indx]) ;
   return($dir_list_images) ;
 }

  // проверить наличие каталога изображений для объектной таблицы
 function check_obj_table_image_dir($tkey)
 { global $descr_obj_tables,$descr_obj_clss ;
    // проверяем, существует ли каталог для хранения изображений
    // если нет, то создаем
    $show_img_pars=0 ;
    $cur_table=$descr_obj_tables[$tkey] ;
    if (sizeof($cur_table->list_clss)) foreach($cur_table->list_clss as $clss=>$clss_tkey)
     if ($descr_obj_clss[$clss] and $descr_obj_clss[$clss]->get_obj_by_clss(3) and $cur_table->list_clss[$clss]==$tkey) $show_img_pars=1 ;


    if ($descr_obj_tables[$tkey]->dir_to_image and $show_img_pars)
    { echo "<br><b>Проверка существования каталога изображений для '".$descr_obj_tables[$tkey]->table_name."'</b><br>" ;
      $dir=$descr_obj_tables[$tkey]->dir_to_image ;
      //if (is_dir($dir))
      {  echo $dir.": " ;
         if (!is_dir($dir))  { if (mkdir($dir)) echo "cоздан <br>" ;
	        					else echo "Невозможно создать<br>" ;
	        				 }
	      else echo 'OK<br>'  ;

	      echo $dir."small/: " ;
	      if (!is_dir($dir.'small/'))  { if (mkdir($dir.'small/')) echo "cоздан <br>" ;
	        							 else echo "Невозможно создать <br>" ;
	        							}
	      else echo 'OK<br>'  ;

	      echo $dir."source/: " ;
	      if (!is_dir($dir.'source/'))  { if (mkdir($dir.'source/')) echo "cоздан <br>" ;
	        							 else echo "Невозможно создать <br>" ;
	        							}
	      else echo 'OK<br>'  ;


	      if (sizeof($descr_obj_tables[$tkey]->list_clone_img)) foreach( $descr_obj_tables[$tkey]->list_clone_img as $dir_clone=>$info)
	      { $_dir=$dir.$dir_clone.'/' ;
	        echo $_dir.': ' ;
	        if (!is_dir($_dir))  { if (mkdir($_dir)) echo "cоздан <br>" ;
	          					   else echo "Невозможно создать <br>" ;
	          					 }
	        else echo 'OK<br>'  ;
	      }
	  } //else echo 'Каталог '.$dir.' не существует<br>' ;
	  echo '<br>' ;
	}
 }

 function delete_clone_image($tkey,$fname)
 {
    global $descr_obj_tables ;
    $dir=$descr_obj_tables[$tkey]->dir_to_image ;
    //echo '<b>Удаляем клоны изображения для '.$dir.$fname.'</b><br>' ;
    if (sizeof($descr_obj_tables[$tkey]->list_clone_img)) foreach($descr_obj_tables[$tkey]->list_clone_img as $rec)
    { if (is_file($dir.$rec['dir'].'/'.$fname))
 	  {  //echo 'Клон удален '.$dir.$rec['dir'].'/'.$fname.'<br>' ;
 	     unlink($dir.$rec['dir'].'/'.$fname) ;
 	  }
    }
 }

 // сохраняем загруженное изображение
// tkey  -
// ftmpname - имя временной фото ( c путем)
// fname - имя конечного фото (без пути)

function upload_image($tkey,$pkey,$source_file,$desct_file,$options=array())
{ global $descr_obj_tables ;
  //set_time_limit(0)  ;
  $dir=$descr_obj_tables[$tkey]->dir_to_image.'source/' ;
  $parent=execSQL_van('select parent from '.$descr_obj_tables[$tkey]->table_name.' where pkey='.$pkey) ;
  $desct_name=$parent['parent'].'_'.$desct_file ;

  if (is_file($dir.$desct_name)) if (!unlink($dir.$desct_name)) echo '&nbsp;&nbsp;Не удается заменить файл '.$dir.$desct_name;

  //if (!rename($source_file, $dir.$desct_name)) echo '&nbsp;&nbsp;Не удается загрузить файл '.$dir.$desct_name.'<br>';
  if (!move_uploaded_file($source_file, $dir.$desct_name)) echo '&nbsp;&nbsp;Не удается загрузить файл '.$dir.$desct_name.'<br>';
  else
  { $size_info=get_image_info($dir.$desct_name,'source ') ;
    if (!$options['no_comment']) echo 'Файл '.$desct_name.' успешно загружен, <span class=green>размер '.$size_info.'</span>' ;
    $file_type=get_image_type($dir.$desct_name) ;
    echo 'Формат файла: '.$file_type.'<br>' ;
    if ($file_type=='gif')
    { echo 'Изображение будет преобразовано в формать JPG<br>' ;
      $desct_name=convert_image($dir,$desct_name,'jpg') ;
    }
    update_rec_in_table($descr_obj_tables[$tkey]->table_name,array('obj_name'=>$desct_name),'pkey='.$pkey,array('debug'=>0)) ;
    if (!$options['no_clone']) create_clone_image($tkey,$desct_name,$options)  ;
    return($desct_name) ;
  }
}


function upload_image_by_href($tkey,$pkey,$source_file,$options=array())
{  global $descr_obj_tables ;
   $dir=$descr_obj_tables[$tkey]->dir_to_image.'/source/' ;
   $parent=execSQL_van('select parent from '.$descr_obj_tables[$tkey]->table_name.' where pkey='.$pkey) ;
   $desct_name=$parent['parent'].'_'.basename($source_file) ;
   //$desct_name=basename($source_file) ;

   if (is_file($dir.$desct_name)) if (!unlink($dir.$desct_name)) echo '&nbsp;&nbsp;Не удается заменить файл '.$dir.$desct_name;

   $source_file=str_replace(' ','%20',$source_file) ;
   //echo 'source_file='.$source_file.'<br>' ;
   //echo 'desct_file='.$dir.$desct_name.'<br>' ;

   if (!copy($source_file,$dir.$desct_name)) '&nbsp;&nbsp;Не удается загрузить файл '.$dir.$desct_name.'<br>';
    else { $size_info=get_image_info($dir.$desct_name,'source ') ;
           if (!$options['no_comment']) echo 'Файл '.$desct_name.' успешно загружен, <span class=green>размер '.$size_info.'</span>' ;
           $file_type=get_image_type($dir.$desct_name) ;
           if ($file_type)
           { echo 'Формат файла: <span class="black bold">'.$file_type.'</span><br>' ;
	         if ($file_type=='GIF')
	            { echo 'Изображение будет преобразовано в формать JPG<br>' ;
	              $desct_name=convert_image($dir,$desct_name,'jpg') ;
	            }
	       } else { echo '<span class=red>Неизвестный формат файла, обработка невозможна</span>' ;
	       		    $options['no_clone']=1 ;
	       		  }

	       update_rec_in_table($descr_obj_tables[$tkey]->table_name,array ('obj_name'=>$desct_name),'pkey='.$pkey) ;
	       if (!$options['no_clone']) create_clone_image($tkey,$desct_name,$options)  ;

	       return(1) ;
         }
}

function generate_check_code_img($type=0)
{ global $session_name ;
  if ($session_name) session_name($session_name) ;
  session_start() ;
  if (!$type) $type=0 ;

  $img_params=$_SESSION['check_code_img'][$type] ; //damp_array($img_params) ;

  if (!sizeof($img_params)) return ;
  $_SESSION['check_code_number']='' ;
  // создаем фон изображения
  $im_convas  = imagecreatetruecolor($img_params['width'],$img_params['height']);
  imagefill($im_convas,0,0,hexdec($img_params['back']));
  $x=$img_params['x'] ;
  $y=$img_params['y'] ;
  $angle=0 ;
  $font_name=$img_params['font_name'] ; $font_size=$img_params['font_size'] ;

  $step_x=$img_params['step_x'] ;

  $dev_angle=$img_params['dev_angle'] ;
  $dev_x=$img_params['dev_x'] ;
  $dev_y=$img_params['dev_y'] ;

  for ($i=0; $i<$img_params['digit_count']; $i++)
  { // получаем случайное число 0..9
    $numb=rand(0,9) ;
    $_SESSION['check_code_number'].=$numb ;
    // вносим девиацию в текущие координаты буквы
    if ($dev_x) $_x=round($x+rand(-$dev_x,$dev_x)) ; else $_x=$x ;
    if ($dev_y) $_y=round($y+rand(-$dev_y,$dev_y)) ; else $_y=$y ;
    if ($dev_angle) $_angle=round($angle+rand(-$dev_angle,$dev_angle)) ; else $_angle=$angle ;
    if ($_angle<0) $_angle=360+$_angle ;
    $color='' ; for($j=0;$j<6;$j++) $color.=rand(2,9) ;

    imagefttext ($im_convas,$font_size,$_angle,$_x,$_y,hexdec($color),$font_name,$numb) ;
    $x=$x+$step_x ; // расчет следующей точки
  }

  header('Content-type: image/png');
  imagepng($im_convas) ;
}




  // возвращает информацию по клонам изображения для изображения $obj_name из таблицы  tkey
  function get_clone_info($tkey,$fname)
  { global $descr_obj_tables ;
  	$text='' ;
  	if (sizeof($descr_obj_tables[$tkey]->list_clone_img)) foreach ($descr_obj_tables[$tkey]->list_clone_img as $clone_name=>$clone_info)
  	{  $dir=$descr_obj_tables[$tkey]->dir_to_image.$clone_name.'/' ;
  	   $text.=get_image_info($dir.$fname,$clone_info['resize']['width'].'x'.$clone_info['resize']['height']) ;
    }
    $text.=get_image_info($descr_obj_tables[$tkey]->dir_to_image.'small/'.$fname,'100x100') ;
    return($text) ;
  }

?>