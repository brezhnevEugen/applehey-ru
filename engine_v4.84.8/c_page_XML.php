<?php
// используется для вывода чистого XML документа, вся разметка должна формироваться скриптом
class c_page_XML
{ // конструктор
  public $doc;
  public $xml;

  function c_page_XML(&$options=array())
  {

  }

  function init(&$options=array())
  { $this->on_send_header_before() ;
    $imp = new DOMImplementation; // Creates an instance of the DOMImplementation class
    $this->doc = $imp->createDocument("",""); // Creates a DOMDocument instance
    $this->doc->encoding = 'utf-8';
    $this->doc->formatOutput = true;
    $this->xml = add_element($this->doc,$this->doc,'xml_doc','',array('date'=>date('Y-m-d H:i'))) ;
  }

  // обязательно отправка заголовка через выделенный метод, так как в случае yandex-market может потребоваться вывод в форме html при проверке
  function on_send_header_before() { header("Content-type: text/xml;charset=UTF-8") ; }

  function check_autorize() { return(1);}

  function show()
   {  $this->exec_cmd($_POST['cmd']) ;  // выполняем переданную команду
      $this->doc->saveXML() ;
   }

  // по умолчанию ищем метод для команды только в текущем классе
  function exec_cmd($cmd)
  {   if (method_exists($this,$cmd)) { $this->add_element('exec_method',$cmd) ; $this->$cmd($this->doc,$this->xml) ; }     // ищем метод в c_ajax::---cmd---
      else $this->add_element('exec_method','not_found') ;
  }

  function add_element($name,$value='',$attribute=array(),$options=array())
  { if ($name=='close_mBox_window' and !$value) $value='all' ;
    if ($options['is_div']) $value='<div id="'.$name.'">'.$value.'</div>' ;
    add_element($this->doc,$this->xml,$name,$value,$attribute) ;
  }
    
    
}

?>