<?
function create_sys_table()
 { $_SESSION['list_db_tables']=get_db_info() ;
   if (is_object($_SESSION['events_system'])) $_SESSION['events_system']->log_enabled=0 ;
   include_once('admin/i_setup.php') ;

   ?><h2>Создаем системные таблицы</h2><?
   	$pattern=array( 'table_name'		=> 	TM_DOT,
					'use_clss'			=> 	'1,101,103,104,105,108',
					'table_clss'		=> 	'101',
					'table_title'		=> 	'Объектные таблицы',
					'table_parent'		=> 	2) ;
	$pattern['def_recs'][]				=   array('parent'=>0,'clss'=>1,'indx'=>1,'enabled'=>1,'obj_name'=>'Объектные таблицы') ;
	$pattern['def_recs'][]				=   array('parent'=>1,'clss'=>103,'indx'=>1,'enabled'=>1,'obj_name'=>'Системные таблицы','table_name'=>'admin') ;
	$pattern['def_recs'][]				=   array('parent'=>1,'clss'=>103,'indx'=>2,'enabled'=>1,'obj_name'=>'Таблицы сайта','table_name'=>SITE_CODE) ;
    $id_main=create_table_by_pattern($pattern) ;

    $_SESSION['list_db_tables']=get_db_info() ;
    if (!sizeof($_SESSION['list_db_tables']))  { echo 'Не удалось создать системные таблицы, сорри' ; return ; }

   $id_DOT=execSQL_van('select pkey from obj_descr_obj_tables where clss=103 and table_name="admin"') ;

   	$pattern=array( 'table_name'		=> 	TM_PERSONAL,
					'use_clss'			=> 	'1,95,98',
					'table_clss'		=> 	'101',
					'table_title'		=> 	'Персонал сайта',
					'table_parent'		=> 	$id_DOT['pkey']) ;
   	$pattern['def_recs'][]				=   array('parent'=>0,'clss'=>1,'indx'=>1,'enabled'=>1,'obj_name'=>'Персонал') ;
   	$pattern['def_recs'][]				=   array('parent'=>1,'clss'=>98,'indx'=>1,'enabled'=>1,'obj_name'=>'Админ','login'=>'admin') ;
   	create_table_by_pattern($pattern) ;

   	$pattern=array( 'table_name'		=> 	TM_LINK,
					'use_clss'			=> 	'1,102',
					'table_clss'		=> 	'101',
					'table_title'		=> 	'Связи объектов',
					'table_parent'		=> 	$id_DOT['pkey']) ;
   	create_table_by_pattern($pattern) ;

   	$pattern=array( 'table_name'		=> 	TM_MAIL_TURN,
					'use_clss'			=> 	'1,34',
					'table_clss'		=> 	'101',
					'table_title'		=> 	'Очередь писем',
					'table_parent'		=> 	$id_DOT['pkey']) ;
   	create_table_by_pattern($pattern) ;



   ?><h1>Создаем стандартные таблицы</h1><?
   $_id_DOT=execSQL_van('select pkey from obj_descr_obj_tables where clss=103 and table_name="'.SITE_CODE.'"') ;
   $id_DOT=$_id_DOT['pkey'] ;
   if (!$id_DOT) { echo 'Не найден раздел объектных таблиц с кодом "'.SITE_CODE.'"' ; return ; }

    // настройки  -------------------------------------------------------------------------------------------------------------------
   	$pattern=array() ;
   	$pattern['table_name']		= 	TM_SETTING ;
	$pattern['table_clss']		= 	101 ;
	$pattern['table_parent']	= 	$id_DOT ;
    $pattern['table_title']	    = 	'Настройки сайта' ;
    $pattern['use_clss']		=	'1,99' ;
    $pattern['def_recs'][]	    =	array ('parent'=>0,	'clss'=>1,	'obj_name'=>'Настройки сайта') ;
   	create_table_by_pattern($pattern) ;

   	// шаблоны писем  -------------------------------------------------------------------------------------------------------------------
    $pattern=array() ;
    $pattern['table_name']		= 	TM_MAILS ;
	$pattern['table_clss']		= 	101 ;
	$pattern['table_parent']	= 	$id_DOT;
    $pattern['table_title']	    = 	'Шаблоны писем' ;
    $pattern['use_clss']	    =	'1,14' ;
    $pattern['def_recs'][]	    =	array ('parent'=>0,	'clss'=>1,	'enabled'=>1,	'indx'=>0,	'obj_name'=>'Шаблоны писем') ;
   	create_table_by_pattern($pattern) ;

    // информация по сайту -------------------------------------------------------------------------------------------------------------------
    $pattern=array() ;
   	$pattern['table_name']		= 	TM_INFO ;
	$pattern['table_clss']		= 	101 ;
	$pattern['table_parent']	= 	$id_DOT;
    $pattern['table_title']	    = 	'Информация' ;
    $pattern['use_clss']		= 	'1,7' ;
    $pattern['def_recs'][]	    =	array ('parent'=>0,	'clss'=>1,	'indx'=>0,	'obj_name'=>'Информация сайта') ;
    $pattern['def_recs'][]	    =	array ('parent'=>1,	'clss'=>1,	'indx'=>1,	'obj_name'=>'Шапка сайта') ;
    $pattern['def_recs'][]	    =	array ('parent'=>1,	'clss'=>1,	'indx'=>2,	'obj_name'=>'Левая панель') ;
    $pattern['def_recs'][]	    =	array ('parent'=>1,	'clss'=>1,	'indx'=>3,	'obj_name'=>'Правая панель') ;
    $pattern['def_recs'][]	    =	array ('parent'=>1,	'clss'=>1,	'indx'=>4,	'obj_name'=>'Центральная панель') ;
    $pattern['def_recs'][]	    =	array ('parent'=>1,	'clss'=>1,	'indx'=>5,	'obj_name'=>'Подвал сайта') ;
    $pattern['def_recs'][]	    =	array ('parent'=>1,	'clss'=>1,	'indx'=>6,	'obj_name'=>'Главная страница') ;
   	create_table_by_pattern($pattern) ;

   	// страницы сайта -------------------------------------------------------------------------------------------------------------------
    $pattern=array() ;
   	$pattern['table_name']		= 	TM_PAGES ;
	$pattern['table_clss']		= 	101 ;
	$pattern['table_parent']	= 	$id_DOT;
    $pattern['table_title']	    = 	'Страницы сайта' ;
    $pattern['use_clss']		= 	'1,13,26' ;
    $pattern['def_recs'][]	    =	array ('parent'=>0,	'clss'=>1,	'enabled'=>1,	'indx'=>0,	'obj_name'=>'Страницы сайта') ;
    $pattern['def_indx'][]	    =	array ('c_data'=>'c_data','session_id'=>'session_id,is_robot','is_robot'=>'is_robot') ;
   	create_table_by_pattern($pattern) ;

    // карта сайта -------------------------------------------------------------------------------------------------------------------
    $pattern=array() ;
   	$pattern['table_name']		= 	TM_SITEMAP ;
	$pattern['table_clss']		= 	101 ;
	$pattern['table_parent']	= 	$id_DOT;
    $pattern['table_title']	    = 	'Карта сайта' ;
    $pattern['use_clss']		= 	'1,36' ;
    $pattern['def_recs'][]	    =	array ('parent'=>0,	'clss'=>1,	'enabled'=>1,	'indx'=>0,	'obj_name'=>'Карта сайта') ;
   	create_table_by_pattern($pattern) ;

    // баннеры -------------------------------------------------------------------------------------------------------------------
   	$pattern=array();
   	$pattern['table_name']		= 	TM_BANNERS ;
	$pattern['table_clss']		= 	101 ;
	$pattern['table_parent']	= 	$id_DOT;
    $pattern['table_title']	    = 	'Банеры' ;
    $pattern['use_clss']		=	'1,22' ;
    $pattern['def_recs'][]	    =	array ('parent'=>0,	'clss'=>1,	'enabled'=>1,	'indx'=>0,	'obj_name'=>'Банеры') ;
   	create_table_by_pattern($pattern) ;

   	// меню сайта  -------------------------------------------------------------------------------------------------------------------
    $pattern=array();
   	$pattern['table_name']		= 	TM_SITE_MENU ;
	$pattern['table_clss']		= 	101 ;
	$pattern['table_parent']    = 	$id_DOT;
    $pattern['table_title']	    = 	'Меню сайта' ;
    $pattern['use_clss']		=   '1,97' ;
    $pattern['def_recs'][]	    =	array ('parent'=>0,	'clss'=>1,	'enabled'=>1,	'indx'=>0,	'obj_name'=>'Меню сайта') ;
   	create_table_by_pattern($pattern) ;

    // списки на сайте  -------------------------------------------------------------------------------------------------------------------
    $pattern=array() ;
    $pattern['table_name']		= 	TM_LIST ;
    $pattern['table_clss']		= 	101 ;
    $pattern['table_parent']	= 	$id_DOT;
    $pattern['table_title']	    = 	'Списки на сайте' ;
    $pattern['use_clss']		=   '1,20,21' ;
    $pattern['def_recs'][]	    =	array ('parent'=>0,	'clss'=>1,	'enabled'=>1,	'indx'=>0,	'obj_name'=>'Списки на сайте') ;
    create_table_by_pattern($pattern) ;

    // журнал событий на сайте  -------------------------------------------------------------------------------------------------------------------
   	$pattern=array( 'table_name'		=> 	TM_LOG_EVENTS,
					'use_clss'			=> 	'40',
					'table_clss'		=> 	'108',
					'table_title'		=> 	'Журнал событий',
					'table_parent'		=> 	$id_DOT) ;
   	create_table_by_pattern($pattern) ;

    // журнал страниц сайта  -------------------------------------------------------------------------------------------------------------------
   	$pattern=array( 'table_name'		=> 	TM_LOG_PAGES,
					'use_clss'			=> 	'50',
					'table_clss'		=> 	'108',
					'table_title'		=> 	'Журнал страниц',
					'table_parent'		=> 	$id_DOT) ;
   	create_table_by_pattern($pattern) ;

    // журнал запросов в поиске по сайту  -------------------------------------------------------------------------------------------------------------------
   	$pattern=array( 'table_name'		=> 	TM_LOG_SEARCH,
					'use_clss'			=> 	'44',
					'table_clss'		=> 	'108',
					'table_title'		=> 	'Журнал поисковых запросов',
					'table_parent'		=> 	$id_DOT) ;
   	create_table_by_pattern($pattern) ;

   	// Журнал переходов по баннерам   -------------------------------------------------------------------------------------------------------------------
    $pattern=array( 'table_name'		=> 	TM_LOG_BANNERS,
					'use_clss'			=> 	'45',
					'table_clss'		=> 	'108',
					'table_title'		=> 	'Журнал переходов по баннерам',
					'table_parent'		=> 	$id_DOT) ;
   	create_table_by_pattern($pattern) ;

   	// Журнал ошибок на сайте   -------------------------------------------------------------------------------------------------------------------
    $pattern=array( 'table_name'		=> 	TM_LOG_ERRORS,
					'use_clss'			=> 	'51',
					'table_clss'		=> 	'108',
					'table_title'		=> 	'Журнал ошибок',
					'table_parent'		=> 	$id_DOT) ;
   	create_table_by_pattern($pattern) ;

    if (is_object($_SESSION['events_system'])) $_SESSION['events_system']->log_enabled=1 ;
 }

?>