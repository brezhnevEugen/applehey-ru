<?  include_once('clss_101.php') ;
class clss_108 extends clss_101
{

    /* СОЗДАНИЕ журнальной таблицы ----------------------------------------------------------------------------------------------------------------------------- */
     function obj_create_dialog()
     { global $obj_info ;
       ?><div class=alert>Переделать под вызов функции через AJAX</div><?
       return ;
       damp_array($_POST) ;
       $form_id='form_obj_create_dialog_clss_'.$this->clss ;
       if ($obj_info['clss']!=103) { echo '<p class=alert>Журнальная таблица может быть создана только в разделе каталога</p><br>' ;
                                     return('ok') ;
                                   }
       $set_table_base='log_'.$obj_info['table_name'].'_' ;
       ?><form method="POST" action="ajax.php" id=<?echo $form_id?>>
          <input name="set_table_base" type="hidden" value="<?echo $set_table_base?>">
    	  <table cellspacing=0 cellpadding=0>
    	           <tr class=clss_header><td>Создание журнальной таблицы</td></tr>
    	           <tr class=td_header>
    	             <td>Класс записей</td>
    	           </tr>
                   <tr>
    	                <td><select size="1" name="use_clss" class='text'>
    	          						        <? if (sizeof($_SESSION['descr_clss'])) foreach ($_SESSION['descr_clss'] as $clss=>$rec) if ($rec['parent']==-1)
    	          						            { ?><option value="<?echo $clss?>"><?echo $rec['name']?></option><? }
    	          						        ?>
    								  		  </select>
    					</td>
         		   </tr>
    	     </table>
    	     <button class=button cmd=clss_108_obj_create_apply>Создать</button>
    	     <button class=button>Отменить</button>&nbsp;&nbsp;&nbsp;
          </form>
      <?
     }

     function create_obj_after_dialog()
     { global $use_clss ;
       // ПРОВЕРЯТЬ! после изменения параметров функции create_DOT_object_table не проверялось
       $new_table_options=array(  'table_parent'  =>$_POST['pkey'],
                                  'table_clss'    =>108,
                                  'table_title'   =>$_SESSION['descr_clss'][$use_clss]['table_name'],
                                  'table_name'    =>$_SESSION['descr_clss'][$use_clss]['table_code'],
                                  'use_clss'      =>$use_clss
                               ) ;

       //create_DOT_object_table($_POST['pkey'],$_SESSION['descr_clss'][$use_clss]['table_name'],$set_table_base.$_SESSION['descr_clss'][$use_clss]['table_code'],108,'',$use_clss) ;
       create_DOT_object_table($new_table_options) ;
     }

    // просмотр журнала - в редакторе таблиц
     function show_props($obj_info,$options=array())
     {	global $_stat_mode ;
        if ($options['mode']) $mode=$options['mode'] ;
     	if ($_stat_mode) $mode=$_stat_mode ;
     	list($clss,$tkey)=each(_DOT($obj_info['pkey'])->list_clss) ;
     	?><input name="_stat_mode" type="radio" value="1" <?if ($mode==1) echo 'checked' ?>>За сегодня&nbsp;&nbsp;&nbsp;
     	  <input name="_stat_mode" type="radio" value="2" <?if ($mode==2) echo 'checked' ?>>За неделю&nbsp;&nbsp;&nbsp;
     	  <input name="_stat_mode" type="radio" value="3" <?if ($mode==3) echo 'checked' ?>>За месяц&nbsp;&nbsp;&nbsp;
     	  <input name="_stat_mode" type="radio" value="4" <?if ($mode==4) echo 'checked' ?>>За год&nbsp;&nbsp;&nbsp;
     	  <input name="_stat_mode" type="radio" value="5" <?if ($mode==5) echo 'checked' ?>>Все&nbsp;&nbsp;&nbsp;
     	<button onclick="exe_cmd('ok')">Показавать</button><?

        // формируем условие отбора записей
        $now=getdate() ;
        $mode=1 ;
        switch ($mode)
        { case 1: $start_time=mktime(0, 0, 0, $now['mon'],$now['mday'],$now['year']); // начало дня
                  $usl='c_data>='.$start_time ;
                  $title=' за сегодня ' ;
            	  break;
          case 2: $start_time=time()-(($now['wday'])? ($now['wday']-1)*86400:7*86400) ;// начало недели
                  $usl='c_data>='.$start_time ;
                  $title=' за неделю ' ;
          		  break ;
          case 3: $start_time=mktime(0, 0, 0, $now['mon'],1,$now['year']); // начало месяца
                  $usl='c_data>='.$start_time ;
                  $title=' за месяц ' ;
            	  break;
          case 4: $start_time=mktime(0, 0, 0, 1,1,$now['year']); // начало года
                  $usl='c_data>='.$start_time ;
                  $title=' за год ' ;
            	  break;

          default: $usl='' ; // все

        }
        //echo $usl.'<br>' ;
        $title="Журнал '"._DOT($obj_info['pkey'])->/** @noinspection PhpExpressionResultUnusedInspection */name."' ".$title ;

     	$list_recs=execSQL('select * from '._DOT($obj_info['pkey'])->table_name.' where '.$usl.' order by c_data desc,pkey desc') ;

        set_reffer_values($list_recs,40) ;

        $this->show_based_obj_list($list_recs,array('read_only'=>1,'clss'=>$clss,'no_check'=>1,'title'=>$title,'all_enabled'=>1,'no_htmlspecialchars'=>1)) ;
     }


}

?>