<?    // запись в списке
include_once('clss_0.php') ;
class clss_56 extends clss_0
{
    function _show_list_items($table_id,$usl='',$options=array())
      {
         include_once(_DIR_TO_ENGINE.'/admin/i_clss_func.php') ;
         $options_select=array() ; $next_page=0 ;
                          list($tkey,$table_name)=check_table_tkey($table_id) ; // table_id может быть как имя таблицы, так и её код или название подсистемы
                         // определяем имя и код таблицы, где храняться объекты отображаемого класса
                         $clss_table_id=_DOT($tkey)->list_clss[$this->clss] ;
                         if (!$clss_table_id)
                         { echo '<div class=alert>Объекты класса '.$this->clss.' не поддерживаются текущей таблицей</div>' ;
                           return(array()) ;
                         }
                         $options=array_merge($this->view['list']['options'],$options) ;
                         //damp_array($options) ;

                         //echo '$this->clss='.$this->clss.'<br>' ;
                         //echo '$clss_table_id='.$clss_table_id.'<br>' ;
                         $clss_table_name=$_SESSION['tables'][$clss_table_id] ; //echo '$clss_table_name='.$clss_table_name.'<br>' ;
                         // готовим запрос для выборки объектов
                         $usl_select='clss='.$this->clss ;
                         if ($usl) $usl_select.=' and '.$usl ;
                         if ($options['usl_filter']) $usl_select.=' and '.$options['usl_filter'] ;

                         //echo 'usl='.$usl.'<br>' ;
                         //echo 'usl_filter='.$options['usl_filter'].'<br>' ;


                         if (!$options['default_order']) $options['default_order']='indx asc' ;
                         list($def_order,$def_mode)=explode(' ',$options['default_order']) ;
                         if (!$_SESSION['table_order'][$tkey][$this->clss]['order_by']) $_SESSION['table_order'][$tkey][$this->clss]['order_by']=$def_order ;
                         if (!$_SESSION['table_order'][$tkey][$this->clss]['order_mode']) $_SESSION['table_order'][$tkey][$this->clss]['order_mode']=$def_mode ;
                         //damp_array($options) ; echo '---------------------------------------------------------' ;
                         //damp_array($_SESSION['table_order'][$tkey][$this->clss]) ; echo '---------------------------------------------------------' ;
                         if ($options['order']) list($options['order_by'],$options['order_mode'])=explode(' ',$options['order']) ;

                         if (!$options['order_by'])   $options['order_by']=$_SESSION['table_order'][$tkey][$this->clss]['order_by'] ;
                         if (!$options['order_mode']) $options['order_mode']=$_SESSION['table_order'][$tkey][$this->clss]['order_mode'] ;

                         $options_select['order_by']=$options['order_by'].' '.$options['order_mode'] ;

                         // делаем запрос на общее число объектов без разбиения на страницы
                         $count_recs=execSQL_value('select count(pkey) from '.$clss_table_name.'  where '.$usl_select.$options_select['limit']) ; //

                         if ($options['count']) // если задано число выводимых строк, разбивку на страницы не производим
                         {  $from_pos=0 ;
                            if ($options['count']<$count_recs) $count_recs=$options['count'] ;  // если в базе более записей чем надо показать, то считаеи число записаей = $options['count']
                            $to_pos=$count_recs ;
                            $options_select['limit']=' limit '.$options['count'] ;
                         }
                         else // иначе по текущей страницы определяем параметры limit для запроса
                         {  $from_pos=($options['page']-1)*$this->count_obj_by_page ;
                            $to_pos=$from_pos+$this->count_obj_by_page ;
                            if ($to_pos>$count_recs) $to_pos=$count_recs ;
                            $options_select['limit']='limit '.$from_pos.','.$this->count_obj_by_page ;
                            $ostatok=$count_recs-$to_pos ;
                            $next_page=($to_pos<$count_recs)? ($options['page']+1):0 ;
                         }
                         echo '$usl_select='.$usl_select.'<br>' ;
                         if ($options['link_to']) $options_select['link_to']=$options['link_to'] ;
                         //damp_array($options_select) ;
                         //$options_select['debug']=1 ;
                         //damp_array($options_select) ;
                         $options_select['debug']=$options['debug'] ;

                         $list_items=select_db_recs($clss_table_id,$usl_select,$options_select) ;
                         //print_2x_arr($list_items) ;
                         $this->prepare_public_info_for_arr($list_items) ;
                         //print_2x_arr($list_items) ;

                         // если задана опция показывать свойства объекта вместо одной записи
                         if ($options['show_van_rec_as_item'] and sizeof($list_items)==1)
                         {  list($id,$rec)=each($list_items) ;
                            $this->show_props($rec) ;
                            return(array()) ;
                         }

                         $options['info_count_text']='1...'.$to_pos.' из '.$count_recs  ;


         ?><iframe id=demo_frame src="/blank.php?template_id=<?echo implode(',',array_keys($list_items))?>" style="width:100%;height:10000px;border:none;margin-top:20px;"></iframe><?

                         ///damp_array($list_items) ;
                         if ($options['only_tr_content']) $this->show_based_obj_list($list_items,$options) ;   // будет вызвана функция show_based_obj_list($list_obj,$options) ;
                         else
                         {   $list_id='list_'.rand(1,1000000) ;
                             ?><div class="list_item" id="<?echo $list_id?>" tkey="<?echo $tkey?>" clss="<?echo $this->clss?>" usl="<?echo $usl?>" usl_filter="<?echo $options['usl_filter']?>" list_id="<?echo $list_id?>"  read_only="<?echo $options['read_only']?>" no_check="<?echo $options['no_check']?>" no_icon_edit="<?echo $options['no_icon_edit']?>" no_view_field="<?echo $options['no_view_field']?>" no_icon_photo="<?echo $options['no_icon_photo']?>" order_by="<?echo $options['order_by']?>" order_mode="<?echo $options['order_mode']?>" count="<?echo $options['count']?>"><?
                                if (sizeof($list_items))
                                  { if ($count_recs>50 and sizeof($options['buttons'])) { $this->show_buttons($options) ; echo '<br>' ; }
                                    $this->show_based_obj_list($list_items,$options) ;   // будет вызвана функция show_based_obj_list($list_obj,$options) ;
                                    if ($next_page) {?><div class=next_page><input type="button" class=view_page page="<?echo $next_page?>" value="Еще <?echo $ostatok?> строк"></div><?}
                                    // показываем кнопки списка
                                    //damp_array($options) ;
                                    if (sizeof($options['buttons'])) $this->show_buttons($options) ;
                                  }
                                else echo '<div class=empry_list>Ничего не найдено</div>' ;
                             ?></div><?
                         }
                         if ($to_pos>$count_recs) $to_pos=$count_recs ;
                         $result=array('from'=>$from_pos+1,'to'=>$to_pos,'all'=>$count_recs,'next_page'=>$next_page) ;
                         if (sizeof($list_items)==1 and $options['return_cur_rec'])
                         { reset($list_items) ;
                           list($id,$rec)=each($list_items) ;
                           $result['cur_rec']=$rec ;
                         }
             return($result) ;

      }

}

?>