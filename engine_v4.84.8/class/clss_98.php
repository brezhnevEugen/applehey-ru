<?
include_once('clss_0.php') ;
class clss_98 extends clss_0
{

    function obj_create_dialog()
         { $form_id='form_obj_create_dialog_clss_'.$this->clss ;
           ?><form method="POST" action="ajax.php" id=<?echo $form_id?>>
                 <table class="debug left">
                    <tr><td>Имя сотрудника</td><td><input type=text class="text" name=new_obj[obj_name]></td></tr>
                    <tr><td>Email</td><td><input type=text class="text" name=new_obj[email]></td></tr>
                    <tr><td>Логин</td><td><input type=text class="text" name=new_obj[login]></td></tr>
                    <tr><td>Пароль</td><td><input type=text class="text" name=new_obj[password]></td></tr>
                 </table>
                 <p>После создания учетной записи для нового сотрудника Вы сможете задать права доступа к панели управления сайтом</p>
                 <button class=button_green mode=append_viewer_main cmd=create_obj_after_dialog clss="<?echo $this->clss?>" parent_reffer="<?echo $_POST['parent_reffer']?>">Добавить</button>
                 <? show_select_action('after_clss_'.$_POST['clss'].'_create_action',array('go_list'=>'перейти к списку аккаунтов','go_this'=>'перейти к созданному аккаунту','go_create'=>'создать еще один аккаунт')) ;?>

             </form>
           <?
           return(0) ;
         }

  // создаем объект нужного класса
  function obj_create(&$finfo,$options=array())
  { $obj_info=parent::obj_create($finfo,$options) ;
    echo 'Создан аккаунт "<span class=green>'.$obj_info['obj_name'].'</span>"<br>' ;
    return($obj_info) ;
  }

    function panel_menu()
    {
       global $obj_info ;// damp_array($obj_info) ;
       if ($obj_info['login']!='admin' and $obj_info['login']!='root')
       { $cur_member=new c_member($obj_info) ;
         $_SESSION['admin_account_system']->reboot_member($cur_member) ;
         //$_SESSION['admin_account_system']->get_info_from_db_by_login($obj_info['login'],$cur_member) ;
         //$_SESSION['admin_account_system']->reload_menu($cur_member) ;
         //damp_array($cur_member) ;
         ?><table id=admin_main_menu><tr><td id=main_menu><?$_SESSION['admin_account_system']->show_menu($_SESSION['member'],$cur_member) ;?></td><tr></table><?
         ?><button cmd=save_member_menu_item>Сохранить меню</button><?
       }
       else echo '<div class=alert>Меню для данного аккаунта задается в /ini/init.php</div>' ;

    }

    function panel_pass()
    {
       global $obj_info ;
       ?><h1>Смена пароля для входа в панель управления сайтом</h1><?
       //damp_array($_POST) ;
       $new_usr_pass=$_POST['new_usr_pass'] ;
       if ($new_usr_pass)
       {   	//$new_usr_pass = "5207403";
            $path_t = mb_substr(md5(uniqid ($new_usr_pass)),0,3). "-56";
            $new_pass = crypt ($new_usr_pass, $path_t);
            $accounts=$this->get_htpassword_values(_DIR_TO_ROOT.'/'._ADMIN_.'/.htpasswd') ;
            $accounts[$obj_info['login']]=$new_pass ;
            $this->set_htpassword_values(_DIR_TO_ROOT.'/'._ADMIN_.'/.htpasswd',$accounts) ;
            if ($obj_info['email'])
            { $text='Уважаемый(ая) '.$obj_info['obj_name'].'!<br><br>' ;
              $text.='Вам был назначен новый пароль для доступа к админке сайта '._MAIN_DOMAIN.'<br>' ;
              $text.='Адрес:<strong><a href="'._PATH_TO_ADMIN.'/">'._PATH_TO_ADMIN.'/</a></strong><br>' ;
              $text.='Логин:<strong> '.$obj_info['login'].'</strong><br>' ;
              $text.='Пароль:<strong> '.$new_usr_pass.'</strong><br><br>' ;
              $text.='Смену пароля произвел <strong> '.$_SESSION['member']->name.' c IP '.IP.'</strong><br>' ;

              _send_mail($obj_info['email'],'Смена пароля в админку для сайта '._MAIN_DOMAIN,$text) ;

              echo '<span class=green>На адрес '.$obj_info['email'].' отправлено уведомление о смене пароля</span><br><br>';
              echo 'Текст сообщения:<br><br>'.$text ;

            }
            ?><br><button cmd=panel_pass>Ok</button><?
            //damp_array($accounts);

            //echo _DIR_TO_ROOT.'/admin/.htpasswd<br>' ;
            //echo $cont.'<br>';
       }
       else {?><input name="new_usr_pass" type="text" value="" class=text><button cmd=panel_pass>Сменить</button><?}
    }

    function get_htpassword_values($fname)
    {   $new_arr=array() ;
        $cont=file_get_contents($fname) ;
        if (!sizeof($cont)) return ;
        $arr=explode(chr(13).chr(10),$cont) ;
        if (sizeof($arr)) foreach($arr as $rec)
        { list($key,$value)=explode(':',$rec) ;
          $new_arr[$key]=$value ;
        }
        return($new_arr);
    }

    // эти функции должны находиться в классе admin_account_system
    function set_htpassword_values($fname,$arr)
    {   $new_arr=array() ;
        if (sizeof($arr)) foreach($arr as $key=>$value) $new_arr[]=$key.':'.$value ;
        $cont=implode(chr(13).chr(10),$new_arr) ;
        $cnt=file_put_contents($fname,$cont) ;
        if (!$cnt) echo 'Не получилось сохранить данные в файл<br>' ;
        else       echo 'Данные успешно сохранены.<br>' ;
    }

    function save_member_menu_item()
    { global $menu_item_check,$obj_info ;
      $sql='delete from '.$_SESSION['admin_account_system']->table_name.' where parent='.$obj_info['pkey'] ;
      if (!$result = mysql_query($sql)) SQL_error_message($sql,'',1) ;
      if (sizeof($menu_item_check)) foreach($menu_item_check as $item_href=>$item_value)
       adding_rec_to_table($_SESSION['admin_account_system']->table_name,array('parent'=>$obj_info['pkey'],'clss'=>0,'obj_name'=>$item_href),array('debug'=>0)) ;
      return('ok') ;
    }

    function update_member_info(&$member,$rec)
    {   $this->prepare_public_info($rec) ;
        $member->info=$rec ;
        $member->id=$rec['pkey'] ;
        $member->reffer=$rec['_reffer'];
        $member->parent=$rec['parent'] ;
        $member->login=$rec['login'] ;
        $member->name=$rec['__name'];
        $member->contact_name=$rec['__contact_name'] ;
        $member->dop_info=$rec['__info'] ;   // временно, нет смысла дублировать информацию
    }

}
?>