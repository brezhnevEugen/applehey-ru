<?    // запись в списке
include_once('clss_0.php') ;
class clss_20 extends clss_0
{
  // создаем объект нужного класса
  function obj_create($finfo,$options=array())
  { //$options['debug']=1 ;
    // вызываем базовую функцию создания объекта
    if (!$finfo['tkey'] and !$options['parent_reffer']) $finfo['tkey']=_DOT(TM_LIST)->pkey ;
    $obj_info=parent::obj_create($finfo,$options) ;
    // небоходимо присвоить значение id
    $last_id=execSQL_value('select max(id) from '._DOT($obj_info['tkey'])->table_name.' where parent='.$obj_info['parent']) ;
    $last_id++ ;
    update_rec_in_table($obj_info['tkey'],array('id'=>$last_id),'pkey='.$obj_info['pkey']) ;
    $obj_info['id']=$last_id ;
    // после добавления записи в список обновляем список в сессии
    // может быть заблокировано, если создается набор записей списка и далее будет единое обновление данных списка
    if (!$options['no_save_to_session']) $this->after_save_rec($obj_info['tkey'],$obj_info['pkey']) ;
    if ($options['return_id']) return($obj_info['id']) ;
    else return($obj_info) ;
  }

  // при удалении записи из списка, также удаляем запись в сессии
  function on_delete_event($obj_info,$options=array())
  { $arr_name=$_SESSION['index_list_names'][$obj_info['parent']] ;
    unset($_SESSION[$arr_name][$obj_info['id']]) ;
    parent::on_delete_event($obj_info,$options) ;
  }

  // -----------------------------------------------------------------------------------------------------------------------------------------------------------------------
  //
  //  ВЫВОД ОБЪЕКТОВ КЛАССА В VIEWER - показ списка дочерних объектов
  //
  // -----------------------------------------------------------------------------------------------------------------------------------------------------------------------

  function before_view_rec(&$rec,$options=array())
  { if (!isset($_SESSION['index_list_names'][$rec['parent']])) return ;
    $arr_name=$_SESSION['index_list_names'][$rec['parent']] ;
    $rec_list=$_SESSION[$arr_name][$rec['id']] ;
    if (is_array($rec_list) and sizeof($rec_list)) $rec=array_merge($rec,$rec_list) ;
  }

  // действие после сохранение записи из списка в БД - обновляем список в сессии
  function after_save_rec($tkey,$pkey)
  { $rec=select_db_obj_info($tkey,$pkey) ;
    //$arr[$rec['id']]=$rec ;
    $arr_name=$_SESSION['index_list_names'][$rec['parent']] ;
    // создаем класс и перезагружаем данные списка в сессии
    _IL($rec['parent'])->init($rec) ;
    $_SESSION[$arr_name][$rec['id']]=$rec ;
  }

  function prepare_public_info(&$rec)
  {
      $name=$rec['obj_name'] ;
      $name=trim(strip_tags($name))  ;   // удаляем начальные и конечные пробелы и HTML-теги
      $name=str_replace(array("\n"),array(''),$name) ; // удяляем переносты строк
      if (!$name) $name='# '.$rec['pkey'] ;
      $rec['_in_tree_name']=$name ;
      $rec['_in_tree_child_cnt']=0 ;
      if (isset($this->parent_to) and !sizeof($this->parent_to)) $rec['_in_tree_child_cnt']=0 ;
  }

  function get_key_field()  { return('id') ;  }
}

?>