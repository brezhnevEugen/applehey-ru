<?php
include_once(_DIR_TO_ENGINE."/admin/i_clss_func.php") ;
include_once(_DIR_TO_ENGINE.'/admin/c_menu_tabs.php') ;

class c_file_uploader extends c_editor_obj
{
  function site_top_head(&$options=array())
  { if ($this->cmd!='quick_upload')
    {?><head>
        <title>Загрузчик файлов</title>
        <META http-equiv=Content-Type content="text/html; charset=utf-8" />
        <META NAME="Document-state" CONTENT="Dynamic" />
        <META NAME="revizit-after" CONTENT="5 days" />
        <META name="revisit"  content="5 days" />
        <script type="text/javascript" src="<?echo _PATH_TO_ADMIN_ADDONS?>/jquery-1.8.3.min.js"></script>
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/mootools/1.2.2/mootools.js"></script>
        <link rel="stylesheet" type="text/css" href="<?echo _PATH_TO_ADMIN_ADDONS?>/fancyupload-v3.0.1/fancyupload.css" media="screen, projection">
        <script type="text/javascript" src="<?echo _PATH_TO_ADMIN_ADDONS?>/fancyupload-v3.0.1/source/Swiff.Uploader.js"></script>
        <script type="text/javascript" src="<?echo _PATH_TO_ADMIN_ADDONS?>/fancyupload-v3.0.1/source/Fx.ProgressBar.js"></script>
        <script type="text/javascript" src="<?echo _PATH_TO_ADMIN_ADDONS?>/fancyupload-v3.0.1/source/FancyUpload2.js"></script>
        <script type="text/javascript" src="<?echo _PATH_TO_ADMIN_ADDONS?>/fancyupload-v3.0.1/fancyupload2.js"></script>
        <link rel="stylesheet" type="text/css" href="<?echo _PATH_TO_ENGINE?>/admin/style.css">
      </head>
      <script type="text/javascript">var _PATH_TO_ADMIN_ADDONS='<?echo _PATH_TO_ADMIN_ADDONS?>' ;</script>
    <?}
  }

 function body($options=array())
 { if ($this->cmd=='quick_upload') { $this->ck_editor_upload_files() ; return ;}  // стыковка с редактором ckeditor
   ?><body><div id=wrapper><? $this->block_main(); ?></div></body><?
 }

function block_main()
    { ?><div class=upload_trace><?
      $_POST['show_comment']=1 ;
      if (sizeof($_FILES['upload_file']['name'])) $this->upload_files($_FILES['upload_file']) ;
      if (sizeof($_POST['upload_urls'])) $this->upload_files_by_href($_POST['upload_urls']) ;
      ?></div><?

      $active_tab=($_POST['active_tab'])? $_POST['active_tab']:0;
      ?><script src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
        <link rel="stylesheet" href="http://code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css" />
        <div id="tabs">
          <ul><li><a href="#tabs-1">Файлы с диска</a></li>
              <!--<li><a href="#tabs-2">Один файл</a></li>-->
              <li><a href="#tabs-3">Загрузка по ссылкам</a></li>
          </ul>
          <div id="tabs-1"><?$this->panel_fancyupload() ;?></div>
          <?/*?><div id="tabs-2"><?$this->panel_files_upload(); ?></div><?*/?>
          <div id="tabs-3"><?$this->panel_links_upload(); ?></div>
        </div>
        <script type="text/javascript">$j(function() {$j( "#tabs" ).tabs({active:<?echo $active_tab?>});});</script>
        <style type="text/css">
            div.upload_trace{font-size:10px;margin:10px;}
            textarea{width:420px;height:100px;margin-bottom:10px;}
            ol#group_elements img{position:relative;top:3px;}
            .ui-tabs{padding:0;}
            .ui-widget-content{border:none;}
        </style>
      <?
    }

    function panel_upload_alert()
    { $max_upload = (int)(ini_get('upload_max_filesize')) ;
      $max_post = (int)(ini_get('post_max_size')) ;
      $memory_limit = (int)(ini_get('memory_limit'));
      $upload_mb = min($max_upload, $max_post, $memory_limit);
      echo 'Внимание! Максимальный размер файла, который Вы можете загрузить - <span class=green>'.$upload_mb.' Мб</span><br>' ;
      switch($_GET['mode'])
        { case 'obj_img': echo 'Поддерживаются форматы файлов: jpg, gif, png. Можно сразу несколько файлов, упакованных в zip.<br>' ; break ;

        }
    }

    function panel_fancyupload()
    { $this->panel_upload_alert() ;
      ?><form name="form" id="form-upload" method="post" enctype="multipart/form-data" action="<?echo _PATH_TO_ENGINE?>/admin/fancyupload.php">
            <input type="hidden" name=active_tab value=0>
            <?  if ($_GET['mode']) {?><input type=hidden name=mode id=mode value="<?echo $_GET['mode']?>"><?}
                if ($_GET['reffer']) {?><input type=hidden name=reffer id=reffer value="<?echo $_GET['reffer']?>"><?}
               if ($_GET['clss']) {?><input type=hidden name=clss id=clss value="<?echo $_GET['clss']?>"><?}
            ?>
            <div id="panel_fancyupload" class=hide>
                       <fieldset id="upload-fallback">
                           <legend>Загрузка файлов</legend>
                           <p>Подождите, идет запуск загрузчика файлов... </p>
                           <label for="upload-photoupload">Загрузка файла:<input type="file" name="Filedata" /></label>
                       </fieldset>
                       <div id="upload-status" class="hide">
                           <div id=menu_panel>
                               <img src="<?echo _PATH_TO_ADMIN_ADDONS?>/fancyupload-v3.0.1/assets/select_file.png"> <a href="#" id="upload-browse">Выбрать файлы</a>
                               <!--<img src="<?echo _PATH_TO_ADMIN_ADDONS?>/fancyupload-v3.0.1/assets/upload_file.png"> <a href="#" id="upload-upload">Загрузить на сервер</a>-->
                               <br>
                           </div>
                           <div><strong class="overall-title"></strong><br /><img src="<?echo _PATH_TO_ADMIN_ADDONS?>/fancyupload-v3.0.1/assets/progress-bar/bar.gif" class="progress overall-progress" /></div>
                           <div><strong class="current-title"></strong><br /><img src="<?echo _PATH_TO_ADMIN_ADDONS?>/fancyupload-v3.0.1/assets/progress-bar/bar.gif" class="progress current-progress" /></div>
                           <div class="current-text"></div>
                       </div>
                       <ul id="upload-list"></ul>
           	    </div>
              </form>
             <?
    }


  function panel_files_upload()
  {
    $this->panel_upload_alert() ;

    ?><form name="form" id="form-upload" method="post" enctype="multipart/form-data" action="">
        <input type="hidden" name=active_tab value=1>
        <br><ol id=group_elements>
            <li class=item><input name="upload_file[]" type=file class=source size=50><img class=del src="<? echo _PATH_TO_ADMIN_IMG?>/off2.gif"></li>
            <li class=item><input name="upload_file[]" type=file class=source size=50><img class=del src="<? echo _PATH_TO_ADMIN_IMG?>/off2.gif"></li>
            <li class=item><input name="upload_file[]" type=file class=source size=50><img class=del src="<? echo _PATH_TO_ADMIN_IMG?>/off2.gif"></li>
          </ol>
        <br>
        <div class=panel_button>
          <button cmd=upload_files>Загрузить</button>
          <button id=append_file>Еще файл</button>
         <!--<input name="show_comment" type="checkbox" value="1" <?if ($_POST['show_comment']) echo 'checked'?>>Показать процесс загрузки-->
        </div>
        <script type="text/javascript">
            $j('button#append_file').click(function(){$j('ol#group_elements').append('<li class=item><input name="upload_file[]" type=file class=source size=50><img class=del src="<? echo _PATH_TO_ADMIN_IMG?>/off2.gif"></li>');return(false);});
            $j('img.del').click(function(){$j(this).parent().remove() ;});
        </script>
       </form>
    <?
  }

  function panel_links_upload()
  {  $this->panel_upload_alert() ;
     ?><br>Укажите ссылки на файлы, которые Вам необходмо загрузить. <strong>Каждый новый файл - с новой строки.</strong><br><?

     ?><form name="form" id="form-upload" method="post" enctype="multipart/form-data" action="">
          <input type="hidden" name=active_tab value=2>
          <br><textarea name=upload_urls class=text></textarea><br>
          <div class=panel_button>
               <button id=upload_button onclick="exe_cmd('upload_files')">Загрузить</button>
               <!--<input name="show_comment" type="checkbox" value="1" <?if ($_POST['show_comment']) echo 'checked'?>>Показать процесс загрузки-->
          </div>
       </form>
     <?
  }

  function upload_files($file_info)
  {  $options=array() ;
     //$options['debug']=1 ;
     $options['view_upload']=$_POST['show_comment'] ;
     $options['return_clone_url']='small' ;
     $options['clss']=$_GET['clss'] ;

	 if (sizeof($file_info['name'])) foreach($file_info['name'] as $indx=>$upld_name_file) if (!$file_info['error'][$indx] and $file_info['name'][$indx])
	      {	// готовим инфу для загрузки
			$cur_file_info['name']		=$file_info['name'][$indx];
			$cur_file_info['type']		=$file_info['type'][$indx];
			$cur_file_info['tmp_name']	=$file_info['tmp_name'][$indx];
			$cur_file_info['error']		=$file_info['error'][$indx] ;
			$cur_file_info['size']		=$file_info['size'][$indx] ;
            // вызываем стандартную функцию для загрузки фото - в зависимости что именно загружается
            switch($_GET['mode'])
            { case 'obj_img':  $result[$indx]['img_url']  = obj_upload_image($_GET['reffer'],$cur_file_info,$options) ;
                               // получаем инфу по изображению
                               $img_info = @getimagesize(_DIR_TO_ROOT.'/'.str_replace('/small/','/source/',$result[$indx]['img_url']));
                               if (sizeof($img_info))
                               { $result[$indx]['width']    = $img_info[0];
                                 $result[$indx]['height']   = $img_info[1];
                                 $result[$indx]['mime']     = $img_info['mime'] ;
                               }
                               break ;
              case 'obj_file': $obj_rec=_CLSS($_GET['clss'])->obj_create(array(),array('parent_reffer'=>$_GET['reffer'])) ;
                               obj_upload_file($obj_rec['_reffer'],$cur_file_info,$options) ;
                               break ;
              case 'obj_banner': // создаем банер, загружаем изображение (флеш, видео), прописываем код в объекте для показа банера
                                if ($_GET['reffer']) $obj_rec=get_obj_info($_GET['reffer']) ; //  получаем инфу по коду выбранного объекта
                                if ($obj_rec['clss']!=$_GET['clss'])  $obj_rec=_CLSS($_GET['clss'])->obj_create(array(),array('parent_reffer'=>$_GET['reffer'])) ; // если это не баннер, создаем новый объект "баннер"
                                $new_file_name=upload_file($cur_file_info,'/public/',array('view_upload'=>1,'debug'=>0)) ;
                                include_extension('get_file_view_text')  ;
                                $text=get_file_view_text($new_file_name,array('flash_width'=>200,'use_preview'=>1)) ;
                                /*?><textarea><? echo $text ; ?></textarea><?*/
                                if ($text) update_rec_in_table($obj_rec['tkey'],array('value'=>$text),'pkey='.$obj_rec['pkey']) ;
                                break ;

            }

          }
     //damp_array($result) ;
  }

  function upload_files_by_href($urls)
  {  $arr_url=explode("\n",$urls) ;
     if (sizeof($arr_url)) foreach($arr_url as $href_file) if (trim($href_file))
	      {	$href_file=trim($href_file) ;
            $href_file=urldecode($href_file) ;
	        // создаем объект - фото или файл
            //$obj_rec=_CLSS($_GET['clss'])->obj_create(array(),array('parent_reffer'=>$_GET['reffer'])) ;

            switch($_GET['mode'])
            { case 'obj_img':   list($file_reffer,$file_dir)=obj_upload_image_by_href($_GET['reffer'],$href_file,array('use_dir'=>'source/','clss_file'=>$_GET['clss'])) ;
                                /*
                                list($file_reffer,$file_dir)=obj_upload_file_by_href($obj_rec['_reffer'],$href_file,array('use_dir'=>'source/','clss_file'=>3)) ;
                                if (!$file_dir) echo '<div class=alert>Не удалось загрузить файл "<strong>'.$href_file.'</strong>"</div>' ;
                                if ($file_dir)
                                {   echo '<div class=green>Файл "<strong>'.$href_file.'</strong>" успешно загружен</div>' ;
                                    // проверяем, возможно ли создание клонов для данного типа изображений
                                    $check_file_dir=check_image_type($file_dir) ;
                                    // если создание клонов возможно и не запрещено - создаем клоны
                                    list($file_pkey,$file_tkey)=explode('.',$obj_rec['_reffer']) ;
                                    if ($check_file_dir) create_clone_image($file_tkey,basename($check_file_dir),array('view_upload'=>$_POST['show_comment'])) ;
                                } */
                                break ;
              case 'obj_file':  $href_file=str_replace('ftp://','http://',$href_file) ;
                                $href_file=str_replace('public_html/','',$href_file) ;
                                $href_file=str_replace('www/htdocs/','',$href_file) ;
                                obj_upload_file_by_href($_GET['reffer'],$href_file,array('view_upload'=>$_POST['show_comment'])) ;
                                break ;
              case 'obj_banner': // создаем банер, загружаем изображение (флеш, видео), прописываем код в объекте для показа банера
                                 if ($_GET['reffer']) $obj_rec=get_obj_info($_GET['reffer']) ; //  получаем инфу по коду выбранного объекта
                                 if ($obj_rec['clss']!=$_GET['clss'])  $obj_rec=_CLSS($_GET['clss'])->obj_create(array(),array('parent_reffer'=>$_GET['reffer'])) ; // если это не баннер, создаем новый объект "баннер"
                                 $new_file_name=upload_file_by_href($href_file,'public/',array('view_upload'=>1,'debug'=>0)) ;
                                 include_extension('get_file_view_text')  ;
                                 $text=get_file_view_text($new_file_name,array('flash_width'=>200,'use_preview'=>1)) ;
                                 /*?><textarea><? echo $text ; ?></textarea><?*/
                                 if ($text) update_rec_in_table($obj_rec['tkey'],array('value'=>$text),'pkey='.$obj_rec['pkey']) ;
                                 break ;
            }
          }
      echo '<br>' ;
  }

  function ck_editor_upload_files() // фоновая загрузка файлов для редактора ckeditor
  {  global $debug_trace_files_upload_to_mail ;
     if ($_FILES['upload']['error']) { echo 'Ошибка при передаче файла в загрузчик:'.$_FILES['upload']['error'] ; return ;}
     //$code=$this->detect_cyr_charset($_FILES['upload']['name']) ;
     if ($debug_trace_files_upload_to_mail) ob_start() ;
     list($type,$sybtype)=explode('/',$_FILES['upload']['type']) ;
     if (!file_exists(_DIR_TO_PUBLIC.'/'.$type)) create_dir(_DIR_TO_PUBLIC.'/'.$type) ;
     $upload_file=upload_file($_FILES['upload'],_DIR_TO_PUBLIC.'/'.$type.'/',array('debug'=>$debug_trace_files_upload_to_mail,'source_is_utf8'=>0)) ;
     if ($debug_trace_files_upload_to_mail)
     { $debug_text=ob_get_contents() ; ob_end_clean() ; _send_mail(_EMAIL_ENGINE_ALERT,'debug file upload on '._BASE_DOMAIN.' from IP '.IP,$debug_text,array('no_reg_events'=>1,'_Content-type'=>'text/plain')) ;
     }
     $file_patch=_PATH_TO_SITE.hide_server_dir($upload_file) ;
     echo 'Загружен файл "<span class="bold green">'.basename($upload_file).'</span>"' ;
     ?><script type="text/javascript">window.parent.CKEDITOR.tools.callFunction(<?echo $_GET["CKEditorFuncNum"]?>,"<?echo $file_patch?>");</script><?

  }

}





?>