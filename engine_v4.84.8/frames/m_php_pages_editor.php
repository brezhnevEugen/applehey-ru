<?php
include_once(_DIR_TO_ENGINE."/admin/i_clss_func.php") ;
include_once(_DIR_TO_ENGINE.'/admin/c_menu_tabs.php') ;
include_once(_DIR_TO_ENGINE.'/admin/c_admin_page.php') ;

class c_php_pages_editor extends c_editor_obj
{
  function body($options=array())
  { $this->body_frame_3x_vert(array('fra_tree'=>_PATH_TO_ADMIN.'/php_pages_editor_tree.php','title'=>$options['title'])) ;
  }
}

class c_php_pages_editor_tree extends c_admin_page
{
  function body($options=array())
  {global $br_vers ;
   // исключаеим из поиска каталоги, где нельзя размещать информацию
   $no_index=get_no_index_dirs() ;
   $start_dir=_DIR_TO_ROOT.'/' ;
   $start_dir_id=hide_server_dir($start_dir) ;
   $start_dir_name=basename($start_dir) ;

   $use_filter=1 ;
   // получаем содержимое корневой директории
   $list_dir=get_dir_list($start_dir) ;
   ?><body id=tree>
     <div id=tree_menu></div>
     <?if (strpos($br_vers,'IE6')===false){?><div class=space_div></div><?}?>
     <script type=text/javascript >

      if (clss_icon[1]==undefined) { clss_icon[1]= new Array ; clss_icon[1][0]= new Array ; clss_icon[1][1]= new Array ; }
      if (clss_icon[2]==undefined) { clss_icon[2]= new Array ; clss_icon[2][0]= new Array ; clss_icon[2][1]= new Array ; }
      if (clss_icon[3]==undefined) { clss_icon[3]= new Array ; clss_icon[3][0]= new Array ; clss_icon[3][1]= new Array ; }

      clss_icon[1][1]['open'] ="<? echo _PATH_TO_ADMIN_IMG?>clss/folder_on_open.gif" ;
	  clss_icon[1][1]['close']="<? echo _PATH_TO_ADMIN_IMG?>clss/folder_on_close.gif" ;
	  clss_icon[1][0]['open'] ="<? echo _PATH_TO_ADMIN_IMG?>clss/folder_off_open.gif" ;
	  clss_icon[1][0]['close']="<? echo _PATH_TO_ADMIN_IMG?>clss/folder_off_close.gif" ;
      clss_icon[2][1]['open'] ="<? echo _PATH_TO_ADMIN_IMG?>clss/item_on.gif" ;
	  clss_icon[2][1]['close']="<? echo _PATH_TO_ADMIN_IMG?>clss/item_on.gif" ;
	  clss_icon[2][0]['open'] ="<? echo _PATH_TO_ADMIN_IMG?>clss/item_off.gif" ;
	  clss_icon[2][0]['close']="<? echo _PATH_TO_ADMIN_IMG?>clss/item_off.gif" ;
      clss_icon[3][1]['open'] ="<? echo _PATH_TO_ADMIN_IMG?>clss/item_on_ed.gif" ;
	  clss_icon[3][1]['close']="<? echo _PATH_TO_ADMIN_IMG?>clss/item_on_ed.gif" ;
	  clss_icon[3][0]['open'] ="<? echo _PATH_TO_ADMIN_IMG?>clss/item_off_ed.gif" ;
	  clss_icon[3][0]['close']="<? echo _PATH_TO_ADMIN_IMG?>clss/item_off_ed.gif" ;

      object[1]= new Array ;
      object[1]["<?echo $start_dir_id?>"] = new obj(1,1,"<?echo $start_dir_id?>",null,"<?echo $start_dir_name?>") ;
      object[1]["<?echo $start_dir_id?>"].expand_cmd=6 ;
      object[1]["<?echo $start_dir_id?>"].only_dirs=1 ;
      object[1]["<?echo $start_dir_id?>"].use_filter=<?echo $use_filter?> ;
      <?if (sizeof($list_dir)) foreach($list_dir as $name=>$dir) if (!$use_filter or ($use_filter and !in_array($dir,$no_index))){ echo 'object[1]["'.hide_server_dir($start_dir).'"].add_child(1,"'.hide_server_dir($dir).'","'.$name.'",1,1);';}?>
      object[1]["<?echo $start_dir_id?>"].show() ;
      var rn=Math.random();
      if (parent.fra_view!=null) parent.fra_view.location="<?echo _PATH_TO_ADMIN?>/php_pages_editor_viewer.php?fname="+root.pkey+"&rn="+rn ;
      function obj_click(tkey,pkey,cmd,evt)
        { var rn=Math.random();
          var obj=object[1][pkey] ;
          if (evt!=undefined) { evt.cancelBubble=true ; evt.returnValue=false ; }

          if (select_obj!=undefined)
          { var class_sel=/select/g ;
            var unsel_class=select_obj.className.replace(class_sel,'') ;
            select_obj.className=unsel_class ;
          }
          select_obj = document.getElementById('obj_'+obj.id);
          //$('div#obj_'+obj.id).addClass('select') ;
          //alert($('div#obj_'+obj.id).name) ;
          select_obj.className=select_obj.className+' select' ;
          select_obj_tree=obj ;
          if (parent.fra_view!=null) parent.fra_view.location="<?echo _PATH_TO_ADMIN?>/php_pages_editor_viewer.php?fname="+pkey+"&rn="+rn ;
        }

    </script>
    </body>
   <?
  }
}


class c_php_pages_editor_viewer extends c_admin_page
{

 function body(&$options=array()) { $options['body_class']='file_explorer'; $this->body_frame($options) ; }

 function select_obj_info()
 { global $fname,$obj_info ;
   //echo 'fname='.$fname.'<br>' ;
   if (is_dir(_DIR_TO_ROOT.$fname)) $obj_info=$this->analize_dir($fname) ;
   else if (is_file(_DIR_TO_ROOT.$fname)) $obj_info=$this->analize_page($fname) ;
   else $obj_info=array() ;
   //damp_array($obj_info);
   if (!is_object($_SESSION['top_menu'])) $_SESSION['top_menu']=new c_top_menu() ;
 }

 function analize_dir($fname)
 { $obj_info['type']='dir' ;
   $obj_info['obj_name']='Директория "'._PATH_TO_SITE.$fname.'"' ;
   $obj_info['patch']=_PATH_TO_SITE.$fname ;
   $obj_info['dir']=_DIR_TO_ROOT.$fname ;
   $obj_info['name']=$fname ;
   return($obj_info) ;
 }

 // адрес должен передаваться от корня сайта
 function analize_page($fname)
 { $obj_info['obj_name']=$fname ;
   $obj_info['type']='file' ;
   $obj_info['patch']=_PATH_TO_SITE.$fname ;
   $obj_info['dir']=_DIR_TO_ROOT.$fname ;
   $obj_info['file_name']=basename($fname) ;
   $obj_info['file_type']=mime_content_type($obj_info['dir']) ;
   if ($obj_info['file_type']=='text/html')
      {  $obj_info['config']=$this->parse_php_file($fname) ;
         $options=$this->exec_config($obj_info['config']['options']) ; //$options['debug']=1 ;
         $obj_info['options']=$options ;
         if (sizeof($obj_info['config']))
         {   $obj_info['patch_page']=_PATH_TO_SITE.$fname ;
             $class_file=get_name_class_file(dirname($fname),basename($fname),$options) ; // таже функция, что используется в i_system при генерации страницы
             if (file_exists($class_file))
             { $obj_info['page_mode']=2 ; // страница сайта с собственным шаблоном
               $obj_info['class_file']=$class_file ;
               $obj_info['class_name']=get_name_class(dirname($fname),basename($fname),$options) ;
               if ($options['system']) $obj_info['system_name']=str_replace('_system','',$options['system']) ;
             }
             else { $db_page_info=select_objs(TM_PAGES,'*','href="'.$fname.'" and clss=26 and enabled=1',array('no_arr'=>1,'no_slave_table'=>1,'debug'=>0)) ;
                    if ($db_page_info['pkey']) { $obj_info['page_mode']=1 ; // страница сайта с информацией в БД
                                                 $obj_info['db_tkey']=$_SESSION['pkey_by_table'][TM_PAGES] ;
                                                 $obj_info['db_pkey']=$db_page_info['pkey'] ;
                                               }
                    else $obj_info['page_mode']=3 ; // страница сайта без класса
                  }
         }
         else  if (strpos($fname,'.html')!==false) $obj_info['page_mode']=5 ;
         else $obj_info['page_mode']=4 ;
        //echo 'page_mode='.$obj_info['page_mode'].'<br>' ;
      }
    else if ($obj_info['file_type']=='application/javascript') $obj_info['page_mode']=6 ;
     else if ($obj_info['file_type']=='text/css') $obj_info['page_mode']=7 ;
      else if (strpos($fname,'.htaccess')!==false) $obj_info['page_mode']=8 ;

      switch($obj_info['page_mode'])
      { case 1:     $obj_info['page_mode_text']='DB страница сайта' ; $obj_info['obj_name']='DB cтраница сайта "'._PATH_TO_SITE.$fname.'"' ; break ;
        case 2:     $obj_info['page_mode_text']='PHP страница сайта' ; $obj_info['obj_name']='PHP cтраница сайта "'._PATH_TO_SITE.$fname.'"' ; break ;
        case 3:     $obj_info['page_mode_text']='LOST страница сайта' ; $obj_info['obj_name']='LOST страница сайта "'._PATH_TO_SITE.$fname.'"' ; break ;
        case 4:     $obj_info['page_mode_text']='TXT файл' ; $obj_info['obj_name']='Текстовой файл "'._PATH_TO_SITE.$fname.'"' ; break ;
        case 5:     $obj_info['page_mode_text']='HTML страница' ; $obj_info['obj_name']='HTML страница "'._PATH_TO_SITE.$fname.'"' ; break ;
        case 6:     $obj_info['page_mode_text']='JS файл' ; $obj_info['obj_name']='JavaScript файл "'._PATH_TO_SITE.$fname.'"' ; break ;
        case 7:     $obj_info['page_mode_text']='CSS файл' ; $obj_info['obj_name']='CSS файл "'._PATH_TO_SITE.$fname.'"' ; break ;
        case 8:     $obj_info['page_mode_text']='CONF файл' ; $obj_info['obj_name']='CONF файл "'._PATH_TO_SITE.$fname.'"' ; break ;
        default:    $obj_info['obj_name']='Файл "'._PATH_TO_SITE.$fname.'"' ;

      }
      return($obj_info);
 }

 function block_main()
 {  global $cmd,$obj_info,$fname,$cur_page,$mode ;
    if ($obj_info['type']=='dir')  $cur_menu_item=$this->show_dir_title() ;
    if ($obj_info['type']=='file') $cur_menu_item=$this->show_file_title() ;

     ?><form id=form name="form" method="post" action='' ENCTYPE="multipart/form-data">
	    <input name="cmd"       type="hidden" id=cmd        value=""/>
        <input name="cur_page"  type="hidden" id=cur_page   value="<?echo $cur_page?>">
        <input name="fname"     type="hidden"               value="<?echo $fname?>">
    <?
    //damp_array($obj_info) ;
    $show_next=1 ;
    if (method_exists($this,$cmd)) $show_next=$this->$cmd() ;

    if ($show_next) switch($cmd)
    {  case 'select_file': $this->return_file_patch(); break ;

       // по умолчние выводим данные для соответствующей вкладки меню
       default: switch($cur_menu_item['action'])
                { case 1001: $this->show_dir_info() ; break ;
                  //case 2001: $this->show_file_info() ; break ;
                  case 2003: $this->show_file_operation() ; break ;
                  //case 2004: $this->show_page_db_source() ; break ;
                  case 2005: $this->show_page_php_source() ; break ;
                  case 2006: $this->show_page_text_source() ; break ;
                  case 2007: $this->show_conf_file() ; break ;
                }
    }
    ?></form>
      <script type="text/javascript">
         // переход по страницам
         $jj('a.to_page').click(function(){$j('#cur_page').val($j(this).attr('tag'));$j('#form').submit();return(false);}) ;
         // показать в соседнем фрейме исходный код страницы
         $j('a.to_page_source').click(function(){$j('a.to_file.select').removeClass('select');$j(this).addClass('select');window.top.fra_order.location=_PATH_TO_ADMIN+"php_pages_editor_viewer.php?fname=<?echo $obj_info['name']?>"+$j(this).text() ;return(false) ;});
         // показать в соседнем фрейме DB объект для редактирования страницы
         $j('a.to_page_DB').click(function(){$j('a.to_file.select').removeClass('select');$j(this).addClass('select');window.top.fra_order.location=_PATH_TO_ADMIN+"fra_viewer.php?tkey="+$j(this).attr('tkey')+"&pkey="+$j(this).attr('pkey');return(false) ;});
         // показать в соседнем фрейме саму страницу
         $j('a.to_page_view').click(function(){$j('a.to_file.select').removeClass('select');$j(this).addClass('select');window.top.fra_order.location=$j(this).attr('title') ; return(false) ;});
         // пока
         $j('a#to_create_page').click(function(){$j('#cmd').val('show_dialog_create_page');$j('#form').submit();return(false);}) ;
      </script>
     <?
     return ;
     echo '<br><br><br><br>' ;
     echo 'fname='.$fname.'<br>' ;
     echo 'cmd='.$cmd.'<br>' ;
     if ($cmd) echo (method_exists($this,$cmd))? 'Функция "'.$cmd.'" существует<br>':'Команда не выполнена<br>' ;
     echo 'mode='.$mode.'<br>' ;
     echo 'menu_action:'.$menu_action.'<br>' ;
     damp_array($obj_info);
 }

 function show_dir_title()
 {   global $obj_info ;
     $menu_set[1]=array("name" => 'Файлы',"action" => 1001) ;
     $menu_set[2]=array("name" => 'Операции',"action" => 1002) ;
     ?><p class='obj_header'><?echo $obj_info['obj_name'] ;?></p><?
     $cur_menu_item=$_SESSION['top_menu']->show($obj_info,array('menu_set'=>$menu_set)) ;
     return($cur_menu_item) ;
 }

 function show_file_title()
 {  global $obj_info ;
    if ($obj_info['page_mode']==1) $menu_set[1]=array("name" => 'Текст HTML',"action" => 2004) ;
    if ($obj_info['page_mode']==2) $menu_set[1]=array("name" => 'Исходный код php',"action" => 2005) ;
    if ($obj_info['page_mode']==4) $menu_set[1]=array("name" => 'Текст',"action" => 2006) ;
    if ($obj_info['page_mode']==5) $menu_set[1]=array("name" => 'HTML код',"action" => 2006) ;
    if ($obj_info['page_mode']==6) $menu_set[1]=array("name" => 'JavaScript код',"action" => 2006) ;
    if ($obj_info['page_mode']==7) $menu_set[1]=array("name" => 'Правила CSS',"action" => 2006) ;
    if ($obj_info['page_mode']==8) $menu_set[1]=array("name" => 'Директивы',"action" => 2006) ;
    $menu_set[2]=array("name" => 'Операции',"action" => 2003) ;
    if ($obj_info['page_mode']==2 or $obj_info['page_mode']==3) $menu_set[3]=array("name" => 'Конфигурация страницы',"action" => 2007) ;
    ?><p class='obj_header'><?
	     echo $obj_info['obj_name'] ;
	     if ($obj_info['class_file']) echo '<br><span class="small">'.hide_server_dir($obj_info['class_file']).' ==> '.$obj_info['patch'].'</span>' ;
	     if ($obj_info['template_file']) echo '<br><span class="small">'.hide_server_dir($obj_info['template_file']).' ==> '.$obj_info['patch'].'</span>' ;
	?></p><?
     $cur_menu_item=$_SESSION['top_menu']->show($obj_info,array('menu_set'=>$menu_set)) ;
    return($cur_menu_item) ;
 }

 function show_dir_info()
  {   ?><div id=panel_create><img src="<?echo _PATH_TO_ENGINE.'/admin/images/add_icon.png'?>"><a href="" id=to_create_page class=title>Создать страницу сайта</a></div>
        <div class=clear></div><hr><?
      $this->show_files() ;
  }

  function show_files()
    { global $cur_page,$obj_info ;
      if ($cur_page=='') $cur_page=1 ;
      $pattern=array('html','htm','php','js','css','txt','htaccess') ;
      $cur_dir_list_files=get_files_list($obj_info['dir'],0,$pattern) ;
      // выводим файлы постранично, не более 30 на страницу
      $num_pages=sizeof($cur_dir_list_files)/30 ;
      if ($cur_page>($num_pages+1)) $cur_page=1 ;
      if ($num_pages>1)
      { ?><span class=title>Страница</span> <?
        for ($i=0; $i<$num_pages; $i++)
         { $class=(($i+1)==$cur_page)? 'class="to_page select"':'class=to_page' ;
           echo '<a href="" '.$class.' tag='.($i+1).'>'.($i+1).'</a>&nbsp;&nbsp;' ;
         }
        // оставляем в массиве тока текущий диспазон страниц
        $start_indx=($cur_page-1)*30+1 ;
        $end_indx=$start_indx+30-1 ; if ($end_indx>sizeof($cur_dir_list_files)) $end_indx=sizeof($cur_dir_list_files) ;
        $res_list_obj=array_slice($cur_dir_list_files,$start_indx,30) ;
        echo 'файлы '.$start_indx.'...'.$end_indx.' из '.sizeof($cur_dir_list_files).'<br><br>' ;
      } else $res_list_obj=$cur_dir_list_files ;

      $this->show_files_full_info($res_list_obj) ;
    }

    function show_files_full_info($res_list_obj)
    { global $obj_info ;
      ?><table class=left><?
        if (sizeof($res_list_obj)) foreach($res_list_obj as $fname)
        { $info=$this->analize_page(hide_server_dir($fname)) ;
          //$file_size=filesize($fname) ;
          $mime_type=mime_content_type($fname) ;
          if (strpos($fname,'.php')!==false) $mime_type='application/x-php' ;
          $mime_file=str_replace('/','-',$mime_type).'.png' ;
          $icon='<img src="'._PATH_TO_ENGINE.'/img/mime/32x32/gnome-mime-'.$mime_file.'">' ;
          $a_text=($info['page_mode']==1)? 'class="to_page_DB" pkey="'.$info['db_pkey'].'" tkey="'.$info['db_tkey'].'"':'class="to_page_source"' ;
          ?><tr><?
          ?><td><input type=checkbox name=select_files[<?echo urlencode(basename($fname))?>] value=1></a></td><?
          ?><td><a href="" class=to_page_view title="<?echo $obj_info['name'].basename($fname)?>"><? echo $icon ?></a></td><?
          ?><td class=width_200><a href="" <?echo $a_text?>><? echo basename($fname) ?></a></td><?
          ?><td class=center><? echo $info['page_mode_text'] ?></td><?
          ?><td><? echo hide_server_dir($info['class_file'])?></td><?
          ?><td><? echo $info['class_name']?></td><?
          ?></tr><?
        }
      ?></table><?
      if (!sizeof($res_list_obj)) echo '<div class=alert>Директория пуста</div><br>' ;
    }

 function show_dialog_mkdir()
  {
    ?><h2>Создать подкаталог</h2>
      <p><input name="new_name_dir" type="text" class=text value=""><br>
      <button onclick="exe_cmd('show_dialog_mkdir_step_2')">Создать подкаталог</button></p>
    <?
  }

 function show_dialog_mkdir_step_2()
   { global $new_name_dir,$abs_cur_dir,$otn_cur_dir,$abs_root_upload_dir ;
     echo '<br><br>' ;
     if ($new_name_dir)
     { echo 'Создаем подкаталог: <strong>'.$otn_cur_dir.$new_name_dir.'</strong> ' ;
       if (@mkdir($abs_cur_dir.$new_name_dir,_CREATE_DIR_PERMS)) echo '-<span class=green> OK</span><br>' ; else echo ' - <span class=red>директория уже существует или недостаточно прав</span><br>' ;
       unset($_SESSION['list_dir']) ; global $list_dir ; $list_dir=array() ;
       $this->read_dir_list($abs_root_upload_dir,'',$list_dir) ;
       return($otn_cur_dir.$new_name_dir.'/') ;

     } else echo '<div class=alert>Не указано имя создаваемого каталога</div><br>' ;
   }

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 //
 //  функции для разбора и показа скриптов сайта
 //
 ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  function show_page_php_source()
  { global $obj_info ;
    if ($obj_info['class_file']) $file=$obj_info['class_file'] ;
      else if ($obj_info['template_file']) $file=$obj_info['template_file'] ;
      else $file=_DIR_TO_ROOT.$obj_info['patch'] ;
      if ($obj_info['class_file'] or $obj_info['template_file']) echo '<br><span class="green bold">Внимание!</span><span class=black> Вы получили доступ к php-коду страницы сайта. Если Вы не знаете, что это такое, не пытайтесь самостоятельно править код страницы.</span><br><br>' ;
      $cont=file_get_contents($file) ;
      /*
      ?><div class="black">Ссылка на страницу сайта: <a href="<?echo $obj_info['patch_page']?>" target="_blank"><?echo $obj_info['patch_page']?></a></div><br><?
      if ($obj_info['class_file']) {?><div class="black">Адрес скрипта для этой страницы: <span style="text-decoration:underline;"><?echo hide_server_dir($obj_info['class_file'])?></span></div><br><?
                                    ?><div class="black">Имя класса для этой страницы: <span style="text-decoration:underline;"><?echo $obj_info['class_name']?></span></div><br><?
                                   } ;
      if ($obj_info['template_file']){?><div class="black">Адрес шаблона скрипта для этой страницы: <span style="text-decoration:underline;"><?echo hide_server_dir($obj_info['template_file'])?></span></div><br><?
                                      ?><div class="black">Имя шаблона класса для этой страницы: <span style="text-decoration:underline;"><?echo $obj_info['template_name']?></span></div><br><?
                                     } ;
      */
      ?><textarea name=content cols="20" rows=10><?echo htmlspecialchars($cont)?></textarea><br><br>
        <button onclick="exe_cmd('save_text_page')">Сохранить</button>
        <style type="text/css">textarea{width:90%;height:500px;margin-left:20px;word-wrap:normal;}</style>
      <?

  }

  function show_page_text_source()
  {   global $obj_info ;
      $cont=file_get_contents($obj_info['dir']) ;
      ?><textarea name=content cols="20" rows=10><?echo htmlspecialchars($cont)?></textarea><br><br>
        <button onclick="exe_cmd('save_text_page')">Сохранить</button>
        <style type="text/css">textarea{width:90%;height:500px;margin-left:20px;word-wrap:normal;}</style>
      <?
  }

  function save_text_page()
    { global $obj_info,$content ;
      $file=($obj_info['class_file'])? $obj_info['class_file']:$obj_info['dir'] ;
      $cnt=file_put_contents($file,stripslashes($content)) ;
      if ($cnt) echo '<div class=green>Cтраница успешно сохранена</div>' ; else echo '<div class=alert>Не удалось сохранить страницу</div><br>' ;
      return(1) ;
    }

  function show_file_operation()
    { ?><ul>
          <li>Удалить <a href="#" onclick="exe_cmd('delete_page');return false;">cтраницу сайта</a></li>
        </ul>
      <?
    }


  function delete_page()
    { global $obj_info ;
      $this->safe_del_file($obj_info['dir']) ;
      if ($obj_info['class_file']) $this->safe_del_file($obj_info['class_file']) ;
      ?><script type="text/javascript">window.top.fra_view.location=window.top.fra_view.location;</script><?
      return(0);
    }

  function safe_del_file($file_dir)
  {  $temp_dir=_DIR_TO_ROOT.'/!deleted/' ;
     $temp_name=str_replace('/','|',hide_server_dir($file_dir)) ;
     if (!is_dir($temp_dir) and !mkdir($temp_dir,_CREATE_DIR_PERMS)) { echo 'Не удалось создать папку для хранения удаленных файлов. Создайте папку самостоятельно и попробуйте еще раз' ; return ; }
     if (!rename($file_dir,$temp_dir.$temp_name)) echo 'Не удалось удалить файл "<strong>'.hide_server_dir($file_dir).'</strong>"' ;
     else echo 'Файл "<strong>'.hide_server_dir($file_dir).'"</strong> - удален<br>' ;
     //echo 'from: '.$file_dir.'<br>' ;
     //echo 'to  : '.$temp_dir.$temp_name.'<br>';

  }


   function show_dialog_create_page()
    { global $use_as_catalog ;
      $arr_parent_class2=$this->get_base_class_to_new_page() ;
      $extensions=array('.html','.php','.css','.js','.txt','') ;
      ?><h1>Создание страницы сайта</h1>
      <input class=new_page_type_radio_check type="radio" name=new_page_type value=0 checked>Создать страницу сайта со стандартной структурой, текст основной части хранится в БД <span class="green bold">(рекомендуется)</span><br>
      <? /*<input class=new_page_type_radio_check type="radio" name=new_page_type value=1>Создать страницу на основе шаблона с прямым редактированием<br>*/?>
      <input class=new_page_type_radio_check type="radio" name=new_page_type value=2>Создать страницу сайта с редактируемой структурой, макет страницы описывается в скрипте<br>
      <input class=new_page_type_radio_check type="radio" name=new_page_type value=10>Создать текстовой файл на сайте<br>
      <br>
      <p class="black bold">Укажите имя новой страницы сайта. Можете указать имя на русском языке, имя файла страницы будет автоматически преобразовано в транслит.</p><br>
          <input class="text large" type=text value="test2" name="new_page_name">
          <select name="new_page_ext"><?if (sizeof($extensions)) foreach($extensions as $value) echo '<option value="'.$value.'">'.$value.'</option>'?></select>
          <br><br>

      <div class="dir_file_parent"><p class="black bold">Выберите наследуемый шаблон:</p><select name="base_class" class=pattern><?
          if (sizeof($arr_parent_class2)) foreach($arr_parent_class2 as $indx=>$parent_class)
           {  list($fname,$class_name)=explode(':',$indx) ;
              echo '<option value="'.$indx.'"'.(($class_name=='site')? 'selected':'').'>'.$fname.': '.$parent_class.' -> ...</option>' ;
           }
        ?></select></div>
        <script type="text/javascript">
           $j('input.new_page_type_radio_check').change(function(){ if($j(this).val()==0 || $j(this).val()==10) $j('div.dir_file_parent').css('display','none'); else $j('div.dir_file_parent').css('display','block');});
        </script>
        <style type="text/css">
           div.dir_file_parent{display:none;}
           input.text{margin-left:20px;width:200px;}
           select.pattern{margin:10px 20px;}
        </style>
        <br>
        <button onclick="exe_cmd('show_dialog_create_page')">Обновить</button>
        <button onclick="exe_cmd('show_dialog_create_page_step_2')">Создать</button>
        <?
        return(0); // 0 - больше не выводить ничего

    }

    function show_dialog_create_page_step_2()
    { global $new_page_name,$new_page_type,$base_class,$obj_info,$new_page_ext;
      $new_page_name=safe_file_names($new_page_name) ;
      /*
      damp_array($obj_info) ;
      echo 'dir='.$obj_info['name'].'<br>';
      echo 'new_page_name='.$new_page_name.'<br>' ;
      echo 'new_page_ext='.$new_page_ext.'<br>' ;
      echo 'new_page_type='.$new_page_type.'<br>' ;
      echo 'base_class='.$base_class.'<br>'       ;
      */
      switch($new_page_type)
      { case 0: // создаем страницу на основе базового шаблона с редактированием через админку
                $this->create_page_from_standart_template($obj_info['name'],$new_page_name.$new_page_ext,$new_page_name) ;
                break ;
        case 2: // cоздаем страницу с собственным шаблоном
                $this->create_page_by_template($obj_info['name'],$new_page_name.$new_page_ext,'Новая страница',$base_class) ;
                break ;
        case 10:// создаем пустую страницу без шаблона
                $this->create_blank_page($obj_info['name'],$new_page_name.$new_page_ext) ;
                break ;
      }

      ?><br><button onclick="exe_cmd('show_dialog_create_page')">Создать еще одну страницу</button><?
      return(0); // 0 - больше не выводить ничего
    }

    function create_page_from_standart_template($dir,$fname,$title)
    { echo '<h1>Создаем стандартную страницу сайта с редактированием через админку</h1>' ;
      $id=adding_rec_to_table(TM_PAGES,array('obj_name'=>$title,'clss'=>26,'href'=>$dir.$fname,'enabled'=>1,'parent'=>1)) ; //echo 'Добавлена запись в БД: '.$id.'<br>' ; echo 'dir='.$dir.'<br>' ; echo 'fname='.$fname.'<br>' ;
      $ini_patch=_DIR_TO_ROOT.'/ini/patch.php' ;
      // прописываем стандарный скрипт в корне
      $cont="<? // создано системой автоматического создания шаблонов страниц ".get_month_year(0,'day month year time')."\ninclude ('".$ini_patch."')  ;\ninclude (_DIR_TO_ENGINE.'/i_system.php') ;\n\$options=array();\nshow_page(\$options) ;\n?>" ;
      $cnt=create_page($dir,$fname,$cont) ;
      if ($cnt) {?><script type="text/javascript">window.top.fra_order.location=_PATH_TO_ADMIN+"php_pages_editor_viewer.php?fname=<?echo $dir.$fname?>" ;</script><?}
    }

    function create_page_by_template($dir,$fname,$title,$base_class)
    { echo '<h1>Создаем PHP страницу с собственным шаблоном</h1>' ;
      $ini_patch=_DIR_TO_ROOT.'/ini/patch.php' ;
      list($base_class,$parent_class_name)=explode(':',$base_class) ;
      $extends_class='extends c_'.$parent_class_name ;
      $exitends_include="include('".basename($base_class)."') ;" ;
      $class_file=get_name_class_file($dir,$fname) ; // таже функция, что используется в i_system при генерации страницы
      $class_name=get_name_class($dir,$fname) ;
      // прописываем стандарный скрипт в корне
      $cont="<? // создано системой автоматического создания шаблонов страниц ".get_month_year(0,'day month year time')."\ninclude ('".$ini_patch."')  ;\ninclude (_DIR_TO_ENGINE.'/i_system.php') ;\n\$options=array();\nshow_page(\$options) ;\n?>" ;
      $cnt=create_page($dir,$fname,$cont) ;
      if ($cnt)
      { $cont="<? // создано системой автоматического создания шаблонов страниц ".get_month_year(0,'day month year time')."\n".$exitends_include."\nclass ".$class_name." ".$extends_class."\n{\n\t// вывод основной информации\n\tfunction block_main()\n\t{\n\t\techo 'Данная система создана системой автоматического создания шаблонов страниц' ;\n\t}\n}\n?>" ;
        $cnt=create_page('/'._CLASS_.'/',basename($class_file),$cont,'шаблон страницы сайта') ;
        if ($cnt) {?><script type="text/javascript">window.top.fra_order.location=_PATH_TO_ADMIN+"php_pages_editor_viewer.php?fname=<?echo $dir.$fname?>" ;</script><?}
      }
    }

    function create_blank_page($dir,$fname)
    { echo '<h1>Создаем текстовой файл на сайте</h1>' ;
      $cnt=create_page($dir,$fname,' ','текстовой файл') ;
      if ($cnt) {?><script type="text/javascript">window.top.fra_order.location=_PATH_TO_ADMIN+"php_pages_editor_viewer.php?fname=<?echo $dir.$fname?>" ;</script><?}
    }

 ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 //
 //  вспомогательные функции для разбора скриптов сайта
 //
 ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

     function parse_php_file($file_name)
    {
         $matches=array() ;
         //echo $file_name.'<br>';
         $cont=file_get_contents(_DIR_TO_ROOT.$file_name);
         if ((strpos($cont,'<?')===false and strpos($cont,'?>')===false) and (strpos($cont,'<?php')===false and strpos($cont,'php?>')===false)) return($matches) ;
         /*?><div class="blau small"><?echo nl2br($cont) ;?></div><?*/
         // делаем парсинг на include ;
         // \s* - ждем любое количество пробелов
         // (.*?) - ждем любой набор симоволов
         preg_match_all('/include\s*\((.*?)\)\s*;/is',$cont,$matches['include'],PREG_SET_ORDER) ;
         if (sizeof($matches['include'])) foreach($matches['include'] as $rec) $cont=str_replace($rec[0],'',$cont) ;
         // делаем парсинг на $options ;
         // \s* - ждем любое количество пробелов
         // [\"\']? - ждем " или '
         // ([^\'\">]+?) - ждем любой набор симоволов, в которые не входит ' или " или >
         // ([^;]+?) - ждем любой набор симоволов, в которые не входит ;
         preg_match_all('/\$options\s*\[\s*[\"\']?([^\'\"]+?)[\"\']?\s*\]\s*=\s*([^;]+?)\s*;/is',$cont,$matches['options'],PREG_SET_ORDER) ;
         if (sizeof($matches['options'])) foreach($matches['options'] as $rec) $cont=str_replace($rec[0],'',$cont) ;
         // делаем парсинг на объявленные переменные ;
         // \s* - ждем любое количество пробелов
         // [\"\']? - ждем " или '
         // ([^\'\">]+?) - ждем любой набор симоволов, в которые не входит ' или " или >
         // ([^;]+?) - ждем любой набор симоволов, в которые не входит ;
         preg_match_all('/\$(.*?)\s*=\s*(.*?)\s*;/is',$cont,$matches['vars'],PREG_SET_ORDER) ;
         if (sizeof($matches['vars'])) foreach($matches['vars'] as $rec) $cont=str_replace($rec[0],'',$cont) ;
         // делаем парсинг на функцию страницы ;
         // \s* - ждем любое количество пробелов
         // [\"\']? - ждем " или '
         // ([^\'\">]+?) - ждем любой набор симоволов, в которые не входит ' или " или >
         // ([^;]+?) - ждем любой набор симоволов, в которые не входит ;
         preg_match_all('/(\w*?)\((\S*?)\)\s*;/is',$cont,$matches['functions'],PREG_SET_ORDER) ;
         //echo nl2br($cont) ;
         return($matches);
    }

    function exec_config($arr)
    { $options=array() ;
      if (sizeof($arr)) foreach($arr as $rec) eval($rec[0]) ; // выполняем директивы из файла
      compatible_options_names($options) ; // преобразование старых имен опций в новые - i_system
      return($options);
    }

    function get_base_class_to_new_page()
    {   $dir_class=get_files_list(_DIR_TO_CLASS.'/') ;
        if (sizeof($dir_class)) foreach($dir_class as $rec)
         { $fname=basename($rec) ;
           //if (strpos($fname,'_')!==0 and $fname!='version_checked.php' and $fname!='mc_ext_moduls.php')
           if (strpos($fname,'c_')===0) // файл должен начинаться с префикса с_
           { //echo $fname.'<br>' ;
             $text=file_get_contents($rec) ;
             if (preg_match_all('/class c_(.+?) extends c_(.+?) /is',$text,$matches,PREG_SET_ORDER)) ;
               $arr_parent_class2[hide_server_dir($rec).':'.$matches[0][1]]='c_'.str_replace('{','',$matches[0][2]).' -> c_'.$matches[0][1]  ;
           } //else echo '<div class=red>'.$fname.'</div>';
         }
         asort($arr_parent_class2);
         return($arr_parent_class2) ;
    }

    function show_conf_file()
    { global $obj_info ;
      $conf=$obj_info['config'] ;
      if (sizeof($conf['include'])) {?><h1>Include</h1><ul><? foreach($conf['include'] as $rec) echo '<li>'.$rec[1].'</li>' ; ?></ul><?}
      if (sizeof($conf['options'])) {?><h1>Options</h1><ul><? foreach($conf['options'] as $rec) echo '<li>'.$rec[1].' = '.$rec[2].'</li>' ; ?></ul><?}
      if (sizeof($conf['vars'])) {?><h1>Vars</h1><ul><? foreach($conf['vars'] as $rec) echo '<li>'.$rec[1].' = '.$rec[2].'</li>' ; ?></ul><?}
      if (sizeof($conf['functions'])) {?><h1>Functions</h1><ul><? foreach($conf['functions'] as $rec) echo '<li>'.$rec[1].'('.$rec[2].')</li>' ; ?></ul><?}
    }


    function show_a_to_edit_page($href)
    {?><br><br>
       Через несколько секунд Вы будут перенаправлены на страницу редактирования новой страницы сайта.<br>
       Если Ващ  браузер не поддерживает автоматического перенаправлния, Вы можете перейти к правке содержания страницы по этой <a href="<?echo $href?>" class="black bold">ссылке</a>
       <br><br>
       <?return;?>
       <noscript><meta http-equiv="Refresh" content="2; URL=<?echo $href?>" /></noscript>
        <script type="text/javascript"><!--
          function exec_refresh()
          { window.status = "Переадресация..." + myvar;
            myvar = myvar + " .";
            var timerID = setTimeout("exec_refresh();", 100);
              if (timeout > 0) {timeout -= 1; }
              else
              { clearTimeout(timerID);
                window.status = "";
                window.location = "<?echo $href?>";
              }
          }
          var myvar = "";
          var timeout = 20;
          exec_refresh();
      //--></script>
     <?
    }

}

?>