<?php
include_once(_DIR_TO_ENGINE."/admin/i_clss_func.php") ;
include_once(_DIR_TO_ENGINE.'/admin/c_menu_tabs.php') ;

class c_editor_pages extends c_editor_obj
{
  function body($options=array())
  { $this->body_frame_2x(array('fra_tree'=>_PATH_TO_ADMIN.'/editor_pages_tree.php','title'=>$options['title'])) ;
  }
}

class c_editor_pages_tree extends c_admin_page
{
  function body($options=array())
  {global $br_vers ;
   // исключаеим из поиска каталоги, где нельзя размещать информацию
   $no_index=get_no_index_dirs() ;
   $start_dir=_DIR_TO_ROOT.'/' ;
   $start_dir_id=hide_server_dir($start_dir) ;
   $start_dir_name=basename($start_dir) ;

   $use_filter=1 ;
   // получаем содержимое корневой директории
   $list_dir=get_dir_list($start_dir) ;
   ?><body id=tree>
     <div id=tree_menu></div>
     <?if (strpos($br_vers,'IE6')===false){?><div class=space_div></div><?}?>
     <script type=text/javascript >

      if (clss_icon[1]==undefined) { clss_icon[1]= new Array ; clss_icon[1][0]= new Array ; clss_icon[1][1]= new Array ; }
      if (clss_icon[2]==undefined) { clss_icon[2]= new Array ; clss_icon[2][0]= new Array ; clss_icon[2][1]= new Array ; }
      if (clss_icon[3]==undefined) { clss_icon[3]= new Array ; clss_icon[3][0]= new Array ; clss_icon[3][1]= new Array ; }

      clss_icon[1][1]['open'] ="<? echo _PATH_TO_ADMIN_IMG?>clss/folder_on_open.gif" ;
	  clss_icon[1][1]['close']="<? echo _PATH_TO_ADMIN_IMG?>clss/folder_on_close.gif" ;
	  clss_icon[1][0]['open'] ="<? echo _PATH_TO_ADMIN_IMG?>clss/folder_off_open.gif" ;
	  clss_icon[1][0]['close']="<? echo _PATH_TO_ADMIN_IMG?>clss/folder_off_close.gif" ;
      clss_icon[2][1]['open'] ="<? echo _PATH_TO_ADMIN_IMG?>clss/item_on.gif" ;
	  clss_icon[2][1]['close']="<? echo _PATH_TO_ADMIN_IMG?>clss/item_on.gif" ;
	  clss_icon[2][0]['open'] ="<? echo _PATH_TO_ADMIN_IMG?>clss/item_off.gif" ;
	  clss_icon[2][0]['close']="<? echo _PATH_TO_ADMIN_IMG?>clss/item_off.gif" ;
      clss_icon[3][1]['open'] ="<? echo _PATH_TO_ADMIN_IMG?>clss/item_on_ed.gif" ;
	  clss_icon[3][1]['close']="<? echo _PATH_TO_ADMIN_IMG?>clss/item_on_ed.gif" ;
	  clss_icon[3][0]['open'] ="<? echo _PATH_TO_ADMIN_IMG?>clss/item_off_ed.gif" ;
	  clss_icon[3][0]['close']="<? echo _PATH_TO_ADMIN_IMG?>clss/item_off_ed.gif" ;

      object[1]= new Array ;
      object[1]["<?echo $start_dir_id?>"] = new obj(1,1,"<?echo $start_dir_id?>",null,"<?echo $start_dir_name?>") ;
      object[1]["<?echo $start_dir_id?>"].expand_cmd=6 ;
      object[1]["<?echo $start_dir_id?>"].only_dirs=1 ;
      object[1]["<?echo $start_dir_id?>"].use_filter=<?echo $use_filter?> ;
      <?if (sizeof($list_dir)) foreach($list_dir as $name=>$dir) if (!$use_filter or ($use_filter and !in_array($dir,$no_index))){ echo 'object[1]["'.hide_server_dir($start_dir).'"].add_child(1,"'.hide_server_dir($dir).'","'.$name.'",1,1);';}?>
      object[1]["<?echo $start_dir_id?>"].show() ;
      var rn=Math.random();
      if (parent.fra_view!=null) parent.fra_view.location="<?echo _PATH_TO_ADMIN?>/file_explorer_viewer.php?fname="+root.pkey+"&mode=editor&rn="+rn ;
      function obj_click(tkey,pkey,cmd,evt)
        { var rn=Math.random();
          var obj=object[1][pkey] ;
          if (evt!=undefined) { evt.cancelBubble=true ; evt.returnValue=false ; }

          if (select_obj!=undefined)
          { var class_sel=/select/g ;
            var unsel_class=select_obj.className.replace(class_sel,'') ;
            select_obj.className=unsel_class ;
          }
          select_obj = document.getElementById('obj_'+obj.id);
          //$j('div#obj_'+obj.id).addClass('select') ;
          //alert($j('div#obj_'+obj.id).name) ;
          select_obj.className=select_obj.className+' select' ;
          select_obj_tree=obj ;
          if (parent.fra_view!=null) parent.fra_view.location="<?echo _PATH_TO_ADMIN?>/file_explorer_viewer.php?fname="+pkey+"&mode=editor&rn="+rn ;
        }

    </script>
    </body>
   <?
  }
}


?>