<?php
// старая версия радектора тегов, пока временно не поддерживается

include_once(_DIR_TO_ENGINE."/admin/i_clss_func.php") ;
include_once(_DIR_TO_ENGINE.'/admin/c_menu_tabs.php') ;
include_once(_DIR_TO_ENGINE.'/admin/c_admin_page.php') ;

//-----------------------------------------------------------------------------------------------------------------------------
// редактор мета-тегов
//-----------------------------------------------------------------------------------------------------------------------------

class c_editor_meta_tags extends c_editor_obj
{
  function body($options=array()) { $this->body_frame_2x(array('fra_tree'=>'meta_tags_tree.php?cur_page_href=index.php')) ; }
}

//-----------------------------------------------------------------------------------------------------------------------------
// редактор мета-тегов
//-----------------------------------------------------------------------------------------------------------------------------


// настройки редактора мета-тегов
class c_meta_tags_tree extends c_admin_page
{

  function body()
  { global $cur_page_href,$cmd,$db_title,$db_keywords,$db_description,$from_site ;

    //echo '$obj_reffer='.$obj_reffer ;
    //$list=get_obj_parents($obj_reffer) ;
    //damp_array($list) ;


    $meta_info=execSQL_van('select * from '.TM_KEYWORDS.' where obj_name="'.$cur_page_href.'" and enabled=1') ;
    if ($cmd=='save_meta')
    {
      $finfo=array(		'parent'		=>	1,
   						'clss'			=>	96,    // 1 - папка
     					'obj_name'		=>	$cur_page_href,
     					//'href'			=>  $cur_page_href,
     					'title'			=>	$db_title,
     					'keywords'		=>	$db_keywords,
     					'description'	=>	$db_description
   				   ) ;

      if ($meta_info['pkey']) update_rec_in_table(TM_KEYWORDS,$finfo,'pkey='.$meta_info['pkey']) ;
      					 else adding_rec_to_table(TM_KEYWORDS,$finfo) ;

      $meta_info=execSQL_van('select * from '.TM_KEYWORDS.' where obj_name="'.$cur_page_href.'" and enabled=1') ;
      global $obj ;
      //damp_array($obj) ;
      save_list_obj($_POST['obj']) ;
    }

    //damp_array($meta_info);
    global $obj_reffer ;
    //echo 'allow_url_fopen='.ini_get('allow_url_fopen') ;
  	$page=file_get_contents('http://'.$cur_page_href);
    //$links = pc_link_extractor($page);
    $title = pc_title_extractor($page) ;

    //if (sizeof($links))
     { ?><form name="form" method="post" ENCTYPE="multipart/form-data">
		   	<input name="cmd"  type="hidden" value="<?echo $cmd?>"   id='cmd'>
			<input name="cur_page_href"  type="hidden" value="<?echo $cur_page_href?>">
			<input name="obj_reffer" type="hidden" value="<?echo $obj_reffer?>">
          <div class=panel_meta_tags>
          <h1>Текущие залоловки страницы&nbsp;<span class=green><?echo $cur_page_href?></span>:</h1>
          <table class=left style='width:93%;'><?if (sizeof($title)) foreach($title as $name=>$value){?><tr><td style='width:70px;'><?echo $name?></td><td><?echo $value?></td></tr><?}?></table>
          <h1>Собственные мета-теги страницы&nbsp;<span class=green><?echo $cur_page_href?></span>:</h1>
          <? $c1=($meta_info['title'])? 'back_green':'';
             $c2=($meta_info['keywords'])? 'back_green':'';
             $c3=($meta_info['description'])? 'back_green':'';
          ?>
 	      <div class=title>T:</div><input class='<?echo $c1?> text' name='db_title' type='text' value='<?echo $meta_info['title']?>'><br>
 	      <div class=title>K:</div><textarea name='db_keywords' class='<?echo $c2?>' rows="2" wrap='auto' type='text'><?echo $meta_info['keywords']?></textarea><br>
 	      <div class=title>D:</div><textarea name='db_description' class='<?echo $c3?>' rows="5" wrap='auto' type='text'><?echo $meta_info['description']?></textarea><br>
 	      <button onclick="exe_cmd('save_meta')">Сохранить изменения</button><br><br>

	 	  <?if ($obj_reffer)
	 	  { list($reffer_obj_pkey,$reffer_obj_tkey)=explode('.',$obj_reffer) ;
	 	    if (isset($_SESSION['descr_obj_tables'][$reffer_obj_tkey]))
	 	      { $reffer_obj_info=execSQL_van('select parent,obj_name,clss,title,keywords,description from '._DOT($reffer_obj_tkey)->table_name.' where pkey = "'.$reffer_obj_pkey.'"') ;
          		$c1=($reffer_obj_info['title'] and !$c1)? 'back_green':'';
             	$c2=($reffer_obj_info['keywords'] and !$c2)? 'back_green':'';
             	$c3=($reffer_obj_info['description'] and !$c3)? 'back_green':'';
	 	        $input_name='obj['.$reffer_obj_tkey.']['.$reffer_obj_info['clss'].']['.$reffer_obj_pkey.']' ;
		 	  	?><h1>Мета-теги '<span class=green><?echo $reffer_obj_info['obj_name']?></span>'&nbsp;(<?echo $obj_reffer?>)</h1>
		 	  	  <div class=title>T:</div><input class='<?echo $c1?> text' name='<?echo $input_name?>[title]' type='text' value='<?echo $reffer_obj_info['title']?>'><br>
		 	  	  <div class=title>K:</div><textarea class='<?echo $c2?> memo' name='<?echo $input_name?>[keywords]' rows="5" wrap='auto' type='text'><?echo $reffer_obj_info['keywords']?></textarea><br>
		 	      <div class=title>D:</div><textarea class='<?echo $c3?> memo' name='<?echo $input_name?>[description]' rows="5" wrap='auto' type='text'><?echo $reffer_obj_info['description']?></textarea><br>
		 	      <button onclick="exe_cmd('save_meta')">Сохранить изменения</button><br>
	          <?} ?>


           <?}

           $tkey_setting=$_SESSION['pkey_by_table'][TM_SETTING] ;
           if (_DOT($tkey_setting)->pkey)
           { $def_meta_tags=execSQL('select comment,pkey,clss,value from '._DOT($tkey_setting)->table_name.' where comment="LS_def_page_keywords" or comment="LS_def_page_title" or comment="LS_def_page_descr" ') ;
          	 $c1=($def_meta_tags['LS_def_page_title']['value'] and !$c1)? 'back_green':'';
             $c2=($def_meta_tags['LS_def_page_keywords']['value'] and !$c2)? 'back_green':'';
             $c3=($def_meta_tags['LS_def_page_descr']['value'] and !$c3)? 'back_green':'';

             $input_name='obj['.$tkey_setting.']['.$def_meta_tags['def_page_title']['clss'].']' ;
	            ?><h1>Мета-теги по умолчанию для <span class=green>всех страниц сайта</span></h1>
		 	  	  <div class=title>T:</div><input class='<?echo $c1?> text' name='<?echo $input_name?>[<?echo $def_meta_tags['LS_def_page_title']['pkey']?>][value]' type='text' value='<?echo $def_meta_tags['LS_def_page_title']['value']?>'><br>
		 	  	  <div class=title>K:</div><textarea class='<?echo $c2?> memo' name='<?echo $input_name?>[<?echo $def_meta_tags['LS_def_page_keywords']['pkey']?>][value]'  rows="5" wrap='auto' type='text'><?echo $def_meta_tags['LS_def_page_keywords']['value']?></textarea><br>
		 	      <div class=title>D:</div><textarea class='<?echo $c3?> memo' name='<?echo $input_name?>[<?echo $def_meta_tags['LS_def_page_descr']['pkey']?>][value]' rows="5" wrap='auto' type='text'><?echo $def_meta_tags['LS_def_page_descr']['value']?></textarea><br>
		 	      <button onclick="exe_cmd('save_meta')">Сохранить изменения</button><br><br>
		   <?}

          if (!$from_site)
         	{ $cur_page_href=add_pars_to_url('http://'.$cur_page_href,array('debug_mode'=>'meta_tags','reboot_setting'=>1)) ;
         	  ?><script>if (parent.fra_view!=null) parent.fra_view.location="<?echo _PATH_TO_SITE.'/'.$cur_page_href?>" ;</script><?
		    }?>

		  </div>
        </form>
       <?
     }
  }

}



function pc_head_extractor($s)
{
  $a = array();
  if (preg_match_all('/<head>(.+?)<\/head>/is',$s,$matches,PREG_SET_ORDER)) {
    foreach($matches as $match) {
      array_push($a,array($match[1],$match[2]));
    }
  }
  return $a;
}


?>