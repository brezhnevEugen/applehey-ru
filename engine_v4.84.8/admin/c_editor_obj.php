<?
//-----------------------------------------------------------------------------------------------------------------------------
// Промежуточный класс для всех страниц из нескольких фреймов
//-----------------------------------------------------------------------------------------------------------------------------
include_once('c_admin_page.php') ;
class c_editor_obj extends c_admin_page
{
 // эта функция будет выполнена в первую очередь // объявить глобальные переменные скрипта
 // переопределяеть её нельзя, иначе параметры таблицы необходиомо задавать вручную (сделано только на системных таблицах)
 function command_execute($options=array())
 { switch ($options['use_table'])
     { //case 'obj_descr_clss' : 			create_class_model() ; 		break ;
       case 'obj_descr_obj_tables' : 	create_model_obj_tables() ; break ;
     }
   // зачем перегружать подсистемы кадлый раз при показе фрейма
   //if ($options['use_system']) create_system_modul_obj($options['use_system'],array('no_upload_rec'=>1,'debug'=>0)) ;
 }

 function _page_title($show_title='') { global $tkey ; echo ($show_title)? $show_title:_DOT($tkey)->title ;}

 function site_top_head($options=array()) {} // для страницы с фреймама загрузки заголовков не нужно

 function body($options=array())
  { global $tkey  ;
    $tree_src=($options['use_tree_src'])? $options['use_tree_src'].'?tkey='.$tkey:'fra_tree.php?tkey='.$tkey;
    if ($options['root_id']) $tree_src.='&root_id='.$options['root_id'] ;
    $this->body_frame_2x(array('fra_tree'=>_PATH_TO_ADMIN.'/'.$tree_src,'title'=>$options['title'],'root_id'=>$options['root_id'])) ;
  }
}
?>