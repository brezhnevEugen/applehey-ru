<?php
include_once (_DIR_TO_ENGINE.'/c_page_XML_AJAX.php') ;
include_once (_DIR_TO_ENGINE.'/admin/i_admin_system.php') ;
include_once(_DIR_TO_ENGINE.'/admin/i_clss_func.php') ;
class c_ajax extends c_page_XML_AJAX
{  public $use_macros=0 ;
   // для кнопок типа func=delete_all_img_of_clone script="ext/image/delete_all_img_of_clone"
   function exec_func($func)
   { $arr=explode('/',$_POST['script']) ;
     $script_dir='' ;
     if ($arr[0]=='mod' and sizeof($arr==3)) { $script_dir=_DIR_TO_MODULES.'/'.$arr[1].'/'.$arr[2].'.php' ; }
     if ($arr[0]=='ext' and sizeof($arr==3)) { $script_dir=_DIR_ENGINE_EXT.'/'.$arr[1].'/'.$arr[2].'.php' ; }
     if ($script_dir and file_exists($script_dir))
     { include_once($script_dir) ;
       if (function_exists($func))  $func() ;
     } else echo 'Скрипт "'.$script_dir.'" не найден<br>' ;
   }

   // аналог функции c_site::panel_function()
   function exec_cmd($cmd)
   {   //damp_array($_POST) ;
       // если в форме перед отправкой производилось подклбчение расширения или класса, подклбчаем его снова
       if (sizeof($_POST['include_extension'])) foreach($_POST['include_extension'] as $ext_name=>$value) include_extension($ext_name) ;
       /*
       list($stop_view2,$cmd_exec)=exec_script_cmd($cmd,$_POST['script'],array('debug'=>0,'cur_page'=>$this)) ;
       if (!$cmd_exec) list($stop_view2,$cmd_exec)=exec_script_cmd('AJAX_'.$cmd,$_POST['script'],array('debug'=>0,'cur_page'=>$this)) ;

       if (!$cmd_exec)  { $this->add_element('exec_func_or_method','not_found',array('cmd'=>$cmd,'script'=>$_POST['script'])) ;
                          ?><strong>POST:</strong><?
                          damp_array($_POST,1,-1) ;
                          echo '<div style="text-align:center;">Метод, функция или расширение<br>"<strong>'.$cmd.'</strong>"<br>или<br> "<strong>'.'AJAX_'.$cmd.'</strong>"<br> не найдены</div>' ;
                        }
       */
       /// !!! AJAX должна выполнять команду только из текущего скрипта
       // никаких автоподключенных скриптв. При необходимости использовать внешний функции подключать вручную.
       $AJAX_cmd='AJAX_'.$cmd ;
       if (method_exists($this,$cmd)) { $this->add_element('exec_method',$cmd) ; $this->$cmd() ; }     // ищем метод в c_ajax::---cmd---
       else if (method_exists($this,$AJAX_cmd)) { $this->add_element('exec_method',$AJAX_cmd) ; $this->$AJAX_cmd() ; } // ищем метод в c_ajax::AJAX_---cmd---
       /*
       else if (function_exists($AJAX_cmd)) { $this->add_element('exec_func',$AJAX_cmd) ; $AJAX_cmd() ; } // ищем функцию  AJAX_---cmd--- в уже подключенных файлах
       else if (function_exists($cmd)) { $this->add_element('exec_func',$cmd) ; $cmd() ; } // ищем функцию  ---cmd--- в уже подключенных файлах (временно оставлено для совместимости)
       */
       /*возвращаем временно для обеспечения совместимости*/
       else if (include_script_cmd($cmd,$_POST['func_name'])) { $this->add_element('exec_extension',$cmd) ; $cmd() ; } // ищем расширение ---cmd--- ВНИМАНИЕ! в расширение не передаются doc и xml

       else { $this->add_element('exec_func_or_method','not_found',array('cmd'=>$cmd,'script'=>$_POST['script'])) ;
              ?><strong>POST:</strong><?
              //damp_array($_POST,1,-1) ;
              echo '<div style="text-align:center;">Метод, функция или расширение<br>"<strong>'.$cmd.'</strong>"<br>или<br> "<strong>'.$AJAX_cmd.'</strong>"<br> не найдены</div>' ;
            }

   }

   // генерация HTML-JS кода для фрейма загрузки фото в модальном окне
   function panel_obj_img()
   { $this->add_element('title','Изображения') ;
     //$this->add_element('after_close_window',$_POST['after_close_window']) ;
     //$this->add_element('url',$_POST['url']) ;
     $recs_img=select_db_obj_child_recs($_POST['reffer'],array('only_clss'=>3,'no_group_tkey_clss'=>1)) ;
     list($pkey,$tkey)=explode('.',$_POST['parent_reffer']) ;
     $tket_img=_DOT($tkey)->list_clss[3] ;
     _CLSS(3)->show_list_items($tket_img,'parent='.$pkey) ;
     //damp_array($recs_img) ;
   }

   // генерация HTML-JS кода для фрейма загрузки фото в модальном окне
//view-source:http://postroirai.clss.ru/admin/fra_viewer.php?&table_code=goods&clss=200&reffer=20766.136&class=selected&clss=200&reffer=20766.136&class=selected&rn=0.8722502326127142
//view-source:http://postroirai.clss.ru/admin/fra_viewer.php?table_code=goods&clss=200&reffer=20766.136&class=selected&menu_id=prop_manual_2
//view-source:http://postroirai.clss.ru/admin/fra_viewer.php?table_code=goods&clss=200&reffer=20766.136&class=selected&menu_id=structure
//view-source:http://postroirai.clss.ru/admin/fra_viewer.php?table_code=goods&clss=200&reffer=20643.136&class=selected&menu_id=structure_3
   function panel_obj_img_upload()
   { $this->success='modal_window' ;
     $this->modal_panel_title='Загрузка изображений' ;
     if ($_POST['after_close_window']=='update_tr_rec')
         $this->add_element('after_close_update_tr_rec',$_POST['parent_reffer']) ;
     else
        { $this->add_element('after_close_update_page','1') ;
          $this->add_element('after_close_go_url_append','&menu_id=structure_'.$_POST['clss']) ;
        }
     //$this->add_element('after_close_window',$_POST['after_close_window']) ;
     //$this->add_element('url',$_POST['url']) ;
     //$this->add_element('after_close_update_item',$_POST['parent_reffer']) ; - не работает
     ?><iframe src = "<? echo _PATH_TO_ADMIN?>/file_uploader.php?mode=obj_img&reffer=<?echo $_POST['parent_reffer']?>&clss=<?echo $_POST['clss']?>" name = 'fra_fancyupload' id = fra_fancyupload scrolling=auto frameborder=0 width=500 height = "400" xmlns = "http://www.w3.org/1999/html"></iframe><?
     /*?><script type="text/javascript">alert(1)</script><button class=v2 cmd="test">test</button><?*/
   }

   function panel_obj_img_upload_tr_update()
   { $this->success='modal_window' ;
     $this->modal_panel_title='Загрузка изображений' ;

     //$this->add_element('after_close_window',$_POST['after_close_window']) ;
     //$this->add_element('url',$_POST['url']) ;
     //$this->add_element('after_close_update_item',$_POST['parent_reffer']) ; - не работает
     ?><iframe src = "<? echo _PATH_TO_ADMIN?>/file_uploader.php?mode=obj_img&reffer=<?echo $_POST['parent_reffer']?>&clss=<?echo $_POST['clss']?>" name = 'fra_fancyupload' id = fra_fancyupload scrolling=auto frameborder=0 width=500 height = "400" xmlns = "http://www.w3.org/1999/html"></iframe><?
   }

   // генерация HTML-JS кода для фрейма загрузки фото в модальном окне
   function panel_obj_file_upload()
   { $this->add_element('title','Загрузка файлов') ;
     $this->add_element('after_close_window',$_POST['after_close_window']) ;
     $this->add_element('url',$_POST['url']) ;
     ?><iframe src="<? echo _PATH_TO_ADMIN?>/file_uploader.php?mode=obj_file&reffer=<?echo $_POST['parent_reffer']?>&clss=<?echo $_POST['clss']?>" name='fra_fancyupload' id=fra_fancyupload  scrolling=auto frameborder=0 width=500 height="400"></iframe><?
   }

   // генерация HTML-JS кода для фрейма загрузки банера в модальном окне
   function panel_obj_banner_upload()
   { $this->add_element('title','Загрузка банера') ;
     $this->add_element('after_close_window',$_POST['after_close_window']) ;
     $this->add_element('url',$_POST['url']) ;
     ?><iframe src="<? echo _PATH_TO_ADMIN?>/file_uploader.php?mode=obj_banner&reffer=<?echo $_POST['parent_reffer']?>&clss=<?echo $_POST['clss']?>" name='fra_fancyupload' id=fra_fancyupload  scrolling=auto frameborder=0 width=500 height="400"></iframe><?
   }

   function create_obj_dialog() {$this->create_obj() ; }
   function create_obj()
   { $obj_clss=_CLSS($_POST['clss']) ;
     $clss_name=$obj_clss->name($_POST['parent_reffer']) ;
     if (method_exists($obj_clss,'obj_create_dialog'))
     { $this->add_element('title','Добавление нового объекта "'.$clss_name.'"') ;
       $obj_clss->obj_create_dialog($_POST) ;
     }
     else
     { $new_obj_rec=$obj_clss->obj_create($_POST['new_obj'],array('parent_reffer'=>$_POST['parent_reffer'],'debug'=>0)) ;
       if (sizeof($new_obj_rec))
       {   if ($_SESSION['descr_clss'][$_POST['clss']]['details_create'])
           { $this->add_element('new_window','fra_viewer.php?obj_reffer='.$new_obj_rec['_reffer']) ;
             //$this->add_element('new_window','/admin/fra_viewer.php?obj_reffer='.$new_obj_rec['_reffer']) ;

           }
           else
       { ob_start() ;
         $options['only_tr_content']=1 ; // нужно отдаль только внутренний контент страницы
         $options['no_header']=1 ;
         $options['read_only']=$_POST['read_only'] ;
         $options['no_icon_edit']=$_POST['no_icon_edit'] ;
         $options['no_icon_photo']=$_POST['no_icon_photo'] ;
         $options['no_view_field']=$_POST['no_view_field'] ;
         $options['no_check']=$_POST['no_check'] ;
         $obj_clss->show_list_items($new_obj_rec['tkey'],'pkey='.$new_obj_rec['pkey'],$options) ; // нужно отдаль только внутренний контент страницы
         $text=ob_get_clean() ;
         $this->add_element('tr_content',$text,array('reffer'=>$new_obj_rec['_reffer'])) ;

           }
       }
       else {?><div class=alert>Не удалось создать новый объект "<?echo $clss_name ?>"</div><?}
     }
   }

   // генерация HTML-JS кода для панели результатов создания объекта
   function create_obj_after_dialog()
   { $obj_clss=_CLSS($_POST['clss']) ;
     $clss_name=$obj_clss->name($_POST['parent_reffer']) ;
     //$this->add_element('title','Создание "'._CLSS($clss)->name.'"') ;
     $this->add_element('title','') ;
     ?><br>Создаем новый объект "<span class="black bold"><?echo $clss_name?></span>"....<br><?
     $rec=$obj_clss->create_obj_after_dialog($_POST) ;
     echo (is_array($rec) or $rec='ok')? '<span class=info>ОК</span><br><br>':'<span class=red>Ошибка</span><br><br>' ;
     if (is_array($rec))
     {  ob_start() ;
        $options['only_tr_content']=1 ; // нужно отдаль только внутренний контент страницы
        $options['no_header']=1 ;
        $options['read_only']=$_POST['read_only'] ;
        $options['no_icon_edit']=$_POST['no_icon_edit'] ;
        $options['no_icon_photo']=$_POST['no_icon_photo'] ;
        $options['no_view_field']=$_POST['no_view_field'] ;
        $options['no_check']=$_POST['no_check'] ;
        $obj_clss->show_list_items($rec['tkey'],'pkey='.$rec['pkey'],$options) ;
        $text=ob_get_clean() ;
        $this->add_element('tr_content',$text,array('reffer'=>$rec['_reffer'])) ;
     }
     // запомимаем текущее положение select выбора действия
     $_SESSION['form_values']['after_clss_'.$obj_clss->clss.'_create_action']=$_POST['after_clss_'.$obj_clss->clss.'_create_action'] ;
     switch($_POST['after_clss_'. $obj_clss->clss.'_create_action'])
     {  case 'go_list':    // если в POST нет list_id - дописываем код на переход по url
                           if (!$_POST['list_id'] and $_POST['list_id']!='undefined'){?><script type="text/javascript">document.location="<?echo '/'._ADMIN_.'/fra_viewer.php?obj_reffer='.$rec['_parent_reffer'] ;?>"</script><?} break ;
                           break ;
        case 'go_this':    ?><script type="text/javascript">document.location="<?echo '/'._ADMIN_.'/fra_viewer.php?obj_reffer='.$rec['_reffer'] ;?>"</script><? break ;
        case 'go_create':  $obj_clss->obj_create_dialog($_POST) ;
     }

   }

   function dialog_create_obj_by_list()
    { $this->add_element('title','Создание объектов по списку') ;
      if (!isset($_POST['clss']) or !isset($_POST['parent_reffer'])) return ;
      ?><form method="POST">Выберите тип создаваемого объекта: <?
        list($pkey,$tkey)=explode('.',$_POST['parent_reffer']) ;
        $arr_clss=_CLSS($_POST['clss'])->get_adding_clss($tkey,$pkey) ;
        ?><select size="1" name="select_clss"><?
        if (sizeof($arr_clss)) foreach($arr_clss as $clss=>$name)
            if (!_CLSS($clss)->no_create_by_list) {?><option value="<?echo $clss?>"><?echo $name?></option><?}
        ?></select><br>
           <br><strong>Введите список названий объектов</strong>, каждое название с новой строки<br><br>
            <textarea name="list_items" rows=20 cols=100 wrap="on" class=text></textarea><br>
           <br><button mode=append_viewer_main cmd=dialog_create_obj_by_list_apply parent_reffer="<?echo $_POST['parent_reffer']?>">Далее</button><br><br>
        </form>
          <?
       }

    function dialog_create_obj_by_list_apply()
    {  if ($_POST['list_items'])
          { $arr=explode("\n",$_POST['list_items']) ;
            if (sizeof($arr)) foreach($arr as $name)
            {  $name=trim($name) ;
               if ($name) { $finfo=array('obj_name'=>$name) ;
                            $obj_info=_CLSS($_POST['select_clss'])->obj_create($finfo,array('parent_reffer'=>$_POST['parent_reffer'],'debug'=>0)) ;
                            echo 'Создан "<span class=green>'.$obj_info['obj_name'].'</span>"<br>' ;
                          }
            }
            echo '<br><div class="green bold">Было добавлено '.sizeof($arr).' элементов</div><br>' ;
            ?><script type="text/javascript">document.location="<?echo '/'._ADMIN_.'/fra_viewer.php?obj_reffer='.$_POST['parent_reffer'].'&menu_id=structure_'.$_POST['select_clss'] ;?>"</script><?
          }
    }

    function list_items_search()
       { list($usl,$options)=_CLSS($_POST['clss'])->prepare_search_usl($_POST['target_search'],$_POST['search_text']) ;
          //$place_result_div=($_POST['place_result_div'])? $_POST['place_result_div']:'viewer_main' ;
          //$this->add_element('place_result_div',$_POST['place_result_div']) ;
         //echo '$usl='.$usl.'<br>' ;
         //echo '$options=' ; print_r($options) ; echo '<br>' ;
         // получаем и показывем строки
         $pages_info=_CLSS($_POST['clss'])->show_list_items($_POST['tkey'],$usl,$options) ;
          // выводим информацию по отображенным строкам
          $this->add_element('from_pos',$pages_info['from']) ;
          $this->add_element('to_pos',$pages_info['to']) ;
          $this->add_element('all_pos',$pages_info['all']) ;
          $this->add_element('next_page',$pages_info['next_page']) ;
          $this->add_element('info_count_text','1...'.$pages_info['to'].' из '.$pages_info['all'])  ;
       }


    function list_items_fast_search()
    {   $options=array() ;
        $clss=$_POST['clss'] ;
        $fname=$_POST['fname'] ;
        if ($_POST['search']) $options['usl_filter']=_CLSS($clss)->get_search_usl($fname,$_POST['search']) ;
        $this->list_items($options)  ;
    }

    function list_items_sort_change()
    {   //damp_array($_POST) ;
        if ($_POST['fname'][0]=='_') return ;
        $tkey=$_POST['tkey'] ;
        $clss=$_POST['clss'] ;
        $fname=$_POST['fname'] ;
        $order_by=$_POST['order_by'] ;
        $order_mode=$_POST['order_mode'] ;

        $this->add_element('fname',$fname) ;

        if ($order_by==$fname) $order_mode=($order_mode=='asc')? 'desc':'asc' ;
        else                   $order_by=$fname ;

        $_POST['order_by']=$order_by ;
        $_POST['order_mode']=$order_mode ;

        $this->add_element('order_by',$order_by) ;
        $this->add_element('order_mode',$order_mode) ;

        $_SESSION['table_order'][$tkey][$clss]['order_by']=$order_by ;
        $_SESSION['table_order'][$tkey][$clss]['order_mode']=$order_mode ;

        $sort_img_src=($order_mode=='asc')? _PATH_TO_ENGINE.'/admin/images/sort_down.png':_PATH_TO_ENGINE.'/admin/images/sort_up.png' ;
        $img='<img class=sort src="'.$sort_img_src.'">' ;
        $this->add_element('img',$img) ;

        $this->list_items() ;

    }

    function get_list_items($options=array())
    { list($pkey,$tkey)=explode('.',$_POST['reffer']) ;
      $options['tkey']=$tkey ;
      $options['usl']='parent='.$pkey ;
      $options['only_tr_content']=0 ;
      $options['no_header']=0 ;
      ob_start() ;
      $this->list_items($options) ;
      $text=ob_get_clean() ;
      $this->add_element('viewer_main.HTML',$text) ;
    }


    // возвращаем список записей по стандартным условиям запроса
    // нестандартные условия (поиск, фильтрация) должны быть переденаны через options
    function list_items($options=array())
    { $tkey=$_POST['tkey'] ;
      $clss=$_POST['clss'] ;
      if (isset($options['tkey'])) $tkey=$options['tkey'] ;
      if (isset($options['clss'])) $clss=$options['clss'] ;
      $options['order_by']=$_POST['order_by'] ;
      $options['order_mode']=$_POST['order_mode'] ;
      $options['no_check']=$_POST['no_check'] ;
      $options['read_only']=$_POST['read_only'] ;
      $options['no_icon_edit']=$_POST['no_icon_edit'] ;
      $options['no_icon_photo']=$_POST['no_icon_photo'] ;
      $options['no_view_field']=$_POST['no_view_field'] ;
      $options['count']=$_POST['count'] ;
      $options['page']=$_POST['page'] ;
      if (!isset($options['only_tr_content'])) $options['only_tr_content']=1 ; // нужно отдаль только внутренний контент страницы
      if (!isset($options['no_header'])) $options['no_header']=1 ;
      // формируем условие для отбора строк
      $usl=($options['usl'])? $options['usl']:stripslashes($_POST['usl']);
      if ($_POST['usl_filter']) $options['usl_filter']=stripslashes($_POST['usl_filter']) ;
      // получаем и показывем строки
      $pages_info=_CLSS($clss)->show_list_items($tkey,$usl,$options) ;
      // выводим информацию по отображенным строкам
      $this->add_element('from_pos',$pages_info['from']) ;
      $this->add_element('to_pos',$pages_info['to']) ;
      $this->add_element('all_pos',$pages_info['all']) ;
      $this->add_element('next_page',$pages_info['next_page']) ;
      $this->add_element('info_count_text','1...'.$pages_info['to'].' из '.$pages_info['all'])  ;
      //$this->add_element('select_usl',$select_usl) ;
    }

    // отдаем html код для генерации окошка быстрого поиска
    function list_items_fast_search_prepare_field()
    {
       $tkey=$_POST['tkey'] ;
       $clss=$_POST['clss'] ;
       $fname=$_POST['fname'] ;
       $this->add_element('fname',$fname) ;
       $obj_clss=_CLSS($clss) ;
       $type=get_name_type($obj_clss->fields[$fname]) ;
       switch($type)
       { case 'varchar':
         case 'text':   ?><input fname="<?echo $fname?>" class="fast_search text"><? ; break ;
         default: echo  $type ;
       }
    }

   // сохраняем данные
   function save_list()
   {   $this->add_element('mode','tr_update') ;
       ob_start() ; save_list_obj($_POST['obj']) ; $this->add_element('html',ob_get_clean()) ;
       if (sizeof($_POST['obj'])) foreach($_POST['obj'] as $tkey=>$arr_clss) foreach($arr_clss as $clss=>$arr_pkey) foreach($arr_pkey as $pkey=>$rec)
       { ob_start() ;
         $options['only_tr_content']=1 ; // нужно отдаль только внутренний контент страницы
         $options['no_header']=1 ;
         $options['read_only']=$_POST['read_only'] ;
         $options['no_icon_edit']=$_POST['no_icon_edit'] ;
         $options['no_icon_photo']=$_POST['no_icon_photo'] ;
         $options['no_view_field']=$_POST['no_view_field'] ;
         $options['no_check']=$_POST['no_check'] ;
         $options['return_cur_rec']=1 ; // вернуть имя записи для дерева
         $show_info=_CLSS($clss)->show_list_items($tkey,'pkey='.$pkey,$options) ;
         $text=ob_get_clean() ;
         //print_r($show_info) ;
         //print_r($rec_info) ;
         $this->add_element('tr_content',$text,array('reffer'=>$pkey.'.'.$tkey,'name'=>$show_info['cur_rec']['obj_name'])) ;
       }
       $this->add_element('show_notife','Сохранено',array('color'=>'green')) ;
   }

   // возвращаем одну строку для указанного объекта
   function  get_obj_tr_rec()
   {    list($pkey,$tkey)=explode('.',$_POST['reffer']) ;
        ob_start() ;
        $options['only_tr_content']=1 ; // нужно отдаль только внутренний контент страницы
        $options['no_header']=1 ;
        $options['read_only']=$_POST['read_only'] ;
        $options['no_icon_edit']=$_POST['no_icon_edit'] ;
        $options['no_icon_photo']=$_POST['no_icon_photo'] ;
        $options['no_check']=$_POST['no_check'] ;
        $options['return_cur_rec']=1 ; // вернуть имя записи для дерева
        $show_info=_CLSS($_POST['clss'])->show_list_items($tkey,'pkey='.$pkey,$options) ;
        $text=ob_get_clean() ;
        $this->add_element('tr_content',$text,array('reffer'=>$_POST['reffer'],'name'=>$show_info['cur_rec']['obj_name'])) ;
   }

   // возвращем одно поле объекта
    function get_obj_field()
     {  $reffer=$_POST['reffer'] ;
        $fname=$_POST['fname'] ;
        $obj_info=get_obj_info($reffer) ;
        $obj_clss=_CLSS($obj_info['clss']);
        //$value=$obj_clss->get_rec_field_to_view($obj_info,$fname) ;
        $value=$obj_clss->get_rec_field_to_edit($obj_info,$fname) ;
        $use_HTML_editor=$obj_clss->use_HTML_editor($obj_info,$fname) ;

        $this->add_element('pkey',$obj_info['pkey']) ;
        $this->add_element('tkey',$obj_info['tkey']) ;
        $this->add_element('clss',$obj_info['clss']) ;
        $this->add_element('fname',$fname) ;
        $this->add_element('use_html_editor',$use_HTML_editor) ;
        $this->add_element('options','',$obj_clss->fields_info[$fname]) ;
        echo htmlspecialchars($value) ; // уйдет в HTML
     }

    // возвращем массив для выпадающего списка
    function get_array_values()
    { $arr=$_SESSION[$_POST['arr_id']] ;
      $arr_field=$_POST['arr_field'] ;
      if (sizeof($arr)) foreach($arr as $id=>$rec) if (is_array($rec)) $this->add_element('arr_value',$rec[$arr_field],array('id'=>$id)) ;
                                               else                    $this->add_element('arr_value',$rec,array('id'=>$id)) ;
      if (isset($_POST['reffer']))
      { $obj_info=get_obj_info($_POST['reffer']) ;
        $this->add_element('pkey',$obj_info['pkey']) ;
        $this->add_element('tkey',$obj_info['tkey']) ;
        $this->add_element('clss',$obj_info['clss']) ;
      }
      if (isset($_POST['fname'])) $this->add_element('fname',$_POST['fname']) ;
      //print_r($_SESSION[$_POST['arr_id']]) ;
    }

    // возвращем массив для выпадающего списка
    function get_table_clss_values()
    { $tkey=$_SESSION[$_POST['tkey']] ;
      $clss=$_SESSION[$_POST['clss']] ;
      $arr=array() ;
      $_SESSION['goods_system']->tree['root']->get_path_array($arr) ;

      if (sizeof($arr)) foreach($arr as $id=>$value) $this->add_element('arr_value',$value,array('id'=>$id)) ;
      if (isset($_POST['reffer']))
      { $obj_info=get_obj_info($_POST['reffer']) ;
        $this->add_element('pkey',$obj_info['pkey']) ;
        $this->add_element('tkey',$obj_info['tkey']) ;
        $this->add_element('clss',$obj_info['clss']) ;
      }
      if (isset($_POST['fname'])) $this->add_element('fname',$_POST['fname']) ;
      //print_r($_SESSION[$_POST['arr_id']]) ;
    }

 // ajax сохранение данных
 function set_value()
 {  list($pkey,$tkey)=explode('.',$_POST['reffer']) ;
    if ($pkey!='new')
    { $clss=($_POST['clss'])? $_POST['clss']:get_obj_clss($_POST['reffer']) ;
      $obj[$tkey][$clss][$pkey][$_POST['fname']]=$_POST['value'] ;
      //print_r($obj) ;
      include_once(_DIR_TO_ENGINE."/admin/i_admin_system.php") ;
      $update_cnt=save_list_obj($obj) ;
      $this->add_element('update_cnt',$update_cnt) ;
      $this->add_element('fname',$_POST['fname']) ;
    }
    else
    {

    }
 }


  // ajax удаление данных
  function delete_check_objs()
  {  ob_start() ;
      delete_check_objs() ;
     $this->add_element('show_notife',ob_get_clean()) ;
     if (sizeof($_POST['_check'])) foreach($_POST['_check'] as $tkey=>$arr_clss) foreach($arr_clss as $clss=>$arr_pkey) foreach($arr_pkey as $pkey=>$rec) $this->add_element('remove_rows','',array('reffer'=>$pkey.'.'.$tkey)) ;
  }

  // ajax удаление данных
  function delete_check_objs_v2()
  {  ob_start() ;
        delete_check_objs() ;
     $this->add_element('show_notife',ob_get_clean()) ;
     if (sizeof($_POST['_check'])) foreach($_POST['_check'] as $tkey=>$arr_clss) foreach($arr_clss as $clss=>$arr_pkey) foreach($arr_pkey as $pkey=>$rec) $this->add_element('remove_rows','',array('reffer'=>$pkey.'.'.$tkey)) ;
     ?><script type="text/javascript"><?

     ?></script><?
  }

  // вернуть html код всех изображенией объекта для показа через HS
  /* пока не используется - не удалось номально настроить HS
  function upload_image_to_hs()
  {  list($pkey,$tkey)=explode('.',$_POST['parent_reffer']) ;
     $tkey_img=_DOT($tkey)->list_clss[3] ;
     $recs=execSQL('select pkey,obj_name,file_name,type,size from '._DOT($tkey_img)->table_name.' where parent='.$pkey.' and clss=3 order by indx',array('no_indx'=>1)) ;
     ?><div class=hidden><?
     if (sizeof($recs)) foreach($recs as $i=>$rec_image) if ($i) echo '<a href="'.img_clone($rec_image,'source').'" target=_clean class=group_'.$pkey.' rel="highslide"><img src="'.img_clone($rec_image,'small').'"></a>' ;
     ?></div><?
     $this->add_element('pkey',$pkey) ;
  }
  */

  // подгрузка содержимого дерева
  // $_POST['reffer'] - родитель
  // $_POST['clss'] - класс родителя
  function upload_object_tree_items()
  { //list($pkey,$tkey)=explode('.',$_POST['reffer']) ;
    //$arr_child_clss=_CLSS($_POST['clss'])->get_adding_clss($tkey) ;
    //print_r($arr_child_clss) ;

   //$options['use_page_view_sort_setting']=0 ; // используем настройки сортировки редактора объектов, если они таб быти определееы

   //--------------------------------------------------------------------------------------------------------------------------------------------------
   // если передан reffer объекта, значит надо подгрузить дочерние объекты
   //--------------------------------------------------------------------------------------------------------------------------------------------------
   if ($_POST['reffer'])
   { // получаем всех потомков текущего объекта
     $list_clss=array() ; $cnt=0 ;
     $list_obj=select_db_obj_child_recs($_POST['reffer'],array('order_by'=>_DEFAULT_TREE_ORDER,'fill_count_childs'=>1,'no_image'=>1)) ;
       ?><ul><? if (sizeof($list_obj)) foreach ($list_obj as $tkey_arr) if (sizeof($tkey_arr)) foreach ($tkey_arr as $clss=>$recs) { $cnt+=_CLSS($clss)->print_list_recs_to_tree($recs) ; $list_clss[]=$clss ; }?></ul><?
     generate_clss_style_icon($list_clss) ;
     // получаем инфу по текущему объекту без инфы по дочерним объектам
     $cur_obj=get_obj_info($_POST['reffer'],array('no_child_obj'=>1)) ;
     _CLSS($cur_obj['clss'])->prepare_public_info($cur_obj) ;
     $this->add_element('cur_obj_name',$cur_obj['_in_tree_name']) ;
     $this->add_element('cur_obj_child_cnt',$cnt) ;

   }


   // если раскрываемый объект - объектная таблица и эта таблица не таблица описаний объектных таблиц, включить в состав содержимое данной объектной таблицы
   //if ($obj_info['clss']==101 and $tkey!=$pkey) $obj_cat=execSQL_van('select * from '._DOT($pkey)->table_name.' where parent is null or parent=0') ;
   //if ($obj_cat['pkey']) { add_element($doc,$objects,'obj',$obj_cat['obj_name'],array('id'=>$obj_cat['pkey'],'tkey'=>$pkey,'parent'=>$pkey,'clss'=>$obj_cat['clss'],'enabled'=>$obj_cat['enabled'])) ;
   //                        $use_clss[$obj_cat['clss']]=$obj_cat['clss'] ;
   //                      }


   //--------------------------------------------------------------------------------------------------------------------------------------------------
   // если передан временной интервал - подгружаем объекты из врвменного интервала
   //--------------------------------------------------------------------------------------------------------------------------------------------------
   if ($_POST['t1'] and $_POST['t2'])
   {   $this->add_element('t1',$_POST['t1']) ;
       $this->add_element('t2',$_POST['t2']) ;

       $start_time=$_POST['t1'] ; $end_time=$_POST['t2'] ;
       $tkey=$_POST['tkey'] ; $table_name=$_SESSION['descr_obj_tables'][$tkey]->table_name ;
       $usl_select=($_POST['usl_select'])? ' and '.$_POST['usl_select']:'' ;
       $first_data=getdate($start_time) ; $last_data=getdate($end_time) ;
       $day_count=($end_time-$start_time+1)/24/60/60 ;


       // если передан день или неделя
       if (($first_data['mday']==$last_data['mday'] and $first_data['mon']==$last_data['mon'] and $first_data['year']==$last_data['year']) or
           ($first_data['wday']==1 and $last_data['wday']==0 and $day_count==7) or
           ($first_data['mday']!=$last_data['mday'] and $first_data['mon']==$last_data['mon'] and $first_data['year']==$last_data['year'])
          )
           { $time_usl=' and c_data>='.$start_time.' and c_data<='.$end_time ;
             $list_obj=select_db_recs($tkey,'parent!=0'.$time_usl.$usl_select,array('order_by'=>'c_data desc','fill_count_childs'=>1,'no_image'=>1)) ;
             $list_clss=group_by_clss($list_obj) ;

             ?><ul><? if (sizeof($list_clss)) foreach($list_clss as $clss=>$clss_recs) if (sizeof($clss_recs)) _CLSS($clss)->print_list_recs_to_tree($clss_recs) ; ?></ul><?
             generate_clss_style_icon(array_values($list_clss)) ;
           }

       //если передан диапазон месяца - раскрываем по дням
       /*
       if ($first_data['mday']!=$last_data['mday'] and $first_data['mon']==$last_data['mon'] and $first_data['year']==$last_data['year'])
       { ?><ul><?
         $start_day=$start_time ;
         while($start_day<=$end_time)
         { $now=getdate($start_day) ;
         	$end_day=mktime(23,59,59,$now['mon'],$now['mday'],$now['year']); // конец текущего дня
         	$time_usl=' and c_data>='.$start_day.' and c_data<='.$end_day ;
      		$stats=execSQL_van('select count(pkey) as cnt from '._DOT($tkey)->table_name.' where clss not in (1,10) '.$time_usl.$sql_add_usl) ;
            if ($stats['cnt']) {?><li clss=1 t1=<?echo $start_day?> t2=<?echo $end_day?> cnt=<?echo $stats['cnt']?>><?echo date('d.m.y',$start_day)?><span class=cnt>(<?echo $stats['cnt']?>)</span></li><?}
         	$start_day=mktime(0, 0, 0, $now['mon'],$now['mday']+1,$now['year']); // начало следующего дня
         }
         ?></ul><?
       }*/

       //если передан диапазон года - раскрываем по месяцам
       //print_r($first_data) ; echo '<br>' ; print_r($last_data) ;
       if ($first_data['mon']==1 and $last_data['mon']==12 and $first_data['year']==$last_data['year'])
       { ?><ul><?
         $start_month=$start_time ;
         while($start_month<=$end_time)
         { $now=getdate($start_month) ;
         	$end_month=mktime(0,0,0,$now['mon']+1,1,$now['year'])-1; // конец текущего месяца
         	$stats=execSQL_van('select count(pkey) as cnt from '.$table_name.' where parent!=0 '.$usl_select.' and c_data>='.$start_month.' and c_data<='.$end_month) ;
      		if ($stats['cnt']) {?><li clss=1 t1=<?echo $start_month?> t2=<?echo $end_month?> cnt="<? echo $stats['cnt']?>" stop_data><?echo get_month_year($start_month,'month year')?><span class=cnt>(<?echo $stats['cnt']?>)</span></li><?}
         	$start_month=mktime(0, 0, 0, $now['mon']+1,1,$now['year']); // начало следующего месяца
         }
         ?></ul><?
       }

   }

  }

}



?>