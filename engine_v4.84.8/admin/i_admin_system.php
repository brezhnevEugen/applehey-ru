<?php
include_once(_DIR_TO_ENGINE.'/class/clss_0.php') ;
include_once(_DIR_TO_ENGINE.'/admin/i_img_working.php') ;

 // ---------------------------------------------------------------------------------------------------------------------------------
 // функции обработки стандартных команд
 // ---------------------------------------------------------------------------------------------------------------------------------
 function cmd_exe_save_list() { save_list_obj($_POST['obj']) ; unset($_POST['cmd'],$_GET['cmd']) ; }

 // ---------------------------------------------------------------------------------------------------------------------------------
 // сохранение значений полей объектов
 // ---------------------------------------------------------------------------------------------------------------------------------

 function save_list_obj($list_objs)
   {  if ($GLOBALS['read_only_mode']) {?><div class=alert>Включен режим работы "только чтение", изменения запрещены</div><? return ; }

      $after_update_action=array() ; $update_count=0 ;

      include_once(_DIR_TO_ENGINE.'/admin/i_img_working.php') ;

      // сохраняем данные
      if (sizeof($list_objs)) foreach ($list_objs as $tkey=>$t_info)
	  { $table_name=_DOT($tkey)->table_name ; //echo '<strong>Работаем с '.$table_name.'</strong><br>' ;
	    if (!sizeof(_DOT($tkey)->field_info)) upload_descr_table($tkey) ;
	    $descr_table_goods=_DOT($tkey)->field_info ; //execSQL("DESCRIBE $table_name",2) ;

	    if (sizeof($t_info)) foreach ($t_info as $clss=>$c_info)
         {   $obj_clss=_CLSS($clss);
             if (sizeof($c_info)) foreach ($c_info as $pkey=>$info)
		     { $_sql=array() ;
              $obj_clss->before_save_rec($tkey,$pkey,$info) ;
              unset($info['tkey']) ;
              unset($info['_reffer']) ;
              unset($info['_parent_reffer']) ;
              //damp_array($info) ;
	          if (sizeof($info)) foreach ($info as $fname=>$fvalue)
	            if ($descr_table_goods[$fname]) // если поле присутсвует в таблице
	       		 { $is_change=1 ; // изначально производит замену значения
	       		   //echo $fname."=" ; print_r($fvalue) ; echo "(".$descr_table_goods[$fname].")<br>";
	       		   //if (is_array($fvalue)) echo implode(' ',array_keys($fvalue)).'<br>' ;
	       		   // отрабатываем событие - on_change_event - надо найти функцию создания объекта класса $clss - это новый способ
                   $is_change=$obj_clss->on_change_event($tkey,$pkey,$fname,$fvalue) ;

	       		   // пробелы, добавленные в начало и конец  multichange поля при сохранении - не трогать!!! нужны для для поиска по значению multichange!!!
                   if ($obj_clss->fields[$fname]=='multichange' and sizeof($fvalue)) $fvalue=' '.implode(' ',array_keys($fvalue)).' ' ;
	       		   //echo 'get_magic_quotes_gpc='.get_magic_quotes_gpc().'<br>' ;
	       		   //echo 'descr_table_goods['.$fname.'][Type]='.$descr_table_goods[$fname].'<br>' ;

                   // обновление для url_name делать до добавления ковычек в имя
                   if ($fname=='obj_name' and isset($descr_table_goods['url_name'])) $_sql[]="url_name='".$obj_clss->prepare_url_obj(array('obj_name'=>$fvalue,'tkey'=>$tkey,'pkey'=>$pkey))."'" ;

	       		   if (strstr($descr_table_goods[$fname],'text') or strstr($descr_table_goods[$fname],'varchar')) $fvalue=(!get_magic_quotes_gpc())? "'".addslashes($fvalue)."'":"'".$fvalue."'"; // текст заключаем в ковычки
		           //$fvalue="'".addslashes(stripcslashes($fvalue))."'"  ;

                   if (strstr($descr_table_goods[$fname],'float')) $fvalue=str_replace(array(' ',','),array('','.'),$fvalue) ;

		           if (!$fvalue and (strstr($descr_table_goods[$fname],'int') or strstr($descr_table_goods[$fname],'float') or strstr($descr_table_goods[$fname],'object') or strstr($descr_table_goods[$fname],'index'))) $fvalue=0 ;	// если значение пусто
		           if ($obj_clss->fields[$fname]=='timedata') $fvalue=strtotime($fvalue) ;


		           //print_r($obj_clss->fields[$fname]) ; echo ' --- '.$fvalue.'<br>';
		           if ($is_change) $_sql[]="$fname=".$fvalue ;

                   if ($fname=='enabled' and (_DOT($tkey)->setting['update_parent_enabled']=='php' or _DOT($tkey)->setting['update_parent_enabled']=='sql')) $after_update_action[]='update_parent_enabled' ;

	       		 }
                // обработка свойств, имена полей передаютя как
                else if (strpos($fname,'props_')!==false)
                {  $props_info=explode('_',$fname) ;
                   $props_id=$props_info[1]  ;
                   // сохраняем значение во все три поля - так как вытаскивать будем уже из нужного полдя
                   $res_sql='replace into '.$table_name.'_props (parent,id,clss,enabled,indx,value,num,str) values ('.$pkey.','.$props_id.',6,1,'.$props_id.',"'.$fvalue.'","'.$fvalue.'","'.$fvalue.'")' ;
                   //echo '<div class=alert>'.$res_sql."</div>" ;
                   mysql_query($res_sql) or die("Invalid query: " . mysql_error()."-".$res_sql);
                  $update_count++ ;
                }
              else
              {  echo 'Поле "'.$fname.'" отсутствует в таблице "'._DOT($tkey)->table_name.'". Необходимо провести проверку таблицы<br>' ;
                 //damp_array(_DOT($tkey)) ;
              }
	       	  if (isset($descr_table_goods['r_data']) and sizeof($_sql)) $_sql[]="r_data=".time() ;	// фиксируем дату обновления объекта
              if (sizeof($_sql)) { $sql=implode(',',$_sql) ;
	                       		     $res_sql="update $table_name set $sql where pkey=".$pkey ;
	                      		     //echo '<div id=alert>'.$res_sql."</div>" ;
                                     $res=execSQL_update($res_sql) ;
                                     $obj_clss->after_save_rec($tkey,$pkey) ;
                                     $update_count+=$res  ;
	                      		     // выполняем операции, отложенные на после сохранения объекта
                                     if (sizeof($after_update_action)) foreach($after_update_action as $action) switch($action)
                                     { case 'update_parent_enabled': if (_DOT($tkey)->setting['update_parent_enabled']=='php') update_parent_enabled_over_php($tkey,$pkey) ;
                                                                     if (_DOT($tkey)->setting['update_parent_enabled']=='sql') update_parent_enabled_over_sql($tkey,$pkey) ;
                                                                     break ;
                                     }
	                      		   }

	        }
         }
       }

      //echo '<strong>Данные сохранены</strong><br>' ;
      //return($update_count);
      return('ok');
    }


// ---------------------------------------------------------------------------------------------------------------------------------
// удаление отмеченных объектов
// ---------------------------------------------------------------------------------------------------------------------------------

 // удаляем объекты, что помечены checkbox, коды объектов в массиве $_POST['_check']
 function delete_check() {delete_check_objs();}
 function delete_check_objs()
	{ global $use_trash ;
      if (_ENGINE_DEMO_MODE) {?><div class=alert>Включен режим работы "только чтение", изменения запрещены</div><? return ; }
	  if (sizeof($_POST['_check']))
	  {   save_list_obj($_POST['obj']);
		  if ($use_trash) copy_check_obj_to_trash($_POST['_check']) ;
		  cmd_delete_obj($_POST['_check']) ;
	  }
      return('ok') ;
	}

 // удаляем объект
 // объект для удаления - если нет массива POST['del_obj'] удаляется объект $_POST['tkey']+$_POST['clss']+$_POST['pkey']
 function delete_obj($rec=array())
 { if (_ENGINE_DEMO_MODE) {?><div class=alert>Включен режим работы "только чтение", изменения запрещены</div><? return ; }
   if (sizeof($rec))
     { $tkey=$rec['tkey'] ;
       $clss=$rec['clss'] ;
       $pkey=$rec['pkey'] ;
     }
   if (sizeof($_POST['del_obj']))
     { $tkey=$_POST['del_obj']['tkey'] ;
       $clss=$_POST['del_obj']['clss'] ;
       $pkey=$_POST['del_obj']['pkey'] ;
     }
   else if (isset($_POST['tkey']) and isset($_POST['clss']) and isset($_POST['pkey']))
     { $tkey=$_POST['tkey'] ;
       $clss=$_POST['clss'] ;
       $pkey=$_POST['pkey'] ;
     }
   else if (isset($_GET['tkey']) and isset($_GET['clss']) and isset($_GET['pkey']))
     { $tkey=$_GET['tkey'] ;
       $clss=$_GET['clss'] ;
       $pkey=$_GET['pkey'] ;
     }
   if (isset($tkey) and isset($clss) and isset($pkey)) _CLSS($clss)->obj_delete($tkey,$pkey,array('debug'=>0)) ;
   return('ok') ;
 }



 // объекты на удаление: массив [код таблицы][clss]=[pkey,pkey,pkey,pkey....] - внимание, тут должным быть pkey в ключах массива
 // переделано в версии 4.44 30.05.11
 function cmd_delete_obj($del_info)
  {if (_ENGINE_DEMO_MODE) {?><div class=alert>Включен режим работы "только чтение", изменения запрещены</div><? return ; }

   $debug=0 ;
   if ($debug) { echo '<strong>ТЕСТОВЫЙ РЕЖИМ - имитация удаления</strong><br>объекты на удаление: ' ; damp_array($del_info,1,-1) ; echo '<br>' ; }
   if (sizeof($del_info)) foreach ($del_info as $tkey=>$clss_info)
    if (sizeof($clss_info)) foreach ($clss_info as $clss=>$obj_arr)
     if (sizeof($obj_arr))
     { $obj_clss=_CLSS($clss) ;
       $obj_clss->obj_delete($tkey,array_keys($obj_arr),array('debug'=>$debug)) ;
     }
 }

 // копируем основное изображение объекта в мусорную корзину
 function copy_obj_img_to_trash($tkey,$fname)
 {
     $source=_DOT($tkey)->dir_to_file.'source/'.$fname ;
     $desc=_DIR_TO_DELETED.'/'.$fname ;
     if (is_file($source)) rename($source,$desc) ;
 }

 // удаляет основное и клоны изображений у объекта
  function clear_obj_img($tkey,$fname,$debug=0)
  {   $list_clones=array_keys(_DOT($tkey)->list_clone_img()) ;
      $list_clones[]='small' ;
      $list_clones[]='source' ;
      foreach($list_clones as $clone_name)
      { $clone_source_fname=_DOT($tkey)->dir_to_file.$clone_name.'/'.$fname;
        if (is_file($clone_source_fname))
        { unlink($clone_source_fname) ;
          if ($debug) echo 'Удален файл '._DOT($tkey)->dir_to_file.$clone_name.'/'.$fname.'<br>' ;
        }
      }
  }



 // выполнят набор команд к базе данных, хранящихся в массиве $_POST['sql_cmd']
 function exec_sql_list_cmd()
 {  if (_ENGINE_DEMO_MODE) {?><div class=alert>Включен режим работы "только чтение", изменения запрещены</div><? return ; }
	echo "<h2>Выполнение списка команд</h2>" ;
    array_unique($_POST['sql_cmd']) ; //damp_array($_POST['sql_cmd']) ;
    if (sizeof($_POST['sql_cmd'])) foreach ($_POST['sql_cmd'] as $sql) // идем по списку отмеченных операций
      { echo $sql ;
        if (!mysql_query($sql)) echo ' - <span class=red>Error</span>'.mysql_error().'<br>';
        else                    echo ' - <span class=green>OK</span><br>' ;
      }
    echo '<h2 class=green>Все операции выполнены</h2><br>' ;
    return('ok') ;
 }


 // ---------------------------------------------------------------------------------------------------------------------------------
 // разные другие полезные функции
 // ---------------------------------------------------------------------------------------------------------------------------------

 /* сортируем элементы массива по заданому порядку */
 function arr_cmp(&$arr1,&$arr2)
  {  global $list_obj_sort_field,$list_obj_sort_mode;
     //print_r($list_obj_sort_field);
     //print_r($arr1) ;echo '<br>' ;
     //print_r($arr2) ;echo '<br>' ;
     $tkey=$arr1['tkey'] ;
  	 $clss=$arr1['clss'] ;
     if (sizeof($list_obj_sort_field[$tkey])) $cur_sort_field=$list_obj_sort_field[$tkey][$clss] ;
     if (sizeof($list_obj_sort_mode[$tkey])) $cur_sort_mode=$list_obj_sort_mode[$tkey][$clss] ;
     if (!$cur_sort_field) $cur_sort_field='pkey' ;
     if (!$cur_sort_mode) $cur_sort_mode='asc' ;

     //$cur_sort_mode='desc' ;
     if ($arr1[$cur_sort_field]==$arr2[$cur_sort_field]) $res=0 ;
     else if ($cur_sort_mode=='asc') $res=($arr1[$cur_sort_field]>$arr2[$cur_sort_field])? 1:-1 ;
     else $res=($arr1[$cur_sort_field]>$arr2[$cur_sort_field])? -1:1 ;
     //echo $cur_sort_field.' '.$cur_sort_mode.':'.$arr1[$cur_sort_field].' '.$res.' '.$arr2[$cur_sort_field].'<br>' ;
     return($res) ;
  }

 function sort_by_cur_setting(&$list_obj)
  { if (sizeof($list_obj)) foreach ($list_obj as $cur_tkey=>$tkey_arr)
     if (sizeof($tkey_arr)) foreach ($tkey_arr as $clss=>$clss_arr) { usort($list_obj[$cur_tkey][$clss],'arr_cmp') ;
    																  //print_2x_arr($list_obj[$cur_tkey][$clss]) ; echo '<br>' ;
    																}
  }

 // отправка почтового сообщения
 function send_email_message()
  { global $send_email_to,$send_email_from,$send_email_theme,$send_email_message ;
    if ($send_email_message)
    { _send_mail($send_email_to,$send_email_theme,$send_email_message,array('Content-type'=>'text/plain','use_email'=>$send_email_from,'debug'=>1)) ;
      /*?><script>alert('Ваше сообщение отправлено.');</script><?*/
    }
    return('ok');
  }


//--------------------------------------------------------------------------------------------------------------------------------------------------
//
// работа с файловой системой
//
//--------------------------------------------------------------------------------------------------------------------------------------------------

 // загрузка файла через форму
 // $href  		- ссылка на файл для загрузки
 // $dir_to     - директория, куда необходимо загрузить файл (можно как от корня есервера, так и от корня сайта)
 // $options['use_prefix'] - префик к имени файла

 function upload_file($file_info,$dir_to,$options=array())
 {  //damp_array($options) ;
    $dir_to=_DIR_TO_ROOT.hide_server_dir($dir_to) ;
    if ($options['debug']) { echo '<h2>file_info</h2>' ; print_1x_arr($file_info) ;}
    if ($options['debug']) { echo '<h2>options</h2>' ; print_1x_arr($options) ; }
    $safe_file_name=safe_file_names($file_info['name'],$options) ; // получаем безопсное для сохранения имя файла
    if (!$safe_file_name) { echo 'Не удалось получить безопасное имя файла - загрузка не удалась' ; return ; }
    $new_file_name=$dir_to.$options['use_dir'].$options['use_prefix'].$safe_file_name ;
    //$name='/'.str_replace(_DIR_TO_ROOT.'/','',$new_file_name) ;
    $name=hide_server_dir($new_file_name) ;
    if ($options['debug']) echo 'safe_file_name='.$safe_file_name.'<br>new_file_name='.$new_file_name.'<br>' ;
    if (is_file($new_file_name))  if (@unlink($new_file_name)) echo 'Старый файл удален <span class=green>успешно</span><br>' ; else { echo '<span class=red>Недостаточно прав для удаления старого файла</span> ('.hide_server_dir($new_file_name).')<br>' ; return ; }
    if ($options['view_upload']) echo 'Загружаем файл: <strong>'.$name.'</strong> ' ;
    $result=false ;
    if ($file_info['tmp_name']) $result=move_uploaded_file($file_info['tmp_name'],$new_file_name) ;
    if ($file_info['upl_name'])
    {        if (!$options['no_delete_source_img']) $result=rename($file_info['upl_name'],$new_file_name) ;
            else                                   $result=copy($file_info['upl_name'],$new_file_name) ;
    }
    if ($result)
    		{ if ($options['view_upload']) echo ' - <span class="green bold"> OK</span><br>' ;
    		  check_file_perms($new_file_name,$options['debug']) ;
    		  return ($new_file_name) ;
    		}
    else 	{ if ($options['view_upload']) echo ' - <span class=red>Не удалось получить файл по ссылке или недостаточно прав для сохранения файла</span><br>' ; return('') ; }
 }

 // загрузка файлы по ссылке
 // $href  		- ссылка на файл для загрузки
 // $dir_to     - директория, куда необходимо загрузить файл
 // $options['use_prefix'] - префик к имени файла

 function upload_file_by_href($href,$dir_to,$options=array())
 {  if (!file_exists($href))
    { if (strpos($href,'ftp://')!==false) $href=str_replace(array('ftp://','public_html/'),array('http://',''),$href) ;
      $href=urldecode($href) ;
      $safe_file_name=safe_file_names(basename($href)) ;  // получаем безопсное для сохранения имя файла
      $safe_file_href=safe_web_patch($href) ; // получаем безопасный для скачивания путь к файлу - на случай если в имени есть пробелы и прочая мура
    }
    else
    { $safe_file_href=$href ;
      $safe_file_name=safe_file_names(basename($href)) ;
    }
    if (!$dir_to) { echo '<span class="black bold">upload_file_by_href</span>: <span class=red>Не задан каталог для сохранения файла</span><br>' ; return('') ; }
    $new_file_name=$dir_to.$options['use_dir'].$options['use_prefix'].$safe_file_name ;
    $name='/'.str_replace(_DIR_TO_ROOT.'/','',$new_file_name) ;
    if ($options['debug']) echo 'source href='.$href.'<br>safe_file_href='.$safe_file_href.'<br>safe_file_name='.$safe_file_name.'<br>new_file_name='.$new_file_name.'<br>' ;
    if (is_file($new_file_name))
    { if (rename($new_file_name,$new_file_name.'_res')) echo 'Старый файл переименван <span class=green>успешно</span><br>' ;
      else 	{ echo '<span class=red>Недостаточно прав для переименования старого файла</span><br>' ;
      		  return('') ;
      		}
    }
    if ($options['view_upload']) echo 'Загружаем файл: <strong><a href="'.$name.'" target=_clean>'.$name.'</a> из '.$safe_file_href.'</strong> ' ;
    if (@copy($safe_file_href,$new_file_name))
    		{ if ($options['view_upload']) echo ' - <span class="green bold"> OK</span><br>' ;
    		  check_file_perms($new_file_name,$options['debug']) ;
    		  if (is_file($new_file_name.'_res'))
    		  { if (unlink($new_file_name.'_res')) { if ($options['view_upload']) echo 'Старый файл удален <span class=green>успешно</span><br>' ; }
    		  	else 							   { if ($options['view_upload']) echo '<span class=red>Недостаточно прав для удаления старого файла</span><br>' ;}
    		  }
    		  return ($new_file_name) ;
    		}
    else 	{ if ($options['view_upload']) echo ' - <span class=red>Не удалось получить файл по ссылке или недостаточно прав для сохранения файла</span><br>' ;
    		  //echo $new_file_name.'_res'.'<br>' ;
    		  //echo (is_file($new_file_name.'_res'))? '1':'0' ; echo '<br>' ;
    		  if (is_file($new_file_name.'_res'))
    		  { if (rename($new_file_name,$new_file_name.'_res')) { if ($options['view_upload']) echo 'Отмена переименования старого файла<span class=green>успешно</span><br>' ;}
    		  	else 											  { if ($options['view_upload']) echo '<span class=red>Недостаточно прав для переименования старого файла</span><br>' ;}
    		  }
    		  return('') ;
    		}
 }


// сохраняем загруженное изображение
// tkey  -
// ftmpname - имя временной фото ( c путем)
// fname - имя конечного фото (без пути)

 function obj_upload_file($obj_reffer,$file_info,$options=array())
{
   //damp_array($options) ;
    list($pkey,$tkey)=explode('.',$obj_reffer) ;
    $options['use_prefix']=$pkey.'_' ;
    $upload_file_dir=upload_file($file_info,_DOT($tkey)->dir_to_file,$options) ;
    //echo  '$upload_file_dir='.$upload_file_dir.'<br>' ;
    if ($upload_file_dir) //'&nbsp;&nbsp;Не удается загрузить файл по сслыке "<span class="red bold">'.$source_href.'</span>"<br>';
     {  list($size_info,$file_type)=get_small_file_info($upload_file_dir,$options) ;
        $obj_name=($file_info['obj_name'])? $file_info['obj_name']:$file_info['name'] ;
        $file_name=basename($upload_file_dir) ;
	    update_rec_in_table(_DOT($tkey)->table_name,array('obj_name'=>$obj_name,'file_name'=>$file_name,'type'=>$file_type,'size'=>$size_info),'pkey='.$pkey,array('debug'=>0)) ;
     }
    return($upload_file_dir) ;
}

 // загрузка файла для объекта
 // $tkey,$pkey - код объекта
 // href - ссылка для загрузки

 function obj_upload_file_by_href($reffer,$source_href,$options=array())
{
    //echo 'Загружаем файл для объекта:'.$reffer.'<br>' ;
    if (!$options['no_create_obj'])
	{   $obj_reffer=$reffer ;
        list($pkey,$tkey)=explode('.',$obj_reffer) ;
	    if (!$tkey) { echo 'Не указан код таблицы объекта<br>' ; return(array()) ; }
	    $finfo=$options['finfo'] ; // параметры для создания фото можно передать через options
		$finfo['parent']=$pkey ;
        if ($options['comment']) $finfo['manual']=$options['comment'] ;
        $clss_file=($options['clss_file'])? $options['clss_file']:5 ;
        include_class($clss_file) ;
        $options['tkey']=$tkey ;
        //damp_array($finfo) ;
        //damp_array($options) ;
        $file_rec=_CLSS($clss_file)->obj_create($finfo,$options) ;
        $file_reffer=$file_rec['_reffer'] ;


        //$img_reffer=clss_3_obj_create($finfo,$options) ;
        if (!$file_reffer) {echo 'Не удалось создать дочерний объект файл для '.$reffer.'<br>' ; return(array()) ;}
	}
    else $file_reffer=$reffer ;

    // $reffer - уже код объекта-изображения
	list($pkey,$tkey)=explode('.',$file_reffer) ;

   //list($pkey,$tkey)=explode('.',$reffer) ;
   $source_href=urldecode($source_href) ;

   if (!$options['no_use_id_in_name']) $options['use_prefix']=$pkey.'_' ;
   //damp_array(_DOT($tkey)) ;
   $upload_file_dir=upload_file_by_href($source_href,_DOT($tkey)->dir_to_file,$options) ;
   if ($upload_file_dir)
     { list($size_info,$file_type)=get_small_file_info($upload_file_dir,$options) ;
       $fname=basename($upload_file_dir) ;
       $ftitle=($options['name'])? $options['name']:$fname ;
       $ftitle=($options['finfo']['obj_name'])? $options['finfo']['obj_name']:$ftitle ;

	   update_rec_in_table(_DOT($tkey)->table_name,array ('obj_name'=>$ftitle,'file_name'=>$fname,'type'=>$file_type,'size'=>$size_info),'pkey='.$pkey) ;
	   return(array($file_reffer,$upload_file_dir)) ;
     }
   else echo '<span class="black bold">obj_upload_file_by_href</span>: Не удается загрузить файл по сслыке "<span class="red bold">'.$source_href.'</span>"<br>';
   return(array()) ;
}

// копируем файл в пределах сервера
function obj_move_file_by_href($reffer,$source_href,$options=array())
{
  list($pkey,$tkey)=explode('.',$reffer) ;

  $options['use_prefix']=$pkey.'_' ;
  //damp_array(_DOT($tkey)) ;
  $upload_file_dir=upload_file_by_href($source_href,_DOT($tkey)->dir_to_file,$options) ;
  if ($upload_file_dir)
    { list($size_info,$file_type)=get_small_file_info($upload_file_dir,$options) ;
      $name=basename($upload_file_dir) ;
   update_rec_in_table(_DOT($tkey)->table_name,array ('obj_name'=>$name,'file_name'=>$name,'type'=>$file_type,'size'=>$size_info),'pkey='.$pkey) ;
   return($upload_file_dir) ;
    }
  else echo '<span class="black bold">obj_upload_file_by_href</span>: Не удается загрузить файл по сслыке "<span class="red bold">'.$source_href.'</span>"<br>';
}


 // создаем дочерный объект фото и загружаем изображение
 // reffer - код объекта, для которого необходимо создать фото, в формате pkey.tkey
 // $cur_file_info - информация по загружаемому изображению:
 //     [name]
 //     [tmp_name]
 // $options[]
 function obj_upload_image($obj_reffer,$cur_file_info,$options=array())
 {
    //echo 'Загружаем файл для объекта:'.$obj_reffer.'<br>' ;
	//echo 'Создан объект ' ; print_r($obj_ref) ; echo '<br>' ;
    // проверяем, что файл не архив, если архив, то надо распаковать его и добавить к объекту все файлы
    //damp_array($cur_file_info) ;
    if (strpos($cur_file_info['name'],'.zip')!==false)
    { echo '<div class="black bold">Будет произведена распаковка архива</div>' ;
      list($temp_dir,$files_info)=unpack_arhive($cur_file_info) ;
      //damp_array($files_info) ;
      //$options['debug']=2 ;
      //$options['view_upload']=2 ;
      if (sizeof($files_info)) foreach($files_info as $file_info) obj_upload_image($obj_reffer,$file_info,$options) ;
 	  if (!rmdir($temp_dir)) echo 'Не удалось удалить временный каталог \''.$temp_dir.'\' <br>' ; else echo 'Удален временный каталог \''.$temp_dir.'\' <br>' ;
      return('') ;
    }

    list($pkey,$tkey)=explode('.',$obj_reffer) ;
    $img_clss=($options['clss'])? $options['clss']:3 ;
    //include_class(3) ;
    //$img_reffer=clss_3_obj_create(array('parent'=>$pkey,'indx'=>$options['img_indx']),array('tkey'=>$tkey,'debug'=>0,'no_reg_events'=>$options['no_reg_events'])) ;
    //echo 'clss='.$clss.'<br>' ;
    $obj_clss=_CLSS($img_clss);
    $new_obj_rec=array('parent'=>$pkey,'indx'=>$options['img_indx']) ;
    if (isset($options['new_obj_rec']) and $options['new_obj_rec']) $new_obj_rec=array_merge($new_obj_rec,$options['new_obj_rec']) ;
    $img_rec=$obj_clss->obj_create($new_obj_rec,array('debug'=>0,'tkey'=>$tkey,'no_reg_events'=>$options['no_reg_events'])) ;
    $img_reffer=$img_rec['_reffer'] ;
    $img_pkey=$img_rec['pkey'] ;
    $img_tkey=$img_rec['tkey'] ;
	if (!$img_reffer) {echo 'Не удалось создать дочерний объект "'._CLSS($img_clss)->name.'" для '.$obj_reffer.'<br>' ; return('') ;}

	// загружаем рисунок как обычный файл
	$options['use_dir']='source/' ;
	$file_dir=obj_upload_file($img_reffer,$cur_file_info,$options) ;  // => /var/www/roman/data/www/auto.clss.ru/public/colors/source/11_photo-0049.jpg
    $img_name=basename($file_dir) ; // => 11_photo-0049.jpg
	// проверяем, возможно ли создание клонов для данного типа изображений
	if (!$file_dir) return($img_reffer) ;
    if (_CONVERT_UPLOADED_GIF_TO_JPG)
    { if ($options['view_upload']) echo 'Проверяем тип изображения для возможной конвертации в JPG<br>' ;
      $file_dir=check_image_type($file_dir,$options) ;
      // обновляем информацию в объекте - конвертация могла изменить размеры
	  list($size_info,$file_type)=get_small_file_info($file_dir,$options) ;
	  update_rec_in_table(_DOT($img_tkey)->table_name,array('file_name'=>$img_name,'type'=>$file_type,'size'=>$size_info),'pkey='.$img_pkey,array('debug'=>2)) ; // => update obj_site_goods_image set file_name='11_photo-0049.jpg',type='image/jpeg',size='14582',r_data='1348148850' where pkey=11
    }
	_CLSS(3)->after_save_rec($img_tkey,$img_pkey) ;

	// если создание клонов возможно и не запрещено - создаем клоны
    include_once(_DIR_TO_ENGINE."/admin/i_img_working.php") ;
	if ($file_dir and !$options['no_clone']) create_clone_image($img_tkey,$img_name,$options) ;
    if ($options['return_clone_url']) return(hide_server_dir(_DOT($img_tkey)->dir_to_file.$options['return_clone_url'].'/'.$img_name)) ;
    return($img_reffer) ;
 }

 // 1. полный цикл загрузки изображения: создаем объект "Фото", загружаем файл в source и создаем клоны
 // reffer - объект, для которого необходимо загрудить фото
 // $source_href - адрес, откуда надо загружить фото

 // 2. малый цикл загрудки изображения: загружаем файл в source и создаем клоны
 // reffer - объект-изображение, на который загружается файл
 // $source_href - адрес, откуда надо загружить фото
 // $options['no_create_obj']=1
 function obj_upload_image_by_href($reffer,$source_href,$options=array())
 {
    //echo 'Загружаем файл для объекта:'.$reffer.'<br>' ;
    if (!$options['no_create_obj'])
	{   $obj_reffer=$reffer ;
        list($pkey,$tkey)=explode('.',$obj_reffer) ;
	    if (!$tkey) { echo 'Не указан код таблицы объекта<br>' ; return('') ; }
	    $finfo=$options['finfo'] ; // параметры для создания фото можно передать через options
		$finfo['parent']=$pkey ;
        if ($options['comment']) $finfo['manual']=$options['comment'] ;
        include_class(3) ;
        $options['tkey']=$tkey ;
        $img_rec=_CLSS(3)->obj_create($finfo,$options) ;
        $img_reffer=$img_rec['_reffer'] ;
        if (!$img_reffer) {echo 'Не удалось создать дочерний объект фото для '.$reffer.'<br>' ; return('') ;}
	}
    else $img_reffer=$reffer ;

    // $reffer - уже код объекта-изображения
	list($img_pkey,$img_tkey)=explode('.',$img_reffer) ;
	//print_r($obj_img_ref);

	// загружаем рисунок как обычный файл
	$options['use_dir']='source/' ;
    $options['no_create_obj']=1 ;
    $options['use_prefix']=$img_pkey.'_' ;

	//list($file_reffer,$file_dir)=obj_upload_file_by_href($img_reffer,$source_href,$options) ;
    $file_dir=upload_file_by_href($source_href,_DOT($img_tkey)->dir_to_file,$options) ;
	if ($file_dir)
	{	// проверяем, возможно ли создание клонов для данного типа изображений
        // если создание клонов для текущего формата невозможно, изображение будет сохранено в другом формате
	    $check_file_dir=check_image_type($file_dir,$options) ;

        list($size_info,$file_type)=get_small_file_info($check_file_dir,$options) ;
	    $fname=basename($check_file_dir) ;
	    $ftitle=($options['name'])? $options['name']:$fname ;
	    $ftitle=($options['finfo']['obj_name'])? $options['finfo']['obj_name']:$ftitle ;

		update_rec_in_table($img_tkey,array ('obj_name'=>$ftitle,'file_name'=>$fname,'type'=>$file_type,'size'=>$size_info),'pkey='.$img_pkey) ;

        // если создание клонов возможно и не запрещено - создаем клоны
		if ($check_file_dir and !$options['no_clone']) create_clone_image($img_tkey,basename($check_file_dir),$options) ;
		return (array($img_reffer,$file_dir)) ;  // 30.05.11 - в идеале бы вернуть размер файла
	}
    else
    {  echo 'Файл не был загружен<br>' ;
       return (array($img_reffer,'')) ;
    }
 }


 // удаляем все фото объекта
 function erase_images($reffer)
 {
   //echo 'Удаляем все фото объекта<br>' ;
   list($pkey,$tkey)=explode('.',$reffer) ;
   $tkey_image=_DOT($tkey)->list_clss[3] ;
   $list_pkeys=execSQL_line('select pkey from '._DOT($tkey_image)->table_name.' where parent='.$pkey) ;
   if (sizeof($list_pkeys))
   { $del_info[$tkey_image][3]=array_flip($list_pkeys) ;
     cmd_delete_obj($del_info) ;
   }
 }

 // возвращает в массиве размер и тип файла $file_dir
 // если передан параметр $show_info=1, выводится сообщение данными

 function get_small_file_info($file_dir,$options=array())
 {  $size_info=filesize($file_dir) ;
    if ($options['view_upload']) echo 'Размер файла <span class=green>'.format_size($size_info).'</span>' ;
    $file_type=mime_content_type($file_dir) ;
    if ($options['view_upload']) {if ($file_type) echo ', формат файла: <span class="black bold">'.$file_type.'</span><br>' ; else echo '<br>' ;}
    return(array($size_info,$file_type)) ;
 }

 // возвращает в простом массиве список всех поддиректорий указанной директории
 function read_dir_list($dir,$name,&$list_dir=array(),$level=0)
  { $list_obj=scandir($dir) ;
    $list_dir[]=$name ;
   	$level++ ;
   	//echo $level.'<br>' ;
   	if (sizeof($list_obj)) foreach($list_obj as $i=>$fname) if ($fname!='.' and $fname!='..')
	  { $cur_obj=$dir.$fname ;
	    if (is_dir($cur_obj)) read_dir_list($cur_obj.'/',$name.$fname.'/',$list_dir,$level) ;
         }
}




  // проверить наличие каталога изображений для объектной таблицы
 function check_obj_table_file_dir($tkey)
 {
    // проверяем, существует ли каталог для хранения изображений
    // если нет, то создаем
    $show_file_pars=0 ;
    $cur_table=_DOT($tkey) ;
    if (sizeof($cur_table->list_clss)) foreach($cur_table->list_clss as $clss=>$clss_tkey)
    {
        $obj_clss=_CLSS($clss);
        if (is_object(_CLSS($clss)) and $obj_clss->has_parent_clss(5) and $cur_table->list_clss[$clss]==$tkey) $show_file_pars=1 ;
    }


    if (_DOT($tkey)->dir_to_file and $show_file_pars)
    { echo "<br><strong>Проверка существования каталога файлов для '"._DOT($tkey)->table_name."'</strong><br>" ;
      $dir=_DOT($tkey)->dir_to_file ;
      //if (is_dir($dir))
      {  echo $dir.": " ;
         if (!is_dir($dir)) create_dir($dir) ; else echo 'OK<br>'  ;

	  } //else echo 'Каталог '.$dir.' не существует<br>' ;
	  echo '<br>' ;
	}
 }

 // возвращает ссылку для открытия объекта в новом окне
 // $rec - запись по объекту
 // !!! ДОРАБОТАТЬ: перенести в класс
 function generate_ref_obj_viewer($rec,$no_href=0,$show_img=0)
 {  // при показе имен персонала не надо делать их гиперссылками
    if ($rec['tkey']==$_SESSION['pkey_by_table'][TM_PERSONAL]) $no_href=1 ;
    $obj_clss=_CLSS($rec['clss']);
    $class_name=$obj_clss->name ;
    switch ($rec['clss'])
    { case 15: $ref_text='Тема № '.$rec['parent'].' от '.date('d.m.Y H:i',$rec['c_data']) ; break ;
      case 16: $ref_text='Тема № '.$rec['pkey'].' от '.date('d.m.Y H:i',$rec['c_data']) ; break ;
      case 0: // объект без класса - может быть запись журнала
      		   if ($rec['addr']) $ref_text=$rec['addr'] ;
      		   //$no_href=1 ;
      		   break ;
      default: if ($rec['obj_name']) $ref_text=$rec['obj_name'] ;
               else                  $ref_text=$obj_clss->name.' # '.$rec['pkey'] ;
    }
    //if ($rec['obj_name'])
    //      $ref_text=$rec['obj_name'] ;
    // else $ref_text=$rec['addr'].' - '.$rec['uri'] ;
    //print_r($rec).'<br><br><br>';
    if ($ref_text)
    { if ($show_img) $res='<img alt="" src="'.img_clone($rec,'small').'" border="0"><br>' ;
      if (!$no_href) $res.=$class_name.': <a href="" class="a_ref" onclick="show_obj('.$rec['tkey'].','.$rec['pkey'].',1);return(false);">'.$ref_text.'</a>' ;
                else $res.=$ref_text ;
    }
    //echo $res.'<br>' ;
    return($res) ;
 }

?>