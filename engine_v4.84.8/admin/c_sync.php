<?php
// скрипт на удаление, более нигде не вызывается

include_once (_DIR_TO_ENGINE.'/c_page_XML.php') ;
class c_sync extends c_page_XML
{


 function show()
 {  global $doc,$yml,$cmd,$tkey,$pkey,$find_source,$use_style,$no_xml ;

	 // скрипт имеет право вызываться только из скриптов текущего сервера или из clss.ru
	 //$url_arr=parse_url($_SERVER['HTTP_REFERER']) ;

	 //echo 'host='.$url_arr['host'].'<br>' ;
	 //echo 'SERVER_NAME='.$_SERVER['SERVER_NAME'].'<br>' ;
	 //echo 'REMOTE_ADDR='.$_SERVER['REMOTE_ADDR'].'<br>' ;

	 //if ($cmd==4) damp_array($_SERVER) ;
	 //if ($url_arr['host']!=$_SERVER['SERVER_NAME'] and strpos($_SERVER['REMOTE_ADDR'],'127.0.0.1')===false) {echo 'Доступ запрещен' ; exit ; }
	 if($use_style==1)
	 {?><style type="text/css">
		table{table-layout:auto;width:auto;border-collapse:collapse;position:relative;margin: 0 0 10px 0; padding: 0;background-color: #F9F9F7;font-family:Tahoma,Arial,sans-serif;font-size:11px;}
		table tr.clss_header{color:#23598c;height:30px;background-color:#FFFDE8;padding:5px;font-weight: bold;text-align:left;}
		table tr.clss_header span.title{display:block;float:left;font-weight: bold;text-align:left;padding-left:5px;}
		table tr.clss_header span.title img{margin-bottom:-4px;}
		table tr.clss_header span.setting{display:block;float:right;text-align:right;padding-right:5px;}
		table tr.clss_header span a:hover{color:black;text-decoration:underline;}
		table tr.td_header{background-color:#F0F4F9;color:#222222;}
		table td{width:auto;border:1px solid #E1E1E1;text-align:center;padding:2px 4px;}
		table td img{ margin:0 3px -2px 3px;}
		table.left td{ text-align: left; }
	   </style>
	 <?}
     else header("Content-type: text/xml;") ;

	 //$cmd=$_GET['cmd'] ;
	 $tkey=$_GET['tkey'] ; $pkey=$_GET['pkey'] ; $find_source=$_GET['find_source'] ;
     if (isset($_GET['reffer'])) list($tkey,$pkey)=explode('_',$_GET['reffer']) ;

	 //echo 'cmd='.$cmd.'<br>' ;
     //exit ;
	 // готовим DOM-XML объект
	 $imp = new DOMImplementation; // Creates an instance of the DOMImplementation class
	 $doc = $imp->createDocument("",""); // Creates a DOMDocument instance
	 $doc->encoding = 'utf-8';
     $doc->formatOutput = true;
	 $yml = add_element($doc,$doc,'objs_export','',array('date'=>date('Y-m-d H:i'))) ;
     ob_start() ;
	 // выгружаем содержимое по коду команды
	 switch ($cmd)
	 { case 1: $this->export_childs() ;  	break;
	   case 2: $this->find_objs() ;		break ;
	   case 3: $this->get_img_info() ;		break ;
	   case 4: $this->get_public_info() ;  break ;
	   case 5: $this->filter_objs() ;  	break ;
	   case 6: $this->get_files() ;  		break ;
	   case 10: $this->get_dir_content() ;  		break ;
	   case 14: $this->get_obj_images_icons() ;  		break ;

	 }
     $text=ob_get_clean() ;
     add_element($doc,$yml,'html',$text) ;
	 // выгружаем XML если никакого вывода еще не было (может быть при отладке)
	 if (!headers_sent($filename,$linenum))
	 {  if (!$no_xml) echo $doc->saveXML() ;
		 else
		 { header("Content-type: text/plain; charset=utf-8");
		   header("Cache-Control: no-store, no-cache, must-revalidate");
		   header("Cache-Control: post-check=0, pre-check=0", false) ;
		   if ($cmd<100) echo $doc->saveXML() ;
		 }
	 }
	 else
	 {
	 	 //echo "Заголовки уже отправлены в $filename на строке $linenum\n" ;
	 }
 }


 // dозвращаем содержимое каталога $pkey
 function get_dir_content()
 {  global $pkey ;
    $dir_name=$pkey ;
    $list_dir=get_dir_list(_DIR_TO_ROOT.$dir_name) ;
    $list_files=get_files_list(_DIR_TO_ROOT.$dir_name) ;
    //damp_array($list_dir) ;
    //damp_array($list_files) ;


 }


 function get_files()
 { global $pkey,$only_dirs,$use_filter ;
   $dir_name=$pkey ;
   $list_dir=get_dir_list(_DIR_TO_ROOT.$dir_name) ;
   if (!$only_dirs) $list_files=get_files_list(_DIR_TO_ROOT.$dir_name) ;
   if ($use_filter) $no_index=get_no_index_dirs() ; else $no_index=array() ;
   $list=array() ;
   $i=0 ;
   $str_cnt='' ;
   if (sizeof($list_dir)) foreach($list_dir as $name=>$dir) if (!$use_filter or ($use_filter and !in_array($dir,$no_index)))
    {  $cnt_cont=sizeof(scandir($dir))-2 ;
       //$str_cnt=($cnt_cont)? ' ['.$cnt_cont.']':'' ;
       $list[1][1][$i]=array('tkey'=>1,'parent'=>-3,'pkey'=>hide_server_dir($dir),'clss'=>1,'enabled'=>1,'indx'=>$i,'obj_name'=>$name.$str_cnt,'cnt'=>$cnt_cont) ;
       $i++ ;
    }
   if (sizeof($list_files)) foreach($list_files as $fname)
   {   $name=basename($fname) ;
       $list[1][1][$i]=array('tkey'=>1,'parent'=>-3,'pkey'=>$dir_name.$name,'clss'=>0,'enabled'=>1,'indx'=>$i,'obj_name'=>$name,'cnt'=>0) ;
       $i++ ;
   }
   //damp_array($list);
   $this->export_obj_to_xml($list,0,1) ;
 }

 // ФУНКЦИЯ ИСПОЛЬЗУЕТ УСТАРЕВШИЕ ДАННЫЕ !!!!!!!!!
 function get_public_info()
 { //echo 'Служебная информация по сайту<br>' ;
   global $doc,$yml ;
   $_shop_name=execSQL_van('select * from '.TM_SETTING.' where comment="shop_name"') ;
   $_shop_company=execSQL_van('select * from '.TM_SETTING.' where comment="shop_company"') ;
   $public_info=add_element($doc,$yml,'public_info','') ;
   add_element($doc,$public_info,'shop_name',$_shop_name['value']) ;
   add_element($doc,$public_info,'shop_company',$_shop_company['value']) ;

   $setting=add_element($doc,$yml,'setting','') ;
   $this->var_to_element($doc,$setting,'engine_vers') ;
   $this->var_to_element($doc,$setting,'reboot_all') ;
   //$this->var_to_element($doc,$setting,'use_cache') ;
   $this->var_to_element($doc,$setting,'time_cache_life') ;
   $this->var_to_element($doc,$setting,'debug') ;
   $this->var_to_element($doc,$setting,'debug_SQL_query') ;
   $this->var_to_element($doc,$setting,'debug_mails') ;
   $this->var_to_element($doc,$setting,'time_line_mode') ;
   $this->var_to_element($doc,$setting,'log_system_info') ;
   $this->var_to_element($doc,$setting,'log_logons') ;
   $this->var_to_element($doc,$setting,'info_db_error') ;
   $this->var_to_element($doc,$setting,'send_mail_by_error') ;
   $this->var_to_element($doc,$setting,'auto_generate_stats') ;
   $this->var_to_element($doc,$setting,'auto_erase_logs') ;

   // выкладываем размеры журналов
   $table_info=execSQL('show table status  like "log%"') ;
   $log_info=add_element($doc,$yml,'log_info','') ;
   if (sizeof($table_info)) foreach($table_info as $info) add_element($doc,$log_info,$info['Name'],$info['Rows']) ;

   global $modules ;
   $modules_info=add_element($doc,$yml,'modules','') ;
   if (sizeof($modules)) foreach($modules as $name) add_element($doc,$modules_info,$name) ;


   //$this->var_to_element($doc,$public_info,'') ;
   //$this->var_to_element($doc,$public_info,'') ;
   //$this->var_to_element($doc,$public_info,'') ;


 }

 function var_to_element(&$doc,&$public_info,$var_name)
 { 	global ${$var_name} ;
 	add_element($doc,$public_info,$var_name,${$var_name}) ;
 }

 //
 function get_img_info()
 { global $tkey,$pkey,$doc,$yml ;
   $dir=_DOT($tkey)->dir_to_file.'source/' ;
   $img_name=execSQL_van('select obj_name from '._DOT($tkey)->table_name.' where pkey='.$pkey) ;
   $size=getimagesize($dir.$img_name['obj_name']) ;
   add_element($doc,$yml,'img_info','',array('name'=>_DOT($tkey)->patch_to_file.'source/'.$img_name['obj_name'],'width'=>$size[0]+35,'height'=>$size[1]+30)) ;
 }

 function get_obj_images_icons()
 { global $doc,$yml ;
   list($tkey,$pkey)=explode('_',$_GET['reffer']) ;
   $arr=select_db_obj_child_recs($_GET['reffer']) ;
   $tkey_img=_DOT($tkey)->list_clss[3] ;
   ob_start() ;
   include('i_clss_func.php') ;
   _CLSS(3)->show_based_obj_list($arr[$tkey_img][3]) ;
   $text=ob_get_clean() ;
   add_element($doc,$yml,'div_inner_html',$text) ;
 }




 // экспортирование дочерних объектов
 function export_childs()
 { global $tkey,$pkey,$obj_info ;
   if (!sizeof(_DOT($tkey))) { /*echo 'sizeof(_DOT('.$tkey.')=0' ; */	return ;}
   $obj_info=execSQL_van('select * from '._DOT($tkey)->table_name.' where pkey='.$pkey) ;
   if (!$obj_info['pkey']) return ;

   $options['debug']=0 ;
   $options['no_connect_table']=1 ;
   //$options['order_by']='indx' ;
   $options['order_by']=_DEFAULT_TREE_ORDER;
   $options['use_page_view_sort_setting']=0 ; // используем настройки сортировки редактора объектов, если они таб быти определееы
   $options['get_all_child']=1 ;

   // получаем всех потомков текущего объекта
   //damp_array($options) ;
   $list_obj=select_db_obj_child_recs($pkey.'.'.$tkey,$options) ;
   //damp_array($list_obj) ;

   // ДОРАБОТАТЬ: почему подсчет количества только для 1 и 10 класса?
   if (sizeof($list_obj[$tkey][1]))  $this->update_count_childs($list_obj[$tkey][1],$tkey) ;
   if (sizeof($list_obj[$tkey][10])) $this->update_count_childs($list_obj[$tkey][10],$tkey) ;
   //print_r($list_obj) ;

   $this->export_obj_to_xml($list_obj) ;
 }

 function update_count_childs(&$arr_obj,$tkey)
 {
 	$list_pkeys=implode(',',array_keys($arr_obj)) ;
 	$count_info=execSQL('select parent,count(pkey) as cnt from '._DOT($tkey)->table_name.' where parent in ('.$list_pkeys.') group by parent order by parent asc') ;
    if (sizeof($arr_obj)) foreach($arr_obj as $id=>$rec) $arr_obj[$id]['cnt']=(isset($count_info[$rec['pkey']]))? $count_info[$rec['pkey']]['cnt']:0 ;
 }

 function find_objs()
 { global $tkey,$find_source ;

   $options['no_image']=1 ;
   $options['debug']=0 ;
   $options['no_connect_table']=1 ;
   $options['order_by']='indx' ;
   $options['found_text']=$find_source ;
   $list_obj=select_objs($tkey,'','',$options)  ;
   if (!sizeof($list_obj[$tkey])) $list_obj[$tkey][0][]=array('tkey'=>$tkey,'parent'=>'_filter','pkey'=>0,'obj_name'=>'Ничего не найдено','clss'=>0,'enabled'=>1) ;

   $this->export_obj_to_xml($list_obj,'_find') ;
 }

 function filter_objs()
 { global $tkey,$filter,$find_source ;

   $options['no_image']=1 ;
   $options['debug']=0 ;
   $options['no_connect_table']=1 ;
   $options['order_by']='indx' ;
   $options['found_text']=$find_source ;
   //damp_array($arr_filter[_DOT($tkey)->table_name][$filter]) ;
   $sql='SELECT *, '.$tkey.' as tkey FROM '._DOT($tkey)->table_name.' t '.$_SESSION['arr_filter'][_DOT($tkey)->table_name][$filter]['sql'] ;
   //echo $sql ;
   $_list_obj=execSQL($sql) ;
   if (sizeof($_list_obj)) $list_obj[$tkey]=group_by_field('clss',$_list_obj) ;
    else $list_obj[$tkey][0][]=array('tkey'=>$tkey,'parent'=>'_filter','pkey'=>0,'obj_name'=>'Ничего не найдено','clss'=>0,'enabled'=>1) ;

   $this->export_obj_to_xml($list_obj,'_filter') ;
 }

 /* не используется в связи с переносом функции формирования текста для дерева в описание класса
    clss_0::get_tree_item_value($rec)
 function prepare_name_by_rules(&$rec,&$arr_rules)
 {  $res='' ;
 	if (sizeof($arr_rules)) foreach($arr_rules as $fname)
     { // смотрим что тип поля
       if ($_SESSION['descr_clss'][201]['list']['field'][$fname]['indx_select'])
       { $arr_name=$_SESSION['descr_clss'][201]['list']['field'][$fname]['indx_select'] ;
         $arr_filed=$_SESSION['descr_clss'][201]['list']['field'][$fname]['indx_field'] ;
         global ${$arr_name} ;
         $res.=${$arr_name}[$rec[$fname]][$arr_filed] ;
       }
       else $res.=(isset($rec[$fname]))? $rec[$fname]:$fname ;
     }
 	return($res) ;
 }
 */

 function export_obj_to_xml($list_obj,$use_parent='',$no_sort=0)
 { global $doc,$yml,$tkey,$pkey,$obj_info ;
   //damp_array($list_obj) ;
   //print_2x_arr($list_obj[3367][1]) ; echo '<br>' ;

   // 29.06.01 отключена так как работает некорректно и в общем не нужна так как сортировка идет при выборке из базы
   //if (!$no_sort) sort_by_cur_setting($list_obj) ;

   //global $list_obj ;
   //print_2x_arr($list_obj[3367][1]) ; echo '<br>' ;
   $objects= add_element($doc,$yml,'objects','',array('id'=>$pkey,'tkey'=>$tkey)) ;
   if (sizeof($list_obj)) foreach ($list_obj as $cur_tkey=>$tkey_arr)
     if (sizeof($tkey_arr)) foreach ($tkey_arr as $clss=>$clss_arr)
       if (sizeof($clss_arr)) foreach ($clss_arr as $rec)
       {   $obj_clss=_CLSS($rec['clss']);
           if (!$obj_clss->no_show_in_tree)
           {   $obj_clss->prepare_public_info($rec) ;
               $parent=($use_parent)? $use_parent:$rec['parent'] ;
               if (!isset($rec['cnt'])) $rec['cnt']=1 ;
               if (!isset($rec['enabled'])) $rec['enabled']=1 ;
               $params=array('id'=>$rec['pkey'],'tkey'=>$rec['tkey'],'parent'=>$parent,'clss'=>$rec['clss'],'enabled'=>$rec['enabled'],'cnt'=>$rec['_in_tree_cnt'],'class_name'=>$obj_clss->name) ;
               if ($rec['start_time']) $params['start_time']=$rec['start_time'] ;
               if ($rec['end_time']) $params['end_time']=$rec['end_time'] ;
               if ($rec['add_usl'])  $params['add_usl']=$rec['add_usl'] ;
               //print_r($rec) ; echo '<br>' ;
               //print_r($params) ; echo '<br>' ;
               add_element($doc,$objects,'obj',$rec['_in_tree_name'],$params) ;
               $use_clss[$rec['clss']]=$rec['clss'] ;
           }
       }

   // если раскрываемый объект - объектная таблица и эта таблица не таблица описаний объектных таблиц, включить в состав содержимое данной объектной таблицы
   if ($obj_info['clss']==101 and $tkey!=$pkey) $obj_cat=execSQL_van('select * from '._DOT($pkey)->table_name.' where parent is null or parent=0') ;



   if ($obj_cat['pkey']) { add_element($doc,$objects,'obj',$obj_cat['obj_name'],array('id'=>$obj_cat['pkey'],'tkey'=>$pkey,'parent'=>$pkey,'clss'=>$obj_cat['clss'],'enabled'=>$obj_cat['enabled'])) ;
                           $use_clss[$obj_cat['clss']]=$obj_cat['clss'] ;
                         }

 }

}

?>