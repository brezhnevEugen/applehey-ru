<?
//-----------------------------------------------------------------------------------------------------------------------------
// Главная страница адмики
//-----------------------------------------------------------------------------------------------------------------------------

include_once('c_admin_page.php') ;
class c_index extends c_admin_page
{

   function _site_top_head($options=array())
    { ?><head>
          <title><? echo $_SESSION['LS_def_page_title'].' ' ; if (isset($this)) $this->page_title($options['title']); ?></title>
          <META http-equiv=Content-Type content="text/html; charset=utf-8" />
          <META NAME="Document-state" CONTENT="Dynamic" />
          <META NAME="revizit-after" CONTENT="5 days" />
          <META name="revisit"  content="5 days" />
          <? if ($_SESSION['favicon']){?><link href="<?echo $_SESSION['favicon']?>" rel="shortcut icon"><? echo "\n\t" ;} ?>
          <script type="text/javascript" src="<?echo _PATH_TO_ADMIN_ADDONS?>/jquery-1.8.3.min.js"></script>
          <SCRIPT type="text/javascript" src="<?echo _PATH_TO_ADMIN_ADDONS?>/mBox/jquery.form.js" ></SCRIPT> <?/*используется ajaxForm для отправки форм через ajax*/?>
          <SCRIPT type="text/javascript" src="<?echo _PATH_TO_ADMIN_ADDONS?>/mootools/mootools-core-1.3.1.js" ></SCRIPT>
          <SCRIPT type="text/javascript" src="<?echo _PATH_TO_ADMIN_ADDONS?>/mootools/mootools-more-1.3.1.1.js" ></SCRIPT>
          <script type="text/javascript" src="<?echo _PATH_TO_ADMIN_ADDONS?>/cpojer-mootools-filemanager-751d4ef/Source/FileManager.js" charset="utf-8"></script>
          <script type="text/javascript" src="<?echo _PATH_TO_ADMIN_ADDONS?>/cpojer-mootools-filemanager-751d4ef/Source/Gallery.js" charset="utf-8"></script>
          <script type="text/javascript" src="<?echo _PATH_TO_ADMIN_ADDONS?>/cpojer-mootools-filemanager-751d4ef/Source/Uploader/Fx.ProgressBar.js" charset="utf-8"></script>
          <script type="text/javascript" src="<?echo _PATH_TO_ADMIN_ADDONS?>/cpojer-mootools-filemanager-751d4ef/Source/Uploader/Swiff.Uploader.js" charset="utf-8"></script>
          <script type="text/javascript" src="<?echo _PATH_TO_ADMIN_ADDONS?>/cpojer-mootools-filemanager-751d4ef/Source/Uploader.js" charset="utf-8"></script>
          <script type="text/javascript" src="<?echo _PATH_TO_ADMIN_ADDONS?>/cpojer-mootools-filemanager-751d4ef/Language/Language.ru.js" charset="utf-8"></script>
          <script type="text/javascript">
              window.addEvent('domready', function()
                         {   var file_manager = new FileManager(
                             {      url: '<?echo _PATH_TO_ADMIN_ADDONS?>/cpojer-mootools-filemanager-751d4ef/manager.php',
                                    assetBasePath: '<?echo _PATH_TO_ADMIN_ADDONS?>/cpojer-mootools-filemanager-751d4ef/Assets',
                                    //assetBasePath: _PATH_TO_ADMIN_ADDONS+'/cpojer-mootools-filemanager-751d4ef/Assets',
                                    language: 'ru',
                                    hideOnClick: true,
                                    uploadAuthData: {session: 'MySessionId'},
                                    upload: true,
                                    destroy: true,
                                    rename: true,
                                    createFolders: true
                             });
                             $$('a.file_explorer').addEvent('click', file_manager.show.bind(file_manager));
                         });
          </script>
          <script type="text/javascript" src="<?echo _PATH_TO_ENGINE?>/admin/script.js"></script>
          <link rel="stylesheet" type="text/css" href="<?echo _PATH_TO_ENGINE?>/admin/style.css">
        <script type="text/javascript">
        	var _PATH_TO_ADMIN='<?echo _PATH_TO_ADMIN?>/' ;
            var _PATH_TO_ADMIN_ADDONS='<?echo _PATH_TO_ADMIN_ADDONS?>' ;
        </script>
        </head>
     <?
    }

  function body($options=array()) { $this->body_1x() ; }

  function block_main()
  { global $engine_vers,$db_name,$log_pages_info,$alert_moduls;
    ?><table id=admin_main_menu><tr><td id=main_menu><?$_SESSION['admin_account_system']->show_menu($_SESSION['member']) ;?></td><?
  	?><td id=main_info_block><?
		if ($_SESSION['member']->id) echo 'Пользователь: '.$_SESSION['member']->name ;
		if (_CURRENT_USER=='admin' or _CURRENT_USER=='root')
		{ echo '&nbsp;&nbsp;<br />engine vers. '.$engine_vers.'<br />База:'.$db_name.'<br><br>' ;
		 if ($log_pages_info)
		  { include_once(_DIR_TO_ENGINE.'/admin/i_stats.php') ;

		    $info=get_users_active_info(10) ;
            ?><h2>Cтатистика</h2><?

		     echo 'Сегодня на сайт зашло : '.$info['all_users'].' чел.<br>' ;
		     echo 'Сразу ушло : '.$info['passive_users'].' чел.<br>' ;
		     echo 'Осталось : '.$info['active_users'].' чел.<br>' ;

		     if ($info['all_users']){?><a href="<?echo _PATH_TO_ADMIN?>/log_pages.php?cmd=21&show=1">Посмотреть входы</a><br><br><?}

		     if ($info['now_users']) { echo 'Сейчас на сайте: '.$info['now_users'].' чел.<br>' ; ?><a href="<?echo _PATH_TO_ADMIN?>/log_pages.php?cmd=20&show=1" >Посмотреть активность</a><br><br><?}
		     else echo 'Сейчас на сайте никого нет<br><br>' ;
		  }
        if ($GLOBALS['no_access_to_robots']) {?><div class="index_alert"><strong>Внимание!</strong><br>Доступ к сайту поисковиков заблокирован</div><?}
        /*if (!ini_get('register_globals')) {?><div class="index_alert"><strong>Внимание!</strong><br>Для работы движка обязательна опция register_globals=on<br>Формат для .htaccess: php_flag register_globals on</div><?} ;*/

        if (file_exists(_DIR_TO_ROOT.'/setup.php')) {?><div class="index_alert"><strong>Внимание!</strong><br>setup.php в корне</div><?}

		 if ($alert_moduls)
		 { global $__functions ;
           $func_name='' ;
		   if (sizeof($__functions['alert'])) foreach($__functions['alert'] as $func_name) $func_name() ;
		 }
		}

    ?></td></tr></table><?
  }

}