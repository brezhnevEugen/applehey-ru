<?
//ALTER TABLE  `mebel-zarechye`.`log_site_pages` ADD INDEX  `robot_session_data` (  `session_id` ( 50 ) ,  `is_robot` ,  `c_data` )
//ALTER TABLE  `mebel-zarechye`.`log_site_pages` ADD INDEX  `c_data` (  `c_data` )

function exec_send_site_stats_informations($test_mode=0)
{   $debug=0 ;
    $now=getdate() ;
    $data_start=mktime(0,0,0,$now['mon'],$now['mday']-1,$now['year']);
    $data_end=mktime(0,0,0,$now['mon'],$now['mday'],$now['year']);
    $data_next_start=mktime(0,rand(1,60),0,$now['mon'],$now['mday']+1,$now['year']);
    if ($test_mode) {?><strong>Назначаем время следующего старта запуска статистики: <?echo date('H:i:s d.m.Y',$data_next_start)?></strong><?}

    // прошли сутки - значит время обновлять статистику. Первым делом сохраняем флаг, что статистика обработана - чтобы другие не лезли
    if (!$test_mode) { execSQL_update('update '.TM_SETTING.' set value='.$data_next_start.' where comment="LS_stats_last_update"') ;
                       $_SESSION['LS_stats_last_update']=$data_next_start ;
                      }
    global $engine_vers  ;
    include_once(_DIR_TO_ENGINE.'/frames/m_log_frames.php') ;
    // собираем статистику по работе сайта для отправки на центральный сервер
    if ($test_mode) {?><h1>Cтатистика по работе сайта</h1><?}
    //if ($test_mode) $debug=1 ;
    $round_to=4 ; // округляем до 4-х знаков
    $delimeter='-----' ;

    $params['site']=_BASE_DOMAIN ;
    $params['SERVER_SOFTWARE']=$_SERVER['SERVER_SOFTWARE'] ;
    $params['IP']=$_SERVER['SERVER_ADDR'] ;
    $params['to_data']=$data_start ;
    $params['vers']=$engine_vers ;
    $params['modules']=implode(',',$GLOBALS['use_modules']) ;
    $params['active_users']=0 ;
    $params['passive_users']=0 ;
    $list= execSQL('select session_id,count(pkey) as cnt,user_agent,min(c_data) as first_data from '.TM_LOG_PAGES.' where is_robot=0 group by session_id having first_data>='.$data_start.' and first_data<='.$data_end,$debug) ;
    if (sizeof($list)) foreach($list as $rec) if ($rec['cnt']>1)  $params['active_users']++ ; else $params['passive_users']++ ;
    // число заходов роботов на сайт
    $params['robots_cnt']= execSQL_value('select count(pkey) from '.TM_LOG_PAGES.' where is_robot>0 and c_data>='.$data_start.' and c_data<='.$data_end) ;

    // анализ информации по загрузке движка
    $list_session_rec=execSQL_van('select count(pkey),min(time_gen),max(time_gen),avg(time_gen),avg(mem_size),min(sql_cnt),max(sql_cnt),avg(sql_cnt),min(sql_time),max(sql_time),avg(sql_time)  from '.TM_LOG_PAGES.' where c_data>='.$data_start.' and c_data<='.$data_end.' and is_reboot=1 order by c_data desc',$debug) ;
    $params['reboot_cnt']=$list_session_rec['count(pkey)'] ;
    $params['reboot_time_gen_min']=round($list_session_rec['min(time_gen)'],$round_to) ;
    $params['reboot_time_gen_max']=round($list_session_rec['max(time_gen)'],$round_to) ;
    $params['reboot_time_gen_avg']=round($list_session_rec['avg(time_gen)'],$round_to) ;
    $params['reboot_mem_size_avg']=round($list_session_rec['avg(mem_size)'],0) ;
    $params['reboot_sql_cnt_min']=$list_session_rec['min(sql_cnt)'] ;
    $params['reboot_sql_cnt_max']=$list_session_rec['max(sql_cnt)'] ;
    $params['reboot_sql_cnt_avg']=$list_session_rec['avg(sql_cnt)'] ;
    $params['reboot_sql_time_min']=round($list_session_rec['min(sql_time)'],$round_to) ;
    $params['reboot_sql_time_max']=round($list_session_rec['max(sql_time)'],$round_to) ;
    $params['reboot_sql_time_avg']=round($list_session_rec['avg(sql_time)'],$round_to) ;
    // анализ информации по обычной работе движка
    $list_session_rec=execSQL_van('select count(pkey),min(time_gen),max(time_gen),avg(time_gen),avg(mem_size),min(sql_cnt),max(sql_cnt),avg(sql_cnt),min(sql_time),max(sql_time),avg(sql_time) from '.TM_LOG_PAGES.' where c_data>='.$data_start.' and c_data<='.$data_end.' and (is_reboot=0 or is_reboot is null)  order by c_data desc',$debug) ;
    $params['normal_cnt']=$list_session_rec['count(pkey)'] ;
    $params['normal_time_gen_min']=round($list_session_rec['min(time_gen)'],$round_to) ;
    $params['normal_time_gen_max']=round($list_session_rec['max(time_gen)'],$round_to) ;
    $params['normal_time_gen_avg']=round($list_session_rec['avg(time_gen)'],$round_to) ;
    $params['normal_mem_size_avg']=round($list_session_rec['avg(mem_size)'],0) ;
    $params['normal_sql_cnt_min']=$list_session_rec['min(sql_cnt)'] ;
    $params['normal_sql_cnt_max']=$list_session_rec['max(sql_cnt)'] ;
    $params['normal_sql_cnt_avg']=$list_session_rec['avg(sql_cnt)'] ;
    $params['normal_sql_time_min']=round($list_session_rec['min(sql_time)'],$round_to) ;
    $params['normal_sql_time_max']=round($list_session_rec['max(sql_time)'],$round_to) ;
    $params['normal_sql_time_avg']=round($list_session_rec['avg(sql_time)'],$round_to) ;
    $params['session_size']=strlen(serialize($_SESSION)) ;
    $params['log_pages_size']= execSQL_value('select count(pkey) from '.TM_LOG_PAGES) ;
    $params['log_events_size']= execSQL_value('select count(pkey) from '.TM_LOG_EVENTS) ;
    if ($_SESSION['TM_goods']) $params['obj_goods_size']= execSQL_value('select count(pkey) from '.$_SESSION['TM_goods']) ;
    if ($_SESSION['TM_news']) $params['obj_news_size']= execSQL_value('select count(pkey) from '.$_SESSION['TM_news']) ;
    if ($_SESSION['TM_artikle']) $params['obj_art_size']= execSQL_value('select count(pkey) from '.$_SESSION['TM_artikle']) ;

    // число входов в админку
    $params['admin_in']=execSQL_value('select count(pkey)  from '.TM_LOG_EVENTS.' where c_data>='.$data_start.' and c_data<='.$data_end.' and obj_name="Вход в админку" order by c_data desc') ;

    if ($test_mode) damp_array($params,1,-1) ;
    $text=arr_to_csv($params).$delimeter ;
    if ($test_mode) {?><h1>Целостность скриптов сайта</h1><?}
    // целостность файлов сайта, кроме директории _DIR_TO_PUBLIC
    $list_files=get_all_files(_DIR_TO_ROOT.'/',array('no_include_dirs'=>array(_DIR_TO_PUBLIC.'/'),'file_pattern'=>array('php','js','css','html','htm','htaccess','htpasswd'))) ;
    //$list_files=get_all_files(_DIR_TO_ROOT.'/class/') ;
    if ($test_mode){?><h2>Проверено: <? echo sizeof($list_files)?> файлов</h2><?}
    $list_files_info=get_files_info($list_files) ;
    if ($test_mode) print_2x_arr($list_files_info) ;
    $text.=arr_to_csv($list_files_info).$delimeter ;
    if ($test_mode) {?><h1>Отправку уведоления на сервер</h1><?}
    if ($test_mode) {echo '<strong>Размер письма:</strong>'.strlen($text).'<br>' ;}
    _send_mail(_EMAIL_ENGINE_STATS,'Статистика сайта '._BASE_DOMAIN.' за '.get_month_year($data_start,'day month year'),$text,array('debug'=>0,'no_reg_events'=>1,'no_append_css'=>1)) ;

    ob_start() ;
    //if ($_SESSION['LS_stats_logons_email']) c_log_viewer_pages::show_vhod_pages('logon','prevday','Входы на сайт за '.get_month_year($data_start,'day month year')) ;
    // собираем статистику, после анализа журналов данные из них ранее начала предудущего дня будут удалены
    generate_sum_stats(1) ;

    $text=ob_get_clean() ;
    _send_mail('debug@12-24.ru','Статистика сайта '._BASE_DOMAIN.' за '.get_month_year($data_start,'day month year'),$text,array('debug'=>0,'no_reg_events'=>1,'no_append_css'=>1)) ;
    //if ($_SESSION['LS_stats_logons_email']) _send_mail($_SESSION['LS_stats_logons_email'],'Статистика сайта '._BASE_DOMAIN.' за '.get_month_year($data_start,'day month year'),$text,array('debug'=>0,'no_reg_events'=>1)) ;
 }


  function arr_to_csv($arr)
  { $res_str='' ;
    if (sizeof($arr)) foreach($arr as $id=>$value)
      { if (is_array($value)) $res_str.=$id.'?'.implode('&',$value)."|"  ;
        else                  $res_str.=$id.'?'.$value."|"  ;
      }
    return($res_str) ;
  }

  // проверяем стистику, возвращаем необходимость её обновления
  function check_log_pages($debug=0)
  { global $TM_stats ;
    $now=time() ;
	$end_prev_day = mktime(23, 59, 59, date("m",$now), date("d",$now)-1, date("Y",$now)); // вреся конца предыдущего дня

    $info=execSQL_van('select max(r_data) as last from '.$TM_stats) ;

    return($end_prev_day>$info['last']) ;
  }

  function generate_sum_stats($debug=0)
  { global $__functions ;
    $func_name='' ;
    if ($debug){?><h1>Расчет статистики</h1><?} else { ?><h2>Расчет статистики</h2><?}
    list($data_from,$data_last)=get_time_logs_intervals(TM_LOG_PAGES,48,$debug) ;
	if ($debug) $count_rec=execSQL_van('select count(pkey) as cnt from '.TM_LOG_PAGES) ;
    if ($debug) echo 'Записей в журнале: '.$count_rec['cnt'].'<br>' ;

    if ($data_from>$data_last) { if ($debug) echo '<span class=red>Статистика на сегодня уже посчитана</span>' ; return ; }

    //set_robots_by_session_id(1,$debug) ;

    while($data_from<=$data_last)
     { $cnt=0 ;
       $data_to = mktime(23, 59, 59, date("m",$data_from), date("d",$data_from), date("Y",$data_from)); // вреся конца дня

       if ($debug) echo 'Обрабатываем день: <span class="bold black">'.date('d.m.y',$data_from).'</span> ('.date('H:i:s ',$data_from).'-'.date('H:i:s ',$data_to).')  <br>' ;
	   else 	   echo 'Обрабатываем день: <span class="bold black">'.date('d.m.y',$data_from).'</span><br>' ;

       if (sizeof($__functions['stats'])) foreach($__functions['stats'] as $func_name)
          if (function_exists($func_name)) $cnt+=$func_name($data_from,$data_to,$debug) ;

       if ($debug) { echo '<br>Всего за день добавлены <span class="bold green">'.$cnt.'</span> записей<br><br>' ;
                     echo 'Удаляем с журнала обработанные записи ранее '.date('d.m.y H:i:s',$data_from).'<br>' ;
                   }

       // оставляем в журнале записти за последние XX дней
       $XX=7 ;
       $now=time() ;
       $clear_to	= mktime(0, 0, 0, date("m",$now), date("d",$now)-$XX, date("Y",$now))-1; // время окончания вчерашнего дня


       $sql='delete from '.TM_LOG_PAGES.' where c_data<'.$clear_to ;
       if (!$result = mysql_query($sql)) SQL_error_message($sql,'',1) ; else if ($debug) echo '<strong>Записи удалены</strong><br>' ;
       execSQL_update('OPTIMIZE TABLE  '.TM_LOG_PAGES) ;
       if ($debug) { $cnt=execSQL_value('select count(pkey) as cnt from '.TM_LOG_PAGES) ;
                     echo 'Записей в журнале после очистки: '.$cnt.'<br>' ;
                   }

       $data_from=mktime(0, 0, 0,date("m",$data_from), date("d",$data_from)+1, date("Y",$data_from));
	 }


  }

  function robot_name($name)
  { switch ($name)
    {
      case 'Yandex/1.01.001 (compatible; Win16; I)': $res='Yandex - основной индексирующий робот' ; break ;
      case 'Yandex/1.01.001 (compatible; Win16; P)': $res='Yandex - индексатор картинок' ; break ;
      case 'Yandex/1.01.001 (compatible; Win16; H)': $res='Yandex - определение зеркал сайтов' ; break ;
      case 'Yandex/1.01.001 (compatible; Win16; D)': $res='Yandex - служебный робот' ; break ;
      case 'Yandex/1.01.001 (compatible; Win16; M)': $res='Yandex - служебный робот' ; break ;
      case 'YandexSomething/1.0': 					 $res='Yandex - индексатор новостей' ; break ;
      case 'Yandex.Commerce.Pinger': 				 $res='Yandex - простукивалка яндекс-маркета и директа' ; break ;
      case 'Yandex/1.02.000 (compatible; Win16; F)': $res='Yandex - индексатор пиктограмм сайтов (favicons);' ; break ;
      case 'YandexMarket/1.9-2 (compatible; http://market.yandex.ru)': $res='Yandex - запрос выгрузки в маркет;' ; break ;

      case 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)': 					$res='Goodle - индексирующий робот' ; break ;
      case 'Mozilla/5.0 (compatible; Yahoo! Slurp/3.0; http://help.yahoo.com/help/us/ysearch/slurp)': 	$res='Yahoo - индексирующий робот' ; break ;
      case 'StackRambler/2.0 (MSIE incompatible)': 														$res='Rambler - индексирующий робот' ; break ;
      case 'StackRambler/2.0': 																			$res='Rambler - новый индексирующий робот' ; break ;

      case 'msnbot/1.1 (+http://search.msn.com/msnbot.htm)': 											$res='MSN - основной индексирующий робот' ; break ;
      case 'msnbot/2.0b (+http://search.msn.com/msnbot.htm)': 											$res='MSN - новый индексирующий робот' ; break ;

      case 'Gigabot/3.0 (http://www.gigablast.com/spider.html)': 										$res='Gigablast - основной индексирующий робот' ; break ;
      case 'Mozilla/5.0 (Twiceler-0.9 http://www.cuil.com/twiceler/robot.html)': 						$res='cuil.com - индексирующий робот' ; break ;
      case 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.bing.com/bingbot.htm)': 					$res='bing.com - индексирующий робот' ; break ;
      case 'msnbot-media/1.1 (+http://search.msn.com/msnbot.htm)': 					                    $res='search.msn.com - индексирующий робот' ; break ;


      default: $res=$name ;
    }
    if ($res!=$name) $res='<span class=green>'.$res.'<span>' ;
    return($res) ;



  }



  function check_user_agent()
  { $cnt_all=0 ; $res_stats_user=array() ;
    $stats=execSQL('select user_agent,is_robot,count(pkey) as cnt from '.TM_LOG_PAGES.' group by user_agent order by is_robot,user_agent',0,1) ;
    if (sizeof($stats)) foreach($stats as $i=>$rec)
     { if (!$rec['is_robot']) { $br_vers=defines_vers($rec['user_agent']) ;
     						   if ($br_vers!='OTHER') $res_stats_user[$br_vers]+=$rec['cnt'] ;
     						   else 				  $res_stats_user[$rec['user_agent']]+=$rec['cnt'] ;
     						 }
       else					 {  $res_stats_robots[]=array('user_agent'=>$rec['user_agent'],
                                					      'check_brauser'=>defines_vers($rec['user_agent']),
       													  'cnt'=>$rec['cnt']
       												     ) ;
       					     }
       $cnt_all+=$rec['cnt'] ;
     }

    ?><h2>Определены как посетители</h2><?
    damp_array($res_stats_user) ;
    ?><h2>Определены как роботы</h2><?
    ?><table><?
    if (sizeof($res_stats_robots)) foreach($res_stats_robots as $rec)
    {?><tr><td class=left><?echo $rec['user_agent']?></td><td class=left><?echo $rec['check_brauser']?></td><td><?echo $rec['cnt']?></td><td><?echo $rec['max_pages']?></td></tr><?}
    ?></table><?
    echo $cnt_all ;
  }


  function check_robots_by_session_id()
  { $sql_session='select session_id,user_agent,url_from,ip,is_robot from '.TM_LOG_PAGES.' where is_robot!=1 group by session_id having count(pkey)=1 order by ip' ;
    $list= execSQL($sql_session,0,1) ;
    print_2x_arr($list,'Сессии с одной страницей в выдаче') ;
    $sql_ip='select ip,count(session_id) as cnt from ('.$sql_session.') as t1 group by ip having count(session_id)>1' ;
    $list= execSQL($sql_ip,0,1) ;
    print_2x_arr($list,'IP, с которых было более одной сессии по одной странице') ;
  }

  function set_robots_by_session_id($clear_robots=0,$debug=0)
  {  $all_cnt=0 ;
   	 if ($clear_robots) execSQL_update('update '.TM_LOG_PAGES.' set is_robot=0 where is_robot=2') ;
     if ($debug){?><span class="bold green">Метод динамической фильтрации роботов</span> - <?}
     // получаем список сессий, в которых была просмотрена только одна страница и которые не были определены как робот по заголовку
     $list=execSQL('select pkey,ip,user_agent,url_from,session_id from '.TM_LOG_PAGES.' where is_robot=0 group by session_id having count(pkey)=1 order by ip') ;
     // группируем по IP
     $list2=group_by_field('ip',$list,'pkey') ;
     //damp_array($list2) ;
     if (sizeof($list2)) foreach($list2 as $ip=>$rec) if (sizeof($rec)>1) // если имеется более чем одна запись для IP
     {  $str_pkeys=implode(',',array_keys($rec)) ;
     	// делаем дополнительную проверку - это робот или человек
     	$arr=array() ;
     	foreach($rec as $session) $arr[$session['user_agent']]=1 ;
        // проверяем, что если user_agent во всех запросах одинаковый - то это скорее всего посетитель с выключенными кукисами
     	if (sizeof($arr)==1) $sql='update '.TM_LOG_PAGES.' set session_id="'.$session['session_id'].'" where ip = "'.$ip.'" and pkey in('.$str_pkeys.')' ;
     	else {				 $sql='update '.TM_LOG_PAGES.' set is_robot=2 where ip = "'.$ip.'" and pkey in('.$str_pkeys.')' ;
     						 $all_cnt+=sizeof($rec) ;
     		 }
        //echo 'sql='.$sql.'<br>' ;
     	execSQL_update($sql) ;

     }
     //if ($debug) 
     echo 'обнаружено и помечено '.$all_cnt.' входов роботов<br><br>' ;

  }

   function check_robots_recs()
   { global $robots ;
     echo '<h1>Отмечаем роботов в журнале</h1>' ;

     $robots_recs=execSQL_van('select count(c_data) as cnt from '.TM_LOG_PAGES.' where is_robot>0') ;
     echo 'Записей по роботам в журнале до: '.$robots_recs['cnt'].'<br>' ;

     $sql='update '.TM_LOG_PAGES.' set is_robot=0 ';
     if (!$result = mysql_query($sql)) SQL_error_message($sql,'',1) ;

     $_usl=array() ;
     $_usl[]="user_agent=''" ;
     if (sizeof($robots)) foreach($robots as $rec) $_usl[]="user_agent like '%".$rec."%'" ;

     if (sizeof($_usl))
     { $usl=implode(' or ',$_usl) ;
       $sql='update '.TM_LOG_PAGES.' set is_robot>0 where '.$usl ;
       if (!$result = mysql_query($sql)) SQL_error_message($sql,'',1) ;
     }

     $robots_recs=execSQL_van('select count(c_data) as cnt from '.TM_LOG_PAGES.' where is_robot>0') ;
     echo 'Записей по роботам в журнале после: '.$robots_recs['cnt'].'<br>' ;
   }

   // удалить пустые сесии
   function delete_logs_recs()
   { echo '<h1>Удаляем записи с пустым значением session_id</h1>' ;
     $count_rec=execSQL_van('select count(pkey) as cnt from '.TM_LOG_PAGES) ;
     echo 'Записей в журнале до удаления: '.$count_rec['cnt'].'<br>' ;

     $_usl=array() ;
     $_usl[]="session_id=''" ;

     if (sizeof($_usl)) $usl=implode(' or ',$_usl) ;

     $robots_recs=execSQL_van('select count(c_data) as cnt from '.TM_LOG_PAGES.' where '.$usl.' ') ;
     echo 'Будет удалено записей: '.$robots_recs['cnt'].'<br>' ;

     $sql='delete from '.TM_LOG_PAGES.' where '.$usl ;
     if (!$result = mysql_query($sql)) SQL_error_message($sql,'',1) ;

     $count_rec=execSQL_van('select count(pkey) as cnt from '.TM_LOG_PAGES) ;
     echo 'Записей в журнале после удаления: '.$count_rec['cnt'].'<br>' ;
   }


   function delete_last_day_from_stats()
   { global $TM_stats ;

     $stats_recs=execSQL_van('select max(c_data) as max_data from '.$TM_stats) ;          // последняя запись в статистике
     $last_data=$stats_recs['max_data'] ;
     echo 'Последняя запись в статистике: '.date('d.m.y H:i:s ',$last_data).'<br>' ;
     $data_from = mktime(0,0,0,date("m",$last_data), date("d",$last_data), date("Y",$last_data)); // вреся конца дня
     echo 'Удаляем записи в статистике от : '.date('d.m.y H:i:s ',$data_from).'<br>' ;

     $count_rec=execSQL_van('select count(pkey) as cnt from '.$TM_stats) ;
     echo 'Записей в статистике до удаления: '.$count_rec['cnt'].'<br>' ;

     $sql='delete from '.$TM_stats.' where c_data>='.$data_from ;
     if (!$result = mysql_query($sql)) SQL_error_message($sql,'',1) ;

     $count_rec=execSQL_van('select count(pkey) as cnt from '.$TM_stats) ;
     echo 'Записей в статистике после удаления: '.$count_rec['cnt'].'<br>' ;

     $stats_recs=execSQL_van('select max(c_data) as max_data from '.$TM_stats) ;          // последняя запись в статистике
     $last_data=$stats_recs['max_data'] ;
     echo 'Последняя запись в статистике: '.date('d.m.y H:i:s ',$last_data).'<br>' ;
   }

/**************************************************************************************************************************************

   тестовое заполнение журналов

***************************************************************************************************************************************/



   // создаем тестовый журлан входов
   function	generate_test_logs()
   { global $TM_site_logons ;
     ?><h1>Генерация тестового журнала входов</h1><?
     $time_start=mktime(0, 0, 0, 1, 1, 2008);
     $time_end=time() ;
     echo 'Диапазон заполнения журнала: '.date('d.m.y H:i:s ',$time_start).' - '.date('d.m.y H:i:s ',$time_end).'<br>' ;
     $add_reffers[]='http://yandex.ru?text=yandex' ;
     $add_reffers[]='http://rambler.ru?text=rambler' ;
     $add_reffers[]='http://mail.ru?text=mail' ;
     $add_reffers[]='http://price.ru?text=price' ;
     $cur_time=$time_start ;
     $i=0 ; $j=0 ;
     while($cur_time<=$time_end)
     { $finfo=array(	'c_data'		=>	$cur_time,
	    				'addr'			=>	date('d.m.y H:i:s ',$cur_time),
	    				'referer'		=>	$add_reffers[$i],
	    				'user_agent'	=>	'',
	    				'uri'			=>	''
	    			) ;
	   adding_rec_to_table($TM_site_logons,$finfo) ;
	   $i++ ;
	   $j++ ;
	   if ($i>(sizeof($add_reffers)-1)) $i=0 ;

	   $cur_time=mktime(date("H",$cur_time)+1, date("i",$cur_time), date("s",$cur_time), date("m",$cur_time), date("d",$cur_time), date("Y",$cur_time));
     }
     echo 'Сгенерировано '.$j.' записей<br>' ;

     $list=execSQL('select * from '.$TM_site_logons.'  order by c_data ') ;
     _CLSS(43)->show_list_items($list,array('read_only'=>1,'no_check'=>1,'title'=>'Статистика входов','all_enabled'=>1,'no_htmlspecialchars'=>1)) ;

   }

   // создаем тестовую запись в журнале входов
   function	generate_test_rec()
   { global $TM_site_logons ;
     ?><h1>Генерация тестовой записи журнала входов</h1><?
     $add_reffers[]='http://yandex.ru?text=yandex' ;
     $add_reffers[]='http://rambler.ru?text=rambler' ;
     $add_reffers[]='http://mail.ru?text=mail' ;
     $add_reffers[]='http://price.ru?text=price' ;
     $cur_time=time() ;
     $rand_keys = array_rand($add_reffers);
       $finfo=array(	'c_data'		=>	$cur_time,
	    				'addr'			=>	date('d.m.y H:i:s ',$cur_time),
	    				'referer'		=>	$add_reffers[$rand_keys],
	    				'user_agent'	=>	'',
	    				'uri'			=>	''
	    			) ;
	   adding_rec_to_table($TM_site_logons,$finfo) ;

     echo 'Сгенерировано 1 запись<br>' ;

     $list=execSQL('select * from '.$TM_site_logons.' order by c_data desc limit 5') ;
     _CLSS(43)->show_list_items($list,array('read_only'=>1,'no_check'=>1,'title'=>'Статистика входов - последние 5 записей','all_enabled'=>1,'no_htmlspecialchars'=>1)) ;

   }


  // возвращает время, с которого надо обрабатывать журнал
  // с учетом наличия записей в статистике
  function get_time_logs_intervals($jurnal,$clss,$debug=0)
     {  global $TM_stats ;

	     $logs_recs=execSQL_van('select min(c_data)  as min_data,max(c_data) as max_data from '.$jurnal) ; // первая и последняя запись в журнале
         if ($TM_stats and $_SESSION['pkey_by_table'][$TM_stats])
         { if ($clss) $last_stats_data=execSQL_value('select max(r_data) as max_data from '.$TM_stats.' where clss='.$clss) ;          // последняя запись в статистике
	       else		  $last_stats_data=execSQL_value('select max(r_data) as max_data from '.$TM_stats) ;          // последняя запись в статистике
         } else       $last_stats_data=-1 ;

	     $first_log_data=$logs_recs['min_data'] ;     // дата первой записи в журнале
	     $last_log_data=$logs_recs['max_data'] ;      // дата последней записи в журнале

	     $now=time() ;
	     $prev_day_end	= mktime(0, 0, 0, date("m",$now), date("d",$now), date("Y",$now))-1; // время окончания вчерашнего дня
	     $cur_day_end	= mktime(23, 59, 59, date("m",$now), date("d",$now), date("Y",$now)); // время окончания сегодняшнего дня

	     // время начала анализа журнала
	     // если в статистике уже есть записи, то идем от даты последней записи+1
	     // если в статистике последняя запись моложе первой записи журнала, то идем от первой записи журнала

	     $from=($first_log_data>$last_stats_data or $last_stats_data==-1)? $first_log_data:$last_stats_data+1 ;

	     //if ($debug) echo date('d.m.y H:i:s ',$first_log_data).' - начало журнала <br>' ;
	     if ($debug and $last_stats_data!=-1) echo '<span class="green bold">'.date('d.m.y H:i:s ',$last_stats_data).'</span> - последняя запись статистики <br>' ;


	     // время окончания анализа журнала
	     // самая последняя точка - конец вчерашнего дня
	     // если последняя запись моложе, используем её

	     $to=($last_log_data<$prev_day_end)? $last_log_data:$prev_day_end ;

	     if ($debug) echo date('d.m.y H:i:s ',$first_log_data).' - '.date('d.m.y H:i:s ',$last_log_data).': интервал записей в журнале<br>' ;
         if ($debug) echo date('d.m.y H:i:s ',$from).' - '.date('d.m.y H:i:s ',$to).': интервал обработки журнала<br>' ;

	     return(array($from,$to)) ;
	 }


/**************************************************************************************************************************************

   Работа с новым журналом

***************************************************************************************************************************************/


// эти данные мы видим в главном меню сайта
function get_users_active_info($last_minut=10,$debug=0)
   { // делаем проверку на роботов через анализ сессий,IP и просмотренных страниц
   	 //set_robots_by_session_id(0,$debug) ;

   	 $now=getdate() ; $res=array() ;
     $start_day=mktime(0,0,0,$now['mon'],$now['mday'],$now['year']); // c
     $start_active=mktime($now['hours'],$now['minutes']-$last_minut,$now['seconds'],$now['mon'],$now['mday'],$now['year']);
     // в этом варианте учитыываются страницы которые были открыты в текущеи сутки, но сессий могли быть начаты в прошлые сутки
     //$list= execSQL('select session_id,count(pkey) as cnt,user_agent,is_robot,min(c_data) as first_data from '.TM_LOG_PAGES.' where is_robot=0 and c_data>='.$start_day.' group by session_id order by c_data asc',$debug) ;
     // в этом варианте учитываются только те страницы, сессии которых были открыты в текущих сутках
     $list= execSQL('select session_id,count(pkey) as cnt,user_agent,min(c_data) as first_data from '.TM_LOG_PAGES.' where is_robot=0 group by session_id having first_data>='.$start_day.'',$debug) ;
     if (sizeof($list)) foreach($list as $rec) if ($rec['cnt']>1)  $res['active_users']++ ; else $res['passive_users']++ ;

     $list= execSQL('select session_id,count(pkey) as cnt,user_agent from '.TM_LOG_PAGES.' where is_robot=0 and c_data>='.$start_active.' group by session_id order by c_data',$debug) ;
     $res['now_users']=sizeof($list) ;

     $res['all_users']=$res['active_users']+$res['passive_users'] ;

     if ($debug) { damp_array($res) ;
                   //$list=get_pages_stat_info('segodna','','',2) ;
                   //echo 'Число записей:'.sizeof($list).'<br>';
                   //print_2x_arr($list);

                   // определяем одноразовые входы с одного IP

                   //$sql_session='select session_id,ip,user_agent,min(c_data) as tm1,max(c_data) as tm2   from '.TM_LOG_PAGES.' where is_robot=0 group by session_id having min(c_data)>='.$start_day.' and count(pkey)=1 order by ip' ;
                   //$list= execSQL($sql_session,2) ;
                   //$list= execSQL('select ip,count(session_id) as cnt,user_agent,((max(tm2)-min(tm1))/60) as time from ('.$sql_session.') as t1 group by ip having cnt>1',2) ;

                   //echo
                   //int mysql_affected_rows

                 }

                 
     return($res) ;
   }

// эти данные мы видим в просмотре журнала
function get_pages_stat_info($mode='active',$res='count',$robot_name='',$debug=0)
 {  global $select_IP ; // select_IP - глобальный POST параметр
	$now=getdate() ; $data_from=0 ; $data_to=0 ;  $usl=array() ; $usl_sql='' ;
    $last_minut=10 ;

	if ($mode=='segodna')       $data_from=mktime(0,0,0,$now['mon'],$now['mday'],$now['year']);
	if ($mode=='active')        $data_from=mktime($now['hours'],$now['minutes']-$last_minut,$now['seconds'],$now['mon'],$now['mday'],$now['year']);
	if ($mode=='sutki')         $data_from=mktime($now['hours'],$now['minutes'],$now['seconds'],$now['mon'],$now['mday']-1,$now['year']);
	if ($mode=='prevday')       { $data_from=mktime(0,0,0,$now['mon'],$now['mday']-1,$now['year']); $data_to=mktime(0,0,0,$now['mon'],$now['mday'],$now['year']) ; }


	if ($mode=='segodna')       $usl[]='first_data>='.$data_from ;
	if ($mode=='active') 	    $usl[]='last_data>='.$data_from ;
	if ($mode=='sutki')         $usl[]='first_data>='.$data_from ;
	if ($mode=='prevday')       $usl[]='first_data>='.$data_from.' and first_data<='.$data_to ;



    if (!$robot_name) 			 		$usl[]='is_robot=0' ; //$usl[]='not (cnt=1 and url_from="")' ;
    else if ($robot_name=='all') 		$usl[]='is_robot>0' ;
    else if ($robot_name=='dynamik')    $usl[]='is_robot=2' ;
    else                         		$usl[]='is_robot>0 and user_agent like "%'.$robot_name.'%"' ;

	//if (!$robot_name) $usl[]='user_agent!="Yandex.Commerce.Pinger"' ;
	//if (!$robot_name) $usl[]='user_agent not like "%bot%"' ;
	//else if ($robot_name!='all')
    if ($select_IP) $usl[]='IP="'.$select_IP.'"' ;
	
	if (sizeof($usl)) $usl_sql=implode(' and ',$usl) ;

	$list_session_rec=execSQL('select pkey,session_id,max(c_data) as last_data,min(c_data) as first_data,ip,url_from,search_text,url,obj_name,user_agent,is_robot,country,city, count(pkey) as cnt from '.TM_LOG_PAGES.' group by session_id having  '.$usl_sql.' order by c_data desc',$debug) ;

	if ($res=='count') return(sizeof($list_session_rec)) ; else return($list_session_rec) ;
 }

 function get_pages_stat_info_jurnal($mode='active')
 {  global $select_IP ;
    $now=getdate() ; $data_from=0 ; $data_to=0 ; $usl=array() ; $usl_sql='' ;
    $last_minut=10 ;
     //$select_IP='85.175.97.99' ;
	if ($mode=='sutki')         $data_from=mktime($now['hours'],$now['minutes'],$now['seconds'],$now['mon'],$now['mday']-1,$now['year']);
	if ($mode=='active') 		$data_from=mktime($now['hours'],$now['minutes']-$last_minut,$now['seconds'],$now['mon'],$now['mday'],$now['year']);
    if ($mode=='prevday')     { $data_from=mktime(0,0,0,$now['mon'],$now['mday']-1,$now['year']); $data_to=mktime(0,0,0,$now['mon'],$now['mday'],$now['year']) ; }
	// для all временное условие не формируется

	if ($data_from) $usl[]='c_data>='.$data_from ;
	if ($data_to)   $usl[]='c_data<='.$data_to ;
	if ($select_IP) $usl[]='IP="'.$select_IP.'"' ;

	if (sizeof($usl)) $usl_sql=' where '.implode(' and ',$usl) ;

	//echo $usl_sql.'<br>' ;

	$list_session_rec=execSQL('select *,c_data as last_data,c_data as first_data,1 as cnt from '.TM_LOG_PAGES.' '.$usl_sql.' order by c_data desc') ;


	return($list_session_rec) ;
 }

?>