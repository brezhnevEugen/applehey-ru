<?php
// класс для выделения низкоуровневых функций страницы в отдельный скрипт
class c_core_HTML
{  public $system_name ;
   public $system ;
   public $script_file ;
   public $script_dir ;
   public $class_name ;
   public $message_id;
   public $h1;   // заголовок h1 на станице
   public $title; // заголовок title на в head страницы
   public $keywords; // заголовок keywords на в head страницы
   public $description; // заголовок description на в head страницы
   public $path;  // путь страницы
   public $SEO_url;  // информация в карте сайта
   public $SEO_info;  // информация в карте сайта
   public $result ;
   public $no_sitemap=0 ;
   public $no_obj_reffer_to_site_map=0 ;   // не использовать _reffer объекта для
   public $allow_GET_params=array() ;  // $allow_GET_params=array('dd'=>'int') ;
   public $checked_GET_params=0 ;      // разрешение проверки корректности параметров
   public $false_GET_params_action=301 ; // действие в случае некоректного параметра или некоректного url
   public $no_query_params=0 ; // запрет на передачу параметров в адресной строке
   public $only_dir_in_url=0 ; // эта страница должны открываться только по url /catalog/sales/, /catalog/sales/)%7Bthis.close()%7D%7D%5D выдаст 404 ошибку или 301 редирект
   public $prev_HTML; //  код в модулях, выводимый в поток при подклбчении подулей (код вне функций). Выводится в зависимости от типа текущей страницы (HTML, XML)
   public $noindex=0; //  запрет индексации страницы роботами
   public $unvalidate_params=0 ;
   public $cmd_exec_result ; // результат выполнения команды (обычно для подсистемы)
   public $cmd_exec_HTML_output ;
   public $cur_obj ;  // замена global $obj_info ;
   public $body_class ;  // замена define('_BODY_CLASS','col_750_200 main_l') ; ;

   function check_autorize() { return(1);}

  function c_core_HTML(&$options=array())
  {

  }

  function init(&$options=array())
  {  global $obj_info ;
     $is_html_doc=(!$options['no_html_head'] and !$options['no_html_doc_type'] and ($options['content_type']=='' or $options['content_type']=='text/html'))? 1:0 ;
     if (!$is_html_doc) { echo 'Используйте для не HTML страниц собственный скрипт класса.' ; _exit() ; }
     // получаем очищенный url - на него будет записана вся информация в таблице карты сайта
     $this->SEO_url=$this->get_SEO_url() ;                                  set_time_point('get_SEO_url - OK') ;
     // проверка хоста и при необходимости направление на основной хост или
     $this->check_location($options) ;                                      set_time_point('check_location - OK') ;
     // проверка страницы пагинатора
     $this->check_paginator($options) ;                                             set_time_point('check_paginator - OK') ;
     // проверка входных переменных на корректность
     $this->check_GET_params($options) ;                                    set_time_point('check_GET_params - OK') ;
     // эта функция должна быть тут на тот случай, что если потребуется устновка куки, это должно быть сделано до отправки первой информации на сайт
     $this->on_send_header_before($options) ;                               set_time_point('on_send_header_before - OK') ;
     // обработка входных данных до определения текущего объекта
     $this->command_execute($options) ; set_time_point('command_execute - OK') ;

     // получаем seo info для текущего объекта
     $this->SEO_info=$this->get_page_from_site_map_by_URL($this->SEO_url) ; set_time_point('get_page_from_site_map - OK') ;
     //damp_array($this->SEO_info) ;
     if ($this->SEO_info['r301to'] and $_SESSION['__fast_edit_mode']!='enabled') redirect_301_to($this->SEO_info['r301to']) ;

     // получить из базы инфоримацию по ткущему объекту
     if (!$options['no_select_obj_info'])
     { $this->select_obj_info() ;        set_time_point('select_obj_info - OK') ;
       if ($this->result==404 and isset($this->on_404_action) and isset($this->on_404_action['redirect_to']))
       { redirect_301_to($this->on_404_action['redirect_to']) ;
         $this->result=301 ;
       }
     }

     if (!$this->SEO_info['pkey'] and $GLOBALS['obj_info']['_reffer'] and !sizeof($GLOBALS['obj_info']['__filter_set']))
     { $this->SEO_info=$this->get_page_from_site_map_by_reffer($GLOBALS['obj_info']['_reffer']) ; set_time_point('get_page_from_site_map - OK') ;
       // если найденные теги объекта уже связанны с именем страницы, сбрасываем код записи - в sitemap будет добавлена новая запись с текущем url и выбранными метатегами
       if (trim($this->SEO_info['url'])) unset($this->SEO_info['pkey']) ;

     }

     // еще одна обработка обработка входных данных - после обработки текущего объекта
     $this->cmd_exec() ; set_time_point('cmd_exec - OK') ;

     // позволяем задать вручную seo info для текущей страницы
     //$this->set_meta() ;                                                    set_time_point('set_meta - OK') ;
     // получаем seo info для текущего объекта
     //$this->SEO_info=$this->get_page_from_site_map($this->SEO_url,$obj_info['_reffer']) ; set_time_point('get_page_from_site_map - OK') ;
     // итоговое формирование тегов для текущей страницы
     $this->get_cur_page_meta_tags() ; set_time_point('get_cur_page_meta_tags - OK') ;

    //damp_array($this) ;

     if (method_exists($this,'prepare_page_title')) { echo 'Метод <strong>prepare_page_title</strong> более не используется, используйте конструктор страницы для задания метатегов<br>Данные сгенерированных тегов должны быть сохранены в this' ; _exit() ; }
     if (method_exists($this,'autogenerate_meta_tags')) { echo 'Метод <strong>autogenerate_meta_tags</strong> более не используется, используйте конструктор страницы для задания метатегов<br>Данные сгенерированных тегов должны быть сохранены в this' ; _exit() ; }
  }


// 1. Неосновные хосты
// При запросе любых URLs, содержащих неосновные хосты (например, http://as-com.ru/ или http://as-com.ru/Generatoryi/Agilent.html без префикса WWW) необходимо, чтобы сервер отдавал ответ HTTP/1.1 301 Moved Permanently. При этом в поле Location должен быть прописан URL, содержащий основной хост ресурса (http://www.as-com.ru/ и http://www.as-com.ru/Generatoryi/Agilent.html, соответственно).
// основной хост определяется в patch.php, по умолчнию - www.as-com.ru
// 2. Переадресация с главной страницы
//Ссылки на главную страницу на всех страницах сайта должны быть одного вида и указывать в качестве URL http://www.as-com.ru/.
//При запросе ссылок вида http://www.as-com.ru/index.html, http://as-com.ru/index.htm, http://as-com.ru/index.php и т.п. сервер должен отдавать HTTP/1.1 301 Moved Permanently. При этом в поле Location должен быть прописан URL, содержащий основной хост ресурса, т.е. http://www.as-com.ru/.
// $redirect_page_to_main_domains - флаг для patch.php - использовать перенаправление

  function check_location($options=array())
  { $to_url=$this->check_location_rules() ;
    if ($to_url and !$options['no_redirect']) redirect_301_to($to_url,1) ;
  }


  // функция должна вернуть новый адрес страницы, если необходимо перенаправление
  function check_location_rules()
  { global $redirect_page_to_main_domains,$redicted_pages,$cur_page_path,$SF_REQUEST_URI,$redirect_page_to_main_page;
    $new_url='' ;

    // проходим массив перенаправляемых страниц и проверяем текущую страницу
    if ($redirect_page_to_main_page and sizeof($redicted_pages)) foreach($redicted_pages as $from=>$to)
       if ($cur_page_path==$from) { $new_url=_CUR_DOMAIN.str_replace($from,$to,$SF_REQUEST_URI) ; return($new_url) ; }
       // старый алгоритм, пока отклбчен, потом удалить
       //if(strpos($SF_REQUEST_URI,$from)!==false) { $new_url=str_replace($from,$to,$SF_REQUEST_URI) ; return($new_url) ; }

    //global $subdomain ; echo '$subdomain='.$subdomain.'<br>' ;
    // если мы не на основном домене и текущий домен совпадант с базовым или произошла замена страницы
    if ($redirect_page_to_main_domains and _CUR_DOMAIN!=_MAIN_DOMAIN and (_CUR_DOMAIN==_BASE_DOMAIN or $subdomain='www')) { $new_url=_MAIN_DOMAIN.$SF_REQUEST_URI ; return($new_url) ; }

    // проверка, не используется ли мобильное устройство
    // если переход с мобильного домена на основной - ставим кукис что работаем на основном домене
    $mob_device_use_main_domain=$_COOKIE['mob_device_use_main_domain'] ;
    if (_CUR_DOMAIN!=_MOBILE_DOMAIN and strpos($_SERVER['HTTP_REFERER'],_MOBILE_DOMAIN)!==false) { setcookie('mob_device_use_main_domain',1) ; $mob_device_use_main_domain=1 ; }
    if (_MOBILE_DOMAIN!=_MAIN_DOMAIN and is_mobile() and !$mob_device_use_main_domain and _CUR_DOMAIN!=_MOBILE_DOMAIN) { $new_url=_MOBILE_DOMAIN ; return($new_url) ; }
    //echo '$new_url='.$new_url.'<br>' ;
    return('') ;
  }


  // действия перед отправкой заголовков - обычно сохранение кукисов
  function on_send_header_before($options=array())
   { global $cur_system ;
     if (is_object($cur_system) and method_exists($cur_system,'on_send_header_before')) $cur_system->on_send_header_before($options) ;
   }

  // отработка команд ДО определения текущего объекта
  // вызывается для всех скриптов текущего модуля, если в скрипте не объявлена собственная функция command_execute
  function command_execute()
	{ global $cur_system ;  //echo 'Подсистема' ; damp_array($cur_system) ;
      if (is_object($cur_system) and method_exists($cur_system,'command_execute'))  $this->cmd_exec_result=$cur_system->command_execute() ;
      $this->local_command_execute() ;
	}

  function local_command_execute() {}

  // отработка команд ПОСЛЕ определения текущего объекта
  // вызывается для всех скриптов текущего модуля, если в скрипте не объявлена собственная функция cmd_exec
  function cmd_exec()
	{ global $cur_system ;
      if (is_object($cur_system) and method_exists($cur_system,'cmd_exec'))  $cur_system->cmd_exec() ;
      // 4.10.13 - если передана команда cmd, и в текущем скрипте у класса страницы есть метоd CMD_.... - вызывем этот метод
      $cmd_method_name='CMD_'.$_POST['cmd'] ;
      if (isset($_POST['cmd']) and method_exists($this,$cmd_method_name)) $this->$cmd_method_name() ;
	}

  // получение информации по объекту
  // 5.08.2011 - получение текущей страницы вынесено в page_system
  function select_obj_info()
  { global $cur_system ;
    // защита от некореткных URL для  стрнаниц директорий
    if ($this->no_query_params and sizeof($_GET))
      { if ($this->false_GET_params_action==404) $this->result=404 ;
        else                                     redirect_301_to(_CUR_PAGE_DIR) ;
      }
    if ($this->only_dir_in_url and _CUR_PAGE_NAME)
      { if ($this->false_GET_params_action==404) $this->result=404 ;
        else                                     redirect_301_to(_CUR_PAGE_DIR) ;
      }
    // если необходима предварительная обработка URL с целью извленения параметров фильтра из него - необходимо в переккрытой select_obj_info вызывать  select_obj_info_by_tree с нужными параметрами
    if (is_object($cur_system) and method_exists($cur_system,'select_obj_info')) 
    { $result_parse_URL=$this->result_parse_URL=$cur_system->select_obj_info(_CUR_PAGE_DIR,_CUR_PAGE_NAME) ;
      unset($this->result_parse_URL['cur_obj']) ;
      $this->result=$result_parse_URL['http_code'] ;
      $this->cur_obj=$result_parse_URL['cur_obj'] ;
      $this->cur_section_id=$result_parse_URL['cur_section_id'] ;


      // если не удалось определить id по URL, проверяем нераспознные dir - может это фильтры
      // фильтр может быть как в одном dir, так и в нескольких подряд
      //damp_array($result_parse_URL) ;
      if ($result_parse_URL['http_code']==200)
      { $this->filter_set['parent']=$result_parse_URL['cur_section_id'] ;   // по старому
        $this->filter_set['section_id']=$result_parse_URL['cur_section_id'] ; // по новому

      }
      elseif ($result_parse_URL['http_code']==404 and sizeof($result_parse_URL['levels_404']))
        { // вызываемая функция должна вернуть массив параметров фильтра
          // в старых вариантах она еще и сразу прописывала все в  $_SESSION['filter']
          $this->filter_set=$cur_system->parse_url_dir_to_filter_value(implode('&',$result_parse_URL['levels_404']),$result_parse_URL['last_found_id']) ;  // передаем директоию фильтра и код объекта, к которому применен фильтр
          if (sizeof($this->filter_set)) // если найден фильтр, делаем текущим объектом директрию, к которой применен фильтр
          { $this->result=200 ;

            // дублируем cur_section_id в результат парсинга URL
            $this->result_parse_URL['cur_section_id']=$this->cur_section_id=$result_parse_URL['last_found_id'] ;

            if ($this->cur_section_id)
            { $obj_info=select_db_obj_info($cur_system->tkey,$this->cur_section_id,array('get_child_obj'=>'3,5','only_enabled_child'=>1,'add_usl'=>$cur_system->usl_show_section,'debug'=>0)) ; // damp_array($obj_info) ;
            $obj_info['__filter_set']=$this->filter_set ;
            $cur_system->adding_info_from_tree_model($obj_info) ;
            $cur_system->fill_obj_info_to_section_rec($this->cur_section_id,$obj_info) ;
    $this->cur_obj=$obj_info ;
            }
          }
          //if (sizeof($_SESSION['filter'])) { $_SESSION['filter']=$filter_set ; $not_found=0 ; if ($debug) echo 'Найден фильтр: '.print_r($_SESSION['filter'],true).'<br>' ; }
        }


      /*
      if ($this->cur_obj['pkey'] and
              $result_parse_URL['true_url'] and
              $result_parse_URL['true_url']!=$result_parse_URL['cur_url'])
      { if ($GLOBALS['debug_page_relocation']) echo '<h2>Отладка redirect_301_to - несоответствие _CUR_PAGE_DIR href текущего объекта</h2>true_url='.$result_parse_URL['true_url'].'<br>cur_url='.$result_parse_URL['cur_url'].'<br>pkey='.$this->cur_obj['pkey'].'<br>' ;
         $new_url=str_replace($result_parse_URL['cur_url'],$result_parse_URL['true_url'],rawurldecode($_SERVER['REQUEST_URI'])) ;
         if ($GLOBALS['debug_page_relocation']) echo 'Требуется перенаправление на правильный адрес, new_url='.$new_url.'<br>' ;
         redirect_301_to($new_url) ;
      } */
    }
    else if (is_object($_SESSION['pages_system'])) $this->cur_obj=$_SESSION['pages_system']->get_page_content(_CUR_PAGE_DIR._CUR_PAGE_NAME) ; //damp_array($obj_info);


    // глобалим результат - для обеспечения совместимости со старым кодом
    $GLOBALS['obj_info']=$this->cur_obj ;
    $GLOBALS['section_id']=$this->cur_section_id ;
    $GLOBALS['filter']=$this->filter_set ;

    //damp_array($this) ;
    //damp_array($GLOBALS['obj_info']) ;
  }

  // проверяем страницу пагинатора
  // если в url есть конструкция типа /page_xxx.html - значит, передан номер страницы
  // переносим номер страницы в $_GET['page'] и очищаем $cur_page_name, удаляем конструкцию из $SF_REQUEST_URI ;
  function check_paginator($options=array())
  {  if ($options['no_check_paginator']) return ;
     //global $SF_REQUEST_URI_arr,$SF_REQUEST_URI,$cur_page_name ;
     // проверяем наличие в $SF_REQUEST_URI page_xxx
     //list($from_page,$url)=extract_page_number_from_path(_CUR_PAGE_PATH) ;
     //echo '$from_page='.$from_page.'<br>' ;
     //echo '$url='.$url.'<br>' ;
     //if ($from_page)
     //{ $_GET['page']=$from_page ;
     //  $SF_REQUEST_URI=$url ;
     //  damp_array($_GET) ;
       //$_GET['cur_page_name']='' ;
       //$cur_page_name='' ;

     //  return ;
     //}
     /*
     if (strpos($SF_REQUEST_URI_arr['path'],'page_')!==false) // выделяем номер страницы из адреса
     {  $arr_path=explode('/',$SF_REQUEST_URI_arr['path']) ;
        $str_page=$arr_path[sizeof($arr_path)-1] ;
        if (preg_match_all('/page_(.+?)\.html/is',$str_page,$matches,PREG_SET_ORDER)) $_GET['page']=$matches[0][1];  //foreach($matches as $match) $arr[]=$match[1];
        //echo '$page='.$_GET['page'].'<br>' ;
        // удаляем страницу пагинатора из url, очищаем $cur_page_name
        if ($_GET['page']) { $cur_page_name='' ; $_GET['cur_page_name']='' ; $SF_REQUEST_URI=str_replace($matches[0][0],'',$SF_REQUEST_URI) ; return ; }
     }
     */
     // проверяем старую систему подачи номеров страниц - ?page=xxx  - если будет обнаружена, формируем правильный url и делаем редирект
     if (_PAGE_NUMBER_SOURCE=='in_name')
     {   if (_CUR_PAGE_QUERY) parse_str(_CUR_PAGE_QUERY,$in_params) ;
         // проверяем наличие в $in_params параметра page - если он есть, делаем редирект на страницу page_xxx.html
         if (isset($in_params['page']))
         { //check_input_var($in_params['page'],'int') ;
           $to_url=_CUR_PAGE_DIR.'page_'.$in_params['page'].'.html' ;
           unset($in_params['page']) ;
           $new_query=http_build_query($in_params) ; // преобразуем $in_params в строку запроса
           if ($new_query) $to_url.='?'.$new_query ;
           redirect_301_to($to_url) ;
         }
     }
  }

  // если запрос преобразован через modrewrite то теряются параметры, переданные через '?'
  // поэтому парсим строку запроса, необходимые параметры преобразуем в переменные
  // также проверяем присутствуют ли встроке запроса неразрешенные параметры
  // если такие параметры присутствуют - убираем их и делаем педирект на правильный url
  function check_GET_params($options=array())
  {  global $debug_metatags,$SF_REQUEST_URI_arr,$SF_REQUEST_URI ;
     if (_ENGINE_MODE=='admin') return ;
     $in_params=array() ; $true_query='' ;

     if (isset($SF_REQUEST_URI_arr['query'])) parse_str($SF_REQUEST_URI_arr['query'],$in_params) ;

    // проверяем наличие в $SF_REQUEST_URI ($in_params) разрешенных параметров, если есть ненужные парамеры, удаляем их
    if ($this->checked_GET_params)
    {   //echo _CUR_PAGE_DIR.'<br>'._CUR_PAGE_NAME.'<br>' ;
       //$allow_GET_params=array_merge($_SESSION['allow_GET_params'],$this->allow_GET_params) ; /// ??? надо ли задавать разрешенные параметры для всег сайта сразу???
       $allow_GET_params=$this->allow_GET_params ; //damp_array($allow_GET_params) ; damp_array($in_params) ;
       //damp_array($allow_GET_params) ;
       //damp_array($in_params) ;
       if (sizeof($in_params)) foreach($in_params as $name=>$value)
       {  if (!in_array($name,$allow_GET_params))
          { if (isset($allow_GET_params[$name])) check_input_var($in_params[$name],$allow_GET_params[$name]) ; // если параметр разрешенный - приводим к виду соответственно типу
            else unset($in_params[$name]) ; // инача - удаляем параметр из строки
          }
       }
       //damp_array($in_params) ;
       // после проверки в $in_params остаются только разрпешенные параметры и проверенные параметры
       if (sizeof($in_params)) $true_query=urldecode(http_build_query($in_params)) ; // преобразуем $in_params обратно в строку запроса
       //echo '$SF_REQUEST_URI_arr[query]='.$SF_REQUEST_URI_arr['query'].'<br>$true_query='.$true_query.'<br>' ;
       if ($true_query!=$SF_REQUEST_URI_arr['query']) // если строка запроса изменилась
       {  if ($this->false_GET_params_action==301) // и зададно выполнение редиректа
           { $to_url=$SF_REQUEST_URI_arr['path'] ;
             if ($true_query) $to_url.='?'.$true_query ;
             redirect_301_to($to_url) ;
           }
          else if ($this->false_GET_params_action==404) { $this->result=404 ; $this->unvalidate_params=1 ; } // или зададно выдача ошибки
       }


    }


     if (sizeof($in_params)) { $_GET=array_merge($_GET,$in_params) ; // дополняем $_GET параметрами из командной строки
                               parse_str($true_query) ;              // вносим параметры запроса в глобальные переменные
                             }
     //damp_array($_GET) ;
     //echo  $true_query.'<br>' ;

     if (sizeof($_GET)) foreach($_GET as $name=>$value) $GLOBALS[$name]=$value ;

     /* отлючсе
     if (isset($_GET['seo'])) { $debug_metatags=2 ;
                                parse_str($true_query,$in_params) ;
                                unset($in_params['seo']) ;
                                $true_query=http_build_query($in_params) ;
                                $SF_REQUEST_URI=$SF_REQUEST_URI_arr['path'] ;
                                if ($true_query) $SF_REQUEST_URI.='?'.$true_query ;
                                //echo $SF_REQUEST_URI ;
                              }
    */
  }


  // стандартная функция для вызова страницы
  function show(&$options=array())
  {
    header("Content-type: text/html;charset=UTF-8");
    //echo $this->result ;
    if ($this->result==404) { header("HTTP/1.1 404 Not found"); if ($this->error_404()=='exit') return ; }

    $replace_word=$_SESSION['global_replace_worlds'] ; if (_CUR_LANG) { $replace_word['href="/']='href="/'._CUR_LANG.'/' ; $replace_word["href='/"]="href='/"._CUR_LANG."/" ; } //damp_array($replace_word) ;

    // используем всегда кеширование вывода, чтобы в любом месте вывода можно было сделаь 301 redirect
    ob_start() ;
            $this->HTML($options) ;
            $page_HTML=ob_get_clean() ;

            if ($_SESSION['__fast_edit_mode']!='enabled')
            { if (is_array($replace_word) and sizeof($replace_word)) $page_HTML=str_replace(array_keys($replace_word),$replace_word,$page_HTML) ;
              include_once(_DIR_ENGINE_EXT.'/macros/macros.php') ;
              macros_apply($page_HTML) ;
            }

            echo $page_HTML ;
            echo $this->prev_HTML ;

    $this->save_page_SEO_info($options) ; // сохраняем страницу в карту сайта

    //$this->show_SEO_edit_links($options) ; // показываем ссылки для редактирования SEO страницы (только для $__fast_edit_mode=='enabled' или !$options['use_body'])

    $this->show_fast_edit_links() ; // показываем ссылки для входа/выхода режима быстрого редактирования

    $this->logging_pages_info($options) ; // сохраняем показ страницы в лог (если условия разрешают)

    exec_on_day_cron() ;

    $this->show_time_trace($options) ; // показываем трассировку выполения страницы

  }

  function HTML($options=array()) {}

    function error_404()
    {  global $debug_page_relocation ;
       //trace() ;
       // перед тем как окончательно выдать сообщение о невозможности найти страницу, проверяем,
       // есть ли для текущего url страница в карте сайта, и если есть - не связан ли с ней объект
       // если объект связан, по коду можно вытащить его текущий url, а потом сделать редирект на него
       // ВАЖНО! если страница со старым url будет найдена, удалять её нельзя - так так последующие запросы должны переходить на неё
        //damp_array($this) ;
        if ($this->SEO_info['obj_reffer'] and !$this->unvalidate_params)
         { list($_pkey,$_tkey)=explode('.',$this->SEO_info['obj_reffer']) ;
           $rec_obj=select_db_obj_info($_tkey,$_pkey,array('add_usl'=>'enabled=1 and _enabled=1')) ;
           $new_url=(_USE_SUBDOMAIN)? $rec_obj['__href']:_MAIN_DOMAIN.$rec_obj['__href'] ;
              // если получен правильный url
              if ($new_url!=$this->SEO_url)
              { // помечаем текущую страницу в карте сайта как 301
                if ($this->SEO_info['pkey']) execSQL_update('update '.TM_SITEMAP.' set status=301 where pkey='.$this->SEO_info['pkey']) ;
             if ($debug_page_relocation) echo 'cur_page_url='.$this->SEO_url.'<br>' ;
             if ($GLOBALS['debug_page_relocation']) echo '<h2>Отладка redirect_301_to при error_404 для текущего объекта</h2>$this->SEO_url='.$this->SEO_url.'<br>$new_url='.$new_url.'<br>obj_reffer='.$this->SEO_info['obj_reffer'].'<br>' ;
             redirect_301_to($new_url,1) ;
           }
         }

       // помечаем текущую страницу в карте сайта как 404
       // если её еще нет в карте сайта сохраняем для последующего анализа ненайденных страниц
       if ($this->SEO_info['pkey']) execSQL_update('update '.TM_SITEMAP.' set status=404 where pkey='.$this->SEO_info['pkey']) ;
       else                         $this->save_page_SEO_info(array('status'=>404)) ;

      header("HTTP/1.0 404 Not Found");

      if (!_SHOW_404_AS_PAGE or $this->unvalidate_params)
      { $this->panel_404() ;
        return('exit') ;
      }
      return('') ;
    }


   //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   //
   //  ФОРМИРОВАНИЕ МЕТА-ТЕГОВ СТРАНИЦЫ
   //
   //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    // получить текущие мета-теги страницы  - вызывается в HTML перед выводом head страницы()
    // приоритет мета-тегов (с меньшего к большему)
    // 1. Теги по умолчанию
    // 2. Теги в скрипте
    // 3. Теги в объекте (но заданы в скрипте)
    // 4. Теги с SEO
    // к вызову функции уже могут быть заданы $this->title, $this->keywords, $this->description, $this->h1
    // а также $obj_info['title'], $obj_info['keywords'], $obj_info['description'], $obj_info['h1']
    function get_cur_page_meta_tags()
     { global $obj_info ; // описание объекта
       global $debug_metatags,$cur_system ;

       // берем сначала теги по умолчанию  - задаются в настройках сайта
       $def_title=$_SESSION['LS_def_page_title'] ;
       $def_keywords=$_SESSION['LS_def_page_keywords'] ;
       $def_description=$_SESSION['LS_def_page_descr'] ;
       $def_h1='Страница сайта' ;
       if ($this->result==404) $def_h1='Страница не найдена - 404' ;
       if ($debug_metatags) echo '<div class="green bold">Адрес страницы в карте сайта - '.$this->SEO_url.'</div>' ;
       if ($debug_metatags) echo '<div class="green bold">Объект страницы - '.$obj_info['obj_name'].'</div><br>' ;
       if ($debug_metatags) echo '<div class="green bold">Теги по умолчанию:</div><span class="red bold">title: </span>'.$_SESSION['LS_def_page_title'].'<br><span class="red bold">keywords: </span>'.$_SESSION['LS_def_page_keywords'].'<br><span class="red bold">descr: </span>'.$_SESSION['LS_def_page_descr'].'<br><span class="red bold">h1: </span>'.$def_h1.'<br>' ;
       // если определен текущий объект, то теги по умолчанию получаем на основе имени объекта
       if (sizeof($obj_info))
       { $obj_title='' ; $obj_keywords='' ;$obj_description='' ; $obj_h1='' ;
         if ($obj_info['obj_name'])                      $obj_title=$obj_info['obj_name'].(($_SESSION['LS_def_page_title'])? ' - '.$_SESSION['LS_def_page_title']:'') ;
         if ($obj_info['__name'])                        $obj_title=$obj_info['__name'].(($_SESSION['LS_def_page_title'])? ' - '.$_SESSION['LS_def_page_title']:'') ;
         if ($obj_info['__title'])                       $obj_title=$obj_info['__title'] ;

         if ($obj_info['obj_name'])                      $obj_h1=$obj_info['obj_name'] ;
         if ($obj_info['__name'])                        $obj_h1=$obj_info['__name'];
         if ($obj_info['__h1'])                          $obj_h1=$obj_info['__h1'] ;
         if ($_SESSION['__fast_edit_mode']=='enabled') $obj_h1.='<span class="fe" reffer="'.$obj_info['_reffer'].'"></span>' ;
         if ($debug_metatags) echo '<br><div class="green bold">Теги по умолчанию полученные из текущего объекта:</div><span class="red bold">title: </span>'.$obj_title.'<br><span class="red bold">keywords: </span>'.$obj_keywords.'<br><span class="red bold">descr: </span>'.$obj_description.'<br><span class="red bold">h1: </span>'.$obj_h1.'<br>' ;
         // переностим теги объекта в теги по умолчанию
         if ($obj_title)        $def_title=$obj_title ;
         if ($obj_keywords)     $def_keywords=$obj_title ;
         if ($obj_description)  $def_description=$obj_title ;
         if ($obj_h1)           $def_h1=$obj_h1 ;
       }
       // если заголовок еще не задан принудительно, пытаемся его получить из текущих объектов
       //if (!$this->h1 and $this->title)            $def_h1=$this->title ; // используем метатег title страницы
       //if (!$def_title and $this->h1)                  $def_title=$this->h1.' - '.$_SESSION['LS_def_page_title']  ;

       if ($debug_metatags) echo '<br><div class="green bold">Теги скрипта:</div><span class="red bold">title: </span>'.$this->title.'<br><span class="red bold">keywords: </span>'.$this->keywords.'<br><span class="red bold">descr: </span>'.$this->description.'<br><span class="red bold">h1: </span>'.$this->h1.'<br>' ;

       // если теги скрипта не заполнены - заполняем тегами по умолчнию (с участием объекта или из настроек сайта)
       //if (!$this->title)        $this->title=(($this->h1)? $this->h1.' - ':'').$def_title ;
       if (!$this->title)        $this->title=$def_title ;
       if (!isset($this->keywords))     $this->keywords=$def_keywords ;
       if (!isset($this->description))  $this->description=$def_description ;
       if (!$this->h1)           $this->h1=$def_h1 ;

       if (_CUR_PAGE_NUMBER>1)   { $this->title='Страница '._CUR_PAGE_NUMBER.' - '.$this->title ;
                                   $this->h1.='<span class=paginator> cтраница '._CUR_PAGE_NUMBER.'</span>' ;
                                   if ($debug_metatags) echo '<br><div class="green bold">Теги страницы c учетом пагинатора:</div><span class="red bold">title: </span>'.$this->title.'<br>h1: </span>'.$this->h1.'<br>' ;
                                 }

       // если есть теги в описании товара, используем их
       // 21.03.12 - отключено - теги в объектах более не используются
       //if ($obj_info['title'])         $this->title=$obj_info['title'] ;
       //if ($obj_info['keywords'])      $this->keywords=$obj_info['keywords'] ;
       //if ($obj_info['description'])   $this->description=$obj_info['description'] ;
       //if ($debug_metatags) echo '<div class="green bold">Теги объекта:</div><span class="red bold">title: </span>'.$obj_info['title'].'<br><span class="red bold">keywords: </span>'.$obj_info['keywords'].'<br><span class="red bold">descr: </span>'.$obj_info['description'].'<br>' ;

       if ($this->SEO_info['title'])       $this->title=$this->SEO_info['title'] ;
       if ($this->SEO_info['keywords'])    $this->keywords=$this->SEO_info['keywords'] ;
       if ($this->SEO_info['description']) $this->description=$this->SEO_info['description'] ;
       if ($this->SEO_info['h1'])          $this->h1=$this->SEO_info['h1'] ;
       if ($debug_metatags) echo '<br><div class="green bold">Теги страницы в SEO:</div><span class="red bold">title: </span>'.$this->SEO_info['title'].'<br><span class="red bold">keywords: </span>'.$this->SEO_info['keywords'].'<br><span class="red bold">descr: </span>'.$this->SEO_info['description'].'<br><span class="red bold">h1: </span>'.$this->SEO_info['h1'].'<br>' ;

       // добавляем приставку и суффикс к заголовку сайта
       if ($_SESSION['LS_pre_title'])  $this->title=$_SESSION['LS_pre_title'].' '.$this->title ;
       if ($_SESSION['LS_after_title']) $this->title.=' '.$_SESSION['LS_after_title'] ;
       if ($_SESSION['__fast_edit_mode']=='enabled') $this->title.=' - быстрое редактирование' ;

       if (!$this->title and $this->h1)
       {  $this->title=$this->h1 ;
          if ($debug_metatags) echo '<br><div class="green bold"><span class="red bold">this->title</span> не задан, используем значение <span class="red bold">this->h1</span>'.$this->h1.'<br>' ;
       }

       // очищаем метатеги от html-форматирования
       $this->title=strip_tags($this->title) ;
       $this->keywords=strip_tags($this->keywords) ;
       $this->description=strip_tags($this->description) ;

       if ($debug_metatags) echo '<br><div class="green bold">Теги итог:</div><span class="red bold">title: </span>'.$this->title.'<br><span class="red bold">keywords: </span>'.$this->keywords.'<br><span class="red bold">descr: </span>'.$this->description.'<br><span class="red bold">h1: </span>'.$this->h1.'<br>' ;

       // если путь не задан, пытаемся его получить из текущих объектов
       //if (!sizeof($this->path) and $obj_info['__page_path'])    $this->path=$obj_info['__page_path'] ; // сначала пытаемся получить путь у текущего объекта  - 28.08.11 - устарело, в топку
       if (!sizeof($this->path) and is_object($cur_system))      $this->path=$cur_system->get_cur_page_path() ; // потом - у текущей системы
       if (!sizeof($this->path))                                 $this->path['/']=_INDEX_PAGE_NAME ;
     }

   //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   //
   //  РАБОТА С КАРТОЙ САЙТА
   //
   //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

     function get_SEO_url()
     { global $SF_REQUEST_URI ;
       if ($GLOBALS['debug_SEO_info']) echo '<strong>Формируем SEO url</strong><br>' ;
       $page_addr=urldecode(_CUR_DOMAIN.$SF_REQUEST_URI) ;
       // 15.04.12 - отключено, т.к. по сути ничего не делало
       //echo '$page_addr='.$page_addr.'<br>' ;
       //$arr=parse_url($page_addr) ; damp_array($arr,1,-1) ;
       //$page_name=$arr['path'] ;
       //if ($arr['query']) $page_name.='?'.$arr['query'] ;
       //echo '$page_name='.$page_name.'<br>' ;
       if ($GLOBALS['debug_SEO_info']) echo 'SEO url='.$page_addr.'<br>' ;
       return($page_addr);
     }

     // возвращаем описание страницы из карты сайта для url
     function get_page_from_site_map_by_URL($url)
     { // ищем сначала по обработанному  urldecode url
       if ($GLOBALS['debug_SEO_info']) echo '<strong>Ищем запись SEO по обработанному  urldecode url</strong><br>' ;
       $rec=execSQL_van('select * from '.TM_SITEMAP.' where url like "'.addslashes($url).'" and enabled=1 order by url limit 1',array('debug'=>$GLOBALS['debug_SEO_info'])) ;
       // если страница не найдена, ищем по начальному url
       if (!$rec['pkey'])
       {   if ($GLOBALS['debug_SEO_info']) echo '<strong>Ищем запись SEO начальному url</strong><br>' ;
           $rec=execSQL_van('select * from '.TM_SITEMAP.' where enabled=1 and url="'.str_replace('%2F','/',urlencode($url)).'" order by url limit 1',array('debug'=>$GLOBALS['debug_SEO_info'])) ;
       }
       return($rec) ;
     }

     // возвращаем описание страницы из карты сайта для reffer
     function get_page_from_site_map_by_reffer($reffer='')
       {  // ищем любые теги для текущего объекта
          if ($GLOBALS['debug_SEO_info']) echo '<strong>Ищем запись SEO по _reffer текущего obj_info</strong><br>' ;
          // может быть найдена как запись с url так и без url
          $rec=execSQL_van('select * from '.TM_SITEMAP.' where enabled=1 and obj_reffer="'.$reffer.'" order by url limit 1',array('debug'=>$GLOBALS['debug_SEO_info'])) ;
       return($rec) ;
     }

     function save_page_SEO_info($options=array())
       { global $obj_info,$save_page_in_sitemap,$debug_save_SEO_info ;
         if (_ENGINE_MODE=='admin') return ;                  // находясь в админке
         if (IP==_IP_12_24_ru) return ;                       // если запрос делает сервер 12-24.ru

         if (strpos($this->SEO_url,'market.yandex.ru')!==false) return ;  // если url содержит стоп-слово типа /goods_319.html?utm_source=market.yandex.ru&utm_medium=cpc&utm_campaign=kartochka&_openstat=bWFya2V0LnlhbmRleC5ydTvQntC60YHQuNC80LXRgtGAIERPNjAwKyDQv9GA0LjQsdC-0YAg0LTQu9GPINC-0L_RgNC10LTQtdC70LXQvdC40Y8g0YDQsNGB0YLQstC-0YDQtdC90L3QvtCz0L4g0LrQuNGB0LvQvtGA0L7QtNCwOzE4MzQxNjQxNTU7

         // полное отклбчение сохранания страниц в карте сайте - просто выходим
         if (!$save_page_in_sitemap) return ;

         // если определен текущий объект, путь страницы совпадаетм с путем объекта и не запрещено сохранять код объекта в карту сайта
         //damp_array($obj_info,1,-1) ;
         $_CUR_PAGE_PATH=(_USE_SUBDOMAIN)? 'http://'._CUR_DOMAIN._CUR_PAGE_PATH:_CUR_PAGE_PATH ;
         $obj_reffer=($obj_info['pkey'] and ($obj_info['__href']==$_CUR_PAGE_PATH or _CUR_PAGE_IS_PAGINATOR) and !$this->no_obj_reffer_to_site_map)? $obj_info['_reffer']:'' ;
         if ($debug_save_SEO_info)
         {
           echo '<br><br><br><br><br><br><br><br><br><br>' ;
          // damp_array($obj_info) ;
           echo  '__href='.$obj_info['__href'].'<br>' ;
           echo  '_CUR_PAGE_PATH='._CUR_PAGE_PATH.'<br>' ;
           echo  'no_obj_reffer_to_site_map='.$this->no_obj_reffer_to_site_map.'<br>' ;

         }

         $script=hide_server_dir($this->script_file) ;
         $status=200 ;
         if (isset($this->result) and !is_array($this->result)) $status=$this->result ;
         if ($options['status']) $status=$options['status'] ;

         if ($debug_save_SEO_info)
         { echo '<br><br><br><br><br><br><br><br><br><br>' ;
           echo $obj_info['__href'].'<br>' ;
           echo _CUR_PAGE_PATH.'<br>' ;
           echo '$obj_reffer='.$obj_reffer.'<br>' ;
         }

         if (!$this->SEO_info['pkey'])
         { $finfo=array(  	  'parent'		    =>	1,
                              'clss'			=>	36,
                              'url'			    =>  $this->SEO_url,
                              'status'			=>  $status,
                              'obj_reffer'      =>  $obj_reffer,
                              'script'          =>  $script,
                              'class'           =>  $this->class_name
                           ) ;
           // если если информация о метатегах, значит она взята с первичной страницы объекта - эти теги нужно сохранить
           // но только если страница сайта не является страницей пагинатора
           if (!_CUR_PAGE_IS_PAGINATOR)
           { if ($this->SEO_info['title']) $finfo['title']=$this->SEO_info['title'] ;
             if ($this->SEO_info['keywords']) $finfo['keywords']=$this->SEO_info['keywords'] ;
             if ($this->SEO_info['description']) $finfo['description']=$this->SEO_info['description'] ;
           }

           $this->SEO_info['pkey']=adding_rec_to_table_fast(TM_SITEMAP,$finfo,array('debug'=>$debug_save_SEO_info)) ;
         }
         else
         { $finfo=array() ;
           //if ($obj_info['_reffer']!=$this->SEO_info['obj_reffer']) $finfo['obj_reffer']=$obj_info['_reffer'] ;
           //if ($obj_info['pkey'] and $obj_info['__href']!=_CUR_PAGE_PATH) unset($finfo['obj_reffer']) ;
           if ($obj_reffer and $this->SEO_info['obj_reffer']!=$obj_reffer) $finfo['obj_reffer']=$obj_reffer;
           if ($this->SEO_info['script']!=$script) $finfo['script']=$script ;
           if ($this->SEO_info['class']!=$this->class_name) $finfo['class']=$this->class_name ;
           if ($this->SEO_info['status']!=$status) $finfo['status']=$status ;
           // если были найдена SEO без url - сохраняем текущий url. Но найдено она могла быть только по _reffer
           if (!$this->SEO_info['url']) $finfo['url']=$this->SEO_url ;
           if (sizeof($finfo))
           { update_rec_in_table(TM_SITEMAP,$finfo,'pkey='.$this->SEO_info['pkey'],array('debug'=>$debug_save_SEO_info)) ;
             //echo 'Обновлена запись в SITE MAP' ;
             //damp_array($finfo) ;
           }
           else if ($debug_save_SEO_info) echo 'Изменения записи в карте сайта не требуется<br>' ;
         }
         //echo $this->SEO_url ;
         if ($debug_save_SEO_info) damp_array($this->SEO_info) ;
       }




    // выбираем результат работы функции из кеша, если она уже была отработана ранее или
    // выполняем функции и результат запоминаем в кеше
     function show_cache($func_name)
     { global $reboot_all ;
     	if (!isset($_SESSION[$func_name]) or $reboot_all)
     	{ ob_start();
   	      $this->$func_name();
   	      $_SESSION[$func_name] = ob_get_clean();
     	}
     	echo $_SESSION[$func_name] ;
     }

     function show_SEO_edit_links($options)
     {
     }



      function show_fast_edit_links($options=array())
       { global $enabled_fast_edit_to_system_ip ;
         if ($options['use_body']) return ; // только для главного макета
         if ($_SESSION['__fast_edit_mode']!='enabled') return ; // только в режиме быстрого редактирования
         if (_ENGINE_MODE=='admin') return ; // только в режиме быстрого редактирования

                  ?><div id="panel_fast_edit_operation"><div class=title>Режим быстрого редактирования сайта</div><?
                  if ($this->SEO_info['pkey']) {?><div class="item"><a href="" onclick="show_obj(<?echo $this->SEO_info['tkey']?>,<?echo $this->SEO_info['pkey']?>);return(false);">Редактировать SEO тексты этой страницы - "<?echo $this->SEO_url?>"</a></div><?}
                  ?><div class="item"><a href="<?echo _PATH_TO_ADMIN?>/fastedit.php?mode=disabled&to=<?echo _CUR_PAGE_PATH?>">Выйти из режима быстрого редактирования сайта</a></div><?
                  ?></div><style type=text/css>
                     div#panel_fast_edit_operation{position:fixed;bottom:20px;left:20px;background:#FDF98B;padding:10px;z-index:10000;
                         -webkit-box-shadow: 0px 0px 20px 0px rgba(50, 50, 50, 0.44);
                         -moz-box-shadow:    0px 0px 20px 0px rgba(50, 50, 50, 0.44);
                         box-shadow:         0px 0px 20px 0px rgba(50, 50, 50, 0.44);
                         font-family:arial, helvetica;
                     }
                     div#panel_fast_edit_operation div.title{margin:10px 0;font-weight:bold;text-transform:uppercase;}
                     div#panel_fast_edit_operation div.item{margin:10px 0;}
                  </style><?

         /*
         if (_IS_SYSTEM_IP and $enabled_fast_edit_to_system_ip and _ENGINE_MODE!='admin' and $_SESSION['__fast_edit_mode']!='enabled') echo '<div><br><a href="'._PATH_TO_ADMIN.'/fastedit.php?mode=enabled">Войти в режим быстрого редактирования</a></div>';
         if ($_SESSION['__fast_edit_mode']=='enabled') {?><div><br><a href="<?echo _PATH_TO_ADMIN?>/fastedit.php?mode=disabled&to=<?echo _CUR_PAGE_PATH?>">Выйти из режима быстрого редактирования сайта</a></div><br><br><br><br><br><br><br><br><?}
         */
       }

  //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  //
  //  РАБОТА С ЖУРНАЛОМ СТРАНИЦ
  //
  //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  // сохранение в базу информации по показанной странице
  function logging_pages_info($options)
    { global $log_pages_info,$log_pages_for_system_ip,$log_pages_robots ;
      // статистика не собирается если:
      if (_IS_SYSTEM_IP and !$log_pages_for_system_ip) return ; // клиент в списке своих - в журнал не сохраняем
      if (!$log_pages_info) return ;  					    // выключен сбор статистики
      if (!$log_pages_robots and $GLOBALS['flags']['is_robot']) return ;  		// выключен сбор статистики по роботам
      if (_ENGINE_MODE=='admin') return ;                  // находясь в админке
      if ($options['no_reg_in_log_pages']) return ;        // прямой запрет

      // делаем расчет числовых параметров генерации страницы
      global $__times,$__memory,$__debug_sql_cnt,$__debug_sql_time,$robot_sess_name ;
      if (sizeof($__times)) { $start_time=reset($__times) ;  $last_time=end($__times) ; $gen_time=round($last_time-$start_time,6) ;  } else  $gen_time=0 ;
      if (sizeof($__memory)) $last_mem=end($__memory) ; else $last_mem=0 ;
      if (!$__debug_sql_cnt) $__debug_sql_cnt=0 ;

      $url_from=addslashes(mb_substr(urldecode($_SERVER['HTTP_REFERER']),0,256)) ;
      $reffer_text_find=get_se_query($url_from) ;

      global $obj_info ;

      $info=array(		'c_data'		=>	time(),
       					'ip'			=>	IP,
   	     				'url'			=>	$this->SEO_url,
   	     				'obj_name'		=>	$this->title,
   	     				'reffer'		=>	$obj_info['_reffer'],
   	     				'session_id'	=>  ($GLOBALS['flags']['is_robot'])? $robot_sess_name:session_id(),
   	     				'url_from'		=>	str_replace("'","&#039",$url_from),
   	     				'search_text'	=>  $reffer_text_find,
   	     				'user_agent'	=>  $_SERVER['HTTP_USER_AGENT'],
   	    				'time_gen'		=>	$gen_time,
   	    				'mem_size'		=>	$last_mem,
   	    				'sql_cnt'		=>  $__debug_sql_cnt,
   	    				'sql_time'		=>  round($__debug_sql_time,4),
   	    				'use_cache'		=>  $this->result,
   	    				'is_robot'		=>	$GLOBALS['flags']['is_robot'],
   	    				'is_reboot'		=>	($GLOBALS['flags']['is_reboot'])? 1:0,
   	    				'page_keywords' =>	$this->keywords,
   	    				'page_description' =>	$this->description
   	    			) ;

     if ($_COOKIE['GeoIP_info']) {$info['country']=$_COOKIE['GeoIP_country'] ; $info['city']=$_COOKIE['GeoIP_city'] ; }

      global $partners_system,$partner_uid ; if (isset($partners_system) and $partner_uid)  $info['partner_uid']=$partner_uid ;

      adding_rec_to_table_fast(TM_LOG_PAGES,$info,array('no_default_fields'=>1,'no_return_id'=>1)) ;
    }

    function show_time_trace($options)
    { global $time_line_mode,$__sql_trace,$__debug_sql_time,$__debug_sql_cnt,$__times,$alert_to_slow_page ; ;
      if ($time_line_mode and $options["content_type"]!="text/xml" and !$options["no_time_trace"])
      { ?><div id=time_trace><?
        show_time_line($time_line_mode/*,$text*/) ;
        $time=round($__debug_sql_time,4) ;
        if ($GLOBALS['debug_db']) { echo '<br>Всего запросов:'.$__debug_sql_cnt.', время выполения: '.$time.'<br>' ;
        damp_array($__sql_trace,1,-1);

                                   }
        $list_files=get_included_files() ;
        echo '<br>Все подклбченные файлы:'.sizeof($list_files) ;
        damp_array($list_files,1,-1) ;
        echo '<br><br>!Размер файла сессии: ' ;
        $sess=serialize($_SESSION) ;
        echo strlen($sess).'<br>' ;

        ?></div><?
      }

      $t=$__times['end']-$__times['start'] ;
      if ($t>1 and $alert_to_slow_page)  // если время открытия страниц более секунды - отправляем сообщение, что сайт тупит
      {  ob_start() ;
         show_time_line(2/*,$text*/) ;
         echo '<br>Всего запросов:'.$__debug_sql_cnt.', время выполения: '.round($__debug_sql_time,4).'<div></div>' ;
         damp_array($__sql_trace,1,-1);
         $text=ob_get_clean() ;
         _send_mail('debug@12-24.ru','Тупит сайт '._BASE_DOMAIN,$text,array('debug'=>0,'no_reg_events'=>1,'Content-type'=>'text/html')) ;
      }
    }
}

?>