<?
// ВНИМАНИЕ!!! используется передача больших объемов данных через сессию, что не есть хорошо


// импорт объектов из таблиц XML
 function import_from_xml_excel()
 {  global $obj_info,$tkey ;
    ?><h1>Импорт содержимого из таблицы XML Exel XML в раздел "<?echo $obj_info['obj_name']?>"</h1>
    <span class=bold>Шаг 1 из 3</span><br><br>
	   Файл таблицы <INPUT type=file name='xml_source_file' /><br><br>
	   Тип создаваемых объектов:
	    <select size="1" name="select_clss" class='text'>
  		  <? if (sizeof(_DOT($tkey)->list_clss_ext)) foreach (_DOT($tkey)->list_clss_ext as $clss=>$clss_info)
  		  	  {?><option value="<?echo $clss?>" class='text'><?echo _CLSS($clss)->name?></option> <?}?>
		</select><br><br>
	   Учитывать выделение цветом: <input name="use_color_select" type="checkbox" value="ON" checked><br><br>
	  <button class=button cmd=import_from_xml_excel_step_2 mode=dialog>Загрузить файл</button>
  <?
 }


 function get_field_names($select_clss,$tkey)
 {    $obj_clss=_CLSS($select_clss) ;
      $list_clss_fields=$obj_clss->fields ;
	  unset($list_clss_fields['pkey']) ;
	  unset($list_clss_fields['parent']) ;
	  unset($list_clss_fields['clss']) ;
	  unset($list_clss_fields['c_data']) ;
	  unset($list_clss_fields['r_data']) ;
	  if (sizeof($list_clss_fields)) foreach($list_clss_fields as $fname=>$ftype)
	   { unset($_legal_values) ;
	   	 $legal_values=$ftype ;
	   	 $list_clss_fields[$fname]=array($fname,$legal_values) ;
	   	 if (isset($obj_clss->view['details']['field'][$fname]['title'])) 		    $list_clss_fields[$fname][0]=$obj_clss->view['details']['field'][$fname]['title'] ;
	   	 if (isset($obj_clss->view['list']['field'][$fname]['title']))    		    $list_clss_fields[$fname][0]=$obj_clss->view['list']['field'][$fname]['title'] ;
	   	 if (isset($obj_clss->view['details']['field'][$fname]['indx_select'])) 	$_legal_values=$obj_clss->view['details']['field'][$fname]['indx_select'] ;
		 if (isset($obj_clss->view['list']['field'][$fname]['indx_select'])) 		$_legal_values=$obj_clss->view['list']['field'][$fname]['indx_select'] ;

         if ($_legal_values) { global ${$_legal_values} ;
         					   $list_values=implode(',',array_values(${$_legal_values})) ;
         	                   $legal_values='Одно из значений: '.$list_values.',<br> другие значения будут проигновированны' ;
         					 }

		 if (isset(_DOT($tkey)->list_connect[$select_clss][$fname]))
		       { $indx_tkey=_DOT($tkey)->list_connect[$select_clss][$fname]['slave_table'] ;
		         $legal_values='Значения из списка "'._DOT($indx_tkey)->name.'",<br> отсутствующие значения будут добавлены' ;
               }

         $list_clss_fields[$fname][1]=$legal_values ;


	   }

	  $list_clss_fields['parent']=array('Код раздела','Целое число') ;
      $list_clss_fields['pkey']=array('Код товара','Целое число') ;

	  // добавляем возможные дочерние элементы, описанные в parent_to
	  if (sizeof($obj_clss->parent_to)) foreach($obj_clss->parent_to as $child_clss)
	  { $obj_clss=_CLSS($child_clss);
	    $child_name=$obj_clss->name ;
	    $list_child_clss_fields=$obj_clss->fields ;
	    $cur_tkey=_DOT($tkey)->list_clss[$child_clss] ;

		unset($list_child_clss_fields['pkey']) ;
		unset($list_child_clss_fields['parent']) ;
		unset($list_child_clss_fields['clss']) ;
		unset($list_child_clss_fields['c_data']) ;
		unset($list_child_clss_fields['r_data']) ;

		if (sizeof($list_child_clss_fields)) foreach($list_child_clss_fields as $fname=>$ftype)
		   { $legal_values=$ftype ;
		   	 if (isset($obj_clss->view['details']['field'][$fname]['title'])) $list_child_clss_fields[$fname]=$obj_clss->view['details']['field'][$fname]['title'] ;
			 if (isset($obj_clss->view['list']['field'][$fname]['title']))    $list_child_clss_fields[$fname]=$obj_clss->view['list']['field'][$fname]['title'] ;
		   	 if (isset($obj_clss->view['details']['field'][$fname]['indx_select'])) $legal_values=$obj_clss->view['details']['field'][$fname]['indx_select'] ;
			 if (isset($obj_clss->view['list']['field'][$fname]['indx_select']))    $legal_values=$obj_clss->view['list']['field'][$fname]['indx_select'] ;

			 if (isset(_DOT($cur_tkey)->list_connect[$child_clss][$fname]))
		       { $indx_tkey=_DOT($cur_tkey)->list_connect[$child_clss][$fname]['slave_table'] ;
		         $legal_values='Значения из списка "'._DOT($indx_tkey)->name.'", отсутствующие значения будут добавлены' ;
               }

			 $list_clss_fields[$child_clss.'.'.$fname][0]=$child_name.'.'.$list_child_clss_fields[$fname] ;
			 $list_clss_fields[$child_clss.'.'.$fname][1]=$legal_values ;



			 //$list_clss_fields[$child_clss.'.'.$fname][1]=$child_name.'.'.$list_child_clss_fields[$fname][1] ;
		   }

        //damp_array($list_child_clss_fields) ;
	  }
	  return($list_clss_fields) ;
 }


 function import_from_xml_excel_step_2()
 {  global $obj_info,$xml_source_file,$select_clss,$use_color_select ;

    //set_time_limit(0) ;
    //include_once('i_XML.php') ;

    ?><h1>Импорт содержимого из таблицы XML Exel XML в раздел "<?echo $obj_info['obj_name']?>"</h1>
    <span class=bold>Шаг 2 из 4</span><br><br>
    <?
    $_SESSION['XML_source']=file_get_contents($xml_source_file) ;
	$dom = new DomDocument();
   if ($dom->loadXML($_SESSION['XML_source']))
	{ $_SESSION['list_recs']=import_ARR_from_XML_exel($dom) ; //print_array($_SESSION['list_recs']) ;
	  $list_styles=import_styles_from_XML_exel($dom) ;
	  if (sizeof($list_styles)) foreach($list_styles as $rec) $arr_colors[$rec['color']]=1 ;

	  if (sizeof($_SESSION['list_recs']))
	  { echo '<span class="green bold">Загружено: '.sizeof($_SESSION['list_recs']).' записей </span><br><br>' ;
	    if ($use_color_select)
	    {
		    echo '<span class="black bold">Укажите цвет ячеек, которые будут импортироваться:</span><br><br>' ;
	        ?><table><tr><td>Цвет</td><td>импортировать</td></tr><?
	        if (sizeof($arr_colors)) foreach($arr_colors as $color_code=>$i)
	         { ?><tr><td style="background-color:<?echo $color_code?>;width:100px;"><?echo ($color_code)? $color_code:'без выделения'?></td><td><input name="selected_colors[<?echo $color_code?>]" type="checkbox" value="ON"></td></tr><?}

		    ?></table><br><br>
		<?}?>
        <input name="select_clss" type="hidden" value="<?echo $select_clss?>">
        <button onclick="exe_cmd('')">Отмена</button>
        <button class="button" cmd=import_from_xml_excel_step_3 mode=dialog>Далее</button>
        <h1>Загруженные записи</h1>
        <?
        print_2x_arr($_SESSION['list_recs']) ;
	  } else echo 'Загруженный Вами файл не соответствует формату Microsoft Exel XML или файл пуст<br>' ;
	}
	else echo 'Неверный формат XML' ;
 }

 function import_from_xml_excel_step_3()
 {  global $obj_info,$select_clss,$tkey,$selected_colors ;

    //damp_array($selected_colors) ;

    //set_time_limit(0) ;
    //include_once('i_XML.php') ;

    ?><h1>Импорт содержимого из таблицы XML Exel XML в раздел "<?echo $obj_info['obj_name']?>"</h1>
    <span class=bold>Шаг 3 из 4</span><br><br>
    <?
    $dom = new DomDocument();
    if ($dom->loadXML($_SESSION['XML_source']))
	{ $list_styles=import_styles_from_XML_exel($dom) ;
	  //print_array($list_styles) ;
	  if (sizeof($list_styles)) foreach($list_styles as $name_style=>$info_style) if (isset($selected_colors[$info_style['color']])) $selected_styles[$name_style]=1 ;
      if (isset($selected_colors[0])) $selected_styles['Default']=1 ;
      //print_array($selected_styles) ;

	  $_SESSION['list_recs']=import_ARR_from_XML_exel($dom,array('only_styles'=>$selected_styles)) ;

	  //print_array($list_styles) ;
	  //print_2x_arr($_SESSION['list_recs']) ;

	  // формируем ожимаемый набор полей таблицы
      $list_clss_fields=get_field_names($select_clss,$tkey) ;

	  if (sizeof($_SESSION['list_recs']))
	  { echo '<span class="green bold">Загружено: '.sizeof($_SESSION['list_recs']).' записей, включая заголовок</span><br><br>' ;
	    echo '<span class="black bold">Произведите сопоставление полей:</span><br><br>' ;


	    ?><table><tr class=clss_header><?
	    list($indx,$rec)=each($_SESSION['list_recs']) ;
	    if (sizeof($rec)) foreach($rec as $i=>$field)
	     {?><td><?
		      if (strpos($field,'.')===false) echo '<input name="use_as_index" type="radio" value="'.$i.'" checked> ключ'  ;
		      ?><br><br><?
		      if (sizeof($list_clss_fields))
			    { echo '<select name=sel_XML_OBJ['.$i.']><option value=""></option>' ;
			      foreach ($list_clss_fields as $fname=>$show_info)
			      { $status=($show_info[0]==$field)? ' selected ':'' ;
			      	?><option value="<?echo $fname?>" class='text' <?echo $status?>><?echo $show_info[0]?></option><?
			      }
			      echo '</select>' ;
			    }

	      ?></td><?
	     }
	    ?></tr><?
	    reset($_SESSION['list_recs']) ;
	    foreach($_SESSION['list_recs'] as $i=>$rec)
	    { ?><tr><? foreach($rec as $value) echo '<td>'.$value.'</td>' ; ?></tr><?
	      if ($i>10) break ;
	    }
	    ?></table><?


	    ?></table><br><input name="first_rec_header" type="checkbox" value="1" checked>Первая запись - заголовки<br><br>
        <input name="use_as_index" type="radio" value="-1" checked>Не обновлять существующие записи, только добавлять<br><br>
        <input name="enabled_update" type="checkbox" value="1" checked>Разрешить обновление существующих записей. Если снять эту галку обновление существующих записей не произойдет<br><br>
        <input name="enabled_insert" type="checkbox" value="1" checked>Разрешить добавление новых записей. Если снять эту галку добавление новых записей не произойдет<br><br>




        <input name="select_clss" type="hidden" value="<?echo $select_clss?>">
        <button class=button cmd=import_from_xml_excel_step_4 mode=dialog>Импортировать объекты</button><?

	  } else echo 'Загруженный Вами файл не соответствует формату Microsoft Exel XML или файл пуст<br>' ;
	}
	else echo 'Неверный формат XML' ;
 }

 function import_from_xml_excel_step_4()
 { global $select_clss,$sel_XML_OBJ,$obj_info,$first_rec_header,$tkey,$use_as_index,$enabled_update,$enabled_insert ;
   ?><h1>Импорт содержимого из таблицы XML Exel в раздел "<?echo $obj_info['obj_name']?>"</h1>
     <span class=bold>Шаг 4 из 4</span><br><br>
   <?
   if ($first_rec_header) unset($_SESSION['list_recs'][0]) ;
   $index_fname=$sel_XML_OBJ[$use_as_index] ;
   echo '<span class="green bold">Будет импортировано: '.sizeof($_SESSION['list_recs']).' записей </span><br><br>' ;

   if ($index_fname) echo 'Используется режим <span class="red bold">обновления </span> существующих записей, отсутствующие записи будут добавлены<br><br>' ;
    else echo 'Используется режим <span class="red bold">добавления </span> новых записей, все записи будут добавлены<br><br>' ;

   //echo 'Массив соотвествий:' ; damp_array($sel_XML_OBJ) ;

   // удаляем из массива соотвествий пустые значения
   if (sizeof($sel_XML_OBJ)) foreach($sel_XML_OBJ as $i=>$value) if (!$value) unset($sel_XML_OBJ[$i]) ;

   //echo 'Очищенный массив соотвествий:' ; damp_array($sel_XML_OBJ) ;

   // сначала проходим по списку значений и заменяем значения индексных полей кодами
   if (sizeof($sel_XML_OBJ)) foreach($sel_XML_OBJ as $i=>$fname)
   { if (strpos($fname,'.')===false) $cur_clss=$select_clss ; else list($cur_clss,$fname)=explode('.',$fname) ;
     $cur_tkey=_DOT($tkey)->list_clss[$cur_clss] ;
     //echo $cur_clss.':'.$fname.' - '.$cur_tkey.'<br>' ;

     // если поле связоно с индексной таблицей
     if (isset(_DOT($cur_tkey)->list_connect[$cur_clss][$fname]))
       { $indx_tkey=_DOT($cur_tkey)->list_connect[$cur_clss][$fname]['slave_table'] ;
         $slave_field=_DOT($cur_tkey)->list_connect[$cur_clss][$fname]['slave_field'] ;
         $slave_show=_DOT($cur_tkey)->list_connect[$cur_clss][$fname]['slave_show'] ;
         //damp_array(_DOT($cur_tkey)->list_connect[$cur_clss][$fname]) ;
         // проходим по всем записям импортируемого списка, собираем уникальный набор значений полей и одновременно вписываем в него соответствющий код индексного значения
       	 if (sizeof($_SESSION['list_recs'])) foreach($_SESSION['list_recs'] as $XML_rec) if ($XML_rec[$i]) $arr_indx_values[$indx_tkey][$XML_rec[$i]]='' ;
       	 if (sizeof(_DOT($indx_tkey)->values)) foreach(_DOT($indx_tkey)->values as $save_rec)
       	  if (isset($arr_indx_values[$indx_tkey][$save_rec[$slave_show]])) $arr_indx_values[$indx_tkey][$save_rec[$slave_show]]=$save_rec[$slave_field] ;

		 // имеет массив индексных значений сгруппированный по коду таблиц, для существующих значений проставлен код, для отстуствующих - пусто
		 $flag_table_update=0 ;
		 // !!! отлючено в связи с отказов поддержки индексных таблиц
         //_DOT($indx_tkey)->upload_indx_values() ;

		 if (sizeof($arr_indx_values[$indx_tkey])) foreach($arr_indx_values[$indx_tkey] as $value=>$key) if ($key=='')
		     { // добавляем в таблицу отсутствующие значения
     	       $new_pkey=adding_rec_to_table($indx_tkey,array($slave_show=>$value),array('type'=>'indx')) ;
               if ($slave_field!='pkey')
                   { $arr=execSQL_van('select '.$slave_field.' from '._DOT($indx_tkey)->table_name.' where pkey='.$new_pkey) ;
                     $key=$arr[$slave_field] ;
                   } else $key=$new_pkey ;

               $arr_indx_values[$indx_tkey][$value]=$key ;
               echo 'В список <span class=bold>\''._DOT($indx_tkey)->name.'\'</span> добавлено значение <span class="green bold">\''.$value.'\'</span><br>' ;
               $flag_table_update=1 ;
		     }

	     // !!! отлючено в связи с отказов поддержки индексных таблиц
         //if ($flag_table_update) _DOT($indx_tkey)->upload_indx_values() ;

         if (sizeof($_SESSION['list_recs'])) foreach($_SESSION['list_recs'] as $j=>$XML_rec) if ($XML_rec[$i]) $_SESSION['list_recs'][$j][$i]=$arr_indx_values[$indx_tkey][$XML_rec[$i]] ;
       }

     // готовим специальный список для поля валюты, так как текущий список не подходит
     //if ($fname=='val')

     // если поле связано со списоком-массивом заменеям значение на кодл соответствующего значения из списка
     $obj_clss=_CLSS($cur_clss) ;
     if (isset($obj_clss->view['list']['field'][$fname]['indx_select']) or isset($obj_clss->view['details']['field'][$fname]['indx_select']))
     {  $name_array=($obj_clss->view['list']['field'][$fname]['indx_select'])? $obj_clss->view['list']['field'][$fname]['indx_select']:$obj_clss->view['details']['field'][$fname]['indx_select'] ;
        $indx_field=($obj_clss->view['list']['field'][$fname]['indx_field'])? $obj_clss->view['list']['field'][$fname]['indx_field']:$obj_clss->view['details']['field'][$fname]['indx_field'] ;
        $indx_array=array() ;
        //echo 'name_array='.$name_array.'<br>' ;
        global ${$name_array} ;
        // если список не плоский а содержит массивы, необходмо подготовить из него плоский список
     	list($id,$rec1)=each(${$name_array}) ;
        if (is_array($rec1)) foreach(${$name_array} as $id=>$rec_array) $indx_array[$id]=$rec_array[$indx_field] ; else $indx_array=${$name_array} ;
        //damp_array($indx_array,1,-1) ;
     	if (sizeof($_SESSION['list_recs'])) foreach($_SESSION['list_recs'] as $j=>$XML_rec) if ($XML_rec[$i])
     	{ //if (in_array($XML_rec[$i],${$name_array})) echo $XML_rec[$i].'='.array_search($XML_rec[$i],${$name_array}).'<br>' ;
     	  //else echo 'Значение '.$XML_rec[$i].' для поля '.$fname.' будет проигнорированно<br>' ;
          if ($fname=='val') $XML_rec[$i]=strtoupper($XML_rec[$i]) ;
          if (in_array($XML_rec[$i],$indx_array)!==false) $_SESSION['list_recs'][$j][$i]=array_search($XML_rec[$i],$indx_array) ;
     	    else { echo '<span class=red>Строка '.($j+1).': значение "'.$XML_rec[$i].'" для поля '.$fname.' будет проигнорированно</span><br>' ;
     	  		   unset($_SESSION['list_recs'][$j][$i]) ;
     	  	     }
          //echo 'res1='.in_array($XML_rec[$i],${$name_array}).'<br>' ;
          //echo 'res2='.array_search($XML_rec[$i],${$name_array}).'<br>' ;

     	}
     }

   }

   //damp_array($_SESSION['list_recs']) ;

   // если задано ключевое поле для обновления данных, то делаем выборку из таблицы всех объектов данного класса, не учитываем пустые поля
   if ($index_fname)
   { $_exist_values=execSQL('select pkey,'.$index_fname.' from '._DOT($tkey)->table_name.' where clss='.$select_clss) ;
     if (sizeof($_exist_values)) foreach($_exist_values as $rec) if ($rec[$index_fname]) $exist_values[$rec[$index_fname]]=$rec['pkey'] ;
     //damp_array($exist_values) ;
     unset($_exist_values) ;
   }

   $cnt_insert=array() ;
   $cnt_update=array() ;
   // массив приведен к виду, в которм его можно сохранить в БД
   if (sizeof($_SESSION['list_recs'])) foreach($_SESSION['list_recs'] as $XML_rec)
    { $new_obj=array()  ;

      // если запись уже существует в таблице, то вписываем в новый объект код этой записи
      if ($index_fname and isset($exist_values[$XML_rec[$use_as_index]])) $new_obj['pkey']=$exist_values[$XML_rec[$use_as_index]] ;

   	  // сопоставляем поля и значения по списку сопоставлений
   	  // ВНИМАНИЕ!!! значения полей parent и pkey будут использованы из импортируемой таблицы, если они там определеы
   	  if (sizeof($sel_XML_OBJ)) foreach($sel_XML_OBJ as $i=>$fname) if (strpos($fname,'.')===false) if (isset($XML_rec[$i])) $new_obj[$fname]=$XML_rec[$i] ;

      //echo 'Массив для записи в базу:<br>' ; damp_array($new_obj) ;

      // если в новом объекте нет кода то добавляем новую запись, иначе производим обновление
      $obj_name=($new_obj['obj_name'])? '"'.$new_obj['obj_name'].'"':'' ;
      $obj_name.=($new_obj['art'])? ' арт "'.$new_obj['art'].'"':'' ;

      if (!$new_obj['pkey'])
      { echo '<span style="color:#6495ed;">Не найдено:</span> '.$obj_name ;
        if (!$new_obj['parent']) $new_obj['parent']=$obj_info['pkey'] ;
       	if (!$new_obj['clss']) 	$new_obj['clss']=$select_clss ;
       	if ($enabled_insert)
        { save_obj_in_table($tkey,$new_obj,array('debug'=>0)) ;
          echo ' - <span style="color:#009933;">данные добавлены</span>' ;
          $cnt_insert[$select_clss]++ ;
        }
        echo '<br>' ;
      }
      else
      { echo '<span style="color:#009933;">Найдено: </span>'.$obj_name ;
        if ($enabled_update)
        { update_rec_in_table($tkey,$new_obj,'pkey='.$new_obj['pkey']) ;
	      echo ' - <span style="color:#009933;">данные обновлены</span>' ;
          $cnt_update[$select_clss]++ ;
	    }
        echo '<br>' ;
      }
    }
   echo '<br>' ;
   if (sizeof($cnt_insert)) foreach($cnt_insert as $clss=>$clss_cnt) echo '<span class="green bold">Cоздано: '.$clss_cnt.' объектов класса \''._CLSS($clss)->name.'\'</span><br>' ;
   if (sizeof($cnt_update)) foreach($cnt_update as $clss=>$clss_cnt) echo '<span class="green bold">Обновлено: '.$clss_cnt.' объектов класса \''._CLSS($clss)->name.'\'</span><br>' ;

   ?><br><button onclick="exe_cmd('')">Выход</button><?
 }


// создаем массив записей на основе таблицы Exel XML
 function import_ARR_from_XML_exel(&$dom,$options=array())
 {   $objs_xml=$dom->getElementsByTagName("Row");
     if ($options['only_styles']) $list_styles=$options['only_styles'] ;
     $i=0 ;
	 if (sizeof($objs_xml)) foreach ($objs_xml as $obj_xml)
	  { unset($obj) ;
	    //
	    if (sizeof($list_styles))
	    { $style_name=$obj_xml->getAttribute('ss:StyleID') ;
	      //echo '<br>ROW style_name='.$style_name.'  ' ;
	      if ($style_name) $flag_row=(isset($list_styles[$style_name]))? 1:0 ; else $flag_row=1 ;
	      //echo 'flag_row='.$flag_row.'<br>' ;
	    } else $flag_row=1 ;

	    if (!$i) $flag_row=1 ;

	    if ($flag_row) foreach($obj_xml->childNodes as $prop_xml) if ($prop_xml->nodeName=='Cell')
	     { if (sizeof($list_styles))
		    { $style_name=$prop_xml->getAttribute('ss:StyleID') ;
		      if (!$style_name) $style_name='Default' ;
		      if ($style_name) $flag_cell=(isset($list_styles[$style_name]))? 1:0 ; else $flag_cell=1 ;
		      //echo 'CELL style_name="'.$style_name.'"  ' ;
		      //echo 'flag_cell='.$flag_cell.'<br>' ;


		    } else $flag_cell=1 ;

	       if (!$i) $flag_cell=1 ;

	       if ($flag_cell)
	       	  { $i=$prop_xml->getAttribute('ss:Index') ;
	       		//if (!$i) $obj[]=trim(iconv('utf-8','windows-1251',$prop_xml->nodeValue)) ;
	       		if (!$i) $obj[]=trim($prop_xml->nodeValue) ;
	       		//else $obj[$i-1]=trim(iconv('utf-8','windows-1251',$prop_xml->nodeValue)) ;
	       		else $obj[$i-1]=trim($prop_xml->nodeValue) ;
	          }
	     }

	    $i++ ;
	    if (sizeof($obj)) $objs[]=$obj ;
	    if ($options['limit'] and sizeof($objs)>=$options['limit']) break ;
	  }
     return($objs) ;
 }

// создаем массив цветов на основе таблицы Exel XML
 function import_styles_from_XML_exel(&$dom,$options=array())
 {   global $tkey,$obj_info,$replace_indent,$save_pkey ;
     $objs_xml=$dom->getElementsByTagName("Style");
	 if (sizeof($objs_xml)) foreach ($objs_xml as $obj_xml)
	  { unset($obj) ;
	    $style_name=$obj_xml->getAttribute('ss:ID') ;

        unset($color) ;

	    foreach($obj_xml->childNodes as $prop_xml) if ($prop_xml->nodeName=='Interior')
	     { $styles[$style_name]['color']=$prop_xml->getAttribute('ss:Color') ;
	     }
	  }
     return($styles) ;
 }


?>