<?
function import_stock_from_excel()
{ //damp_array($_SESSION['VL_arr']) ;
  ?><h1>Импорт склада из файла excel</h1>
     <strong>1. Подготовте файл:</strong>
        <ul><li>1-й столбец - <select name=col1_fname><option value=art>Артикул</option><option value=obj_name>Наименование</option><option value=pkey>Код товара</option></select></li>
            <li>2-й столбец - <select name=col2_fname><option value=stock>Склад</option></select></li>
        </ul>
        <br>
     <strong>2. Загрузите файл</strong> <INPUT type=file name='import_file' /><br><br>
   <button cmd=import_stock_from_excel_step_2 class=button_green>Импорт склада</button>
   <input type=checkbox name=test_mode value=1> Тест (не обновлять склад)
   <input type=checkbox name=check_goods value=1> Контрольный запрос к базе на обновленный товар
   <input type=checkbox name=show_excel_table value=1> Показать импортированную таблицу из файла
  <?
}

  function import_stock_from_excel_step_2()
  { //damp_array($_FILES['import_file']) ;
    if (is_uploaded_file($_FILES['import_file']['tmp_name']))
    { if ($_FILES['import_file']['tmp_name'])
      { $desc_file=_DIR_TO_ROOT.'/temp/'.safe_file_names($_FILES['import_file']['name']) ;
        move_uploaded_file($_FILES['import_file']['tmp_name'],$desc_file) ;
        echo '<h2>Файл сохранен в <strong>'.hide_server_dir($desc_file).'</strong></h2><br>' ;
        echo 'Размер файла: '.format_size($_FILES['import_file']['size']).'<br><br>' ;
        if ($_FILES['import_file']['type']!='application/octet-stream' and $_FILES['import_file']['type']!='application/vnd.ms-excel')
         { echo '<div class=alert>Загруженный файл не является файлом Excel. Попробуйте еще раз.</div>' ;
           import_stock_from_excel() ;
         }
        else import_stock_from_excel_step_3($desc_file) ;

      }
      else
      { echo '<div class=alert>Не удалось загрузить файл. Попробуйте еще раз.</div>' ;
        import_stock_from_excel() ;
      }
    }
  }

  function import_stock_from_excel_step_3($desc_file)
  { //damp_array($_POST) ;
     $debug=$_POST['test_mode'] ;
     $id_fname=$_POST['col1_fname'] ;
     $stock_fname=$_POST['col2_fname'] ;
     $arr_pkeys=array() ;
     ob_start() ;
     $list_goods=execSQL('select '.$id_fname.',pkey,'.$stock_fname.' as stock from '.$_SESSION['goods_system']->table_name.' where clss in (2,200)') ;
     if (sizeof($_SESSION['VL_arr'])) foreach($_SESSION['VL_arr'] as $id=>$rec) $list_val[$rec['YA']]=$id ;

     set_include_path(_DIR_EXT.'/PHPExcel/');
     if (!file_exists(_DIR_EXT.'/PHPExcel/')) {echo 'Расширение PHPExcel не установлено '; return ; } ;

     include_once 'PHPExcel/IOFactory.php';
     $cnt=0 ;
     $objPHPExcel = PHPExcel_IOFactory::load($desc_file);
     //$objPHPExcel->setInputEncoding('CP1251');
     $objPHPExcel->setActiveSheetIndex(0);
     // начинаем заполняем с корневого раздела
     $cnt_update=0 ;
      // читаем активную страницу
     $aSheet = $objPHPExcel->getActiveSheet();
     if (is_object($aSheet))
       {   $rowIterator = $objPHPExcel->getActiveSheet()->getRowIterator();
           foreach($rowIterator as $row)
           { $cellIterator = $row->getCellIterator();
             $cellIterator->setIterateOnlyExistingCells(false); // Loop all cells, even if it is not set
             $rec_id='' ; $stock='' ; $new_val='' ;  $cnt++ ;
             foreach ($cellIterator as $cell)
               { $value=$cell->getCalculatedValue();
                 //$value=iconv('windows-1251','UTF-8',$value) ;
                 if ($value) switch($cell->getColumn())
                 { case 'A':  $rec_id = $value; break ; // актикул, имя или код товара
                   case 'B':  $stock=str_replace(array(' ',','),array('','.'),$value) ; break ;// склад
                 }
               }
              echo $cnt.': <strong>'.$rec_id.'</strong>' ;
              if (isset($list_goods[$rec_id]))
              { $db_rec=$list_goods[$rec_id] ;
                echo ': '.$db_rec['stock'].'  => <span class=green>'.$stock.'</span>' ;
                if (!$debug) update_rec_in_table($_SESSION['goods_system']->table_name,array($stock_fname=>$stock),'pkey='.$db_rec['pkey']) ;
                $arr_pkeys[]=$db_rec['pkey'] ;
                $cnt_update++ ;
              } else echo ' <span class=red>???</span>' ;
              echo '<br>' ;
              $recs[]=array($rec_id,$stock,$new_val) ;
           }
           echo 'Обновлено:<strong>'.$cnt_update.'</strong> товаров<br>' ;
           if ($_POST['check_goods'])
           {  ?><h1>Контрольный запрос к базе на обновленный товар</h1><?
              $goods=execSQL('select pkey as id,obj_name,'.$stock_fname.' as stock from '.$_SESSION['goods_system']->table_name.' where pkey in ('.implode(',',$arr_pkeys).')') ;
              print_2x_arr($goods) ;

           }
           if ($_POST['show_excel_table']) { ?><h1>Импортированная таблица из файла</h1><? print_2x_arr($recs) ; }

       }
    $text=ob_get_flush()  ;
    $text.='<link rel="stylesheet" type="text/css" href="'._PATH_TO_ENGINE.'/admin/style.css">' ;
    $log_name='import_'.date('Y_m_d_H_i',time()).'.html' ;
    if (!$debug)  file_put_contents(_DIR_TO_ROOT.'/temp/'.$log_name,$text) ;
    if (!$debug) _event_reg('Импорт склада из файла Excel','Загружен файл <strong>'.$_FILES['import_file']['name'].'</strong>, '.$cnt.' строк<br>Обновлено:<strong>'.$cnt_update.'</strong> товаров<br><a href="/temp/'.$log_name.'" target=_BLANK>Лог импорта</a>') ;
    //if (!$debug) _event_reg('Импорт склада из файла Excel','Обновлено:<strong>'.$cnt_update.'</strong> товаров') ;
  }

?>