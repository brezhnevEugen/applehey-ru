<?
function update_move_shiping_to_service()
{ ?><h1>Оформляем доставку в заказах как услугу</h1>
    <p>Описание доставки будет перенесено из таблицы заказа в таблицу товаров заказа как услуга.</p>
    <p>Перед выполнением операции проверьте что списках сайт создан список "IL_type_shiping" с вариантами доставки.</p>
    <p>Если списка нет, его необходимо создать путем инсталяции модуля "заказы" в режиме создания списоков и проверив соответствие добавленных пунктов успользуемым на сайте вариантам доставки</p>
    <p>После данной операции можно удалить из таблицы поля "type_dost","dostavka","price_dost"</p><br>
    <? if (!sizeof($_SESSION['IL_type_shiping'])) {?><div class=alert>Список IL_type_shiping не существует! Операция прервана.</div><? return ; }?>
    <p><input type="checkbox" name=test_mode checked value=1> Проверка (без добавления, случайные 100 заказов)</p><br>
    <button cmd="update_move_shiping_to_service_apply">Выполнить</button>
  <?
}

function update_move_shiping_to_service_apply()
{ global $TM_orders,$TM_orders_goods ;
  set_time_limit(0) ;
  ?><h1>Оформляем доставку в заказах как услугу</h1><?
  if ($_POST['test_mode']) echo '<div class=alert>Тестовый режим</div>' ;
  $order=($_POST['test_mode'])? 'order by rand()':'order by t1.pkey' ;
  $limit=($_POST['test_mode'])? 'limit 100':'' ;
  //$order_info=execSQL('select t1.pkey as id,t1.type_dost,t1.dostavka,t1.price_dost,t2.obj_name,t2.price from '.$TM_orders.' t1 left join '.$TM_orders_goods.' t2 on t2.parent=t1.pkey and reffer="shiping" where t1.clss=82 and (type_dost>0 or dostavka!="" or not dostavka is null) and (t2.obj_name="" or t2.obj_name is null) '.$order.' '.$limit) ;
  $order_info=execSQL('select t1.pkey as id,t1.dostavka,t1.price_dost,t2.obj_name,t2.price from '.$TM_orders.' t1 left join '.$TM_orders_goods.' t2 on t2.parent=t1.pkey and reffer="shiping" where t1.clss=82 and (dostavka!="" or not dostavka is null) and (t2.obj_name="" or t2.obj_name is null) '.$order.' '.$limit) ;
  if ($_POST['test_mode']) print_2x_arr($order_info) ;
  echo 'Будет добавлено <strong>'.sizeof($order_info).'</strong> услуг доставки<br>' ;
  if (sizeof($order_info)) foreach($order_info as $rec)
  { $name=($rec['dostavka'])? $rec['dostavka']:$_SESSION['IL_type_shiping'][$rec['type_dost']]['obj_name'] ;
    if (!$_POST['test_mode']) adding_rec_to_table($TM_orders_goods,array('parent'=>$rec['id'],'clss'=>120,'indx'=>10,'obj_name'=>$name,'reffer'=>'shiping','price'=>$rec['price_dost'],'cnt'=>1,'temp'=>1)) ;
    echo 'Добавлена услуга доставки <strong>"'.$name.'"</strong> ('.$rec['price_dost'].') в заказ #'.$rec['id'].'<br>' ;
  }
}

?>