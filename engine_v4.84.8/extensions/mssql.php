<?
class mssql
{  public $Link ;

  function mssql($db_host,$db_name,$db_username,$db_password,$options=array())
  {
     $this->connect_database($db_host,$db_name,$db_username,$db_password,$options) ;
  }

  function connect_database($db_host,$db_name,$db_username,$db_password,$options=array())
   {    global $debug_db ;
        $database = mssql_connect($db_host, $db_username, $db_password);
        $text_debug=($debug_db)? 'Параметры подключения:<br>host='.$db_host.'<br>db_name='.$db_name.'<br>user_name='.$db_username.'<br>':'' ;
        if (!$database) die("<hr><center><strong>Невозможно подключиться к серверу MySQL<hr></strong></center>".$text_debug);
        if (!($this->Link = mssql_select_db($db_name))) die("<hr><div style='text-align:center;font-weight:bold;'>Не найдена БД '$db_name'</div><hr>");
        mssql_query("SET IMPLICIT_TRANSACTIONS OFF");
   }

  function close()
   {
    // mssql_close($this->Link) ; // закрывает коннект с базой
   }


  function execSQL($sql,$options=array()) // запрос к БД
    {  $arr=array() ;
       $sql=iconv('utf-8','windows-1251',$sql) ;

       $result = mssql_query($sql) ;
       while (($new_res=mssql_fetch_assoc($result))!==false)    if ($options['indx_field']) $arr[$new_res[$options['indx_field']]]=$new_res ;
                                                                else $arr[]=$new_res ;

       if (sizeof($arr)) foreach($arr as $i=>$rec)
           if (sizeof($rec)) foreach($rec as $j=>$value)
           $arr[$i][$j]=iconv('windows-1251','utf-8',$value) ;

       return $arr;
    }

function execSQL_van($sql,$options=array(),$no_indx=0,$ignore_error=0) // запрос к БД
  { global $debug_db ;
    $sql=iconv('utf-8','windows-1251',$sql) ;
    $debug=$options['debug'] ;
    $sql=iconv('utf-8','windows-1251',$sql) ;
    if ($debug or $debug_db)  { echo '<strong>Запрос</strong>: '.$sql.': <br>' ;  }

    if (!$result = mssql_query($sql)) $this->SQL_error_message($sql) ;

	if ($debug or $debug_db)  { echo '<strong>Получено '.mssql_num_rows($result).' записей : </strong>' ;  }

	$arr=mssql_fetch_assoc($result) ;

    if (sizeof($arr)) foreach($arr as $i=>$value) $arr[$i]=iconv('windows-1251','utf-8',$value) ;
    return $arr;
  }

  // возвращает один столбец
  function execSQL_row($sql,$options=array())
  { global $debug_db ;
    $sql=iconv('utf-8','windows-1251',$sql) ;
    $arr=array() ;
    $debug=$options['debug'] ;
	if (!$result = mssql_query($sql)) $this->SQL_error_message($sql) ;

    if ($debug or $debug_db){echo '<strong>Получено '.mssql_num_rows($result).' записей : </strong>' ;}

    if ($result>"") for ($i=0;$i<mssql_num_rows($result);$i++) $arr[mssql_result($result,$i,0)]=mssql_result($result,$i,1);

    if (sizeof($arr)) foreach($arr as $i=>$value) $arr[$i]=iconv('windows-1251','utf-8',$value) ;
    return $arr;
  }

 function execSQL_Line($sql,$options=array())
  { global $debug_db ;
    $sql=iconv('utf-8','windows-1251',$sql) ;
    $arr=array() ;
    $debug=0 ;
    if (!is_array($options)) { $debug=$options ; $options=array() ; }
    if ($options['debug'])              $debug=$options['debug'] ;

    if (!$result = mssql_query($sql)) SQL_error_message($sql) ;

    if ($debug or $debug_db){echo '<strong>Получено '.mssql_num_rows($result).' записей : </strong>' ;}

    if ($result>"") for ($i=0;$i<mssql_num_rows($result);$i++) $arr[]=mssql_result($result,$i,0);

    if (sizeof($arr)) foreach($arr as $i=>$value) $arr[$i]=iconv('windows-1251','utf-8',$value) ;

	return $arr;
  }

    // вывод сообщения о ошибку при SQL-запроске
      function SQL_error_message($sql,$text='Ошибка при отработке SQL запроса:',$nostop=0)
      { global $info_db_error ; //$info_db_error=0 ;
      	if ($info_db_error)
      	{ echo "<div align=left><strong>".$text."</strong><br><span style='color:red;'>'".mysql_error()."</span><br><span style='color:blue;'>".$sql."'</span><br></div>" ;
          $arr=debug_backtrace() ;
          ?><script type="text/javascript">
    	      function show_debug_backtrace(div_id)
    	      { var div=document.getElementById('debug_backtrace_'+div_id) ;
    	        if (div==undefined) return ;
    	        if (div.style.display=='block') div.style.display='none' ; else div.style.display='block' ;
    	      }
            </script>
            <a href="" class="red bold" onclick="show_debug_backtrace(1);return(false);">Показать дамп отладки</a><br><?
          ?><div id=debug_backtrace_1 style="display:none;"><?damp_array($arr,2) ;?></div><?
          //echo debug_print_backtrace() ;
          //if (!$nostop) exit ;
        } else trigger_error($text.' - '.mssql_error().$sql) ;
      }


// функция для выполнения запросов update,delete и т.д.
 function execSQL_update($sql,$options=array())
  { $sql=iconv('utf-8','windows-1251',$sql) ;
    if (!mssql_query($sql)) $this->SQL_error_message($sql) ;
  }

  // для запросов, возвращающих только одно значение
  function execSQL_value($sql,$options=array()) // запрос к БД
  { $res=$this->execSQL_van($sql,$options) ;
    if (sizeof($res))
    { reset($res) ;
      list($indx,$value)=each($res) ;
      return($value) ;
    }
    return(0) ;
  }

  function adding_rec_to_table($table_name,$finfo,$options=array())
     { $_fields=array() ; $_values=array() ; $res=0 ;
       foreach ($finfo as $fname=>$fvalue) { $_fields[]=$fname ; $_values[]="'".addcslashes($fvalue,"'")."'" ; }
       $str_fields=(sizeof($_fields))? implode(',',$_fields):'' ;
       $str_values=(sizeof($_values))? implode(',',$_values):'' ;
       $mode=($options['use_replace'])? 'replace':'insert' ;
       $sql=$mode." into $table_name (".$str_fields.") values (".$str_values.");" ;
       $sql=iconv('utf-8','windows-1251',$sql) ;
       if (!mssql_query($sql)) $this->SQL_error_message($sql,'Не удалось добавить запись в таблицу : ') ;
        else if (!$options['no_return_id']) $res=execSQL_value("SELECT LAST_INSERT_ID() as pkey") ;
       return($res) ;
     }

function update_rec_in_table($table_name,$finfo,$usl_update,$options=array())
 { $_fields=array() ;
   foreach ($finfo as $fname=>$fvalue) $_fields[]=$fname."='".addcslashes($fvalue,"'")."'" ;
   $sql="update $table_name set ".implode(',',$_fields)." where ".$usl_update;
   $sql=iconv('utf-8','windows-1251',$sql) ;
   if ($options['debug']) echo $sql.'<br>' ;
   if (!mssql_query($sql)) echo 'Не удалось обновить запись в таблице' ;
 }

}
?>