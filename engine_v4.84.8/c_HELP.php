<?php
// исходный файл для размещения в /class/c_HTML
// в движке не используется, является промежуточным между c_page и c_core_HTML
// после перемещения в /class/ убрать переименовать класс в c_HTML
include_once (_DIR_TO_ENGINE.'/c_core_HTML.php') ;
class c_HELP extends c_core_HTML
{ public $allow_GET_params=array() ;  // $allow_GET_params=array('dd'=>'int') ;
  public $checked_GET_params=0 ;      // разрешение проверки корректности параметров
  public $false_GET_params_action=404 ; // действие в случае некоректного параметра или некоректного url
  public $only_dir_in_url=0 ; // эта страница должны открываться только по url /catalog/sales/, /catalog/sales/)%7Bthis.close()%7D%7D%5D выдаст 404 ошибку или 301 редирект
  public $prev_HTML; //  код в модулях, выводимый в поток при подклбчении подулей (код вне функций). Выводится в зависимости от типа текущей страницы (HTML, XML)
  public $noindex=0; //  запрет индексации страницы роботами
  public $HEAD=array() ;
  public $DOCTYPE='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">' ;
  public $HTML_TAG=array('<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">','</html>') ;

  function set_head_tags()
 { //------------------------------------------------------
   // стандартный JQuery
    $this->HEAD['js'][]='http://source.12-24.ru/AddOns/jquery-1.8.3.min.js' ;
    $this->HEAD['js'][]='http://source.12-24.ru/AddOns/jquery.form.js' ;

   // mootools
    $this->HEAD['js'][]='http://source.12-24.ru/AddOns/mootools-core-1.3.1.js' ;
    $this->HEAD['js'][]='http://source.12-24.ru/AddOns/mootools-more-1.3.1.1.js' ;

   // библитека mBox
    $this->HEAD['js'][]='http://source.12-24.ru/AddOns/StephanWagner-mBox-0.2.6/Source/mBox.Core.js' ;
    $this->HEAD['js'][]='http://source.12-24.ru/AddOns/StephanWagner-mBox-0.2.6/Source/mBox.Modal.js' ;
    $this->HEAD['js'][]='http://source.12-24.ru/AddOns/StephanWagner-mBox-0.2.6/Source/mBox.Modal.Confirm.js' ;
    $this->HEAD['js'][]='http://source.12-24.ru/AddOns/StephanWagner-mBox-0.2.6/Source/mBox.Notice.js' ;
    $this->HEAD['js'][]='http://source.12-24.ru/AddOns/StephanWagner-mBox-0.2.6/Source/mBox.Tooltip.js' ;
    $this->HEAD['css'][]='http://source.12-24.ru/AddOns/StephanWagner-mBox-0.2.6/Source/assets/mBoxCore.css' ;
    $this->HEAD['css'][]='http://source.12-24.ru/AddOns/StephanWagner-mBox-0.2.6/Source/assets/mBoxModal.css' ;
    $this->HEAD['css'][]='http://source.12-24.ru/AddOns/StephanWagner-mBox-0.2.6/Source/assets/mBoxNotice.css' ;
    $this->HEAD['css'][]='http://source.12-24.ru/AddOns/StephanWagner-mBox-0.2.6/Source/assets/mBoxTooltip.css' ;
    $this->HEAD['css'][]='http://source.12-24.ru/AddOns/StephanWagner-mBox-0.2.6/Source/assets/themes/mBoxTooltip-Black.css' ;
    $this->HEAD['css'][]='http://source.12-24.ru/AddOns/StephanWagner-mBox-0.2.6/Source/assets/themes/mBoxTooltip-BlackGradient.css' ;

   // библитека mForm
    $this->HEAD['js'][]='http://source.12-24.ru/AddOns/StephanWagner-mForm-0.2.6/Source/mForm.Core.js' ;
    $this->HEAD['js'][]='http://source.12-24.ru/AddOns/StephanWagner-mForm-0.2.6/Source/mForm.Element.js' ;
    //$this->HEAD['js'][]='http://source.12-24.ru/AddOns/StephanWagner-mForm-0.2.6/Source/mForm.Element.Select.js' ;
    $this->HEAD['js'][]='http://source.12-24.ru/AddOns/StephanWagner-mForm-0.2.6/Source/mForm.Submit.js' ;
    $this->HEAD['css'][]='http://source.12-24.ru/AddOns/StephanWagner-mForm-0.2.6/Source/assets/mForm.css' ;
    //$this->HEAD['css'][]='http://source.12-24.ru/AddOns/StephanWagner-mForm-0.2.6/Source/assets/mFormElement-Select.css' ;

   // стандартное подключение highslide
    $this->HEAD['js'][]='http://source.12-24.ru/AddOns/highslide-4.1.13/highslide/highslide-with-gallery.min.js' ;
    $this->HEAD['css'][]='http://source.12-24.ru/AddOns/highslide-4.1.13/highslide/highslide.css' ;

   //расширение - модальные окна
   $this->HEAD['js'][]='/class/ext/mBox/modal_window.js' ;

    $this->HEAD['css'][]='http://source.12-24.ru/style_help.css' ;
 }

  // отдаем HTML контент
  // пока оставляем как есть, но в следующей версии необходимо убрать обработку опций  content_type,no_html_doc_type,no_html_head
  // теперь вывод XML страниц только через свой скрипт c_page_XML
  function HTML(&$options=array())
  {
    $this->set_head_tags() ;

    echo $this->DOCTYPE ; // <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
    echo $this->HTML_TAG[0] ; // <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

    $this->site_top_head($options) ;  // заголовок страницы

    $method_body=($options['use_body'])? ($options['use_body']):'body' ; // необходмый шаблон макета страницы может быть передан через $options. Используется для страниц, не имеющий собственного скрипта в /class/

    $this->$method_body($options) ;

    echo $this->HTML_TAG[1] ; // </html>
  }

  function site_top_head($options=array())
  { ?><head>
       <title><? echo $this->title;?></title>
        <META name="keywords" content="<?echo $this->keywords;?>">
        <META name="description" content="<?echo $this->description?>">
        <META http-equiv="Content-Type" content="text/html; charset=utf-8">
        <META NAME="Document-state" CONTENT="Dynamic">
        <meta name="robots" content="noindex, nofollow, noarchive">
        <? if (sizeof($this->HEAD['css'])) foreach($this->HEAD['css'] as $file_name){?><link rel="stylesheet" type="text/css" href="<?echo $file_name?>" charset="utf-8"><? echo "\n\t" ; }
        if (sizeof($this->HEAD['js']))  foreach($this->HEAD['js']  as $file_name){?><SCRIPT type="text/javascript" src="<?echo $file_name?>" ></SCRIPT><? echo "\n\t" ;}
     ?>
      </head>
      <?
  }

  // стандартный вывод тела страницы
  function body()
  { ?><body>
          <div id=wrapper>
		     <div id=block_top> <? $this->block_top() ; ?></div>
		     <div id=block_main><? $this->block_main() ; ?></div>
	         <div id=block_bottom><? $this->block_bottom() ; ?></div>
	      </div>
      </body><?
   }

 function block_top() {}
 function block_bottom() {}

 function block_main()
  { $this->page_title() ; // стандартный заголовок страницы

  }

  // базовый заголовк страницы - вверху путь, снизу заголовок - для шаблонов
  function page_title($title='',$path=array(),$options=array())
  { if (is_array($title)) { $options=$title ; $title='' ; $path=array() ; } // на случай, если page_title вызвана с путем в первом параметре
    //$this->panel_path($path,$options);  // заголовк страницы по умолчанию
    $this->panel_title($title,$options);  // заголовк страницы по умолчанию
  }

  // показ заголовка h1 текущей страницы
  function panel_title($title='')
  { $text_h1=($title)? $title:$this->h1 ;
    ?><h1><? echo $text_h1 ?></h1><?
  }

}

?>