<?php
// запрещенные к использованию имена полей:
// _dir_to_file_

$__functions['init'][]		='_admin_vars' ;
$__functions['boot_admin'][]='_admin_boot' ;
$__functions['install'][]	='_admin_install_modul' ;



 function _admin_vars() //
 {  // стандартные объекты сайта -----------------------------------------------------------------------------------------------------------------


	//-----------------------------------------------------------------------------------------------------------------------------
	// описание стандартных классов движка
	//-----------------------------------------------------------------------------------------------------------------------------

	// шаблоны полей
    $_SESSION['pattern_clss_fields']=array() ;
	$_SESSION['pattern_clss_fields']['Позиция']	    =array('title'=>'Позиция','class'=>'small') ;
    $_SESSION['pattern_clss_fields']['Наименование']	=array('title'=>'Наименование','class'=>'small','td_class'=>'left') ;

    $_SESSION['ARR_templates_type']=array(1=>'show_list_items',2=>'show_list_section',4=>'show_item',3=>'template') ;
    $_SESSION['ARR_templates_obj']=array(1=>'Текущий объект',2=>'Слинкованный объект',3=>'id объекта через второй параметр') ;

	// описания классов
    include_once('i_descr_clss.php') ;   // описания всех классов (без модулей)
    declare_class() ;

	//----------------------------------------------------------------------------------------------------------------------
	// описание пунктов меню админа сайта
	//-----------------------------------------------------------------------------------------------------------------------

	set_time_point('Определенны <strong>total</strong> переменные')   ;
    $_SESSION['menu_admin']=array() ;
   $_SESSION['menu_admin']['Контент'][]=array('name'=>'Главная страница','href'=>'editor_landing.php','icon'=>'menu/newspaper.jpg') ;
	$_SESSION['menu_admin']['Контент'][]=array('name'=>'Страницы сайта','href'=>'editor_pages.php','icon'=>'menu/book.jpg') ;
	$_SESSION['menu_admin']['Контент'][]=array('name'=>'Редактор мета-тегов','href'=>'editor_metatags.php','icon_ext'=>_PATH_TO_ENGINE.'/admin/images/menu/seo-tag-48.png') ;
    $_SESSION['menu_admin']['Контент'][]=array('name'=>'Блоки информации','href'=>'editor_info.php','icon'=>_PATH_TO_ADMIN_IMG.'/menu/banner-icon-161.png') ;
    $_SESSION['menu_admin']['Контент']['editor_banner']=array('name'=>'Банеры','href'=>'editor_banners.php','icon'=>_PATH_TO_ADMIN_IMG.'/menu/banner-icon.png') ;
	//$_SESSION['menu_admin']['Контент'][]=array('name'=>'SEO тексты','href'=>'editor_meta_tags.php') ;
    $_SESSION['menu_admin']['Контент']['editor_list']=array('name'=>'Редактор списков','href'=>'editor_list.php','icon'=>'menu/blacklist.jpg') ;
    $_SESSION['menu_admin']['Контент']['file_manager']=array('name'=>'Файловый проводник','href'=>'#','icon'=>'menu/files.jpg','id'=>'file_explorer') ;
	//$_SESSION['menu_admin']['Контент'][]=array('name'=>'Анализатор страниц','href'=>'page_analizator.php','icon'=>'menu/page_analizator.gif','onclick'=>'onclick="open_window(\''._PATH_TO_ADMIN.'/page_analizator.php\',800,600,\'page_analizator\',\'yes\'); return false;"') ;
    $_SESSION['menu_admin']['Контент'][]=array('name'=>'Навигация сайта','href'=>'editor_site_menu.php','icon'=>'menu/site_menu.jpg') ;

    $_SESSION['menu_admin']['Управление'][]=array('name'=>'Шаблоны писем','href'=>'editor_mail_templates.php','icon'=>'menu/flood_mail.jpg') ;
    $_SESSION['menu_admin']['Управление'][]=array('name'=>'Настройки сайта','href'=>'editor_site_setting.php','icon'=>'menu/gear.jpg') ;
	//$_SESSION['menu_admin']['Управление'][]=array('name'=>'Служебные операции','href'=>'site_operation.php','icon'=>'menu/windows_config.jpg') ;

	//$_SESSION['menu_admin']['Управление'][]=array('name'=>'Мусорная корзина','href'=>'editor_trash.php','icon'=>'menu/trash.jpg') ;
	//$_SESSION['menu_admin']['Управление'][]=array('name'=>'Информация о движке','href'=>'sys_info.php','icon'=>'menu/info.jpg') ;
	if (_SUPPORT_EMAIL) $_SESSION['menu_admin']['Управление'][]=array('name'=>'Техподдержка','href'=>'engine_support.php','icon'=>'menu/privacy_mail.jpg') ;

	$_SESSION['menu_admin']['Управление'][]=array('name'=>'Персонал','href'=>'editor_personal.php','icon'=>'menu/administrator.jpg') ;
	$_SESSION['menu_admin']['Управление'][]=array('name'=>'Быстрое редактирование','href'=>'fastedit.php?to=/&mode=enabled','icon'=>'menu/hand_write.jpg') ;
	$_SESSION['menu_admin']['Управление'][]=array('name'=>'Перезагрузка','href'=>'reboot.php','icon'=>'menu/reload.jpg') ;

	$_SESSION['menu_admin']['Журналы'][]=array('name'=>'Журнал страниц','href'=>'log_pages.php','icon'=>'menu/log-text.png') ;
	$_SESSION['menu_admin']['Журналы'][]=array('name'=>'Что ищут','href'=>'log_searches.php','icon'=>'menu/log-text.png') ;
	$_SESSION['menu_admin']['Журналы']['log_banner']=array('name'=>'Переходы по банерам','href'=>'log_ban_stat.php','icon'=>'menu/log-text.png') ;
	$_SESSION['menu_admin']['Журналы']['log_error']=array('name'=>'Журнал ошибок','href'=>'log_errors.php','icon'=>'menu/log-text.png') ;
	$_SESSION['menu_admin']['Журналы'][]=array('name'=>'Журнал событий','href'=>'log_events.php','icon'=>'menu/log-text.png') ;
	//$_SESSION['menu_admin']['Журналы'][]=array('name'=>'События в админе','href'=>'log_admin_events.php','icon'=>'menu/log-text.png') ;

}

 function _admin_boot($options=array())
 {  // создаем систему аккаунтов на сайте

    if (!is_object($_SESSION['admin_account_system'])) $_SESSION['admin_account_system']=new c_admin_account_system() ;
    if (!is_object($_SESSION['member'])) $_SESSION['member']=new c_member() ;

    $_SESSION['events_system']=new c_events_system() ; // журнал событий
    $_SESSION['mail_system']=new c_mail_system() ; // создаем систему почты
    $_SESSION['pages_system']= new c_pages_system() ; // создаем систему страниц сайта
    $_SESSION['info_system']= new c_info_system() ; // создаем систему информации сайта
 }

 function _admin_install_modul($DOT_root)
 {  echo '<h2>Инсталируем модуль <strong>Базовая админка</strong></h2>' ;

    // создаем скрипты для пунктов меню
    $file_items=array() ;
    include('i_descr_menu_items.php') ; // описания пунктов меню
    create_menu_item($file_items) ;

    SETUP_get_file('/admin/css/style.css','/admin/style.css');
    //SETUP_get_file('/admin/css/admin_style.css','/admin/admin_style.css');

    SETUP_get_img_from_arhive('/admin/img.zip',_PATH_TO_ADMIN.'/') ;
    SETUP_get_img('/setup/admin_img_menu/edit_obj_table.gif',_PATH_TO_ADMIN.'/img/menu/edit_obj_table.gif') ;
    SETUP_get_img('/setup/admin_img_menu/php_page_editor.png',_PATH_TO_ADMIN.'/img/menu/php_page_editor.png') ;
    SETUP_get_img('/setup/admin_img_menu/edit_operation.gif',_PATH_TO_ADMIN.'/img/menu/edit_operation.gif') ;


    $id_sect=SETUP_add_section_to_site_setting('Мета-теги') ;
    SETUP_add_rec_to_site_setting($id_sect,'Заголовок страницы по умолчанию',_MAIN_DOMAIN,'LS_def_page_title') ;
    SETUP_add_rec_to_site_setting($id_sect,'Ключевые слова по умолчанию','Ключевые слова сайта '._MAIN_DOMAIN,'LS_def_page_keywords') ;
    SETUP_add_rec_to_site_setting($id_sect,'Описание страницы по умолчанию','описание сайта '._MAIN_DOMAIN,'LS_def_page_descr') ;
    SETUP_add_rec_to_site_setting($id_sect,'Приставка к заголовку','','LS_pre_title') ;
    SETUP_add_rec_to_site_setting($id_sect,'Суффикс к заголовку','','LS_after_title') ;

    $id_sect=SETUP_add_section_to_site_setting('Настройка почтовых уведомлений') ;
    SETUP_add_rec_to_site_setting($id_sect,'Название сайта',_MAIN_DOMAIN,'LS_public_site_name') ;
    SETUP_add_rec_to_site_setting($id_sect,'E-mail администратора сайта','info@'._BASE_DOMAIN,'LS_email_site') ;
    SETUP_add_rec_to_site_setting($id_sect,'Адрес, от которого будут уходить письма с сайта','noreply@'._BASE_DOMAIN,'LS_email_site_noreply') ;
    //SETUP_add_rec_to_site_setting($id_sect,'e-mail, на который необходимо дублировать сообщения о событиях на сайте','debug@12-24.ru','LS_email_site_to_info') ;
    SETUP_add_rec_to_site_setting($id_sect,'Подпись по умолчанию в шаблонах писем','С уважением, Администрация сайта '._MAIN_DOMAIN.', <a href="mailto:info@'._BASE_DOMAIN.'">info@'._BASE_DOMAIN.'</a> <a href="http://'._MAIN_DOMAIN.'">http://'._MAIN_DOMAIN.'</a>','LS_mail_bottom_podpis') ;

    $id_sect=SETUP_add_section_to_site_setting('Статистика') ;
    SETUP_add_rec_to_site_setting($id_sect,'Время последнего обновления статистики',1,'LS_stats_last_update') ;
    SETUP_add_rec_to_site_setting($id_sect,'Email для доставки информации о входах на сайт','debug@12-24.ru','LS_stats_logons_email') ;
    SETUP_add_rec_to_site_setting($id_sect,'Также высылать информацию о нагрузке сайта','debug@12-24.ru','LS_stats_debug_to_email') ;

    $id_sect=SETUP_add_section_to_site_setting('Журналы') ;
    SETUP_add_rec_to_site_setting($id_sect,'Срок хранения записей в журнале событий, дней',0,'LS_log_events_day_live') ;

 }

?>