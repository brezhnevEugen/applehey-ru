<?php

$__functions['init'][]='_site_vars' ;
$__functions['boot_site'][]	='_site_boot' ;

include_once(_DIR_TO_ENGINE.'/c_site_objects.php') ;

function _site_vars() //
{   // загружаем общие настройки для сайта и адмика
    $_SESSION['arr_file_js']=array() ;     // устаревнее, перенесено в c_page
    $_SESSION['arr_file_css']=array() ;    // устаревнее, перенесено в c_page
    $_SESSION['meta_info']=array() ;       // устаревнее, перенесено в c_page
    $_SESSION['check_code_img']=array() ;
    $_SESSION['global_replace_worlds']=array() ;
    $_SESSION['img_pattern']=array() ;
    $_SESSION['favicon']='' ;               // устаревнее, перенесено в c_page
    $_SESSION['init_options']=array() ;
    $_SESSION['frame_viewer_menu']=array() ;
    $_SESSION['ext_panel_func']=array() ;
    $_SESSION['_MOUNT_SUBDOMAIN']=array() ;
    $_SESSION['_MOUNT_POINT']=array() ;
    // настройки, определяющие HEADER страниц сайта ------------------------------------------------------------------------------------------------

    //$_SESSION['meta_info'][]=array('name'=>"verify-v1",'content'=>"Z8SS3+QRnDi1BOtPJRQyxH92mkOP4FG+39hRCwwIdp8=") ;
	//$_SESSION['meta_info'][]=array('name'=>"yandex-verification",'content'=>"459cfcd781a918c0") ;

    // настройки, определяющее поведение и вид страниц сайта ---------------------------------------------------------------------------------------
	//$_SESSION['check_code_img'][0]=array('digit_count'=>6,'width'=>250,'height'=>70,'back'=>'FFCB00','font_name'=>'images/BirchCTT Normal.Ttf','font_size'=>'40','x'=>20,'y'=>50,'step_x'=>35,'dev_x'=>0,'dev_y'=>5,'dev_angle'=>35) ;

 	// настройка языков сайта --------------------------------------------------------------------------------------------------------------------------------
    // в path.php
    // $lang_arr[]=array('name'=>'русский','name_eng'=>'russian','suff'=>'','dir'=>'') ;
    // $lang_arr['uk']=array('name'=>'украинский','name_eng'=>'ua','suff'=>'_uk','dir'=>'ua') ;
    //

    // настройки вывода страниц сайта для всех систем по умолчанию
    // ['options']['page_mode']
    // два режима показа страниц
    // 0: показывается только номера страниц, кнопки вперед и назад только при превышении числа показываемых страниц заданного лимита. Кнопки перелистывают группу страниц
    // 1: показываются номера страниц и кнопки вперед - назад. Кнопки перелистывают на одну страница. Все номера страниц.
    // 2: показываются номера страниц и кнопки вперед - назад. Кнопки перелистывают на одну страница. Номера страниц - в заданом лимите.

    $_SESSION['init_options']['default']['pages']['options']=array('pages_num_format'=>"%'1d",'page_max_number_count'=>10,'page_number_rasdel'=>'&nbsp;&nbsp;|&nbsp;&nbsp;','page_image_prev'=>'/images/arr_left_dark.gif','page_image_next'=>'/images/arr_right_dark.gif') ;
    $_SESSION['init_options']['default']['pages']['options']['page_mode']=0 ;
    $_SESSION['init_options']['default']['pages']['size']=array('10'=>'По 10 позиций','20'=>'По 20 позиций','30'=>'По 30 позиций','50'=>'По 50 позиций','all'=>'Все') ;
    $_SESSION['init_options']['default']['pages']['sort_type']=array() ;
    $_SESSION['init_options']['default']['pages']['sort_type'][1]=array('name'=>'По порядку',		'order_by'=>'indx',		'diapazon_title'=>'Позиции:') ;
    $_SESSION['init_options']['default']['pages']['sort_type'][3]=array('name'=>'По наименованию',	'order_by'=>'obj_name',	'diapazon_title'=>'Наименования:') ;
    $_SESSION['init_options']['default']['pages']['sort_type'][4]=array('name'=>'По дате создания',	'order_by'=>'c_data desc','diapazon_title'=>'Дата:') ;
    $_SESSION['init_options']['default']['pages']['panel_top']['func_name']='panel_list_items_top_navigat' ;
    $_SESSION['init_options']['default']['pages']['panel_bottom']['func_name']='panel_list_items_bottom_navigat' ;

    // настройка редактора
    //$_SESSION['CK_editor_toolbar']='' ;
   /*
    $_SESSION['CK_editor_toolbar']="toolbar :
     [  ['Source'],
        ['Cut','Copy','Paste','PasteText','PasteFromWord'],
        ['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
        ['Format,FontSize','TextColor','BGColor'],
        ['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],
        ['NumberedList','BulletedList','-','Outdent','Indent'],
        ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
        ['Link','Unlink','Anchor','Image','Flash','Table']
     ]" ;   */

}

// функция вызывется только при загрузке движка
function _site_boot($options)
{
 	// создаем объект текущий пользователь - только один раз при первом открытии сайта
    // 20.11.12 - объект member используется только для хранения информации о текущем пользователе, поэтому его класс недоступен для перекрытия
    if (!is_object($_SESSION['member'])) $_SESSION['member']=new c_member() ;
    $_SESSION['events_system']=new c_events_system() ; // журнал событий
    $_SESSION['ban_system']= new c_ban_system() ; // создаем систему баннеров
    $_SESSION['pages_system']= new c_pages_system() ; // создаем систему страниц сайта
    $_SESSION['search_stat']= new c_search_register() ; // создаем объект для регистрации поисковых запросов
    $_SESSION['info_system']= new c_info_system() ; // создаем систему информации сайта
    $_SESSION['mail_system']=new c_mail_system() ; // создаем систему почты

    // создаем системы меню сайта
    create_system_modul_obj('menu',array('debug'=>0)) ;  // создание объекта menu_system

}


?>
