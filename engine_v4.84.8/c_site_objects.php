<?php

class c_pages_system
{
   function c_pages_system()
   { $this->table_name=TM_PAGES ;
     $this->tkey=_DOT(TM_PAGES)->pkey ;
     $this->usl_show_items='clss=26 and enabled=1' ;
   }

   function get_page_content($page_href)
   {   $real_page_path=str_replace('/'._CUR_LANG.'/','/',$page_href) ;
       $obj_info=select_db_recs($this->table_name,'href="'.$real_page_path.'" and '.$this->usl_show_items,array('no_arr'=>1)) ;
       if ($obj_info['pkey'])
       {   $this->prepare_public_info($obj_info) ;
           _CUR_PAGE()->h1=$obj_info['obj_name']  ;

           $recs=select_db_recs('obj_site_pages','enabled=1 and parent='.$obj_info['pkey'],array('order_by'=>'pkey','debug'=>0)) ;
           //damp_array($recs) ;
           //$data=array() ; $recs=array() ;
           if (sizeof($recs)) foreach($recs as $i=>$rec)
                    {  //$section_code=$this->tree[$rec['parent']]->rec['code'] ;
                       $id=($rec['code'])? $rec['code']:$rec['pkey'] ;
                       if ($rec['clss']==71)
                       { if ($rec['value']) $obj_info['_info'][$id]=$rec['value'] ;
                         else if ($rec['_image_name']) $obj_info['_info'][$id]=img_clone($rec) ;
                         $rec['_info_rec'][$id]=$rec ;
                       }
                       if ($rec['clss']==70)
                       { $obj_info['_info'][$id]=$rec['pkey'] ;
                         $rec['_info_rec'][$id]=$rec ;
                         $recs2=select_db_recs('obj_site_pages','enabled=1 and parent='.$rec['pkey'],array('order_by'=>'pkey','debug'=>0)) ;
                         $obj_info['_info_rec2'][$id]=$recs2 ;

                       }
                    }
       }
       if ($obj_info['pkey']) return($obj_info) ; else return(array()) ;
   }

   function show_page_content($obj_info)
   {   if (!is_array($obj_info)) $obj_info=$this->get_page_content($obj_info) ;
       //_macros($obj_info['value']) ;
       echo $obj_info['value'].' '.$obj_info['_fast_edit_icon'] ;
   }

   function export_pages_to_sitemap($page_xml)
   { $list_pages=execSQL('select pkey,href,r_data from '.$this->table_name.' where '.$this->usl_show_items.' order by parent,indx') ;
     if (sizeof($list_pages)) foreach($list_pages as $rec)
        { $href=(strpos($rec['href'],'http')!==false)? $rec['href']:_PATH_TO_SITE.$rec['href'] ;
          $page_xml->add_url($href,$rec['r_data']) ;
        }
   }

   function prepare_public_info(&$rec,$options=array())
   { $rec['__href']=(_USE_SUBDOMAIN)? _PATH_TO_SITE.$rec['href']:$rec['href'] ;
   //  $rec['__href']=$rec['href'] ;
     $rec['__name']=$rec['obj_name'] ;
     $rec['__data']=date("d.m.y",$rec['c_data']) ;
   }

   function prepare_public_info_for_arr(&$arr,$options=array())
   {
     if (sizeof($arr))  foreach($arr as $i=>$rec) $this->prepare_public_info($arr[$i],$options) ;
   }

}


class c_menu_system extends c_system_catalog
{ public $use_href_value=1 ;

  function	c_menu_system($create_options=array())
  { if (!$create_options['table_name']) 			$create_options['table_name']=TM_SITE_MENU ;
    if (!$create_options['patch_mode'])				$create_options['patch_mode']='as_is' ;
    if (!$create_options['url_type'])				$create_options['url_type']='href' ;
    //if (!$create_options['tree']['usl_include'])	$create_options['tree']['usl_include']='clss in (1,10,97) and enabled=1' ;
    if (!$create_options['tree']['usl_include'])	$create_options['tree']['usl_include']='enabled=1' ;
    if (!$create_options['tree']['order_by'])		$create_options['tree']['order_by']='indx' ;
    $create_options['tree']['no_fs_info']=1 ;
    //$create_options['tree']['debug']=1 ;
    //$create_options['debug']=1 ;
    //if (!$create_options['tree']['get_count_by_clss']) $create_options['tree']['get_count_by_clss']=0 ;
    parent::c_system_catalog($create_options) ;

  }

  // перенаправляем на вывод разделов дерева, т.к. в для меню в деверо загружается все элементы
  function show_list_items($rec,$func_name,$options=array()) { $this->show_list_section($rec,$func_name,$options)  ; }

  function prepare_public_info(&$rec,$options=array())
  {
    $rec['__href']=$rec['href'] ;
  }
}

function _show_menu($id,$func_name,$options=array()) { global $menu_system ; $menu_system->show_list_items($id,$func_name,$options) ; }

// класс, отвечающий за регистрацию поисковых запросов ------------------------------------------------------------------------------------------
class c_search_register
{
   public $table_name ;
   public $tkey ;

   function c_search_register($options=array())
 	{  $this->table_name=($options['table_name'])? $options['table_name']:TM_LOG_SEARCH ;
       $this->tkey=$_SESSION['pkey_by_table'][$this->table_name] ;
    }

   function register($finfo)
   {  if (!isset($finfo['c_data'])) $finfo['c_data']=time() ;
      $pkey=adding_rec_to_table($this->tkey,$finfo,array('no_def_value'=>1,'debug'=>0)) ;
	  return($pkey) ;
   }
}

// класс, отвечающий за размещение банеров на сайте -------------------------------------------------------------------------------------------
class c_ban_system
{
   public $table_name ;
   public $tkey ;
   public $banners ; // все баннеры, сгруппированые по подкаталогу
   public $last_show ; // список уже показанных банеров (для исключения повтором при случайном показе)

   function c_ban_system($options=array())
 	{  $this->table_name=($options['table_name'])? $options['table_name']:TM_BANNERS ;
       $this->stat_table_name=($options['stat_table_name'])? $options['table_name']:TM_LOG_BANNERS ;
       $this->tkey=$_SESSION['pkey_by_table'][$this->table_name] ;
       if (!$this->tkey) { echo 'Не найдена таблица '.$this->table_name.'<br>' ; return ; }
       // срочно затычка для подгрузки банеров только из включенных разделов
       $enabled_sections=execSQL_line('select pkey from '.$this->table_name.' where clss=1 and enabled=1') ;
       $this->banners=array() ;
       if (sizeof($enabled_sections))
       { $enabled_sections_ids=implode(',',$enabled_sections) ;
         $banner_recs=execSQL('select * from '.$this->table_name.' where enabled=1 and parent in ('.$enabled_sections_ids.') order by parent,indx') ;
         if (sizeof($banner_recs)) foreach($banner_recs as $id=>$rec) $this->prepare_public_info($banner_recs[$id]) ;
         $this->banners=group_by_field('parent',$banner_recs,'pkey') ;
         //$_banners=select_objs($this->tkey,'',' enabled=1 and parent in ('.$enabled_sections_ids.')',array('order_by'=>'parent,indx','group_by'=>'parent','no_key_field'=>0,'debug'=>0,'no_out_table'=>1)) ;
         //if (sizeof($this->banners)) foreach($this->banners as $section_id=>$list_banners) if (sizeof($list_banners)) foreach($list_banners as $banner_rec) $this->prepare_public_info($this->banners[$section_id][$banner_rec['pkey']]) ;
         // проверяем наличие линка на баннер
         $arr_links=get_arr_links(0,$this->tkey,array('link_clss'=>22,'link_tkey'=>$this->tkey,'debug'=>0,'no_indx'=>1)) ;
         //print_2x_arr($arr_links) ;
         //damp_array($arr_links) ;
         if (sizeof($arr_links)) foreach($arr_links as $rec_link)
         { if ($rec_link['obj_clss']==1) $this->banners[$rec_link['obj_pkey']][$rec_link['link_pkey']]=$banner_recs[$rec_link['link_pkey']] ;


         }
         //damp_array($arr_links) ;
         //damp_array($this->banners) ;
       }
	   set_time_point('Создание объекта <strong>ban system</strong>') ;
    }

   function prepare_public_info(&$rec)
   {
     $rec['__name']=$rec['obj_name'] ;
     $rec['__href']=$rec['href'] ;
   }

   // размещает баннер в текущем месте кода с необходимой обвязкой
   function get_banner($id,$options=array()) { $this->place_banner($id,$options) ;}
   function place_banner($id,$options=array())
   {  global $reffer_text_find ;
      $ban_id=0 ;
      if ($options['return_html'] or $options['return_banner_attr']) ob_start() ;
      $a_class=($options['a_class'])? ' class="'.$options['a_class'].'"':'' ;
      if (isset($this->banners[$id]) and $options['mode']=='show_all' or $options['show_all']) // указана нруппа банеров
      { if (sizeof($this->banners[$id])) foreach($this->banners[$id] as $rec)
        { ?><div class=item><?
          if ($rec['href']) {?><a href="<? echo $rec['href']?>" <?echo $a_class?>><? echo $rec['value']?></a><?}
          else echo $rec['value'] ;
          if ($rec['manual']) {?><div class=manual><?echo $rec['manual']?></div><?}
          ?></div><?
        }
      }
      else
      if (isset($this->banners[$id])) // указана нруппа банеров
        { // выбор банера по ключевому слову - сделать систему полнотекстового поиска для выбора банера
          if ($reffer_text_find)
          {  $max_val=0 ;
             if (sizeof($this->banners[$id])) foreach($this->banners[$id] as $ban_pkey=>$ban_rec)
                { //echo $ban_pkey.' '.$ban_rec['words'].'<br>' ;
                  if ($ban_rec['words'])
                  { $cur_val=similar_text($reffer_text_find,$ban_rec['words'])  ;
                    if ($cur_val>$max_val) {$max_val=$cur_val ; $max_pkey=$ban_pkey ; }
                  }

                }
             $ban_id=$max_pkey ;
          }
          else
          { if ($options['debug']) echo 'Выводим один баннер из группы # '.$id.'<br>' ;
            $ban_ids=array_keys($this->banners[$id]) ;
            if (sizeof($this->last_show[$id])==sizeof($this->banners[$id])) { $this->last_show[$id]=array() ; /*echo '<strong class=red>Сбрасываем очередб выдачи</strong><br>' ; */}
            $ban_ids2=array_flip($ban_ids) ;
            $ban_id_first=$ban_ids[0] ; $ban_id_last=$ban_ids[sizeof($ban_ids)-1] ;
            if (sizeof($this->last_show[$id])) { $last_show_id=$this->last_show[$id][sizeof($this->last_show[$id])-1] ; $last_show_pos=$ban_ids2[$last_show_id] ; }
            else                               { $last_show_id=0 ; $last_show_pos=0 ; }

            //echo '$last_show_id='.$last_show_id.'<br>' ;
            //echo '$last_show_pos='.$last_show_pos.'<br>' ;

            switch($options['mode'])
            { case 'next_banner': if ($last_show_id and $last_show_pos<sizeof($ban_ids) and $last_show_id!=$ban_id_last) $ban_id=$ban_ids[$last_show_pos+1] ; else { $ban_id=$ban_id_first ;  $this->last_show[$id]=array() ; } break ;
              case 'prev_banner': if (!$last_show_id or $last_show_pos==0) { $ban_id=$ban_id_last ;  $this->last_show[$id]=array() ; } else $ban_id=$ban_ids[$last_show_pos-1] ; break ;
              default:            if (sizeof($this->last_show[$id])) foreach($this->last_show[$id] as $bid) unset($ban_ids2[$bid]) ;
                                  $ban_id=array_rand($ban_ids2) ; //выборка случайным образом из тех, что еще не были показаны
                                  //echo '$ban_ids2: Выбираем из:' ; print_r(array_flip($ban_ids2)) ; echo '<br>' ;

            }
            $this->last_show[$id][]=$ban_id ; // запоминаем id последнего показанного банера
            if ($options['debug']) echo '$ban_id:'.$ban_id.'<br>' ;
            //echo '$this->last_show[$id]: Очередь выдачи:' ; print_r($this->last_show[$id]) ; echo '<br>' ;
            //echo $this->banners[$id][$ban_id]['value'] ; echo '<br>' ;
          }

          $ban_info=$this->banners[$id][$ban_id] ; //damp_array($ban_info) ;
          if ($options['template']) print_template($ban_info,$options['template'],$options) ;
          else
          { if ($ban_info['href']) echo '<a href="'.$ban_info['href'].'" '.$a_class.'>'.$ban_info['value'].'</a>' ;
            else                   echo $ban_info['value'] ;



          }
        }
      else if (sizeof($this->banners)) foreach($this->banners as $parent=>$rec) if (array_key_exists($id,$rec))
         {  $ban_info=$this->banners[$parent][$id] ;
            if ($options['template']) print_template($ban_info,$options['template'],$options) ;
            else
            { if ($ban_info['href']) echo '<a href="'.$ban_info['href'].'" class="'.$a_class.'">'.$ban_info['value'].'</a>' ;
              else                   echo $ban_info['value'] ;
         }
      }
      if ($options['return_html']) return(ob_get_clean()) ;
      if ($options['return_banner_attr'])
      { $text=ob_get_clean() ;
        $img_info=pc_image_extractor($text) ;
        $a_info=pc_link_extractor($text) ;
        $res['img_src']=$img_info[0][0] ;
        $res['a_href']=$a_info[0][0] ;
        $res['obj_name']=$ban_info['obj_name'] ;
        return($res) ;
      }
      else return($ban_id) ; // возвращаем код показанного банера
   }

   function reg_show_event($id)
   { return ;
     // не регистрируем показы баннеров в журнале для сокращения числа запросов к базе
     if (_IS_SYSTEM_IP) return ;
	 //print_r($this) ;
     $reg_data= mktime(0, 0, 0, date("m"), date("d"), date("Y")); // время начала суток
     $rec_pkey=execSQL_van('select pkey,c_view from '.$this->stat_table_name.' where c_data='.$reg_data.' and c_view>0 and reffer="'.$id.'.'.$this->tkey.'"') ;
     if ($rec_pkey['pkey']) update_rec_in_table($this->stat_table_name,array('c_view'=>$rec_pkey['c_view']+1),'pkey='.$rec_pkey['pkey'],array('no_def_value'=>1)) ;
     	else {	$finfo=array('c_data'=>$reg_data,'c_view'=>1,'reffer'=>$id.'.'.$this->tkey) ;
     		    $pkey=adding_rec_to_table($this->stat_table_name,$finfo,array('no_def_value'=>1)) ;
     		    //echo 'Добавили запись pkey='.$pkey.'<br>' ;
     		 }
   }

   function reg_click_event($id) {}

   // регистрирует показ баннера в журнале
   function reg_banner_to_log($from_ban)
	{ if (_IS_SYSTEM_IP) return ;


	  // проверяем корректность кода баннера
	  list($pkey,$tkey)=explode('.',$from_ban) ;
	  $pkey=$pkey/1 ; $tkey=$tkey/1 ;
	  if (!$pkey or !$tkey) return ;
	  $reffer=$pkey.'.'.$tkey ;
	  //echo 'reffer='.$reffer.'<br>' ;

	  if (!_DOT($tkey)->pkey) return  ;

	  //header("Content-type: text/plain; charset=utf-8");
	  ////header("Cache-Control: no-store, no-cache, must-revalidate");
	  //header("Cache-Control: post-check=0, pre-check=0", false);

	   $reg_data= mktime(0, 0, 0, date("m"), date("d"), date("Y")); // время начала суток
	   $rec_pkey=execSQL_van('select pkey,c_goto from '.TM_LOG_BANNERS.' where c_data='.$reg_data.' and reffer="'.$reffer.'"') ;

	  $cnt=(!$rec_pkey['c_goto'])? 1:$rec_pkey['c_goto']+1 ;

	  if ($rec_pkey['pkey']) { $sql='update '.TM_LOG_BANNERS.' set c_goto='.$cnt.' where pkey='.$rec_pkey['pkey'] ;
	  						   //echo $sql ;
	   						   if (!$result = mysql_query($sql)) SQL_error_message($sql,'Не удалось добавить запись в таблицу : ',1) ;
	   						 }

	}

    function show_list_items($group_id,$template_name,$options=array())
    {
       if (sizeof($this->banners[$group_id]))  print_template($this->banners[$group_id],$template_name,$options) ;
       else panel_show_template_alert('Группа баннеров не существует',$group_id,$template_name,$options) ;
    }

 }

 // класс, отвечающий за размещение информации на сайте -------------------------------------------------------------------------------------------
class c_info_system
{
   public $table_name ;
   public $tkey ;
   public $infos=array() ; // все баннеры, сгруппированые по подкаталогу
   public $templates=array() ; // все баннеры, сгруппированые по подкаталогу
   public $templates_names=array() ; // все баннеры, сгруппированые по подкаталогу

   function c_info_system($options=array())
 	{  $this->use_makros=$options['use_makros'] ;
       $this->table_name=($options['table_name'])? $options['table_name']:TM_INFO ;
       $this->tkey=$_SESSION['pkey_by_table'][$this->table_name] ;
       $data=select_objs($this->tkey,'',' enabled=1',array('debug'=>$options['debug'],'order_by'=>'parent,indx','no_cur_lag_correct'=>0)) ;
       $this->infos=$data[$this->tkey][7] ; //select_objs($this->tkey,'',' clss=7 and enabled=1',array('debug'=>$options['debug'],'order_by'=>'parent,indx','no_group_clss'=>1)) ;
       $this->templates=$data[$this->tkey][56] ; //select_objs($this->tkey,'',' clss=7 and enabled=1',array('debug'=>$options['debug'],'order_by'=>'parent,indx','no_group_clss'=>1)) ;
	   $this->section=$data[$this->tkey][1] ;
       if (sizeof($this->templates)) foreach($this->templates as $id=>$rec) $this->templates_names[$rec['obj_name']]=$id ;
	   set_time_point('Создание объекта <strong>info system</strong>') ;
    }

   // размещает информа в текущем месте кода с необходимой обвязкой
   function info_id($id,$no_edit=0)
   { if (sizeof($this->infos[$id]))
        { $text=get_cur_lang_field($this->infos[$id],'value') ; //damp_array($this->infos[$id]) ;
          //_macros($text) ;
          //if (!$this->infos[$id]['is_HTML'] and stripos($text,'<script')===false) $text=nl2br($text) ;
          if ($this->infos[$id]['is_HTML'] and $_SESSION['__fast_edit_mode']=='enabled' and !$no_edit) $text='<section>'.$text.'</section>' ;
          if ($_SESSION['__fast_edit_mode']=='enabled' and !$no_edit) $text=$text.'<span class="fe" fname="obj_name" reffer="'.$id.'.'.$this->tkey.'"></span>' ;
          echo $text ;
        }
   }

   // размещает информа в текущем месте кода с необходимой обвязкой
   function info_id_name($id,$no_edit=0)
   { if (sizeof($this->infos[$id]))
        { $text=get_cur_lang_field($this->infos[$id],'obj_name') ;
          global $makros_system ; if (isset($makros_system)) $makros_system->exec($text) ;
          if ($this->infos[$id]['is_HTML'] and $_SESSION['__fast_edit_mode']=='enabled' and !$no_edit) $text='<section>'.$text.'</section>' ;
          if ($_SESSION['__fast_edit_mode']=='enabled' and !$no_edit) $text=$text.'<span class="fe" fname="obj_name" reffer="'.$id.'.'.$this->tkey.'"></span>' ;
          echo $text ;
        }
   }

   function get_id($id,$no_edit=0)
   { if (sizeof($this->infos[$id]))
        { $text=get_cur_lang_field($this->infos[$id],'value') ;
          if ($_SESSION['__fast_edit_mode']=='enabled'  and !$no_edit) $text=$text.'<span class="fe" fname="obj_name" reffer="'.$id.'.'.$this->tkey.'"></span>' ;
          return($text) ;
        }
   }

   function info($text,$no_edit=0,$no_def=0)
   { if (sizeof($this->infos)) foreach($this->infos as $id=>$info)
   	  if ($info['obj_name']==$text)
   	  { //echo $id ;
   	    $this->info_id($id,$no_edit) ;
   	    return ;
   	  }
     if (!$no_def) echo $text ;
   }

   function get($text,$no_edit=0,$no_def=0)
   { if (sizeof($this->infos)) foreach($this->infos as $id=>$info) if ($info['obj_name']==$text)  return($this->get_id($id,$no_edit)) ;
     if (!$no_def) return($text)  ;
   }

   /*старые имена функций, поддерживаются для совсемтимости, в дальнейшем будут удалены --------------------------------------------*/
   function place_info($id) 							{ $this->info_id($id) ; }
   function place_info_by_name($text,$debug=0) 			{ $this->info($text,$debug) ;}
   function get_info($id) 								{ return($this->get_id($id)) ; }
   function get_info_by_name($text,$debug=0) 			{ return($this->get($text,$debug));}
 }

function _get($text,$no_edit=0,$no_def=0) 	{ return($_SESSION['info_system']->get($text,$no_edit,$no_def)) ; }
function _get_id($id,$no_edit=0) 			{ return($_SESSION['info_system']->get_id($id,$no_edit)) ; }

function _info($text,$no_edit=0,$no_def=0)	{ $_SESSION['info_system']->info($text,$no_edit,$no_def) ; }
function _info_id($id,$no_edit=0)			{ $_SESSION['info_system']->info_id($id,$no_edit) ; }
function _info_id_name($id,$no_edit=0)		{ $_SESSION['info_system']->info_id_name($id,$no_edit) ; }



function ___info_id__()
{ $arr=execSQL_row('select pkey,obj_name from '.$_SESSION['info_system']->table_name.' where clss=7 and enabled=1') ;
  __damp_array_to_select($arr,'__select_info_id','','Блоки информации',1) ;
  global $__select_info_id ;
  if ($__select_info_id) { echo '_info_id('.$__select_info_id.');<br>' ;
  						   _info_id($__select_info_id) ;
  						 }
}

function ___info__()
{ $arr=execSQL_row('select pkey,obj_name from '.$_SESSION['info_system']->table_name.' where clss=7 and enabled=1') ;
  __damp_array_to_select($arr,'__select_info_id','','Блоки информации',0) ;
  global $__select_info_id ;
  if ($__select_info_id) { echo "_info('".$__select_info_id."');<br>" ;
  						   _info_id($__select_info_id) ;
  						 }
}


class c_fon_work
{
   function c_fon_work($options=array())
   {   global $TM_arb ;
   	   $this->enabled=0 ;
 	   if (!$TM_arb) return ;
       $this->table_name=($options['table_name'])? $options['table_name']:$TM_arb ;
       $this->tkey=$_SESSION['pkey_by_table'][$this->table_name] ;
       if (!$this->tkey) return ;
       $this->work=execSQL_van('select * from '.$this->table_name.' where enabled=1 and clss=24 and parent=1 order by indx') ;
       if (sizeof($this->work)) $this->enabled=1 ; else $this->enabled=0 ;
	   set_time_point('Создание объекта <strong>fon_work</strong>') ;
   }

   function arb()
   { global $dir_to_module,$send_mail_block_size ;
     include($dir_to_module.'i_fon_work.php') ;
     $state=execSQL_van('select enabled from '.$this->table_name.' where pkey='.$this->work['pkey']) ;
     if (!$state['enabled']) return ;
     $func_name='' ;
     $func_name=$this->work['func_name'] ;
     for ($i=0; $i<$send_mail_block_size; $i++)
     {   if ($func_name and function_exists($func_name)) $status=$func_name($this->work['arg']) ; else $status=-1 ;
	     if ($status!=-1) update_rec_in_table($this->table_name,array('status'=>$status),'pkey='.$this->work['pkey']) ;
	     else { update_rec_in_table($this->table_name,array('enabled'=>0),'pkey='.$this->work['pkey']) ;
	            $this->enabled=0 ;
	     	  }
     }
   }
}

 class c_IL_list
 { public $id ;
   public $name ;
   function c_IL_list($arr_name,$id=0)
   { $this->id=$id ;
     $this->name=$arr_name ;
   }

   // подгрузка данных из базы и сохзранение в сессии
   // при создания объекта _IL необходимо передавать id
   function reload_list()
   {  if (!$this->id)
       { echo '<div class=red>Не опеределено ID массива для объекта списка:</div>' ;
         damp_array($this,1,-1) ;
         return ;
       }

      if (!$this->name)
      { echo '<div class=red>Не опеределено имя массива для объекта списка:</div>' ;
        damp_array($this,1,-1) ;
        return ;
      }

      $lists=select_db_recs(TM_LIST,'parent='.$this->id,array('indx'=>'id'));
      $this->upload_list($lists) ;
   }

   function upload_list($arr_list)
   { if (!$this->name)
       { echo '<div class=red>Не опеределено имя массива для объекта списка:</div>' ;
         damp_array($this,1,-1) ;
         return ;
       }
     if (sizeof($arr_list)) foreach($arr_list as $i=>$rec) $this->init($arr_list[$i]) ;

     unset($_SESSION[$this->name]) ; $_SESSION[$this->name]=array() ;
     $_SESSION[$this->name]=$arr_list ;
     $GLOBALS[$this->name]=&$_SESSION[$this->name] ;
     if ($this->name) $_SESSION['index_list_names'][$this->id]=$this->name; // дубляж, уже ничего не дает, но пусть будет
   }

   // дообработчка записей списка перед загрузкой списка в сесиию
   function init(&$rec)
   {

   }

   function get_multichange_field_view($value,$options=array())
   { if (!$value) return ;
     $list=$_SESSION[$this->name] ;
     $indx_field=($options['fname'])? $options['fname']:'obj_name'  ;
     $arr_values=array() ;
     $arr_id=explode(' ',trim($value))  ;
     if (sizeof($arr_id)) foreach($arr_id as $id) $arr_values[]=$list[$id][$indx_field] ;
     if (sizeof($arr_values)) $text=implode('<br>',$arr_values) ; else $text='' ;
     return($text) ;
   }

   function get_id_of_value($value,$options=array())
   {  $id=0 ;
      if (sizeof($_SESSION[$this->name])) foreach($_SESSION[$this->name] as $rec) if ($rec['obj_name']==$value)  $id=$rec['id'] ;
      if (!$id and $options['append_value_is_not_found'])
      { $id=$this->append_rec_to_list(array('obj_name'=>$value)) ;
        echo 'Список "'.$this->name.'": добавлено новое значение "'.$value.'"<br>' ;
      }
      return($id) ;
   }

   function append_rec_to_list($rec)
   {  $rec['parent']=$this->id ;
      if (!$rec['clss'])  $rec['clss']=20 ;
      $id=_CLSS($rec['clss'])->obj_create($rec,array('return_id'=>1)) ;
      return($id) ;
   }

 }

 // создаем объект класса списка
 // через этот объект будет производится все операции со списком
 // list_id - id списка или PHP название массива
 // ВНИМАНИЕ!!! массив уже должен быть зарегистрирован в $_SESSION['index_list_names']

 function _IL($id)
 { //echo '<br>IN: ('.$id.')<br>' ;
   $_id=(int)$id ; $arr_name='' ;
   if (!$_id)  { $arr_name=$id ;
                 $list_id=array_search($arr_name,$_SESSION['index_list_names']) ;
                 if ($GLOBALS['debug_indx_list']) echo 'По названию списка <strong>'.$arr_name.'</strong> определен код массива: <strong>'.$list_id.'</strong><br>' ;
               }
   else        { $list_id=$_id ;
                 $arr_name=$_SESSION['index_list_names'][$list_id] ;
                 if ($GLOBALS['debug_indx_list']) echo 'По коду списка <strong>'.$list_id.'</strong> определено arr_name: <strong>'.$arr_name.'</strong><br>' ;
               }
   //echo 'OUT: ('.$list_id.','.$arr_name.')<br>' ;

   if (is_object($GLOBALS['obj_list_'.$list_id])) return($GLOBALS['obj_list_'.$list_id]) ;
   if (class_exists($arr_name,false)) { $GLOBALS['obj_list_'.$list_id]=new $arr_name($arr_name,$list_id) ; if ($GLOBALS['debug_indx_list']) echo 'Для '.$arr_name.' использован класс '.$arr_name.'<br>' ; }
   else                               { $GLOBALS['obj_list_'.$list_id]=new c_IL_list($arr_name,$list_id) ; if ($GLOBALS['debug_indx_list']) echo 'Для '.$arr_name.' использован класс c_IL_list<br>' ; }
   return($GLOBALS['obj_list_'.$list_id]) ;
 }



 /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 //
 // просто функции, которые необходимы только для сайта
 //
 /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

 function search_register($text,$cnt1,$cnt2)
 { global $TM_search ;
 	adding_rec_to_table($TM_search,array('obj_name'=>addslashes($text),'clss'=>19,'cnt_goods'=>$cnt1,'cnt_cats'=>$cnt2)) ; // создаем новую ветку
 }


?>
