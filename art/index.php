<? // доработанный входной скрипт
   // позволяем обращатся к индивидуальным шаблонам страниц каталога, на основе пути и названия страницы
   // примеры URL страниц, dir скрипта и class_name
   // /art/press/               => /class/ext/art/c_art_press.php           c_page_art_press
   // /art/press/demo/1.html    => /class/ext/art/c_art_press_demo_1.php    c_page_art_press_demo_1
   // /art/press/demo/ss/1.html    => /class/ext/art/c_art_press_demo_ss_1.php    c_page_art_press_demo_ss_1
   // всe URL для которых не будет найдено собственных скриптов, будут обработаны через стандартный входной скрипт  /class/ext/c_art.php  c_page_art

   include ($_SERVER['DOCUMENT_ROOT']."/ini/patch.php")  ;
   include (_DIR_TO_ENGINE."/i_system.php") ;
   // !!! директория, в которой расположены скрипты каталога
   $options['ext']='art' ;

   $arr=explode('/',_CUR_PAGE_DIR) ;
   $section_script_template='c_'.$options['ext'].'_'.$arr[3] ;
   $check_file=_DIR_EXT.'/'.$options['ext'].'/c_'.$options['ext'] ;
   $check_class='c_page_'.$options['ext'] ;
   if (sizeof($arr)>2) for($i=2;$i<sizeof($arr);$i++) if ($arr[$i])
   { $cur_check_file=$check_file.'_'.$arr[$i].'.php' ;
     $cur_check_class=$check_class.'_'.$arr[$i].'.php' ;
     //echo 'Ищем файл '.$cur_check_file.'<br>' ;
     if (file_exists($cur_check_file))
     {  $options['class_file']=$cur_check_file ;
        $options['class_name']=$cur_check_class ;
     }
     $check_file.='_'.$arr[$i] ;
     $check_class.='_'.$arr[$i] ;
   }
   if (_CUR_PAGE_NAME)
   { $arr_name=explode('.',_CUR_PAGE_NAME) ;
     $cur_check_file=$check_file.'_'.$arr_name[0].'.php' ;
     $cur_check_class=$check_class.'_'.$arr_name[0].'.php' ;
     //echo 'Ищем файл '.$cur_check_file.'<br>' ;
     if (file_exists($cur_check_file))
        {  $options['class_file']=$cur_check_file ;
           $options['class_name']=$cur_check_class ;
        }
   }

   show_page($options) ;
?>