<?
include ("ext/HTML/c_HTML.php") ;
class c_page extends c_HTML
{

  function set_head_tags()
  { $this->HEAD['favicon']='/favicon.ico'  ;

    // стандартный JQuery
    $this->HEAD['js'][]=_PATH_TO_EXT.'/jquery/jquery-1.8.3.min.js' ;
    $this->HEAD['js'][]=_PATH_TO_EXT.'/jquery/jquery.maskedinput.min.js' ;

    // mootools
    //$this->HEAD['js'][]=_PATH_TO_EXT.'/mootools/mootools-core-1.3.1.js' ;
    $this->HEAD['js'][]='http://ajax.googleapis.com/ajax/libs/mootools/1.4.5/mootools-yui-compressed.js' ;
    $this->HEAD['js'][]=_PATH_TO_EXT.'/mootools/mootools-more-1.3.1.1.js' ;

   // библитека mBox - mForm + модальные окна
   $this->HEAD['js'][]=_PATH_TO_EXT.'/mBox/jquery.form.js' ;
   $this->HEAD['js'][]=_PATH_TO_EXT.'/mBox/StephanWagner-mBox-0.2.6/mBox.Core.js' ;
   $this->HEAD['js'][]=_PATH_TO_EXT.'/mBox/StephanWagner-mBox-0.2.6/mBox.Modal.js' ;
   //$this->HEAD['js'][]=_PATH_TO_EXT.'/mBox/StephanWagner-mBox-0.2.6/mBox.Modal.Confirm.js' ;
   //$this->HEAD['js'][]=_PATH_TO_EXT.'/mBox/StephanWagner-mBox-0.2.6/mBox.Notice.js' ;
   $this->HEAD['js'][]=_PATH_TO_EXT.'/mBox/StephanWagner-mBox-0.2.6/mBox.Tooltip.js' ;
   $this->HEAD['css'][]=_PATH_TO_EXT.'/mBox/StephanWagner-mBox-0.2.6/assets/mBoxCore.css' ;
   $this->HEAD['css'][]='/assets/mBoxModal.css' ;
   //$this->HEAD['css'][]=_PATH_TO_EXT.'/mBox/StephanWagner-mBox-0.2.6/assets/mBoxNotice.css' ;
   $this->HEAD['css'][]=_PATH_TO_EXT.'/mBox/StephanWagner-mBox-0.2.6/assets/mBoxTooltip.css' ;
   //$this->HEAD['css'][]=_PATH_TO_EXT.'/mBox/StephanWagner-mBox-0.2.6/assets/themes/mBoxTooltip-Black.css' ;
   //$this->HEAD['css'][]=_PATH_TO_EXT.'/mBox/StephanWagner-mBox-0.2.6/assets/themes/mBoxTooltip-BlackGradient.css' ;
   $this->HEAD['js'][]=_PATH_TO_EXT.'/mBox/StephanWagner-mForm-0.2.6/mForm.Core.js' ;
   $this->HEAD['js'][]=_PATH_TO_EXT.'/mBox/StephanWagner-mForm-0.2.6/mForm.Element.js' ;
   $this->HEAD['js'][]=_PATH_TO_EXT.'/mBox/StephanWagner-mForm-0.2.6/mForm.Element.Select.js' ;
   $this->HEAD['js'][]=_PATH_TO_EXT.'/mBox/StephanWagner-mForm-0.2.6/mForm.Submit.js' ;
   $this->HEAD['css'][]='/assets/mForm.css' ; //_PATH_TO_EXT.'/mBox/StephanWagner-mForm-0.2.6/assets/mForm.css' ;
   $this->HEAD['css'][]='/assets/mFormElement-Select.css' ; //_PATH_TO_EXT.'/mBox/StephanWagner-mForm-0.2.6/assets/mFormElement-Select.css' ;
   $this->HEAD['js'][]=_PATH_TO_EXT.'/mBox/modal_window.js' ;

   // стандартное подключение highslide
   $this->HEAD['js'][]=_PATH_TO_EXT.'/highslide/highslide-4.1.13/highslide.js' ;
   $this->HEAD['css'][]=_PATH_TO_EXT.'/highslide/highslide-4.1.13/highslide.css' ;

   // стандартное подключение ZeroClipboard
   $this->HEAD['js'][]=_PATH_TO_EXT.'/zeroclipboard-1.2.3/ZeroClipboard.js' ;

    // стандартные файлы - должны подключаться последними
   $this->HEAD['js'][]='/script.js' ;
   $this->HEAD['css'][]='/style.css' ;
  }


  function local_page_head()
  {
    ?><script type="text/javascript">
        var _PATH_TO_EXT='<?echo _PATH_TO_EXT?>' ;
        var clip ;
        ZeroClipboard.setDefaults( { moviePath: 'http://<?echo _MAIN_DOMAIN._PATH_TO_EXT?>/zeroclipboard-1.2.3/ZeroClipboard.swf' } );
     </script><?
  }

  function body() {?><body class=x1><table id=wrapper><tr><td><? $this->block_main();?></td></tr></body><?}

  function body_work()
  { ?><body>
	     <div id=block_top><div class=inner><?$this->block_top();?></div></div>
	     <div id=block_tabs><?  $this->tabs_setting();
                                $this->block_tabs();
                            ?>
         </div>
	     <div id=block_main><? if ($this->checked_objs()) $this->block_main();?><div class=clear></div></div>
	     <div id=block_bottom><? $this->block_bottom();?></div>
	</body>
    <?
  }

  function block_top()
  {
    $this->panel_account_info() ;
    $this->panel_logout() ;
  }

  function block_bottom()
  {

  }

  // -----------------------------------------------------------------------------------
  // шапка сайта
  // -----------------------------------------------------------------------------------

  function panel_account_info()
  { ?><div id=panel_account_info><a href="/cabs/"><?echo $_SESSION['member']->name?></a></div><?
  }

  function panel_logout()
  {
     ?><div id=panel_logout><a href="/account/logout.php">Выход</a></div><?
  }

  // -----------------------------------------------------------------------------------
  // панель вкладок меню
  // -----------------------------------------------------------------------------------

  function tabs_setting() {}

  function block_tabs()
  { $cur_page=str_replace($this->tabs_dir,'',_CUR_PAGE_DIR._CUR_PAGE_NAME) ;
    //echo '_CUR_PAGE_DIR='._CUR_PAGE_DIR.'<br>' ; echo 'tabs_dir='.$this->tabs_dir.'<br>' ; echo '$cur_page='.$cur_page.'<br>' ;
    ?><div id="cab_tabs">
        <? if (sizeof($this->tabs)) foreach($this->tabs as $page=>$name) {?><div class="item <?if ($cur_page==$page) echo 'active'?>"><a href="<?echo $this->tabs_dir.$page?>"><?echo $name?></a></div><?}?>
      <div class=clear></div></div>
    <?
  }

 // проверка корректности разпознования id объектов, переданных через URL
  function checked_objs() { return(1) ; }

  // -----------------------------------------------------------------------------------
  // загрузчик файлов
  // -----------------------------------------------------------------------------------

  function panel_fancyupload($options=array())
  {
    ?><div id="panel_fancyupload" class="<?echo $options['class']?>">
       <?$this->panel_upload_alert() ;?>
              <form name="form" id="form-upload" method="post" enctype="multipart/form-data" action="/class/fancyupload.php">
              <input type="hidden" name=active_tab value=0>
              <input type=hidden name=UID id=UID value="<?echo md5(uniqid(rand(),true))?>">
                     <fieldset id="upload-fallback">
                         <legend>Загрузка файлов</legend>
                         <p>Подождите, идет запуск загрузчика файлов... </p>
                         <label for="upload-photoupload">Загрузка файла:<input type="file" name="Filedata" /></label>
                     </fieldset>
                     <div id="upload-status" class="hide">
                         <div id=menu_panel>
                             <img src="<?echo _PATH_TO_EXT?>/fancyupload-v3.0.1/assets/select_file.png"> <a href="#" id="upload-browse">Добавить скан документов</a>
                             <br>
                         </div>
                         <div><strong class="overall-title"></strong><br /><img src="<?echo _PATH_TO_EXT?>/fancyupload-v3.0.1/assets/progress-bar/bar.gif" class="progress overall-progress" /></div>
                         <div><strong class="current-title"></strong><br /><img src="<?echo _PATH_TO_EXT?>/fancyupload-v3.0.1/assets/progress-bar/bar.gif" class="progress current-progress" /></div>
                         <div class="current-text"></div>
                     </div>
                     <ul id="upload-list"></ul>
              </form>
       </div>
    <?
  }

    function panel_upload_alert()
    { $max_upload = (int)(ini_get('upload_max_filesize')) ;
      $max_post = (int)(ini_get('post_max_size')) ;
      $memory_limit = (int)(ini_get('memory_limit'));
      $upload_mb = min($max_upload, $max_post, $memory_limit);
      echo 'Внимание! Максимальный размер файла, который Вы можете загрузить -<br><span class=green>'.$upload_mb.' Мб</span><div class=format_alert>Форматы изображений - <strong>JPG, PNG, BMP</strong></div><br><br>' ;
    }

}

?>