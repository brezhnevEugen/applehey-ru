<?php
include_once(_DIR_TO_MODULES.'/reports/i_report.php') ;
class report_orders extends c_report
{
 var $title='Заказы' ;
 var $view_by_space_filter=1 ;

 function setting($options=array())
 {
      // внимание! для не администратора тут только сам оператор
      ?><div id="panel_filter">
             <form id="sky-form" class="sky-form"><input type="hidden" name="filter_dir" value="<?echo _CUR_REPORT_DIR?>">
                  <fieldset>
                      <div class="row" id="person_info">
                           <section class="col col-2">
                               <label class="label">Номер заказа</label>
                               <label class="input">
                                    <input type="text" name="filter[name]" placeholder="" value="<?echo htmlspecialchars($this->filter['name'])?>">
                               </label>
                           </section>
                           <section class="col col-2">
                               <label class="label">Артикул товара</label>
                               <label class="input">
                                    <input type="text" name="filter[art]" placeholder="" value="<?echo htmlspecialchars($this->filter['art'])?>">
                               </label>
                           </section>
                           <section class="col col-2">
                               <label class="label">Штрих-код товара</label>
                               <label class="input">
                                    <input type="text" name="filter[code]" placeholder="" value="<?echo htmlspecialchars($this->filter['code'])?>">
                               </label>
                           </section>
                          <section class="col col-2">
                              <label class="label">Статус</label>
                              <label class="select">
                                  <select name="filter[status]"><option></option>
                                      <?if (sizeof($_SESSION['list_status_orders'])) foreach($_SESSION['list_status_orders'] as $id=>$value){?><option value="1" <?if ($this->filter['status']==$id) echo 'selected'?>><?echo $value?></option><?}?>
                                  </select>
                              </label>
                          </section>
                           <section class="col col-2">
                               <label class="label">Оплата</label>
                               <label class="select">
                                   <select name="filter[payment]"><option></option>
                                       <?if (sizeof($_SESSION['ARR_orders_payments'])) foreach($_SESSION['ARR_orders_payments'] as $id=>$value){?><option value="1" <?if ($this->filter['payment']==$id) echo 'selected'?>><?echo $value?></option><?}?>
                                   </select>
                               </label>
                           </section>
                          <section class="col col-2">
                              <label class="label">Дата от</label>
                              <label class="input">
                                  <i class="icon-append fa fa-calendar"></i>
                                  <input type="text" name="filter[data]" class="datepicker" value="<?echo $this->filter['data']?>">
                              </label>
                            </section>
                        </div>
                  </fieldset>
                  <?$this->panel_button_apply();?>
              </form>
       </div><br>
     <?
  }
    
 function setting_details($options=array())
     {    $_usl=array() ;
          ?><div id="panel_filter">
                 <form id="sky-form" class="sky-form"><input type="hidden" name="filter_dir" value="<?echo _CUR_REPORT_DIR?>">
                      <fieldset>
                               <section class="col">
                                   <label class="label">Наименование</label>
                                   <label class="input">
                                        <input type="text" name="filter[name]" placeholder="" value="<?echo htmlspecialchars($this->filter['name'])?>">
                                   </label>
                               </section>
                               <section class="col">
                                   <label class="label">Артикул</label>
                                   <label class="input">
                                        <input type="text" name="filter[art]" placeholder="" value="<?echo htmlspecialchars($this->filter['art'])?>">
                                   </label>
                               </section>
                               <section class="col">
                                   <label class="label">Штрих-код</label>
                                   <label class="input">
                                        <input type="text" name="filter[code]" placeholder="" value="<?echo htmlspecialchars($this->filter['code'])?>">
                                   </label>
                               </section>
                              <section class="col">
                                  <label class="label">Показаны на сайте</label>
                                  <label class="select">
                                      <select name="filter[enabled]"><option></option>
                                          <option value="1" <?if ($this->filter['enabled']=='1') echo 'selected'?>>Да</option>
                                          <option value="2" <?if ($this->filter['enabled']=='2') echo 'selected'?>>Нет</option>
                                      </select>
                                  </label>
                              </section>
                              <? if ($this->filter['enabled']==1) $_usl[]='enabled=1 and _image_name!=""' ;
                                 if ($this->filter['enabled']==2) $_usl[]='(enabled=0 or enabled is null or _image_name="" or _image_name is null)' ;
                              ?>
                               <section class="col">
                                   <label class="label">Пол</label>
                                   <label class="select">
                                       <select name="filter[pol]"><option></option>
                                           <option value="m" <?if ($this->filter['pol']=='m') echo 'selected'?>>Мужчины</option>
                                           <option value="w" <?if ($this->filter['pol']=='w') echo 'selected'?>>Женщины</option>
                                           <option value="k" <?if ($this->filter['pol']=='k') echo 'selected'?>>Дети</option>
                                       </select>
                                   </label>
                               </section>
                               <? if ($this->filter['pol']) $_usl[]=$this->filter['pol'].'=1' ;
                               if (isset($this->filter['pol']) and ($this->filter['pol']=='w' or $this->filter['pol']=='m' or $this->filter['pol']=='k'))
                               { ?><section class="col">
                                       <label class="label">Категория товара</label>
                                       <label class="select">
                                           <?
                                           $usl=(sizeof($_usl))? ' and '.implode(' and ',$_usl):'' ;
                                           $arr=array();
                                           $arr_ids=execSQL_line('select distinct(type) from view_goods_all where clss=200 and type>0 '.$usl) ;
                                           if (sizeof($arr_ids))  $arr=execSQL_row('select pkey,obj_name from obj_site_category where pkey in ('.implode(',',$arr_ids).') order by obj_name') ;
                                           ?>
                                           <select name="filter[type]"><option></option><?
                                               if (sizeof($arr)) foreach($arr as $id=>$name) {?><option value="<?echo $id?>" <?if ($id==$this->filter['type']) echo 'selected'?>><?echo $name?></option><?} ?>
                                           </select>
                                       </label>
                                    </section>
                                  <?
                               }
                               if ($this->filter['type']) $_usl[]='type='.$this->filter['type'] ;
                               ?>
                               <section class="col">
                                   <label class="label">Бренд</label>
                                   <label class="select">
                                       <?
                                       $usl=(sizeof($_usl))? ' and '.implode(' and ',$_usl):'' ;
                                       $arr=array();
                                       $arr_ids=execSQL_line('select distinct(brand_id) from view_goods_all where clss=200 and brand_id>0 '.$usl) ;
                                       if (sizeof($arr_ids))  $arr=execSQL_row('select id,obj_name from obj_site_list where clss=222 and parent=1447 and id in ('.implode(',',$arr_ids).') order by obj_name') ;
                                       ?>
                                       <select name="filter[brand]"><option></option><?
                                           if (sizeof($arr)) foreach($arr as $id=>$name) {?><option value="<?echo $id?>" <?if ($id==$this->filter['brand']) echo 'selected'?>><?echo $name?></option><?} ?>
                                       </select>
                                   </label>
                               </section>
                               <?if ($this->filter['brand']) $_usl[]='brand_id='.$this->filter['brand'] ;?>
                               <section class="col">
                                   <label class="label">Сезон</label>
                                   <label class="select">
                                       <?
                                       $usl=(sizeof($_usl))? ' and '.implode(' and ',$_usl):'' ;
                                       $arr=array();
                                       $arr_ids=execSQL_line('select distinct(sezon_id) from view_goods_all where clss=200 and sezon_id>0 '.$usl) ;
                                       if (sizeof($arr_ids))  $arr=execSQL_row('select id,obj_name from obj_site_list where clss=20 and parent=	7656 and id in ('.implode(',',$arr_ids).') order by obj_name') ;
                                       ?>
                                       <select name="filter[sezon]"><option></option><?
                                           if (sizeof($arr)) foreach($arr as $id=>$name) {?><option value="<?echo $id?>" <?if ($id==$this->filter['sezon']) echo 'selected'?>><?echo $name?></option><?} ?>
                                       </select>
                                   </label>
                               </section>
                               <section class="col">
                                   <label class="label">Показать только:</label>
                                   <label class="checkbox"><input type="checkbox" name="filter[new]" value="1" <?if ($this->filter['new']) echo 'checked'?>><i></i> Новинки</label>
                                   <label class="checkbox"><input type="checkbox" name="filter[hits]" value="1" <?if ($this->filter['hits']) echo 'checked'?>><i></i> Хиты</label>
                                   <label class="checkbox"><input type="checkbox" name="filter[no_sezon]" value="1" <?if ($this->filter['no_sezon']) echo 'checked'?>><i></i> Без сезона</label>
                                   <label class="checkbox"><input type="checkbox" name="filter[no_brand]" value="1" <?if ($this->filter['no_brand']) echo 'checked'?>><i></i> Без бренда</label>
                               </section>
                               <div class="clear"></div>
                               <section class="col col_6">
                                   <label class="label">Фото:</label>
                                   <label class="checkbox"><input type="checkbox" name="filter[photo]" value="1" <?if ($this->filter['photo']==1) echo 'checked'?>><i></i> Есть</label>
                                   <label class="checkbox"><input type="checkbox" name="filter[photo]" value="2" <?if ($this->filter['photo']==2) echo 'checked'?>><i></i> Нет</label>
                               </section>
                               <section class="col col_6">
                                   <label class="label">Цена:</label>
                                   <label class="checkbox"><input type="checkbox" name="filter[price]" value="1" <?if ($this->filter['price']==1) echo 'checked'?>><i></i> Есть</label>
                                   <label class="checkbox"><input type="checkbox" name="filter[price]" value="2" <?if ($this->filter['price']==2) echo 'checked'?>><i></i> Нет</label>
                               </section>
                               <section class="col col_6">
                                   <label class="label">Наличие:</label>
                                   <label class="checkbox"><input type="checkbox" name="filter[stock]" value="1" <?if ($this->filter['stock']==1) echo 'checked'?>><i></i> Есть</label>
                                   <label class="checkbox"><input type="checkbox" name="filter[stock]" value="2" <?if ($this->filter['stock']==2) echo 'checked'?>><i></i> Нет</label>
                               </section>

                      </fieldset>
                      <?$this->panel_button_apply();?>
                  </form>
           </div><br>
         <?
      }     

  function create_usl_select_obj($_params,&$options)
  {  //damp_array($this->filter) ;
     $usl=array() ;
     $usl[]='clss=82' ;
     if ($this->filter['name'])              $usl[]='obj_name like "'.$this->filter['name'].'%"' ;
     if ($this->filter['art'])               $usl[]='art like "'.$this->filter['art'].'%"' ;
     if ($this->filter['code'])
     {   $goods_ids=execSQL_line('select goods_id from obj_site_1c where code="'.htmlspecialchars($this->filter['code']).'"') ;
         if (sizeof($goods_ids)) $usl[]='pkey in ('.implode(',',$goods_ids).')' ;
         else                    $usl[]='pkey=0' ;
     }
     if ($this->filter['photo']==1)           $usl[]='_image_name!=""' ;
     if ($this->filter['photo']==2)           $usl[]='(_image_name="" or _image_name is null)' ;
     if ($this->filter['price']==1)           $usl[]='price>0' ;
     if ($this->filter['price']==2)           $usl[]='(price=0 or price is null' ;
     if ($this->filter['stock']==1)           $usl[]='stock>0' ;
     if ($this->filter['stock']==2)           $usl[]='(stock=0 or stock is null' ;
     if ($this->filter['enabled']==1)         $usl[]='enabled=1 and _image_name!=""' ;
     if ($this->filter['enabled']==2)         $usl[]='(enabled=0 or enabled is null or _image_name="" or _image_name is null)' ;
     if ($this->filter['pol']=='m')           $usl[]='m=1' ;
     if ($this->filter['pol']=='w')           $usl[]='w=1' ;
     if ($this->filter['pol']=='k')           $usl[]='k=1' ;
     if ($this->filter['type'])               $usl[]='type='.$this->filter['type'] ;
     if ($this->filter['brand'])              $usl[]='brand_id='.$this->filter['brand'] ;
     if ($this->filter['sezon'])              $usl[]='sezon_id='.$this->filter['sezon'] ;
     if ($this->filter['new'])                $usl[]='new=1' ;
     if ($this->filter['hits'])               $usl[]='top=1' ;
     if ($this->filter['no_sezon'])           $usl[]='(sezon_id=0 or sezon_id is null)' ;
     if ($this->filter['no_brand'])           $usl[]='(brand_id=0 or brand_id is null)' ;
     if ($this->filter['no_photo'])           $usl[]='(_image_name="" or _image_name is null)' ;
     if ($this->filter['no_type'])            $usl[]='(type=0 or type is null)' ;
     if ($this->filter['no_price'])           $usl[]='(price=0 or price is null)' ;

     $usl_res=implode(' and ',$usl) ;
     //damp_array($usl) ;
     return($usl_res) ;
  }

 function panel_info_filter()
 { $_str=array() ; $info=array() ; $title='' ;
   if ($this->filter['name'])               $info['Наименование']='"'.htmlspecialchars($this->filter['name']).'"' ;
   if ($this->filter['art'])                $info['Артикул']='"'.htmlspecialchars($this->filter['art']).'"' ;
   if ($this->filter['code'])               $info['Штрих-код']='"'.htmlspecialchars($this->filter['code']).'"' ;
   if ($this->filter['photo']==1)           $info['Фото']='Есть' ;
   if ($this->filter['photo']==2)           $info['Фото']='Нет' ;
   if ($this->filter['price']==1)           $info['Цена']='Есть' ;
   if ($this->filter['price']==2)           $info['Цена']='Нет' ;
   if ($this->filter['stock']==1)           $info['Наличие']='Есть' ;
   if ($this->filter['stock']==2)           $info['Наличие']='Нет' ;
   if ($this->filter['enabled']==1)         $info['На сайте']='Да' ;
   if ($this->filter['enabled']==2)         $info['На сайте']='Нет' ;
   if ($this->filter['pol']=='m')           $info['Пол']='Мужская' ;
   if ($this->filter['pol']=='w')           $info['Пол']='Женская' ;
   if ($this->filter['pol']=='k')           $info['Пол']='Детская' ;
   if ($this->filter['type'])               $info['Категория']=execSQL_value('select obj_name from obj_site_category where pkey='.$this->filter['type']) ;
   if ($this->filter['brand'])              $info['Бренд']=execSQL_value('select obj_name from obj_site_list where parent=1447 and id='.$this->filter['brand']) ;
   if ($this->filter['sezon'])              $info['Сезон']=execSQL_value('select obj_name from obj_site_list where parent=7656 and id='.$this->filter['sezon']) ;
   if ($this->filter['new'])                $info['Новинки']='Да' ;
   if ($this->filter['hits'])               $info['Хиты']='Да' ;
   if ($this->filter['no_sezon'])           $info['Без сезона']='Да' ;
   if ($this->filter['no_brand'])           $info['Без бренда']='Да' ;
   if ($this->filter['no_photo'])           $info['Без фото']='Да' ;
   if ($this->filter['no_type'])            $info['Без категории']='Да' ;
   if ($this->filter['no_price'])           $info['Без цены']='Да' ;

   if (sizeof($info)) foreach($info as $title=>$value) $_str[]=$title.': <strong>'.$value.'</strong>' ;
   if (sizeof($_str)) $title='<p class=center>'.implode(' ',$_str).'</p>' ;
   return $title ;
 }


  function get_cnt_items($usl,$options=array())
  { $cnt=execSQL_value('select count(pkey)
                        from  view_orders
                        where '.$usl,$options) ;
    return($cnt) ;
  }

  function get_items($usl,$options=array())
  { $limit=($options['limit'])? ' '.$options['limit']:'' ;
    $order=($this->filter['order'])? ' order by '.$this->filter['order']:' order by pkey desc' ;
 	$list_rec=execSQL('select *
                       from  view_orders
                       where '.$usl.$order.$limit,$options) ;
    //if (sizeof($list_rec)) foreach($list_rec as $id=>$rec) PERSONAL()->prepare_public_info_to_personal($list_rec[$id],$options) ;
    return($list_rec) ;
  }


 function print_template_HTML($list_recs,$options)
 {   //print_2x_arr($list_recs) ;
     $this->panel_info_select($options) ;
     include_class(0) ;
     _CLSS(82)->show_list_items($list_recs,$options) ;
     return ;


     ?><table class="basic fz_small full auto">
       <tr>
           <th>№ п/п</th>
           <th>№ пропуска</th>
           <th>HR_ID</th>
           <th>ФИО</th>
           <th>Организция</th>
           <th>Отдел</th>
           <th>Дата рождения</th>
       </tr>
     <? $i=($options['first_obj'])? $options['first_obj']:1;
        if (sizeof($list_recs)) foreach($list_recs as $rec)
        {?>
            <tr>
                <td><?echo $i ; $i++ ;?></td>
                <td><?echo $rec['PROPUSK_NUMBER']?></td>
                <td><?echo $rec['HR_ID']?></td>
                <td class="left"><?echo $rec['obj_name']?></td>
                <td class="left"><?echo $rec['_org_name']?></td>
                <td class="left"><?echo $rec['_otdel_name']?></td>
                <td><?echo $rec['dr']?></td>
            </tr>
       <?}?>
     </table><?

     $this->panel_info_select($options) ;
 }



}

?>