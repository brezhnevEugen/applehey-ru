<?php
include(_DIR_EXT.'/admin_editor/c_admin_editor_viewer.php') ;
class c_page_cab_admin_editor_viewer_childs extends c_page_cab_admin_editor_viewer
{

  public $h1='Карточка объекта - Дамп БД' ;
  public $cur_child_clss=0 ;

  function select_obj_info()
  { $arr=explode('/',_CUR_PAGE_DIR) ;
    $this->cur_child_clss=(isset($arr[6]) and $arr[6]>0)? $arr[6]:0 ;

    parent::select_obj_info() ;
    if ($this->cur_rec['pkey'])
    { $this->h1.=' - '._CLSS($this->cur_child_clss)->name ;

    }

  }

   function block_main()
   { $this->page_title() ;
     if ($this->cur_child_clss)
     { include_class(0) ;
       _CLSS($this->cur_child_clss)->show_list_items($this->cur_rec['obj_clss_'.$this->cur_child_clss],array()) ;
       _CLSS($this->cur_child_clss)->show_buttons(array('buttons'=>CLSS($this->cur_child_clss)->view['list']['options']['buttons'],
                                                        'cur_obj_rec'=>$this->cur_rec)
                                                 ) ;
     }
     //damp_array(_CLSS($this->cur_child_clss)) ;
     //damp_array(_CLSS($this->cur_child_clss)) ;
   }

}
?>