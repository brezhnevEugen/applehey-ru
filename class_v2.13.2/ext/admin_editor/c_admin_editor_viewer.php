<?php
include(_DIR_TO_CLASS.'/c_page.php') ;
include(_DIR_TO_ENGINE.'/admin/i_clss_func.php') ;
class c_page_cab_admin_editor_viewer extends c_page
{

  public $h1='Карточка объекта' ;
  public $cur_rec=array() ;

  function set_head_tags()
   { parent::set_head_tags() ;
     //if (_CUR_PAGE_DIR=='/cabs/photo/')
     //  $this->set_head_tags_fancyupload() ;

   }

   function set_head_tags_fancyupload()
   {   $this->HEAD['js'][]='https://ajax.googleapis.com/ajax/libs/mootools/1.5.1/mootools-yui-compressed.js' ;
       $this->HEAD['js'][]=_PATH_TO_EXT.'/fancyupload-v3.0.1/source/Swiff.Uploader.js' ;
       $this->HEAD['js'][]=_PATH_TO_EXT.'/fancyupload-v3.0.1/source/Fx.ProgressBar.js' ;
       $this->HEAD['js'][]=_PATH_TO_EXT.'/fancyupload-v3.0.1/source/FancyUpload2.js' ;
       $this->HEAD['js'][]=_PATH_TO_EXT.'/fancyupload-v3.0.1/fancyupload2.js' ;
       $this->HEAD['css'][]=_PATH_TO_EXT.'/fancyupload-v3.0.1/fancyupload.css' ;
       $this->HEAD['css'][]='/images/PT-Sans/stylesheet.css' ;
   }

    function select_obj_info()
    { if (_CUR_ID)
      { $this->cur_rec=get_obj_info(_CUR_ID) ;
        $this->cur_rec['__href_window']='/cab/admin_editor/viewer/'.$this->cur_rec['_reffer'].'/' ;
      }
      if ($this->cur_rec['pkey']) $this->h1=$this->cur_rec['obj_name'] ;
      else                        $this->result=404 ;
    }

    function panel_main_menu_title()
    {     $arr=array() ;
          $arr[]=$_SESSION['descr_clss'][$this->cur_rec['clss']]['name'] ;
          $arr[]='<strong>'.$this->cur_rec['obj_name'].'</strong>' ;
          ?><li aria-haspopup="true"><?echo implode('<br>',$arr)?></li><?
    }

      function menu_config()
      { if ($this->cur_rec['pkey'])
        {  $this->menu[$this->cur_rec['__href_window']]='Карточка' ;

           $menu_set=_CLSS($this->cur_rec['clss'])->get_menu_set_to_table($this->cur_rec['tkey'],$this->cur_rec['pkey']) ;
           if (sizeof($menu_set)) foreach($menu_set as $menu_rec)
            {  switch($menu_rec['type'])
                { case 'prop': $this->expand_menu_prop() ; break ;
                  case 'structure': $this->expand_menu_structure() ; break ;

                }

            }

           //$this->menu[$this->cur_rec['__href_window'].'log_events/']='Журнал событий' ;
           $this->menu[$this->cur_rec['__href_window'].'db/']='Дамп БД' ;
        }
        else parent::menu_config() ;
      }

   function block_main()
   { $this->page_title() ;

    


     echo '_CUR_ID='._CUR_ID.'<br>' ;
     if (sizeof($this->cur_rec)) _CLSS($this->cur_rec['clss'])->show_props($this->cur_rec,array())  ;
       
       
   }

   function expand_menu_prop()
   {

   }
    
   function expand_menu_structure()
   {
       $obj_childs_info=select_obj_childs_clss_cnt($this->cur_rec['_reffer']) ; // получаем список классов, которые уже добавлены к текущему объекту
       //damp_array($obj_childs_info) ;
       // пункты меню по существующим дочерним объектам
           if (sizeof($obj_childs_info)) foreach($obj_childs_info as $child_clss=>$cnt)
             if ($child_clss!=6)
                { $obj_clss=_CLSS($child_clss);
                  $this->menu[$this->cur_rec['__href_window'].'childs/'.$child_clss.'/']=$obj_clss->name($this->cur_rec['_reffer']); ;
                }
           // пункт меню "Добавить"
           $obj_create_info=_CLSS($this->cur_rec['clss'])->get_adding_clss($this->cur_rec['tkey'],$this->cur_rec['pkey']) ; //получаем список классов, которые могут быть добавлены к текущему объекту
       //damp_array($obj_create_info,1,-1) ;
           if (sizeof($obj_create_info))
               {  //$item_rec['id']=$this->id ; ;
                  //$item_rec['name']='Добавить' ;
                  //$item_rec['icon']='<img src="'._PATH_TO_ADMIN_IMG.'/add_icon.png" width="16" height="17" alt="" border="0" />' ;
                  //$this->panel_items[$item_rec['id']]=new c_menu_panel_structure_item($item_rec,$child_clss) ;
                  $this->menu[$this->cur_rec['__href_window'].'adding/']='Добавить' ;
               }

   }


    

}
?>