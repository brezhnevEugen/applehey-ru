<?php
include_once(_DIR_TO_MODULES.'/reports/i_report.php') ;
class report_stats2 extends c_report
{
 var $title='Статистика по загрузке фото' ;
 var $view_by_space_filter=1 ;
 var $list_accounts=array() ;
 var $link_objs='' ;

 function __construct($create_options=array())
 { parent::__construct($create_options);
   //if (MEMBER()->rol==4) $this->link_objs=get_linked_ids_to_rec(MEMBER()->info) ;
 }

 function setting($options=array())
 {   $data_info=execSQL_van('select min(c_data) as min_data,max(c_data) as max_data from obj_site_stats_uploads') ;
       // damp_array($data_info) ;
      $t=$data_info['min_data'] ;
      while($t<$data_info['max_data'])
      {   $ts=getdate($t) ;
          $cur_month=mktime(0,0,0,$ts['mon'],1,$ts['year']) ;
          $arr[$cur_month]=get_month_year($cur_month,'month year') ;
          $t=mktime(0,0,0,$ts['mon']+1,1,$ts['year']) ;
      }
     $arr=array_reverse($arr,true) ;
     if (!$this->filter['month']) $this->filter['month']=$cur_month ;


     ?><div id="panel_filter" class="one_row">
             <form id="sky-form" class="sky-form"><input type="hidden" name="filter_dir" value="<?echo ($options['filter_dir'])? $options['filter_dir']:_CUR_REPORT_DIR?>">
                  <fieldset>
                      <div class="row">
                          <section class="col col-2">
                              <label class="label">Месяц:</label>
                              <label class="select">
                                  <select name="filter[month]">
                                      <?foreach($arr as $t=>$month){?><option value="<?echo $t?>" <?if ($this->filter['month']==$t) echo 'selected'?>><?echo $month?></option><?}?>
                                  </select>
                              </label>
                          </section>
                        </div>
                  </fieldset>
                  <div id="panel_buttons"><input type="submit" class="button v2" cmd="set_filter" value="OK"></div>
              </form>
       </div><br>
     <?
  }


 function panel_info_filter()
 { $_str=array() ; $info=array() ; $title='' ;
   if ($this->filter['month'])                            $info['Месяц']=get_month_year($this->filter['month'],'month year') ; ;
   if (sizeof($info)) foreach($info as $title=>$value) $_str[]=$title.': <strong>'.$value.'</strong>' ;
   if (sizeof($_str)) $title='<p class=center>'.implode(' ',$_str).'</p>' ;
   return $title ;
 }

// шаблон - вывод ссылками без разделитея
  function print_template_HTML($list_recs,$options=array())
  { global $member ;
    $id=($options['id'])? 'id='.$options['id']:'id=report_clients' ;
    $this->panel_info_select($options) ;
    ?><table class="basic fz_small full auto " <?echo $id?>>
        <tr><th>ID</th><th><i class="fa fa-flag"></i></th><th>Фото</th><th>ФИО</th><th>Последнее посещение</th><th>Операции</th></tr><? ;
        if (sizeof($list_recs)) foreach($list_recs as $rec)
        { ?><tr class="item rec_<?echo $rec['pkey']?>">
                <td class="id"><?echo $rec['pkey']?></td>
                <td class="сenter"><span id=fav_<?echo $rec['pkey']?> class="favorite <?if ($rec['favorite']) echo 'active'?> v2" cmd="personal/change_vaforite" client_id="<? echo $rec['pkey']?>"><i class="fa fa-flag"></i></span></td>
                <td class="center"><a  href="<?echo $rec['__href']?>"><img src="<?echo img_clone($rec,'small')?>"></a></td>
                <td class="left"><a href="<?echo $rec['__href']?>"><?echo $rec['obj_name']?></a>
                    <?if ($rec['typ']>1) echo '<div class="client_type"><span class="a_'.$rec['typ'].'">'.$_SESSION['IL_cat_clients'][$rec['typ']]['obj_name'].'</span></div>'?>
                    <div class="doc_info"><?echo $_SESSION['IL_docs'][$rec['doc']]['obj_name'].' <strong>'.$rec['pass1'].' '.$rec['pass2'].'</strong>'?></div>

                </td>
                <td><div id="check_info_<?echo $rec['pkey']?>">
                        <?if ($rec['check_data']) echo date('d.m.Y H:i',$rec['check_data']).'<br>'.ACCOUNTS()->get_member_name_by_id($rec['check_account'])?></div><br>
                        <button class="v2" cmd="personal/set_member" member_id="<?echo $rec['pkey']?>">Посетил</button>
                        <br><br><a href="" class="v2" cmd="personal/get_last_pos" client_id="<?echo $rec['pkey']?>">Последние посещения объектов</a>
                </td>
                <td class="center"><button class="button_small edit v1" href="<?echo $rec['__href']?>" title="Перейти на страницу редактирования клиента">Редактировать</button>
                                                 <?if ($member->info['status']){?><button class="button_small delete v2" cmd=personal/delete_member member_id="<?echo $rec['pkey']?>" title="Удалить клиента из базы">Удалить</button><?}?>
                                                 <button class="button_small copy" data-clipboard-text="<?echo 'http://'._MAIN_DOMAIN.$rec['__href']?>"  title="Скопировать в буфер обмена адрес страницы клиента">Скопировать URL</button>
                </td>
            </tr><?
        }
      ?></table>
        <style type="text/css">
            table#report_clients{}
            table#report_clients div.client_type{}
            table#report_clients div.client_type{padding:5px 0;}
            table#report_clients div.client_type span.a_10{background:red;color:white;padding:5px;}
            table#report_clients div.doc_info{}
        </style>

      <?
      $this->panel_info_select($options) ;
  }

    function view_as_HTML($options=array())
    {   if (!$this->no_view_title){?><h2 class="center"><?echo $this->title?></h2><?}
        echo $this->panel_info_filter() ;
        $_usl=array() ;
        // формируем условия
        if ($this->filter['month'])
        { $time_from=$this->filter['month'] ;
          $ts=getdate($time_from) ;
          $time_to=mktime(0,0,0,$ts['mon']+1,1,$ts['year'])-1 ;
          $_usl[]='c_data>='.$time_from;
          $_usl[]='c_data<='.$time_to;
          //echo 'time_from='.date('d.m.Y H:i:s',$time_from).'<br>$time_to ='.date('d.m.Y H:i:s',$time_to) ;
        }
        $usl_select=(sizeof($_usl))? ' and '.implode(' and ',$_usl):'' ;

        $this->panel_info_select($options) ;

        ?><table class="basic fz_small full fixed"><tr><th>Фотограф</th><th>Прикреплено фото - Товары в продаже</th><th>Прикреплено фото - Снято с продажи</th><th>Товар не найден</th><th>Принято</th><th>Отклонено</th></tr><?

            if (sizeof($_SESSION['arr_photograf_names']))foreach($_SESSION['arr_photograf_names'] as $id=>$name)
            {
                $usl='member_id='.$id.$usl_select ;
                $cnt_upload_sucses_enabled=execSQL_value('select count(pkey) from obj_site_stats_uploads where '.$usl.' and event_id=3 and result_code in (1,10) and goods_enabled=1') ; // Прикрепление изображения к товару, Успешно
                $cnt_upload_sucses_disabled=execSQL_value('select count(pkey) as cnt from obj_site_stats_uploads where '.$usl.' and event_id=3 and result_code in (1,10) and goods_enabled=0') ; // Прикрепление изображения к товару, Успешно
                $cnt_upload_not_found=execSQL_value('select count(pkey) as cnt from obj_site_stats_uploads where '.$usl.' and event_id=2 and result_code=9') ; // Загрузка файла JPG, Товар не найден
                $cnt_upload_allow=execSQL_value('select count(pkey) as cnt from obj_site_stats_uploads where '.$usl.' and event_id=3 and result_code=1') ; // Прикрепление изображения к товару, принято
                $cnt_upload_blocked=execSQL_value('select count(pkey) as cnt from obj_site_stats_uploads where '.$usl.' and event_id=3 and result_code=10') ; // Прикрепление изображения к товару, отклонено
                ?><tr>
                    <td class="left"><?echo $name?></td>
                    <td><?if ($cnt_upload_sucses_enabled) echo $cnt_upload_sucses_enabled?></td>
                    <td><?if ($cnt_upload_sucses_disabled) echo $cnt_upload_sucses_disabled?></td>
                    <td><?if ($cnt_upload_not_found) echo $cnt_upload_not_found?></td>
                    <td><?if ($cnt_upload_allow) echo $cnt_upload_allow?></td>
                    <td><?if ($cnt_upload_blocked) echo $cnt_upload_blocked?></td>
                  </tr>
                <?
            }

        ?></table><?
    }

}

?>