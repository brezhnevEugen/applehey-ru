<?
//include_once('messages.php');
include(_DIR_TO_ENGINE.'/c_page_XML_AJAX.php') ;
include(_DIR_TO_ENGINE.'/admin/i_admin_system.php') ;
class c_page_ajax extends c_page_XML_AJAX
{
  function set_log_filter()
  {   global $member ;
      //$this->add_element('show_modal_window','HTML',array('title'=>'Прикрепление изображений к товарам','after_close_update_page'=>1)) ;
      //damp_array($_POST) ;

      $_SESSION['log_filter'][$_POST['go_page']]['member_id']=$_POST['member_id'] ;
      $_SESSION['log_filter'][$_POST['go_page']]['event_id']=$_POST['event_id'] ;
      $_SESSION['log_filter'][$_POST['go_page']]['result_id']=$_POST['result_id'] ;
      $_SESSION['log_filter'][$_POST['go_page']]['time_from']=strtotime($_POST['time_from']) ;
      $_SESSION['log_filter'][$_POST['go_page']]['time_to']=strtotime($_POST['time_to'].' 23:59:59') ;
      $_SESSION['log_filter'][$_POST['go_page']]['goods_id']=$_POST['goods_id'] ;
      $_SESSION['log_filter'][$_POST['go_page']]['kadr']=$_POST['kadr'] ;
      $_SESSION['log_filter'][$_POST['go_page']]['group_type']=$_POST['group_type'] ;
      $_SESSION['log_filter'][$_POST['go_page']]['status_photo']=$_POST['status_photo'] ;
      $_SESSION['log_filter'][$_POST['go_page']]['img_name']=$_POST['img_name'] ;
      $go_page=($_POST['go_page']!='/')? $_POST['go_page'].'/':'' ;
      if ($member->info['status']==2) $this->add_element('go_url','/cabs/photo/'.$go_page) ;
      if ($member->info['status']==3) $this->add_element('go_url','/cabs/admin/'.$go_page) ;

  }

  function clear_log_filter()
  {
      $_SESSION['log_filter']=array() ;
      if ($_POST['go_url']) $this->add_element('go_url',$_POST['go_url']) ;
      else                  $this->add_element('update_page',1) ;
  }

  function upload_photo_to_goods()
  { $this->add_element('show_modal_window','HTML',array('title'=>'Прикрепление изображений к товарам','after_close_update_page'=>1)) ;
    $this->add_element('after_close_update_page',1) ;
    $_SESSION['photo_to_upload']=array() ;
    if (sizeof($_POST['img'])) foreach($_POST['img'] as $goods_id=>$img_arr) if (sizeof($img_arr)) foreach($img_arr as $indx=>$img_name) $_SESSION['photo_to_upload'][]=array('goods_id'=>$goods_id,'img_name'=>$img_name) ;
    $_SESSION['photo_to_upload_indx']=0 ;

    ?><div id="upload_photo_to_goods">
        <p>Примерное время обработки составит <?echo 2*sizeof($_SESSION['photo_to_upload'])?> секунд</p>
        <p id="process_info">Обработано <strong id="count_img">0</strong> из <strong><?echo sizeof($_SESSION['photo_to_upload']) ?></strong> изображений</p>

     <?

    ?><div id="script_div"><script type="text/javascript">$j(document).ready(function(){$j('div#upload_photo_to_goods').oneTime(1000,'start_upload',function(){send_ajax_request({cmd:'cabs/upload_next_photo_to_goods'});})});</script></div>
      <div id="textarea_upload_status"></div>
      <div id="upload_photo_to_goods_buttons" class=buttons></div>

    </div>
     <?
  }

  function block_foto()
  {  global $member ;
     //$this->add_element('show_modal_window','HTML') ;
     //damp_array($_POST) ;
     $stats=execSQL_van('select * from obj_site_stats_uploads where pkey="'.$_POST['stat_id'].'"') ;
     $comment=($stats['obj_name'])? $stats['obj_name'].'<br>':'' ;
     $comment=$comment.'['.date('d.m.Y H:i',time()).'] Отклонено администратором '.$member->name ;
     update_rec_in_table('obj_site_stats_uploads',array('result_code'=>10,'obj_name'=>$comment),'pkey='.$_POST['stat_id']) ;
     update_rec_in_table('obj_site_goods_image',array('enabled'=>0),'parent='.$stats['goods_id'].' and obj_name="'.$stats['img_name'].'"') ;
     $img_rec=execSQL_van('select pkey,obj_name from obj_site_goods_image where parent='.$stats['goods_id'].' and obj_name="'.$stats['img_name'].'"');
     // обновляем поле "главное фото" у товара
     _CLSS(3)->after_save_rec($img_rec['tkey'],$img_rec['pkey']) ;
     ob_start() ;
     ?><script type="text/javascript">
        $j('div#st_<? echo $_POST['stat_id']?>').removeClass('status_1').addClass('status_10');
        $j('div#st_<? echo $_POST['stat_id']?> div.action').attr('cmd','cabs/unblock_foto');
     </script><?
     $text=ob_get_clean() ;
     $this->add_element('JSCODE',$text) ;
     $this->add_element('show_nofife','Фото отклонено') ;
     $rec_photo=execSQL_van('select * from obj_site_goods_image where parent='.$stats['goods_id'].' and obj_name="'.$stats['img_name'].'"') ;
     _event_reg('Фото отклонено',$rec_photo['obj_name'],$rec_photo['_reffer'],$rec_photo['_parent_reffer'],$stats['_reffer']) ;
  }

  function unblock_foto()
  {  global $member ;
     //$this->add_element('show_modal_window','HTML') ;
     //damp_array($_POST) ;
     $stats=execSQL_van('select * from obj_site_stats_uploads where pkey="'.$_POST['stat_id'].'"') ;
     $comment=($stats['obj_name'])? $stats['obj_name'].'<br>':'' ;
     $comment=$comment.'['.date('d.m.Y H:i',time()).'] Принято администратором '.$member->name ;
     update_rec_in_table('obj_site_stats_uploads',array('result_code'=>1,'obj_name'=>$comment),'pkey='.$_POST['stat_id']) ;
     update_rec_in_table('obj_site_goods_image',array('enabled'=>1),'parent='.$stats['goods_id'].' and obj_name="'.$stats['img_name'].'"') ;
     $img_rec=execSQL_van('select pkey,obj_name from obj_site_goods_image where parent='.$stats['goods_id'].' and obj_name="'.$stats['img_name'].'"');
     // обновляем поле "главное фото" у товара
     _CLSS(3)->after_save_rec($img_rec['tkey'],$img_rec['pkey']) ;
     ob_start() ;
     ?><script type="text/javascript">
      $j('div#st_<? echo $_POST['stat_id']?>').removeClass('status_10').addClass('status_1');
      $j('div#st_<? echo $_POST['stat_id']?> div.action').attr('cmd','cabs/block_foto');
     </script><?
     $text=ob_get_clean() ;
     $this->add_element('JSCODE',$text) ;
     $this->add_element('show_nofife','Фото принято') ;
     $rec_photo=execSQL_van('select * from obj_site_goods_image where parent='.$stats['goods_id'].' and obj_name="'.$stats['img_name'].'"') ;
     _event_reg('Фото принято',$rec_photo['obj_name'],$rec_photo['_reffer'],$rec_photo['_parent_reffer'],$stats['_reffer']) ;

  }

  function  upload_next_photo_to_goods()
  {  global $member,$goods_system ;
     if ($_SESSION['photo_to_upload_indx']<sizeof($_SESSION['photo_to_upload']))
     {  ob_start() ;
        $cur_img_name=$_SESSION['photo_to_upload'][$_SESSION['photo_to_upload_indx']]['img_name'] ;
        $goods_id=$_SESSION['photo_to_upload'][$_SESSION['photo_to_upload_indx']]['goods_id'] ;
        $goods_status=execSQL_value('select enabled from obj_site_goods where pkey='.$goods_id) ;
        // загружаем фото $cur_img_name в товар
        list($name,$ext)=explode('.',$cur_img_name) ;
        list($kadr,$number)=explode('-',$name) ;
        if (!$number) $number=0 ;
        $cur_img_dir=_DIR_TO_ROOT.'/private/'.$member->id.'/'.$cur_img_name ;

        $goods_name=$_SESSION['uploaded_kadr_info'][$kadr][$goods_id]['goods_name'] ;
        $sortament_color=$_SESSION['uploaded_kadr_info'][$kadr][$goods_id]['color'] ;

        $rec_exist_img=execSQL_van('select pkey,file_name from obj_site_goods_image where parent='.$goods_id.' and (obj_name="086A'.$cur_img_name.'" or obj_name="086a'.$cur_img_name.'" or obj_name="'.$cur_img_name.'")') ;
        if (!$rec_exist_img['pkey'])
            {  $goods_reffer=$goods_id.'.'.$goods_system->tkey ;
               obj_upload_image($goods_reffer,array('name'=>$cur_img_name,'upl_name'=>$cur_img_dir,'color'=>$sortament_color),array('img_indx'=>$number+1,'view_upload'=>0,'no_delete_source_img'=>1)) ;
               $result='Загружаем <strong>'.$cur_img_name.'</strong> в товар <strong>'.$goods_name.'</strong>' ;
               if (!$goods_status) $result.=', товар снят с продажи'  ;
               $result.='<br>'  ;
               $result_code=1  ;
            }
        else { $result='<div class=red>Фото <strong>'.$cur_img_name.'</strong> уже загружено в товар <strong>'.$goods_name.'</strong></div>' ; $result_code=8  ; }

        adding_rec_to_table('obj_site_stats_uploads',array(  'clss'=>212,
                                                             'member_id'=>$member->id,
                                                             'event_id'=>3,
                                                             'img_name'=>$cur_img_name,
                                                             'kadr'=>$kadr,
                                                             'goods_id'=>$goods_id,
                                                             'goods_name'=>$goods_name,
                                                             'goods_enabled'=>$goods_status,
                                                             'color'=>$sortament_color,
                                                             'result_text'=>$result,
                                                             'result_code'=>$result_code,
                                                             'file'=>$_SESSION['uploaded_kadr_info'][$kadr][$goods_id]['file'],
                                                             'line'=>$_SESSION['uploaded_kadr_info'][$kadr][$goods_id]['line']
                                                            )) ;

        $text=ob_get_clean()  ;
        $result.=$text ;

        $this->add_element('textarea_upload_status',$result,array('mode'=>'append')) ;
        $this->add_element('script_div.HTML','<script type="text/javascript">$j("div#upload_photo_to_goods").oneTime(1000,"start_upload",function(){send_ajax_request({cmd:"cabs/upload_next_photo_to_goods"});});</script>') ;
        $_SESSION['photo_to_upload_indx']++ ;
        $this->add_element('count_img.HTML',$_SESSION['photo_to_upload_indx']) ;
     }
     else
     {
        $this->add_element('process_info.HTML','<strong>Загрузка и обработка завершена</strong>') ;
        $this->add_element('upload_photo_to_goods_buttons.HTML','<button class="cancel">Закрыть окно</button>') ;
        // удаляем исходники фото
        if (sizeof($_SESSION['photo_to_upload'])) foreach($_SESSION['photo_to_upload'] as $info) unlink(_DIR_TO_ROOT.'/private/'.$member->id.'/'.$info['img_name']) ;
     }





     //if (sizeof($_POST['img'])) foreach($_POST['img'] as $kadr=>$arr_indx)
     //  if (sizeof($arr_indx)) foreach($arr_indx as $indx=>$img_name)

  }

  function clear_cabinet()
  {   global $member ;
      $member_dir=_DIR_TO_ROOT.'/private/'.$member->id.'/' ;
      $list=get_files_list($member_dir,array()) ;
      if (sizeof($list)) foreach($list as $fname) if (is_file($fname)) unlink($fname) ;
      $this->update_page=1 ;
      adding_rec_to_table('obj_site_stats_uploads',array(  'clss'=>212,
                                                           'member_id'=>$member->id,
                                                           'event_id'=>4
                                                           )) ;
  }

  function check_goods_code()
  { global $goods_system,$member ;
    $rec_sort=execSQL_van('select * from '.TM_SORTAMENT.' where code="'.$_POST['code'].'"') ;
    if ($rec_sort['pkey'])
    { $rec_goods=get_obj_info($rec_sort['goods_id'].'.136') ;
      if ($rec_goods['pkey'])
      {  $goods_system->prepare_public_info($rec_goods) ;
         ob_start() ;
         ?><h2>Фото товара</h2><?
         $recs_goods=array() ;
         $recs_goods[$rec_goods['pkey']]=$rec_goods ;
         print_template($recs_goods,'list_goods_image_upload',array('panel_select_pages'=>'top','member_id'=>$member->id,'no_block'=>1)) ;
         ?><h2>Карточка товара</h2><?
         include_once(_DIR_TO_CLASS.'/c_catalog_goods.php') ;
         $page=new c_page_catalog_goods() ;
         $page->panel_goods_item($rec_goods,array('no_tab_support'=>1,'no_tab_responses'=>1,'no_razmer_info'=>1,'no_buy_buttons'=>1)) ;
         $text=ob_get_clean() ;
         $this->add_element('panel_goods_info.HTML',$text) ;
         ob_start() ;

         ?><form><strong>Номер кадра</strong><br>
           <input type="text" name="kadr" value=""> <input type="submit" class=v2 cmd=cabs/set_kadr_to_code code="<?echo $rec_sort['code']?>" value="Сохранить"></form>
           <div id="panel_set_kadr_alert" class="float_left"></div><div class="clear"></div>
           <script type="text/javascript">$j('input[name="kadr"]')[0].focus() ;</script>

           <h2>Товар по штрих-коду "<?echo $rec_sort['code']?>"</h2>
           <table class=list_items_sortament>
           <colgroup><col id="c1"><col id="c2"><col id="c3"><col id="c4"><col id="c5"></colgroup>
           <tr><th>Штрих-код</th><th>Артикул</th><th>Наименование</th><th>Цвет</th><th>Размер</th><th>Сезон</th></tr><?
               { ?><tr>
                      <td class=code><? echo $rec_sort['code']?></td>
                      <td class=code><? echo $rec_goods['art']?></td>
                      <td class=code><? echo $rec_goods['obj_name']?></td>
                      <td class=color><? echo $_SESSION['IL_colors'][$rec_sort['color']]['obj_name']?></td>
                      <td class=size><? echo $rec_sort['size']?></td>
                      <td class=sezon><? echo $rec_sort['sezon']?></td>
                    </tr>
                 <?
               }
             ?></table><br>
               <h2>Кадры для товара "<?echo $rec_goods['obj_name']?>"</h2>
            <? $recs_kadr=execSQL('select * from obj_site_kadr where goods_id='.$rec_goods['pkey']) ;
               if (sizeof($recs_kadr))
                  { ?><table class=list_items_sortament><colgroup><col id="c1"><col id="c2"><col id="c3"><col id="c4"><col id="c5"></colgroup>
                      <tr><th>Дата добавления</th><th>Кадр</th><th>Фотограф</th><th>Сортамент</th><th>Цвет</th><th>Размер</th><th>Сезон</th></tr>
                    <?
                    foreach($recs_kadr as $rec_kadr) { $rec_sort2=execSQL_van('select * from '.TM_SORTAMENT.' where pkey="'.$rec_kadr['sortament_id'].'"') ;
                                                       $rec_member=execSQL_van('select pkey,obj_name from obj_site_account where pkey="'.$rec_kadr['member_id'].'"') ;

                                                        ?><tr><td><?echo date('d.m.Y h:i',$rec_kadr['c_data'])?></td>
                                                            <td><?echo $rec_kadr['kadr']?></td>
                                                            <td><?echo $rec_member['obj_name']?></td>
                                                            <td><?echo $rec_sort2['code']?></td>
                                                            <td class=color><? echo $_SESSION['IL_colors'][$rec_sort2['color']]['obj_name']?></td>
                                                            <td class=size><? echo $rec_sort2['size']?></td>
                                                            <td class=sezon><? echo $rec_sort2['sezon']?></td>
                                                        </tr><?
                                                     }
                    ?></table><?
                  } else {?>Не задано ни одного кадра для данного товара<?}
         $text=ob_get_clean() ;
         $this->add_element('result_check_goods_code.HTML',$text) ;
      }

    }
    else
    {   $this->add_element('result_check_goods_code.HTML','<div class="red">Штрих-код отстствует в базе</div>') ;
        $this->add_element('panel_goods_info.HTML','') ;
        ob_start() ;
        ?><script type="text/javascript">
           $j('input[name="code"]').val('') ;
           $j('input[name="code"]')[0].focus() ;
         </script><?
        $text=ob_get_clean() ;
        $this->add_element('JSCODE',$text) ;
    }
  }

  function set_kadr_to_code()
  { global $member ;
    $_POST['kadr']=trim($_POST['kadr']) ;
    if (!$_POST['code']) return ;

    $rec_sort=execSQL_van('select * from '.TM_SORTAMENT.' where code="'.$_POST['code'].'"') ;
    if ($rec_sort['pkey'])
    { $rec_goods=get_obj_info($rec_sort['goods_id'].'.136') ;
      if ($rec_goods['pkey'])
      {   $rec=execSQL_van('select * from obj_site_kadr where member_id="'.$member->id.'" and kadr="'.$_POST['kadr'].'"') ;
          if (!$rec['pkey'])
          { adding_rec_to_table('obj_site_kadr',array('clss'=>215,'member_id'=>$member->id,'code'=>$rec_sort['code'],'kadr'=>$_POST['kadr'],'goods_id'=>$rec_goods['pkey'],'sortament_id'=>$rec_sort['pkey'])) ;
            $this->add_element('result_check_goods_code.HTML','<div class="green">СОХРАНЕНО</div>') ;
            $this->add_element('panel_goods_info.HTML','') ;
            ob_start() ;
            ?><script type="text/javascript">
               $j('input[name="code"]').val('') ;
               $j('input[name="code"]')[0].focus() ;
             </script><?
            $text=ob_get_clean() ;
            $this->add_element('JSCODE',$text) ;
            $result_code=1 ;
            $result='Сохранено для товара <strong>'.$rec_goods['obj_name'].'</strong> ['.$rec_sort['code'].']' ;

          }
          else
          { $this->add_element('panel_set_kadr_alert.HTML','<div class="alert">Данный номер кадра уже задан для товара <strong>'.$rec_goods['obj_name'].'</strong> ['.$rec_sort['code'].']</div>') ;
            $result_code=11 ;
            $result='Данный номер кадра уже задан для товара <strong>'.$rec_goods['obj_name'].'</strong> ['.$rec_sort['code'].']' ;
          }

          adding_rec_to_table('obj_site_stats_uploads',array(  'clss'=>212,
                                                               'member_id'=>$member->id,
                                                               'event_id'=>5,
                                                               'kadr'=>$_POST['kadr'],
                                                               'goods_id'=>$rec_goods['pkey'],
                                                               'goods_name'=>$rec_goods['obj_name'],
                                                               'goods_enabled'=>$rec_goods['enabled'],
                                                               'color'=>$rec_sort['color'],
                                                               'result_text'=>$result,
                                                               'result_code'=>$result_code
                                                              )) ;
      }
    }


  }

  function get_tr_table_of_kadr()
  {   if (isset($_SESSION['uploaded_kadr_info'][$_POST['kadr']])) return ;
      ob_start() ;
      include_once(_DIR_EXT.'/angel-discount/import_kadr.php') ;
      global $member ;
      $member_dir=_DIR_TO_ROOT.'/private/'.$member->id.'/' ;
      import_from_table_of_kadr($member->id,$member_dir,$_POST['kadr']) ;
      $text=ob_get_clean() ;
      $this->add_element('table_of_kadr.APPEND',$text) ;

  }

}

