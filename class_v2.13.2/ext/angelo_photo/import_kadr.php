<?
//include_once(_DIR_EXT.'/angel-discount/sync_1c.php')  ;

/*=====================================================================================================================================================================================================================================
 *
 * ИМПОРТ описаний из EXCEL
 *
 *=====================================================================================================================================================================================================================================*/

 function import_info_from_excel()
 {
   /*
   global $list_photo2 ;
   $list_photo2=array() ;
   $list_photo=get_files_list(_DIR_TO_ROOT.'/photo2/') ;
   if (sizeof($list_photo)) foreach($list_photo as $id=>$file_name)
   { $img_name=str_replace(array('086A','.jpg'),'',basename($file_name)) ;
     $list_photo2[$img_name]=$file_name  ;
   } */

   $list=get_files_list(_DIR_TO_ROOT.'/1c/1cbitrix/',array('ext_pattern'=>'xls')) ;
   echo 'Найдено '.sizeof($list).' файлов для обработки<br>' ;
   $i=0 ;
   if (sizeof($list)) foreach($list as $fdir)
   { ?><h2>Импортируем файл <?echo basename($fdir)?></h2><?
     import_from_excel_parsing_file($fdir) ;
     rename($fdir,str_replace('1cbitrix','1cbitrix/checked_xls',$fdir)) ;
     $i++ ;
     if ($i==40) break ;
   }

   /*
   //$list=get_files_list(_DIR_TO_ROOT.'/1c/old_format/',array('ext_pattern'=>'xls')) ;
   //if (sizeof($list)) foreach($list as $fdir)
   //{ ?><h2>Импортируем файл <?echo basename($fdir)?></h2><?
   //  import_from_excel_parsing_file_old($fdir) ;
   //}  */
 }


function import_from_excel_parsing_file($xls_file_name)
 { //global $list_photo2 ;
   set_include_path(_DIR_EXT.'/PHPExcel/');
   if (!file_exists(_DIR_EXT.'/PHPExcel/')) {echo 'Расширение PHPExcel не установлено '; return ; } ;
   set_time_limit(0) ;
   include_once 'PHPExcel/IOFactory.php';

   execSQL_update('delete from obj_site_stats_kadr where file="'.basename($xls_file_name).'"') ;

   $objPHPExcel = PHPExcel_IOFactory::load($xls_file_name);
   $objPHPExcel->setActiveSheetIndex(0);
   // начинаем заполняем с корневого раздела
   $res_arr=array() ; $arr_row_names=array() ;   $update_goods=0 ; $update_sortament=0 ; $cnt_kadr=0 ;   $cnt_no_photo=0 ;  $cnt_no_code=0 ;
   // читаем активную страницу
   $aSheet = $objPHPExcel->getActiveSheet();
    if (is_object($aSheet))
    {   $rowIterator = $objPHPExcel->getActiveSheet()->getRowIterator();
        foreach($rowIterator as $row)
        { $cellIterator = $row->getCellIterator();
          $cellIterator->setIterateOnlyExistingCells(false); // Loop all cells, even if it is not set
          $cur_row=$row->getRowIndex() ;
          $rec_goods=array() ;  $sortament_data=array() ; $rec_stat=array() ; $arr_img=array() ; $art='' ; $code='' ;
          $result=array('file'=>'/new/'.basename($xls_file_name).':'.$cur_row) ;
          $rec_stat['clss']=207 ;
          $rec_stat['file']=basename($xls_file_name) ;
          $rec_stat['line']=$cur_row ;
          foreach ($cellIterator as $cell)
            { $value=$cell->getValue(); //$value=$cell->getCalculatedValue();
              if ($cur_row==1) $arr_row_names[$cell->getColumn()]=str_replace("\n",'',$value) ;  // первая строка - заголовки
              else
              { $res_arr[$cur_row][$arr_row_names[$cell->getColumn()]]=$value ;
                  $col_name=trim($arr_row_names[$cell->getColumn()]);
                  switch($col_name)
                  { case 'N':             break ;
                    case '№':             break ;
                    case 'Номенклатура':  $rec_goods['obj_name']=$value ; // $arr=explode(',',$value) ; $rec_goods['obj_name']=$arr[0] ;
                                          break ;
                    case 'Характеристика номенклатуры': { $arr=explode(',',$value) ; $sortament_data['size']=$arr[0] ; }   break ;
                    case 'Артикул':       $art=$value ;    break ;
                    case 'Штрихкод':      $code=trim($value) ;    break ;
                    case 'Номер кадра':   $sortament_data['kadr']=$value ;
                                          /*  просто фиксируем номер кадра, изображене не проверяем
                                          if ($value)
                                          {   if (isset($list_photo2[$value]))
                                              { $rec_stat['img_name']=basename($list_photo2[$value]) ;
                                                //$arr_img[]=$list_photo2[$value] ;
                                                //$res_arr[$cur_row]['img'][]='<img width=150 src=/photo2/'.basename($list_photo2[$value]).'>' ;

                                              }
                                              else
                                              { if ($value) echo '<div class=black>'.$cur_row.': '.$rec_goods['obj_name'].', - кадр '.$sortament_data['kadr'].' - фото нет</div>' ;
                                                $cnt_no_photo++ ;
                                              }
                                          } */
                                          break ;
                    case 'Доп характеристики':   if ($value) $rec_goods['comment']=$value ;    break ;
                    case 'Состав':        if ($value) $rec_goods['sostav']=$value ;    break ;

                    case 'Раздел':        //if ($value and !isset($types_level_1[$value])) $types_level_1[$value]=adding_rec_to_table('obj_site_category',array('clss'=>60,'parent'=>1,'obj_name'=>$value)) ;
                                          //$id_type_1=$types_level_1[$value] ;
                                          //$rec['type']=$id_type_1 ; //  основной тип товара
                                          break ;

                    case 'Группа товаров'://if ($value=='Куртка') $value='Куртки' ;
                                          //if ($value=='Жилет') $value='Жилеты' ;
                                          //if ($value=='Платье') $value='Платья' ;
                                          //if ($value and !isset($types_level_2[$value])) $types_level_2[$value]=adding_rec_to_table('obj_site_category',array('clss'=>60,'parent'=>$id_type_1,'obj_name'=>$value)) ;
                                          //$id_type_2=$types_level_2[$value] ;
                                          //$rec['parent']=1 ;
                                          //$rec['type']=$id_type_2 ; // уточняем тип
                                          break ;
                    case 'Стиль':         if ($value=='Городской шик3') $value='Городской шик' ;
                                          if ($value=='городской шик') $value='Городской шик' ;
                                          if ($value) $rec_goods['style']=_IL('IL_style')->get_id_of_value($value,array('append_value_is_not_found'=>1)) ;
                                          break ;
                    case 'Реальный цвет': if ($value)
                                          {   $value=mb_convert_case(trim($value),MB_CASE_LOWER) ;
                                              $sortament_data['color']=_IL('IL_colors')->get_id_of_value($value,array('append_value_is_not_found'=>1)) ;
                                          }
                                          break ;
                    case 'Категория':     if ($value) $sortament_data['categor']=_IL('IL_categor')->get_id_of_value(check_categor_value($value),array('append_value_is_not_found'=>1)) ;
                                          break ;

                    default: echo '<div class=red>Нераспознаыый столбец: '.$col_name.' = '.$value.'</div>' ;

                  }
              }
            }
          $rec_stat['kadr']=$sortament_data['kadr'] ;
          $rec_stat['code']=$code ;
          $rec_stat['art']=$art ;
          $rec_stat['comment']=$rec_goods['comment'] ;
          $rec_stat['sostav']=$rec_goods['sostav'] ;
          $rec_stat['style_id']=$rec_goods['style'] ;
          $rec_stat['color_id']=$sortament_data['color'] ;
          $rec_stat['categor_id']=$sortament_data['categor'] ;
          //damp_array($rec_goods,1,-1) ;
          //damp_array($sortament_data,1,-1) ;

          if ($code)
          {   $sortament_rec=execSQL_van('select pkey,parent from '.TM_SORTAMENT.' where code="'.$code.'"') ;
              $rec_stat['sortament_id']=$sortament_rec['pkey'] ;
              $rec_stat['goods_id']=$sortament_rec['parent'] ;
              $result['code']=$code ;
              if ($sortament_rec['pkey'])
              { //update_rec_in_table(TM_SORTAMENT,$sortament_data,'pkey='.$sortament_rec['pkey']) ;
                $update_sortament++ ;
                $result['sortament_id']=$sortament_rec['pkey'] ;
                $result['goods_id']=$sortament_rec['parent'] ;
                $result['art']=$art ;
                // обновляем  запись товара сортамента
                $goods_rec=execSQL_van('select pkey,enabled from obj_site_goods where pkey="'.$sortament_rec['parent'].'"') ;
                $rec_stat['goods_enabled']=$goods_rec['enabled'] ;
                //update_rec_in_table('obj_site_goods',$rec_goods,'pkey='.$sortament_rec['parent']) ; $update_goods++ ;
                /*
                if (sizeof($arr_img)) foreach($arr_img as $img_dir)
                  {   $file_name=basename($img_dir) ;
                      $rec_img=execSQL_van('select pkey,file_name from obj_site_goods_image where clss=3 and file_name like "%_'.$file_name.'" and parent="'.$sortament_rec['parent'].'"') ;
                      if (!$rec_img['pkey'])
                      { $img_reffer=obj_upload_image($sortament_rec['parent'].'.136',array('name'=>basename($img_dir),'upl_name'=>$img_dir,'color'=>$sortament_data['color']),array('view_upload'=>0,'no_delete_source_img'=>1)) ;
                        echo '<div class=grren>'.$rec_goods['obj_name'].', - кадр '.$sortament_data['kadr'].' - изображение загружено</div>' ;
                        list($rec_img['pkey'],$tkey)=explode('.',$img_reffer) ;
                      }
                      if ($sortament_data['color']) update_rec_in_table('obj_site_goods_image',array('color'=>$sortament_data['color']),'pkey='.$rec_img['pkey']) ;
                      //else echo 'Изображение '.$file_name.' уже загружено<br>' ;
                  }
               */
              }
              else
              { echo '<div class=red>'.$cur_row.': Не найден сортамент штрих-код '.$code.'</div>' ;
                $cnt_no_code++ ;
              }
          }


          //if ($sortament_data['kadr'])  $cnt_kadr++ ;

          if ($rec_stat['code'] and $rec_stat['art']) adding_rec_to_table('obj_site_stats_kadr',$rec_stat) ;
        }
        //print_2x_arr($res_arr) ;
        ?><div>Обработано <strong><? echo $cur_row ?></strong> строк файла</div><?
        ?><div>Проверено <strong><? echo $cnt_kadr ?></strong> записей по товарам</div><?
        ?><div>Обновлено товаров <strong><? echo $update_goods ?></strong> позиций</div><?
        ?><div>Обновлено сортаментов товаров <strong><? echo $update_sortament ?></strong> позиций </div><?
        if ($cnt_no_photo){?><div>Не найдено изображений для заданных кадров <strong><? echo $cnt_no_photo ?></strong>  </div><?}
        if ($cnt_no_code) {?><div>Не найдено товаров по указанному штрих-коду <strong><? echo $cnt_no_code ?></strong>  </div><?}

        $_SESSION['sync_1c_result']['check_xls']++ ; // число проверенных прайс-листов
        $_SESSION['sync_1c_result']['kadr_xls_set_all']+=$update_sortament ; // задано кадров для сортамента
        $_SESSION['sync_1c_result']['kadr_xls_img_not_found']+=$cnt_no_photo ; // Не найдено изображений для заданных кадров
        $_SESSION['sync_1c_result']['kadr_xls_old_1C_code']+=$cnt_no_code ; // Не найдено товаров по указанному штрих-коду




    }
 }

function check_categor_value($value)
{ switch($value)
  { case 'Мужской': $value='Мужская' ; break ;
    case 'Женский': $value='Женские' ; break ;
    case 'женское': $value='Женские' ; break ;
    case 'Белье':   $value='Женские' ; break ;
    case 'Женская':   $value='Женские' ; break ;
    case 'Детская':   $value='Детские' ; break ;
  }
  return($value) ;

}


 /*парсинг XLS файла, загруженного в личный кабинет*/
 function import_from_excel_parsing_file_to_table($xls_file_name,$member_dir)
 { global $goods_system ;  //global $debug_db ; $debug_db=2 ;
   set_include_path(_DIR_TO_ROOT.'/AddOns/PHPExcel/');
   if (!file_exists(_DIR_TO_ROOT.'/AddOns/PHPExcel/')) {echo 'Расширение PHPExcel не установлено '; return(array('Расширение PHPExcel не установлено',true)) ; ; } ;
   set_time_limit(0) ;
   ?><form><?
   ob_start() ;
   include_once 'PHPExcel/IOFactory.php';

   $arr_img=array() ;
   $list=get_files_list($member_dir,array('ext_pattern'=>'jpg')) ;
   if (sizeof($list)) foreach($list as $file_dir)
      { list($name,$ext)=explode('.',basename($file_dir)) ;
        list($kadr,$number)=explode('-',$name) ;
        if (!$number) $number=0 ;
        $arr_img[$kadr][$number]=basename($file_dir) ;
        if (sizeof($arr_img[$kadr])>1) ksort($arr_img[$kadr]) ;
      }


   $objPHPExcel = PHPExcel_IOFactory::load($xls_file_name);
   $objPHPExcel->setActiveSheetIndex(0);
   // начинаем заполняем с корневого раздела
   $res_arr=array() ; $arr_row_names=array() ;   $update_goods=0 ; $update_sortament=0 ; $cnt_kadr=0 ;   $cnt_no_photo=0 ;  $cnt_no_code=0 ; $cnt_checked_photo=0 ; $arr_code=array() ;  $arr_goods_ids=array() ;  $goods_recs=array() ;
   // читаем активную страницу
   $aSheet = $objPHPExcel->getActiveSheet();
   ?><table class="report"><tr><th>Номер строки</th><th>Номер кадра</th><th>Шрих-код</th><th>Товар</th><th>Описание</th><th>Изображение для загрузки</th></tr><?
    if (is_object($aSheet))
    {   // первый проход - собираем все code со всех строк
        $rowIterator = $objPHPExcel->getActiveSheet()->getRowIterator();
        foreach($rowIterator as $row)
        { $cellIterator = $row->getCellIterator();
          $cellIterator->setIterateOnlyExistingCells(false); // Loop all cells, even if it is not set
          $cur_row=$row->getRowIndex() ;
          foreach ($cellIterator as $cell)
            { $value=$cell->getValue(); //$value=$cell->getCalculatedValue();
              if ($cur_row==1) $arr_row_names[$cell->getColumn()]=str_replace("\n",'',$value) ;  // первая строка - заголовки
              else
              { $res_arr[$cur_row][$arr_row_names[$cell->getColumn()]]=$value ;
                  $col_name=trim($arr_row_names[$cell->getColumn()]);
                  if ($col_name=='Штрихкод') $arr_code[]='"'.trim($value).'"'  ;
              }
            }

        }

        //damp_array($arr_code) ;
        // получаем сразу все необходмые данные
        $sortament_recs=execSQL('select * from '.TM_SORTAMENT.' where code in ('.implode(',',$arr_code).')',array('use_fname_indx'=>'code')) ;
        if (sizeof($sortament_recs))
        { foreach($sortament_recs as $rec) $arr_goods_ids[]=$rec['goods_id'] ;
          $goods_ids=implode(',',$arr_goods_ids) ;
          $goods_recs=execSQL('select * from '.TM_GOODS.' where pkey in ('.$goods_ids.')') ;
          $rec_imgs=execSQL('select * from obj_site_goods_image where parent in ('.$goods_ids.') order by indx') ;
          $rec_imgs_all=group_by_field('parent',$rec_imgs) ;
        }

        $rowIterator = $objPHPExcel->getActiveSheet()->getRowIterator();
        foreach($rowIterator as $row)
        { $cellIterator = $row->getCellIterator();
          $cellIterator->setIterateOnlyExistingCells(false); // Loop all cells, even if it is not set
          $cur_row=$row->getRowIndex() ;
          $rec_goods=array() ;  $sortament_data=array() ; $rec_stat=array() ; ; $art='' ; $code='' ; $goods_rec=array() ;  $kadr=0 ; $goods_id=0 ;  $rec_imgs=array() ;
          $rec_stat['clss']=207 ;
          $rec_stat['file']=basename($xls_file_name) ;
          $rec_stat['line']=$cur_row ;
          foreach ($cellIterator as $cell)
            { $value=$cell->getValue(); //$value=$cell->getCalculatedValue();
              if ($cur_row==1) $arr_row_names[$cell->getColumn()]=str_replace("\n",'',$value) ;  // первая строка - заголовки
              else
              { $res_arr[$cur_row][$arr_row_names[$cell->getColumn()]]=$value ;
                  $col_name=trim($arr_row_names[$cell->getColumn()]);
                  switch($col_name)
                  { case 'N':             break ;
                    case '№':             break ;
                    case 'Номенклатура':  $rec_goods['obj_name']=$value ; break ;
                    case 'Характеристика номенклатуры': { $arr=explode(',',$value) ; $sortament_data['size']=$arr[0] ; }   break ;
                    case 'Артикул':       $art=$value ;    break ;
                    case 'Штрихкод':      $code=trim($value) ;    break ;
                    case 'Номер кадра':   $kadr=trim($value) ; break ;
                    case 'Доп характеристики':   if ($value) $rec_goods['comment']=$value ;    break ;
                    case 'Состав':        if ($value) $rec_goods['sostav']=$value ;    break ;
                    case 'Раздел':        break ;
                    case 'Группа товаров':break ;
                    case 'Стиль':        // if ($value) $rec_goods['style']=_IL('IL_style')->get_id_of_value($value,array('append_value_is_not_found'=>1)) ;
                                         break ;
                    case 'Реальный цвет': if ($value)
                                          {   $value=mb_convert_case(trim($value),MB_CASE_LOWER) ;
                                              $sortament_data['color']=_IL('IL_colors')->get_id_of_value($value,array('append_value_is_not_found'=>1)) ;
                                          }
                                          break ;
                    case 'Категория':     if ($value) $sortament_data['categor']=_IL('IL_categor')->get_id_of_value(check_categor_value($value),array('append_value_is_not_found'=>1)) ; break ;
                    default: echo '<div class=red>Нераспознаыый столбец: '.$col_name.' = '.$value.'</div>' ;
                  }
              }
            }

          $rec_stat['kadr']=$kadr ;
          $rec_stat['code']=$code ;
          $rec_stat['art']=$art ;
          $rec_stat['comment']=$rec_goods['comment'] ;
          $rec_stat['sostav']=$rec_goods['sostav'] ;
          if ($sortament_data['categor']==1) $rec_goods['m']=1 ;
          if ($sortament_data['categor']==2) $rec_goods['w']=1 ;
          if ($sortament_data['categor']==3) $rec_goods['k']=1 ;
          $rec_stat['style_id']=$rec_goods['style'] ;
          $rec_stat['color_id']=$sortament_data['color'] ;
          $rec_stat['categor_id']=$sortament_data['categor'] ;

          if ($code)
          {   $sortament_rec=$sortament_recs[$code] ;// execSQL_van('select pkey,parent from '.TM_SORTAMENT.' where code="'.$code.'"') ;
              $rec_stat['sortament_id']=$sortament_id=$sortament_rec['pkey'] ;
              $rec_stat['goods_id']=$goods_id=$sortament_rec['parent'] ;
              $rec_stat['code']=$code ;
              if ($sortament_rec['pkey'])
              { $update_sortament++ ;
                $rec_stat['sortament_id']=$sortament_rec['pkey'] ;
                $rec_stat['goods_id']=$sortament_rec['parent'] ;
                $rec_stat['art']=$art ;
                // обновляем  запись товара сортамента
                $goods_rec=$goods_recs[$sortament_rec['goods_id']] ; // execSQL_van('select * from obj_site_goods where pkey="'.$sortament_rec['parent'].'"') ;
                $goods_rec['__href']=$goods_system->get_url_item($goods_rec) ;
                  $rec_stat['goods_name']=$goods_rec['obj_name'] ;
                  $rec_stat['goods_enabled']=$goods_rec['enabled'] ;
                // смотрим, не загружено ли это фото уже в этот товар
                  $goods_images=$rec_imgs_all[$goods_id] ; // execSQL('select pkey,file_name from obj_site_goods_image where parent='.$goods_id.' order by indx') ;

                //damp_array($sortament_data) ;
                //damp_array($rec_goods) ;
                //update_rec_in_table(TM_SORTAMENT,$sortament_data,'pkey='.$sortament_rec['pkey']) ; $update_sortament++ ;
                //update_rec_in_table('obj_site_goods',$rec_goods,'pkey='.$goods_rec['pkey']) ; $update_goods++ ;
              }
              else
              { //echo '<div class=red>'.$cur_row.': Не найден сортамент штрих-код '.$code.'</div>' ;
                $cnt_no_code++ ;
              }
          }

          if ($kadr)  $cnt_kadr++ ;

          ?><tr>
                <td><?echo $cur_row?></td>
                <td><?if ($kadr) echo $kadr?></td>
                <td><?echo $code.'&nbsp;&nbsp;&nbsp;' ;
                     if ($code and $goods_id) echo ($rec_stat['goods_enabled'])? '<img class="status" src="/admin/img/ok_1.gif" title="Товар показывается на сайте" alt="Товар показывается на сайте">':'<img class="status" src="/admin/img/off2.gif" title="Товар отсутствует в 1С" alt="Товар отсутствует в 1С">' ;
                    ?>
                </td>
                <td><?if ($code)
                    {  if ($goods_id and $rec_stat['goods_enabled']) echo '<strong><a href="'.$goods_rec['__href'].'" target="_blank">'.$goods_rec['obj_name'].'</a></strong><br>' ;
                       else if ($goods_id and !$rec_stat['goods_enabled']) echo '<strong>'.$goods_rec['obj_name'].'</strong><br>' ;
                       else echo 'Товар не найден' ;
                       if (sizeof($goods_images)) foreach($goods_images as $rec_img){?><img src="/public/catalog/small/<?echo $rec_img['file_name']?>"><?}
                    } ?>
                <td><? if ($rec_goods['comment']) echo $rec_goods['comment'].'<br>' ;
                       if ($rec_goods['sostav']) echo $rec_goods['sostav'].'<br>' ;
                       if ($sortament_data['size']) echo 'Размер: '.$sortament_data['size'].'<br>' ;
                       if ($sortament_data['color']) echo 'Цвет: '.$_SESSION['IL_colors'][$sortament_data['color']]['obj_name'].'<br>' ;
                       if ($rec_goods['style']) echo 'Стиль: '.$_SESSION['IL_style'][$rec_goods['style']]['obj_name'].'<br>' ;
                       if ($sortament_data['categor']) echo 'Пол: '.$_SESSION['IL_categor'][$sortament_data['categor']]['obj_name'].'<br>' ;
                    ?>
                </td>
                <td <?if ($kadr){?>id="img_<?echo $kadr?>"<?}?> <?if ($goods_id and $rec_stat['goods_enabled']) echo ' class=on'?> goods_id="<?echo $goods_id?>"><?
                       if (sizeof($arr_img[$kadr])) foreach($arr_img[$kadr] as $indx=>$img_name)
                       { //$rec_exist_img=execSQL_van('select pkey,file_name from obj_site_goods_image where parent="'.$goods_id.'" and (obj_name="086A'.$img_name.'" or obj_name="086a'.$img_name.'" or obj_name="'.$img_name.'")') ;
                         //$checked=(!$rec_exist_img['pkey'])? 'checked':'' ;
                         $flag=0 ;
                         if (sizeof($goods_images)) foreach($goods_images as $rec_img) if (strpos($rec_img['obj_name'],$img_name)!==false) $flag=1 ;
                         $checked=(!$flag)? 'checked':'' ;

                         if ($checked) $cnt_checked_photo++ ;

                         if ($goods_id) {?><div class=checkbox id="<?echo $kadr.'_'.$indx?>"><input type=checkbox class="check_to_upload" <?echo $checked?> value="<?echo $img_name?>" name="img[<?echo $goods_id?>][]"></div><? }
                         ?><img width=100 src="<?echo hide_server_dir($member_dir).$img_name?>"><?
                       }

                    ?></td>
           </tr><?

           // по идеет надо бы сохранять под $code а не goods_id но много переделывать
           if ($kadr) $_SESSION['uploaded_kadr_info'][$kadr][$goods_id]=array('code'=>$code,'goods_id'=>$goods_id,'goods_name'=>$goods_rec['obj_name'],'color'=>$rec_stat['color_id'],'file'=>basename($xls_file_name),'line'=>$cur_row) ;
        }
        ?></table>
          <style>
            table.report{width:100%;table-layout: auto;border-collapse:collapse;margin-top:15px;}
            table.report td{border:1px solid #eeeeee;text-align:center;padding:5px 5px;}
            table.report th{border:1px solid #eeeeee;background:#dddddd; text-align:center;padding:0 5px;font-weight:bold;  font-weight: normal;  font-family: 'PTSansBold';font-size:14px;text-transform:uppercase;}
          </style>
          <script type="text/javascript">
              $j('input.check_to_upload').click(function()
                 {   $j('#cnt_uploads_photo').html($j('div.checkbox :checkbox:checked').length) ;
                 }) ;

          </script>

        <?
        //print_2x_arr($res_arr) ;
        $text=ob_get_clean() ;

        ?><div class="process_info"><?
        ob_start()  ;
            ?><div>Обработано <strong><? echo $cur_row ?></strong> строк файла</div><?
            ?><div>Проверено <strong><? echo $cnt_kadr ?></strong> записей по товарам</div><?
            /*?><div>Обновлено товаров <strong><? echo $update_goods ?></strong> позиций</div><?
            ?><div>Обновлено сортаментов товаров <strong><? echo $update_sortament ?></strong> позиций </div><?*/
            if ($cnt_no_photo){?><div>Не найдено изображений для заданных кадров <strong><? echo $cnt_no_photo ?></strong>  </div><?}
            if ($cnt_no_code) {?><div>Не найдено товаров по указанному штрих-коду <strong><? echo $cnt_no_code ?></strong>  </div><?}
        $result_text=ob_get_clean() ;
        echo $result_text ;
        ?></div><?

        ?><div class="button_upload_photo_to_goods">
                <input type="submit" class="v2" cmd="cabs/upload_photo_to_goods" value="Прикрепить фото к товарам"><br>
                <p>Будет прикреплено к товарам: <strong id="cnt_uploads_photo"><?echo $cnt_checked_photo?></strong> фото</p>
         </div>
          <div class=clear></div><?


        echo $text ;

        //dublicate_pol() ;

        ?></form><?
        return(array($result_text,false)) ;
    } else return(array('Не удалось прочитать файл Excel',true)) ;
 }

 function import_from_table_of_kadr($member_id,$member_dir,$only_kadr=0,$use_data=0)
 { global $goods_system,$debug_db ;
   set_time_limit(0) ;      //     $debug_db=2 ;
   if (!$only_kadr){?><form><?}
   ob_start() ;

   $arr_img=array() ; //  $_SESSION['uploaded_kadr_info']=array() ;
     $usl_kadr='' ;
   $list=get_files_list($member_dir,array('ext_pattern'=>'jpg')) ;
   if (sizeof($list)) foreach($list as $file_dir)
      { list($name,$ext)=explode('.',basename($file_dir)) ;
        list($kadr,$number)=explode('-',$name) ;
        if (!$number) $number=0 ;
        $arr_img[$kadr][$number]=basename($file_dir) ;
        if (sizeof($arr_img[$kadr])>1) ksort($arr_img[$kadr]) ;
      }

    //damp_array($arr_img) ;

    $update_goods=0 ; $update_sortament=0 ; $cnt_kadr=0 ;   $cnt_no_photo=0 ;  $cnt_no_code=0 ; $cnt_checked_photo=0 ;   $arr_sortament_ids=array() ;  $arr_goods_ids=array() ;
     $usl_img_kadr=(sizeof($arr_img))? ' and kadr in ('.implode(',',array_keys($arr_img)).')':'' ;

    if (!$only_kadr)
    { $cnt=execSQL_value('select count(pkey) from obj_site_kadr where member_id='.$member_id.' and goods_id>0 and sortament_id>0 and code!=""') ;
      if ($cnt) echo 'В вашей базе <strong>'.$cnt.'</strong> кадров. Загрузите изображения в панели слева, чтобы они появились в данной таблице<br><br>' ;
      $usl_kadr=(sizeof($_SESSION['uploaded_kadr_info']))? 'and kadr in ('.implode(',',array_keys($_SESSION['uploaded_kadr_info'])).')':'' ;
      if ($use_data)
      {  echo '<h2>Показаны кадры, загруженные '.date('d.m.Y',$use_data).'</h2>' ;
         $data=getdate($use_data) ;   // damp_array($now) ;
         $t1=mktime(0,0,0,$data['mon'],$data['mday'],$data['year']);
         $t2=mktime(23,59,59,$data['mon'],$data['mday'],$data['year']);
         $usl_kadr=' and c_data>='.$t1.' and c_data<='.$t2 ;
         $usl_img_kadr='' ;
         //echo '$usl_kadr='.$usl_kadr.'<br>' ;
      }

      ?><table class="report" id="table_of_kadr"><tr><th>Номер кадра</th><th>Шрих-код</th><th>Товар</th><th>Описание</th><th>Изображение для загрузки</th></tr><?

     //echo '$usl_kadr='.$usl_kadr.'<br>' ;
    }
    else $usl_kadr=' and kadr='.$only_kadr ;


   if ($usl_kadr or $usl_img_kadr) $recs_kadr=execSQL('select * from obj_site_kadr where member_id='.$member_id.' and goods_id>0 and sortament_id>0 and code!="" '.$usl_img_kadr.$usl_kadr.' order by kadr desc') ;
   if (sizeof($recs_kadr))
   { foreach($recs_kadr as $rec) { $arr_goods_ids[]=$rec['goods_id'] ; $arr_sortament_ids[]=$rec['sortament_id'] ; }
     $goods_ids=implode(',',$arr_goods_ids) ; $sortament_ids=implode(',',$arr_sortament_ids) ;
     // получаем сразу все необходмые данные
     $goods_recs=execSQL('select * from '.TM_GOODS.' where pkey in ('.$goods_ids.')') ;
     $sortament_recs=execSQL('select * from '.TM_SORTAMENT.' where pkey in ('.$sortament_ids.')') ;
     $rec_imgs=execSQL('select * from obj_site_goods_image where parent in ('.$goods_ids.') order by indx') ;
     $rec_imgs_all=group_by_field('parent',$rec_imgs) ;


     foreach($recs_kadr as $rec_kadr)
     {   $sortament_data=array() ; $goods_data=array() ; $rec_stat=array() ; ; $goods_rec=array() ;   $goods_id=0 ;  $rec_imgs=array() ;
         $sortament_rec=$sortament_recs[$rec_kadr['sortament_id']] ;
         $goods_rec=$goods_recs[$rec_kadr['goods_id']] ;
         $goods_rec['__href']=$goods_system->get_url_item($goods_rec) ;

         $code=$rec_kadr['code'] ;
         $kadr=trim($rec_kadr['kadr']) ;
         $goods_id=$goods_rec['pkey'] ;

         //goods_data['style']=_IL('IL_colors')->get_id_of_value($rec_kadr['style'],array('append_value_is_not_found'=>1)) ;
         //goods_data['comment']=$rec_kadr['comment'] ;
         //goods_data['sostav']=$rec_kadr['sostav'] ;
         //$sortament_data['size']=$rec_kadr['size'] ;
         //$rec_kadr['color']=mb_convert_case(trim($rec_kadr['color']),MB_CASE_LOWER) ;
         //$sortament_data['color']=_IL('IL_colors')->get_id_of_value($rec_kadr['color'],array('append_value_is_not_found'=>1)) ;
         //$sortament_data['categor']=_IL('IL_categor')->get_id_of_value(check_categor_value($rec_kadr['categor']),array('append_value_is_not_found'=>1)) ; break ;

         //if ($sortament_data['categor']==1) $goods_data['m']=1 ;
         //if ($sortament_data['categor']==2) $goods_data['w']=1 ;
         //if ($sortament_data['categor']==3) $goods_data['k']=1 ;

         // смотрим, не загружено ли это фото уже в этот товар
         $goods_images=$rec_imgs_all[$goods_id] ;
         //$rec_img=execSQL_van('select pkey,file_name from obj_site_goods_image where parent='.$goods_id.' and obj_name="'.$kadr.'.jpg"') ;

         //update_rec_in_table(TM_SORTAMENT,$sortament_data,'pkey='.$sortament_rec['pkey']) ; $update_sortament++ ;
         //update_rec_in_table(TM_GOODS,$goods_data,'pkey='.$goods_rec['pkey']) ; $update_goods++ ;

         $cnt_kadr++ ;

          ?><tr>
                <td><?if ($kadr) echo $kadr?></td>
                <td><?echo $code.'&nbsp;&nbsp;&nbsp;' ;
                     if ($code and $goods_id) echo ($goods_rec['enabled'])? '<img class="status" src="/admin/img/ok_1.gif" title="Товар показывается на сайте" alt="Товар показывается на сайте">':'<img class="status" src="/admin/img/off2.gif" title="Товар отсутствует в 1С" alt="Товар отсутствует в 1С">' ;
                    ?>
                </td>
                <td><?if ($code)
                    {  if ($goods_id and $goods_rec['enabled']) echo '<strong><a href="'.$goods_rec['__href'].'" target="_blank">'.$goods_rec['obj_name'].'</a></strong><br>' ;
                       else if ($goods_id and !$goods_rec['enabled']) echo '<strong>'.$goods_rec['obj_name'].'</strong><br>' ;
                       else echo 'Товар не найден' ;
                       /*if ($rec_img['pkey']){?><img src="/public/catalog/small/<?echo $rec_img['file_name']?>"><?}*/
                       if (sizeof($goods_images)) foreach($goods_images as $rec_img){?><img src="/public/catalog/small/<?echo $rec_img['file_name']?>"><?}
                    } ?>
                <td><? if ($goods_rec['comment']) echo $goods_rec['comment'].'<br>' ;
                       if ($goods_rec['sostav']) echo $goods_rec['sostav'].'<br>' ;
                       if ($sortament_rec['size']) echo 'Размер: '.$sortament_rec['size'].'<br>' ;
                       if ($sortament_rec['color']) echo 'Цвет: '.$_SESSION['IL_colors'][$sortament_rec['color']]['obj_name'].'<br>' ;
                       if ($goods_rec['style']) echo 'Стиль: '.$_SESSION['IL_style'][$goods_rec['style']]['obj_name'].'<br>' ;
                       if ($sortament_rec['categor']) echo 'Пол: '.$_SESSION['IL_categor'][$sortament_rec['categor']]['obj_name'].'<br>' ;
                    ?>
                </td>
                <td <?if ($kadr){?>id="img_<?echo $kadr?>"<?}?> <?if ($goods_id and $goods_rec['enabled']) echo ' class=on'?> goods_id="<?echo $goods_id?>"><?
                       if (sizeof($arr_img[$kadr])) foreach($arr_img[$kadr] as $indx=>$img_name)
                       {   $flag=0 ;
                           if (sizeof($goods_images)) foreach($goods_images as $rec_img) if (strpos($rec_img['obj_name'],$img_name)!==false) $flag=1 ;
                           $checked=(!$flag)? 'checked':'' ;
                           if ($checked) $cnt_checked_photo++ ;
                           if ($goods_id) {?><div class=checkbox id="<?echo $kadr.'_'.$indx?>"><input type=checkbox class="check_to_upload" <?echo $checked?> value="<?echo $img_name?>" name="img[<?echo $goods_id?>][]"></div><? }
                           ?><img width=100 src="<?echo hide_server_dir($member_dir).$img_name?>"><?
                       }

                    ?></td>
           </tr><?
           // по идеет надо бы сохранять под $code а не goods_id но много переделывать
           if ($kadr) $_SESSION['uploaded_kadr_info'][$kadr][$goods_id]=array('code'=>$code,'goods_id'=>$goods_id,'goods_name'=>$goods_rec['obj_name'],'sortament_id'=>$rec_kadr['sortament_id'],'color'=>$sortament_rec['color']) ;
          }

   }
   //else echo '<div class="alert">Ваша база кадров пуста. Внеси номера кадров в базу Вы можете через <a href="/cabs/photo/check/">Провeрку товара</a></div>' ;
  if (!$only_kadr){?></table><?
    ?><style>
        table.report{width:100%;table-layout: auto;border-collapse:collapse;margin-top:15px;}
        table.report td{border:1px solid #eeeeee;text-align:center;padding:5px 5px;}
        table.report th{border:1px solid #eeeeee;background:#dddddd; text-align:center;padding:0 5px;font-weight:bold;  font-weight: normal;  font-family: 'PTSansBold';font-size:14px;text-transform:uppercase;}
      </style>
      <script type="text/javascript">
          $j('input.check_to_upload').click(function()
             {   $j('#cnt_uploads_photo').html($j('div.checkbox :checkbox:checked').length) ;
             }) ;

      </script>
    <?
    //print_2x_arr($res_arr) ;
    $text=ob_get_clean() ;

    ?><div class="process_info"><?
    ob_start()  ;
        ?><div>Обработано <strong><? echo sizeof($recs_kadr) ?></strong> кадров товара</div><?
        ?><div>Проверено <strong><? echo $cnt_kadr ?></strong> записей по товарам</div><?
        /*?><div>Обновлено товаров <strong><? echo $update_goods ?></strong> позиций</div><?
        ?><div>Обновлено сортаментов товаров <strong><? echo $update_sortament ?></strong> позиций </div><?*/
        if ($cnt_no_photo){?><div>Не найдено изображений для заданных кадров <strong><? echo $cnt_no_photo ?></strong>  </div><?}
        if ($cnt_no_code) {?><div>Не найдено товаров по указанному штрих-коду <strong><? echo $cnt_no_code ?></strong>  </div><?}
    $result_text=ob_get_clean() ;
    echo $result_text ;
    ?></div><?

    ?><div class="button_upload_photo_to_goods">
            <input type="submit" class="v2" cmd="cabs/upload_photo_to_goods" value="Прикрепить фото к товарам"><br>
            <p>Будет прикреплено к товарам: <strong id="cnt_uploads_photo"><?echo $cnt_checked_photo?></strong> фото</p>
     </div>
      <div class=clear></div><?


    echo $text ;

    //dublicate_pol() ;


    ?></form><?
   }
   return(array($result_text,false)) ;

   //2000344593297
 }


 function table_of_kadr_tr($rec_kadr,&$arr_img,$member_dir,&$cnt_checked_photo)
 { global $goods_system ;
   $code=$rec_kadr['code'] ;
   $kadr=trim($rec_kadr['kadr']) ;
   $goods_rec=execSQL_van('select * from '.TM_GOODS.' where pkey='.$rec_kadr['goods_id']) ;
   $goods_rec['__href']=$goods_system->get_url_item($goods_rec) ;
   $goods_id=$goods_rec['pkey'] ;
   $sortament_rec=execSQL_van('select * from '.TM_SORTAMENT.' where pkey='.$rec_kadr['sortament_id']) ;
   $goods_images=execSQL('select * from obj_site_goods_image where parent='.$goods_id.' order by indx') ;

   ?><tr>
                    <td><?if ($kadr) echo $kadr?></td>
                    <td><?echo $code.'&nbsp;&nbsp;&nbsp;' ;
                         if ($code and $goods_id) echo ($goods_rec['enabled'])? '<img class="status" src="/admin/img/ok_1.gif" title="Товар показывается на сайте" alt="Товар показывается на сайте">':'<img class="status" src="/admin/img/off2.gif" title="Товар отсутствует в 1С" alt="Товар отсутствует в 1С">' ;
                        ?>
                    </td>
                    <td><?if ($code)
                        {  if ($goods_id and $goods_rec['enabled']) echo '<strong><a href="'.$goods_rec['__href'].'" target="_blank">'.$goods_rec['obj_name'].'</a></strong><br>' ;
                           else if ($goods_id and !$goods_rec['enabled']) echo '<strong>'.$goods_rec['obj_name'].'</strong><br>' ;
                           else echo 'Товар не найден' ;
                           /*if ($rec_img['pkey']){?><img src="/public/catalog/small/<?echo $rec_img['file_name']?>"><?}*/
                           if (sizeof($goods_images)) foreach($goods_images as $rec_img){?><img src="/public/catalog/small/<?echo $rec_img['file_name']?>"><?}
                        } ?>
                    <td><? if ($goods_rec['comment']) echo $goods_rec['comment'].'<br>' ;
                           if ($goods_rec['sostav']) echo $goods_rec['sostav'].'<br>' ;
                           if ($sortament_rec['size']) echo 'Размер: '.$sortament_rec['size'].'<br>' ;
                           if ($sortament_rec['color']) echo 'Цвет: '.$_SESSION['IL_colors'][$sortament_rec['color']]['obj_name'].'<br>' ;
                           if ($goods_rec['style']) echo 'Стиль: '.$_SESSION['IL_style'][$goods_rec['style']]['obj_name'].'<br>' ;
                           if ($sortament_rec['categor']) echo 'Пол: '.$_SESSION['IL_categor'][$sortament_rec['categor']]['obj_name'].'<br>' ;
                        ?>
                    </td>
                    <td <?if ($kadr){?>id="img_<?echo $kadr?>"<?}?> <?if ($goods_id and $goods_rec['enabled']) echo ' class=on'?> goods_id="<?echo $goods_id?>"><?
                           if (sizeof($arr_img[$kadr])) foreach($arr_img[$kadr] as $indx=>$img_name)
                           {   $flag=0 ;
                               if (sizeof($goods_images)) foreach($goods_images as $rec_img) if (strpos($rec_img['obj_name'],$img_name)!==false) $flag=1 ;
                               $checked=(!$flag)? 'checked':'' ;
                               if ($checked) $cnt_checked_photo++ ;
                               if ($goods_id) {?><div class=checkbox id="<?echo $kadr.'_'.$indx?>"><input type=checkbox class="check_to_upload" <?echo $checked?> value="<?echo $img_name?>" name="img[<?echo $goods_id?>][]"></div><? }
                               ?><img width=100 src="<?echo hide_server_dir($member_dir).$img_name?>"><?
                           }

                        ?></td>
               </tr><?
               // по идеет надо бы сохранять под $code а не goods_id но много переделывать
               if ($kadr) $_SESSION['uploaded_kadr_info'][$kadr][$goods_id]=array('code'=>$code,'goods_id'=>$goods_id,'goods_name'=>$goods_rec['obj_name'],'sortament_id'=>$rec_kadr['sortament_id'],'color'=>$sortament_rec['color']) ;






 }


function import_from_excel_parsing_file_old($xls_file_name)
 { //global $list_photo2 ;
   set_include_path(_DIR_EXT.'/PHPExcel/');
   if (!file_exists(_DIR_EXT.'/PHPExcel/')) {echo 'Расширение PHPExcel не установлено '; return ; } ;
   set_time_limit(0) ;
   include_once 'PHPExcel/IOFactory.php';

   execSQL_update('delete from obj_site_stats_kadr where file="/old/'.basename($xls_file_name).'"') ;

   $objPHPExcel = PHPExcel_IOFactory::load($xls_file_name);
   $objPHPExcel->setActiveSheetIndex(0);
   // начинаем заполняем с корневого раздела
   $cnt_update=0 ; $cnt_found=0 ; $cnt_no_photo=0 ; $cnt_no_code=0 ; $update_sortament=0 ;
   // читаем активную страницу
   $aSheets = $objPHPExcel->getAllSheets();

   if (sizeof($aSheets)) foreach($aSheets as $indx=>$aSheet) if (is_object($aSheet))
    {   $result=array('file'=>'/old/'.basename($xls_file_name)) ;
        $xlsData = getXLS($aSheet); //извлеаем данные из XLS
        $art=$xlsData[3][1] ;
        $kard=str_replace('100-','',$xlsData[2][1]) ;
        $rec_stat=array() ;
        $rec_stat['clss']=207 ;
        $rec_stat['file']='/old/'.basename($xls_file_name) ;
        $rec_stat['line']=$indx ;
        $rec_stat['kadr']=$kard ;
        $rec_stat['art']=$art ;

        //if ($kard)
        //  {   if (isset($list_photo2[$kard])) $rec_stat['img_name']=basename($list_photo2[$kard]) ;
        //      else { echo '<div class=black>кадр '.$kard.' - фото нет</div>' ; $cnt_no_photo++ ; }
        //  }


        if ($art and $kard)
        {   $result['art']=$art ;
            $rec_goods=execSQL_van('select * from obj_site_goods where art="'.$art.'" and clss=200') ;
            if ($rec_goods['pkey'])
            {  $result['goods_id']=$rec_goods['pkey'] ;
               $rec_stat['goods_id']=$rec_goods['pkey'] ;
               $rec_stat['goods_enabled']=$rec_goods['enabled'] ;
               $sortament_rec=execSQL_van('select pkey,code from '.TM_SORTAMENT.' where parent='.$rec_goods['pkey'].' and clss=65 and (kadr is null or kadr="" or kadr="'.$kard.'")  order by pkey limit 1') ;
               if ($sortament_rec['pkey'])
               { $res=update_rec_in_table(TM_SORTAMENT,array('kadr'=>$kard),'pkey="'.$sortament_rec['pkey'].'"') ;
                 $result['sortament_id']=$sortament_rec['pkey'] ;
                 $rec_stat['sortament_id']=$sortament_rec['pkey'] ;
                 $result['code']=$sortament_rec['code'] ;
                 if ($res['found'])  $cnt_found++ ;
                 if ($res['update'])  $cnt_update++ ;
                 $update_sortament++ ;
               }
            }
            else
            { echo '<div class=red>Артикул '.$art.' - не найден товар</div>' ;
              $cnt_no_code++ ;
            }

        }

        if ($rec_stat['art']) adding_rec_to_table('obj_site_stats_kadr',$rec_stat) ;
    }

     $_SESSION['sync_1c_result']['check_xls']++ ; // число проверенных прайс-листов
     $_SESSION['sync_1c_result']['kadr_xls_set_all']+=$update_sortament ; // задано кадров для сортамента
     $_SESSION['sync_1c_result']['kadr_xls_img_not_found']+=$cnt_no_photo ; // Не найдено изображений для заданных кадров
     $_SESSION['sync_1c_result']['kadr_xls_old_1C_code']+=$cnt_no_code ; // Не найдено товаров по указанному штрих-коду
 }


function getXLS($aSheet){
   //include_once 'Classes/PHPExcel/IOFactory.php';
   //$objPHPExcel = PHPExcel_IOFactory::load($xls);
   //$objPHPExcel->setActiveSheetIndex(0);
   //$aSheet = $objPHPExcel->getActiveSheet();

   //этот массив будет содержать массивы содержащие в себе значения ячеек каждой строки
   $array = array();
   //получим итератор строки и пройдемся по нему циклом
   foreach($aSheet->getRowIterator() as $row){
     //получим итератор ячеек текущей строки
     $cellIterator = $row->getCellIterator();
     //пройдемся циклом по ячейкам строки
     //этот массив будет содержать значения каждой отдельной строки
     $item = array();
     foreach($cellIterator as $cell){
       //заносим значения ячеек одной строки в отдельный массив
       //array_push($item, iconv('utf-8', 'cp1251', $cell->getCalculatedValue()));
       array_push($item, $cell->getCalculatedValue());
     }
     //заносим массив со значениями ячеек отдельной строки в "общий массв строк"
     array_push($array, $item);
   }
   return $array;
 }

?>