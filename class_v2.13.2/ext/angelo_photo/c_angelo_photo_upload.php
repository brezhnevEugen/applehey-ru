<?php
include(_DIR_TO_CLASS.'/c_page.php') ;
class c_page_cab_angelo_photo_upload extends c_page
{

  public $h1='Загрузка фото' ;

  function set_head_tags()
   { parent::set_head_tags() ;
     //if (_CUR_PAGE_DIR=='/cabs/photo/')
       $this->set_head_tags_fancyupload() ;
   }

   function set_head_tags_fancyupload()
   {   $this->HEAD['js'][]='https://ajax.googleapis.com/ajax/libs/mootools/1.5.1/mootools-yui-compressed.js' ;
       $this->HEAD['js'][]=_PATH_TO_EXT.'/fancyupload-v3.0.1/source/Swiff.Uploader.js' ;
       $this->HEAD['js'][]=_PATH_TO_EXT.'/fancyupload-v3.0.1/source/Fx.ProgressBar.js' ;
       $this->HEAD['js'][]=_PATH_TO_EXT.'/fancyupload-v3.0.1/source/FancyUpload2.js' ;
       $this->HEAD['js'][]=_PATH_TO_EXT.'/fancyupload-v3.0.1/fancyupload2.js' ;
       $this->HEAD['css'][]=_PATH_TO_EXT.'/fancyupload-v3.0.1/fancyupload.css' ;
       $this->HEAD['css'][]='/images/PT-Sans/stylesheet.css' ;
   }

    function block_main()
     { $this->page_title() ;
       ?><table class="col_3x"><tr>
            <td colspan="2"><?$this->panel_fancyupload() ;?></td>
            <td><?$this->panel_upload_manual() ;?></td>
         </tr></table><?
       ?><h1>Загруженные фото</h1><?

       $this->panel_upload_xls() ;
       ?><style type="text/css">
           table.col_3x{table-layout:fixed;border-collapse:collapse;width:100%;}
           table.col_3x td{vertical-align:top;}
           table.col_3x div#upload-status{width:300px;float:left;}
           table.col_3x ul#upload-list{width:300px;height:200px;overflow-y:scroll;float:right;}
         </style>
       <?
       //echo 'Сохранено в сессии информации по кадрам: '.sizeof($_SESSION['uploaded_kadr_info']).'<br>'.session_id() ;

     }

     function panel_upload_xls()
     {  global $member ;
        // проверяем наличие папки для загрузки фото, если нет - создаем её
        if (!file_exists(_DIR_TO_ROOT.'/private/'.$member->id.'/')) mkdir(_DIR_TO_ROOT.'/private/'.$member->id.'/') ;
        $member_dir=_DIR_TO_ROOT.'/private/'.$member->id.'/' ;
        //$_SESSION['uploaded_kadr_info']=array() ;
        ?><div id="table_xls"><?
        include_once(_DIR_EXT.'/angelo_photo/import_kadr.php') ;
        $list=get_files_list($member_dir,array('ext_pattern'=>'xls')) ;
        if ($list[0])
           { ?><h2>Читаем файл <?echo basename($list[0])?></h2><?
             import_from_excel_parsing_file_to_table($list[0],$member_dir) ;
           }
        else {?> <p>Загрузите через загрузчик в верхней части страницы сначала файл с описанием кадров <strong>XLS</strong>, затем фото товаров, можно группой.</p>
                   <p>Загруженные фото будут показаны в центральной панели согласно распознаным именам кадров.</p>
                   <p>Проверьте правильность назначения фото товарам, сверяя изображение на фото и название товара. Если изображение сопоставлено неверно, снимите галку в чекбоксе над изображением.</p>
                   <p>В последующем исправьте ошибку - она будет либо в названии кадра фото, либо в файле XLS. Недостающие изображения можно будет прикрепить к товарам позднее, после прикрепления основной части фото.</p>
                   <p>При обновлении страницы чекбокс не будет установлен у товаров, уже содержащих подгтовленное для прикрепления фото.</p>
                   <?}
        ?></div><?
     }


      function panel_fancyupload()
          {   //if (_CUR_PAGE_DIR!='/cabs/photo/' and _CUR_PAGE_DIR!='/cabs/photo/kadr/' ) return ;
              global $member ;
              ?><div id="panel_fancyupload">
                <form action="/fancyupload2.php" method="POST" enctype="multipart/form-data" id="form-upload">
                    <input type=hidden name=session_id id=session_id value="<?echo session_id()?>">
                    <input type=hidden name=member_id id=member_id value="<?echo $member->id?>">
                    <fieldset id="upload-fallback">
                        <legend>Загрузка файлов</legend>
                        <p>Подождите, идет запуск загрузчика файлов... </p>
                        <label for="upload-photoupload">Загрузка файла:<input type="file" name="Filedata" /></label>
                    </fieldset>

                    <div id="upload-status" class="hide">
                        <div id=menu_panel>
                            <img src="<?echo _PATH_TO_EXT?>/fancyupload-v3.0.1/assets/select_file.png"> <a href="#" id="upload-browse">Выбрать файлы</a>
                            <!--<img src="<?echo _PATH_TO_EXT?>/fancyupload-v3.0.1/assets/upload_file.png"> <a href="#" id="upload-upload">Загрузить на сервер</a>-->
                        </div>
                        <div class=status_bar><strong class="overall-title"></strong><br /><img src="<?echo _PATH_TO_EXT?>/fancyupload-v3.0.1/assets/progress-bar/bar.gif" class="progress overall-progress" /></div>
                        <div class=status_bar><strong class="current-title"></strong><br /><img src="<?echo _PATH_TO_EXT?>/fancyupload-v3.0.1/assets/progress-bar/bar.gif" class="progress current-progress" /></div>
                        <div class="current-text"></div>
                    </div>
                    <ul id="upload-list"></ul>

                </form>
        	    </div>
                <br>
                <div id="debug"></div>
            <?
          }

        function panel_upload_manual()
        { ?><div id="manual">
                        <ul>
                            <li>Если Вам необходимо загрузить следующий XLS файл, просто загрузите его здесь. Старый файл удалится автоматически.</li>
                            <li>Файл XLS и фото остануться загруженными в личный кабинет, даже если вы выйдите из него.</li>
                            <li>После прикрепления фото к товару, Вы можете удалить исходные файлы с сервера, они более не потребуются.<br>
                                <? global $member ;
                                   $member_dir=_DIR_TO_ROOT.'/private/'.$member->id.'/' ;
                                   if (file_exists($member_dir))
                                   { $list=get_files_list($member_dir,array()) ;
                                     if (sizeof($list)) {?><button class="v2" cmd="angelo_photo/clear_cabinet">Очистить кабинет</button><?}
                                   }
                                ?></li>
                        </ul>
             </div>
          <?
        }
}
?>