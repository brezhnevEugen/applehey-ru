<?
include_once(_DIR_TO_ENGINE.'/c_page_XML_AJAX.php') ;
class c_page_ajax extends c_page_XML_AJAX
{

  function get_personal_by_id($reffer)
  { list($pkey,$tkey)=explode('.',$reffer) ;
    $rec_personal=array() ;
    if (function_exists('BP') and $tkey==BP()->tkey_personal) $rec_personal=BP()->get_personal_by_id($pkey) ;
    if ($tkey==PERSONAL()->tkey_personal) $rec_personal=PERSONAL()->get_personal_by_id($pkey) ;
    return($rec_personal) ;
  }

  function get_panel_photo_capture()
  { $rec_personal=array() ;
    if($_POST['reffer']) $rec_personal=$this->get_personal_by_id($_POST['reffer']) ;
    if ($rec_personal['pkey'])
    { include(_DIR_EXT.'/webcamera_capture/panel_photo_capture.php') ;
      $options=array() ;
      $options['title']=($_POST['title'])? $_POST['title']:'Фото сотрудника' ;
      $options['save_cmd']=($_POST['save_cmd'])? $_POST['save_cmd']:'webcamera_capture/save_capture_photo' ;
      $options['rec']=$rec_personal ;
      panel_photo_capture($options) ;
      $this->add_element('show_modal_window','HTML',array('width'=>'700px','after_close_func'=>'stop_video_capture')) ;
    }
    else $this->add_element('show_notife','Сотрудник не найден или нет прав доступа',array('type'=>'error')) ;
  }

  function fixed_photo_capture()
  { $str = file_get_contents("php://input");
    $str = rawurldecode($str);
    $str = explode('base64,', $str);
    $fname='/files/'.time().".jpg" ;
    $x=fopen(_DIR_TO_ROOT.$fname,"w");
    fwrite($x, base64_decode($str[1]));
    fclose($x);

    $img_from = imagecreatefromjpeg(_DIR_TO_ROOT.$fname);
    $w=imagesx($img_from)  ; $h=imagesy($img_from) ;
    imagedestroy($img_from) ;

    ob_start() ;
    ?><script type="text/javascript" src="<?echo _PATH_TO_ADDONS.'/tapmodo-jcrop-0.9.12/js/jquery.Jcrop.min.js'?>"></script>
      <script type="text/javascript" src="<?echo _PATH_TO_ADDONS.'/tapmodo-jcrop-0.9.12/js/jquery.color.js'?>"></script>
      <link rel="stylesheet" type="text/css" href="<?echo _PATH_TO_ADDONS?>/tapmodo-jcrop-0.9.12/css/jquery.Jcrop.css"/>

      <script type="text/javascript">
      $j('img#capture_img').attr('src','<?echo $fname?>')
                           .attr('width','<?echo $w?>px')
                           .attr('height','<?echo $h?>px')
                           .css('width','<?echo $w?>px')
                           .css('height','<?echo $h?>px')
                           //.css('display','block')
                           .css('margin','0 auto') ;

      $j('#capture_img_name').val('<?echo $fname?>') ;
      var jcrop_api;
      if (jcrop_api!=null) jcrop_api.setImage('<?echo $fname?>') ; // обновляем изображение в обрезалке

      $j('img#capture_img').Jcrop({
            bgFade:     true,
            bgOpacity: .5,
            setSelect: [ 170, 20, 300, 450 ],
            aspectRatio: 3 / 4,
            onSelect: updateCoords
          },function(){jcrop_api = this;});
      function updateCoords(c) {$j('#x').val(c.x); $j('#y').val(c.y); $j('#w').val(c.w); $j('#h').val(c.h); }
    </script><?
    $text=ob_get_clean()  ;
    $this->add_element('JSCODE',$text) ;
  }

  function crop_img()
  { $file_name=_DIR_TO_ROOT.$_POST['capture_img_name'] ;
    $new_name=str_replace('.jpg','_'.time().'.jpg',$file_name) ;
    $img_from = imagecreatefromjpeg($file_name);
    $w1=imagesx($img_from)  ; $h1=imagesy($img_from) ; 
    $w2=round($_POST['w']) ;  $h2=round($_POST['h']) ;
    $from_x=$_POST['x'] ;     $from_y=$_POST['y'] ;

    $img_to = imagecreatetruecolor( $w2, $h2);
    imagecopyresized($img_to,$img_from,0,0,$from_x,$from_y,$w2,$h2,$w2,$h2);
    imagejpeg($img_to,$new_name,100);
    imagedestroy($img_to) ;
    imagedestroy($img_from) ;
    ob_start() ;
    ?><script type="text/javascript">
      $j('img#capture_img').attr('src','<?echo hide_server_dir($new_name)?>')
                           .attr('width','<?echo $w2?>px')
                           .attr('height','<?echo $h2?>px')
                           .css('width','<?echo $w2?>px')
                           .css('height','<?echo $h2?>px')
                           .css('display','inline') ;
      jcrop_api.destroy();
      $j('#capture_img_name').val('<?echo hide_server_dir($new_name)?>') ;
    </script><?
    $text=ob_get_clean()  ;
    $this->add_element('JSCODE',$text) ;
    //$this->add_element('crop_info.HTML','w1='.$w1.'<br>h1='.$h1.'<br>w2='.$w2.'<br>h2='.$h2.'<br>x='.$from_x.'<br>y='.$from_y."<br>imagecopyresized(0,0,$from_x,$from_y,$w2,$h2,$w1,$h1);") ;
  }

  function save_capture_photo()
  {
    //$this->add_element('show_modal_window','HTML') ;
    $rec_personal=array() ;
    if($_POST['reffer']) $rec_personal=$this->get_personal_by_id($_POST['reffer']) ;
    if ($rec_personal['pkey'])
    { // удаляем все фото объекта
      delete_all_img_of_object($_POST['reffer']) ;
      // добавляем новое фото
      //damp_array($_POST) ;
      $img_info=array('name'=>basename($_POST['capture_img_name']),'upl_name'=>_DIR_TO_ROOT.$_POST['capture_img_name']) ;
      obj_upload_image($_POST['reffer'],$img_info,array('debug'=>0,'view_upload'=>1)) ;
      //$rec_personal=$this->get_personal_by_id($_POST['reffer']) ;
      $this->add_element('update_page',1) ;
      //$this->add_element('personal_photo_'.$rec_personal['pkey'].'.SRC',img_clone($rec_personal),400) ;
      //$this->add_element('show_modal_window','HTML') ;

    }
    else $this->add_element('show_notife','Сотрудник не найден или нет прав доступа',array('type'=>'error')) ;
  }


}