var videoStream = null;
function stop_video_capture() {if (videoStream!=null) videoStream.stop(); }

$j.fn.webcamera_capture = function(options)
  { var settings = $j.extend(
               {  'zerkalo' :false,
                  'save_cmd' :'webcamera_capture/save_capture_photo'
               }, options);

    // settings['bookmark_tag']

    $j(this).wrapInner('<div id="capture_area">'+
                            '<video id="video" autoplay=""></video>'+
                            '<div class="buttons"><button id="snap" class="fa fa-camera">Сделать снимок</button><button class="cancel fa fa-times">Отмена</button><br><label></div>'+
                       '</div>'+
                       '<div id="preview_area" class="hidden"><form>'+
                            '<input type="hidden" id="capture_img_name" name="capture_img_name"><input type="hidden" id="x" name="x"><input type="hidden" id="y" name="y"><input type="hidden" id="w" name="w"><input type="hidden" id="h" name="h">'+
                            '<img id="capture_img" src="" class="hidden">'+
                            //'<img id="capture_img2" src="" class="float_left"><div id="crop_info"></div><div class="clear"></div>'+
                            '<canvas id="canvas" class="hidden" width="640" height="480"></canvas>'+
                            '<div class="buttons"><button class="cancel">Отмена</button><button id="repeat">Еще раз</button><button class="v2" cmd="webcamera_capture/crop_img">Обрезать</button><button class="v2" cmd="'+settings.save_cmd+'">Сохранить</button></div>'+
                       '</form></div>'
                      ) ;

    var canvas = $j("#canvas")[0] ;
    var context = canvas.getContext("2d") ;
    var video = $j("#video")[0] ;
    var videoObj = { "video": true } ;
    var jcrop_api  ;
    var errBack = function(error) {console.log("Ошибка видео захвата: ", error.code); 	};
    var videoStreamUrl = false;

    start_camera() ;

    // Получение и отправка изображения
    $j('button#snap').click(function()
    { $j('div#capture_area').addClass('hidden') ;
      $j('div#preview_area').removeClass('hidden') ;


      if (!videoStreamUrl) alert('То-ли вы не нажали "разрешить" в верху окна, то-ли что-то не так с вашим видео стримом')
      // переворачиваем canvas зеркально по горизонтали (см. описание внизу статьи)
      if (settings['zerkalo'])
      { context.translate(canvas.width, 0);
        context.scale(-1, 1);
      }
      // отрисовываем на канвасе текущий кадр видео
      context.drawImage(video, 0, 0, 640, 480); //context.drawImage(video, 0, 0, video.width, video.height); - не работает
      // получаем data: url изображения c canvas
      var base64dataUrl = canvas.toDataURL('image/jpeg');
      context.setTransform(1, 0, 0, 1, 0, 0); // убираем все кастомные трансформации canvas
      // отправляем на сервер
      $j.ajax({url:'/ajax.php',type:'POST',dataType:'xml',success:exec_ajax_cmd_success_JQ,data:{cmd:'webcamera_capture/fixed_photo_capture',img:base64dataUrl}});
      video.pause(); // Пауза
      videoStream.stop(); // Стоп
    }) ;

    $j('button#repeat').click(start_camera) ;

    function start_camera()
    {  // navigator.getUserMedia  и   window.URL.createObjectURL (смутные времена браузерных противоречий 2012)
       navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia;
       window.URL.createObjectURL = window.URL.createObjectURL || window.URL.webkitCreateObjectURL || window.URL.mozCreateObjectURL || window.URL.msCreateObjectURL;

       // Подключение потока
       if(navigator.getUserMedia)  navigator.getUserMedia(videoObj,
          function(stream)
          { videoStream = stream ;
            // скрываем подсказку //allow.style.display = "none";
            // получаем url поточного видео
            videoStreamUrl = window.URL.createObjectURL(stream);
            // устанавливаем как источник для video
            video.src = videoStreamUrl;
            //video.src = stream; video.play();
          },
          errBack);
      $j('div#capture_area').removeClass('hidden') ;
      $j('div#preview_area').addClass('hidden') ;
      $j('img#capture_img').attr('src','') ;
      if (jcrop_api!=null) jcrop_api.destroy() ;
      return(false) ;
    }
  } ;