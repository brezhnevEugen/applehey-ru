<?php
include_once(_DIR_TO_MODULES.'/reports/i_report.php') ;
class report_accounts extends c_report
{
 var $title='Список аккаунтов' ;
 var $view_by_space_filter=1 ;
 var $list_accounts=array() ;

 function setting($options=array())
 {   ?><div id="panel_filter" class="one_row">
             <form id="sky-form" class="sky-form"><input type="hidden" name="filter_dir" value="<?echo ($options['filter_dir'])? $options['filter_dir']:_CUR_REPORT_DIR?>">
                  <fieldset>
                      <div class="row">
                           <section class="col col-2">
                               <label class="label">Фамилия Имя Отчество</label>
                               <label class="input">
                                    <input type="text" name="filter[name]" placeholder="" value="<?echo htmlspecialchars($this->filter['name'])?>">
                               </label>
                           </section>
                          <section class="col col-2">
                              <label class="label">Логин</label>
                              <label class="input">
                                  <input type="text" name="filter[login]" placeholder="" value="<?echo htmlspecialchars($this->filter['login'])?>">
                              </label>
                          </section>
                        </div>
                  </fieldset>
                  <div id="panel_buttons"><input type="submit" class="button v2" cmd="set_filter" value="OK"></div>
              </form>
       </div><br>
     <?
  }

  function create_usl_select_obj($_params,&$options)
  {  $usl=array() ;
     $usl[]='clss=86' ;
     //$usl[]='enabled=1' ;
     if ($this->filter['name'])              $usl[]='(obj_name like "'.addslashes($this->filter['name']).'%" or name1 like "'.addslashes($this->filter['name']).'%" or  name2 like "'.addslashes($this->filter['name']).'%" or name3 like "'.addslashes($this->filter['name']).'%")' ;
     if ($this->filter['login'])             $usl[]='login like "'.addslashes($this->filter['login']).'%"' ;
     $usl_res=implode(' and ',$usl) ;
     return($usl_res) ;
  }

 function panel_info_filter()
 { $_str=array() ; $info=array() ; $title='' ;
   if ($this->filter['name'])                       $info['ФИО']='"'.$this->filter['name'].'"' ;
   if ($this->filter['login'])                      $info['Логин']='"'.$this->filter['login'].'"' ;

   if (sizeof($info)) foreach($info as $title=>$value) $_str[]=$title.': <strong>'.$value.'</strong>' ;
   if (sizeof($_str)) $title='<p class=center>'.implode(' ',$_str).'</p>' ;
   return $title ;
 }


  function get_cnt_items($usl,$options=array())
  { //$options['debug']=2 ;
    $cnt=execSQL_value('select count(pkey) from  '.ACCOUNTS()->table_account.' where '.$usl,$options) ;
    return($cnt) ;
  }

  function get_items($usl,$options=array())
  { //$options['debug']=2 ;
    $limit=($options['limit'])? ' '.$options['limit']:'' ;
    $order=($this->filter['order'])? ' order by '.$this->filter['order']:' order by obj_name' ;
    $list_rec=execSQL('select * from  '.ACCOUNTS()->table_account.' where '.$usl.$order.$limit,$options) ;
    if (sizeof($list_rec)) foreach($list_rec as $id=>$rec) PERSONAL()->prepare_public_info_to_personal($list_rec[$id],$options) ;
    return($list_rec) ;
  }


// шаблон - вывод ссылками без разделитея
  function print_template_HTML($list_recs,$options=array())
  { global $member ;
     //damp_array($list_recs,1,-1) ;
    $id=($options['id'])? 'id='.$options['id']:'' ;
    $this->panel_info_select($options,'top') ;
    ?><table class="basic fz_small full auto"<?echo $id?>>
        <colgroup><col id="c1"><col id="c2"><col id="c3"><col id="c4"></colgroup>
        <tr><th>ID</th>><th>ФИО</th><th>Логин</th><th>Рутокен WEB ID</th><th>Операции</th></tr><?
        if (sizeof($list_recs)) foreach($list_recs as $rec)
        { ?><tr class="item rec_<?echo $rec['pkey']?>" reffer="<?echo $rec['_reffer']?>">
                <td class="id"><?echo $rec['pkey']?></td>
                <td class="left"><a href="<?echo $rec['__href']?>"><?echo $rec['obj_name']?></a></td>
                <td class="left"><?echo $rec['login']?></td>
                <td class="left"><? $token_IDs=execSQL_row('select pkey,token_ID from obj_site_account_out_150 where clss=150 and parent='.$rec['pkey']) ;
                                    if (sizeof($token_IDs)) foreach($token_IDs as $token_pkey=>$token_ID) {?><div class="item" id="token_info_<?echo $token_pkey?>"><?echo $token_ID?> <a class="v2" cmd="rutokenweb/unregister_token" token_pkey="<?echo $token_pkey?>">Удалить токен</a></div><?}
                    ?></td>
                <td class="center">
                   <button class="button_small v2" cmd="rutokenweb/get_panel_register_token" reffer="<?echo $rec['_reffer']?>">Зарегистрировать токен</button>&nbsp;&nbsp;
                </td>
            </tr><?
        }
      ?></table><?
      $this->panel_info_select($options,'bottom') ;
  }




}

?>