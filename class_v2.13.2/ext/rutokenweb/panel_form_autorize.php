<?
 // форма авторизации в центральной панели
 function panel_form_autorize($options=array())
 {    ?><script src="<?echo _PATH_TO_EXT ?>/rutokenweb/script/sha256.js"></script>
     	<script src="<?echo _PATH_TO_EXT ?>/rutokenweb/script/utf8.js"></script>
   		<script src="<?echo _PATH_TO_EXT ?>/rutokenweb/script/base64.js"></script>
	 	<script src="<?echo _PATH_TO_EXT ?>/rutokenweb/script/script.js"></script>
	 	<object id="cryptoPlugin" type="application/x-rutoken" width="0" height="0"><param name="onload" value="pluginit" /></object>
	    <form id="sky-form" class="sky-form">
	 		<input type="hidden" name="user_sign"  id="user_sign" value="" />
  			<input type="hidden" name="tlogin" id="token_log" value="" />
  			<input type="hidden" name="rnd" id="rnd_client" value="" />
       		<header><img src="<?echo _LOGO_CAB?>"></header>

       				<fieldset>
       					<section>
       						<div class="row">
       							<label class="label col col-4">Логин</label>
       							<div class="col col-8">
       								<label class="select">
										<select  tabindex="1" name="token_login" id="token_login">
										   <option selected="selected" value="none"> — </option>
										</select>
       								</label>
       							</div>
       						</div>
       					</section>


       				</fieldset>
       				<footer>
       					<button type="submit" class="button v2" validate=form cmd=rutokenweb/get_random_string>Вход</button>
                        <button class="button button-secondary v2" cmd="account/set_autorize_by_account">Вход по логину/паролю</button>

       				</footer>
       			</form>
       <form action="demo-login-process.php" id="sky-form2" class="sky-form sky-form-modal">
     			<header>Восстановление пароля</header>

     			<fieldset>
     				<section>
     					<label class="label">E-mail</label>
     					<label class="input">
     						<i class="icon-append fa fa-envelope-o"></i>
     						<input type="email" name="email" id="email">
     					</label>
     				</section>
     			</fieldset>

     			<footer>
     				<button type="submit" name="submit" class="button">Восстановить</button>
     				<a href="#" class="button button-secondary modal-closer">Закрыть</a>
     			</footer>

     			<div class="message">
     				<i class="fa fa-check"></i>
     				<p>Your request successfully sent!<br><a href="#" class="modal-closer">Close window</a></p>
     			</div>
     		</form>

     		<script type="text/javascript">
				$j(document).ready(function() { token_refresh() }) ;
     			$j(function()
     			{
     				// Validation for login form
     				$j("#sky-form").validate(
     				{
     					// Rules for form validation
     					rules:
     					{
                            user_login:
     						{
     							required: true
     							//email: true
     						},
                            user_pass:
     						{
     							required: true,
     							minlength: 1,
     							maxlength: 20
     						}
     					},

     					// Messages for form validation
     					messages:
     					{
                            user_login:
     						{
     							required: 'Пожалуйста, укажите свой логин'
     							//email: 'Please enter a VALID email address'
     						},
                            user_pass:
     						{
     							required: 'Пожалуйста, укажите свой пароль'
     						}
     					},

     					// Do not change code below
     					errorPlacement: function(error, element)
     					{
     						error.insertAfter(element.parent());
     					}
     				});


     				// Validation for recovery form
     				$j("#sky-form2").validate(
     				{
     					// Rules for form validation
     					rules:
     					{
     						email:
     						{
     							required: true,
     							email: true
     						}
     					},

     					// Messages for form validation
     					messages:
     					{
     						email:
     						{
     							required: 'Пожалуйста, укажите свой email',
     							email: 'Пожалуйста, укажите корректный email'
     						}
     					},

     					// Ajax form submition
     					submitHandler: function(form)
     					{
     						$j(form).ajaxSubmit(
     						{
     							beforeSend: function()
     							{
     								$j('#sky-form button[type="submit"]').attr('disabled', true);
     							},
     							success: function()
     							{
     								$j("#sky-form2").addClass('submited');
     							}
     						});
     					},

     					// Do not change code below
     					errorPlacement: function(error, element)
     					{
     						error.insertAfter(element.parent());
     					}
     				});
     			});
                $j(document).ready(function(){$j('input.login').keydown(function(e){if(e.keyCode==13) $j(this).next('input').focus();});}) ;
     		</script>
     <?
 }

?>