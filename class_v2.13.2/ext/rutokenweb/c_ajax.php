<?
include_once(_DIR_TO_ENGINE.'/c_page_XML_AJAX.php') ;
include_once(_DIR_EXT.'/aspmo_rmt/i_mrt.php') ;
class c_page_ajax extends c_page_XML_AJAX
{

    function set_autorize_by_token()
    {
        setcookie ('autorize_mode', "rutokenweb", time()+31536000);
        $this->update_page=1 ;
    }

    function get_random_string()
    {   list($login,$site)=explode('#%#',$_POST['token_login']) ;
        require_once(_DIR_EXT."/rutokenweb/crypto/token.php");
        require_once(_DIR_EXT."/rutokenweb/crypto/classes/interface/CurveFpInterface.php");
        require_once(_DIR_EXT."/rutokenweb/crypto/classes/CurveFp.php");
        $rand_str = token_random();
        $_SESSION[$login]= $rand_str ;    // будет использовано при autorize
        $this->add_element('JSCODE','<script type="text/javascript">random_text = "'.$rand_str.'";token_sign("'.$_POST['token_login'].'","'.$rand_str.'");</script>') ;
    }

    function autorize()
    {   require_once(_DIR_EXT."/rutokenweb/crypto/token.php");
        require_once(_DIR_EXT."/rutokenweb/crypto/classes/interface/CurveFpInterface.php");
        require_once(_DIR_EXT."/rutokenweb/crypto/classes/interface/PointInterface.php");
        require_once(_DIR_EXT."/rutokenweb/crypto/classes/interface/PublicKeyInterface.php");
        require_once(_DIR_EXT."/rutokenweb/crypto/classes/interface/SignatureInterface.php");
        require_once(_DIR_EXT."/rutokenweb/crypto/classes/CurveFp.php");
        require_once(_DIR_EXT."/rutokenweb/crypto/classes/GOSTcurve.php");
        require_once(_DIR_EXT."/rutokenweb/crypto/classes/Point.php");
        require_once(_DIR_EXT."/rutokenweb/crypto/classes/PublicKey.php");
        require_once(_DIR_EXT."/rutokenweb/crypto/classes/Signature.php");
        require_once(_DIR_EXT."/rutokenweb/crypto/classes/util/gmp_Utils.php");

        list($login,$site)=explode('#%#',$_POST['token_login']) ;

        $rand_num = hash('sha256', $_POST['rnd'] . ":" . $_SESSION[$login]);

        $r_ecp = substr($_POST['user_sign'], 0, 64);
        $s_ecp = substr($_POST['user_sign'], 64);

        $rec_account = execSQL_van("SELECT *  FROM ".ACCOUNTS()->table_account." WHERE login='".addslashes($login)."' LIMIT 1");
        $error_message='' ;  $success_mess='' ;

        if ($rec_account['pkey'])
        {   $recs_token=execSQL('select pkey,token_ID,user_xkey,user_ykey from obj_site_account_out_150 where clss=150 and parent='.$rec_account['pkey']) ;
            foreach($recs_token as $rec_token)
            {   $verify = token_verify($rand_num, $rec_token['user_xkey'], $rec_token['user_ykey'], $r_ecp, $s_ecp);

                if ( $verify )
                { $success_mess = "Пользователь <strong>" . $login . "</strong> авторизован с использованием Рутокен Web!";

                  // авторизуем пользователя
                  if (MEMBER()->id) ACCOUNTS()->member_logout() ;
                  ACCOUNTS()->member_autorize($rec_account,array('event_name'=>'Авторизация через Рутокен WEB')) ;
                  $this->add_element('go_url','/cab/') ;   // переходим на страницу личного кабинета
                  break ;  // останавливаем проверку токенов
                }
            }
            // если ни один из токенов не подошел - выводим сообщение
            if (!$success_mess) $error_message="Доступ запрещен!";
        } else $error_message="Пользователь <strong>" . $login . "</strong> не зарегистрирован!";

        if ($error_message)  $this->add_element('show_notife',$error_message,array('type'=>'error')) ;
        if ($success_mess)  $this->add_element('show_notife',$success_mess,array('type'=>'info')) ;
    }

    function get_panel_register_token()
     { include_once('panel_register_token.php') ;
       list($account_id,$tkey)=explode('.',$_POST['reffer']) ;
       $rec_account=ACCOUNTS()->get_member_by_id($account_id) ;
       panel_register_token($rec_account) ;
       $this->add_element('show_modal_window','HTML',array('title'=>'Регистрация Рутокен WEB')) ;
     }

    function register_token()
    {  $rec_account=ACCOUNTS()->get_member_by_id($_POST['account_id']) ;
       execSQL_update('delete from obj_site_account_out_150 where parent='.$rec_account['pkey'].' and token_ID="'.$_POST['token_ID'].'"')  ;
       adding_rec_to_table('obj_site_account_out_150',array('parent'=>$rec_account['pkey'],'clss'=>150,'user_xkey'=>$_POST['xkey'],'user_ykey'=>$_POST['ykey'],'r_xkey'=>$_POST['rxkey'],'r_ykey'=>$_POST['rykey'],'token_ID'=>$_POST['token_ID'])) ;
       $this->update_page=1 ;

    }

    function unregister_token()
    {   execSQL_update('delete from obj_site_account_out_150 where pkey='.$_POST['token_pkey'])  ;
        $this->add_element('token_info_'.$_POST['token_pkey'].'.HTML','') ;
        $this->add_element('show_notife','Токен удален',array('type'=>'info')) ;
    }

}