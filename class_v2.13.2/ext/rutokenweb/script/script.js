var plugin;

function pluginit()
{
     // ...
}

var err = [];
    err[-1]  = 'USB-токен не найден';
    err[-2]  = 'USB-токен не залогинен пользователем';
    err[-3]  = 'PIN-код не верен';
    err[-4]  = 'PIN-код не корректен';
    err[-5]  = 'PIN-код заблокирован';
    err[-6]  = 'Неправильная длина PIN-кода';
    err[-7]  = 'Отказ от ввода PIN-кода';
    err[-10] = 'Неправильные аргументы функции';
    err[-11] = 'Неправильная длина аргументов функции';
    err[-12] = 'Открыто другое окно ввода PIN-кода';
    err[-20] = 'Контейнер не найден';
    err[-21] = 'Контейнер уже существует';
    err[-22] = 'Контейнер поврежден';
    err[-30] = 'ЭЦП не верна';
    err[-40] = 'Не хватает свободной памяти чтобы завершить операцию';
    err[-50] = 'Библиотека не загружена';
    err[-51] = 'Библиотека находится в неинициализированном состоянии';
    err[-52] = 'Библиотека не поддерживает расширенный интерфейс';
    err[-53] = 'Ошибка в библиотеке rtpkcs11ecp';


function token_sign(token_login,random_str)
{
	if ( !plugin.valid )
	{
		alert("Не установлен плагин для работы с USB-токеном");
		return;
	}
	var rnd = Sha256.hash(Math.random().toString(16));
	random_str = Sha256.hash(rnd + ':'+ random_str);
	res = plugin.rtwSign(token_login, random_str);
	if ( res != -7 && res != -12 )
	{
		if	(res < 0) { alert(err[res]); } else send_ajax_request({cmd:'rutokenweb/autorize',user_sign:res,token_login:token_login,rnd:rnd}) ;
	}
}

function token_refresh() {
	console.log('token_refresh') ;
	plugin = document.getElementById("cryptoPlugin");
	log_list = document.getElementById("token_login");
	for (var i = log_list.options.length - 1; i >= 0; i--) {
    log_list.remove(i);
	}
	if (plugin==undefined || !plugin.valid)
	{
		alert("Не установлен плагин для работы с USB-токеном");
		return;
	}
	if (plugin.rtwIsTokenPresentAndOK() === true)
	{
		count_cont = plugin.rtwGetNumberOfContainers();
		for( i=0; i < count_cont; i++ ) {
			cont_name = plugin.rtwGetContainerName(i);
			addOption(log_list, cont_name.replace("#%#", " - "), cont_name, 0, 0);
		}
	} else {
		alert("USB-токен отсутствует");
	}
}

function addOption (oListbox, text, value, isDefaultSelected, isSelected) {
  var oOption = document.createElement("option");
  oOption.appendChild(document.createTextNode(text));
  oOption.setAttribute("value", value);
  if (isDefaultSelected) oOption.defaultSelected = true;
  else if (isSelected) oOption.selected = true;
  oListbox.appendChild(oOption);
}

function link_token(ulogin,site,account_id)
 {	plugin = document.getElementById("cryptoPlugin");
	if (ulogin == '') { alert("Введите логин"); return; }
	if (!plugin.valid) { alert("Установите плагин"); return; }
	if ( !(plugin.rtwIsTokenPresentAndOK() === true) ) { alert("USB-токен отсутствует!"); }
	if (!plugin.rtwIsUserLoggedIn()) { plugin.rtwUserLoginDlg(); }
	if (plugin.rtwIsUserLoggedIn())
	{ var key = plugin.rtwGenKeyPair(ulogin + '#%#'+ site);
	  var rkey = plugin.rtwGetPublicKey('repair key');
	  console.log('key:'+key) ;
	  console.log('rkey:'+rkey) ;

	  plugin.rtwLogout();
	  if (key<0) alert("Ошибка при создании ключевой пары: "+err[key]);
	  else
	  {var xkey = key.substring(0,64);
	   var ykey = key.substring(64);
	   if (rkey)
	   {
		 var rxkey = rkey.substring(0,64);
		 var rykey = rkey.substring(64);
	   }
	   send_ajax_request({cmd:'rutokenweb/register_token',xkey:xkey,ykey:ykey,rxkey:rxkey,rykey:rykey,account_id:account_id,token_ID:plugin.rtwGetDeviceID()}) ;
	}

   }
   else alert("Введен ошибочный PIN-код.");

 }

