<?
  // шаблон - список статей: наименование-ссылка
  function list_images_highslide(&$list_recs,$options=array())
  { global $member ;
    $id=($options['id'])? 'id='.$options['id']:'' ;
    $small_clone=($options['clone_small'])? $options['clone_small']:'small' ;
    $big_clone=($options['big_clone'])? $options['big_clone']:'source' ;
    ?><div class="list_images_highslide" <?echo $id?>><?
    if (sizeof($list_recs)) foreach($list_recs as $rec)
    { ?><div class="item img_<?echo $rec['pkey']?>">
            <a id=a_img_<?echo $rec['pkey']?> href="<?echo img_clone($rec,$big_clone)?>?r=<?echo rand(1,5000)?>" title="<?echo $rec['obj_info']?>" onclick="return hs.expand(this)"><img id=img_<?echo $rec['pkey']?> src="<?echo img_clone($rec,$small_clone)?>" alt="<?echo $options['alt']?> # 1"></a>
            <?if ($options['show_name']){?><div class="name"><?echo $rec['obj_name']?></div><?}?>
            <?if($member->info['status']){?><div class="delete hidden v2" cmd="personal/delete_image" image_id="<?echo $rec['pkey']?>"></div><?}?>
            <div class="rotate_l hidden v2" cmd="personal/rotate_l_image" image_id="<?echo $rec['pkey']?>"></div>
            <div class="rotate_r hidden v2" cmd="personal/rotate_r_image" image_id="<?echo $rec['pkey']?>"></div>
            <div class="set_main hidden v2" cmd="personal/set_main_image" image_id="<?echo $rec['pkey']?>"></div>
            <a href="/files/<?echo $rec['file_name']?>">Оригинал</a>
        </div><?
    }
    ?><div class="clear"></div></div><?
  }
?>
