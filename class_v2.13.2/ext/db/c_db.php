<?php
include_once(_DIR_TO_CLASS.'/c_page.php') ;
class c_page_cab_db extends c_page
{ public $title='Проверка БД' ;
  function check_access(){return(1);}
  var $no_view_in_setting_right=1 ;

 function body()
 {
     $this->page_title('Проверка БД') ;
     $this->CMD_check_setting() ;
     $this->CMD_check_db() ;
 }

 function CMD_check_setting()
 {
   global $__functions,$install_options ;
   $install_options['install_XML_export']=1 ;
   $install_options['install_create_table']=1 ;
   include_once(_DIR_TO_ROOT.'/ini/'._INIT_) ;
   include_once(_DIR_TO_ENGINE.'/admin/i_setup.php') ;
   //damp_array($__functions['install']) ;
   if (sizeof($__functions['install'])) foreach($__functions['install'] as $func_name) ${$func_name(0)} ;
 }

 function CMD_check_db()
 {
  $cnt_sql_cmd=0 ;
  include_once(_DIR_EXT.'/db/i_table_checked.php') ;
  ?><h1>Проверка структуры объектных таблиц</h1><?
  ?><table class=table_checked><?
   $options=array('no_update_descr_tables'=>1,
                  'no_show_button'=>1,
                  'show_in_outer_table'=>1,
                  'no_check_not_used_field'=>1,
                  'no_check_type_fields'=>1
                 );
   if (sizeof($_SESSION['descr_obj_tables'])) foreach ($_SESSION['descr_obj_tables'] as $obj) $cnt_sql_cmd+=table_checked($obj->pkey,$options) ;
    if ($cnt_sql_cmd) {?><tr><td colspan=2><button class=button cmd=exec_sql_list_cmd mode=dialog>Выполнить выбранные операции</button></td><?}
  ?></table><?
  //return(array('no_default_panel'=>1)) ;
 }


}

?>