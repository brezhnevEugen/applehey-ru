<?
include_once(_DIR_TO_ENGINE.'/c_page_XML_AJAX.php') ;
class c_page_ajax extends c_page_XML_AJAX
{
    function change_vaforite()
    {   $status=execSQL_value('select pkey from obj_site_members_out_201 where clss=201 and parent='.$_POST['client_id'].' and account_id='.$_SESSION['member']->id,2) ;
        if ($status)
        { execSQL_update('delete from obj_site_members_out_201 where clss=201 and parent='.$_POST['client_id'].' and account_id='.$_SESSION['member']->id) ;
          $this->add_element('show_notife','Удалено из избранного') ;
          $this->add_element('JSCODE','<script type="text/javascript">$j("#fav_'.$_POST['client_id'].'").removeClass("active")</script>') ;
        }
        else
        { adding_rec_to_table('obj_site_members_out_201',array('clss'=>201,'parent'=>$_POST['client_id'],'account_id'=>$_SESSION['member']->id)) ;
          $this->add_element('JSCODE','<script type="text/javascript">$j("#fav_'.$_POST['client_id'].'").addClass("active")</script>') ;
          $this->add_element('show_notife','Добавлено в избранное') ;
        }

    }

    function clear_favorite()
    {
        execSQL_update('delete from obj_site_members_out_201 where clss=201 and account_id='.$_SESSION['member']->id) ;
        $this->add_element('show_notife','Список избранного очищен') ;
        $this->update_page=1 ;

    }

    function fixed_card_change()
    {
        _event_reg('Замена карточки','',$_POST['client_id'].'.104') ;
        $this->add_element('show_notife','Сохранено',array('type'=>'info')) ;
    }

    function set_member()
    { update_rec_in_table('obj_site_members',array('check_data'=>time(),'check_account'=>$_SESSION['member']->id),'pkey='.$_POST['member_id'],array('debug'=>0)) ; // создаем клиента
      $this->add_element('check_info.HTML','<span class=green>'.date('d.m.Y H:i',time()).', '.$_SESSION['member']->name.'</span>') ;
      $this->add_element('check_info_'.$_POST['member_id'].'.HTML','<span class=green>'.date('d.m.Y H:i',time()).'<br>'.$_SESSION['member']->name.'</span>') ;
      _event_reg('Посетил','',$_POST['member_id'].'.104') ;
      //execSQL_update('update obj_site_members set check_data=r_data,check_account=creator_id where clss=200') ;
    }

    function save_member()
       { $member_id=$_POST['member_id'] ;
         $name1=str_replace(' ','',$_POST['f1']) ;
         $name2=str_replace(' ','',$_POST['f2']) ;
         $name3=str_replace(' ','',$_POST['f3']) ;
         $pass1=str_replace(' ','',$_POST['p1']) ;
         $pass2=str_replace(' ','',$_POST['p2']) ;
         //$card_value=str_replace(' ','',$_POST['card']) ;
         $comment=$_POST['card'] ;

         $ID_melbet=str_replace(' ','',$_POST['ID_melbet']) ;
         $ID_fon=str_replace(' ','',$_POST['ID_fon']) ;
         $ID_zenit=str_replace(' ','',$_POST['ID_zenit']) ;
         $ID_win=str_replace(' ','',$_POST['ID_win']) ;

         /*
         $card_exists=$this->check_card_number($card_value,$member_id) ;
         if ($card_exists)
         {    $this->add_element('success','ajax_to_modal_window') ;
              $this->add_element('modal_panel_width','800px') ;
              echo 'Указанный Вами номер карты (<strong>'.$card_value.'</strong>) уже используется:' ;
              search_member_by_card($card_value) ;
              return ;
         } */

         $member_exists=$this->check_name_doc_number($name1,$pass1,$pass2,$member_id) ;
         if ($member_exists)
         {   $this->add_element('show_modal_window','HTML',array('width'=>'800px')) ;
             echo 'Такая фамилия и номер документа (<strong>'.$name1.', '.$pass2.'</strong>) уже есть в базе:' ;
             //search_member_by_name1_pass2($name1,$pass2) ;
             return ;
         }

         $member_rec=array('obj_name'  =>  $_POST['f1'].' '.$_POST['f2'].' '.$_POST['f3'],
                           'name1'     =>  $name1, // фамилия
                           'name2'     =>  $name2, // имя
                           'name3'     =>  $name3, // отчество
                           'pass1'     =>  $pass1, // серия
                           'pass2'     =>  $pass2, // номер
                           'card'      =>  $comment, // карта
                           'comment'      =>  $_POST['comment'], // карта
                           'ID_melbet' =>  $ID_melbet, // карта
                           'ID_fon'    =>  $ID_fon, // карта
                           'ID_zenit'  =>  $ID_zenit, // карта
                           'ID_win'  =>  $ID_win, // карта
                           'doc'       =>  ($_POST['doс'])? $_POST['doс']:1, // тип документа
                           'phone'     =>  $_POST['phone'],
                           'submit'    =>  $_POST['client_submit'],
                           'typ'       =>  $_POST['client_type']
                       );
         //damp_array($member_rec,1,-1) ;
         //$this->add_element('show_modal_window','HTML',array('width'=>'800px')) ;
         update_rec_in_table('obj_site_members',$member_rec,'pkey='.$member_id) ; // создаем клиента
         _event_reg('Редактирование клиента','',$member_id.'.104') ;

         $this->append_img_to_member($member_id,$_POST['images']) ;  // прикрепляем к записи уже загруженные изображения

         $this->update_page=1 ; // команда на обновление страницы
       }

    // удаление клиента
    function delete_member()
    {  $this->add_element('success','ajax_to_modal_window') ;
       $this->add_element('no_close_button',1) ;
       ?><div class="center">Вы действительно хотите удалить этого клиента из базы?<br><br>
         <button class="cancel">Отмена</button> <button class="button_red v2" cmd="personal/delete_member_apply" update_page="<?echo $_POST['update_page']?>" member_id="<?echo $_POST['member_id']?>">Удалить</button></div>
       <?
    }

    // удаление клиента
    function delete_member_apply()
    {  // удаляем клиента из базы
       _event_reg('Удаление клиента','',$_POST['member_id'].'.104') ;
       _CLSS(200)->obj_delete(104,$_POST['member_id']) ;
       // JS код для визуального удаления записи из списка
       ob_start() ;
       ?><script type="text/javascript">$j('table.list_personal_table_img tr.rec_<?echo $_POST['member_id']?>').remove();</script><?
       $this->add_element('JSCODE',ob_get_clean()) ;
       $this->add_element('close_mBox_window','mBox_delete_member') ;
       $this->add_element('panel_edit_member','<div class="alert">Данные по клиенту были успешно удалены из базы</div>') ;
    }

    // удаление фото
    function delete_image()
    {  $this->add_element('success','ajax_to_modal_window') ;
       $this->add_element('no_close_button',1) ;
       ?><div class="center">Вы действительно хотите удалить это изображение из базы?<br><br>
         <button class="cancel">Отмена</button> <button class="button_red v2" cmd="personal/delete_image_apply" image_id="<?echo $_POST['image_id']?>">Удалить</button></div>
       <?
    }

    // удаление клиента
    function delete_image_apply()
    {  // удаляем клиента из базы
       $member_id=execSQL_value('select parent from obj_site_members_image where pkey='.$_POST['image_id']) ;
       _event_reg('Удаление изображения','',$member_id.'.104',$_POST['image_id'].'.107') ;
       _CLSS(3)->obj_delete(107,$_POST['image_id']) ;
        $image_name=execSQL_value('select file_name from '._DOT(107)->table_name.' where clss=3 and enabled=1 and not (file_name="" or file_name is null) and parent='.$member_id.' order by indx limit 1') ;
        update_rec_in_table('obj_site_members',array('_image_name'=>$image_name),'pkey='.$member_id) ;

       // JS код для визуального удаления записи из списка
       ob_start() ;
       ?><script type="text/javascript">$j('div.list_images_highslide div.item.img_<?echo $_POST['image_id']?>').remove();</script><?
       $this->add_element('JSCODE',ob_get_clean()) ;
       $this->add_element('close_mBox_window','mBox_delete_image') ;
    }

    function rotate_l_image()
    {   $rec_img=execSQL_van('select * from obj_site_members_image where pkey='.$_POST['image_id']) ;
        $this->rotare_img($rec_img['file_name'],200,-90) ;
        $this->rotare_img($rec_img['file_name'],'small',-90) ;
        $this->rotare_img($rec_img['file_name'],'source',-90) ;
        $this->add_element('img_'.$_POST['image_id'].'.SRC','/public/members/200/'.$rec_img['file_name'].'?r='.rand(1,5000)) ;
        $this->add_element('a_img_'.$_POST['image_id'].'.HREF','/public/members/source/'.$rec_img['file_name'].'?r='.rand(1,5000)) ;
    }

    function rotate_r_image()
    {
        $rec_img=execSQL_van('select * from obj_site_members_image where pkey='.$_POST['image_id']) ;
        $this->rotare_img($rec_img['file_name'],200,90) ;
        $this->rotare_img($rec_img['file_name'],'small',90) ;
        $this->rotare_img($rec_img['file_name'],'source',90) ;
        $this->add_element('img_'.$_POST['image_id'].'.SRC','/public/members/200/'.$rec_img['file_name'].'?r='.rand(1,5000)) ;
        $this->add_element('a_img_'.$_POST['image_id'].'.HREF','/public/members/source/'.$rec_img['file_name'].'?r='.rand(1,5000)) ;
    }
    function set_main_image()
    {   $rec_img=execSQL_van('select * from obj_site_members_image where pkey='.$_POST['image_id']) ;
        execSQL_update('update obj_site_members_image set indx=indx+1 where parent='.$rec_img['parent']) ;
        execSQL_update('update obj_site_members_image set indx=1 where pkey='.$_POST['image_id']) ;
        execSQL_update('update obj_site_members set _image_name="'.$rec_img['file_name'].'" where pkey='.$rec_img['parent']) ;
        ob_start() ;
        ?><script type="text/javascript">
             $j('div.list_images_highslide').prepend($j('img#img_<?echo $_POST['image_id']?>').closest("div.item")) ;
                       </script>
        <?
        //$this->add_element('img_'.$_POST['image_id'].'.SRC','/public/members/200/'.$rec_img['file_name'].'?r='.rand(1,5000)) ;
        //$this->add_element('a_img_'.$_POST['image_id'].'.HREF','/public/members/source/'.$rec_img['file_name'].'?r='.rand(1,5000)) ;
        $text=ob_get_clean() ;
        $this->add_element('JSCODE',$text) ;
    }

    function rotare_img($file_name,$clone,$grad)
    {
        $imagick = new Imagick();
        $imagick->readImage(_DIR_TO_ROOT.'/public/members/'.$clone.'/'.$file_name);
        $imagick->rotateImage(new ImagickPixel('none'),$grad);
        $imagick->writeImage(_DIR_TO_ROOT.'/public/members/'.$clone.'/'.$file_name);
        $imagick->clear();
        $imagick->destroy();
    }

  function check_pass1()
  { if (strlen($_POST['value'])!=4)
    { $this->add_element('result',0) ;
      $this->add_element('alert_text','Неправильно введен серия паспорта, требуется 4 цифры') ;
    }
    else $this->add_element('result',1) ;
    $this->add_element('element_id',$_POST['element_id']) ;
  }

  function check_pass2()
  { if (strlen($_POST['value'])!=6)
    { $this->add_element('result',0) ;
      $this->add_element('alert_text','Неправильно введен номер паспорта, требуется 6 цифр') ;
    }
    else $this->add_element('result',1) ;
    $this->add_element('element_id',$_POST['element_id']) ;
  }

  function check_name()
  { $pattern = "/[^A-Za-zАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЬЫЪЭЮЯабвгдеёжзийклмнопрстуфхцчшщьыъэюя]+/ui";
    $text=$_POST['value'] ;
    //$text=iconv('utf-8','windows-1251',stripcslashes($_POST['value'])) ;
    if (preg_match_all($pattern, $text, $matches))
    { $this->add_element('result',0) ;
      //unset($matches[0][0]) ;
      $err=implode('',$matches[0]) ;
      //$err=iconv('windows-1251','utf-8',$err) ;
      if ($err==' ') $err='пробел' ;
      $this->add_element('alert_text','Текст содержит недопустимые символы <span class=red>'.$err.'</span>') ;

    }
    else $this->add_element('result',1) ;
    $this->add_element('element_id',$_POST['element_id']) ;
  }

  function check_ID_melbet()
  { $id=$this->check_field('ID_melbet',$_POST['value']) ;
    if ($id) $this->add_element('alert_text','Данный ID уже используется') ;
    else $this->add_element('result',1) ;
    $this->add_element('element_id',$_POST['element_id']) ;
  }

  function check_ID_fon()
  { $id=$this->check_field('ID_fon',$_POST['value']) ;
    if ($id) $this->add_element('alert_text','Данный ID уже используется') ;
    else $this->add_element('result',1) ;
    $this->add_element('element_id',$_POST['element_id']) ;
  }

  function check_ID_zenit()
  { $id=$this->check_field('ID_zenit',$_POST['value']) ;
    if ($id) $this->add_element('alert_text','Данный ID уже используется') ;
    else $this->add_element('result',1) ;
    $this->add_element('element_id',$_POST['element_id']) ;
  }

  function check_ID_win()
  { $id=$this->check_field('ID_win',$_POST['value']) ;
    if ($id) $this->add_element('alert_text','Данный ID уже используется') ;
    else $this->add_element('result',1) ;
    $this->add_element('element_id',$_POST['element_id']) ;
  }

  // номер карты должен быть уникальный - проверяется только при создании нового клиента
  function check_card()
  { $id=$this->check_card_number($_POST['value']) ;
    if ($id)
    { $this->add_element('result',0) ;
      $this->add_element('alert_text','Этот номер карты уже используется') ;
    }
    else        $this->add_element('result',1) ;
    $this->add_element('element_id',$_POST['element_id']) ;
  }

  function check_card_number($value,$member_id=0)
  { $sql='select pkey from obj_site_members where card="'.$value.'"' ;
    if ($member_id) $sql.=' and pkey!='.$member_id ;
    $id=execSQL_value($sql) ;
    return($id) ;
  }

  function check_field($fname,$value,$member_id=0)
  { $sql='select pkey from obj_site_members where '.$fname.'="'.$value.'"' ;
    if ($member_id) $sql.=' and pkey!='.$member_id ;
    $id=execSQL_value($sql) ;
    return($id) ;
  }

  function check_name_doc_number($name,$pass1,$pass2,$member_id=0)
  { $sql='select pkey from obj_site_members where name1="'.$name.'" and pass1="'.$pass1.'" and pass2="'.$pass2.'"' ;
    if ($member_id) $sql.=' and pkey!='.$member_id ;
    $id=execSQL_value($sql) ;
    return($id) ;
  }

  function get_last_pos()
  {  //  damp_array($_POST,1,-1) ;
      $client_name=PERSONAL()->get_personal_by_id($_POST['client_id']) ;
      $recs=execSQL('select distinct(t1.member_id) as member_id2, t3.obj_name as member_name,
                            (select max(c_data) from log_site_events t2 where  t2.event_id=3 and t2.client_id='.$_POST['client_id'].' and t2.member_id=member_id2) as last_time
                            from log_site_events t1
                            left join obj_site_account t3 on t3.pkey=t1.member_id
                            where  t1.event_id=3 and t1.client_id='.$_POST['client_id'].' and t1.member_id>0 order by last_time desc',0) ;

      //damp_array($recs,1,-1) ;
      ?><h2>Клиент: <?echo $client_name['obj_name']?></h2><?
      ?><div style="max-height:400px;overflow-y:scroll;"><table class="basic fz_small full" style="margin:0 auto;"><tr><th>Место</th><th>Последний раз был</th></tr><?
      if (sizeof($recs)) foreach($recs as $rec)
        {?><tr><td class="left"><?echo ($rec['member_name'])? $rec['member_name']:'Объект ID '.$rec['member_id2']?></td>
               <td><?echo date('d.m.Y H:i',$rec['last_time'])?></td></tr><?
        }
      ?></table></div><br><p class="center"><button class="cancel">Закрыть</button></p><?
      $this->add_element('show_modal_window','HTML',array('title'=>'Последние посещения клиента')) ;
  }

































   function get_panel_delete_personal()
   { $rec_personal=PERSONAL()->get_personal_by_id($_POST['id']) ;
     ?><p class="center">Вы действительно хотите удалить сотрудника</p>
       <p class="center"><strong><?echo $rec_personal['obj_name']?></strong>?</p>
       <br>
       <p class="center"><button class="v2" cmd="personal/delete_personal" id="<?echo $rec_personal['pkey']?>">Удалить</button>&nbsp;&nbsp;&nbsp;<button class="cancel">Отмена</button></p>

     <?
     $this->add_element('show_modal_window','HTML',array('title'=>'Удаление сотрудника')) ;
   }

   function delete_personal()
   { $rec_personal=PERSONAL()->get_personal_by_id($_POST['id']) ;
     if ($rec_personal['pkey'])
     { execSQL_update('update obj_site_members set enabled=0 where pkey='.$rec_personal['pkey']) ;
       LOGS()->reg_log('Удаление сотрудника',$rec_personal['obj_name'].' (id '.$rec_personal['pkey'].')') ;
       $this->add_element('update_page',1) ;
     }
   }

   function append_client()
       { $name1=str_replace(' ','',$_POST['f1']) ;
         $name2=str_replace(' ','',$_POST['f2']) ;
         $name3=str_replace(' ','',$_POST['f3']) ;
         $pass1=str_replace(' ','',$_POST['p1']) ;
         $pass2=str_replace(' ','',$_POST['p2']) ;
         //$card_value=str_replace(' ','',$_POST['card']) ;
         $comment=$_POST['card'] ;

         $ID_melbet=str_replace(' ','',$_POST['ID_melbet']) ;
         $ID_fon=str_replace(' ','',$_POST['ID_fon']) ;
         $ID_zenit=str_replace(' ','',$_POST['ID_zenit']) ;
         $ID_win=str_replace(' ','',$_POST['ID_win']) ;
         /*
         $card_exists=$this->check_card_number($card_value) ;
         if ($card_exists)
         {   $this->add_element('success','ajax_to_modal_window') ;
             $this->add_element('modal_panel_width','800px') ;
             echo 'Указанный Вами номер карты (<strong>'.$card_value.'</strong>) уже используется:' ;
             search_member_by_card($card_value) ;
             return ;
         } */

         $member_exists=$this->check_name_doc_number($name1,$pass1,$pass2) ;
         if ($member_exists)
         {   $this->add_element('show_modal_window','HTML',array('width'=>800)) ;
             echo 'Такая фамилия и номер документа (<strong>'.$name1.', '.$pass2.'</strong>) уже есть в базе' ;
             //search_member_by_name1_pass2($name1,$pass2) ;
             return ;
         }

         $member_rec=array(    'enabled'   =>  1,
                           'parent'    =>  1,
                           'clss'      =>  200,
                           'obj_name'  =>  $_POST['f1'].' '.$_POST['f2'].' '.$_POST['f3'],
                           'name1'     =>  $name1, // фамилия
                           'name2'     =>  $name2, // имя
                           'name3'     =>  $name3, // отчество
                           'pass1'     =>  $pass1, // серия
                           'pass2'     =>  $pass2, // номер
                           'card'      =>  $comment, // карта
                           'ID_melbet' =>  $ID_melbet, // карта
                           'ID_fon'    =>  $ID_fon, // карта
                           'ID_zenit'  =>  $ID_zenit, // карта
                           'ID_win'  =>  $ID_win, // карта

                           'doc'       =>  ($_POST['doс'])? $_POST['doс']:1, // тип документа
                           'phone'     =>  $_POST['phone'],
                           'submit'    =>  $_POST['client_submit'],
                           'typ'       =>  $_POST['client_type'],
                           'creator_id'       =>  $_SESSION['member']->id,
                           'check_account'       =>  $_SESSION['member']->id,
                           'check_data'       =>  time()
                       );

         $member_id=adding_rec_to_table('obj_site_members',$member_rec) ; // создаем клиента
         _event_reg('Добавление клиента','',$member_id.'.104') ;

         $this->append_img_to_member($member_id,$_POST['images']) ; // прикрепляем к записи уже загруженные изображения

         $this->go_url='/cabs/personals/person/'.$member_id.'/' ;

       }

       function save_member2()
       { $member_id=$_POST['member_id'] ;
         $name1=str_replace(' ','',$_POST['f1']) ;
         $name2=str_replace(' ','',$_POST['f2']) ;
         $name3=str_replace(' ','',$_POST['f3']) ;
         $pass1=str_replace(' ','',$_POST['p1']) ;
         $pass2=str_replace(' ','',$_POST['p2']) ;
         //$card_value=str_replace(' ','',$_POST['card']) ;
         $comment=$_POST['card'] ;

         $ID_melbet=str_replace(' ','',$_POST['ID_melbet']) ;
         $ID_fon=str_replace(' ','',$_POST['ID_fon']) ;
         $ID_zenit=str_replace(' ','',$_POST['ID_zenit']) ;
         $ID_win=str_replace(' ','',$_POST['ID_win']) ;

         /*
         $card_exists=$this->check_card_number($card_value,$member_id) ;
         if ($card_exists)
         {    $this->add_element('success','ajax_to_modal_window') ;
              $this->add_element('modal_panel_width','800px') ;
              echo 'Указанный Вами номер карты (<strong>'.$card_value.'</strong>) уже используется:' ;
              search_member_by_card($card_value) ;
              return ;
         } */

         $member_exists=$this->check_name_doc_number($name1,$pass1,$pass2,$member_id) ;
         if ($member_exists)
         {  $this->add_element('show_modal_window','HTML',array('width'=>800)) ;
             echo 'Такая фамилия и номер документа (<strong>'.$name1.', '.$pass2.'</strong>) уже есть в базе' ;
             //search_member_by_name1_pass2($name1,$pass2) ;
             return ;
         }

         $member_rec=array(    'obj_name'  =>  $_POST['f1'].' '.$_POST['f2'].' '.$_POST['f3'],
                           'name1'     =>  $name1, // фамилия
                           'name2'     =>  $name2, // имя
                           'name3'     =>  $name3, // отчество
                           'pass1'     =>  $pass1, // серия
                           'pass2'     =>  $pass2, // номер
                           'card'      =>  $comment, // карта
                           'ID_melbet' =>  $ID_melbet, // карта
                           'ID_fon'    =>  $ID_fon, // карта
                           'ID_zenit'  =>  $ID_zenit, // карта
                           'ID_win'  =>  $ID_win, // карта
                           'doc'       =>  ($_POST['doс'])? $_POST['doс']:1, // тип документа
                           'phone'     =>  $_POST['phone'],
                           'submit'    =>  $_POST['client_submit'],
                           'typ'       =>  $_POST['client_type']
                       );

         update_rec_in_table('obj_site_members',$member_rec,'pkey='.$member_id) ; // создаем клиента
         _event_reg('Редактирование клиента','',$member_id.'.104') ;

         $this->append_img_to_member($member_id,$_POST['images']) ;  // прикрепляем к записи уже загруженные изображения

         $this->update_page=1 ; // команда на обновление страницы
       }


    function check_name_doc_number2($name,$pass1,$pass2,$member_id=0)
      { $sql='select pkey from '.PERSONAL()->view_personal.' where name1="'.$name.'" and pass1="'.$pass1.'" and pass2="'.$pass2.'"' ;
        if ($member_id) $sql.=' and pkey!='.$member_id ;
        $id=execSQL_value($sql) ;
        return($id) ;
      }

function append_member()
    { $name1=str_replace(' ','',$_POST['f1']) ;
      $name2=str_replace(' ','',$_POST['f2']) ;
      $name3=str_replace(' ','',$_POST['f3']) ;
      $pass1=str_replace(' ','',$_POST['p1']) ;
      $pass2=str_replace(' ','',$_POST['p2']) ;
      //$card_value=str_replace(' ','',$_POST['card']) ;
      $comment=$_POST['card'] ;

      $ID_melbet=str_replace(' ','',$_POST['ID_melbet']) ;
      $ID_fon=str_replace(' ','',$_POST['ID_fon']) ;
      $ID_zenit=str_replace(' ','',$_POST['ID_zenit']) ;
      $ID_win=str_replace(' ','',$_POST['ID_win']) ;
      /*
      $card_exists=$this->check_card_number($card_value) ;
      if ($card_exists)
      {   $this->add_element('success','ajax_to_modal_window') ;
          $this->add_element('modal_panel_width','800px') ;
          echo 'Указанный Вами номер карты (<strong>'.$card_value.'</strong>) уже используется:' ;
          search_member_by_card($card_value) ;
          return ;
      } */

      $member_exists=$this->check_name_doc_number($name1,$pass1,$pass2) ;
      if ($member_exists)
      {   $this->add_element('success','ajax_to_modal_window') ;
          $this->add_element('modal_panel_width','800px') ;
          echo 'Такая фамилия и номер документа (<strong>'.$name1.', '.$pass2.'</strong>) уже есть в базе:' ;
          search_member_by_name1_pass2($name1,$pass2) ;
          return ;
      }

      $member_rec=array(    'enabled'   =>  1,
                        'parent'    =>  1,
                        'clss'      =>  200,
                        'obj_name'  =>  $_POST['f1'].' '.$_POST['f2'].' '.$_POST['f3'],
                        'name1'     =>  $name1, // фамилия
                        'name2'     =>  $name2, // имя
                        'name3'     =>  $name3, // отчество
                        'pass1'     =>  $pass1, // серия
                        'pass2'     =>  $pass2, // номер
                        'card'      =>  $comment, // карта
                        'ID_melbet' =>  $ID_melbet, // карта
                        'ID_fon'    =>  $ID_fon, // карта
                        'ID_zenit'  =>  $ID_zenit, // карта
                        'ID_win'  =>  $ID_win, // карта

                        'doc'       =>  ($_POST['doс'])? $_POST['doс']:1, // тип документа
                        'phone'     =>  $_POST['phone'],
                        'submit'    =>  $_POST['client_submit'],
                        'typ'       =>  $_POST['client_type'],
                        'creator_id'       =>  $_SESSION['member']->id,
                        'check_account'       =>  $_SESSION['member']->id,
                        'check_data'       =>  time()
                    );

      $member_id=adding_rec_to_table('obj_site_members',$member_rec) ; // создаем клиента
      _event_reg('Добавление клиента','',$member_id.'.104') ;

      $this->append_img_to_member($member_id,$_POST['images']) ; // прикрепляем к записи уже загруженные изображения

      $this->go_url='/cab/personal/person/'.$member_id.'/' ;

    }



    // прикрепляем к записи уже загруженные изображения
    function append_img_to_member($member_id,$arr_names)
    { if (sizeof($arr_names)) foreach($arr_names as $img_name)
      {   // копируем изображение на FTP резервной копии
          //copy(_DIR_TO_ROOT._TEMP_UPLOAD_DIR.$img_name,_FTP_RES_COPY_IMG.'/'.$img_name) ;
          if (file_exists(_DIR_TO_ROOT._TEMP_UPLOAD_DIR.$img_name))
          { // просто переносим исходное изображение и готовые превьюшку в папку изображений клиентов
            $new_obj_rec=array('parent'=>$member_id,'file_name'=>$img_name,'obj_name'=>$img_name) ;
            $new_obj_rec=_CLSS(3)->obj_create($new_obj_rec,array('debug'=>0,'tkey'=>107,'no_reg_events'=>1)) ;
            $image_name=execSQL_value('select file_name from '._DOT(107)->table_name.' where clss=3 and enabled=1 and not (file_name="" or file_name is null) and parent='.$member_id.' order by indx limit 1') ;
            update_rec_in_table('obj_site_members',array('_image_name'=>$image_name),'pkey='.$member_id) ;
            rename(_DIR_TO_ROOT._TEMP_UPLOAD_DIR.$img_name,_DOT(107)->dir_to_file.'source/'.$img_name) ;
            rename(_DIR_TO_ROOT._TEMP_UPLOAD_DIR.'200_'.$img_name,_DOT(107)->dir_to_file.'200/'.$img_name) ;
            rename(_DIR_TO_ROOT._TEMP_UPLOAD_DIR.'small_'.$img_name,_DOT(107)->dir_to_file.'small/'.$img_name) ;
            _event_reg('Добавление изображения','',$member_id.'.104',$new_obj_rec['_reffer']) ;
            //if (!$member->id)
          }
      }
    }



 function personal_create()
 {
   $reffer=PERSONAL()->personal_create($_POST) ;
   $this->add_element('close_mBox_window','all') ;
   if ($reffer)     { $rec_personal=PERSONAL()->get_personal_by_id($reffer) ;
                      switch($_POST['after_create'])
                       { case 'list': if (!$_POST['to_extlist']) $this->add_element('go_url','/cab/personal/') ;
                                      else                       $this->add_element('go_url','/cab/personal/extlist/') ;
                                      $_SESSION['after_personal_create']='list' ;
                                      break ;
                         case 'card': ob_start() ;
                                      ?><script type="text/javascript">var newWin=window.open("<?echo $rec_personal['__href']?>",'win_second','status=1,dependent=1,width=1920,height=1000,left=1920,top=200, resizable=1, scrollbars=yes') ;newWin.focus() ;</script><?
                                      $_SESSION['after_personal_create']='' ;
                                      $this->add_element('JSCODE',ob_get_clean()) ;
                                      break ;
                         case 'photo': if ($rec_personal['pkey'])
                                        { include(_DIR_EXT.'/webcamera_capture/panel_photo_capture.php') ;
                                          panel_photo_capture(array('title'=>'Фото сотрудника','save_cmd'=>'save_personal_photo','rec'=>$rec_personal)) ;
                                          $this->add_element('show_modal_window','HTML',array('width'=>'700px','after_close_func'=>'stop_video_capture')) ;
                                        }
                                       $_SESSION['after_personal_create']='photo' ;
                                       break ;
                       }
                    }
    else $this->add_element('show_notife','Не удалось добавить нового сотрудника',array('type'=>'error')) ;

 }

 function personal_save()
 { if (!is_member_right('personal_change_propusk')) {unset($_POST['propusk']) ; $this->add_element('show_notife','Нет прав для измерения номера пропуска',array('type'=>'alert')) ; }
   $res=PERSONAL()->personal_save($_POST['reffer'],$_POST) ;
   //damp_array($_POST) ; damp_array($res) ; $this->add_element('success','modal_window') ; ; return ;

   if ($res['update'])                       { //$this->add_element('show_notife','Сохранено',array('type'=>'info')) ;
                                               $this->update_page=1 ;
                                             }
   else if($res['found'] and !$res['update'])  { $this->add_element('show_notife','Изменений не произведено',array('type'=>'info')) ; $this->add_element('close_mBox_window','all') ; }
   else                                        { $this->add_element('show_notife','Не удалось сохранить изменения',array('type'=>'info')) ; $this->add_element('close_mBox_window','all') ; }
 }


}