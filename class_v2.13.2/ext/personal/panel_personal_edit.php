<?

function panel_personal_edit($options=array())
{
  $rec=$options['rec'] ;
  //damp_array($rec) ;
  ?><div id="panel_personal_edit" <?if ($rec['_reffer']) echo 'reffer="'.$rec['_reffer'].'"'?>>
    <form test="aaa">
    <?if ($options['to_extlist']){?><input type="hidden" name="to_extlist" value="1"><?}?>
    <table id="panel_personal_edit" class="info">
     <?if ($_SESSION['LS_person_use_propusk'])
        {?><tr><td>Номер пропуска:</td><td><?if (is_member_right('personal_change_propusk')){?><input type="text" name="propusk" id="propusk" is_checked="aspmo/check_propusk_to_unique" data-required lang="LAT" case="UPPER" value="<?echo addslashes($rec['propusk'])?>">
                 <?if ($rec['pkey']){?><button class=v2 cmd="aspmo_personal/leave_propusk" no_send_form="1" NoSubmitFormOnEnter=1 id="<?echo $rec['pkey']?>">Освободить пропуск</button><?}?>
                 <div class=status></div><?} else echo $rec['propusk'];?></td></tr>
        <?}?>
     <?if ($_SESSION['LS_person_use_driving']){?><tr><td>Номер водительского удостоверения:</td><td><?if (is_member_right('personal_change_propusk')){?><input type="text" name="driving" id="driving" _is_checked="aspmo/check_propusk_to_unique" value="<?echo addslashes($rec['driving'])?>"> <div class=status></div><?} else echo $rec['driving'];?></td></tr><?}?>
     <?if ($_SESSION['LS_person_use_tab_number']){?><tr><td>Табельный номер:</td><td><?if (is_member_right('personal_change_propusk')){?><input type="text" name="tab_number" id="tab_number" _is_checked="aspmo/check_propusk_to_unique"  value="<?echo addslashes($rec['tab_number'])?>"> <div class=status></div><?} else echo $rec['tab_number'];?></td></tr><?}?>
     <tr><td>ФИО:</td><td><input type="text" name="name" class="big" data-required value="<?echo addslashes($rec['obj_name'])?>" lang="RUS" ></td></tr>
     <?if ($_SESSION['LS_person_use_dr']){?><tr><td>Дата рождения:</td><td><input type="text" name="dr" data-required value="<?echo addslashes($rec['dr'])?>" mask="99.99.9999"></td></tr><?}?>
     <tr><td>Пол:</td><td><label><input type="radio" name="pol" value="0" <?if (!$rec['pol']) echo 'checked'?>> Мужской</label>
                          <label><input type="radio" name="pol" value="1" <?if ($rec['pol']) echo 'checked'?>> Женский</label>
                     </td>
     </tr>
     <?if ($_SESSION['LS_person_use_adres']){?><tr><td>Адрес:</td><td><textarea name="adres"><?echo htmlspecialchars($rec['adres'])?></textarea></td></tr><?}?>
     <?if ($_SESSION['LS_person_use_org']){?><tr><td>Предприятие:</td><td><select name="org_id"  id="org_id"><?
                $recs=PERSONAL()->get_list_personal_section() ; // получаем список организаций
                if (sizeof($recs)) foreach($recs as $rec_list) {?><option value="<?echo $rec_list['pkey']?>" <?if ($rec['_org_id']==$rec_list['pkey']) echo 'selected'?>><?echo $rec_list['obj_name']?></option><?}

     ?></select></td></tr><?}?>
     <?if (isset(PERSONAL()->tree[$rec['parent']])) {$arr_orgs=PERSONAL()->tree[$rec['parent']]->get_arr_parent() ; $cnt_levels=sizeof($arr_orgs) ; }
       if ($_SESSION['LS_person_use_otdel'])
         {?><tr><td>Подразделение:</td>
                <td><div style="position:relative;top:0;left:0;">
                <?if ($cnt_levels<=3){?><input type="text" id="otdel_name" name="otdel_name" class="big" SetAutoComplete cmd="autocomplette_otdel" value="<?echo $rec['_otdel_name']?>"><?}else echo $rec['_otdel_name'];?>
               </div></td></tr><?}
       if ($_SESSION['LS_person_use_working']){?><tr><td>Должность:</td><td><div style="position:relative;"><input type="text" id="working_name" name="working_name" class="big"  SetAutoComplete cmd="autocomplette_working" value="<?echo addslashes($rec['_work_name'])?>"></div></td></tr><?}?>
     <tr><td>Проход всегда:</td><td><div style="position:relative;"><input type="checkbox"  name="full_access"  value="1" <?if ($rec['full_access']) echo 'checked'?>></div></td></tr>
     <?if (MEMBER()->rol==1){?><tr><td>Без ограничений по МО:</td><td><div style="position:relative;"><input type="hidden"  name="demo_access"  value="0"><input type="checkbox"  name="demo_access"  value="1" <?if ($rec['demo_access']) echo 'checked'?>></div></td></tr><?}?>
     <tr><td>Смена:</td><td>с <input type="text" name="smena_s" value="<?echo addslashes($rec['smena_s'])?>" mask="99:99" class="small"> по <input type="text" name="smena_e" value="<?echo addslashes($rec['smena_e'])?>" mask="99:99" class="small">
         </td>
     </tr>
     <?if ($rec['pkey']){?><tr><td>Уволен:</td><td><input type="checkbox"  name="fired"  value="1" <?if (!$rec['enabled']) echo 'checked'?>> <?if ($rec['fired_data']) echo date('d.m.Y H:i',$rec['fired_data'])?></td></tr><?}?>
     <?if ($options['save_cmd']=='personal_create'){?><tr><td>После создания перейти</td>
             <td><label><input type="radio" name="after_create" value="list" <?if ($_SESSION['after_personal_create']=='list') echo 'checked'?>> общему списку </label><br>
                 <label><input type="radio" name="after_create" value="card" <?if (!$_SESSION['after_personal_create']) echo 'checked'?>> мед.карточке </label><br>
                 <label><input type="radio" name="after_create" value="photo" <?if ($_SESSION['after_personal_create']=='photo') echo 'checked'?>> фотографированию </label>
             </td></tr><?}?>

   </table>
   <div class="buttons">
       <input type="submit" class="v2" cmd="aspmo_personal/<?echo $options['save_cmd']?>" value="Сохранить" validate="form" NoSubmitFormOnEnter="1">
       <!--<button class="v2 fa fa-check-circle-o" cmd="aspmo_personal/<?echo $options['save_cmd']?>" _validate="form">Сохранить</button>-->
       <button class="cancel fa fa-times">Отмена</button>
   </div>
   <script type="text/javascript">$j(document).ready(function ()
         {  $j('select#org_id').live('change',function(){$j('#otdel_name').val('')}) ;
            $j('input#propusk').ajax_check({content_class:'alert',position:{x: ['right', 'outside'],y:'top'}}) ;
            $j('input[SetAutoComplete]').each(input_autoComplete) ;
         }) ;</script>
   </form></div><?
}





?>