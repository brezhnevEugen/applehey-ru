<?
function panel_personal_info_small($rec,$options=array())
{ ?><div id="panel_personal_info" reffer="<?echo $rec['_reffer']?>"><?
   ?><table class="info">
     <tr><td>ФИО:</td><td><strong><?echo $rec['obj_name']?></strong></td></tr>
     <tr><td>Пол:</td><td><?echo (!$rec['pol'])? 'Мужской':'Женский'?></td></tr>
     <?if ($_SESSION['LS_person_use_dr']){?><tr><td>Дата рождения:</td><td><?echo $rec['dr']?></td></tr><?}?>
     <?if ($_SESSION['LS_person_use_dr']){?><tr><td>Возраст:</td><td><?echo $rec['age']?></td></tr><?}?>
     <?if ($_SESSION['LS_person_use_org']){?><tr><td>Предприятие:</td><td><?echo $rec['_org_name']?></td></tr><?}?>
     <?if ($_SESSION['LS_person_use_otdel']){?><tr><td>Подразделение:</td><td><?echo $rec['_otdel_name']?></td></tr><?}?>
     <?if ($_SESSION['LS_person_use_working']){?><tr><td>Должность:</td><td><?echo $rec['_work_name']?></td></tr><?}?>
     <?if ($_SESSION['LS_person_use_propusk']){?><tr><td>Номер пропуска:</td><td><?echo $rec['propusk']?></td></tr><?}?>
     <?if ($_SESSION['LS_person_use_tab_number']){?><tr><td>Табельный номер:</td><td><?echo $rec['tab_number']?></td></tr><?}?>
     <?if ($_SESSION['LS_person_use_propusk']){?><tr><td>Телефон:</td><td><?echo $rec['phone']?></td></tr><?}?>
    </table>
    <div class="buttons"><?
             if ($options['button']['change_info'] and is_member_right('personal_change_info')) {?><button class="v2 fa fa-pencil-square-o" cmd="aspmo_personal/get_panel_personal_edit_info">Редактировать</button><?}
             if ($options['button']['change_photo'] and is_member_right('personal_change_photo')) {?><button class="v2 fa fa-camera" cmd="webcamera_capture/get_panel_photo_capture">Фото c камеры</button><?}
             if ($options['button']['change_photo'] and is_member_right('personal_change_photo')) {?><button class="v2 fa fa-camera" cmd="aspmo_personal/get_panel_upload_photo">Загрузить фото</button><?}
             if ($options['button']['pupilometr'] and $rec['_pupilomeria_cnt']){?><button class="v1  fa fa-eye"  href="<?echo $rec['__href'].'pp/'; ?>">Пупиллометрия</button><?}
             if ($options['button']['med_card']){?><button class="v1" href="<?echo $rec['__href']?>">Медицинская карточка сотрудника</button><?}
   ?></div></div><?
  //damp_array($rec) ;
}

?>