<?
include_once('i_pp.php');

class c_page_pChart_graf_pressure_ball
{

   function show(&$options=array())
   {   $dir_to_pChart=_DIR_TO_ADDONS.'/pChart2.1.4' ;
       /* CAT:Line chart */
       include($dir_to_pChart."/class/pData.class.php");
       include($dir_to_pChart."/class/pDraw.class.php");
       include($dir_to_pChart."/class/pImage.class.php");

       //----------------------------------------------------------------------------------------------------------------------------------

        //$recs=execSQL('select pkey as id,c_data,ball_s,ball_d from obj_site_mo_session where ball_s>0 and ball_d>0 and member_id='.$this->rec_personal['pkey'].' order by pkey desc') ;
        $personal_id=$_GET['personal_id'] ;

        $recs_s=execSQL_row('select c_data,ball_s from obj_site_mo_session where ball_s>0 and ball_d>0 and member_id='.$personal_id.' order by pkey desc') ;
        $recs_d=execSQL_row('select c_data,ball_d from obj_site_mo_session where ball_s>0 and ball_d>0 and member_id='.$personal_id.' order by pkey desc') ;
        //print_2x_arr($recs) ;

       /* Create and populate the pData object */
        $MyData = new pData();
        //for($i=0;$i<=30;$i++) { $MyData->addPoints(rand(1,15),"Probe 1"); }
        $MyData->addPoints($recs_s,"systolic");
        $MyData->setSerieTicks("Probe 2",4);
        $MyData->setAxisName(0,"Temperatures");

        /* Create the pChart object */
        $myPicture = new pImage(1100,500,$MyData);

        /* Turn of Antialiasing */
        $myPicture->Antialias = FALSE;

        /* Add a border to the picture */
        $myPicture->drawGradientArea(0,0,1100,500,DIRECTION_VERTICAL,array("StartR"=>240,"StartG"=>240,"StartB"=>240,"EndR"=>180,"EndG"=>180,"EndB"=>180,"Alpha"=>100));
        $myPicture->drawGradientArea(0,0,1100,500,DIRECTION_HORIZONTAL,array("StartR"=>240,"StartG"=>240,"StartB"=>240,"EndR"=>180,"EndG"=>180,"EndB"=>180,"Alpha"=>20));

        /* Add a border to the picture */
        $myPicture->drawRectangle(0,0,1099,499,array("R"=>0,"G"=>0,"B"=>0));

        /* Write the chart title */
        $myPicture->setFontProperties(array("FontName"=>$dir_to_pChart."/fonts/Forgotte.ttf","FontSize"=>11));
        $myPicture->drawText(150,35,"Ball of pressure",array("FontSize"=>20,"Align"=>TEXT_ALIGN_BOTTOMMIDDLE));

        /* Set the default font */
        $myPicture->setFontProperties(array("FontName"=>$dir_to_pChart."/fonts/pf_arma_five.ttf","FontSize"=>6));

        /* Define the chart area */
        $myPicture->setGraphArea(60,40,1050,450);

        /* Draw the scale */
        $scaleSettings = array("XMargin"=>10,"YMargin"=>10,"Floating"=>TRUE,"GridR"=>200,"GridG"=>200,"GridB"=>200,"GridAlpha"=>100,"DrawSubTicks"=>TRUE,"CycleBackground"=>TRUE);
        $myPicture->drawScale($scaleSettings);

        /* Write the chart legend */
        $myPicture->drawLegend(640,20,array("Style"=>LEGEND_NOBORDER,"Mode"=>LEGEND_HORIZONTAL));

        /* Turn on Antialiasing */
        $myPicture->Antialias = TRUE;

        /* Enable shadow computing */
        $myPicture->setShadow(TRUE,array("X"=>1,"Y"=>1,"R"=>0,"G"=>0,"B"=>0,"Alpha"=>10));

        /* Draw the area chart */
        $Threshold = "";
        $Threshold[] = array("Min"=>0,"Max"=>1,"R"=>250,"G"=>0,"B"=>0,"Alpha"=>100);
        $Threshold[] = array("Min"=>1,"Max"=>2,"R"=>250,"G"=>88,"B"=>0,"Alpha"=>100);
        $Threshold[] = array("Min"=>2,"Max"=>3,"R"=>250,"G"=>176,"B"=>0,"Alpha"=>100);
        $Threshold[] = array("Min"=>3,"Max"=>4,"R"=>250,"G"=>235,"B"=>0,"Alpha"=>100);
        $Threshold[] = array("Min"=>4.5,"Max"=>5.5,"R"=>0,"G"=>250,"B"=>0,"Alpha"=>100);
        $Threshold[] = array("Min"=>5.5,"Max"=>6,"R"=>6,"G"=>250,"B"=>0,"Alpha"=>100);
        $Threshold[] = array("Min"=>6,"Max"=>7,"R"=>206,"G"=>250,"B"=>0,"Alpha"=>100);
        $Threshold[] = array("Min"=>7,"Max"=>8,"R"=>250,"G"=>235,"B"=>0,"Alpha"=>100);
        $myPicture->setShadow(TRUE,array("X"=>1,"Y"=>1,"R"=>0,"G"=>0,"B"=>0,"Alpha"=>20));
        $myPicture->drawAreaChart(array("Threshold"=>$Threshold));

        /* Draw a line chart over */
        $myPicture->drawLineChart(array("ForceColor"=>TRUE,"ForceR"=>0,"ForceG"=>0,"ForceB"=>0));

        /* Draw a plot chart over */
        $myPicture->drawPlotChart(array("PlotBorder"=>TRUE,"BorderSize"=>1,"Surrounding"=>-255,"BorderAlpha"=>80));

        /* Write the thresholds */
        $myPicture->drawThreshold(5,array("WriteCaption"=>TRUE,"Caption"=>"Warn Zone","Alpha"=>70,"Ticks"=>2,"R"=>0,"G"=>0,"B"=>255));
        $myPicture->drawThreshold(10,array("WriteCaption"=>TRUE,"Caption"=>"Error Zone","Alpha"=>70,"Ticks"=>2,"R"=>0,"G"=>0,"B"=>255));

        /* Render the picture (choose the best way) */
        $myPicture->autoOutput($dir_to_pChart."/pictures/example.drawAreaChart.threshold.png");

        //--------------------------------------------------------------------------------------------------------------------------------

        //$MyData->setAxisName(0,"Temperatures");
        //$MyData->addPoints(array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"),"Labels");
        //!!!
        //$MyData->setSerieDescription("Labels","Время");
        //$MyData->setAbscissa("Labels");
        //$MyData->setAbscissaName("Время");







   }
}