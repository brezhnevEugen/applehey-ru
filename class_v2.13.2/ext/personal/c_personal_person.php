<?php
include_once(_DIR_TO_CLASS."/c_page.php");
class c_page_cab_personal_person extends c_page
{
  public $h1='Карточка клиента' ;
  public $rec_personal=array() ;
  public $fancyupload=1 ;

 function set_head_tags()
 { parent::set_head_tags() ;
   if ($this->fancyupload)
   { // fancyupload
       $this->HEAD['js'][]=_PATH_TO_EXT.'/fancyupload-v3.0.1/source/Swiff.Uploader.js' ;
       $this->HEAD['js'][]=_PATH_TO_EXT.'/fancyupload-v3.0.1/source/Fx.ProgressBar.js' ;
       $this->HEAD['js'][]=_PATH_TO_EXT.'/fancyupload-v3.0.1/source/FancyUpload2.js' ;
       $this->HEAD['js'][]=_PATH_TO_EXT.'/fancyupload-v3.0.1/fancyupload2.js' ;
       $this->HEAD['css'][]=_PATH_TO_EXT.'/fancyupload-v3.0.1/fancyupload.css"' ;
   }
 }

 function select_obj_info()
 { if (_CUR_ID)
   { $this->rec_personal=PERSONAL()->get_personal_by_id(_CUR_ID,array('get_ext_info'=>1)) ;
     if (!$this->rec_personal['pkey']) $this->result=404 ;
   }
 }

 function second_menu_config()
 { // вкладки для сотрудника
   if ($this->rec_personal['pkey'])
      { $this->second_menu[$this->rec_personal['__href']]='Карточка клиента' ;
        $this->second_menu[$this->rec_personal['__href'].'db/']='Исходные данные' ;
        $this->second_menu[$this->rec_personal['__href'].'history/']='История клиента' ;

        //damp_array($this->rec_personal) ;
        $this->page_type='window' ;
      }
   else parent::menu_config() ;
 }

 function get_cur_page_meta_tags()
 {
   if ($this->rec_personal['pkey']) $this->h1='Карточка клиента '.$this->rec_personal['obj_name'] ;
 }

 function block_main()
 { //$this->page_title() ;
   if (!$this->rec_personal['pkey']) {?><div class="alert">Клиент не найден</div><? return ;}
   ?><div id=panel_client><div id="panel_left"><?
        $this->panel_member_info() ;
       ?></div><div id="panel_right"><?
        $this->panel_member_images() ;
        $this->panel_fancyupload(array('class'=>'hidden'))
   ?></div><div class="clear"></div></div><?
     $this->panel_member_dublicates() ;
    ?><script type="text/javascript">
       $j('button.to_edit').click(function()
       { $j('div#panel_member_info textarea').removeAttr('disabled') ;
         $j('div#panel_member_info input[type="text"]').removeAttr('disabled') ;
         $j('div#panel_member_info input[type="submit"]').removeClass('hidden') ;
         $j('div#panel_member_info button.to_edit').addClass('hidden') ;
         $j('div#panel_fancyupload').removeClass('hidden') ;
         $j('div.list_images_highslide div.item').addClass('editing') ;
         $j('div.list_images_highslide div.item div.delete').removeClass('hidden') ;
         $j('div.list_images_highslide div.item div.rotate_l').removeClass('hidden') ;
         $j('div.list_images_highslide div.item div.rotate_r').removeClass('hidden') ;
         $j('div.list_images_highslide div.item div.set_main').removeClass('hidden') ;

         $j('div#doc_type').addClass('hidden') ;
         $j('div#select_doc_type').removeClass('unvisible') ;

         $j('div#client_type').addClass('hidden') ;
         $j('div#select_client_type').removeClass('unvisible') ;



       }) ;
       $j('input.submit').click(function(){ $j('div#panel_fancyupload #upload-list li.file.file-success').each(function() { $j('form#member_info').append('<input type=hidden name=images[] value="'+$j(this).attr('img_name')+'">') ;});})
     </script>
   <?
 }

 function panel_member_info()
 { global $member ;
   //$card_number=implode(' ',str_split($this->rec_personal['card'],4)) ;
   $card_number=$this->rec_personal['card'] ;
   //
   list($id,$fr)=each($_SESSION['IL_cat_clients']) ;
   if (!isset($_SESSION['IL_cat_clients'][$this->rec_personal['typ']])) $this->rec_personal['typ']=$fr['id'] ;
   ?><div id=panel_member_info><form id="member_info">
         <table><colgroup><col id="c1"><col id="c2"></colgroup>
           <tr><td>Фамилия:</td><td><input disabled type=text name=f1 id="f1" data-required value="<?echo htmlspecialchars($this->rec_personal['name1'])?>" is_checked="personal/check_name"></td></tr>
           <tr><td>Имя:</td><td><input disabled type=text name=f2 id="f2" data-required value="<?echo htmlspecialchars($this->rec_personal['name2'])?>" is_checked="personal/check_name"></td></tr>
           <tr><td>Отчество:</td><td><input disabled type=text name=f3 id="f3" data-required value="<?echo htmlspecialchars($this->rec_personal['name3'])?>" is_checked="personal/check_name"></td></tr>
           <tr><td><div id="doc_type"><?echo $_SESSION['IL_docs'][$this->rec_personal['doc']]['obj_name']?></div>
                   <div id="select_doc_type" class="unvisible"><select data-select name="doc"><? if (sizeof($_SESSION['IL_docs'])) foreach($_SESSION['IL_docs'] as $rec) {?><option value="<?echo $rec['id']?>" <?if ($this->rec_personal['doc']==$rec['id']) echo 'selected'?>><?echo $rec['obj_name']?></option><?}?></select></div>
               </td>
               <td>Серия <input disabled type=text name=p1 id=p1 value="<?echo htmlspecialchars($this->rec_personal['pass1'])?>" <?/* data-required data-validate="equally:4" data-number  is_checked="personal/check_pass1"*/?>>
                                    номер <input disabled type=text name=p2 id=p2 value="<?echo htmlspecialchars($this->rec_personal['pass2'])?>" data-required <?/* data-validate="equally:6;" data-number  is_checked="cpersonal/heck_pass2"*/?>></td></tr>
           <!--<tr><td>ID Мелбет:</td><td><input maxlength="7" disabled type=text name=ID_melbet id=ID_melbet  is_checked="personal/check_ID_melbet" value="<?echo htmlspecialchars($this->rec_personal['ID_melbet'])?>" ></td></tr>-->
           <tr><td>ID ФОН:</td><td><input maxlength="7" disabled type=text name=ID_fon id=ID_fon  is_checked="personal/check_ID_fon" value="<?echo htmlspecialchars($this->rec_personal['ID_fon'])?>"  ></td></tr>
           <!--<tr><td>ID Зенит:</td><td><input  maxlength="7" disabled type=text name=ID_zenit id=ID_zenit  is_checked="personal/check_ID_zenit" value="<?echo htmlspecialchars($this->rec_personal['ID_zenit'])?>" ></td></tr>-->
           <tr><td>ID Винлайн:</td><td><input  maxlength="7" disabled type=text name=ID_win id=ID_win  is_checked="personal/check_ID_win" value="<?echo htmlspecialchars($this->rec_personal['ID_win'])?>" ></td></tr>
           <tr><td>Телефон:</td><td><input disabled type=text name=phone id="phone" value="<?echo $this->rec_personal['phone']?>"> <?if($member->info['status']){?><label><input type="checkbox" name="client_submit" value="1" <?if ($this->rec_personal['submit']) echo 'checked'?>> Оповещать клиента</label><?}?></td></tr>
           <tr><td>Статус:</td><td><div id="client_type"><span class="a_<?echo $this->rec_personal['typ']?>"><?echo $_SESSION['IL_cat_clients'][$this->rec_personal['typ']]['obj_name']?></span></div>
                                      <div id="select_client_type" class="unvisible"><select data-select name="client_type"><? if (sizeof($_SESSION['IL_cat_clients'])) foreach($_SESSION['IL_cat_clients'] as $rec) {?><option value="<?echo $rec['id']?>" <?if ($rec['id']==$this->rec_personal['typ']) echo ' selected'?>><?echo $rec['obj_name']?></option><?}?></select></div></td></tr>
           <tr><td>Последнее посещение:</td><td id="check_info"><?if($this->rec_personal['check_data'])
                   {   $account_name=execSQL_value('select obj_name from obj_site_account where pkey='.$this->rec_personal['check_account']) ;
                       echo date('d.m.Y H:i',$this->rec_personal['check_data']).' ['.$account_name.']' ;
                   }
           ?></td></tr>
           <tr><td>Примечание:</td><td><textarea disabled name=card id=card><?echo htmlspecialchars($this->rec_personal['card'])?></textarea></td></tr>
           <?if (MEMBER()->rol==1 or MEMBER()->rol==3){?>
           <tr><td>Дополнительная ифнормация (видна только расширенным аккаунтам):</td><td><textarea disabled name=comment id=comment><?echo htmlspecialchars($this->rec_personal['comment'])?></textarea></td></tr>
           <?}?>
           <tr><td colspan="2"><br><div>
                       <button  class="v2" cmd="personal/set_member" member_id="<?echo $this->rec_personal['pkey']?>" >Посетил</button>&nbsp;&nbsp;&nbsp;
                       <button  class="button_green to_edit">Редактировать</button>
                       <input type=submit id="submit" class="hidden submit button_green v2" cmd=personal/save_member member_id="<?echo $this->rec_personal['pkey']?>" NoSubmitFormOnEnter value="Сохранить изменения" validate=form>&nbsp;&nbsp;&nbsp;
                       <?if ($member->info['status']){?><button class="button_red delete v2 float_right" cmd=personal/delete_member update_page=1 member_id="<?echo $this->rec_personal['pkey']?>" title="Удалить клиента из базы">Удалить</button><?}?>
                       <?$this->panel_card_change()?>
                   </div></td></tr>
         </table>
      </form>
     </div>
     <script type="text/javascript">
        $j('div#panel_member_info').ajax_check({content_class:'alert',position:{x: ['right', 'outside'],y:'top'}}) ;
        $j('div#panel_member_info input').live('focus',function(){ $j(this).removeClass('input_error') ;}) ;
        $j("div#panel_member_info #phone").mask("(999) 999-9999");
     </script>
     <style>
         div#client_type span.a_10{background: red;  color: white;  padding: 5px;}
     </style>
   <?
 }

 function panel_member_images()
 {  ?><div id="panel_member_images"><h2>Загруженные изображения</h2><?
    if (sizeof($this->rec_personal['obj_clss_3'])) print_template($this->rec_personal['obj_clss_3'],'highslide/list_images_highslide',(array('clone_small'=>200))) ;
    else echo "<br>Изображения не загружены" ;
    ?></div><?
 }

 function panel_member_dublicates()
 {  global $members_system ;
    $arr_ids=execSQL_line('select pkey from obj_site_members where pkey!='.$this->rec_personal['pkey'].' and name1="'.$this->rec_personal['name1'].'" and  name2="'.$this->rec_personal['name2'].'" and  name3="'.$this->rec_personal['name3'].'"') ;
    if (sizeof($arr_ids))
    { //PERSONAL()->show_list_items('clss=200','list_members',array('panel_select_pages'=>0,'panel_select_usl'=>0,'debug'=>0,'use_usl'=>'pkey in ('.implode(',',$arr_ids).')')) ;
      include_once(_DIR_EXT.'/reports/report_clients.php') ;
      $report_obj=new report_clients() ;
      $report_obj->title='Однофамильцы клиента' ;
      $report_obj->filter['ids']=implode(',',$arr_ids) ;
        $report_obj->view_as_HTML() ;
      //$report_obj->setting(array('filter_dir'=>'/cab/reports/report_clients/')) ;
    }

 }

 function panel_card_change()
 {  ?><div id=panel_card_change style="float:right;margin-right:10px;">
          <label class="label" onclick="$j(this).next().toggleClass('hidden');">Замена карточки</label>
          <div class="hidden"><button class="v2" cmd="personal/fixed_card_change" client_id="<?echo $this->rec_personal['pkey']?>">Зафиксировать</button></div>
      </div>
    <?
 }

}



?>