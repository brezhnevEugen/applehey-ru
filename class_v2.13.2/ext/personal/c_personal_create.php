<?php
include_once ("c_personal.php") ;
class c_page_cab_personal_create extends c_page_cab_personal
{
 public $h1='Добавление клиента' ;

    function set_head_tags()
      { parent::set_head_tags() ;
        // fancyupload
        $this->HEAD['js'][]=_PATH_TO_EXT.'/fancyupload-v3.0.1/source/Swiff.Uploader.js' ;
        $this->HEAD['js'][]=_PATH_TO_EXT.'/fancyupload-v3.0.1/source/Fx.ProgressBar.js' ;
        $this->HEAD['js'][]=_PATH_TO_EXT.'/fancyupload-v3.0.1/source/FancyUpload2.js' ;
        $this->HEAD['js'][]=_PATH_TO_EXT.'/fancyupload-v3.0.1/fancyupload2.js' ;
        $this->HEAD['css'][]=_PATH_TO_EXT.'/fancyupload-v3.0.1/fancyupload.css"' ;
     }

     function block_main()
     { $this->page_title() ;
       ?><div id=panel_client><div id="panel_left"><? $this->panel_member_info() ;?></div><div id="panel_right"><? $this->panel_fancyupload() ; ?></div><div class="clear"></div></div>
         <script type="text/javascript">
           // переносим данные по изображениям в форму
           $j('input.submit').click(function(){ $j('div#panel_fancyupload #upload-list li.file.file-success').each(function() { $j('div#panel_member_info form').append('<input type=hidden name=images[] value="'+$j(this).attr('img_name')+'">') ;});})
         </script>
       <?
     }

     function panel_member_info()
     { ?><div id=panel_member_info><form id="member_info">
               <table>
                 <tr><td>Фамилия:</td><td><input type=text name=f1 id="f1" data-required value="" is_checked="personal/check_name"></td></tr>
                 <tr><td>Имя:</td><td><input type=text name=f2 id="f2" data-required value="" is_checked="personal/check_name"></td></tr>
                 <tr><td>Отчество:</td><td><input type=text name=f3 id="f3" data-required value="" is_checked="personal/check_name"></td></tr>
                 <tr><td><select data-select name="doc"><? if (sizeof($_SESSION['IL_docs'])) foreach($_SESSION['IL_docs'] as $rec) {?><option value="<?echo $rec['id']?>"><?echo $rec['obj_name']?></option><?}?></select></td>
                     <td>Серия <input type=text name=p1 id="p1" value="" <?/*data-required data-validate="equally:4" data-number is_checked="personal/check_pass1"*/?>>
                                          номер <input type=text name=p2 id="p2" value=""  data-required <?/*data-validate="equally:6;" data-number is_checked="personal/check_pass2"*/?>></td></tr>
                 <!--<tr><td>ID Мелбет:</td><td><input maxlength="7" type=text name=ID_melbet id=ID_melbet value="" is_checked="personal/check_ID_melbet"></td></tr>-->
                 <tr><td>ID ФОН:</td><td><input maxlength="7" type=text name=ID_fon id=ID_fon value="" is_checked="personal/check_ID_fon" ></td></tr>
                 <!--<tr><td>ID Зенит:</td><td><input  maxlength="7" type=text name=ID_zenit id=ID_zenit value="" is_checked="personal/check_ID_zenit" ></td></tr>-->
                 <tr><td>ID Винлайн:</td><td><input  maxlength="7" type=text name=ID_win id=ID_win value="" is_checked="personal/check_ID_win" ></td></tr>
                 <tr><td>Телефон:</td><td><input type=text name=phone value="" id="phone"> <label><input type="checkbox" name="personal/client_submit" value="1" checked> Оповещать клиента</label></td></tr>
                 <tr><td>Статус:</td><td><div id=select_client_type><select data-select name="client_type"><? if (sizeof($_SESSION['IL_cat_clients'])) foreach($_SESSION['IL_cat_clients'] as $rec) {?><option value="<?echo $rec['id']?>"><?echo $rec['obj_name']?></option><?}?></select></div></td>
                 <tr><td>Примечание:</td><td><textarea name=card value="" id="card"></textarea></td></tr>
                 <tr><td></td><td><br><input type=submit id="submit"  class="submit button_green v2" cmd=personal/append_member NoSubmitFormOnEnter value="Добавить клиента" validate=form></td></tr>
               </table>
            </form>
           </div>
           <script type="text/javascript">
            $j('div#panel_member_info').ajax_check({content_class:'alert',position:{x: ['right', 'outside'],y:'top'}}) ;
            $j('div#panel_member_info input').live('focus',function(){$j(this).removeClass('input_error')}) ;
            $j("div#panel_member_info #phone").mask("(999) 999-9999");
            //$j('div#panel_member_info input').keydown(enter_as_tab);
            //$j('div#panel_member_info select').keydown(enter_as_tab);


           </script>
       <?
     }



}



?>