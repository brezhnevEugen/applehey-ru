<?
function panel_list_personal($options=array())
{
  ?><div id="panel_list_personal"><?

      $_usl=array() ; $view_client=1 ; $title='' ; $arr_title=array() ; $cnt=0 ; $options_view=array() ;
      if ($options['client_id'])
      { $ids_orgs=execSQL_line('select pkey from obj_site_personals where clss=207 and enabled=1 and client_id='.MEMBER()->id) ;
        if (sizeof($ids_orgs)) foreach($ids_orgs as $org_id) if (isset(PERSONAL()->tree[$org_id])) $arr_parens_ids[]=PERSONAL()->tree[$org_id]->get_list_child() ;
        if (sizeof($arr_parens_ids)) $_usl[]='parent in ('.implode(',',$arr_parens_ids).')' ;
        else                         $_usl[]='pkey is null' ; // чтобы не показывать никого если у клиента нет ни одной организации - сортрудников не показываем
      }
      if ($options['view_ext'])     $_usl[]='ext=1';
      if ($options['view_fired'])   $_usl[]='enabled=0';
      else                          $_usl[]='enabled=1';
      if ($options['org_id'] and isset(PERSONAL()->tree[$options['org_id']]))  $_usl[]='parent in ('.PERSONAL()->tree[$options['org_id']]->get_list_child().')' ;
      if ($options['park_id'])      $_usl[]='parent='.$options['park_id'] ;
      if ($options['personal_id'])  $_usl[]='pkey in ('.$options['personal_id'].')' ;

      //damp_array($options,1,-1);
      //damp_array($_usl,1,-1);

      // формируеем заголовок
      if ($options['client_id'])           $arr_title['Клиент']=execSQL_value('select obj_name from obj_site_account where pkey='.$options['client_id']) ;
      if ($options['view_ext'])            $arr_title['Доп.списки']='Да' ;
      if ($options['view_fired'])          $arr_title['Уволенные']='Да' ;
      if ($options['org_id'])              $arr_title['Организация']=PERSONAL()->tree[$options['org_id']]->name ;
      if ($options['park_id'])             $arr_title['Подразделение']=PERSONAL()->tree[$options['park_id']]->name ;
      if ($options['personal_id_search'])  $arr_title['Поиск сотрудников по']=$options['personal_id_search'] ;

        $options['ext']='client' ;
        $options['cmd']='export_personals' ;
        $arr=array() ;
        if (sizeof($options)) foreach($options as $key=>$value)
        { $arr[]=$key.'='.$value ;
          $query=implode('&',$arr) ;
          $arr_title['Выгрузка в excel']='<a href="/XLS/?'.$query.'">Скачать</a>' ;
        }


      if (sizeof($_usl)) $usl=implode(' and ',$_usl) ; else $usl='' ;
      if (sizeof($arr_title))
      { ?><h3>Параметры показа:</h3><?
        ?><table class="basic fz_small left"><?
        foreach($arr_title as $title=>$value) {?><tr><td><?echo $title?>:</td><td><strong><?echo $value?></strong></td></tr><?}
        ?></table><?
      }
      $options_view['debug']=0 ;
      $options_view['panel_select_pages']=1 ;
      $options_view['order']=$_SESSION['list_members_sort_mode'] ;
      $options_view['order']=$_SESSION['list_members_sort_mode'] ;
      $options_view['pages_base_url']=$options['pages_base_url'] ;

      $cnt=execSQL_value('select count(pkey) from obj_site_personals where clss=211 and _enabled=1 and '.$usl) ;
      if ($cnt) PERSONAL()->show_list_items($usl,'personal/list_personal_table_img',$options_view) ;
      else      {?><div class="alert">Сотрудников не найдено</div><?}

      ?></div><?
      return($cnt) ;
}
?>