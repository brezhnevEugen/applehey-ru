<?php
include (_DIR_TO_ENGINE."/c_page_XML.php") ;
class c_page
{
   function c_page()
   {
       header("Content-type: application/octet-stream;charset=UTF-8") ;
       header("Content-Disposition: attachment; filename=\""._CUR_PAGE_NAME."\"");
       header("Content-Type: application/x-force-download; name=\""._CUR_PAGE_NAME."\"");
       header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
       header('Pragma: public');
   }

   function show()
   {
     echo file_get_contents(_DIR_TO_ROOT.'/public/members/source/'._CUR_PAGE_NAME) ;
   }


}
?>