<?php
include_once("c_setting.php");
class c_page_cab_setting_accounts extends c_page_cab_setting
{
  public $h1='Персонал' ;

  function block_main()
  { $this->page_title() ;
    $this->panel_members_list() ;
  }

  function second_menu_config()
  {
    $this->second_menu['/cab/setting/accounts/']=array('title'=>'Список персонала','class'=>'fa fa-user-md') ;
  }


  function panel_members_list()
   {  ?><div id="panel_members_list"><?
         $recs=ACCOUNTS()->get_list_members(array('and_disabled'=>1)) ;
         ?><table class="basic fz_small full auto"><tr><th>Состояние</th><th>Имя</th><th>Роль</th><th>Авторизация</th><th>IP</th><th>Примечание</th><th></th></tr><?
         if (sizeof($recs)) foreach($recs as $rec) if (MEMBER()->rol==1 or (MEMBER()->rol!=1 and $rec['rol']!=1)) {?>
          <tr><td><?echo ($rec['enabled'])? '<span class="green">включен</span>':'<span class="red">выключен</span>'?></td>
              <td class="left"><?echo htmlspecialchars($rec['obj_name'])?></td>
              <td><?echo $rec['_rol_name']?></td>
              <td class="left">
                  <?  if ($rec['time_login']) echo 'Вход:&nbsp;&nbsp; <strong>'.date('d.m.Y H:i:s',$rec['time_login']).'</strong><br>' ;
                      if ($rec['time_logout']) echo 'Выход: <strong>'.date('d.m.Y H:i:s',$rec['time_logout']).'</strong><br>' ;
                  ?>
              </td>
              <td><?echo $rec['ip']?></td>
              <td class="left"><?if ($rec['rol']==4)
                    {   $link_objs=get_linked_ids_to_rec($rec) ;
                        if ($link_objs)
                        { $arr_objs=execSQL_row('select pkey,obj_name from obj_site_account where pkey in ('.$link_objs.') and pkey!='.$rec['pkey']);
                          echo '<strong>Контролируемые объекты:</strong><br>'.implode('<br>',$arr_objs) ;
                        }

                    }
                  ?>
              </td>
              <td><button class="v1" href="/cab/setting/account/<?echo $rec['pkey']?>/">Редактировать</button></td>
           </tr>
       <?}?>
        </table><?
     ?></div><?
  }

}



?>