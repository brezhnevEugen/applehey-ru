<?php
include_once("c_setting_accounts.php");
class c_page_cab_setting_account extends c_page_cab_setting_accounts
{
  public $h1='Карточка персонала' ;
  public $rec_member ;

 function select_obj_info()
 { $cur_id=(_CUR_ID)? _CUR_ID:MEMBER()->id ;
   $this->rec_member=ACCOUNTS()->get_member_by_id($cur_id) ;
   if (!$this->rec_member['pkey']) $this->result=404 ;
   else if (MEMBER()->id==$cur_id) $this->allow_access_to_member=MEMBER()->id  ; // разрешаем доступ к странице для текущего пользователя
 }

 function second_menu_config()
 { parent::second_menu_config() ;
   if ($this->rec_member['pkey'])
   { $this->second_menu[$this->rec_member['__href']]=array('title'=>'Карточка персонала "'.$this->rec_member['obj_name'].'"','class'=>'fa fa-user-md') ;
     $this->second_menu[$this->rec_member['__href'].'pass/']=array('title'=>'Смена пароля','class'=>'fa fa-user-md') ;
     $this->second_menu[$this->rec_member['__href'].'sert/']=array('title'=>'Сертификат','class'=>'fa fa-user-md') ;
     if ($this->rec_member['rol']==4) $this->second_menu[$this->rec_member['__href'].'objects/']=array('title'=>'Объекты контроля','class'=>'fa fa-user-md') ;
   }
 }

 function block_main()
 { $this->page_title() ;
   $this->panel_member_edit() ;
 }

  function panel_member_edit()
    {  $rec=$this->rec_member ;  //damp_array($this->rec_member) ; //damp_array($_SESSION['member']) ;
       ?><div id="panel_member_edit"><form enctype="multipart/form-data">
        <table class="info">
         <?if (MEMBER()->id!=$rec['pkey']){?><tr><td>Состояние:</td><td><input type="hidden" name=member_edit[enabled] value="0"> <input type=checkbox name=member_edit[enabled] value="1" <?if ($this->rec_member['enabled']) echo 'checked'?>></td></tr><?}?>
         <tr><td>ФИО:</td><td><input data-required type="text" name="member_edit[obj_name]" class="big" value="<?echo htmlspecialchars($rec['obj_name'])?>"></td></tr>
         <tr><td>Роль:</td><td><?if (MEMBER()->id!=$rec['pkey']){?>
                                   <select  name=member_edit[rol] data-required data-select><option value=""></option>
                                      <?if (sizeof($_SESSION['ARR_roll'])) foreach($_SESSION['ARR_roll'] as $id=>$rec_rol){?><option value="<?echo $id?>" <?if ($id==$rec['rol']) echo ' selected'?>><?echo $rec_rol['obj_name']?></option><?}?>
                                   </select><?}else echo $rec['_rol_name']?></td></tr>
         <tr><td>Контактный email:</td><td><input type="text" name="member_edit[email]" class="big" value="<?echo htmlspecialchars($rec['email'])?>"></td></tr>
         <tr><td>Контактный телефон:</td><td><input type="text" name="member_edit[phone]" class="big" value="<?echo htmlspecialchars($rec['phone'])?>"></td></tr>
         <tr><td>Логин:</td><td><input data-required class=login type=text name=member_edit[login] value="<?echo addslashes($rec['login'])?>"></td></tr>
         <tr><td>Пароль:</td><td><?echo addslashes($rec['password'])?></td></tr>
         <tr><td>Рутокен Web:</td><td><?
                 //$token_IDs=execSQL_row('select pkey,token_ID from obj_site_account_out_150 where clss=150 and parent='.$rec['pkey']) ;
                 if (sizeof($token_IDs)) foreach($token_IDs as $token_pkey=>$token_ID) {?><div class="item" id="token_info_<?echo $token_pkey?>"><?echo $token_ID?> <a class="v2" cmd="rutokenweb/unregister_token" token_pkey="<?echo $token_pkey?>">Удалить токен</a></div><?}
                 ?></td></tr>
         <tr><td>Разрешить авторизацию по паролю при наличии токена:</td><td><input type="hidden" name="member_edit[auth_by_pass]" value="0"><input type="checkbox" name="member_edit[auth_by_pass]" value="1" <?if ($rec['auth_by_pass']) echo 'checked'?>></td></tr>
         <?if ($rec['rol']==5){?><tr><td>Серия путевого листа:</td><td><input data-required class=login type=text name=member_edit[ext_info][seria_PL] value="<?echo addslashes($rec['_ext_info']['seria_PL'])?>"></td></tr><?}?>
         <?if ($rec['rol']==7){?><tr><td>Серия счетов:</td><td><input data-required class=login type=text name=member_edit[ext_info][seria_INV] value="<?echo addslashes($rec['_ext_info']['seria_INV'])?>"></td></tr><?}?>
         <?if ($rec['rol']==7){?><tr><td>Серия актов:</td><td><input data-required class=login type=text name=member_edit[ext_info][seria_AKT] value="<?echo addslashes($rec['_ext_info']['seria_AKT'])?>"></td></tr><?}?>
         <tr><td>Подпись сотрудника:</td><td><input name="upload_sugnature[]" type="file" size="50"><br>Формат GIF, прозрачный фон, рекомендованный размер 300x150px</td>
            </tr>

         <tr><td></td><td><input type="submit" class=v1 cmd="save_member" value="Сохранить" validate="form"></td></tr>
       </table>
       </form></div><?
      //damp_array(MEMBER());
    }

  function CMD_save_member()
  { //damp_array($_POST['member_edit']) ;
    $res=ACCOUNTS()->member_save($this->rec_member['pkey'],$_POST['member_edit']) ;

    if (sizeof($_FILES))
     { // загрузка печати
       $recs_files=conver_FILES_to_recs($_FILES['upload_file']) ;  //damp_array($recs_files) ;
       if (sizeof($recs_files[0]))
         { if ($recs_files[0]['type']=='image/gif')
           { ?><h2>Загружаем изображение из файла "<?echo $recs_files[0]['name']?>"</h2><?
             include_once(_DIR_TO_ENGINE.'/admin/i_admin_system.php') ;
             execSQL_update('delete from obj_site_account_image where parent='.$this->rec_member['pkey'].' and alt="stamp"') ;
             $res_upload=obj_upload_image($this->rec_member['_reffer'],$recs_files[0],array('view_upload'=>1,'img_alt'=>'stamp')) ;
             //damp_array($res_upload) ;
             $res_upload=array('type'=>'success','no_default_panel'=>1) ;
             $res='ok' ;
           }
           else $res_upload=array('type'=>'error','code'=>'failed_type','text'=>'Неверный тип файла. Загрузите файл в формате GIF.') ;
         }

       // загрузка подписи
       $recs_files=conver_FILES_to_recs($_FILES['upload_sugnature']) ;  //damp_array($recs_files) ;
       if (sizeof($recs_files[0]))
         { if ($recs_files[0]['type']=='image/gif')
           { ?><h2>Загружаем изображение из файла "<?echo $recs_files[0]['name']?>"</h2><?
             include_once(_DIR_TO_ENGINE.'/admin/i_admin_system.php') ;
             execSQL_update('delete from obj_site_account_image where parent='.$this->rec_member['pkey'].' and alt="signature"') ;
             $res_upload=obj_upload_image($this->rec_member['_reffer'],$recs_files[0],array('view_upload'=>1,'img_alt'=>'signature')) ;
             //damp_array($res_upload) ;
             $res_upload=array('type'=>'success','no_default_panel'=>1) ;
             $res='ok' ;
           }
           else $res_upload=array('type'=>'error','code'=>'failed_type','text'=>'Неверный тип файла. Загрузите файл в формате GIF.') ;
         }

       // загрузка сертификата
         $recs_files=conver_FILES_to_recs($_FILES['upload_CRT']) ; // damp_array($recs_files) ;
         if (sizeof($recs_files[0]))
           { if ($recs_files[0]['type']=='application/x-x509-ca-cert')
             { ?><h2>Загружаем сертификат из файла "<?echo $recs_files[0]['name']?>"</h2><?
               include_once(_DIR_TO_ENGINE.'/admin/i_admin_system.php') ;
               execSQL_update('delete from obj_site_account_files where parent='.$this->rec_member['pkey'].' and obj_name="root_sertificat"') ;
               obj_upload_file($this->rec_member['_reffer'],$recs_files[0],array('view_upload'=>1,'new_obj_rec'=>array('obj_name'=>'root_sertificat'))) ;
               //damp_array($res_upload) ;
               $res_upload=array('type'=>'success','no_default_panel'=>1) ;
               $res='ok' ;
             }
             else $res_upload=array('type'=>'error','code'=>'failed_type','text'=>'Выбранный Вами файл не является сертификатом') ;
           }



       //else $res_upload=array('type'=>'error','code'=>'not_uploaded_file','text'=>'Не был выбран файл') ;
       if ($res_upload['type']=='error') echo '<div class="alert">'.$res_upload['text'].'</div>' ;
     }
     //else $res_upload=array('type'=>'error','code'=>'not_uploaded_file','text'=>'Не был выбран файл') ;

    //damp_array($_FILES) ;
    if (!$res) echo '<div class="red">Изменений не обнаружено</div>' ;
    else { echo '<div class="green">Данные сохранены</div>' ;
           $this->rec_member=ACCOUNTS()->get_member_by_id($this->rec_member['pkey']) ;
         }
  }


}



?>