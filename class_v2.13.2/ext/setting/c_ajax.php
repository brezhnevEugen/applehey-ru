<?
include_once(_DIR_TO_ENGINE.'/c_page_XML_AJAX.php') ;
include_once(_DIR_EXT.'/aspmo_rmt/i_mrt.php') ;
class c_page_ajax extends c_page_XML_AJAX
{
  /*
  function get_panel_member_edit()
  {  $rec_member=ACCOUNTS()->get_member_by_id($_POST['member_id']) ;
     include_once('panel_member_edit.php') ;
     panel_member_edit($rec_member) ;
     $this->add_element('show_modal_window','HTML',array('title'=>'Редактирование персонала')) ;
  } */

  function save_setting()
  {  if (sizeof($_POST['setting'])) foreach($_POST['setting'] as $name=>$value) update_site_setting($name,$value) ;
    $this->add_element('show_notife','Сохранено',array('type'=>'info')) ;
    send_cmd_reboot_client() ;
  }

  function change_right_member()
  {
    if ($_POST['checked']) ACCOUNTS()->allow_right_rol($_POST['rol_id'],$_POST['class_name']) ;
    else                   ACCOUNTS()->decline_right_rol($_POST['rol_id'],$_POST['class_name']) ;
    $this->add_element('show_notife','Сохранено',array('type'=>'info')) ;
    ENGINE()->send_cmd_reboot_client() ;
  }

  function member_create()
  { ob_start() ; //damp_array($_POST,1,-1) ;
    $res=ACCOUNTS()->member_create($_POST['member']) ;
    if ($res['error']=='login_exist') echo '<div class="alert">Данный логин уже используется. Вам необходимо выбрать другой логин для нового аккаунта.</div>' ;
    elseif ($res['error']=='rol_not_set') echo '<div class="alert">Не задана роль аккаунта</div>' ;
    else
    { echo '<div class="alert_info">Аккаунт успешно добавлен.</div>' ;
      echo '<table>'.
           '<tr><td>ФИО:</td><td>'.$res['name'].'</td></tr>'.
           '<tr><td>Логин:</td><td>'.$res['login'].'</td></tr>'.
           '<tr><td>Пароль:</td><td>'.$res['password'].'</td></tr>'.
           '</table><div class="buttons"><button class="v1" href="/cab/setting/members/">Вернуться к списку аккаунтов</button><button class="v1" href="/cab/setting/member/new/">Создать еще один аккаунт</button></div>' ;
      $this->add_element('form.HTML','');
    }
    $text=ob_get_clean() ;
    $this->add_element('result.HTML',$text) ;

    //$this->add_element('success','modal_window') ; damp_array($res,1,-1) ;
   }

   function member_delete()
   {
     if (sizeof($_POST['member'])) foreach($_POST['member'] as $id=>$post_rec) if ($post_rec['check']) $res[]=ACCOUNTS()->member_delete($id) ;
     $this->add_element('update_page',1) ;
   }

    function change_versions()
    { $cont=file_get_contents(_DIR_TO_ROOT.'/ini/patch.php') ;
      if ($_POST['_CLASS_VERS']) $cont=preg_replace('/["\']_CLASS_VERS["\'],\'(.+?)\'/','\'_CLASS_VERS\',\''.$_POST['_CLASS_VERS'].'\'',$cont) ;
      if ($_POST['_ENGINE_VERS']) $cont=preg_replace('/["\']_ENGINE_VERS["\'],\'(.+?)\'/','\'_ENGINE_VERS\',\''.$_POST['_ENGINE_VERS'].'\'',$cont) ;
      $res=file_put_contents(_DIR_TO_ROOT.'/ini/patch.php',$cont) ;
      if ($res) $this->add_element('update_page',1) ;
      else $this->add_element('show_notife','Не удалось внести изменения в файл конфигурации ('._DIR_TO_ROOT.'/ini/patch.php).',array('type'=>'error')) ;
    }


    function save_account_object()
    {   if ($_POST['checked']) create_link($_POST['account_reffer'],$_POST['object_reffer']) ;
        else                   delete_link($_POST['account_reffer'],$_POST['object_reffer']) ;
        $this->add_element('show_notife','Сохранено',array('type'=>'info')) ;
    }



}