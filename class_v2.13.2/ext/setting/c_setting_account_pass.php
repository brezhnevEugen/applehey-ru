<?php
include_once("c_setting_account.php");
class c_page_cab_setting_account_pass extends c_page_cab_setting_account
{
  public $h1='Смена пароля' ;

  function get_cur_page_meta_tags()
  {
    if ($this->rec_member['pkey']) $this->h1='Смена пароля' ;
  }


  function block_main()
  { $this->page_title() ;
    $this->panel_pass_change() ;
  }


 function panel_pass_change()
 { ?><div id="panel_pass_change"><form>
    <table class="info">
     <tr><td>Пароль:</td><td><input data-required class=password type=password name=member_edit[password] value=""></td></tr>
     <tr><td></td><td><input type="submit" class=v1 cmd="change_pass" value="Сохранить" validate="form"></td></tr>
   </table>
   </form></div><?
 }

 function CMD_change_pass()
 { if ($_POST['member_edit']['password']) $res=ACCOUNTS()->member_save($this->rec_member['pkey'],$_POST['member_edit']) ;
   if (!$res) echo '<div class="red">Изменений не обнаружено</div>' ;
   else { echo '<div class="green">Пароль изменен</div>' ;
          $this->rec_member=ACCOUNTS()->get_member_by_id($this->rec_member['pkey']) ;
        }
 }

}



?>