<?php
include_once("c_setting.php");

class c_page_cab_setting_update extends c_page_cab_setting
{
  public $h1='Обновления ПО' ;

  function second_menu_config()
    { $this->second_menu['/cab/setting/update/']=array('title'=>'Обновление','class'=>'fa fa-user-md') ;
      $this->second_menu['/cab/setting/update/history/']=array('title'=>'История версий','class'=>'fa fa-user-md') ;
    }


 function block_main()
 { $this->page_title() ;  //damp_array($_POST,1,-1) ;
   if (!$this->cmd_exec_result['no_default_panel'])
   { ?><table class="setting_update"><tr>
        <td><?$this->update_from_zip() ;?></td>
        <td><?$this->panel_vers_class_select() ;
     $this->panel_vers_engive_select() ;
            ?>
        </td>
        <td><?$this->panel_check_db() ;
     $this->panel_check_setting() ;
            ?>
        </td>
      </tr></table>
      <style type="text/css">
        table.setting_update{width:100%;table-layout:fixed;}
        table.setting_update > tbody > tr > td{vertical-align:top;border-left:1px dashed #aaaaaa;padding:0 15px 0 15px;}
        table.setting_update > tbody > tr > td:first-child{border-left:none;padding-left:0;}
      </style>
    <?
   }
 }

function update_from_zip()
 { set_time_limit(0) ;
   ?><h2>Загрузка обновлений с архива</h2><br>
     <strong>Настройки PHP, определяющие загрузку файлов</strong>
     <table class="basic fz_small">
         <tr><td class="left">Загрузка файлов разрешена</td><td><?echo ini_get('file_uploads')?></td></tr>
         <tr><td class="left">Максимальный размер файла, загружаемого через POST</td><td><?echo ini_get('post_max_size')?></td></tr>
         <tr><td class="left">Максимальный размер закачиваемого файла</td><td><?echo ini_get('upload_max_filesize')?></td></tr>
     </table><br>
      <form method="POST" enctype="multipart/form-data">
     <INPUT type=file name='source_zip'><input type="submit" class=v1  cmd="update_from_zip_apply" value="Загрузить обновления"></form>
     <div class="alert">Внимание! Версия загружаемого обновления не должна совпадать с текущей версией ЭСМО</div>
   <?
 }

 function panel_vers_class_select()
 { $dir_list=get_dir_list(_DIR_TO_ROOT.'/') ;
   $vers_class=array() ;
   if (sizeof($dir_list)) foreach($dir_list as $dir_name=>$dir_path) if (mb_strpos($dir_name,'class')!==false) $vers_class[$dir_name]=str_replace(array('class_v','class'),array('','1.0'),$dir_name) ;
   ?><h2>Версия скриптов ЭСМО</h2><?
      ?><form><table class="vers_info">
            <tr><td>Текущая версия скриптов ЭСМО</td><td class="vers"><?echo _CLASS_VERS?></td></tr>
             <? if (sizeof($vers_class)>1){?><tr><td>Изменить версию</td><td><select name="_CLASS_VERS" data-select><option></option><?foreach($vers_class as $dir_name=>$vers) if ($vers!=_CLASS_VERS) {?><option value="<?echo $vers?>"><?echo $vers?></option><?}?></select></td></tr>
            <tr><td><button class="v2" cmd="setting/change_versions">Изменить версию</button></td></tr><?}?>
        </table><?
   ?></form><?
 }

 function panel_vers_engive_select()
 { $dir_list=get_dir_list(_DIR_TO_ROOT.'/') ;
   $vers_engine=array() ;
   if (sizeof($dir_list)) foreach($dir_list as $dir_name=>$dir_path) if (mb_strpos($dir_name,'engine')!==false) $vers_engine[$dir_name]=str_replace(array('engine_v','engine'),array('','1.0'),$dir_name) ;
     ?><h2>Версия скриптов движка</h2><?
     ?><form><table class="vers_info">
         <tr><td>Текущая версия скриптов движка</td><td class="vers"><?echo _ENGINE_VERS?></td></tr>
         <? if (sizeof($vers_engine)>1){?><tr><td>Изменить версию</td><td><select name="_ENGINE_VERS" data-select><option></option><? foreach($vers_engine as $dir_name=>$vers) if ($vers!=_ENGINE_VERS) {?><option value="<?echo $vers?>"><?echo $vers?></option><?}?></select></td></tr>
         <tr><td><button class="v2" cmd="setting/change_versions">Изменить версию</button></td></tr><?}?>
     </table><?
    ?></form><?
 }

 function panel_check_setting()
 { ?><h2>Настройки ЕСМО</h2><?
   ?><form><input type="hidden" name="cmd" value="check_setting"><table class="vers_info">
         <tr><td><button class=v1 cmd="check_setting">Проверить базовые настройки</button></td></tr>
     </table><?
    ?></form><?
 }

 function CMD_check_setting()
 {
   global $__functions,$install_options ;
   $install_options['install_XML_export']=1 ;
   $install_options['install_create_table']=1 ;
   include_once(_DIR_TO_ROOT.'/ini/'._INIT_) ;
   include_once(_DIR_TO_ENGINE.'/admin/i_setup.php') ;
   //damp_array($__functions['install']) ;
   if (sizeof($__functions['install'])) foreach($__functions['install'] as $func_name) ${$func_name(0)} ;
 }

 function panel_check_db()
 { $dir_list=get_dir_list(_DIR_TO_ROOT.'/') ;
   $vers_engine=array() ;
   if (sizeof($dir_list)) foreach($dir_list as $dir_name=>$dir_path) if (mb_strpos($dir_name,'engine')!==false) $vers_engine[$dir_name]=str_replace(array('engine_v','engine'),array('','1.0'),$dir_name) ;
     ?><h2>База данных</h2><?
     ?><form><input type="hidden" name="cmd" value="check_db"><table class="vers_info">
         <tr><td><button class=v1 cmd="check_db">Проверить таблицы</button></td></tr>
     </table><?
    ?></form><?
 }

 function CMD_check_db()
 {
   $cnt_sql_cmd=0 ;
   include_extension('table/table_checked') ;
   ?><h1>Проверка структуры объектных таблиц</h1><?
   ?><table class=table_checked><?
    if (sizeof($_SESSION['descr_obj_tables'])) foreach ($_SESSION['descr_obj_tables'] as $obj) $cnt_sql_cmd+=table_checked($obj->pkey,array('no_update_descr_tables'=>1,'no_show_button'=>1,'show_in_outer_table'=>1)) ;
     if ($cnt_sql_cmd) {?><tr><td colspan=2><button class=button cmd=exec_sql_list_cmd mode=dialog>Выполнить выбранные операции</button></td><?}
   ?></table><?
   //return(array('no_default_panel'=>1)) ;
 }

function CMD_update_from_zip_apply()
{ include(_DIR_TO_ENGINE.'/AddOns/pclzip.lib.php') ;
  ?><h1>Обновление с архива</h1><?
  if (!is_dir(_DIR_TO_ROOT.'/updates/')) mkdir(_DIR_TO_ROOT.'/updates/') ;
  if (!is_dir(_DIR_TO_ROOT.'/updates/')) { echo 'Не удалось создать временный каталог для распаковки архива. Проверьте правка на /var/www/' ; return ; }
  $temp_file_dir=_DIR_TO_ROOT.'/updates/'.$_FILES['source_zip']['name'] ;
  set_time_limit(0) ;
  if (stripos($_FILES['source_zip']['name'],'zip')!==false)
    {   if (move_uploaded_file($_FILES['source_zip']['tmp_name'],$temp_file_dir))
     {  $zip = new ZipArchive;
        if ($zip->open($temp_file_dir) === TRUE)
           { $res=$zip->extractTo(_DIR_TO_ROOT);
          $zip->close();
             //var_dump($res) ;
          echo 'Загрузка обновления произведена успешно. Выберите новую версию ПО ниже:<br><br><br>';
        }
           else { echo 'ошибка'; }
     } else echo 'Не удается загрузить файл '.hide_server_dir($temp_file_dir).'<br>';
    } else echo 'Загруженный файл не является архивом<br>' ;
    return(array('no_default_panel'=>1)) ;
}

}



?>