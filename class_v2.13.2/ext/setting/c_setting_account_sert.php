<?php
include_once("c_setting_account.php");
class c_page_cab_setting_account_sert extends c_page_cab_setting_account
{
  public $h1='Сертификат' ;

 function set_head_tags()
 {parent::set_head_tags() ;
  $this->HEAD['js'][]=_PATH_TO_EXT.'/ecp/script.js' ;
 }

 function block_main()
 { $this->page_title() ;
   $this->panel_view_sertificat() ;
 }

 function panel_view_sertificat()
 { ?><div id="token_result"></div>
     <object type="application/x-rutoken-pki" id="plugin-object" width="0" height="0"><param name="onload" value="onPluginLoaded"/></object>
     <div id="panel_member_edit"><form enctype="multipart/form-data">
             <table class="info">
              <tr><td>Личный сертификат</td><td><?echo $this->rec_member['sert_name'].'<br>'.$this->rec_member['sert_id']?>&nbsp;&nbsp;&nbsp;<button class="v2" cmd="ecp/get_panel_select_sertificate" member_id="<?echo $this->rec_member['pkey']?>">Выбрать</button></td></tr>
              <tr><td>Корневой сертификат удостоверяющего центра:</td>
                  <td><?if ($this->rec_member['sert_file']){?><a href="<?echo hide_server_dir($this->rec_member['sert_file'])?>"><?echo hide_server_dir($this->rec_member['sert_file'])?></a><?}?></td>
              </tr>
              <tr><td>Загрузить новый корневой сертификат</td>
                  <td><input name="upload_CRT[]" type="file" size="50"></td>
              </tr>
              <tr><td></td><td><input type="submit" class=v1 cmd="save_member" value="Сохранить" validate="form"></td></tr>
            </table>
            </form></div>

     <?if ($this->rec_member['sert_file']){?>
     <div id="panel_view_sertificat"><h2>Информация по корневому сертификату сотрудика</h2><form><?
     exec('/usr/bin/openssl  x509 -in '.$this->rec_member['sert_file'].' -noout -text',$output);
     damp_array($output,1,-1) ;}
   ?></div><?
 }


  function CMD_save_member()
  { //damp_array($_POST['member_edit']) ;
    $res=ACCOUNTS()->member_save($this->rec_member['pkey'],$_POST['member_edit']) ;

    if (sizeof($_FILES))
     { // загрузка сертификата
         $recs_files=conver_FILES_to_recs($_FILES['upload_CRT']) ; // damp_array($recs_files) ;
         if (sizeof($recs_files[0]))
           { if ($recs_files[0]['type']=='application/x-x509-ca-cert')
             { ?><h2>Загружаем сертификат из файла "<?echo $recs_files[0]['name']?>"</h2><?
               include_once(_DIR_TO_ENGINE.'/admin/i_admin_system.php') ;
               execSQL_update('delete from obj_site_account_files where parent='.$this->rec_member['pkey'].' and obj_name="root_sertificat"') ;
               obj_upload_file($this->rec_member['_reffer'],$recs_files[0],array('view_upload'=>1,'new_obj_rec'=>array('obj_name'=>'root_sertificat'))) ;
               //damp_array($res_upload) ;
               $res_upload=array('type'=>'success','no_default_panel'=>1) ;
               $res='ok' ;
             }
             else $res_upload=array('type'=>'error','code'=>'failed_type','text'=>'Выбранный Вами файл не является сертификатом') ;
           }



       //else $res_upload=array('type'=>'error','code'=>'not_uploaded_file','text'=>'Не был выбран файл') ;
       if ($res_upload['type']=='error') echo '<div class="alert">'.$res_upload['text'].'</div>' ;
     }
     //else $res_upload=array('type'=>'error','code'=>'not_uploaded_file','text'=>'Не был выбран файл') ;

    //damp_array($_FILES) ;
    if (!$res) echo '<div class="red">Изменений не обнаружено</div>' ;
    else { echo '<div class="green">Данные сохранены</div>' ;
           $this->rec_member=ACCOUNTS()->get_member_by_id($this->rec_member['pkey']) ;
         }
  }


}



?>