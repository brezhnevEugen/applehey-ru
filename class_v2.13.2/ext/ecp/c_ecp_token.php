<?php
include_once('c_ecp.php') ;
class c_page_cab_ecp_token extends c_page_cab_ecp
{
 public $h1='ЭЦП - токен' ;

    function __construct()
       { $this->HEAD['favicon']='/favicon.png'  ;

         // стандартный JQuery
         $this->HEAD['js'][]=_PATH_TO_EXT.'/jquery/jquery-1.8.3.min.js' ;
         $this->HEAD['js'][]=_PATH_TO_EXT.'/jquery/jquery-ui-1.9.1.custom.min.js' ;
         $this->HEAD['js'][]=_PATH_TO_EXT.'/jquery/jquery.form.js' ;
         $this->HEAD['js'][]=_PATH_TO_EXT.'/jquery/jquery.timers-1.2.js' ;
         //$this->HEAD['js'][]=_PATH_TO_EXT.'/jquery/unserialize.jquery.latest.js' ;
         //$this->HEAD['js'][]=_PATH_TO_EXT.'/jquery/imgpreview.full.jquery.js' ; // http://james.padolsey.com/demos/imgPreview/full/
         //$this->HEAD['js'][]=_PATH_TO_EXT.'/jquery/jquery.maskedinput.min.js' ; // http://digitalbush.com/projects/masked-input-plugin/
         $this->HEAD['js'][]=_PATH_TO_EXT.'/jquery.inputmask-3.x/js/jquery.inputmask.js' ; // https://github.com/RobinHerbots/jquery.inputmask


         // mootools
         //$this->HEAD['js'][]=_PATH_TO_EXT.'/mootools/mootools-core-1.4.5-full.js' ;
         //$this->HEAD['js'][]=_PATH_TO_EXT.'/mootools/mootools-more-1.4.0.1.js' ;

         // библитека jBox + замена mBox
         //$this->HEAD['js'][]=_PATH_TO_EXT.'/mBox/jBox-0.3.1/Source/jBox.min.js' ;
         //$this->HEAD['css'][]=_PATH_TO_EXT.'/mBox/jBox-0.3.1/Source/jBox.css' ;

         //$this->HEAD['js'][]=_PATH_TO_EXT.'/mBox/StephanWagner-mForm-0.2.6/mForm.Core.js' ;
         //$this->HEAD['js'][]=_PATH_TO_EXT.'/mBox/StephanWagner-mForm-0.2.6/mForm.Element.js' ;
         //$this->HEAD['js'][]=_PATH_TO_EXT.'/mBox/StephanWagner-mForm-0.2.6/mForm.Element.Select.js' ;
         //$this->HEAD['js'][]=_PATH_TO_EXT.'/mBox/StephanWagner-mForm-0.2.6/mForm.Submit.js' ;
         //$this->HEAD['css'][]='/'._CLASS_.'/assets/mForm.css' ;
         //$this->HEAD['css'][]=_PATH_TO_EXT.'/mBox/StephanWagner-mForm-0.2.6/assets/mFormElement-Select.css' ;
         //$this->HEAD['js'][]=_PATH_TO_EXT.'/mBox/modal_window.jBox.js' ;


         //$this->HEAD['css'][]=_PATH_TO_ADDONS.'/font-awesome-4.3.0/css/font-awesome.css' ;

         //$this->HEAD['js'][]=_PATH_TO_ADDONS.'/printPage/jquery.printPage.js' ;

         // стандартное подключение highslide
         //$this->HEAD['js'][]=_PATH_TO_ADDONS.'/highslide/highslide-4.1.13/highslide.js' ;
         //$this->HEAD['css'][]=_PATH_TO_ADDONS.'/highslide/highslide-4.1.13/highslide.css' ;

         $this->HEAD['css'][]='/'._CLASS_.'/style.css' ;
         //$this->HEAD['js'][]='/'._CLASS_.'/script.js' ;


       }

 function block_main()
 { global $member ;
   //$this->panel_hand_TO() ;
   //$this->panel_list_TO() ;
     ?><script type="application/javascript" src="/class/ext/ecp/libs/jquery/1.7.1/jquery.min.js"></script>
       <script type="application/javascript" src="/class/ext/ecp/libs/jquery/1.7.1/jquery-base64.min.js"></script>
       <link rel="stylesheet" type="text/css" href="/class/ext/ecp/libs/jqueryui/1.8.16/themes/black-tie/jquery-ui.css">
       <script type="application/javascript" src="/class/ext/ecp/libs/jqueryui/1.8.16/jquery-ui.min.js"></script>

       <link rel="stylesheet" type="text/css" href="/class/ext/ecp/libs/syntaxhighligher/3.0.83/css/shCore.css">
       <link rel="stylesheet" type="text/css" href="/class/ext/ecp/libs/syntaxhighligher/3.0.83/css/shThemeFadeToGrey.css">
       <script type="application/javascript" src="/class/ext/ecp/libs/syntaxhighligher/3.0.83/js/shCore.js"></script>
       <script type="application/javascript" src="/class/ext/ecp/libs/syntaxhighligher/3.0.83/js/shBrushJScript.js"></script>


       <link rel="stylesheet" type="text/css" href="/class/ext/ecp/present.css">
       <script type="application/javascript" src="/class/ext/ecp/present2.js"></script>
     <div id="container">
         <div id="content">
             <div class="test-common ui-widget-content ui-corner-all" id="ui-controls">
                 <div class="test-common">
                     <label for="device-list">Доступные устройства: </label>
                     <select id="device-list" class="item-list">
                         <option>Обновите список устройств</option>
                     </select>
                     <button id="refresh-dev" class="refresh-btn">↓↑</button>
                 </div>
                 <div class="test-common">
                     <label for="device-list">PIN-код:</label>
                     <input id="device-pin" value="12345678" size="15"/>
                     <button id="login">Войти</button>
                     <button id="logout">Выйти</button>
                     <button id="save-pin">Сохранить PIN</button>
                     <button id="remove-pin">Удалить PIN</button>
                 </div>
                 <div class="test-common">
                     <label for="key-list">Ключи на устройстве: </label>
                     <select id="key-list" class="item-list">
                         <option>Выполните вход на устройство</option>
                     </select>
                     <button id="refresh-keys" class="refresh-btn">↓↑</button>
                </div>
                 <div class="test-common">
                     <label for="cert-list">Сертификаты на уcтройстве: </label>
                     <select id="cert-list" class="item-list">
                         <option>Обновите список сертификатов</option>
                     </select>
                     <button id="refresh-certs" class="refresh-btn">↓↑</button>
                </div>
             </div>
         </div>
         <button id="test333">Проверка</button>
         <div id="console"></div>
         <object type="application/x-rutoken-pki" id="plugin-object" width="0" height="0"><param name="onload" value="onPluginLoaded"/></object>
         <!--<object type="application/x-rutoken-pki" id="plugin"></object>-->
         <script type="text/javascript">
         $j(document).ready(function()
         {  $j('button#test333').live('click',function()
            {   console.log('test333 click') ;
                // получение объекта плагина
                //var plugin = document.getElementById("plugin-object");
                //console.log(window.plugin.outerHTML) ;
                //if (!window.plugin.valid) {alert("Couldn't load plugin");}
                // использование асинхронного интерфейса
                //plugin.enumerateDevices
                plugin.pluginObject.enumerateDevices
                ( function(devices)
                  {console.log(devices);
                   console.log('enumerateDevices_success');
                  },
                  function(error)
                  { console.log(error);
                    console.log('enumerateDevices_error');
                  }
                );

                var keys = Array();

                try
                {
                  //  plugin.login(deviceId, "12345678");
                   // keys = plugin.enumerateKeys(deviceId, null);
                }
                catch (error)
                {
                    //console.log(error);
                }

            })



            console.log('document_ready') ;

         }) ;
         </script>

     </div>

   <?

 }



}

?>