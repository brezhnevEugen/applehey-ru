<?
include_once(_DIR_TO_ENGINE.'/c_page_XML_AJAX.php') ;
class c_page_ajax extends c_page_XML_AJAX
{

    // старт ручного МО со РМВ - надо перевести врача на нужную страницу в новой вкладке
    function get_autorize_panel()
     { include_once('panel_pin_autorize.php') ;
       panel_pin_autorize() ;
       $this->add_element('show_modal_window','HTML',array('title'=>'Авторизация по Рутокен ЭЦП')) ;
     }

    function autorize_by_pin()
    { // подпись итоговой последовательности на выбранном пользователем сертификате в формате CMS attached
      $rec_member=execSQL_van('select * from obj_site_account where sert_id="'.$_POST['sert_id'].'"') ;
      if (!$rec_member['pkey'])
      { $this->add_element('panel_alert.HTML','Выбранный сертификат не зарегистрирован в системе') ;
        LOGS()->reg_log('TOKEN','Сертификат не зарегистрирован в системе') ;
        return ;
      }

      ob_start() ;
      LOGS()->reg_log('TOKEN','Вход в токен по PIN, сотрудник '.$rec_member['obj_name'],array('member_id'=>$rec_member['pkey'])) ;
      ?><script type="text/javascript">
           $j('div#panel_pin_autorize').html('Авторизация на токене') ;
           plugin.pluginObject.login(0, '<?echo $_POST['pin']?>',
             function()
             {$j('div#panel_pin_autorize').html('Токен авторизован<br>Формируем подпись для проверки на сервере<br><img src="/images/loading14.gif" width=500px><br>Ожидайте, формирование подписи занимает от 3-х до 4-х минут') ;
              send_ajax_request({cmd:'ecp/req_event','event':'PIN_autorize_success',member_id:<?echo $rec_member['pkey']?>}) ;
              var check_str="<?echo ENGINE()->get_rand_string(8)?>" ;

              plugin.pluginObject.authenticate(0, '<?echo $_POST['sert_id']?>',check_str,
                                                               function(sign)
                                                               { $j('div#panel_pin_autorize').html('Строка подписана, проверяем подпись') ;
                                                                 send_ajax_request({cmd:'ecp/req_event',event:'Подпись сформирована',data:sign,member_id:<?echo $rec_member['pkey']?>}) ;
                                                                 send_ajax_request({cmd:'ecp/check_sign',sign:sign,'check_str':check_str,member_id:<?echo $rec_member['pkey']?>}) ;
                                                               },
                                                               function()
                                                               { $j('div#panel_alert').html('Не удалось сформировать подпись') ;
                                                                 send_ajax_request({cmd:'ecp/req_event','event':'Не удалось сформировать подпись',member_id:<?echo $rec_member['pkey']?>}) ;
                                                               }
                                                          ) ;
             },
             function()
             {  $j('div#panel_alert').html('Ошибка авторизации на токене по PIN') ;
                send_ajax_request({cmd:'ecp/req_event','event':'Ошибка авторизации на токене по PIN',member_id:<?echo $rec_member['pkey']?>}) ;
             }
           );
        </script><?
      $text=ob_get_clean()  ;
      $this->add_element('JSCODE',$text) ;
    }

    function check_sign()
    { ob_start() ;
      $rec_member=ACCOUNTS()->get_member_by_id($_POST['member_id']) ;
      $check_id=LOGS()->reg_log('TOKEN','Подпись получена, проверяем подпись с использование корневого сертификата',array('member_id'=>$rec_member['pkey'])) ;
      $sign_dir=_DIR_TO_ROOT.'/sert/'.$check_id.'_sign.cms' ;
      $data_dir=_DIR_TO_ROOT.'/sert/'.$check_id.'_data.file' ;
      $seft_dir=_DIR_TO_ROOT.'/sert/'.$check_id.'_user.cr' ;
      file_put_contents($sign_dir,"-----BEGIN CMS-----\n".$_POST['sign']."-----END CMS-----") ;
      echo exec('/usr/bin/openssl cms -engine gost -verify -in  '.$sign_dir.' -inform PEM -CAfile '.$rec_member['sert_file'].' -out '.$data_dir.' -certsout '.$seft_dir,$output);
      LOGS()->reg_log('check_sign','/usr/bin/openssl cms -engine gost -verify -in  '.$sign_dir.' -inform PEM -CAfile '.$rec_member['sert_file'].' -out '.$data_dir.' -certsout '.$seft_dir,array('member_id'=>$rec_member['pkey'])) ;
      if (file_exists($data_dir))
      { $cont=file_get_contents($data_dir) ;
        $get_str=mb_substr($cont,0,8) ;
        LOGS()->reg_log('TOKEN','контрольный код:'.$_POST['check_str'].', излеченный из подписи код: '.$get_str,array('member_id'=>$rec_member['pkey'])) ;
        if ($get_str==$_POST['check_str'])
        {  // авторизуем пользователя
           if (MEMBER()->id) ACCOUNTS()->member_logout() ;
            ACCOUNTS()->member_autorize($rec_member,array('event_name'=>'Авторизация через Рутокен ЭЦП')) ;
            $this->add_element('go_url','/cab/') ;   // переходим на страницу личного кабинета
        }
        else echo 'ERROR' ;
      }

      $text=ob_get_clean()  ;
      $this->add_element('panel_alert.HTML',$text) ;
      //LOGS()->reg_log('TOKEN','Подпись проверена',array('member_id'=>$rec_member['pkey'])) ;
    }

    function req_event()
    { LOGS()->reg_log('TOKEN',$_POST['event'],array('member_id'=>$_POST['member_id'])) ;

    }


    function get_panel_select_sertificate()
    { include_once(_DIR_EXT.'/ecp/panel_select_sertificate.php') ;
      panel_select_sertificate(array('member_id'=>$_POST['member_id'])) ;
      $this->add_element('show_modal_window','HTML',array('width'=>600)) ;
    }

    function set_sert_id()
    {  ACCOUNTS()->member_save($_POST['member_id'],array('sert_id'=>$_POST['sert_id'])) ;
       $this->add_element('update_page',1) ;
    }


    function sign_doc()
    { $doc_id=$_POST['doc_id'] ;
      $doc_rec=DOCS()->get_doc_by_id($doc_id) ;
      $cont=file_get_contents($doc_rec['__doc_dir']) ;
      $str=base64_encode($cont) ;
      if ($_SESSION['LS_evulate_ECP'])
      { ?><div class="green center"><strong>Включен режим эмуляции использования ЭЦП</strong></div><br><div class=center><button class="v2" cmd="ecp/sign_doc_apply" sign="<?echo md5($str)?>" doc_id="<?echo $doc_id?>" sert_id="<? echo MEMBER()->info['sert_id']?>">Далее</button></div><?
        $this->add_element('show_modal_window','HTML') ;
      }
      else
      {ob_start()  ;
       ?><script type="text/javascript">
        var options = [] ;
        options.addSignTime = <?echo time()?>;
        options.useHardwareHash = false;
        options.detached = true ;
        options.addUserCertificate =  true ;

        //console.time("sign");
        console.log("HW", options.useHardwareHash);
        console.log("detached: ", options.detached);

        plugin.pluginObject.login(0, '12345678',
                             function()
                             {alert('Вход выполнен') ;
                             },
                             function()
                             { alert('Вход отклонен') ;
                             }
                           );

        plugin.pluginObject.sign(0,"<? echo MEMBER()->info['sert_id']?>", "<?echo $str?>", true, options,
                                 function (res) {console.log("success");   send_ajax_request({cmd:'ecp/sign_doc_apply',sign:res,doc_id:'<?echo $doc_rec['pkey']?>',sert_id:'<? echo MEMBER()->info['sert_id']?>'})},
                                 function(){console.log("error");alert('<?echo  $doc_rec['__doc_dir']?>:<?echo strlen($str)?>:<? echo MEMBER()->info['sert_id']?>')}
                                )
        /*
        plugin.pluginObject.login(0, '12345678',
                     function()
                     {$j('div#panel_pin_autorize').html('Токен авторизован<br>Формируем подпись для проверки на сервере<br><img src="/images/loading14.gif" width=500px><br>Ожидайте, формирование подписи занимает от 3-х до 4-х минут') ;
                      //send_ajax_request({cmd:'ecp/req_event','event':'PIN_autorize_success',member_id:<?echo $rec_member['pkey']?>}) ;
                      //var check_str="<?echo ENGINE()->get_rand_string(8)?>" ;

                      plugin.pluginObject.sign(0,"<? echo MEMBER()->info['sert_id']?>", "<?echo $str?>", true, options,
                                                  function (res) {console.log("success");   send_ajax_request({cmd:'ecp/sign_doc_apply',sign:res,doc_id:<?echo $doc_id?>,sert_id:"<? echo MEMBER()->info['sert_id']?>"})},
                                                  function(err){console.log("error:"+err);}
                                                 )
                     },
                     function()
                     {  //$j('div#panel_alert').html('Ошибка авторизации на токене по PIN') ;
                        //send_ajax_request({cmd:'ecp/req_event','event':'Ошибка авторизации на токене по PIN',member_id:<?echo $rec_member['pkey']?>}) ;
                     }
                   );
          */
        </script><?
       $text=ob_get_clean() ;
       $this->add_element('JSCODE',$text) ;
      }

    }


    function  sign_doc_apply()
    {  //$this->update_page() ;
       //$this->add_element('show_notife','Документ подписан') ;
       $this->add_element('update_page',1) ;
       $doc_rec=DOCS()->get_doc_by_id($_POST['doc_id']) ;
       if (strpos($doc_rec['doc_name'],'report_mo_smena')!==false) ACCOUNTS()->member_smena_out() ;
       //damp_array($_POST)  ;
       $sign=$_POST['sign'] ;
       $sert_id=$_POST['sert_id'] ;
        DOCS()->sign_doc($doc_rec['pkey'],$sert_id,$sign) ;

    }




}