<?php
include_once(_DIR_TO_CLASS.'/c_page.php') ;
class c_page_cab_ecp extends c_page
{
 public $h1='ЭЦП' ;

 function ___construct()
     { $this->HEAD['favicon']='/favicon.png'  ;

       // стандартный JQuery
       $this->HEAD['js'][]=_PATH_TO_EXT.'/jquery/jquery-1.8.3.min.js' ;
       $this->HEAD['js'][]=_PATH_TO_EXT.'/jquery/jquery-ui-1.9.1.custom.min.js' ;
       $this->HEAD['js'][]=_PATH_TO_EXT.'/jquery/jquery.form.js' ;
       $this->HEAD['js'][]=_PATH_TO_EXT.'/jquery/jquery.timers-1.2.js' ;
       //$this->HEAD['js'][]=_PATH_TO_EXT.'/jquery/unserialize.jquery.latest.js' ;
       //$this->HEAD['js'][]=_PATH_TO_EXT.'/jquery/imgpreview.full.jquery.js' ; // http://james.padolsey.com/demos/imgPreview/full/
       //$this->HEAD['js'][]=_PATH_TO_EXT.'/jquery/jquery.maskedinput.min.js' ; // http://digitalbush.com/projects/masked-input-plugin/
       $this->HEAD['js'][]=_PATH_TO_EXT.'/jquery.inputmask-3.x/js/jquery.inputmask.js' ; // https://github.com/RobinHerbots/jquery.inputmask


       // mootools
       //$this->HEAD['js'][]=_PATH_TO_EXT.'/mootools/mootools-core-1.4.5-full.js' ;
       //$this->HEAD['js'][]=_PATH_TO_EXT.'/mootools/mootools-more-1.4.0.1.js' ;

       // библитека jBox + замена mBox
       //$this->HEAD['js'][]=_PATH_TO_EXT.'/mBox/jBox-0.3.1/Source/jBox.min.js' ;
       //$this->HEAD['css'][]=_PATH_TO_EXT.'/mBox/jBox-0.3.1/Source/jBox.css' ;

       //$this->HEAD['js'][]=_PATH_TO_EXT.'/mBox/StephanWagner-mForm-0.2.6/mForm.Core.js' ;
       //$this->HEAD['js'][]=_PATH_TO_EXT.'/mBox/StephanWagner-mForm-0.2.6/mForm.Element.js' ;
       //$this->HEAD['js'][]=_PATH_TO_EXT.'/mBox/StephanWagner-mForm-0.2.6/mForm.Element.Select.js' ;
       //$this->HEAD['js'][]=_PATH_TO_EXT.'/mBox/StephanWagner-mForm-0.2.6/mForm.Submit.js' ;
       //$this->HEAD['css'][]='/'._CLASS_.'/assets/mForm.css' ;
       //$this->HEAD['css'][]=_PATH_TO_EXT.'/mBox/StephanWagner-mForm-0.2.6/assets/mFormElement-Select.css' ;
       //$this->HEAD['js'][]=_PATH_TO_EXT.'/mBox/modal_window.jBox.js' ;


       //$this->HEAD['css'][]=_PATH_TO_ADDONS.'/font-awesome-4.3.0/css/font-awesome.css' ;

       //$this->HEAD['js'][]=_PATH_TO_ADDONS.'/printPage/jquery.printPage.js' ;

       // стандартное подключение highslide
       //$this->HEAD['js'][]=_PATH_TO_ADDONS.'/highslide/highslide-4.1.13/highslide.js' ;
       //$this->HEAD['css'][]=_PATH_TO_ADDONS.'/highslide/highslide-4.1.13/highslide.css' ;

       $this->HEAD['css'][]='/'._CLASS_.'/style.css' ;
       //$this->HEAD['js'][]='/'._CLASS_.'/script.js' ;


     }

 function block_main()
 { global $member ;
   //$this->panel_hand_TO() ;
   //$this->panel_list_TO() ;
     ?><script type="application/javascript" src="/<?echo _CLASS_?>/ext/jquery/jquery-base64.min.js"></script>
       <link rel="stylesheet" type="text/css" href="/<?echo _CLASS_?>/ext/jquery-ui-1.9.2/jquery-ui.css">
       <script type="application/javascript" src="/<?echo _CLASS_?>/ext/jquery/jquery-ui-1.9.1.custom.min.js"></script>

       <link rel="stylesheet" type="text/css" href="/AddOns/syntaxhighligher/3.0.83/css/shCore.css">
       <link rel="stylesheet" type="text/css" href="/AddOns/syntaxhighligher/3.0.83/css/shThemeFadeToGrey.css">
       <script type="application/javascript" src="/AddOns/syntaxhighligher/3.0.83/js/shCore.js"></script>
       <script type="application/javascript" src="/AddOns/syntaxhighligher/3.0.83/js/shBrushJScript.js"></script>


       <link rel="stylesheet" type="text/css" href="/<?echo _CLASS_?>/ext/ecp/present.css">
       <script type="application/javascript" src="/<?echo _CLASS_?>/ext/ecp/present.js"></script>
     <button class="v2" cmd="ecp/get_autorize_panel">Авторизоваться по токену</button>
     <div id="container">
         <div id="content">
             <div class="test-common ui-widget-content ui-corner-all" id="ui-controls">
                 <div class="test-common">
                     <label for="device-list">Доступные устройства: </label>
                     <select id="device-list" class="item-list">
                         <option>Обновите список устройств</option>
                     </select>
                     <button id="refresh-dev" class="refresh-btn">↓↑</button>
                 </div>
                 <div class="test-common">
                     <label for="device-list">PIN-код:</label>
                     <input id="device-pin" value="12345678" size="15"/>
                     <button id="login">Войти</button>
                     <button id="logout">Выйти</button>
                     <button id="save-pin">Сохранить PIN</button>
                     <button id="remove-pin">Удалить PIN</button>
                 </div>
                 <div class="test-common">
                     <label for="key-list">Ключи на устройстве: </label>
                     <select id="key-list" class="item-list">
                         <option>Выполните вход на устройство</option>
                     </select>
                     <button id="refresh-keys" class="refresh-btn">↓↑</button>
                </div>
                 <div class="test-common">
                     <label for="cert-list">Сертификаты на уcтройстве: </label>
                     <select id="cert-list" class="item-list">
                         <option>Обновите список сертификатов</option>
                     </select>
                     <button id="refresh-certs" class="refresh-btn">↓↑</button>
                </div>
             </div>
             <div id="section">
                 <ul>
                     <li><a href="#section-1">Работа с устройством и ключевыми парами</a></li>
                     <li><a href="#section-2">Формирование запроса на сертификат</a></li>
                     <li><a href="#section-3">Работа с сертификатами</a></li>
                     <li><a href="#section-4">ЭЦП и аутентификация</a></li>
                     <li><a href="#section-5">Шифрование</a></li>
                 </ul>
                 <div id="section-1">
                     <div id="DevInfo" class="test ui-widget-content ui-corner-all">
                         <label for="device-info-radio" class="temp">Тип информации:
                             <div class="test-element" id="device-info-radio">
                                 <div class="radio-button"><label><input class="radio-input" type="radio" name="device-info" value="model" checked="checked"/>Модель</label></div>
                                 <div class="radio-button"><label><input class="radio-input" type="radio" name="device-info" value="label"/>Метка</label></div>
                                 <div class="radio-button"><label><input class="radio-input" type="radio" name="device-info" value="type"/>Тип устройства</label></div>
                                 <div class="radio-button"><label><input class="radio-input" type="radio" name="device-info" value="serial"/>Серийный номер</label></div>
                                 <div class="radio-button"><label><input class="radio-input" type="radio" name="device-info" value="pin cache"/>PIN закэширован</label></div>
                                 <div class="radio-button"><label><input class="radio-input" type="radio" name="device-info" value="logged"/>Авторизация</label></div>
                             </div>
                         </label>
                     </div>
                     <div id="ChangePin" class="test ui-widget-content ui-corner-all">
                         <label for=old-pin" class="temp">Старый ПИН:
                         <input id="old-pin" value="12345678" class="test-element input"/>
                         <label for=new-pin" class="temp">Новый ПИН:
                         <input id="new-pin" value="123456" class="test-element input"/>
                     </div>
                     <div id="GenerateKeyPair" class="test ui-widget-content ui-corner-all">
                         <div class="test-element">
                             <input type="checkbox" name="need-pin" class="input" />
                             <label for="need-pin">Требовать ввод PIN на каждую операцию с ключевой парой в PINPad 2</label><br>
                             <input type="checkbox" name="need-confirm" class="input" />
                             <label for="need-confirm">Требовать подтверждения на каждую операцию с ключевой парой в PINPad 2</label>
                         </div>
                         <input id="generate-key-marker" value="BusinessTests" class="test-element input"/>
                     </div>
                     <div id="EnumerateKeys" class="test ui-widget-content ui-corner-all">
                         <input id="enumerate-key-marker" value="BusinessTests" class="test-element input"/>
                     </div>
                     <div id="SetKeyLabel" class="test ui-widget-content ui-corner-all">
                         <input id="key-label" value="test key" class="test-element input"/>
                     </div>
                     <div id="GetKeyLabel" class="test ui-widget-content ui-corner-all"></div>
                     <div id="DeleteKeyPair" class="test ui-widget-content ui-corner-all"></div>
                 </div>
                 <div id="section-2">
                     <div id="CreatePkcs10" class="test ui-widget-content ui-corner-all">
                         <div class="smaller-font">
                             <div class="test-element" id="cert-subject">
                                 <table>
                                     <tr>
                                         <td><div class="input-cell"><label>countryName<input id="countryName" value="RU" class="subject-input"/></label></div></td>
                                         <td><div class="input-cell"><label>stateOrProvinceName<input id="stateOrProvinceName" value="Moscow" class="subject-input"/></label></div></td>
                                     </tr>
                                     <tr>
                                         <td><div class="input-cell"><label>localityName<input id="localityName" value="msk" class="subject-input"/></label></div></td>
                                         <td><div class="input-cell"><label>streetAddress<input id="streetAddress" value="street" class="subject-input"/></label></div></td>
                                     </tr>
                                     <tr>
                                         <td><div class="input-cell"><label>organizationName<input id="organizationName" value="Aktiv" class="subject-input"/></label></div></td>
                                         <td><div class="input-cell"><label>organizationalUnitName<input id="organizationalUnitName" value="IT" class="subject-input"/></label></div></td>
                                     </tr>
                                     <tr>
                                         <td><div class="input-cell"><label>postalAddress<input id="postalAddress" value="postal address" class="subject-input"/></label></div></td>
                                         <td><div class="input-cell"><label>title<input id="title" value="должность" class="subject-input"/></label></div></td>
                                     </tr>
                                     <tr>
                                         <td><div class="input-cell"><label>INN<input id="INN" value="12345678987" class="subject-input"/></label></div></td>
                                         <td><div class="input-cell"><label>SNILS<input id="SNILS" value="12345678987" class="subject-input"/></label></div></td>
                                     </tr>
                                     <tr>
                                         <td><div class="input-cell"><label>OGRN<input id="OGRN" value="12345678987" class="subject-input"/></label></div></td>
                                         <td><div class="input-cell"><label>OGRNIP<input id="OGRNIP" value="12345678987" class="subject-input"/></label></div></td>
                                     </tr>
                                     <tr>
                                         <td><div class="input-cell"><label>commonName<input id="commonName" value="Фамилия Имя Очество" class="subject-input"/></label></div></td>
                                         <td><div class="input-cell"><label>pseudonym<input id="pseudonym" value="pseudonym" class="subject-input"/></label></div></td>
                                     </tr>
                                     <tr>
                                         <td><div class="input-cell"><label>surname<input id="surname" value="surname" class="subject-input"/></label></div></td>
                                         <td><div class="input-cell"><label>givenName<input id="givenName" value="given name" class="subject-input"/></label></div></td>
                                     </tr>
                                     <tr>
                                         <td><div class="input-cell"><label>emailAddress<input id="emailAddress" value="example@example.com" class="subject-input"/></label></div></td>
                                     </tr>
                                 </table>
                             </div>

                             <div class="test-element" id="cert-extensions">
                                 <table>
                                     <tr><th>Key Usage</th><th>Ext Key Usage</th><th>Policies</th></tr>
                                     <tr>
                                         <td><label><input class="checkbox-input" type="checkbox" name="cert-exts-keyusage" value="digitalSignature" checked="checked"/>digitalSignature</label></td>
                                         <td><label><input class="checkbox-input" type="checkbox" name="cert-exts-extkeyusage" value="emailProtection" checked="checked"/>emailProtection</label></td>
                                         <td><label><input class="checkbox-input" type="checkbox" name="cert-exts-policies" value="1.2.643.100.113.1" checked="checked"/>КС1</label></td>
                                     </tr>
                                     <tr>
                                         <td><label><input class="checkbox-input" type="checkbox" name="cert-exts-keyusage" value="nonRepudiation" checked="checked"/>nonRepudiation</label></td>
                                         <td><label><input class="checkbox-input" type="checkbox" name="cert-exts-extkeyusage" value="clientAuth" /></label>clientAuth</td>
                                         <td><label><input class="checkbox-input" type="checkbox" name="cert-exts-policies" value="1.2.643.100.113.2"/>КС2</label></td>
                                     </tr>
                                     <tr>
                                         <td><label><input class="checkbox-input" type="checkbox" name="cert-exts-keyusage" value="keyEncipherment"/>keyEncipherment</label></td>
                                         <td><label><input class="checkbox-input" type="checkbox" name="cert-exts-extkeyusage" value="serverAuth" /></label>serverAuth</td>
                                         <td><label><input class="checkbox-input" type="checkbox" name="cert-exts-policies" value="1.2.643.100.113.3"/>КС3</label></td>
                                     </tr>
                                     <tr>
                                         <td><label><input class="checkbox-input" type="checkbox" name="cert-exts-keyusage" value="dataEncipherment" />dataEncipherment</label></td>
                                         <td><label><input class="checkbox-input" type="checkbox" name="cert-exts-extkeyusage" value="codeSigning" /></label>codeSigning</td>
                                         <td><label><input class="checkbox-input" type="checkbox" name="cert-exts-policies" value="1.2.643.100.113.4"/>КВ1</label></td>
                                     </tr>
                                     <tr>
                                         <td><label><input class="checkbox-input" type="checkbox" name="cert-exts-keyusage" value="keyAgreement"/>keyAgreement</label></td>
                                         <td><label><input class="checkbox-input" type="checkbox" name="cert-exts-extkeyusage" value="timeStamping" /></label>timeStamping</td>
                                         <td><label><input class="checkbox-input" type="checkbox" name="cert-exts-policies" value="1.2.643.100.113.5"/>КВ2</label></td>
                                     </tr>
                                     <tr>
                                         <td><label><input class="checkbox-input" type="checkbox" name="cert-exts-keyusage" value="keyCertSign"/>keyCertSign</label></td>
                                         <td><label><input class="checkbox-input" type="checkbox" name="cert-exts-extkeyusage" value="msCodeInd" /></label>msCodeInd</td>
                                         <td><label><input class="checkbox-input" type="checkbox" name="cert-exts-policies" value="1.2.643.100.113.6"/>КА1</label></td>
                                     </tr>
                                     <tr>
                                         <td><label><input class="checkbox-input" type="checkbox" name="cert-exts-keyusage" value="cRLSign"/>cRLSign</label></td>
                                         <td><label><input class="checkbox-input" type="checkbox" name="cert-exts-extkeyusage" value="msCodeCom" /></label>msCodeCom</td>
                                     </tr>
                                     <tr>
                                         <td><label><input class="checkbox-input" type="checkbox" name="cert-exts-keyusage" value="encipherOnly"/>encipherOnly</label></td>
                                         <td><label><input class="checkbox-input" type="checkbox" name="cert-exts-extkeyusage" value="msCTLSign" /></label>msCTLSign</td>
                                     </tr>
                                     <tr>
                                         <td><label><input class="checkbox-input" type="checkbox" name="cert-exts-keyusage" value="decipherOnly"/>decipherOnly</label></td>
                                         <td><label><input class="checkbox-input" type="checkbox" name="cert-exts-extkeyusage" value="1.3.6.1.5.5.7.3.9" /></label>OCSP</td>
                                     </tr>
                                     <tr>
                                         <td></td>
                                         <td><label><input class="checkbox-input" type="checkbox" name="cert-exts-extkeyusage" value="1.2.643.2.2.34.6" /></label>CryptoPro RA user</td>
                                     </tr>
                                 </table>
                             </div>
                         </div>
                         <textarea id="create-pkcs10-result" class="text-output test-element"></textarea>
                     </div>
                 </div>
                 <div id="section-3">
                     <div id="ImportCertificate" class="test ui-widget-content ui-corner-all">
                         <label for="import-cert-body" class="test-element">Cертификат в формате PEM</label>
                         <textarea id="import-cert-body" class="text-input test-element">-----BEGIN CERTIFICATE-----&#10;MIIE9DCCBKOgAwIBAgIKGeFWzQACAAJYxzAIBgYqhQMCAgMwZTEgMB4GCSqGSIb3&#10;DQEJARYRaW5mb0BjcnlwdG9wcm8ucnUxCzAJBgNVBAYTAlJVMRMwEQYDVQQKEwpD&#10;UllQVE8tUFJPMR8wHQYDVQQDExZUZXN0IENlbnRlciBDUllQVE8tUFJPMB4XDTEy&#10;MDgxNjEyNDgxMFoXDTE0MTAwNDA3MDk0MVowggGKMQswCQYDVQQGEwJSVTEPMA0G&#10;A1UECBMGbW9zY293MREwDwYDVQQHEwhsb2NhbGl0eTEPMA0GA1UECRMGc3RyZWV0&#10;MQ4wDAYDVQQKEwVBa3RpdjELMAkGA1UECxMCSVQxGzAZBgNVBAweEgQ0BD4EOwQ2&#10;BD0EPgRBBEIETDEZMBcGCCqFAwOBAwEBEgsxMjM0MjM1MzQ1MjEWMBQGBSqFA2QD&#10;EgsxMjM0MjM1MzQ1MjEWMBQGBSqFA2QBEgsxMjM0MjM1MzQ1MjEWMBQGBSqFA2QF&#10;EgsxMjM0MjM1MzQ1MjEvMC0GA1UEAx4mBCQEMAQ8BDgEOwQ4BE8AIAQYBDwETwAg&#10;BB4ERwQ1BEEEQgQyBD4xFzAVBgNVBBATDnBvc3RhbCBhZGRyZXNzMRQwEgYDVQRB&#10;Ewtwc2V1ZG9ueW11czEQMA4GA1UEBBMHc3VybmFtZTETMBEGA1UEKhMKZ2l2ZW4g&#10;bmFtZTEiMCAGCSqGSIb3DQEJARYTZXhhbXBsZUBleGFtcGxlLmNvbTBjMBwGBiqF&#10;AwICEzASBgcqhQMCAiMBBgcqhQMCAh4BA0MABEC8dyMzXuEbYHV9NwCWZQmoWmon&#10;xJAfFTMtoKfuWWwhNg3ZuL36UP/zjTd33HolBNkRIC0FgtxpfQ+ES3l3J7PVo4IC&#10;CjCCAgYwDAYDVR0PBAUDAwf3gDB1BgNVHSUBAf8EazBpBggrBgEFBQcDBAYIKwYB&#10;BQUHAwIGCCsGAQUFBwMBBggrBgEFBQcDAwYIKwYBBQUHAwgGCisGAQQBgjcCARUG&#10;CisGAQQBgjcCARYGCisGAQQBgjcKAwEGCCsGAQUFBwMJBgcqhQMCAiIGMEUGA1Ud&#10;IAQ+MDwwCAYGKoUDZHEBMAgGBiqFA2RxAjAIBgYqhQNkcQMwCAYGKoUDZHEEMAgG&#10;BiqFA2RxBTAIBgYqhQNkcQYwHQYDVR0OBBYEFB1Iacp18c1KccWyrbNYHhmTc0Ly&#10;MB8GA1UdIwQYMBaAFG2PXgXZX6yRF5QelZoFMDg3ehAqMFUGA1UdHwROMEwwSqBI&#10;oEaGRGh0dHA6Ly93d3cuY3J5cHRvcHJvLnJ1L0NlcnRFbnJvbGwvVGVzdCUyMENl&#10;bnRlciUyMENSWVBUTy1QUk8oMikuY3JsMIGgBggrBgEFBQcBAQSBkzCBkDAzBggr&#10;BgEFBQcwAYYnaHR0cDovL3d3dy5jcnlwdG9wcm8ucnUvb2NzcG5jL29jc3Auc3Jm&#10;MFkGCCsGAQUFBzAChk1odHRwOi8vd3d3LmNyeXB0b3Byby5ydS9DZXJ0RW5yb2xs&#10;L3BraS1zaXRlX1Rlc3QlMjBDZW50ZXIlMjBDUllQVE8tUFJPKDIpLmNydDAIBgYq&#10;hQMCAgMDQQAN/R7GwKALwybssbX1FnfHqZxvIxh13Pu2+iLgK/cRGQ9Ga9i8d6+0&#10;E4wUHty12HGB0ABPrQ43QmTGeNocDz/h&#10;-----END CERTIFICATE-----</textarea>
                         <label class="test-element">Категория сертификата:
                             <div class="test-element">
                                 <p><label><input class="radio-input" type="radio" name="certificate-category" value="user" checked="checked"/>Пользовательский</label></p>
                                 <p><label><input class="radio-input" type="radio" name="certificate-category" value="ca"/>Корневой</label></p>
                                 <p><label><input class="radio-input" type="radio" name="certificate-category" value="other"/>Другой</label></p>
                             </div>
                         </label>
                     </div>
                     <div id="DeleteCertificate" class="test ui-widget-content ui-corner-all"></div>
                     <div id="GetCertificate" class="test ui-widget-content ui-corner-all">
                         <textarea id="get-cert-result" class="text-output test-element"></textarea>
                     </div>
                     <div id="ParseCertificate" class="test ui-widget-content ui-corner-all">
                         <textarea id="parse-cert-result" class="text-output test-element"></textarea>
                     </div>
                     <div id="ParseCertificateFromString" class="test ui-widget-content ui-corner-all">
                     	<label for="parse-cert-body" class="test-element">Cертификат в формате PEM</label>
                         <textarea id="parse-cert-string-body" class="text-input test-element">-----BEGIN CERTIFICATE-----&#10;MIIE9DCCBKOgAwIBAgIKGeFWzQACAAJYxzAIBgYqhQMCAgMwZTEgMB4GCSqGSIb3&#10;DQEJARYRaW5mb0BjcnlwdG9wcm8ucnUxCzAJBgNVBAYTAlJVMRMwEQYDVQQKEwpD&#10;UllQVE8tUFJPMR8wHQYDVQQDExZUZXN0IENlbnRlciBDUllQVE8tUFJPMB4XDTEy&#10;MDgxNjEyNDgxMFoXDTE0MTAwNDA3MDk0MVowggGKMQswCQYDVQQGEwJSVTEPMA0G&#10;A1UECBMGbW9zY293MREwDwYDVQQHEwhsb2NhbGl0eTEPMA0GA1UECRMGc3RyZWV0&#10;MQ4wDAYDVQQKEwVBa3RpdjELMAkGA1UECxMCSVQxGzAZBgNVBAweEgQ0BD4EOwQ2&#10;BD0EPgRBBEIETDEZMBcGCCqFAwOBAwEBEgsxMjM0MjM1MzQ1MjEWMBQGBSqFA2QD&#10;EgsxMjM0MjM1MzQ1MjEWMBQGBSqFA2QBEgsxMjM0MjM1MzQ1MjEWMBQGBSqFA2QF&#10;EgsxMjM0MjM1MzQ1MjEvMC0GA1UEAx4mBCQEMAQ8BDgEOwQ4BE8AIAQYBDwETwAg&#10;BB4ERwQ1BEEEQgQyBD4xFzAVBgNVBBATDnBvc3RhbCBhZGRyZXNzMRQwEgYDVQRB&#10;Ewtwc2V1ZG9ueW11czEQMA4GA1UEBBMHc3VybmFtZTETMBEGA1UEKhMKZ2l2ZW4g&#10;bmFtZTEiMCAGCSqGSIb3DQEJARYTZXhhbXBsZUBleGFtcGxlLmNvbTBjMBwGBiqF&#10;AwICEzASBgcqhQMCAiMBBgcqhQMCAh4BA0MABEC8dyMzXuEbYHV9NwCWZQmoWmon&#10;xJAfFTMtoKfuWWwhNg3ZuL36UP/zjTd33HolBNkRIC0FgtxpfQ+ES3l3J7PVo4IC&#10;CjCCAgYwDAYDVR0PBAUDAwf3gDB1BgNVHSUBAf8EazBpBggrBgEFBQcDBAYIKwYB&#10;BQUHAwIGCCsGAQUFBwMBBggrBgEFBQcDAwYIKwYBBQUHAwgGCisGAQQBgjcCARUG&#10;CisGAQQBgjcCARYGCisGAQQBgjcKAwEGCCsGAQUFBwMJBgcqhQMCAiIGMEUGA1Ud&#10;IAQ+MDwwCAYGKoUDZHEBMAgGBiqFA2RxAjAIBgYqhQNkcQMwCAYGKoUDZHEEMAgG&#10;BiqFA2RxBTAIBgYqhQNkcQYwHQYDVR0OBBYEFB1Iacp18c1KccWyrbNYHhmTc0Ly&#10;MB8GA1UdIwQYMBaAFG2PXgXZX6yRF5QelZoFMDg3ehAqMFUGA1UdHwROMEwwSqBI&#10;oEaGRGh0dHA6Ly93d3cuY3J5cHRvcHJvLnJ1L0NlcnRFbnJvbGwvVGVzdCUyMENl&#10;bnRlciUyMENSWVBUTy1QUk8oMikuY3JsMIGgBggrBgEFBQcBAQSBkzCBkDAzBggr&#10;BgEFBQcwAYYnaHR0cDovL3d3dy5jcnlwdG9wcm8ucnUvb2NzcG5jL29jc3Auc3Jm&#10;MFkGCCsGAQUFBzAChk1odHRwOi8vd3d3LmNyeXB0b3Byby5ydS9DZXJ0RW5yb2xs&#10;L3BraS1zaXRlX1Rlc3QlMjBDZW50ZXIlMjBDUllQVE8tUFJPKDIpLmNydDAIBgYq&#10;hQMCAgMDQQAN/R7GwKALwybssbX1FnfHqZxvIxh13Pu2+iLgK/cRGQ9Ga9i8d6+0&#10;E4wUHty12HGB0ABPrQ43QmTGeNocDz/h&#10;-----END CERTIFICATE-----</textarea>
                         <textarea id="parse-cert-string-result" class="text-output test-element"></textarea>
                     </div>
                     <div id="GetKeyByCertificate" class="test ui-widget-content ui-corner-all"></div>
                 </div>
                 <div id="section-4">
                     <div id="SignMessage" class="test ui-widget-content ui-corner-all">
                         <label for="sign-message" class="test-element">Сообщение</label>
                         <textarea id="sign-message" class="text-input test-element">Текст очень важного платежного документа</textarea>
                          <p>
                             <label for="use-hw-hash">Вычислять хеш на устройстве</label>
                             <input type="checkbox" name="use-hw-hash" /><br>
                             <label for="detached-sign">Создавать отсоединенную подпись</label>
                             <input type="checkbox" name="detached-sign" /><br>
                             <label for="add-user-cert">Включать сертификат пользователя</label>
                             <input type="checkbox" name="add-user-cert" checked="checked"/>
                         </p>
                         <textarea id="sign-message-result" class="text-output test-element"></textarea>
                     </div>
                     <div id="SignHash" class="test ui-widget-content ui-corner-all">
                         <label for="sign-hash" class="test-element">Хеш</label>
                         <textarea id="sign-hash" class="text-input test-element">01:23:45:67:89:ab:cd:ef:01:23:45:67:89:ab:cd:ef:01:23:45:67:89:ab:cd:ef:01:23:45:67:89:ab:cd:ef</textarea>
                         <textarea id="sign-message-result" class="text-output test-element"></textarea>
                     </div>
                     <div id="SignMessagePinPad" class="test ui-widget-content ui-corner-all">
                         <label for="sign-message" class="test-element">Сообщение</label>
                         <textarea id="sign-message" class="text-input test-element"><!PINPADFILE UTF8><!>невидимый текст<N>ФИО:<V>Петров Петр Петрович Москва, Пионерская ул, д. 3, кв. 72<N>Перевод со счета:<V>42301810001000075212<N>Сумма:<V>150000<N>Валюта:<V>RUR<N>Наименование получателя:<V>Иванова Елена Ивановна<N>Номер счета получателя:<V>40817810338295201618<N>БИК банка получателя:<V>044525225<N>Наименование банка получателя:<V>ОАО 'СБЕРБАНК РОССИИ' Г. МОСКВА<N>Номер счета банка получателя:<V>30101810400000000225<N>Назначение платежа:<V>перевод личных средств</textarea>
                         <p>
                             <label for="use-hw-hash">Вычислять хеш на устройстве</label>
                             <input type="checkbox" name="use-hw-hash" /><br>
                             <label for="detached-sign">Создавать отсоединенную подпись</label>
                             <input type="checkbox" name="detached-sign" />
                         </p>
                         <textarea id="sign-message-result" class="text-output test-element"></textarea>
                     </div>
                     <div id="Authenticate" class="test ui-widget-content ui-corner-all">
                         <label for="authenticate" class="test-element">Собщение от сервера</label>
                         <textarea id="authenticate" class="text-input test-element"><!PINPADFILE UTF8><V>Выполнить аутентификацию?<!>server-random-data</textarea>
                         <textarea id="authenticate-result" class="text-output test-element"></textarea>
                     </div>
                     <div id="Verify" class="test ui-widget-content ui-corner-all">
                         <label for="sign" class="test-element">Подпись в формате CMS</label>
                         <textarea placeholder="Введите CMS" id="sign" class="text-input test-element"></textarea>
                         <p>
                             <label for="use-hw-hash">Вычислять хеш на устройстве</label>
                             <input type="checkbox" name="use-hw-hash" /><br>
                             <label for="verify-signer-cert">Проверять сертификат</label>
                             <input type="checkbox" checked="checked" name="verify-signer-cert" />
                         </p>
                         <label for="data" class="test-element">Подписанные данные</label>
                         <textarea placeholder="Опционально - в случае если проверяется detached подпись" id="data" class="text-input test-element"></textarea>
                         <label for="cert" class="test-element">Сертификат</label>
                         <textarea placeholder="Опционально - в случае если он не включен в cms" id="cert" class="text-input test-element"></textarea>
                         <label for="ca-cert" class="test-element">СА</label>
                         <textarea placeholder="СА" id="ca-cert" class="text-input test-element"></textarea>
                     </div>
     <!-- file reading is not supported in IE yet, so disable it
                     <div id="SignFile" class="test ui-widget-content ui-corner-all">
                         <label for="sign-file" class="test-element">Выберите файл</label>
                         <input id="sign-file" type="file" class="test-element" />
                         <textarea id="sign-file-result" class="text-output test-element"></textarea>
                     </div>
      -->
                 </div>
                 <div id="section-5">
                     <div id="EncryptMessage" class="test ui-widget-content ui-corner-all">
                         <label for="encrypt-certificate" class="test-element">Тело сертификата</label>
                         <textarea id="encrypt-certificate" class="text-input test-element">-----BEGIN CERTIFICATE-----
     MIIEWzCCBAqgAwIBAgIKfFP2xQACAALbSzAIBgYqhQMCAgMwZTEgMB4GCSqGSIb3
     DQEJARYRaW5mb0BjcnlwdG9wcm8ucnUxCzAJBgNVBAYTAlJVMRMwEQYDVQQKEwpD
     UllQVE8tUFJPMR8wHQYDVQQDExZUZXN0IENlbnRlciBDUllQVE8tUFJPMB4XDTEy
     MTAzMTEyMjIyNloXDTE0MTAwNDA3MDk0MVowggGDMQswCQYDVQQGEwJSVTEPMA0G
     A1UECBMGTW9zY293MQwwCgYDVQQHEwNtc2sxDzANBgNVBAkTBnN0cmVldDEOMAwG
     A1UEChMFQWt0aXYxCzAJBgNVBAsTAklUMRcwFQYDVQQQEw5wb3N0YWwgYWRkcmVz
     czEbMBkGA1UEDB4SBDQEPgQ7BDYEPQQ+BEEEQgRMMRkwFwYIKoUDA4EDAQESCzEy
     MzQ1Njc4OTg3MRYwFAYFKoUDZAMSCzEyMzQ1Njc4OTg3MRYwFAYFKoUDZAESCzEy
     MzQ1Njc4OTg3MRYwFAYFKoUDZAUSCzEyMzQ1Njc4OTg3MS8wLQYDVQQDHiYEJAQw
     BDwEOAQ7BDgETwAgBBgEPARPACAEHgRHBDUEQQRCBDIEPjESMBAGA1UEQRMJcHNl
     dWRvbnltMRAwDgYDVQQEEwdzdXJuYW1lMRMwEQYDVQQqEwpnaXZlbiBuYW1lMSIw
     IAYJKoZIhvcNAQkBFhNleGFtcGxlQGV4YW1wbGUuY29tMGMwHAYGKoUDAgITMBIG
     ByqFAwICIwEGByqFAwICHgEDQwAEQAhY4OcPvPJanf/8RhuIh3jt3MAOgtHwJSsp
     vQwPi0SUNU4wILT1M9//WlAAz4hEXqx+9pGfyUiYsg/bgKwLsmWjggF4MIIBdDAL
     BgNVHQ8EBAMCBsAwFgYDVR0lAQH/BAwwCgYIKwYBBQUHAwQwEwYDVR0gBAwwCjAI
     BgYqhQNkcQEwHQYDVR0OBBYEFOxa5YWf5fGZiFQew1lp0FyhfrogMB8GA1UdIwQY
     MBaAFG2PXgXZX6yRF5QelZoFMDg3ehAqMFUGA1UdHwROMEwwSqBIoEaGRGh0dHA6
     Ly93d3cuY3J5cHRvcHJvLnJ1L0NlcnRFbnJvbGwvVGVzdCUyMENlbnRlciUyMENS
     WVBUTy1QUk8oMikuY3JsMIGgBggrBgEFBQcBAQSBkzCBkDAzBggrBgEFBQcwAYYn
     aHR0cDovL3d3dy5jcnlwdG9wcm8ucnUvb2NzcG5jL29jc3Auc3JmMFkGCCsGAQUF
     BzAChk1odHRwOi8vd3d3LmNyeXB0b3Byby5ydS9DZXJ0RW5yb2xsL3BraS1zaXRl
     X1Rlc3QlMjBDZW50ZXIlMjBDUllQVE8tUFJPKDIpLmNydDAIBgYqhQMCAgMDQQAu
     WkeHdQran99h1XeLuepMQA9FIf4PwMzgwCi3bs4ThFQMvh5ZtSXY6aEJ1ohNxzvE
     AWN8elkoasoZJhWjj9zF
     -----END CERTIFICATE-----
                         </textarea>
                         <label for="encrypt-message" class="test-element">Сообщение</label>
                         <textarea id="encrypt-message" class="text-input test-element">Зашифруй меня полностью</textarea>
                         <textarea id="encrypt-message-result" class="text-output test-element"></textarea>
                         <p>
                             <label for="use-hw-encryption">Шифровать на устройстве</label>
                             <input type="checkbox" name="use-hw-encryption" />
                         </p>
                     </div>
                     <div id="DecryptMessage" class="test ui-widget-content ui-corner-all">
                         <label for="decrypt-message" class="test-element">Сообщение</label>
                         <textarea placeholder="Расшифруй меня полностью" id="decrypt-message" class="text-input test-element"></textarea>
                         <textarea id="decrypt-message-result" class="text-output test-element"></textarea>
                     </div>
                     <div id="Encrypt" class="test ui-widget-content ui-corner-all">
                         <label for="encrypt-key" class="test-element">Метка ключа шифрования</label>
                         <input id="encrypt-key" value="secretKey" class="test-element input">
                         <label for="encrypt-message" class="test-element">Сообщение</label>
                         <textarea id="encrypt-message" class="text-input test-element">00:11:22:33:44:55:66:77:88:99:aa:bb:cc:dd:ee:ff</textarea>
                         <textarea id="encrypt-message-result" class="text-output test-element"></textarea>
                     </div>
                     <div id="Decrypt" class="test ui-widget-content ui-corner-all">
                         <label for="encrypt-key" class="test-element">Метка ключа расшифрования</label>
                         <input id="encrypt-key" value="secretKey" class="test-element input">
                         <label for="encrypt-message" class="test-element">Сообщение</label>
                         <textarea id="encrypt-message" class="text-input test-element"></textarea>
                         <textarea id="encrypt-message-result" class="text-output test-element"></textarea>
                     </div>
                 </div>
             </div>
         </div>
         <div id="console"></div>
         <object type="application/x-rutoken-pki" id="plugin-object" width="0" height="0">
             <param name="onload" value="onPluginLoaded"/>
         </object>
     </div>

       <script type="text/javascript">

         function autorize_by_token()
         {   alert('autorize_by_token') ;
             plugin.pluginObject.authenticate(0, '2d:5b:34:23:32:54:e5:ab:ad:4a:58:32:5f:bf:40:bc:d1:24:ac:2c', '<!PINPADFILE UTF8><V>Выполнить аутентификацию?<!>server-random-data',
                                              //$.proxy(function (res)  { alert(res);}, this),
                                              //$.proxy(ui.printError, ui)
                                              function(res){alert(res)},
                                              function(res){alert(res)}
                                              ) ;
             //alert(res) ;

         }
       </script>
   <?

 }


}

?>