var plugin ;
function cryptoPlugin(pluginObject)
{   this.pluginObject = pluginObject;
   if (!this.pluginObject.valid) console.log("Error: couldn't get CryptopluginObject");
   else                          console.log('cryptoPlugin loaded') ;


    this.FillCertificateToSelect = function (element,device,category) // id_select, token id, тип сертификата
    {
        this.pluginObject.enumerateCertificates(device,category,
         function(certificates)
         { console.log('token hat '+certificates.length+' sert:'+certificates[0]);
           $j(element).empty() ;
           for (var c in certificates)
           {  plugin.pluginObject.parseCertificate(0, certificates[c],
                function (handle)
                {
                   return function (cert)
                   {
                       plugin.addCertificateToSelect(element,handle, cert, category);
                   }
                }(certificates[c]),          // 1 -  пользовательские сертификаты
                function(){$j('div#token_result').html('<div class=alert>не удалось прочитать список сертификатов токена</div>');}
               );
           }
         },
         function(){$j('div#token_result').html('<div class=alert>не удалось получить список сертификатов токена</div>');}
        ) ;
    }  ;


    this.addCertificateToSelect = function (element,handle, certificate, category)
    {
        var description = "";
        switch (category)
        {   case 1: description = "Пользовательский| "; break;
            case 2: description = "Корневой| "; break;
            case 3: description = "Другой| "; break;
            case 4: description = "Не задана| "; break;
        }

        var subjectDNs = certificate.subject;
        var noSubject = true;
        for (c in subjectDNs) {
            if (subjectDNs[c]["rdn"] == "commonName" || subjectDNs[c]["rdn"] == "_emailAddress") {
                noSubject = false;
                //description += subjectDNs[c]["rdn"] + "=" + subjectDNs[c]["value"] + "|";
                description += subjectDNs[c]["value"] + "|";
            }
        }
        if (noSubject) description += certificate.serialNumber;

        var title = "Serial number: " + certificate.serialNumber + "\n\nIssuer:\n\t";
        var issuerDNs = certificate.issuer;
        for (c in issuerDNs) {
            title += issuerDNs[c]["rdn"] + "=" + issuerDNs[c]["value"] + "\n\t";
        }

        $j(element).append($j("<option>", {
            'value': handle,
            'title': $j.trim(title).replace(/&/g, "&amp;").replace(/>/g, "&gt;").replace(/</g, "&lt;").replace(/"/g, "&quot;")
        }).text(noSubject ? certificate.serialNumber : description));
    }

}

function onPluginLoaded(pluginObject)
{ try  { plugin = new cryptoPlugin(pluginObject);

        // получаем список сертификатов и используем первый сертификат
        //plugin.pluginObject.enumerateCertificates(0,1,
        // function(certificates) { if (certificates.length) $j('button#start_autorize').attr('sert_id',certificates[0]) ; console.log('token hat '+certificates.length+' sert:'+certificates[0]);},
        // function(){$j('div#token_result').html('<div class=alert>не удалось получить список сертификатов токена</div>');}
        //) ;

     }
 catch (error) { console.log(error) ;}
}