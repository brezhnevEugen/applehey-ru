<?php
include_once(_DIR_TO_MODULES.'/reports/i_report.php') ;
class report_log_pages extends c_report
{
 var $title='Журнал страниц' ;
 var $allow_to_rol=array() ;
 var $filter=array() ;
 var $view_by_space_filter=1 ;
 var $use_jwplayer=1 ;
 var $cur_sort_type=2 ;
 var $table_name='log_site_pages' ;

 function __construct()
 {   parent::__construct() ;
     $this->pages_info['sort_type']=array() ;
     $this->pages_info['sort_type'][1]=array('name'=>'Обратная',		'order_by'=>'pkey desc'    ) ;
     $this->pages_info['sort_type'][2]=array('name'=>'Прямая',      	'order_by'=>'pkey') ;
 }

 function get_time_range_by_filter_to_cur_day()
 { // если дата задана через фильтр, используем её. Иначе используем текущую дату время. Используем getdate для разненесения значений в массив
   $data_from=($this->filter['data_from'])? getdate(strtotime($this->filter['data_from'])):getdate() ;
   $data_to=($this->filter['data_to'])? getdate(strtotime($this->filter['data_to'])):getdate() ;
   // если дата задана через фильтр, преобразовыаем в текст без коррекции. Если была использована текущая дата - добавляем границы текущего дня. Испольщуем mktime
   $fime_from2=($this->filter['data_from'])? mktime($data_from['hours'],$data_from['minutes'],0,$data_from['mon'],$data_from['mday'],$data_from['year']):mktime(0,0,0,$data_from['mon'],$data_from['mday'],$data_from['year']);
   $fime_to2=($this->filter['data_to'])? mktime($data_to['hours'],$data_to['minutes'],0,$data_to['mon'],$data_to['mday'],$data_to['year']):mktime(23,59,59,$data_to['mon'],$data_to['mday'],$data_to['year']);
   return(array($fime_from2,$fime_to2)) ;
 }

 function setting($options=array())
 {   ?><div id="panel_filter" class="one_row">
             <form id="sky-form" class="sky-form"><input type="hidden" name="filter_dir" value="<?echo ($options['cur_report_dir'])? $options['cur_report_dir']:_CUR_REPORT_DIR?>">
                  <fieldset>
                      <div class="row" id="person_info">
                          <section class="col col-2">
                            <label class="label">IP</label>
                            <label class="input">
                                <input type="text" name="filter[ip]" value="<?echo $this->filter['ip']?>">
                            </label>
                        </section>
                          <section class="col col-2">
                              <label class="label">Дата от</label>
                              <label class="input">
                                  <i class="icon-append fa fa-calendar"></i>
                                  <input type="text" name="filter[data_from]" class="datetimepicker" value="<?echo $this->filter['data_from']?>">
                              </label>
                            </section>
                            <section class="col col-2">
                              <label class="label">Дата до</label>
                              <label class="input">
                                  <i class="icon-append fa fa-calendar"></i>
                                  <input type="text" name="filter[data_to]" class="datetimepicker" value="<?echo $this->filter['data_to']?>">
                              </label>
                          </section>
                          <section class="col col-2">
                                 <label class="label">URL</label>
                                 <label class="input">
                                      <input type="text" name="filter[url]" placeholder="" value="<?echo htmlspecialchars($this->filter['url'])?>">
                                 </label>
                           </section>
                          <section class="col col-2">
                                 <label class="label">URL from</label>
                                 <label class="input">
                                      <input type="text" name="filter[url_from]" placeholder="" value="<?echo htmlspecialchars($this->filter['url_from'])?>">
                                 </label>
                           </section>
                          <section class="col col-2">
                               <label class="label">Сортировка:</label>
                               <label class="select">
                                   <select name="filter[order]" id="order">
                                       <option value="" <?if (!$this->filter['order']) echo 'selected'?>>Обратная</option>
                                       <option value="pkey" <?if ($this->filter['order']=='pkey') echo 'selected'?>>Прямая</option>
                                  </select>
                               </label>
                            </section>

                        </div>
                  </fieldset>
                 <div id="panel_buttons"><input type="submit" class="button v2" cmd="set_filter" value="OK"></div>
              </form>
       </div>
     <?
  }


 function _setting_details($options=array())
 {   list($time_from,$time_to)=$this->get_time_range_by_filter_to_cur_day() ;
     ?><div id="panel_filter">
             <form id="sky-form" class="sky-form"><input type="hidden" name="filter_dir" value="<?echo _CUR_REPORT_DIR?>">
               <fieldset>
                   <section>
                     <label class="label">IP</label>
                     <label class="input">
                         <input type="text" name="filter[ip]" value="<?echo $this->filter['ip']?>">
                     </label>
                 </section>
               <section>
                   <label class="label">Дата от</label>
                   <label class="input">
                       <i class="icon-append fa fa-calendar"></i>
                       <input type="text" name="filter[data_from]" class="datetimepicker" value="<?echo date('d.m.Y H:i',$time_from)?>">
                   </label>
                 </section>
                 <section>
                   <label class="label">Дата до</label>
                   <label class="input">
                       <i class="icon-append fa fa-calendar"></i>
                       <input type="text" name="filter[data_to]" class="datetimepicker" value="<?echo date('d.m.Y H:i',$time_to)?>">
                   </label>
                </section>
                <section>
                       <label class="label">Текст в результате</label>
                       <label class="input">
                            <input type="text" name="filter[text]" placeholder="" value="<?echo $this->filter['text']?>">
                       </label>
                 </section>
                   <section>
                        <label class="label">Сортировка:</label>
                        <label class="select">
                           <select name="filter[order]" id="order">
                               <option value="pkey desc" <?if ($this->filter['order']!='pkey') echo 'selected'?>>Обратная</option>
                               <option value="pkey" <?if ($this->filter['order']=='pkey') echo 'selected'?>>Прямая</option>
                           </select>
                        </label>
                     </section>
                  </fieldset>
                  <?$this->panel_button_apply();?>
                  <?$this->panel_buttons_action();?>
              </form>
       </div><br>
     <?
  }

  function create_usl_select_obj($_params,&$options)
  {  //damp_array($_COOKIE) ;
      /*
      $_usl=array('clss=251') ;
                if ($options['mo_id']) $_usl[]='mo_id='.$options['mo_id'] ;
                if ($options['personal_id']) $_usl[]='(user_id='.$options['personal_id'].' or member_id='.$options['personal_id'].')' ;
                if ($options['terminal_id']) $_usl[]='terminal_id='.$options['terminal_id'] ;
                if ($options['reffer']) $_usl[]='(reffer1='.$options['reffer'].' or reffer2='.$options['reffer'].' or reffer3='.$options['reffer'].' or reffer4='.$options['reffer'].' or reffer5='.$options['reffer'].')';
                if ($options['cmd']) $_usl[]='obj_name="'.$options['cmd'].'"' ;
                if ($options['text']) $_usl[]='(comment like "%'.$options['text'].'%" or data like "%'.$options['text'].'%")' ;

                $usl=implode(' and ',$_usl) ;
                $recs=execSQL('select * from '.ENGINE()->table_mo_log.' where '.$usl.' order by pkey desc limit 500') ;
                return($recs) ;
      */
     $usl=array() ;
     $usl[]='clss=50' ;
     if ($this->filter['from_id'])             $usl[]='pkey>'.$this->filter['from_id'] ;
     if ($this->filter['ip'])                  $usl[]='ip="'.$this->filter['from_id'].'"' ;
     if ($this->filter['url'])                 $usl[]='url like "%'.$this->filter['url'].'%"' ;
     if ($this->filter['url_from'])            $usl[]='url_from like "%'.$this->filter['url_from'].'%"' ;
     // время передаваемое в текстовом формате
     if ($this->filter['data_from'])           $usl[]='c_data>='.strtotime($this->filter['data_from']) ;
     if ($this->filter['data_to'])             {  $arr=explode(' ',trim($this->filter['data_to'])) ;
                                                  $time_to=(!$arr[1])? strtotime($this->filter['data_to'].' 23:59:59'):strtotime($this->filter['data_to']) ;
                                                  $usl[]='c_data<='.$time_to ;
                                               }
     $usl_res=implode(' and ',$usl) ;
     //damp_array($usl,1,-1) ;
     return($usl_res) ;
  }

 function panel_info_filter()
 { $_str=array() ; $info=array() ; $title='' ;
   //if ($this->filter['pass_creator'])                    $info['сотрудник']=ACCOUNTS()->get_member_name_by_id($this->filter['pass_creator'] )  ;
   //if ($this->filter['pass_status'])                     $info['статус']= $_SESSION['ARR_pass_status'][$this->filter['pass_status']] ;
   //if ($this->filter['personal_name'])                   $info['следует к']='"'.$this->filter['personal_name'].'"' ;
   if ($this->filter['action'])                            $info['Операция']='"'.$this->filter['action'].'"' ;
   if ($this->filter['name'])                              $info['ФИО']='"'.$this->filter['name'].'"' ;
   if ($this->filter['ip'])                                 $info['IP']='"'.$this->filter['ip'].'"' ;
   //if ($this->filter['doc_number'])                        $info['Номер документа']='"'.$this->filter['doc_number'].'"' ;
   if ($this->filter['data_from'])                         $info['c']=$this->filter['data_from'] ;
   if ($this->filter['data_to'])                           $info['по']=$this->filter['data_to'] ;
   if ($this->filter['to_zone_id'])                        $info['зона доступа']=SKD()->get_full_zone_path($this->filter['to_zone_id'])  ;
   if ($this->filter['pass_type'])                         $info['тип пропуска']=$_SESSION['ARR_pass_type'][$this->filter['pass_type']] ;
   if ($this->filter['pass_number'])                       $info['номер пропуска']='"'.$this->filter['pass_number'].'"';
   if ($this->filter['evt_type'])                          $info['событие']=$_SESSION['ARR_evt_type'][$this->filter['evt_type']] ;
   if ($this->filter['access_type'])                       $info['направление прохода']=$_SESSION['ARR_access_type'][$this->filter['access_type']-1] ;
   if ($this->filter['result'])                            $info['результат']=$_SESSION['ARR_result'][$this->filter['result']-1] ;
   if (sizeof($info)) foreach($info as $title=>$value) $_str[]=$title.': <strong>'.$value.'</strong>' ;
   if (sizeof($_str)) $title='<p class=center>'.implode(' ',$_str).'</p>' ;
   return $title ;
 }


  function get_cnt_items($usl,$options=array())
  { $options['debug']=0 ;
    $cnt=execSQL_value('select count(pkey) from  '.$this->table_name.' where '.$usl,$options) ;
    return($cnt) ;
  }

  function get_items($usl,$options=array())
  { $limit=($options['limit'])? ' '.$options['limit']:'' ;
    $order=($this->filter['order'])? ' order by '.$this->filter['order']:' order by pkey desc' ;
    $list_rec=execSQL('select * from  '.$this->table_name.'  where '.$usl.$order.$limit,$options,1) ;
    return($list_rec) ;
  }

  function print_template_HTML($list_recs,$options=array())
  { $id=($options['id'])? 'id='.$options['id']:'' ;
     $recs_account=execSQL('select pkey,obj_name,rol from obj_site_account') ;
    ?><table class="basic fz_small <?if (!$this->filter['no_full_width']) echo 'full'?> auto" <?echo $id?>>
        <thead>
            <tr><th>ID</th>
                <th>IP</th>
                <th>Дата, время</th>
                <th>Заголовок страницы</th>
                <th>URL страницы</th>
                <th>URL откуда</th>
                <th>Текст поиска</th>
                <th>user_agent</th>
            </tr>
        </thead>
        <tbody>

        <?
        if (sizeof($list_recs)) foreach($list_recs as $rec)
        {   ?><tr>
              <td class=id><? echo $rec['pkey'] ;?></td>
              <td class=id><? echo $rec['ip'] ;?></td>
              <td><? echo date('d.m.Y H:i:s',$rec['c_data']) ;?></td>
              <td class=left><? echo $rec['obj_name']?></td>
              <td class=left><a href="http://<? echo $rec['url']?>" target="_blank"><? echo $rec['url']?></a></td>
              <td class=left><? echo $rec['url_from'] ?></td>
              <td class=left><? echo $rec['search_text'] ?></td>
              <td class=left><? echo $rec['user_agent'] ?></td>
           </tr><?
        }
    ?></tbody></table>

    <? $this->panel_info_select($options) ;
  }

  function print_template_HTML_small($list_recs,$options=array())
  { $id=($options['id'])? 'id='.$options['id']:'' ;
     $recs_account=execSQL('select pkey,obj_name,rol from obj_site_account') ;
    ?><table class="basic fz_small <?if (!$this->filter['no_full_width']) echo 'full'?> auto list_logs_events" <?echo $id?>>
        <colgroup><col id=c1><col id=c2><col id=c3><col id=c3><col id=c4><col id=c5><col id=c6><col id=c7></colgroup>
        <thead>
            <tr><th>ID</th><th>Дата, время</th><th>Оператор</th><th>Данные операции</th></tr>
        </thead>
        <tbody>

        <?
        if (sizeof($list_recs)) foreach($list_recs as $rec)
        {   if (isset($rec['user_id']) and isset($recs_account[$rec['user_id']]) and !$rec['member_id'])
                { $rec['member_id']=$rec['user_id'] ; $rec['user_id']='' ;

                }
            ?><tr>
              <td class=id><? echo $rec['pkey'] ;?></td>
              <td class=data><? echo date('d.m.Y H:i:s',$rec['c_data']) ;?></td>
              <td class=name><? echo $recs_account[$rec['member_id']]['obj_name'].'<br>'.$_SESSION['ARR_roll'][$rec['rol']]['obj_name'] ?></td>
              <td class=data><? if ($rec['data']) damp_array(unserialize($rec['data']),1,-1) ?></td>
           </tr><?
        }
    ?></tbody></table>

    <? $this->panel_info_select($options) ;
  }





}

?>