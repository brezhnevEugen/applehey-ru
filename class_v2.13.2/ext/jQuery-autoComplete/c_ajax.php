<?
include_once(_DIR_TO_ENGINE.'/c_page_TXT.php') ;
class c_page_ajax extends c_page_TXT
{   public $res ;
    public $form_data  ;
    public $element_tags  ;

    function show()
    { $find=$_POST['query'] ;
      if (!$find) return ;
      $this->form_data=array() ;
      $this->element_tags=$_POST['element'] ;
      if (sizeof($_POST['form'])) foreach($_POST['form'] as $rec) $this->form_data[$rec['name']]=$rec['value'] ;

      $this->res = new stdClass() ;
      $this->res->query=$find ;

      switch($this->element_tags['cmd'])
      { case 'autocomplette_otdel':   $this->autocomplette_otdel($find) ; break ;
        case 'autocomplette_working': $this->autocomplette_working($find) ; break ;
        case 'autocomplette_personal_name':   $this->autocomplette_personal_name($find) ; break ;
        //case 'autocomplette_personal_name2':   $this->autocomplette_personal_name2($find) ; break ;
        case 'autocomplette_person_org_name':   $this->autocomplette_person_org_name($find) ; break ;
        //default:                              $this->res->suggestions[]=$this->element_tags['cmd'] ;
      }

      // удаляем дубликаты слов
      if (is_array($this->res->suggestions)) $this->res->suggestions=array_values(array_unique($this->res->suggestions)) ;
      // выдаем результат в формате JSON
      echo json_encode($this->res) ;
    }

    //  autocomplette для personal_name
    function autocomplette_personal_name($find)
    {  $sql='select  pkey,obj_name from obj_site_personals where clss=211 and '.
                          '(UPPER(obj_name) REGEXP UPPER("[[:<:]]'.addslashes($find).'") or '.   // поиск по началу слова, без учета регистра
                           'UPPER(propusk) like UPPER("'.addslashes($find).'") '.
                           //'UPPER(driving) like UPPER("'.addslashes($find).'") or '.
                           //'UPPER(tab_number) like UPPER("'.addslashes($find).'")'.
                          ') limit 10' ;
       //echo $sql ;
       $list_recs=execSQL($sql) ;
        $this->res->sql=$sql ;

       //$list_recs=execSQL('select  pkey,obj_name from obj_site_personals where clss=211 and ((obj_name) rlike ("%'.addslashes($find).'%") or (propusk) rlike ("'.addslashes($find).'") or (driving) rlike ("'.addslashes($find).'") or (tab_number) rlike ("'.addslashes($find).'")) limit 10') ;
       if (sizeof($list_recs)) foreach($list_recs as $rec) $this->res->suggestions[]=$rec['obj_name'] ;
    }

    //  autocomplette для personal_name
    /* не используется
    function autocomplette_personal_name2($find)
    {  $sql='select  pkey,obj_name,parent from obj_site_personals where clss=211 and '.
            //'UPPER(obj_name) REGEXP UPPER("[[:<:]]'.addslashes($find).'") '.   // поиск по началу слова, без учета регистра
            'UPPER(obj_name) like UPPER("'.addslashes($find).'%") '.   // поиск по началу слова, без учета регистра
            ' limit 10' ;
       //echo $sql ;
       $list_recs=execSQL($sql) ;
        $this->res->sql=$sql ;

       //$list_recs=execSQL('select  pkey,obj_name from obj_site_personals where clss=211 and ((obj_name) rlike ("%'.addslashes($find).'%") or (propusk) rlike ("'.addslashes($find).'") or (driving) rlike ("'.addslashes($find).'") or (tab_number) rlike ("'.addslashes($find).'")) limit 10') ;
       if (sizeof($list_recs)) foreach($list_recs as $rec) $this->res->suggestions[]=$rec['obj_name'].'/'.$rec['pkey'].'/'.htmlspecialchars(PERSONAL()->tree[PERSONAL()->tree[$rec['parent']]->parent]->name).'/'.htmlspecialchars(PERSONAL()->tree[$rec['parent']]->name).'/'.PERSONAL()->tree[$rec['parent']]->parent.'/'.$rec['parent'] ;
    } */

    //  autocomplette для working
    function autocomplette_working($find)
    {  // 32 - id спика "Должности компании"
       $list_recs=execSQL('select id,obj_name from '.TM_LIST.' where clss=20 and parent=32 and enabled=1 and UPPER(obj_name) rlike UPPER("'.$find.'") order by obj_name limit 10') ;
       if (sizeof($list_recs)) foreach($list_recs as $rec) $this->res->suggestions[]=$rec['obj_name'] ;
    }

    function autocomplette_otdel($find)
    {  if (!$this->form_data['org_id']) return ;
      $list_recs=execSQL('select pkey,obj_name from '.PERSONAL()->table_name.' where clss=208 and parent='.$this->form_data['org_id'].' and enabled=1 and UPPER(obj_name) rlike UPPER("'.$find.'") order by obj_name limit 10') ;
      if (sizeof($list_recs)) foreach($list_recs as $rec) $this->res->suggestions[]=$rec['obj_name'] ;

    }

    function autocomplette_person_org_name($find)
    { // if (!$this->form_data['org_name']) return ;

      $list_recs=execSQL('select pkey,company from '.BP()->table_persons.' where clss=511 and enabled=1 and company_UP rlike UPPER("'.addslashes($find).'") order by company limit 10') ;
      if (sizeof($list_recs)) foreach($list_recs as $rec) $this->res->suggestions[]=htmlspecialchars($rec['company']) ;

    }



}