$j(function()
{
	$j('.modal-opener').on('click', function()
	{
		if( !$j('#sky-form-modal-overlay').length )
		{
			$j('body').append('<div id="sky-form-modal-overlay" class="sky-form-modal-overlay"></div>');
		}		
	
		$j('#sky-form-modal-overlay').on('click', function()
		{
			$j('#sky-form-modal-overlay').fadeOut();
			$j('.sky-form-modal').fadeOut();
		});
		
		form = $j($j(this).attr('href'));
		$j('#sky-form-modal-overlay').fadeIn();
		form.css('top', '50%').css('left', '50%').css('margin-top', -form.outerHeight()/2).css('margin-left', -form.outerWidth()/2).fadeIn();
		
		return false;
	});
	
	$j('.modal-closer').on('click', function()
	{
		$j('#sky-form-modal-overlay').fadeOut();
		$j('.sky-form-modal').fadeOut();
		
		return false;
	});
});