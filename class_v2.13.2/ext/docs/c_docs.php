<?php
include_once(_DIR_TO_CLASS."/c_page.php");
class c_page_cab_docs extends c_page
{
  public $h1='Документы' ;
  public $rec_doc=array() ;
  public $filter=array() ;
  public $use_datepicker=1 ;
  public $body_class='printSelected' ;
  
  function select_obj_info()
  { if (_CUR_PAGE_NAME)
    { $arr=explode('.',_CUR_PAGE_NAME) ; //damp_array($arr) ;
      $this->rec_doc=DOCS()->get_doc_by_id($arr[0]) ;
      if ($this->rec_doc['pkey'])
      { $this->h1=$this->rec_doc['obj_name'] ;
        $this->no_panel_smena_is_out=1 ;
      }
      else $this->result=404 ;
    }
    if (_CUR_FILTER)
    {  $this->filter=REPORTS()->parse_url(_CUR_FILTER,array()) ;
    }
  }

 function block_main()
 { if (MEMBER()->info['sert_id']) {?><SCRIPT type="text/javascript" src="/class/ext/ecp/script.js"></SCRIPT>
                                     <object type="application/x-rutoken-pki" id="plugin-object" width="0" height="0"><param name="onload" value="onPluginLoaded"/></object><?}
   //damp_array(MEMBER()) ;
   $this->page_title() ;
   if ($this->rec_doc['pkey']) $this->panel_doc_view() ;
   else                        $this->panel_doc_list() ;
 }

function panel_doc_list()
 {
   $recs_account=execSQL_row('select distinct(member_id) as member_id,(select obj_name from obj_site_account where clss=210 and pkey=member_id) as member_name from obj_site_docs where member_id>0 ') ;
   if (MEMBER()->rol>1 or $this->filter['member'])
   { $member_id=($this->filter['member'])? $this->filter['member']:MEMBER()->id ;
     $docs_series=execSQL('select distinct(t1.parent),t2.pkey as id2,t2.obj_name as name2,t2.parent as parent2,t3.pkey as id3,t3.obj_name as name3,t3.parent as parent3 from obj_site_docs t1 '.
                          'left join obj_site_docs t2 on t2.pkey=t1.parent '.
                          'left join obj_site_docs t3 on t3.pkey=t2.parent '.
                          'where t1.member_id='.$member_id.' and t1.clss=215
     ') ;
     if (sizeof($docs_series)) foreach($docs_series as $rec)
     { if ($rec['parent2']==1) $doc_types[]=array('pkey'=>$rec['id2'],'obj_name'=>$rec['name2']) ;
       if ($rec['parent3']==1) $doc_types[]=array('pkey'=>$rec['id3'],'obj_name'=>$rec['name3']) ;
     }
     //print_2x_arr($doc_types) ;

   } else $doc_types=execSQL('select pkey,obj_name,doc_name from obj_site_docs where parent=1') ;


   //damp_array($doc_types) ;
   //damp_array(REPORTS()->reports_list) ;

   ?><form>
     <table class="basic fz_small b2">
                  <?if (MEMBER()->rol==1){?>
                  <tr><td>Автор документа:</td>
                       <td><select id=member name="filter[member]"><option></option>
                               <?if (sizeof($recs_account))foreach($recs_account as $member_id=>$member_name) {?><option value="<?echo $member_id?>" <?if ($member_id==$this->filter['member']) echo 'selected'?>><?echo $member_name?></option><?} ?>
                           </select></td>
                   </tr><?}?>
                  <tr><td>Тип документа:</td>
                       <td><select id=doc_type name="filter[doc_type]"><option></option>
                               <?if (sizeof($doc_types))foreach($doc_types as $rec_type)
                               { $report_obj=REPORTS()->get_report_obj_by_code($rec_type['obj_name']) ;
                                 $report_name=trim($rec_type['obj_name']) ;
                                 if ($report_obj->error) $res['error']=$report_obj->error ;
                                 else
                                 { $report_name=$report_obj->title ;
                                 }
                                 ?><option value="<?echo $rec_type['pkey']?>" <?if ($rec_type['pkey']==$this->filter['doc_type']) echo 'selected'?>><?echo $report_name?></option><?
                               }
                               ?>
                           </select></td>
                  </tr>
                  <tr><td>Дата документа</td>
                      <td><input type="text" name="filter[data_from]" class="datepicker" value="<?echo ($this->filter['data_from'])? $this->filter['data_from']:''?>"> по <input type="text" name="filter[data_to]" class="datepicker" value="<?echo ($this->filter['data_to'])? $this->filter['data_to']:''?>"></td>
                  </tr>
                  <tr><td></td>
                      <td><button class="v2" cmd="set_filter" filter_dir="<?echo _CUR_CAB_DIR?>">Искать</button></td>
                  </tr>

              </table>
              <script type="text/javascript">$j(document).ready(function (){
                      $j('input.datepicker').datepicker($j.datepicker.regional["ru"]);
                      //new mForm.Element.Select({original: 'member_id'});
                      //new mForm.Element.Select({original: 'doc_type'});
                  }) ;</script>


         <div id="panel_debug"></div>
     <?
   if (!sizeof($this->filter)) return ;
   $usl=array() ;
   $usl[]='clss=215' ;
   if (MEMBER()->rol>1)             $usl[]='member_id='.MEMBER()->id ;
   if ($this->filter['member'])     $usl[]='member_id='.$this->filter['member'] ;
   if ($this->filter['data'])       $usl[]='c_data>='.strtotime($this->filter['data']).' and c_data<='.strtotime($this->filter['data'].' 23:59:59') ;
   if ($this->filter['data_from'])  $usl[]='c_data>='.strtotime($this->filter['data_from']) ;
   if ($this->filter['data_to'])    $usl[]='c_data<='.strtotime($this->filter['data_to'].' 23:59:59') ;
   if ($this->filter['doc_type'])
   { $docs_series=execSQL_line('select pkey from obj_site_docs where parent='.$this->filter['doc_type'].' and clss=1') ;
     $docs_series[]=$this->filter['doc_type'] ;
     $series_ids=implode(',',$docs_series) ;
     $usl[]='parent in ('.$series_ids.')' ;
   }

   $usl_res=implode(' and ',$usl) ;
   //damp_array($usl) ;

   $cnt=execSQL_value('select count(pkey) from  obj_site_docs where '.$usl_res) ;
   echo '<strong>Получено '.$cnt.' записей </strong><br>' ;
   if ($cnt) show_list_items('obj_site_docs',$usl_res,'docs/list_docs',array('order'=>'pkey desc','panel_select_pages'=>1)) ;
 }

 function panel_doc_view($options=array())
 { global  $member ;
    ?><div id="panel_report" class="printSelection">
         <div class=panel_report_info>
           <div class="panel_left">
             <div class="autor">Создан: <strong><?echo $this->rec_doc['_member_name']?></strong></div>
             <div class="time">Дата создания: <strong><?echo $this->rec_doc['_date_create']?></strong></div>
             <div class="autor">Подписан: <strong><?echo ($this->rec_doc['sign'])? 'Да':'Нет'?></strong></div>
             <?if ($this->rec_doc['sign']){?><div class="time">Дата подписи: <strong><?echo $this->rec_doc['_date_podpis']?></strong></div><?}?>
           </div>
           <div class="clear"></div>
         </div>
     <?
     if (!$this->rec_doc['code']) $this->rec_doc['code']='PDF' ; // для совместимости с документами прежней версии
     if ($this->rec_doc['doc_name']) switch($this->rec_doc['code'])
     { case 'HTML':  $html=file_get_contents(_DIR_TO_ROOT.'/docs/'.$this->rec_doc['doc_name']) ;
                     echo $html ;
                     /*?><iframe src="http://<?echo _MAIN_DOMAIN?>/docs/<?echo $this->rec_doc['doc_name']?>.html" style="width:100%; height:700px;" frameborder="0" /></iframe><?*/
                     break ;
       case 'PDF':   ?><!--<iframe src="http://<?echo _MAIN_DOMAIN?>/AddOns/pdf.js-master/web/viewer.html?file=http://<?echo _MAIN_DOMAIN?>/docs/<?echo $this->rec_doc['doc_name']?>" style="width:100%; height:700px;" frameborder="0" /></iframe>-->
                       <iframe src="http://<?echo _MAIN_DOMAIN?>/docs/<?echo $this->rec_doc['doc_name']?>" style="width:100%; height:700px;" frameborder="0" /></iframe><?
                     break ;
       default:

     }
     ?><br><br><?

     ?></div>
     <?if (!$options['no_buttons']){?>
       <div class="buttons">
            <?if ($this->rec_doc['code']=='PDF'){?><button class="v1" href="http://<?echo _MAIN_DOMAIN?>/docs/<?echo $this->rec_doc['doc_name']?>" window="1">Скачать PDF</button><?}?>
            <!--<button onclick="javascript:window.print();">Печать</button>-->
            <?if ($this->rec_doc['code']=='PDF' and !$this->rec_doc['sign'] and $member->id==$this->rec_doc['member_id'])
            {   // если пользователь имеет сертификат - то возможность подписания документа
                if (MEMBER()->info['sert_id'] or $_SESSION['LS_evulate_ECP']) {?><button class="v2" cmd="ecp/sign_doc" doc_id="<?echo $this->rec_doc['pkey']?>">Подписать документ</button><?}
                else echo 'У вас нет сертификата для подписи документа' ;
                // пока не подписан - можно удалить
                ?><button class="v2" cmd="docs/doc_delete" doc_id="<?echo $this->rec_doc['pkey']?>">Удалить</button><?
            }
            if ($_SESSION['member']->info['smena_out'] and $this->no_panel_smena_is_out) $this->panel_smena_is_out(array('no_img'=>1)) ;

            ?>
       </div><?}?>
     <?
     //$doc_rec=DOCS()->get_doc_by_id($doc_id) ;
    //damp_array($this->rec_doc) ;
    //damp_array($_SESSION['member']->info) ;
 }

}



?>