/**
 * FancyUpload Showcase
 *
 * @license		MIT License
 * @author		Harald Kirschner <mail [at] digitarald [dot] de>
 * @copyright	Authors
 */

window.addEvent('domready', function()
{ // wait for the content
	// our uploader instance
    var up = new FancyUpload2($('demo-status'), $('demo-list'),
    {   // options object
		// we console.log infos, remove that in production!!
		verbose: true,
        appendCookieData:true,
		//data:{member_id:$('member_id').get('value'),trace_to_email:$('trace_to_email').get('checked')},
		// url is read from the form, so you just have to change one place
		url: $('form-demo').action,
        data: {session_id:$('session_id').get('value')},

		// path to the SWF file
		path: '/class/ext/fancyupload-v3.0.1/source/Swiff.Uploader.swf',
		
		// remove that line to select all files, or edit it, add more items
		//typeFilter: {'Images (*.jpg, *.jpeg, *.gif, *.png)': '*.jpg; *.jpeg; *.gif; *.png'},
		//typeFilter: {'Excel (*.xls, *.xlsx)': '*.xls; *.xlsx; *.gif; *.png'},
		typeFilter: {'Spark files (tv_prog.xml, tv_fav.xml)': 'tv_prog.xml; tv_fav.xml;'},

		// this is our browse button, *target* is overlayed with the Flash movie
		target: 'demo-browse',
		
		// graceful degradation, onLoad is only called if all went well with Flash
		onLoad: function()
        {
			$('demo-status').removeClass('hide'); // we show the actual UI
			if ($('demo-fallback')!=null) $('demo-fallback').destroy(); // ... and hide the plain form
			// We relay the interactions with the overlayed flash to the link
			this.target.addEvents({click: function() {return false;},
				                   mouseenter: function() {this.addClass('hover');},
				                   mouseleave: function() {this.removeClass('hover');this.blur();},
				                   mousedown: function() {this.focus();}
			});

			// Interactions for the 2 other buttons
            // remove all files
			//$('demo-clear').addEvent('click', function() {up.remove();  return false;});
            // start upload
			$('demo-upload').addEvent('click', function() { up.start(); return false;});
		},
        onStart:function()
        { //var trace_to_email=jQuery('input#trace_to_email').attr('value');
          //var trace_to_email=$('trace_to_email').get('checked');
          //var member_id=$('member_id').get('value') ;
          //var data='test=testttsts' ;
          //  member_id:member_id,trace_to_email:trace_to_email} ;
          //this.options.data.trace_to_email=jQuery('input#trace_to_email').attr('check');
          //this.options.data.send_to_email=jQuery('input#send_to_email').attr('val');
          //alert($j('#send_to_email').attr('val')) ;
          //alert(this.options.data.member_id) ;
          //alert(member_id+'-'+this.options.data.trace_to_email) ;
        },
		
		// Edit the following lines, it is your custom event handling
		
		/**
		 * Is called when files were not added, "files" is an array of invalid File classes.
		 * 
		 * This example creates a list of error elements directly in the file list, which
		 * hide on click.
		 */ 
		onSelectFail: function(files)
        {
            files.each(function(file)
              {	new Element('li',
                 {	'class': 'validation-error',
					html: file.validationErrorMessage || file.validationError,
					title: MooTools.lang.get('FancyUpload', 'removeTitle'),
					events: {click: function() {this.destroy();}
                 }
			  }).inject(this.list, 'top');
			}, this);
		},
		
		/**
		 * This one was directly in FancyUpload2 before, the event makes it
		 * easier for you, to add your own response handling (you probably want
		 * to send something else than JSON or different items).
		 */
		onFileSuccess: function(file, response) {
			var json = new Hash(JSON.decode(response, true) || {});

			if (json.get('status') == '1') {
				file.element.addClass('file-success');
				//file.info.set('html', '<strong>Файл успешно загружен:</strong> ' + json.get('width') + ' x ' + json.get('height') + 'px, <em>' + json.get('mime') + '</em>)');
				//file.info.set('html', '<strong>"'+json.get('price_name')+'"</strong>: <strong>'+json.get('import_count')+'</strong> позиций на сумму <strong>'+json.get('import_price')+'</strong>'+json.get('trace')+'</p></em>');
				file.info.set('html', json.get('session_id')+'<br>'+json.get('trace'));
			} else {
				file.element.addClass('file-failed');
				file.info.set('html', '<strong>Ошибка загрузки:</strong> ' + (json.get('error') ? (json.get('error')) : response));
			}
		},
		
		/**
		 * onFail is called when the Flash movie got bashed by some browser plugin
		 * like Adblock or Flashblock.
		 */
		onFail: function(error) {
			switch (error) {
				case 'hidden': // works after enabling the movie and clicking refresh
					alert('To enable the embedded uploader, unblock it in your browser and refresh (see Adblock).');
					break;
				case 'blocked': // This no *full* fail, it works after the user clicks the button
					alert('To enable the embedded uploader, enable the blocked Flash movie (see Flashblock).');
					break;
				case 'empty': // Oh oh, wrong path
					alert('A required file was not found, please be patient and we fix this.');
					break;
				case 'flash': // no flash 9+ :(
					alert('To enable the embedded uploader, install the latest Adobe Flash plugin.')
			}
		}
		
	});
	
});