<?php
class c_page_fancyupload
{
    function check_autorize(){return(1);}

    function show()
    {   ob_start() ;
        $result = array();

        $result['time'] = date('r');
        $result['addr'] = substr_replace(gethostbyaddr($_SERVER['REMOTE_ADDR']), '******', 0, 6);
        $result['agent'] = $_SERVER['HTTP_USER_AGENT'];

        if (count($_GET)) {$result['get'] = $_GET;}
        if (count($_POST)) {$result['post'] = $_POST;}
        if (count($_FILES)) {$result['files'] = $_FILES;}
        if (count($_SERVER)) {$result['server'] = $_SERVER;}
        if (count($_COOKIE)) {$result['cookie'] = $_COOKIE;}

        // we kill an old file to keep the size small
        if (file_exists('script.log') && filesize('script.log') > 102400) {unlink('script.log');}

        $log = @fopen('script.log', 'a');
        if ($log) {fputs($log, print_r($result, true) . "\n---\n");fclose($log);}

        $return=array() ;  $result_text='' ; $result_code='' ;  $event_id=0 ;

        // Проверка загруженного файла
        $error = false;
        if (!isset($_FILES['Filedata']) || !is_uploaded_file($_FILES['Filedata']['tmp_name'])) {$error = 'Не удалось загрузить файл'; $result_code=7 ;}
        // переносим файл в папку пользователя
        $member_dir=_DIR_TO_ROOT.'/private/'.$_POST['member_id'].'/' ;
        $dest_file=$member_dir.$_FILES['Filedata']['name'] ;
        // определяем тип файла
           if (strpos($dest_file,'.xls')!==false)
           {  $event_id=1 ;
              ob_start() ;
              //  удаляем все прыдущие файлы XLS
              $list=get_files_list($member_dir,array('ext_pattern'=>'xls')) ;
              if (sizeof($list)) foreach($list as $file_dir) unlink($file_dir) ;

              if (move_uploaded_file($_FILES['Filedata']['tmp_name'],$dest_file))
              { echo '<h2>Читаем файл XLS</h2>' ;
                include_once(_DIR_EXT.'/angel-discount/import_kadr.php') ;
                list($result_text,$error)=import_from_excel_parsing_file_to_table($dest_file,$member_dir) ;
                if (!$error) $result_code=1 ; else $result_code=7 ;
              }
              else
              { $error=$result_text='Не удалось поместить файл в папку пользователя' ;
                $result_code=6 ;
              }
              $return['table_xls']=ob_get_clean()  ;

           }
           if (strpos($dest_file,'.jpg')!==false)
           {  if (move_uploaded_file($_FILES['Filedata']['tmp_name'],$dest_file))
              {   $event_id=2 ;
                  $img_dir='/private/'.$_POST['member_id'].'/'.basename($dest_file) ;
                  list($name,$ext)=explode('.',basename($dest_file)) ;
                  list($kadr,$number)=explode('-',$name) ;
                  $number=(int)$number ;
                  $kadr=(int)$kadr ;
                  if (!$number) $number=0 ;
                  $size=getimagesize(_DIR_TO_ROOT.$img_dir);
                  $true_name=($number)? $kadr.'-'.$number.'.jpg':$kadr.'.jpg' ;
                  global $member ;
                  $db_kadr_rec=execSQL_van('select * from obj_site_kadr where kadr="'.$kadr.'" and member_id='.$member->id.' and goods_id>0 and sortament_id>0 and code!=""') ;
                  if (!$size[0] or !$size[1])
                  { $error=$result_text='Не удалось определить размер изображения.' ;
                    $result_code=5 ;
                  }
                  else if ($true_name!=basename($dest_file))
                  { $error=$result_text='Формат имени файла не соответсвует шаблону' ;
                    $result_code=2 ;
                  }
                  else if (!isset($_SESSION['uploaded_kadr_info'][$kadr]) and !$db_kadr_rec['pkey'])
                  { $error=$result_text='Кадр ('.$kadr.') для данного фото не задан в XLS или базе кадров' ;
                    $result_code=3 ;
                  }
                  else if ($size[0]>1500 or $size[1]>1500)
                  { $error=$result_text='Превышен допустимый размер в 1500px по одной из сторон изображения. ('.$size[0].'x'.$size[1].')' ;
                    $result_code=4 ;
                  }
                  else
                  { $result_code=1 ;
                    $result_text=$size[0].'x'.$size[1].', '.format_size(filesize(_DIR_TO_ROOT.$img_dir));
                    if (isset($_SESSION['uploaded_kadr_info'][$kadr]))
                    { list($id,$rec)=each($_SESSION['uploaded_kadr_info'][$kadr]) ;
                      if (!$rec['goods_id'])  $result_text.=', товар ['.$rec['code'].'] не найден' ; //.print_r($rec,true) ;
                    }
                    $return['img_src']=$img_dir ;
                    $return['img_name']=basename($dest_file) ;
                    $return['kadr']=$kadr;
                    $return['number']=$number;
                  }


              }
              else
              { $error=$result_text='Не удалось поместить файл в папку пользователя' ;
                $result_code=6 ;
              }
           }



        $rec_log=array('clss'=>212,
                       'member_id'=>$_POST['member_id'],
                       'event_id'=>$event_id,
                       'result_text'=>$result_text,
                       'result_code'=>$result_code
                      ) ;
        if ($event_id==1) $rec_log['file']=$_FILES['Filedata']['name'] ;
        if ($event_id==2) $rec_log['img_name']=$_FILES['Filedata']['name'] ;

        adding_rec_to_table('obj_site_stats_uploads',$rec_log) ;

        if ($error)
        { $return['status']=0 ;
          $return['error']=$error ;
          //if (is_file($dest_file)) unlink($dest_file) ;
        }
        else
        { $return['status']=1 ;
          $return['name']=$_FILES['Filedata']['name'] ;
        }

        $return['debug']=ob_get_clean()  ;
        // Output
        /**
         * Again, a demo case. We can switch here, for different showcases
         * between different formats. You can also return plain data, like an URL
         * or whatever you want.
         *
         * The Content-type headers are uncommented, since Flash doesn't care for them
         * anyway. This way also the IFrame-based uploader sees the content.
         */

        if (isset($_REQUEST['response']) && $_REQUEST['response'] == 'xml')
        {	header('Content-type: text/xml');
        	// Really dirty, use DOM and CDATA section!
        	echo '<response>';
        	foreach ($return as $key => $value)  echo "<$key><![CDATA[$value]]></$key>";
        	echo '</response>';
        }
        else
        {	header('Content-type: application/json');
        	echo json_encode($return);
        }
    }

} ;
?>