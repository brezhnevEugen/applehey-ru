<?php
include_once(_DIR_TO_MODULES.'/reports/i_report.php') ;
class report_log_events extends c_report
{
 var $title='Журнал событий' ;
 //var $allow_to_rol=array() ;
 var $filter=array() ;
 var $view_by_space_filter=1 ;
 var $use_jwplayer=1 ;

 function setting($options=array())
 {   ?><div id="panel_filter" class="one_row">
             <form id="sky-form" class="sky-form"><input type="hidden" name="filter_dir" value="<?echo _CUR_REPORT_DIR?>">
                  <fieldset>
                      <div class="row" id="person_info">
                          <section class="col col-2">
                               <label class="label">Событие2:</label>
                               <label class="select">
                                  <? $recs=execSQL_line('select distinct(obj_name) from '.ENGINE()->table_mo_log.' where obj_name!="" order by obj_name') ;?>
                                     <select name="filter[cmd]" id="to_zone_id"  class="select2 js-example-basic-multiple"><option value="0">Все события</option>
                                        <? if (sizeof($recs)) foreach($recs as $value) {?><option value="<?echo $value?>" <?if ($this->filter['action']==$value) echo 'selected'?>><?echo $value?></option><?} ?>
                                    </select>
                               </label>
                            </section>
                          <section class="col col-2">
                              <label class="label">Дата от</label>
                              <label class="input">
                                  <i class="icon-append fa fa-calendar"></i>
                                  <input type="text" name="filter[data_from]" class="datetimepicker" value="<?if ($this->filter['data_from']) echo $this->filter['data_from']?>">
                              </label>
                            </section>
                            <section class="col col-2">
                              <label class="label">Дата до</label>
                              <label class="input">
                                  <i class="icon-append fa fa-calendar"></i>
                                  <input type="text" name="filter[data_to]" class="datetimepicker" value="<?if ($this->filter['data_to']) echo $this->filter['data_to']?>">
                              </label>
                          </section>
                          <section class="col col-2">
                               <label class="label">Сортировка:</label>
                               <label class="select">
                                  <select name="filter[order]" id="order" class="select2 js-example-basic-multiple">
                                      <option value="0" <?if (!$this->filter['order']) echo 'selected'?>>Обратная</option>
                                      <option value="1" <?if ($this->filter['order']==1) echo 'selected'?>>Прямая</option>
                                  </select>
                               </label>
                            </section>

                        </div>
                  </fieldset>
                 <div id="panel_buttons"><input type="submit" class="button v2" cmd="set_filter" value="OK"></div>
              </form>
       </div>
     <?
  }


 function setting_details($options=array())
 {   ?><div id="panel_filter">
             <form id="sky-form" class="sky-form"><input type="hidden" name="filter_dir" value="<?echo _CUR_REPORT_DIR?>">
               <fieldset>
                 <section>
                    <label class="label">Событие:</label>
                    <label class="select">
                       <? $recs=execSQL_line('select distinct(obj_name) from '.ENGINE()->table_mo_log.' where obj_name!="" order by obj_name') ;?>
                          <select name="filter[cmd]" id="to_zone_id"  class="select2 js-example-basic-multiple"><option value="0">Все события</option>
                             <? if (sizeof($recs)) foreach($recs as $value) {?><option value="<?echo $value?>" <?if ($this->filter['action']==$value) echo 'selected'?>><?echo $value?></option><?} ?>
                         </select>
                    </label>
                 </section>
               <section>
                   <label class="label">Дата от</label>
                   <label class="input">
                       <i class="icon-append fa fa-calendar"></i>
                       <input type="text" name="filter[data_from]" class="datetimepicker" value="<?if ($this->filter['data_from']) echo $this->filter['data_from']?>">
                   </label>
                 </section>
                 <section>
                   <label class="label">Дата до</label>
                   <label class="input">
                       <i class="icon-append fa fa-calendar"></i>
                       <input type="text" name="filter[data_to]" class="datetimepicker" value="<?if ($this->filter['data_to']) echo $this->filter['data_to']?>">
                   </label>
                </section>
                <section>
                       <label class="label">Текст в результате</label>
                       <label class="input">
                            <input type="text" name="filter[text]" placeholder="" lang="RUS" value="<?echo $this->filter['text']?>">
                       </label>
                 </section>
                   <section>
                        <label class="label">Сортировка:</label>
                        <label class="select">
                           <select name="filter[order]" id="order" class="select2 js-example-basic-multiple">
                               <option value="pkey desc" <?if ($this->filter['order']!='pkey') echo 'selected'?>>Обратная</option>
                               <option value="pkey" <?if ($this->filter['order']=='pkey') echo 'selected'?>>Прямая</option>
                           </select>
                        </label>
                     </section>
                  </fieldset>
                  <?$this->panel_button_apply();?>
                  <?$this->panel_buttons_action();?>
              </form>
       </div><br>
     <?
  }

  function create_usl_select_obj($_params,&$options)
  {  $usl=array() ;
     $usl[]='clss=251' ;
     if ($this->filter['from_id'])             $usl[]='pkey>'.$this->filter['from_id'] ;
     if ($this->filter['action'])              $usl[]='obj_name like "'.$this->filter['action'].'"' ;
     if ($this->filter['user_id'])              $usl[]='user_id="'.$this->filter['user_id'].'"' ;

     if ($this->filter['reffer'])              $usl[]='(reffer1="'.$this->filter['reffer'].'" or'.
                                                      ' reffer2 like "'.$this->filter['reffer'].'" or'.
                                                      ' reffer3 like "'.$this->filter['reffer'].'" or'.
                                                      ' reffer4 like "'.$this->filter['reffer'].'" or'.
                                                      ' reffer5 like "'.$this->filter['reffer'].'")' ;

     // время передаваемое в текстовом формате
     if ($this->filter['data_from'])           $usl[]='c_data>='.strtotime($this->filter['data_from']) ;
     if ($this->filter['data_to'])             {  $arr=explode(' ',trim($this->filter['data_to'])) ;
                                                  $time_to=(!$arr[1])? strtotime($this->filter['data_to'].' 23:59:59'):strtotime($this->filter['data_to']) ;
                                                  $usl[]='c_data<='.$time_to ;
                                               }

     // текст внутри результата
     if ($this->filter['text'])              $usl[]='comment like "%'.$this->filter['text'].'%"' ;

     $usl_res=implode(' and ',$usl) ;
     //вdamp_array($usl,1,-1) ;
     return($usl_res) ;
  }

 function panel_info_filter()
 { $_str=array() ; $info=array() ; $title='' ;
   if ($this->filter['action'])                            $info['Операция']='"'.$this->filter['action'].'"' ;
   if ($this->filter['data_from'])                         $info['c']=$this->filter['data_from'] ;
   if ($this->filter['data_to'])                           $info['по']=$this->filter['data_to'] ;
   if ($this->filter['text'])                              $info['текст']=$this->filter['text'] ;
   if (sizeof($info)) foreach($info as $title=>$value) $_str[]=$title.': <strong>'.$value.'</strong>' ;
   if (sizeof($_str)) $title='<p class=center>'.implode(' ',$_str).'</p>' ;
   return $title ;
 }


  function get_cnt_items($usl,$options=array())
  { $cnt=execSQL_value('select count(pkey) from  '.ENGINE()->table_mo_log.' where '.$usl,$options) ;
    return($cnt) ;
  }

  function get_items($usl,$options=array())
  { $limit=($options['limit'])? ' '.$options['limit']:'' ;
    $order=($this->filter['order'])? ' order by '.$this->filter['order']:' order by pkey desc' ;
    $list_rec=execSQL('select * from  '.ENGINE()->table_mo_log.' where '.$usl.$order.$limit,$options,1) ;
    return($list_rec) ;
  }


 function print_template_HTML2($list_recs,$options)
 {  ?><table class="basic fz_small <?if (!$this->filter['no_full_width']) echo 'full'?> auto">
       <tr>
           <th>ID</th>
           <th>IP</th>
           <th>Событие</th>
           <th>Время</th>
           <th>Зона доступа</th>
           <th>Турникет</th>
           <th>Направление</th>
           <th>Результат</th>
           <th>Ф.И.О.</th>
           <th>Пропуск</th>
       </tr>
     <? //$i=($options['first_obj'])? $options['first_obj']:1;
        if (sizeof($list_recs)) foreach($list_recs as $rec)
        { ?><tr>
                <td><?echo $rec['pkey']?></td>
                <td><?echo $rec['IP']?></td>
                <td><?echo $_SESSION['ARR_evt_type'][$rec['evt_type']]?></td>
                <td><?echo date('d.m.Y H:i:s',$rec['c_data'])?>
                     <?if ($rec['_turn_stream_name']){?>&nbsp;<span class="v2" cmd="tracking/get_video_to_pass" stream="<?echo $rec['_turn_stream_name']?>" evt_id="<?echo $rec['pkey']?>"><i class="fa fa-video-camera"></i></span><?}?>
                </td>
                <td class="left"><?echo SKD()->get_full_zone_path($rec['zone_id'])?></td>
                <td class="left"><?echo $_SESSION['ARR_device_name_small'][$rec['_device_clss']].'' ;
                                  if ($rec['_device_id']) echo $rec['_device_id'] ; else echo $rec['_device_turn_id'];
                    ?></td>
                <td><?echo (!$rec['access_type'])? 'Вход':'Выход'?></td>
                <td><?echo ($rec['result'])? '<span class="green">OK</span>':'<span class="red">'.$rec['obj_name'].'</span>'?></td>
                <td class="left"><a href="/cab/bp/person/<?echo $rec['person_id']?>/" class="new_window" title="win_second"><?echo $rec['_person_fio']?></a></td>
                <td class="left"><?if ($rec['pass_id']) { echo $_SESSION['ARR_pass_type_small'][$rec['pass_type']]?> № <?echo $rec['pass_id']?><?}
                                   if ($rec['pass_image']) {?><i class="fa fa-file-image-o fz_18 v2" cmd="aspmo_bp/get_gate_pass_image" event_id="<?echo $rec['pkey']?>"></i><?}

                    ?></td>
            </tr>
       <?}
     ?>
     </table><?

     $this->panel_info_select($options) ;

 }

  function print_template_HTML($list_recs,$options=array())
  { $id=($options['id'])? 'id='.$options['id']:'' ;
//damp_array($list_recs,1,-1) ;
     $recs_account=execSQL('select pkey,obj_name,rol from obj_site_account') ;
    ?><!--<link rel="stylesheet" type="text/css" href="<?echo _PATH_TO_EXT?>/jquery.tableScroll/jquery.tablescroll.css"/>
      <script type="text/javascript" src="<?echo _PATH_TO_EXT?>/jquery.tableScroll/jquery.tablescroll.js"></script>
      <script type="text/javascript" src="<?echo _PATH_TO_EXT?>/jquery.tableScroll/jquery.tablescroll.js"></script>-->
      <table class="basic fz_small <?if (!$this->filter['no_full_width']) echo 'full'?> auto list_logs_events" <?echo $id?>>
        <colgroup><col id=c1><col id=c2><col id=c3><col id=c3><col id=c4><col id=c5><col id=c6><col id=c7></colgroup>
        <thead>
            <tr><th>ID</th><th>IP</th><th>Дата, время</th><th>Оператор</th><th>Действие</th><th>Объекты события</th><th>Результат</th><th>Данные операции</th><th>Время<br>выполения</th></tr>
        </thead>
        <tbody>

        <?
        if (sizeof($list_recs)) foreach($list_recs as $rec)
        {   if (isset($rec['user_id']) and isset($recs_account[$rec['user_id']]) and !$rec['member_id'])
                { $rec['member_id']=$rec['user_id'] ; $rec['user_id']='' ;

                }
            $recs_reffers=array() ; $str=array() ;
            if (isset($rec['reffer1'])) $recs_reffers[]=get_obj_info($rec['reffer1']) ;
            if (isset($rec['reffer2'])) $recs_reffers[]=get_obj_info($rec['reffer2']) ;
            if (isset($rec['reffer3'])) $recs_reffers[]=get_obj_info($rec['reffer3']) ;
            if (isset($rec['reffer4'])) $recs_reffers[]=get_obj_info($rec['reffer4']) ;
            if (isset($rec['reffer5'])) $recs_reffers[]=get_obj_info($rec['reffer5']) ;
            if (sizeof($recs_reffers)) foreach($recs_reffers as $rec_reffer)
              {  $class_name=$_SESSION['descr_clss'][$rec_reffer['clss']]['name'] ;
                 $obj_name=$rec_reffer['obj_name'] ;
                 $obj_ID=$rec_reffer['pkey'] ;
                 $str[]=$class_name.' '.$obj_name.', ID'.$obj_ID.', ['.$rec_reffer['_reffer'].']' ;
              }
            ?><tr>
              <td class=id><? echo $rec['pkey'] ;?></td>
              <td class=id><? echo $rec['ip'] ;?></td>
              <td class=data><? echo date('d.m.Y H:i:s',$rec['c_data']) ;?></td>
              <td class=name><? echo $recs_account[$rec['member_id']]['obj_name'].'<br>'.$_SESSION['ARR_roll'][$rec['rol']]['obj_name'] ?></td>
              <td class=name><? echo $rec['obj_name'] ?></td>
              <td class="left"><? if (sizeof($str)) echo implode('<br>',$str) ;?></td>
              <td class="comment left"><? echo $rec['comment'] ?></td>
              <td class=data><? if ($rec['data']) damp_array(unserialize($rec['data']),1,-1) ?></td>
              <td class=time><? if ($rec['time_exec']) echo round($rec['time_exec'],5) ?></td>
           </tr><?
        }
    ?></tbody></table>

    <? $this->panel_info_select($options) ;
    //$arr_devices=SKD()->get_arr_devices();  $arr_devices_by_zone=group_by_field('parent',$arr_devices)  ;damp_array($arr_devices_by_zone) ;

    ?>
      <script>
      /*<![CDATA[*/

      jQuery(document).ready(function($)
      {
        //$j('#thetable2').tableScroll({height:500});

        //$j('#thetable2').tableScroll();
      });

      /*]]>*/
      </script>
      <?
  }





}

?>