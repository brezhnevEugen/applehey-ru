<?
include_once(_DIR_TO_ENGINE.'/c_page_XML_AJAX.php') ;
//include_once('messages.php') ;
class c_page_ajax extends c_page_XML_AJAX
{

    function report_select()
       { $rec_report=REPORTS()->reports_list[$_POST['report_type']] ;
         $script_dir=$rec_report['script_dir'] ;
         $class_name=$rec_report['class_name'] ;
         if (file_exists(_DIR_EXT.'/'.$script_dir))
            { include_once(_DIR_EXT.'/'.$script_dir) ;
              if (class_exists($class_name))
              { $this->go_url='/cab/reports/'.$_POST['report_type'].'/' ;
           }
           }
         }

       function view_as_html()
       {  $url=REPORTS()->prepare_url($_POST['filter'],array('base_url'=>$_POST['filter_dir'])) ;
          $this->go_url=$url ;
       }

       function view_as_pdf()
       {  //$_POST['filter']['view']='PDF' ;
          $_POST['filter']['view']=($_POST['mode']=='HTML2PDF')? 'prePDF':'PDF' ;
          $url=REPORTS()->prepare_url($_POST['filter'],array('base_url'=>$_POST['filter_dir'])) ;
          $this->go_url=$url ;
       }

       function view_as_xls()
       {  $_POST['filter']['view']='XLS' ;
          $url=REPORTS()->prepare_url($_POST['filter'],array('base_url'=>$_POST['filter_dir'])) ;
          $this->go_url=$url ;
       }

       function view_as_CSV()
       {  $_POST['filter']['view']='CSV' ;
          $url=REPORTS()->prepare_url($_POST['filter'],array('base_url'=>$_POST['filter_dir'])) ;
          $this->go_url=$url ;
       }

       function to_print()
       {  $_POST['filter']['view']='print' ;
          $url=REPORTS()->prepare_url($_POST['filter'],array('base_url'=>$_POST['filter_dir'])) ;
          $this->go_url=$url ;
       }

       // сохраняем отчет в документах
       function save_as_doc()
       {  $arr=explode('/',$_POST['filter_dir']) ;
          $report_type=($_POST['report_type'])? $_POST['report_type']:$arr[3] ;
          $report_mode=($_POST['report_mode'])? $_POST['report_mode']:'PDF' ; // ??? зачем, если отчет все равно сохраняется в PDF
          //damp_array($_POST) ;
          $this->add_element('show_modal_window','HTML') ;

          // комментарий к отчету
          $report_info='' ;
          if ($_POST['time_from']) $report_info=$_POST['time_from'] ;
          if ($_POST['time_to'])   $report_info.=' - '.$_POST['time_to'] ;

          $res=DOCS()->doc_create($report_type,$_POST['filter'],array('report_info'=>$report_info)) ;
          if ($res['error']) $this->add_element('show_notife',$res['error'],array('type'=>'error')) ;
          else $this->add_element('go_url','/cab/docs/'.$res['doc_id'].'.html') ;   // переходим на страницу личного кабинета
       }

  function update_log_view()
  {   $outputfile='log_'.$_POST['cmd_uid'].'.txt' ;
      if (file_exists(_DIR_TO_ROOT.'/tmp/'.$outputfile)) $text=file_get_contents(_DIR_TO_ROOT.'/tmp/'.$outputfile) ;
      else                                               $text='Не найден лог подготовки документа <strong>'.$outputfile.'</strong><br>' ;
      $this->add_element('log_view.HTML',nl2br($text)) ;
      if (strpos($text,'Done')!==false)
      { $this->add_element('buttons777.HTML','<button class="v1 button_green" href="'._PATH_TO_SITE.$_POST['pdf_dir'].'">Далее</button>') ;
        $this->add_element('JSCODE','<script type="text/javascript">$j("#log_view").stopTime("update_log_view");</script>') ;
      }

  }

}