<?php
include_once(_DIR_TO_MODULES.'/reports/i_report.php') ;
class report_log_events_old extends c_report
{
 var $title='Журнал событий' ;
 //var $allow_to_rol=array() ;
 var $filter=array() ;
 public $use_datepicker=1 ;
 var $view_by_space_filter=1 ;
 var $use_jwplayer=1 ;

 function setting($options=array())
 {   ?><div id="panel_filter" class="one_row">
             <form id="sky-form" class="sky-form">
                 <input type="hidden" name="filter_dir" value="<?echo ($options['cur_report_dir'])? $options['cur_report_dir']:_CUR_REPORT_DIR?>">
                  <fieldset>
                      <div class="row" id="person_info">
                          <section class="col col-2">
                               <label class="label">Событие:</label>
                               <label class="select">
                                  <? $usl_reffer=($this->filter['reffer'])? ' and reffer="'.$this->filter['reffer'].'"':'' ;
                                     $recs=execSQL_line('select distinct(obj_name) from log_site_events where obj_name!="" '.$usl_reffer.' order by obj_name') ;?>
                                     <select name="filter[action]" id="action"  class="select2 js-example-basic-multiple"><option value="0">Все события</option>
                                        <? if (sizeof($recs)) foreach($recs as $value) {?><option value="<?echo $value?>" <?if ($this->filter['action']==$value) echo 'selected'?>><?echo $value?></option><?} ?>
                                    </select>
                               </label>
                            </section>
                          <section class="col col-2">
                              <label class="label">Дата от</label>
                              <label class="input">
                                  <i class="icon-append fa fa-calendar"></i>
                                  <input type="text" name="filter[data_from]" class="datepicker" value="<?if ($this->filter['data_from']) echo $this->filter['data_from']?>">
                              </label>
                            </section>
                            <section class="col col-2">
                              <label class="label">Дата до</label>
                              <label class="input">
                                  <i class="icon-append fa fa-calendar"></i>
                                  <input type="text" name="filter[data_to]" class="datepicker" value="<?if ($this->filter['data_to']) echo $this->filter['data_to']?>">
                              </label>
                          </section>
                          <section class="col col-2">
                               <label class="label">Сортировка:</label>
                               <label class="select">
                                  <select name="filter[order]" id="order" class="select2 js-example-basic-multiple">
                                      <option value="pkey desc" <?if (!$this->filter['order']=='pkey desc') echo 'selected'?>>Обратная</option>
                                      <option value="pkey" <?if ($this->filter['order']=='pkey') echo 'selected'?>>Прямая</option>
                                  </select>
                               </label>
                            </section>

                        </div>
                  </fieldset>
                 <div id="panel_buttons"><input type="submit" class="button v2" cmd="set_filter" value="OK"></div>
              </form>
       </div>
     <?
  }


  function create_usl_select_obj($_params,&$options)
  {  $usl=array() ;
     $usl[]='clss=40' ;
     //damp_array($this->filter) ;
     if ($this->filter['from_id'])             $usl[]='pkey>'.$this->filter['from_id'] ;
     if ($this->filter['action'])              $usl[]='obj_name like "'.$this->filter['action'].'"' ;
     if ($this->filter['user_id'])              $usl[]='user_id="'.$this->filter['user_id'].'"' ;

     if ($this->filter['reffer'])              $usl[]='reffer="'.$this->filter['reffer'].'"' ;

     // время передаваемое в текстовом формате
     if ($this->filter['data_from'])           $usl[]='c_data>='.strtotime($this->filter['data_from']) ;
     if ($this->filter['data_to'])             {  $arr=explode(' ',trim($this->filter['data_to'])) ;
                                                  $time_to=(!$arr[1])? strtotime($this->filter['data_to'].' 23:59:59'):strtotime($this->filter['data_to']) ;
                                                  $usl[]='c_data<='.$time_to ;
                                               }

     // текст внутри результата
     if ($this->filter['text'])              $usl[]='comment like "%'.$this->filter['text'].'%"' ;

     $usl_res=implode(' and ',$usl) ;
     //damp_array($this->filter,1,-1) ;
     //damp_array($usl,1,-1) ;
     return($usl_res) ;
  }

 function panel_info_filter()
 { $_str=array() ; $info=array() ; $title='' ;
   if ($this->filter['action'])                            $info['Операция']='"'.$this->filter['action'].'"' ;
   if ($this->filter['data_from'])                         $info['c']=$this->filter['data_from'] ;
   if ($this->filter['data_to'])                           $info['по']=$this->filter['data_to'] ;
   if ($this->filter['text'])                              $info['текст']=$this->filter['text'] ;
   if (sizeof($info)) foreach($info as $title=>$value) $_str[]=$title.': <strong>'.$value.'</strong>' ;
   if (sizeof($_str)) $title='<p class=center>'.implode(' ',$_str).'</p>' ;
   return $title ;
 }


  function get_cnt_items($usl,$options=array())
  { $cnt=execSQL_value('select count(pkey) from  log_site_events where '.$usl,$options) ;
    return($cnt) ;
  }

  function get_items($usl,$options=array())
  { $limit=($options['limit'])? ' '.$options['limit']:'' ;
    $order=($this->filter['order'])? ' order by '.$this->filter['order']:' order by pkey desc' ;
    $list_rec=execSQL('select * from  log_site_events where '.$usl.$order.$limit,$options) ;
    return($list_rec) ;
  }


  function print_template_HTML($list_recs,$options=array())
  { $id=($options['id'])? 'id='.$options['id']:'' ;
     $recs_account=execSQL('select pkey,obj_name,rol from obj_site_account') ;
      //print_2x_arr($list_recs) ;
    ?><table class="basic fz_small <?if (!$this->filter['no_full_width']) echo 'full'?> auto list_logs_events" <?echo $id?>>
        <colgroup><col id=c1><col id=c2><col id=c3><col id=c3><col id=c4><col id=c5><col id=c6><col id=c7></colgroup>
        <thead>
            <tr><th>ID</th><th>IP</th><th>Дата, время</th><th>Оператор</th><th>Действие</th><th>Объекты события</th><th>Доп.инф.</th></tr>
        </thead>
        <tbody>

        <?
        $objs_info=array() ; $cnt=0;
        if (sizeof($list_recs)) foreach($list_recs as $rec)
        {
            if (isset($rec['user_id']) and isset($recs_account[$rec['user_id']]) and !$rec['member_id'])
                { $rec['member_id']=$rec['user_id'] ; $rec['user_id']='' ;

                }
            $list_reffer=explode(' ',trim($rec['links'])) ;
            if (sizeof($list_reffer)) foreach($list_reffer as $reffer)
             { if (!sizeof($objs_info[$reffer])) { $objs_info[$reffer]=select_db_ref_info($reffer) ; $cnt++ ;
                                                   switch($objs_info[$reffer]['clss'])
                                                        { case 3: $objs_info[$reffer]['obj_name']='<a href="'.img_clone($objs_info[$reffer],'source').'" onclick="return hs.expand(this)"><img src="'.img_clone($objs_info[$reffer],'small').'"></a>' ;
                                                        }
                                                 }
             }
             $comment=str_replace('=>','<span class="sd2">=></span>',$rec['comment']) ;


            ?><tr>
              <td class=id><? echo $rec['pkey'] ;?></td>
              <td class=id><? echo $rec['ip'] ;?></td>
              <td class=data><?echo get_month_year($rec['c_data'],'day month year')?><br><? echo date('H:i:s',$rec['c_data']) ;?></td>
              <td class=name><? echo $recs_account[$rec['member_id']]['obj_name'].'<br>'.$_SESSION['ARR_roll'][$rec['rol']]['obj_name'] ?></td>
              <td class=name><? echo $rec['obj_name'] ?></td>
              <td><?if (sizeof($list_reffer)) foreach($list_reffer as $reffer)
                   if ($reffer!=$options['cur_client'] and $reffer!=$rec['member'])
                     if ($objs_info[$reffer]['obj_name']) echo $objs_info[$reffer]['obj_name'].'<br>' ;?></td>
              <td class="comment left"><?echo $comment?></td>
           </tr><?
        }
    ?></tbody></table>

    <? $this->panel_info_select($options) ;
    //$arr_devices=SKD()->get_arr_devices();  $arr_devices_by_zone=group_by_field('parent',$arr_devices)  ;damp_array($arr_devices_by_zone) ;

    ?>
      <script>
      /*<![CDATA[*/

      jQuery(document).ready(function($)
      {
        //$j('#thetable2').tableScroll({height:500});

        //$j('#thetable2').tableScroll();
      });

      /*]]>*/
      </script>
      <?
  }





}

?>