<?php
include_once(_DIR_TO_MODULES.'/reports/i_report.php') ;
class report_clients extends c_report
{
 var $title='Список клиентов' ;
 var $view_by_space_filter=1 ;
 var $list_accounts=array() ;
 var $link_objs='' ;

 function __construct($create_options=array())
 { parent::__construct($create_options);
   if (MEMBER()->rol==4) $this->link_objs=get_linked_ids_to_rec(MEMBER()->info) ;
 }

 function setting($options=array())
 {   if ($this->link_objs) $usl_select_objects=(sizeof($this->link_objs))? ' and pkey in ('.$this->link_objs.')':' and pkey=0' ; else $usl_select_objects='' ;
     // Нужно чтобы в фильтрах "Кем добавлен", "Кого посетил" отображал только пользователей с ролью "Обычный персонал", от остальных смысла нет, они не регистрируют и их не посещают.
     $this->list_accounts=execSQL_row('select pkey,obj_name from obj_site_account where clss=86 and enabled=1 and rol=2 '.$usl_select_objects.' order by obj_name') ;
     ?><div id="panel_filter" class="">
             <form id="sky-form" class="sky-form"><input type="hidden" name="filter_dir" value="<?echo ($options['filter_dir'])? $options['filter_dir']:_CUR_REPORT_DIR?>">

                  <fieldset>
                      <div class="row">
                           <section class="col col-2">
                               <label class="label">ФИО</label>
                               <label class="input">
                                    <input type="text" name="filter[name]" placeholder="" value="<?echo htmlspecialchars($this->filter['name'])?>">
                               </label>
                           </section>
                          <section class="col col-2">
                              <label class="label">Номер документа</label>
                              <label class="input">
                                  <input type="text" name="filter[doc_number]" placeholder="" value="<?echo htmlspecialchars($this->filter['doc_number'])?>">
                              </label>
                          </section>
                          <section class="col col-2">
                              <label class="label">ID ФОН</label>
                              <label class="input">
                                  <input type="text" name="filter[ID_fon]" placeholder="" value="<?echo htmlspecialchars($this->filter['ID_fon'])?>">
                              </label>
                          </section>
                          <section class="col col-2">
                              <label class="label">ID Винлайн</label>
                              <label class="input">
                                  <input type="text" name="filter[ID_win]" placeholder="" value="<?echo htmlspecialchars($this->filter['ID_win'])?>">
                              </label>
                          </section>
                          <section class="col col-4">
                              <label class="label">Статус</label>
                              <label class="select">
                                  <select name="filter[member_type][]" class="js-basic-multiple" multiple="multiple">
                                      <option></option>
                                      <?if (sizeof($_SESSION['IL_cat_clients'])) foreach($_SESSION['IL_cat_clients'] as $rec)
                                      {?><option value="<?echo $rec['id']?>" <?if (isset($this->filter['member_type'][$rec['id']])) echo 'selected'?>><?echo $rec['obj_name']?></option><?}?>
                                  </select>
                              </label>
                          </section>
                          </div>
                          <div class="row">
                          <?$this->section_kem_dobavlen()?>
                          <section class="col col-2">
                              <label class="label">Отсутствовал на ППС</label>
                              <label class="input">
                                  <input name="filter[from_time]" type=text class="datepicker" value="<?echo $this->filter['from_time']?>">
                                  <!--<select name="filter[from_time]">
                                      <option value="0"></option>
                                      <option value="10" <?if ($this->filter['from_time']==10) echo 'selected'?>>Более 10 дней</option>
                                      <option value="20" <?if ($this->filter['from_time']==20) echo 'selected'?>>Более 20 дней</option>
                                      <option value="30" <?if ($this->filter['from_time']==30) echo 'selected'?>>Более 30 дней</option>
                                      <option value="50" <?if ($this->filter['from_time']==50) echo 'selected'?>>Более 50 дней</option>
                                      <option value="100" <?if ($this->filter['from_time']==100) echo 'selected'?>>Более 100 дней</option>
                                  </select>-->
                              </label>
                          </section>
                            <section class="col col-2">
                              <label class="label">Посетившие за, дней</label>
                              <label class="input">
                                <input type="text" name="filter[check_day]" value="<?echo $this->filter['check_day']?>">
                              </label>
                           </section>
                          <section class="col col-2">
                           <?if ($this->report_name=='report_clients_exists'){?>
                          <label class="label">Кого посетил</label>
                          <label class="select">
                                <select name="filter[check_id][]" class="js-basic-multiple js-states form-control" multiple="multiple">
                                    <option value="0"></option>
                                    <?if (sizeof($this->list_accounts)) foreach($this->list_accounts as $id=>$name)
                                    {?><option value="<?echo $id?>" <?if (isset($this->filter['check_id'][$id])) echo 'selected'?>><?echo $name?></option><?}?>
                                </select>
                            </label>
                          <?}?>
                        </section>
                        <?if (MEMBER()->rol==1 or MEMBER()->rol==3){?>
                          <section class="col col-2">
                            <label class="label">Сохранить как</label>
                            <label class="select">
                                <select name="filter[view]" class="select2">
                                    <option></option>
                                    <!--<option value="PDF" <? if ($this->filter['view']=='PDF') echo 'selected'?>>PDF</option>-->
                                    <option value="xls" <? if ($this->filter['view']=='XLS') echo 'selected'?>>Excel XLS</option>
                                    <!--<option value="csv" <? if ($this->filter['view']=='CSV') echo 'selected'?>>Excel CSV</option>-->
                                </select>
                            </label>
                           </section><?}?>
                           <?$this->section_view_only()?>
                           <?$this->filter_section()?>
                        </div>
                  </fieldset>
                  <div id="panel_buttons"><input type="submit" class="button v2" cmd="set_filter" value="OK"></div>
              </form>
       </div><br>
     <script type="text/javascript">
         window.onload = function () {
             $j(".js-basic-multiple").select2({
               //placeholder: "Select a state"
             });
             }  ;
     </script>

     <?
     //damp_array($this) ;
  }

  function section_view_only()
  { ?><section class="col col-4">
        <label class="label">Показывать только</label>
        <input type="checkbox" name="filter[only_my_users]" value="1" <?if ($this->filter['only_my_users']) echo 'checked'?>>&nbsp;&nbsp;Зарегистрированные на Вашем ППС<br>
        <input type="checkbox" name="filter[only_my_checked]" value="1" <?if ($this->filter['only_my_checked']) echo 'checked'?>>&nbsp;&nbsp;Посетивших на Вашем ППС
     </section>
    <?
  }

  function section_kem_dobavlen()
  {?><section class="col col-2">
          <label class="label">Кем добавлен</label>
          <label class="select">
              <select name="filter[creator_id][]" class="js-basic-multiple js-states form-control" multiple="multiple">
                  <option value="0"></option>
                  <?if (sizeof($this->list_accounts)) foreach($this->list_accounts as $id=>$name)
                  {?><option value="<?echo $id?>" <?if (isset($this->filter['creator_id'][$id])) echo 'selected'?>><?echo $name?></option><?}?>
              </select>
          </label>
      </section><?
  }

  function filter_section(){}

  function create_usl_select_obj($_params,&$options)
  {  $usl=array() ;
     $usl[]='clss=200' ;
     $usl[]='enabled=1' ;
     //damp_array($this->filter) ;
     if ($this->filter['name'])              $usl[]='(obj_name like "'.addslashes($this->filter['name']).'%" or name1 like "'.addslashes($this->filter['name']).'%" or  name2 like "'.addslashes($this->filter['name']).'%" or name3 like "'.addslashes($this->filter['name']).'%")' ;
     if ($this->filter['doc_number'])        $usl[]='(pass1 like "'.addslashes($this->filter['doc_number']).'%" or pass2 like "'.addslashes($this->filter['doc_number']).'%")' ;
     if (sizeof($this->filter['member_type']))  $usl[]='typ in ('.implode(',',array_keys($this->filter['member_type'])).')' ;
     if (sizeof($this->filter['creator_id']))   $usl[]='creator_id in ('.implode(',',array_keys($this->filter['creator_id'])).')' ;
      // для крнтролера ограничиваем только своими клиентами
     // ещё по правам Контролёров. в списке клиентов они должны видеть ВСЕХ клиентов как и Обычный персонал. И поиск должен работать по всем клиентом. Ограничения нужны на фильтры, чтобы не могли смотреть кол-во посещённых и зарегистрированных клиентов других Аккаунтов.
     //if ($this->link_objs)                      $usl[]=(sizeof($this->link_objs))? 'creator_id in ('.$this->link_objs.')':'creator_id<0' ;
     if ($this->filter['only_my_users'])     $usl[]='creator_id='.$_SESSION['member']->id ;
     if ($this->filter['ids'])     $usl[]='pkey in ('.$this->filter['ids'].')' ;
     /* версия для select
     if ($this->filter['from_time'])
       { $now=getdate(time()) ;
         $from=mktime($now['hours'],$now['minutes'],$now['seconds'],$now['mon'],$now['mday']-$this->filter['from_time'],$now['year']);
         $usl[]='check_data>0 and check_data<'.$from ;
       }
     */
     if ($this->filter['from_time']) $usl[]='check_data>0 and check_data<'.strtotime($this->filter['from_time']) ;
     if ($this->filter['ID_fon'])                          $usl[]='ID_fon="'.$this->filter['ID_fon'].'"' ;
     if ($this->filter['ID_win'])                          $usl[]='ID_win="'.$this->filter['ID_win'].'"' ;


     if ($this->filter['check_day'] and !$this->filter['check_id'] and !$this->filter['only_my_checked'])
     {  $now=getdate(time()) ;
        $from=mktime($now['hours'],$now['minutes'],$now['seconds'],$now['mon'],$now['mday']-$this->filter['check_day'],$now['year']);
        $usl[]='check_data>0 and check_data>'.$from ;
     }
     if ($this->filter['check_id'])
     {  $now=getdate(time()) ;
        $from=mktime($now['hours'],$now['minutes'],$now['seconds'],$now['mon'],$now['mday']-$this->filter['check_day'],$now['year']);
        $usl_data=($this->filter['check_day'])? 'and c_data>'.$from:'' ;
         // для крнтролера ограничиваем только своими клиентами
         if ($this->link_objs)  $usl_contr=(sizeof($this->link_objs))? ' and member_id in ('.$this->link_objs.') ':' and member_id<0 ' ; else $usl_contr='' ;


        $sql='select distinct(client_id) from log_site_events where event_id=3 '.$usl_data.$usl_contr.' and member_id in ('.implode(', ',array_keys($this->filter['check_id'])).')' ;
        $arr_ids=execSQL_line($sql,0) ;
        //$usl[]='pkey in (select distinct(client_id) from log_site_events where event_id=3 '.$usl_data.' and member_id in ('.implode(',',array_keys($this->filter['check_id'])).') )' ;
        $usl[]='pkey in ('.implode(', ',$arr_ids).')' ;

     }
     if ($this->filter['only_my_checked'])
     {  //$usl[]='check_account='.MEMBER()->id ;
        $now=getdate(time()) ;
        $from=mktime($now['hours'],$now['minutes'],$now['seconds'],$now['mon'],$now['mday']-$this->filter['check_day'],$now['year']);
        $usl_data=($this->filter['check_day'])? 'and c_data>'.$from:'' ;
        $sql='select distinct(client_id) from log_site_events where event_id=3 '.$usl_data.' and member_id='.MEMBER()->id ;
        $arr_ids=execSQL_line($sql,0) ;
        $usl[]='pkey in ('.implode(', ',$arr_ids).')' ;
     }



     $usl_res=implode(' and ',$usl) ;
     //echo $usl_res ;
     return($usl_res) ;
  }

 function panel_info_filter()
 { $_str=array() ; $info=array() ; $title='' ;
   if ($this->filter['name'])                            $info['ФИО']='"'.$this->filter['name'].'"' ;
   if ($this->filter['doc_number'])                      $info['номер документа']='"'.$this->filter['doc_number'].'"' ;
   if (sizeof($this->filter['member_type']))
   {   $arr=array() ; foreach($this->filter['member_type'] as $type=>$enabled) $arr[]=$_SESSION['IL_cat_clients'][$type]['obj_name'] ;
       $info['статус']='"'.implode(', ',$arr).'"' ;
   }
   if (sizeof($this->filter['creator_id']))
   {   $arr=array() ; foreach($this->filter['creator_id'] as $creator_id=>$enabled) $arr[]=$this->list_accounts[$creator_id] ;
       $info['инициатор']='"'.implode(', ',$arr).'"' ;
   }
   if ($this->filter['only_my_users'])                   $info['мои клиенты']='Да' ;
   if ($this->filter['from_time'])                       $info['Отсутствовал на ППС']='с '.$this->filter['from_time'].'' ;
   if ($this->filter['ID_fon'])                          $info['ID ФОН']='"'.$this->filter['ID_fon'].'"' ;
   if ($this->filter['ID_win'])                          $info['ID Винлайн']='"'.$this->filter['ID_win'].'"' ;

   if ($this->filter['check_day'])
          {  $now=getdate(time()) ;
             $from=mktime($now['hours'],$now['minutes'],$now['seconds'],$now['mon'],$now['mday']-$this->filter['check_day'],$now['year']);
             $usl[]='check_data>0 and check_data>'.$from ;
             $info['Посетили с']=date('d.m.Y',$from).' ('.$this->filter['check_day'].' дней)' ;
          }

   if ($this->filter['check_id'])
   {  $account_names=execSQL_line('select obj_name from obj_site_account where pkey in ('.implode(',',array_keys($this->filter['check_id'])).') ') ;
      $info['Инициатор']=implode(', ',$account_names);
   }
   if ($this->filter['only_my_checked']) $info['Посетивших на моем ППС']='Да';

   if (sizeof($info)) foreach($info as $title=>$value) $_str[]=$title.': <strong>'.$value.'</strong>' ;
   if (sizeof($_str)) $title='<p class=center>'.implode(' ',$_str).'</p>' ;
   return $title ;
 }


  function get_cnt_items($usl,$options=array())
  { //$options['debug']=2 ;
    $cnt=execSQL_value('select count(pkey) from  '.PERSONAL()->view_personal.' where '.$usl,$options) ;
    return($cnt) ;
  }

  function get_items($usl,$options=array())
  { //$options['debug']=1 ;
    $limit=($options['limit'])? ' '.$options['limit']:'' ;
    $order=($this->filter['order'])? ' order by '.$this->filter['order']:' order by c_data desc' ;
    $list_rec=execSQL('select * from  '.PERSONAL()->view_personal.' where '.$usl.$order.$limit,$options) ;
    $list_fav=execSQL_line('select parent from obj_site_members_out_201 where account_id='.MEMBER()->id.' and parent>0') ;
    if (sizeof($list_fav)) foreach($list_fav as $id) if (isset($list_rec[$id])) $list_rec[$id]['favorite']=1  ;
    $options['no_get_all_img']=1 ;
    if (sizeof($list_rec)) foreach($list_rec as $id=>$rec) PERSONAL()->prepare_public_info_to_personal($list_rec[$id],$options) ;
    return($list_rec) ;
  }


// шаблон - вывод ссылками без разделитея
  function print_template_HTML($list_recs,$options=array())
  { global $member ;
    $id=($options['id'])? 'id='.$options['id']:'id=report_clients' ;
    $this->panel_info_select($options) ;
    ?><table class="basic fz_small full auto " <?echo $id?>>
        <tr><th>ID</th><th><i class="fa fa-flag"></i></th><th>Фото</th><th>ФИО</th><th>Последнее посещение</th><th>Операции</th></tr><? ;
        if (sizeof($list_recs)) foreach($list_recs as $rec)
        { ?><tr class="item rec_<?echo $rec['pkey']?>">
                <td class="id"><?echo $rec['pkey']?></td>
                <td class="сenter"><span id=fav_<?echo $rec['pkey']?> class="favorite <?if ($rec['favorite']) echo 'active'?> v2" cmd="personal/change_vaforite" client_id="<? echo $rec['pkey']?>"><i class="fa fa-flag"></i></span></td>
                <td class="center"><a  href="<?echo $rec['__href']?>"><img src="<?echo img_clone($rec,'small')?>"></a></td>
                <td class="left"><a href="<?echo $rec['__href']?>"><?echo $rec['obj_name']?></a>
                    <?if ($rec['typ']>1) echo '<div class="client_type"><span class="a_'.$rec['typ'].'">'.$_SESSION['IL_cat_clients'][$rec['typ']]['obj_name'].'</span></div>'?>
                    <div class="doc_info"><?echo $_SESSION['IL_docs'][$rec['doc']]['obj_name'].' <strong>'.$rec['pass1'].' '.$rec['pass2'].'</strong>'?></div>

                </td>
                <td><div id="check_info_<?echo $rec['pkey']?>">
                        <?if ($rec['check_data']) echo date('d.m.Y H:i',$rec['check_data']).'<br>'.ACCOUNTS()->get_member_name_by_id($rec['check_account'])?></div><br>
                        <button class="v2" cmd="personal/set_member" member_id="<?echo $rec['pkey']?>">Посетил</button>
                        <br><br><a href="" class="v2" cmd="personal/get_last_pos" client_id="<?echo $rec['pkey']?>">Последние посещения объектов</a>
                </td>
                <td class="center"><button class="button_small edit v1" href="<?echo $rec['__href']?>" title="Перейти на страницу редактирования клиента">Редактировать</button>
                                                 <?if ($member->info['status']){?><button class="button_small delete v2" cmd=personal/delete_member member_id="<?echo $rec['pkey']?>" title="Удалить клиента из базы">Удалить</button><?}?>
                                                 <button class="button_small copy" data-clipboard-text="<?echo 'http://'._MAIN_DOMAIN.$rec['__href']?>"  title="Скопировать в буфер обмена адрес страницы клиента">Скопировать URL</button>
                </td>
            </tr><?
        }
      ?></table>
        <style type="text/css">
            table#report_clients{}
            table#report_clients div.client_type{}
            table#report_clients div.client_type{padding:5px 0;}
            table#report_clients div.client_type span.a_10{background:red;color:white;padding:5px;}
            table#report_clients div.doc_info{}
        </style>

      <?
      $this->panel_info_select($options) ;
  }


    function print_template_XLS($list_recs,$options)
    {  $this->fields=array( 'i'=>array('title'=>'№','width'=>4,'align'=>'left'),
                            'name1'=>array('title'=>'Фамилия','width'=>15,'align'=>'left'),
                            'name2'=>array('title'=>'Имя','width'=>15,'align'=>'left'),
                            'name3'=>array('title'=>'Отчество','width'=>15,'align'=>'left'),
                            'seria'=>array('title'=>'Серия','width'=>7,'align'=>'left'),
                            'number'=>array('title'=>'Номер','width'=>7,'align'=>'left'),
                            //'id_m'=>array('title'=>'ID Мелбет','width'=>10,'align'=>'left'),
                            'id_f'=>array('title'=>'ID ФОН','width'=>10,'align'=>'left'),
                            'id_v'=>array('title'=>'ID Винлайн','width'=>10,'align'=>'left'),
                            'phone'=>array('title'=>'Телефон','width'=>15,'align'=>'left'),
                            'status'=>array('title'=>'Статус','width'=>10,'align'=>'center'),
                            'comment'=>array('title'=>'Примечание','width'=>20,'align'=>'left'),
                            'autor'=>array('title'=>'Инициатор добавления клиента','width'=>25,'align'=>'left'),
                            'last_edit'=>array('title'=>'Дата последнего редактрирования','width'=>25,'align'=>'center'),
                            'last_autor'=>array('title'=>'Инициатор последнего редактрирования','width'=>30,'align'=>'left'),
                            'last_fixed'=>array('title'=>'Дата последнего посещения','width'=>25,'align'=>'center'),
                            'autor_fixed'=>array('title'=>'Инициатор последнего нажатия "Посетил"','width'=>30,'align'=>'center')
                           ) ;

      // шапка таблицы
      $this->XLS_print_table_header() ;
      $list_accounts=execSQL_row('select pkey,obj_name from obj_site_account') ;
      // данные таблицы
      $i=1 ;
      if (sizeof($list_recs)) foreach($list_recs as $rec)
        { $cur_col_char=$this->first_col_char ;
          if (sizeof($this->fields)) foreach($this->fields as $fname=>$options)
          {  $value='' ;
             switch($fname)
              { case 'i':           $value=$i ;  break ;
                case 'name1':       $value=$rec['name1'] ;  break ;
                case 'name2':       $value=$rec['name2'] ;  break ;
                case 'name3':       $value=$rec['name3'] ;  break ;
                case 'seria':       $value=$rec['pass1'] ;  break ;
                case 'number':      $value=$rec['pass2'] ;  break ;
                //case 'id_m':        $value=$rec['ID_melbet'] ;  break ;
                case 'id_f':        $value=$rec['ID_fon'] ;  break ;
                case 'id_v':        $value=$rec['ID_win'] ;  break ;
                case 'phone':       $value=$rec['phone'] ;  break ;
                case 'status':      $value=$_SESSION['IL_cat_clients'][$rec['typ']]['obj_name'] ;  break ;
                case 'comment':     $value='' ;  break ;
                case 'autor':       $value=$list_accounts[$rec['creator_id']] ;  break ;
                case 'last_edit':   $value=date('d.m.Y H:i',$rec['r_data']) ;  break ;
                case 'last_autor':  $value='' ; ;  break ;
                case 'last_fixed':  $value=($rec['check_data'])? date('d.m.Y H:i',$rec['check_data']):'' ;  break ;
                case 'autor_fixed':  $value=($rec['check_account'])? $list_accounts[$rec['check_account']]:'' ;  break ;

              }

            $col_char=iconv('windows-1251','UTF-8',chr($cur_col_char)) ;
            //$this->aSheet->setCellValueByColumnAndRow($col_char,$this->cur_row,$value);
            $this->aSheet->setCellValue($col_char.$this->cur_row,$value);
            if ($options['align']=='center') $this->aSheet->getStyle($col_char.$this->cur_row)->applyFromArray($this->style['center']);
            if ($options['align']=='left')   $this->aSheet->getStyle($col_char.$this->cur_row)->applyFromArray($this->style['left']);
            $cur_col_char++ ;

          }

          $this->cur_row++ ;
          $i++ ;
        }

    }

    function print_template_CSV($list_recs,$options=array())
    { $i=0 ;
      if (sizeof($list_recs)) foreach($list_recs as $rec)
        { $data=array() ;
          $data['Дата']=date('d.m.Y',$rec['c_data']) ;
          $data['ID сесcии МО']=$rec['pkey'] ;
          $data['Тип контроля']=$_SESSION['ARR_mo_type'][$rec['mo_type']] ;
          $data['Модель']=$_SESSION['ARR_model_osmotr'][$rec['model']] ;
          $data['ФИО сотрудника']=$rec['member_name'] ;
          $data['ДатаРожд']=$rec['member_dr'] ;
          $data['Возраст']=$rec['age'] ;
          $data['Пол']=($rec['member_pol'])? 'Женский':'Мужской' ;
          $data['Пропуск']='"'.$rec['propusk'].'"';
          $data['Терминал/Врач']=($rec['hand_check_user_id'])? $this->recs_doctor[$rec['hand_check_user_id']]['obj_name']:$rec['terminal_id']  ;
          $data['Давление']=($rec['tonometer'])? (($rec['systolic'])? $rec['systolic']:'-').' / '.(($rec['diastolic'])? $rec['diastolic']:'-'):''  ;
          $data['Пульс']=($rec['pulsometer'])? (($rec['pulse'])? $rec['pulse']:'-'):'';  ;
          $data['Температура']=($rec['thermometer'])? (($rec['temperature']>0)? $rec['temperature']:'-'):'';  ;
          $data['Алкоголь']=($rec['alko']>0)? 'Есть'.(($rec['alko_value']>0)? ' ('.$rec['alko_value'].')':''):($rec['alcotester']? 'Нет':'-')  ;
          $data['Жалобы']=($rec['status']>=2 and $rec['status']<=3)? ($rec['question'])? 'Есть':'Нет':'-'   ;
                          if ($rec['question_comment']) $data['Жалобы'].=' '.$rec['question_comment'] ;
          $data['Результат осмотра на терминале']=($rec['_comment'])? strip_tags($rec['_comment']):'-'   ;
          $data['ПЗиПР']=(($rec['simptoms'])? 'Есть':'').$_SESSION['IL_diagnoz'][$rec['simptoms']]['obj_name'] ;
          $data['Комментарий врача']=($rec['doctor_comment'])? $rec['doctor_comment']:'' ;
          $data['Результат осмотра']=strip_tags($rec['_status_name']);
          $data['Допуск']=strip_tags($rec['_dopusk_name']);


          if (sizeof($data)) foreach($data as $fname=>$value) $data[$fname]=iconv('UTF-8','windows-1251',$value) ;
          // для первой строки выводим заголовки
          if (!$i)
          {   $arr_title=array_keys($data) ;
              foreach($arr_title as $i=>$title) $arr_title[$i]=iconv('UTF-8','windows-1251',$title) ;
              fputcsv($this->file_csv,$arr_title,';');
          }
          // вывод данных
          fputcsv($this->file_csv,$data,';');
          $i++ ;
        }

    }



}

?>