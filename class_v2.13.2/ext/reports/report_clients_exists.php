<?php
include_once('report_clients.php') ;
class report_clients_exists extends report_clients
{
 var $title='Список клиентов - посетившие' ;
 var $view_by_space_filter=1 ;
 var $list_accounts=array() ;
 var $report_name='report_clients_exists' ;

 function _setting($options=array())
 {   
     // Нужно чтобы в фильтрах "Кем добавлен", "Кого посетил" отображал только пользователей с ролью "Обычный персонал", от остальных смысла нет, они не регистрируют и их не посещают.
     $this->list_accounts=execSQL_row('select pkey,obj_name from obj_site_account where clss=86 and enabled=1 and rol=2  order by obj_name') ;
     ?><div id="panel_filter" class="">
             <form id="sky-form" class="sky-form"><input type="hidden" name="filter_dir" value="<?echo ($options['filter_dir'])? $options['filter_dir']:_CUR_REPORT_DIR?>">

                  <fieldset>
                      <div class="row">
                           <section class="col col-2">
                               <label class="label">Фамилия Имя Отчество</label>
                               <label class="input">
                                    <input type="text" name="filter[name]" placeholder="" value="<?echo htmlspecialchars($this->filter['name'])?>">
                               </label>
                           </section>
                          <section class="col col-2">
                              <label class="label">Номер документа</label>
                              <label class="input">
                                  <input type="text" name="filter[doc_number]" placeholder="" value="<?echo htmlspecialchars($this->filter['doc_number'])?>">
                              </label>
                          </section>
                          <section class="col col-3">
                              <label class="label">Статус</label>
                              <label class="select">
                                  <select name="filter[member_type][]" class="js-basic-multiple" multiple="multiple">
                                      <option></option>
                                      <?if (sizeof($_SESSION['IL_cat_clients'])) foreach($_SESSION['IL_cat_clients'] as $rec)
                                      {?><option value="<?echo $rec['id']?>" <?if (isset($this->filter['member_type'][$rec['id']])) echo 'selected'?>><?echo $rec['obj_name']?></option><?}?>
                                  </select>
                              </label>
                          </section>
                          <section class="col col-3">
                              <label class="label">Кем добавлен</label>
                              <label class="select">
                                  <select name="filter[creator_id][]" class="js-basic-multiple js-states form-control" multiple="multiple">
                                      <option value="0"></option>
                                      <?if (sizeof($this->list_accounts)) foreach($this->list_accounts as $id=>$name)
                                      {?><option value="<?echo $id?>" <?if (isset($this->filter['creator_id'][$id])) echo 'selected'?>><?echo $name?></option><?}?>
                                  </select>
                              </label>
                          </section>
                          <section class="col col-2">
                              <label class="label">Отсутствовал на ППС</label>
                              <label class="input">
                                  <input name="filter[from_time]" type=text class="datepicker" value="<?echo $this->filter['from_time']?>">
                                  <!--<select name="filter[from_time]">
                                      <option value="0"></option>
                                      <option value="10" <?if ($this->filter['from_time']==10) echo 'selected'?>>Более 10 дней</option>
                                      <option value="20" <?if ($this->filter['from_time']==20) echo 'selected'?>>Более 20 дней</option>
                                      <option value="30" <?if ($this->filter['from_time']==30) echo 'selected'?>>Более 30 дней</option>
                                      <option value="50" <?if ($this->filter['from_time']==50) echo 'selected'?>>Более 50 дней</option>
                                      <option value="100" <?if ($this->filter['from_time']==100) echo 'selected'?>>Более 100 дней</option>
                                  </select>-->
                              </label>
                          </section>
                      </div>
                      <div class="row">
                            <section class="col col-4">
                              <label class="label">Показывать только</label>
                              <input type="checkbox" name="filter[only_my_users]" value="1" <?if ($this->filter['only_my_users']) echo 'checked'?>>&nbsp;&nbsp;Зарегистрированные на Вашем ППС<br>
                              <input type="checkbox" name="filter[only_my_checked]" value="1" <?if ($this->filter['only_my_checked']) echo 'checked'?>>&nbsp;&nbsp;Посетивших на Вашем ППС
                           </section>
                            <section class="col col-3">
                              <label class="label">Посетившие за</label>
                              <input type="text" name="filter[check_day]" value="<?echo $this->filter['check_day']?>"> дней
                           </section>
                          <section class="col col-3">
                          <label class="label">Кого посетил</label>
                          <label class="select">
                                <select name="filter[check_id][]" class="js-basic-multiple js-states form-control" multiple="multiple">
                                    <option value="0"></option>
                                    <?if (sizeof($this->list_accounts)) foreach($this->list_accounts as $id=>$name)
                                    {?><option value="<?echo $id?>" <?if (isset($this->filter['check_id'][$id])) echo 'selected'?>><?echo $name?></option><?}?>
                                </select>
                            </label>
                        </section>
                          <?if (MEMBER()->rol==1 or MEMBER()->rol==3){?>
                          <section class="col col-2">
                            <label class="label">Сохранить как</label>
                            <label class="select">
                                <select name="filter[view]" class="select2">
                                    <option></option>
                                    <!--<option value="PDF" <? if ($this->filter['view']=='PDF') echo 'selected'?>>PDF</option>-->
                                    <option value="xls" <? if ($this->filter['view']=='XLS') echo 'selected'?>>Excel XLS</option>
                                    <!--<option value="csv" <? if ($this->filter['view']=='CSV') echo 'selected'?>>Excel CSV</option>-->
                                </select>
                            </label>
                           </section>
                          <?}?>
                        </div>
                  </fieldset>
                  <div id="panel_buttons"><input type="submit" class="button v2" cmd="set_filter" value="OK"></div>
              </form>
       </div><br>
     <script type="text/javascript">
         window.onload = function () {
             $j(".js-basic-multiple").select2({
               //placeholder: "Select a state"
             });
             }  ;
     </script>

     <?
  }

 function create_usl_select_obj($_params,&$options)
 {  $usl_res=parent::create_usl_select_obj($_params,$options) ;
    $usl_res.=' and check_data>0' ;
    $this->filter['order']='check_data desc' ;
    return($usl_res) ;
 }


}

?>