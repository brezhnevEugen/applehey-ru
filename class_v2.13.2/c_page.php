<?
include_once (_DIR_EXT.'/HTML/c_HTML.php') ;
if (class_exists('c_page')) return ;
class c_page extends c_HTML
{ public $no_panel_smena_is_out=0 ;
  public $menu=array() ;
  public $second_menu=array() ;
  public $menu_show_account_item=1 ;
  public $menu_show_close_item=0 ;
  public $body_type='' ;
  public $page_type='' ; // = window для страниц, которые будут показываться в отдельном окне
  public $result_exec_cmd='' ;
  public $free_access=0 ;
  public $allow_access_to_rol=0 ;
  public $allow_access_to_member=0 ;
  public $access_denited=0 ;
  public $access_denited_comment='' ;
  public $allow_menu_items='' ;
  public $menu_url_params='' ;
  public $use_datepicker=0 ;
  public $use_fusioncharts=0 ;
  public $no_main_menu=0 ;
  public $filter=array() ;
  public $rec_arm=array() ;
  public $cur_zone_id=0 ;

 public $active_tab ;

 function set_head_tags()
 { //if (sizeof($_COOKIE)) foreach($_COOKIE as $name=>$value) if (strpos($name,'SSE-')!==false) setcookie ($name, "");
   // формируем уникальный UID рабочего места, если он еще не задан
   //if (!isset($_COOKIE['arm_uid']))
    //setcookie ('ARM_UID',md5(uniqid(rand(),true),0x7FFFFFFF));
   // используем кукис siteber-medic2localuralkalicom

   //$this->HEAD['favicon']='/favicon.png'  ;
   $this->HEAD['meta'][]=array('name'=>"viewport",'content'=>"width=device-width, initial-scale=0.9, minimum-scale=0.9, user-scalable=no")  ;

   // стандартный JQuery
   $this->HEAD['js'][]=_PATH_TO_EXT.'/jquery/jquery-1.8.3.min.js' ;
   //$this->HEAD['js'][]=_PATH_TO_EXT.'/jquery/jquery-1.11.0.min.js' ;
   $this->HEAD['js'][]=_PATH_TO_EXT.'/jquery/jquery-ui-1.10.4.min.js' ;
   $this->HEAD['js'][]=_PATH_TO_EXT.'/jquery/jquery.form.min.js' ;
   $this->HEAD['js'][]=_PATH_TO_EXT.'/jquery/jquery.validate-1.11.0.min.js' ;
   $this->HEAD['js'][]=_PATH_TO_EXT.'/jquery/jquery.maskedinput-1.4.1.min.js' ; // http://digitalbush.com/projects/masked-input-plugin/
   $this->HEAD['js'][]=_PATH_TO_EXT.'/jquery/jquery.modal.js' ;
   $this->HEAD['js'][]=_PATH_TO_EXT.'/jquery/jquery.timers-1.2.js' ;
   $this->HEAD['js'][]=_PATH_TO_EXT.'/jquery/unserialize.jquery.latest.js' ;
   $this->HEAD['js'][]=_PATH_TO_EXT.'/jquery/imgpreview.full.jquery.js' ; // http://james.padolsey.com/demos/imgPreview/full/

   //$this->HEAD['js'][]=_PATH_TO_EXT.'/jquery.inputmask-3.x/js/jquery.inputmask.js' ; // https://github.com/RobinHerbots/jquery.inputmask

   // mootools
    $this->HEAD['js'][]='http://ajax.googleapis.com/ajax/libs/mootools/1.4.5/mootools-yui-compressed.js' ;
    $this->HEAD['js'][]=_PATH_TO_EXT.'/mootools/mootools-more-1.3.1.1.js' ;

   // библитека jBox + замена mBox
   $this->HEAD['js'][]=_PATH_TO_EXT.'/mBox/jBox-0.3.1/Source/jBox.min.js' ;
   $this->HEAD['css'][]=_PATH_TO_EXT.'/mBox/jBox-0.3.1/Source/jBox.css' ;

   $this->HEAD['js'][]=_PATH_TO_EXT.'/mBox/StephanWagner-mForm-0.2.6/mForm.Core.js' ;
   $this->HEAD['js'][]=_PATH_TO_EXT.'/mBox/StephanWagner-mForm-0.2.6/mForm.Element.js' ;
   //$this->HEAD['js'][]=_PATH_TO_EXT.'/mBox/StephanWagner-mForm-0.2.6/mForm.Element.Select.js' ;
   $this->HEAD['js'][]=_PATH_TO_EXT.'/mBox/StephanWagner-mForm-0.2.6/mForm.Submit.js' ;
   if (MEMBER()->id) $this->HEAD['css'][]='/'._CLASS_.'/assets/mForm.css' ;
   //$this->HEAD['css'][]=_PATH_TO_EXT.'/mBox/StephanWagner-mForm-0.2.6/assets/mFormElement-Select.css' ;
   $this->HEAD['js'][]=_PATH_TO_EXT.'/mBox/modal_window.jBox.js' ;

   $this->HEAD['css'][]=_PATH_TO_ADDONS.'/font-awesome-4.6.3/css/font-awesome.css' ;

   $this->HEAD['js'][]=_PATH_TO_ADDONS.'/printPage/jquery.printPage.js' ;
   $this->HEAD['js'][]=_PATH_TO_EXT.'/jQuery-autoComplete/jquery.auto-complete.js' ;
   $this->HEAD['css'][]=_PATH_TO_EXT.'/jQuery-autoComplete/jquery.auto-complete.css' ;

    // $this->HEAD['js'][]=_PATH_TO_EXT.'/webcamera_capture/jquery.webcamera_capture.js' ;

   // стандартное подключение highslide
   $this->HEAD['js'][]=_PATH_TO_ADDONS.'/highslide/highslide-4.1.13/highslide.js' ;
   $this->HEAD['css'][]=_PATH_TO_ADDONS.'/highslide/highslide-4.1.13/highslide.css' ;

   //select2
   $this->HEAD['css'][]=_PATH_TO_ADDONS.'/select2_v4.0.1/dist/css/select2.css' ;
   $this->HEAD['js'][]=_PATH_TO_ADDONS.'/select2_v4.0.1/dist/js/select2.js' ;



   if ($this->use_datepicker)
   { $this->HEAD['js'][]=_PATH_TO_EXT.'/jquery-ui-1.9.2/jquery.ui.core.js' ;
     $this->HEAD['js'][]=_PATH_TO_EXT.'/jquery-ui-1.9.2/jquery.ui.datepicker.min.js' ;
     $this->HEAD['js'][]=_PATH_TO_EXT.'/jquery-ui-1.9.2/jquery.ui.datepicker-ru.js' ;
     $this->HEAD['js'][]=_PATH_TO_EXT.'/jquery-ui-1.9.2/jquery-ui-timepicker-addon.js' ;
     $this->HEAD['css'][]=_PATH_TO_EXT.'/jquery-ui-1.9.2/jquery-ui.css' ;
     $this->HEAD['css'][]=_PATH_TO_EXT.'/jquery-ui-1.9.2/jquery.ui.datepicker.css' ;
     $this->HEAD['css'][]=_PATH_TO_EXT.'/jquery-ui-1.9.2/jquery-ui-timepicker-addon.css' ;
   }

   // стандартное подключение ZeroClipboard
   $this->HEAD['js'][]=_PATH_TO_EXT.'/zeroclipboard-1.2.3/ZeroClipboard.js' ;


// sky-mega-menu + sky-forms-pro
   $this->HEAD['css'][]=_PATH_TO_ADDONS.'/sky-mega-menu/version_1.2.1/css/demo.css' ;
   $this->HEAD['css'][]=$this->HEAD['css'][]='/'._CLASS_.'/assets/sky-mega-menu.css' ;
   $this->HEAD['css'][]=$this->HEAD['css'][]='/'._CLASS_.'/assets/sky-forms.css' ;
   $this->HEAD['css'][]=_PATH_TO_ADDONS.'/sky-forms-pro/version_2.0.5/css/sky-forms-green.css' ;

   $this->HEAD['css'][]='/AddOns/open-sans/opensans_regular_cyrillic/stylesheet.css' ;
   $this->HEAD['css'][]='/AddOns/open-sans/opensans_light_cyrillic/stylesheet.css' ;
   $this->HEAD['css'][]='/AddOns/open-sans/opensans_bold_cyrillic/stylesheet.css' ;



   // ios7-switch-master
   //$this->HEAD['css'][]=_PATH_TO_EXT.'/interface/ios7-switch-master/ios7-switch.css' ;

   $this->HEAD['css'][]='/'._CLASS_.'/style_admin.css' ;
   $this->HEAD['css'][]='/'._CLASS_.'/style.css' ;
   //$this->HEAD['css'][]='/style.css' ;
   $this->HEAD['js'][]='/'._CLASS_.'/script_admin.js' ;
   $this->HEAD['js'][]='/'._CLASS_.'/script.js' ;

   ?><SCRIPT type=text/javascript>
             var _PATH_TO_EXT="<?echo _PATH_TO_EXT?>" ;
             var _PATH_TO_ADMIN_IMAGES="/<?echo _CLASS_?>/images/old_admin" ;
         </SCRIPT><?

   $this->allow_menu_items=execSQL_row('select obj_name,rol_id from obj_site_permit where rol_id="'.MEMBER()->info['rol'].'"') ;
   $this->menu_config() ;
   if (_CUR_FILTER) $this->filter=REPORTS()->parse_url(_CUR_FILTER,array()) ;
   ///damp_array($this->filter) ;

   if (function_exists('SKD')) $this->rec_arm=SKD()->get_ARM_by_IP(IP,array('check_reboot'=>1)) ;
   if ($this->rec_arm)
   { $this->cur_zone_id=$this->rec_arm['parent'] ;
   }


   $reboot=0 ;
   if (MEMBER()->cur_group_id) $reboot=execSQL_value('select reboot from '.ACCOUNTS()->table_account.' where pkey='.MEMBER()->id) ;
   if ($reboot)
    { reboot_engine() ;
      ACCOUNTS()->update_member_info(MEMBER()) ;
      execSQL_update('update '.ACCOUNTS()->table_account.' set reboot=0 where pkey='.MEMBER()->id) ;
    }


 }

 function local_page_head()
 {
     parent::local_page_head(); // TODO: Change the autogenerated stub
     // переделать потом для поддержки IE
     ?><!--[if lt IE 9]>
         <link rel="stylesheet" href="<?echo _PATH_TO_ADDONS?>/sky-mega-menu/version_1.2.1/css/sky-mega-menu-ie8.css">
         <link rel="stylesheet" href="<?echo _PATH_TO_ADDONS?>/sky-forms-pro/version_2.0.5/css/sky-forms-ie8.css">
         <script src="<?echo _PATH_TO_EXT?>/jquery/html5.js"></script>
         <script src="<?echo _PATH_TO_ADDONS?>/sky-forms-pro/version_2.0.5/js/sky-forms-ie8.js"></script>
         <![endif]-->
         <!--[if lt IE 10]>
            <script src="<?echo _PATH_TO_EXT?>/jquery/jquery.placeholder-0.2.4.min.js"></script>
        <![endif]--><?
 }

  // -----------------------------------------------------------------------------------
  // РАЗЛИЧНЫЕ ВАРИАНТЫ BODY
  // -----------------------------------------------------------------------------------

  function body()
  { if (!$_SESSION['member']->id and !$this->free_access) $this->body_autorize() ;
    else  if ($this->result=='404')                       $this->body_404() ;
    else  if ($this->body_type=='print')                  $this->body_print() ;
    else                                                  $this->body_work() ;
  }

  function body_autorize()
  {
    ?><body class="x1 bg-cyan login_panel"><table id=wrapper class="login_panel"><tr><td><? $this->panel_autorize();?></td></tr></body><?
  }

  function body_404()
  {
    ?><body class="bg-cyan body_work body_404 <?echo $this->body_class?>"><div class="body"><aside>
           <?$this->panel_main_menu()?>
           <?$this->panel_second_menu()?>
           <div id=block_main><?  $this->panel_404();?><div class=clear></div>
           </div>
          </aside></div></body>
    <?
  }

  function body_work()
  { ?><body class="bg-cyan body_work <?echo $this->body_class?>"><div class="body"><aside>
     <?$this->panel_main_menu()?>
     <?$this->panel_second_menu()?>
     <table id="block_center"><tr><td id="center_left"><?$this->panel_block_main()?></td><td id="center_right"><?$this->panel_block_right()?></td></tr></table>
    </aside></div></body>
    <script type="text/javascript">
      /*$j(document).ready(function(){ $j('body').everyTime(30000,'check_autorize',function(){$j.ajax({url:'/ajax.php',type:'POST',dataType:'xml',success:exec_ajax_cmd_success_JQ,data:{cmd:'aspmo/check_autorize2'}});}) ;});*/
    </script>
    <?
    //global $member ;
    //if ($member->id) execSQL_update('update obj_site_account set sess_id="'.session_id().'",IP="'.IP.'" where pkey='.$member->id) ;
  }

  function body_print()
  { ?><body class="<?echo $this->body_class?>">
      <div id=block_main><div class=printSelection><?  if (!$this->check_access()) $this->panel_access_denited() ;
                             else $this->block_main();?></div></div>
      </body>
    <?
  }

  // -----------------------------------------------------------------------------------
  // РАЗЛИЧНЫЕ ВАРИАНТЫ BODY
  // -----------------------------------------------------------------------------------

  function panel_block_main()
  { ?><div id=block_main><?
       if (!$this->check_access() or $this->access_denited) $this->panel_access_denited() ;
       elseif (MEMBER()->_smena_status=='out' and !$this->no_panel_smena_is_out) $this->panel_smena_is_out() ;
       else $this->block_main();
    ?><div class=clear></div></div><?
  }

  function panel_block_right() {} // используется в отчетах для вывода правого меню
  function block_right() {}

  function panel_autorize()
  { /*if ($_SESSION['LS_auth_by_token'])
    { ?><SCRIPT type="text/javascript" src="/class/ext/ecp/script.js"></SCRIPT>
        <!--<object type="application/x-rutoken-pki" id="plugin-object" width="0" height="0"><param name="onload" value="onPluginLoaded"/></object>-->
      <?
    } */
    // в $_COOKIE['autorize_mode']  слхраняется текущий тип авторизации
    if (!$_COOKIE['autorize_mode']) { include_once(_DIR_EXT.'/account/panel_form_autorize.php') ; panel_form_autorize() ; }
    else                            { include_once(_DIR_EXT.'/'.$_COOKIE['autorize_mode'].'/panel_form_autorize.php') ; panel_form_autorize() ; }
  }

  function panel_path($path=array(),$options=array()){}

  function page_title($title='',$path=array(),$options=array())
  { parent::page_title($title,$path,$options) ;
    if ($this->cmd_exec_HTML_output)
    {   ?><div id="result_exec_cmd"><?
        if (sizeof($this->cmd_exec_result)) $this->panel_exec_result() ;
        echo $this->cmd_exec_HTML_output ;
        ?></div><?
    }
    //damp_array($this) ;
  }

    function panel_exec_result()
     { ?><div id="panel_exec_result" class="<?echo $this->cmd_exec_result['type']?>"><?
         if ($this->cmd_exec_result['text']) echo $this->cmd_exec_result['text'] ;
         else if ($this->cmd_exec_result['code']) echo $this->cmd_exec_result['code'] ;
       //damp_array($this->cmd_exec_result,1,-1) ;
       ?></div><?
     }

     function exec_cmd($post) {}


   function cmd_exec()
   {
     if (!$this->access_denited) parent::cmd_exec() ;   // выполнение CMD только если select_obj_info не установил флаг access_denited
   }

  // -----------------------------------------------------------------------------------
  // шапка сайта
  // -----------------------------------------------------------------------------------

  function panel_top_time()
  {  include_once(_DIR_EXT.'/server_time/panel_cur_time.php') ;
     panel_cur_time('d.m.Y H:i',60) ;
  }

  function panel_terminal_status()  {}


  function panel_button_back()
  { ?><div id=panel_button_back><?
       if ($_SERVER['HTTP_REFERER']) {?><button class="v1" href="<?echo $_SERVER['HTTP_REFERER']?>">Назад</button><?}
    ?></div><?
  }

  // -----------------------------------------------------------------------------------
  // панель вкладок меню
  // -----------------------------------------------------------------------------------

 function check_access()
 { if (MEMBER()->info['rol']==1) return(1) ;
   if ($this->allow_access_to_member and $this->allow_access_to_member==MEMBER()->id) return(1) ; // разрешаем доступ есть указана  роль для страницы
   if ($this->allow_access_to_rol and $this->allow_access_to_rol==MEMBER()->rol) return(1) ; // разрешаем доступ есть указана  роль для страницы
   //$access=ACCOUNTS()->get_right_rol(MEMBER()->info['rol'],get_class($this)) ;
   $access=isset($this->allow_menu_items[get_class($this)]) ;
   return($access) ;
 }

 function panel_access_denited()
 { ?><div id="panel_access_denited">
     <?if ($this->access_denited_comment) echo '<h1>'.$this->access_denited_comment.'</h1>' ;
       else {?><h1>У вас нет прав доступа к данной странице</h1><?}
     ?>
      <img src="/<?echo _CLASS_?>/images/access_denited.png">
    </div>
    <div class="small"><?echo 'script_file: '.hide_server_dir($this->script_file).'<br>' ;
                         echo 'class_name: '.$this->class_name.'<br>' ;
                         //damp_array($this) ;
                       ?>
    </div>
    <?
 }

 function panel_smena_is_out($options=array())
 { ?><div id="panel_access_denited">
      <h1>Ваша смена окончена</h1>
      <?if (!$options['no_img']){?><img src="/<?echo _CLASS_?>/images/access_denited.png"><br><br><?}?>
      <button class="v2" cmd="account/account_logout">Выход</button>
    </div><?
 }


 // -----------------------------------------------------------------------------------
 // УДАЛЯЕМ ИЗ МЕНЮ ПУНКТЫ, ДОСТУП К ТОКОТЫМ ЗАПРЕЩЕ ДЛЯ ПОЛЬЗОВАТЕЛЯ
 // -----------------------------------------------------------------------------------

 function member_menu_check(&$menu)
 {  $debug=0 ;
    if ($debug)  damp_array($menu,1,-1) ;
    if ($debug)  damp_array($this->allow_menu_items,1,-1) ;
    if (sizeof($menu)) foreach($menu as $url=>$info) if (!isset($info['no_delete']))
    {  $arr=explode('/',$url) ;  // print_r($arr) ; echo '<br>' ;
       // надо удалить из URL все числа - ID объектов
       unset($arr[0],$arr[sizeof($arr)]) ;
       if (sizeof($arr)) foreach($arr as $i=>$value) if (is_numeric($value))  unset($arr[$i]) ;
       $page_class_name='c_page_'.implode('_',$arr) ;
       //echo 'arr[3]='.$arr[3].'<br>' ;
       //echo 'isset='.(isset($this->allow_menu_items[$arr[3]])? 1:0) ; echo '<br>' ;

       if (!(isset($this->allow_menu_items[$page_class_name]) or isset($this->allow_menu_items[$arr[3]]))) // второе правило - для отчетов
       { unset($menu[$url]) ;
         if ($debug) echo  'DELETE URL='.$url.' page='.$page_class_name.'<br>' ;
       }
       if (is_array($info) and sizeof($info['items'])) $this->member_menu_check($menu[$url]['items']) ;
       //damp_array($info['items']) ;
       //echo '$page_class_name='.$page_class_name.'<br>' ;

    }
 }

  // -----------------------------------------------------------------------------------
  // ГЛАВНОЕ МЕНЮ
  // -----------------------------------------------------------------------------------

 function menu_config()
 {   if (sizeof($_SESSION['list_created_sybsystems'])) foreach($_SESSION['list_created_sybsystems'] as $system_name)
      if (method_exists($_SESSION[$system_name],'declare_menu_items_1')) $_SESSION[$system_name]->declare_menu_items_1($this) ;

    if (sizeof($_SESSION['list_created_sybsystems'])) foreach($_SESSION['list_created_sybsystems'] as $system_name)
      if (method_exists($_SESSION[$system_name],'declare_menu_items_2')) $_SESSION[$system_name]->declare_menu_items_2($this) ;

    if (sizeof($_SESSION['list_created_sybsystems'])) foreach($_SESSION['list_created_sybsystems'] as $system_name)
      if (method_exists($_SESSION[$system_name],'declare_menu_items_3')) $_SESSION[$system_name]->declare_menu_items_3($this) ;

    if (sizeof($_SESSION['list_created_sybsystems'])) foreach($_SESSION['list_created_sybsystems'] as $system_name)
      if (method_exists($_SESSION[$system_name],'declare_menu_items_4')) $_SESSION[$system_name]->declare_menu_items_4($this) ;

    // damp_array($_SESSION['list_created_sybsystems']) ;


    //if (is_member_right('personal_import')) $this->tabs['/personal/upload/']='Импорт Excel' ;
    /*
    $this->menu['/cab/stats/']=array('title'=>'Статистика',
                                       'class'=>'fa fa-file-text-o',
                                       'items'=>array('/cab/stats/'=>array('title'=>'Статистика','class'=>'fa fa-users'),
                                                      //'/cab/reports/'=>'Создать отчет'
                                                     )
                                       ) ;

    */


  if (sizeof($_SESSION['list_created_sybsystems'])) foreach($_SESSION['list_created_sybsystems'] as $system_name)
     if (method_exists($_SESSION[$system_name],'declare_menu_items_5')) $_SESSION[$system_name]->declare_menu_items_5($this) ;





  //global $member ;
  //damp_array($member) ;
 }

 function local_menu_config() {}


 // cmd
 // id
 // class
 // title
 // items
 function panel_mega_menu_item($url,$item)
 {  if (!is_array($item))  $item=array('title'=>$item) ;
    if (is_int($url)) $url='' ;
    $arr_url=parse_url($url) ;
    ?><li aria-haspopup="<?echo (sizeof($item['items']))? 'true':'false'?>" <?if (_CUR_PAGE_DIR==$arr_url['path']) echo ' class=current'?>>
      	<a href="<?echo $url.$this->menu_url_params?>"
                <? if (sizeof($item)) foreach($item as $name=>$value) if ($name!='class' and $name!='title')
                { if ($name=='cmd') echo 'class=v2 cmd="'.$value.'"' ;
                  else              echo $name.'="'.$value.'"' ;
                }
                //if ($item['id']) echo 'id="'.$item['id'].'"' ;
                //if ($item['cmd']) echo 'class=v2 cmd="'.$item['cmd'].'"' ;
                ?>><i class="<?echo $item['class']?>"></i><?echo $item['title']?></a>
        <?if (sizeof($item['items'])){?><div class="grid-container4"><ul><?foreach($item['items'] as $url2=>$item2) $this->panel_mega_menu_item($url2,$item2);?></ul></div><?}?>
     </li>
   <?

 }

 function panel_main_menu()
 {
     if (MEMBER()->info['rol']!=1) $this->member_menu_check($this->menu) ; // удаляем из меню пункты, к которым нет доступа

     if ($this->page_type=='window' or _CUR_ROOT_DIR_NAME=='window')
     {   $this->menu_show_account_item=0 ;
         $this->menu_show_close_item=1 ;
     }
     /*?><ul  id="panel_main_menu" class="sky-mega-menu sky-mega-menu-pos-left sky-mega-menu-response-to-icons sky-mega-menu-anim-scale"><?*/
     ?><ul id="panel_main_menu" class="sky-mega-menu sky-mega-menu-pos-top  sky-mega-menu-fixed  sky-mega-menu-anim-scale"><?
            ?><li class="logo"><img src="<?echo _LOGO_CAB?>"  class="v2" cmd="reboot"></li><?
     // информация по текущему аккааунту
     $this->panel_main_menu_title() ;

     // текущее меню страницы
     if (sizeof($this->menu)) foreach($this->menu as $url=>$item)  $this->panel_mega_menu_item($url,$item) ;

     // кнопка "закрыть страницу"
     if ($this->menu_show_close_item){?><li><a href="" class="close_window"><i class="fa fa-home"></i>Закрыть</a></li><?}
     ?></ul><?
 }

 function panel_main_menu_title()
 {   global $member ;
        if ($this->menu_show_account_item){?>
            <li aria-haspopup="true">
    						<a><?echo '<strong>'.$member->name.'</strong><br>'.$member->working?>
                               <?if (MEMBER()->use_auch_as_smena) switch($member->_smena_status)
                                {  case 'close':     echo '<br><br><div class="red">Смена не открыта</div>' ; break ;
                                   case 'unactive':  echo '<br><br><div class="red">Смена не открыта</div>' ; break ;
                                   case 'active':    echo '<br><br>Начало смены: <strong>'.date('d.m.Y H:i',$member->info['smena_in']).'</strong>' ; break ;
                                   case 'pause':     echo '<br><br><div class="red">Смена приостановлена</div>' ; break ;
                                   case 'out':       echo '<br><br><div class="red">Смена окончена</div>' ; break ;
                                }
                                ?></a>
    						<div class="grid-container3">
    							<ul><?if (MEMBER()->rol==6){?><li><a href="/cab/client/setting/"><i class="fa fa-sliders"></i>Настройки аккаунта</a></li><?}
                                         else                  {?><li><a href="/cab/setting/account/<?echo MEMBER()->id?>/"><i class="fa fa-sliders"></i>Настройки аккаунта</a></li><?}
                                        ?>
                                    <? // для тех пользователей, которые работают по сменам
                                       if (MEMBER()->use_auch_as_smena)
                                        {   // номельная ситуация оконачания смены
                                            if ($member->_smena_status=='active') {?><li><a href="/cab/smenaout/"><i class="fa fa-sign-out"></i>Завершить смену</a></li><?}
                                            // приостановленая смена
                                            elseif ($member->_smena_status=='pause') {?><li><a href="" class="v2" cmd="account/smena_resume"><i class="fa fa-sign-out"></i>Продолжить смену</a></li>
                                                                                                                  <li><a href="/cab/smenaout/"><i class="fa fa-sign-out"></i>Завершить смену</a></li><?}
                                            // принудительное начало смены, когда идет смена другого сотрудника
                                            elseif ($member->_smena_status=='unactive') {?><li><a href="" class="v2" cmd="account/smena_in"><i class="fa fa-sign-out"></i>Начать смену</a></li><?}
                                        }
                                    ?>
    								<li><a href="/" class="v2" cmd="account/account_logout"><i class="fa fa-sign-out"></i>Выход</a></li>
 <? // переход из админа в аккаунт другого пользователя
                                    if (MEMBER()->rol==1)
                                    {  $ARR_members=execSQL('select * from obj_site_account where clss=86 and enabled=1 and rol>1 order by rol,obj_name') ;
                                       $arr_members_by_rol=group_by_field('rol',$ARR_members) ;
                                       ?><li><a href=""><i class=""></i>Перейти в аккаунт</a><div class="grid-container3"><ul><?
                                             foreach($arr_members_by_rol as $rol_id=>$rol_members)
                                                 if ($rol_id!=1){?><li><a href=""><i class=""></i><?echo $_SESSION['ARR_roll'][$rol_id]['obj_name']?></a><div class="grid-container3"><ul><?
                                                             foreach($rol_members as $rec) if ($rec['enabled'] and $rec['_enabled'])
                                                             { ?><li><a href="/" class="v2" cmd="account/account_login_as" member_id="<?echo $rec['pkey']?>"><i class=""></i><?echo $rec['obj_name']?></a></li><?}
                                                     ?></ul></div></li><?}



                                     ?></ul></div></li><?
                                    }
                                    elseif ($_SESSION['admin_login_id']) {?><li><a href="/" class="v2" cmd="account/account_login_as" member_id="<?echo $_SESSION['admin_login_id']?>"><i class="fa fa-sign-out"></i>Вернутся в аккаунт администратора</a></li><?}
                                    ?>    								
    								
    							</ul>
    						</div>
    		</li><?}

 }



  // -----------------------------------------------------------------------------------
  // ДОПОЛНИТЕЛЬНОЕ МЕНЮ
  // -----------------------------------------------------------------------------------

  function second_menu_config() {}

  function panel_second_menu()
  {   $this->second_menu_config() ;
      if (!sizeof($this->second_menu)) return ;
      if (MEMBER()->info['rol']!=1) $this->member_menu_check($this->second_menu) ; // удаляем из меню пункты, к которым нет доступа

      ?><ul id=panel_second_menu class="sky-mega-menu sky-mega-menu-response-to-icons sky-mega-menu-anim-scale"><?
      // текущее дополнительное меню страницы
      if (sizeof($this->second_menu)) foreach($this->second_menu as $url=>$item)  $this->panel_mega_menu_item($url,$item) ;
      ?></ul><div class=clear></div><?
  }

 /*
 function panel_account_info()
  { global $member ;
    if ($member->id){?><span id=panel_account_info2><?
                        echo $_SESSION['ARR_roll'][$member->info['rol']]['obj_name'].'<br>' ;
                        ?><span class=name><?echo $member->name?></span>
                        <? if ($member->info['smena_out']){?><span class="green">Отчет подписан, смена окончена.</span><?}?>
                        <?if (is_member_right('use_auch_as_smena') and !$member->info['smena_out']) {?><a href="/cab/smenaout/">Окончить смену</a><?}
                          else                                      {?><a href="" class="v2" cmd="accounts/account_logout">Выход</a><?}?>
                       </span><?}
  }*/

  /*
  function panel_work_group()
  {  global $member ;
     ?><div id="panel_work_group2"><?
       if ($member->cur_group_id) echo 'Рабочая группа:<br> <strong>'.$member->cur_group_name.'</strong>' ;
     ?></div><?
  } */


  function parse_url($url,$options=array())
   {  $params=array() ; $filter=array() ;
      $str=str_replace(array($options['base_url'],'/','[',']'),'',$url) ;
      $arr_params=explode(',',$str) ;
      if (sizeof($arr_params)) foreach($arr_params as $str_params)
      { $arr=explode('=',$str_params) ;
        if (sizeof($arr)==2) { $arr2=explode(';',$arr[1]) ;
                               if (sizeof($arr2)==1) $params[$arr[0]]=$arr[1] ;
                               else                  $params[$arr[0]]=$arr2 ;
                             }
        else                 $params[$str_params]=1 ;
      }
     if (sizeof($params)) foreach($params as $key=>$value) $filter[$key]=$value ;
     return($filter) ;
   }

// -----------------------------------------------------------------------------------
  // загрузчик файлов
  // -----------------------------------------------------------------------------------

  function panel_fancyupload($options=array())
  {
    ?><div id="panel_fancyupload" class="<?echo $options['class']?>">
       <?$this->panel_upload_alert() ;?>
              <form name="form" id="form-upload" method="post" enctype="multipart/form-data" action="/<?echo _CLASS_?>/fancyupload.php">
              <input type="hidden" name=active_tab value=0>
              <input type=hidden name=UID id=UID value="<?echo md5(uniqid(rand(),true))?>">
                     <fieldset id="upload-fallback">
                         <legend>Загрузка файлов</legend>
                         <p>Подождите, идет запуск загрузчика файлов... </p>
                         <label for="upload-photoupload">Загрузка файла:<input type="file" name="Filedata" /></label>
                     </fieldset>
                     <div id="upload-status" class="hide">
                         <div id=menu_panel>
                             <img src="<?echo _PATH_TO_EXT?>/fancyupload-v3.0.1/assets/select_file.png"> <a href="#" id="upload-browse">Добавить скан документов</a>
                             <br>
                         </div>
                         <div><strong class="overall-title"></strong><br /><img src="<?echo _PATH_TO_EXT?>/fancyupload-v3.0.1/assets/progress-bar/bar.gif" class="progress overall-progress" /></div>
                         <div><strong class="current-title"></strong><br /><img src="<?echo _PATH_TO_EXT?>/fancyupload-v3.0.1/assets/progress-bar/bar.gif" class="progress current-progress" /></div>
                         <div class="current-text"></div>
                     </div>
                     <ul id="upload-list"></ul>
              </form>
       </div>
    <?
  }

    function panel_upload_alert()
    { $max_upload = (int)(ini_get('upload_max_filesize')) ;
      $max_post = (int)(ini_get('post_max_size')) ;
      $memory_limit = (int)(ini_get('memory_limit'));
      $upload_mb = min($max_upload, $max_post, $memory_limit);
      echo 'Внимание! Максимальный размер файла, который Вы можете загрузить -<br><span class=green>'.$upload_mb.' Мб</span><div class=format_alert>Форматы изображений - <strong>JPG, PNG, BMP</strong></div><br><br>' ;
    }


}

?>