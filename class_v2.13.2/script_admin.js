$j(document).ready(function ()
{ // назначаем обработчики событий для панели вывода записей списком
  //$j('div.list_item input.fast_search').live('keyup',div_list_item_input_fast_search_keyup) ;
  //$j('table.list tr.td_header td').live('dblclick',table_list_tr_header_td_dblclick) ;
  $j('table.list tr.td_header td').live('click',table_list_tr_header_td_click) ;

  // отработка команд - кнопок и ссылок
  $j('a[action="new_window"]').live('click',onclick_to_new_window) ; // для <button cmd=.....>,<a cmd=....>,<div cmd=....>,<span cmd=....>

  // назначем обработчики для быстрого редактирования ячеек
  $j('td[el="gm"]').live('click',onclick_gm);
  $j('td[el="gi"]').live('click',onclick_gi);
  $j('td[el="gs"]').live('click',onclick_gs);
  $j('td[el="gc"]').live('click',onclick_gc);
  $j('td[el="gc2"]').live('click',onclick_gc2);
  $j('td[el="gmc"]').live('click',onclick_gmc);
  $j('td[el="gts"]').live('click',onclick_gts);
  $j('td.to_edit').live('click',onclick_to_edit);
  $j('div.to_edit').live('click',onclick_to_edit);
  $j('td.to_save').live('click',onclick_to_save);
  $j('table.list tr td input[type="text"]').live('keydown',function(e){if(e.keyCode==13) { $j(this).closest('tr').children('td.to_save').click() ; f_preventDefault(e); return false;}}) ; // поиск по нажатию на enter

  // клик по "отметить все/снять все"
  $j('table input[name="check_all"]').live('click',onclick_check_all) ;
  $j('table td input[name^="_check"]').live('click',onclick_td_check2) ;
  $j('table td:has(input[name^="_check"])').live('click',onclick_td_check) ;

  // показ скрытых панелей
  $j('div.expand_panel').live('click',function() {$j(this).next('div.panel').toggleClass('hidden') ;}) ;
  $j('a.show_hide_panel').live('click',function(){$j(this).parent().next().toggleClass('visible');return(false);});
  $j('div.show_hide_panel').live('click',function(){$j(this).next().toggleClass('visible');});

   new jBox('Confirm', {confirmButton: 'Да',cancelButton: 'Нет'});

});

/*********************************************************************************
 *
 * показ inline-редактора для полей
 *
//********************************************************************************/

$j.fn.inline_editor = function()
  { $j('.iedit').each(function(){  var reffer=$j(this).attr('reffer') ;
                                   if (reffer==undefined) { reffer=$j(this).closest('[reffer]').attr('reffer') ;
                                                            $j(this).attr('reffer',reffer) ;
                                                          }
                                   //var div_parent=$j(this).parent() ;
                                   // $j(div_parent).addClass('fed') ;
                                   // $j(div_parent).attr('fname',$j(this).attr('fname')) ;
                                   // $j(div_parent).attr('reffer',$j(this).attr('reffer')) ;
                                   // $j(this).remove() ;
                                  }) ;

    $j('.iedit[fname][reffer]').each(function(){$j(this).attr('contenteditable','true')}) ;

    CKEDITOR.on( 'instanceCreated', function( event )
    {
        var editor = event.editor,
        element = editor.element;
        editor.config.toolbarGroups = [] ;

        editor.config.toolbar = [
            { name: 'document', items: [ 'Source', '-', 'NewPage', 'Preview', '-', 'Templates' ] },
            { name: 'clipboard', items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
            '/',
            { name: 'basicstyles', items: [ 'Bold', 'Italic' ] }
        ];

        // Customize editors for headers and tag list.
        // These editors don't need features like smileys, templates, iframes etc.
        if ( element.getAttribute( 'mode' ) == 'easu' )
        {
            // Customize the editor configurations on "configLoaded" event,
            // which is fired after the configuration file loading and
            // execution. This makes it possible to change the
            // configurations before the editor initialization takes place.
            editor.on( 'configLoaded', function()
            {   // Remove unnecessary plugins to make the editor simpler.
                editor.config.removePlugins = 'colorbutton,find,flash,font,' +
                    'forms,iframe,image,newpage,removeformat,' +
                    'smiley,specialchar,stylescombo,templates';
                alert(1) ;
                // Rearrange the layout of the toolbar.
                editor.config.toolbarGroups = [] ; //
                //    { name: 'undo' },
                //    { name: 'clipboard',	groups: [ 'selection', 'clipboard' ] },
                //    { name: 'about' }
                //];
            });
        }
        /*
        if ( element.is( 'h1', 'h2', 'h3' ) || element.getAttribute( 'id' ) == 'taglist' )
        {
            // Customize the editor configurations on "configLoaded" event,
            // which is fired after the configuration file loading and
            // execution. This makes it possible to change the
            // configurations before the editor initialization takes place.
            editor.on( 'configLoaded', function() {

                // Remove unnecessary plugins to make the editor simpler.
                editor.config.removePlugins = 'colorbutton,find,flash,font,' +
                    'forms,iframe,image,newpage,removeformat,' +
                    'smiley,specialchar,stylescombo,templates';

                // Rearrange the layout of the toolbar.
                editor.config.toolbarGroups = [
                    { name: 'editing',		groups: [ 'basicstyles', 'links' ] },
                    { name: 'undo' },
                    { name: 'clipboard',	groups: [ 'selection', 'clipboard' ] },
                    { name: 'about' }
                ];
            });
        }   */

        editor.on('blur',function( e )
        {
          var params = {} ;
          params['cmd']='admin_editor/set_value';
          var el=editor.container ;
          params['reffer']=$j(el).attr('reffer');
          params['fname']=$j(el).attr('fname');
          params['value']=editor.getData();

          $j.ajax({url:'/ajax.php',type:'POST',dataType:'xml',cache:false,data:params});

        });
    });
  };

/*********************************************************************************
 *
 * отработка действий кнопок
 *
//********************************************************************************/

// перезагрузка одной строки таблицы
function reload_tr_rec(reffer)
{ var tr=$j('[reffer="'+reffer+'"]') ; // объект - строка таблицы
  var list_item=$j(tr).closest('div.list_item') ; // рабочий блок
  var data= {cmd:'admin_editor/get_obj_tr_rec',list_id:$j(list_item).attr('list_id'),clss:$j(list_item).attr('clss'),reffer:reffer,no_icon_edit:$j(list_item).attr('no_icon_edit'),no_view_field:$j(list_item).attr('no_view_field'),read_only:$j(list_item).attr('read_only'),no_icon_photo:$j(list_item).attr('no_icon_photo'),no_check:$j(list_item).attr('no_check')} ;
  $j.ajax({url:'/ajax.php',dataType:'xml',type:'POST',cache:false,data:data,success: function (data,status,link) {if (status=='success') mForm_Submit_AJAX_onSuccess(link.responseXML);}}) ;
}

/*********************************************************************************
 *
 * mForm
 *
//********************************************************************************/

// переменосим параметры из <input type=submit......> в сткрытые переменные перед отправкой формы по AJAX
// вызывается из mForm.Submit.js
function convert_tags_to_vars(input,form)
{  // переводим все аттрибуты кликнутого элемента в скрытые переменные
   //for(var j=0; j<input.attributes.length; j++) $j(form).append('<input type="hidden" name="'+input.attributes.item(j).nodeName+'" value="'+input.attributes.item(j).nodeValue+'">') ;
   // сохраняем содержимое радектора в форме
   //if (editor)  { $j('textarea[name="'+editor.name+'"]').val(editor.getData()) ;  }
   $j.each(CKEDITOR.instances,function(){this.updateElement()})  ;
   //
   //removeEditor() ;
}

// возвращем массив с атрибутами тега
 // просто вернуть .attributes нельзя, так как в IE в этом свойсве хранятся, помимо атрибутов, также и DOM-свойства объекта. Другого места, где находились бы атрибуты тега, в IE нет.
 // поэтому для выделения тегов разбираем  outerHTML тега
 // можно разобрать outerHTML через регулярное выражение /\b((\w+)=(["'])([^"']+)["'])/ig  при условии, что все значения тегов заключены в ковычки
 // но и тут IE отличился -  его outerHTML не заключает в ковычки атрибуты id,name,value: <input id=test class="demo my input" reffer="1111" cmd="222/888" name=a333 value=444s>
 // что сильно усложняет подбор регулярного выражения
 // в итоге был выбран промежуточный вариант - имена тегов получаем из outerHTML, а значения тегов из .attributes
 function convert_tag_attributes_to_array(tag_obj)
 {   var arr_attr= {} ;
     var text=tag_obj.outerHTML ;
     var myRe = /\b((\w+)=)/ig;
      while ((myArray = myRe.exec(text)) != null)
      {   var name = myArray[2] ;
          var attr=tag_obj.attributes.getNamedItem(name) ;
          if (attr!=null) arr_attr[name] = attr.value ;
   }
    return arr_attr ;
}



function on_reload_tr_rec(data,status,link)
  { if (status=='success')
      {   var xml = link.responseXML;
          var value = get_xml_tag_value(xml,'tr_html');
          var attr = get_xml_tag_attr(xml,'tr_html');
          if (value!=undefined) $j('tr#'+attr.tr_id).html(value) ;
      }
  }


/*********************************************************************************
 *
 * CKEditor
 *
//********************************************************************************/


var editor, html = '';
var submit_form = '';
var config = {};

function createEditor(id,config)
{ if ( editor ) return;
  // Create a new editor inside the <div id="editor">, setting its value to html
  //alert(id+' - '+config.skinPath) ;
  //alert(html) ;
  editor = CKEDITOR.replace( id, config, html );
}

function removeEditor()
{ if ( !editor ) return;

  // Retrieve the editor contents. In an Ajax application, this data would be
  // sent to the server or used in any other way.
  //document.getElementById( 'editorcontents' ).innerHTML = html = editor.getData();
  //document.getElementById( 'contents' ).style.display = '';

  // Destroy the editor.
  editor.destroy();
  editor = null;
}


/*********************************************************************************
 * SWFObject v1.5: Flash Player detection and embed - http://blog.deconcept.com/swfobject/
 *
 * SWFObject is (c) 2007 Geoff Stearns and is released under the MIT License:
 * http://www.opensource.org/licenses/mit-license.php
 *
//********************************************************************************/


if(typeof deconcept=="undefined"){var deconcept=new Object();}if(typeof deconcept.util=="undefined"){deconcept.util=new Object();}if(typeof deconcept.SWFObjectUtil=="undefined"){deconcept.SWFObjectUtil=new Object();}deconcept.SWFObject=function(_1,id,w,h,_5,c,_7,_8,_9,_a){if(!document.getElementById){return;}this.DETECT_KEY=_a?_a:"detectflash";this.skipDetect=deconcept.util.getRequestParameter(this.DETECT_KEY);this.params=new Object();this.variables=new Object();this.attributes=new Array();if(_1){this.setAttribute("swf",_1);}if(id){this.setAttribute("id",id);}if(w){this.setAttribute("width",w);}if(h){this.setAttribute("height",h);}if(_5){this.setAttribute("version",new deconcept.PlayerVersion(_5.toString().split(".")));}this.installedVer=deconcept.SWFObjectUtil.getPlayerVersion();if(!window.opera&&document.all&&this.installedVer.major>7){deconcept.SWFObject.doPrepUnload=true;}if(c){this.addParam("bgcolor",c);}var q=_7?_7:"high";this.addParam("quality",q);this.setAttribute("useExpressInstall",false);this.setAttribute("doExpressInstall",false);var _c=(_8)?_8:window.location;this.setAttribute("xiRedirectUrl",_c);this.setAttribute("redirectUrl","");if(_9){this.setAttribute("redirectUrl",_9);}};deconcept.SWFObject.prototype={useExpressInstall:function(_d){this.xiSWFPath=!_d?"expressinstall.swf":_d;this.setAttribute("useExpressInstall",true);},setAttribute:function(_e,_f){this.attributes[_e]=_f;},getAttribute:function(_10){return this.attributes[_10];},addParam:function(_11,_12){this.params[_11]=_12;},getParams:function(){return this.params;},addVariable:function(_13,_14){this.variables[_13]=_14;},getVariable:function(_15){return this.variables[_15];},getVariables:function(){return this.variables;},getVariablePairs:function(){var _16=new Array();var key;var _18=this.getVariables();for(key in _18){_16[_16.length]=key+"="+_18[key];}return _16;},getSWFHTML:function(){var _19="";if(navigator.plugins&&navigator.mimeTypes&&navigator.mimeTypes.length){if(this.getAttribute("doExpressInstall")){this.addVariable("MMplayerType","PlugIn");this.setAttribute("swf",this.xiSWFPath);}_19="<embed type=\"application/x-shockwave-flash\" src=\""+this.getAttribute("swf")+"\" width=\""+this.getAttribute("width")+"\" height=\""+this.getAttribute("height")+"\" style=\""+this.getAttribute("style")+"\"";_19+=" id=\""+this.getAttribute("id")+"\" name=\""+this.getAttribute("id")+"\" ";var _1a=this.getParams();for(var key in _1a){_19+=[key]+"=\""+_1a[key]+"\" ";}var _1c=this.getVariablePairs().join("&");if(_1c.length>0){_19+="flashvars=\""+_1c+"\"";}_19+="/>";}else{if(this.getAttribute("doExpressInstall")){this.addVariable("MMplayerType","ActiveX");this.setAttribute("swf",this.xiSWFPath);}_19="<object id=\""+this.getAttribute("id")+"\" classid=\"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000\" width=\""+this.getAttribute("width")+"\" height=\""+this.getAttribute("height")+"\" style=\""+this.getAttribute("style")+"\">";_19+="<param name=\"movie\" value=\""+this.getAttribute("swf")+"\" />";var _1d=this.getParams();for(var key in _1d){_19+="<param name=\""+key+"\" value=\""+_1d[key]+"\" />";}var _1f=this.getVariablePairs().join("&");if(_1f.length>0){_19+="<param name=\"flashvars\" value=\""+_1f+"\" />";}_19+="</object>";}return _19;},write:function(_20){if(this.getAttribute("useExpressInstall")){var _21=new deconcept.PlayerVersion([6,0,65]);if(this.installedVer.versionIsValid(_21)&&!this.installedVer.versionIsValid(this.getAttribute("version"))){this.setAttribute("doExpressInstall",true);this.addVariable("MMredirectURL",escape(this.getAttribute("xiRedirectUrl")));document.title=document.title.slice(0,47)+" - Flash Player Installation";this.addVariable("MMdoctitle",document.title);}}if(this.skipDetect||this.getAttribute("doExpressInstall")||this.installedVer.versionIsValid(this.getAttribute("version"))){var n=(typeof _20=="string")?document.getElementById(_20):_20;n.innerHTML=this.getSWFHTML();return true;}else{if(this.getAttribute("redirectUrl")!=""){document.location.replace(this.getAttribute("redirectUrl"));}}return false;}};deconcept.SWFObjectUtil.getPlayerVersion=function(){var _23=new deconcept.PlayerVersion([0,0,0]);if(navigator.plugins&&navigator.mimeTypes.length){var x=navigator.plugins["Shockwave Flash"];if(x&&x.description){_23=new deconcept.PlayerVersion(x.description.replace(/([a-zA-Z]|\s)+/,"").replace(/(\s+r|\s+b[0-9]+)/,".").split("."));}}else{if(navigator.userAgent&&navigator.userAgent.indexOf("Windows CE")>=0){var axo=1;var _26=3;while(axo){try{_26++;axo=new ActiveXObject("ShockwaveFlash.ShockwaveFlash."+_26);_23=new deconcept.PlayerVersion([_26,0,0]);}catch(e){axo=null;}}}else{try{var axo=new ActiveXObject("ShockwaveFlash.ShockwaveFlash.7");}catch(e){try{var axo=new ActiveXObject("ShockwaveFlash.ShockwaveFlash.6");_23=new deconcept.PlayerVersion([6,0,21]);axo.AllowScriptAccess="always";}catch(e){if(_23.major==6){return _23;}}try{axo=new ActiveXObject("ShockwaveFlash.ShockwaveFlash");}catch(e){}}if(axo!=null){_23=new deconcept.PlayerVersion(axo.GetVariable("$version").split(" ")[1].split(","));}}}return _23;};deconcept.PlayerVersion=function(_29){this.major=_29[0]!=null?parseInt(_29[0]):0;this.minor=_29[1]!=null?parseInt(_29[1]):0;this.rev=_29[2]!=null?parseInt(_29[2]):0;};deconcept.PlayerVersion.prototype.versionIsValid=function(fv){if(this.major<fv.major){return false;}if(this.major>fv.major){return true;}if(this.minor<fv.minor){return false;}if(this.minor>fv.minor){return true;}if(this.rev<fv.rev){return false;}return true;};deconcept.util={getRequestParameter:function(_2b){var q=document.location.search||document.location.hash;if(_2b==null){return q;}if(q){var _2d=q.substring(1).split("&");for(var i=0;i<_2d.length;i++){if(_2d[i].substring(0,_2d[i].indexOf("="))==_2b){return _2d[i].substring((_2d[i].indexOf("=")+1));}}}return "";}};deconcept.SWFObjectUtil.cleanupSWFs=function(){var _2f=document.getElementsByTagName("OBJECT");for(var i=_2f.length-1;i>=0;i--){_2f[i].style.display="none";for(var x in _2f[i]){if(typeof _2f[i][x]=="function"){_2f[i][x]=function(){};}}}};if(deconcept.SWFObject.doPrepUnload){if(!deconcept.unloadSet){deconcept.SWFObjectUtil.prepUnload=function(){__flash_unloadHandler=function(){};__flash_savedUnloadHandler=function(){};window.attachEvent("onunload",deconcept.SWFObjectUtil.cleanupSWFs);};window.attachEvent("onbeforeunload",deconcept.SWFObjectUtil.prepUnload);deconcept.unloadSet=true;}}if(!document.getElementById&&document.all){document.getElementById=function(id){return document.all[id];};}var getQueryParamValue=deconcept.util.getRequestParameter;var FlashObject=deconcept.SWFObject;var SWFObject=deconcept.SWFObject;

/*********************************************************************************
 *
 * обработка событий при выводе списока объектов - сортировка, фильтрация
 *
//********************************************************************************/


// клик по заголовку стролбца - применить сортировку или изменить её направление  ---------------------------------------------------------------------------------------------------------------------------------
function table_list_tr_header_td_click()
 { var fname=$j(this).attr('fname') ;
   if (fname==undefined) return ;
   var div_list_items=$j(this).parents('div.list_item') ;

   $j.ajax({url:'/ajax.php',type:'POST',dataType:'xml',cache:false,
              success             :list_items_sort_change_success,
              data:{cmd           :'admin_editor/list_items_sort_change',
                    tkey          :div_list_items.attr('tkey'),
                    clss          :div_list_items.attr('clss'),
                    usl           :div_list_items.attr('usl'),
                    usl_filter    :div_list_items.attr('usl_filter'),
                    fname         :fname,
                    list_id       :div_list_items.attr('list_id'),
                    order_by      :div_list_items.attr('order_by'),
                    order_mode    :div_list_items.attr('order_mode'),
                    no_check      :div_list_items.attr('no_check'),
                    read_only  :div_list_items.attr('read_only'),
                    no_icon_edit  :div_list_items.attr('no_icon_edit'),
                    no_view_field  :div_list_items.attr('no_view_field'),
                    no_icon_photo :div_list_items.attr('no_icon_photo'),
                    count         :div_list_items.attr('count'),
                    page          :1
                   }
            });
 }

function list_items_sort_change_success(data,status,link)
{ if (status=='success')
    {   var xml = link.responseXML;
        if (xml==undefined)  return ;
        var list_id = get_xml_tag_value(xml,'list_id');
        var order_by = get_xml_tag_value(xml,'order_by');
        var order_mode = get_xml_tag_value(xml,'order_mode');
        var img = get_xml_tag_value(xml,'img');
        var tr_html = get_xml_tag_value(xml,'html');
        $j('div#'+list_id+' table.list').find('img.sort').remove();
        $j('div#'+list_id+' table.list tr.td_header td[fname="'+order_by+'"]').append(img) ;
        $j('div[list_id="'+list_id+'"]').attr('order_by',order_by) ;
        $j('div[list_id="'+list_id+'"]').attr('order_mode',order_mode) ;
        $j('div#'+list_id+' table.list  tr.item').remove() ;
        $j('div#'+list_id+' table.list').append(tr_html) ;
        update_next_page_info(xml) ;
    }
}

// создаем элемент memo  ---------------------------------------------------------------------------------------------------------------------------------

function onclick_gm()
 {  $j(this).removeAttr('el').html('').addClass('wait') ;
    $j.ajax({url:'/ajax.php',type:'POST',dataType:'xml',cache:false,
                success             :onclick_gm_AJAX_success,
                data:{cmd           :'admin_editor/get_obj_field',
                      reffer        :$j(this).closest('[reffer]').attr('reffer'),
                      fname         :$j(this).attr('fn')
                     }
              });
 }

 function onclick_gm_AJAX_success(data,status,link)
 { if (status=='success')
     {   var xml = link.responseXML;
         // определяем ячейку таблицы
         var reffer = get_xml_tag_value(xml,'reffer');
         var fname = get_xml_tag_value(xml,'fname');
         var td=$j('[reffer="'+reffer+'"] td[fn="'+fname+'"]') ;
         // определяем имя  генерируемого элемента
         var tkey = get_xml_tag_value(xml,'tkey');
         var pkey = get_xml_tag_value(xml,'pkey');
         var clss = get_xml_tag_value(xml,'clss');
         var element_name='obj['+tkey+']['+clss+']['+pkey+']['+fname+']' ;
         var element_id='obj_'+tkey+"_"+clss+"_"+pkey+"_"+fname ;
         // определяем дополнительные параметры
         var el_class=td.attr('ec') ;
         var value = get_xml_tag_value(xml,'html');
         var use_html_editor = get_xml_tag_value(xml,'use_html_editor');

         td.html('<textarea id="'+element_id+'" name="'+element_name+'" class="'+el_class+'" type="text">'+value+'</textarea>')
           .removeClass('wait').removeAttr('fn').removeAttr('ec')
           .children('textarea').width(td.width()-8).height(td.height()-4).focus();

         if (use_html_editor==1)
         {
             include_ckeditor_script() ;
             $j('textarea#'+element_name).editor = CKEDITOR.replace(element_id);

         }

         // заменяем значек редактирования на значек сохранения
         $j('[reffer="'+reffer+'"] td.to_edit').removeClass('to_edit').addClass('to_save') ;
     }
 }

 // создаем элемент input ---------------------------------------------------------------------------------------------------------------------------------
 function onclick_gi()
 {   $j(this).html('').addClass('wait').removeAttr('el') ;
     $j.ajax({url:'/ajax.php',type:'POST',dataType:'xml',cache:false,
                 success             :onclick_gi_AJAX_success,
                 data:{cmd           :'admin_editor/get_obj_field',
                       reffer        :$j(this).closest('[reffer]').attr('reffer'),
                       fname         :$j(this).attr('fn')
                      }
               });
 }

function onclick_gi_AJAX_success(data,status,link)
{ if (status=='success')
  {   var xml = link.responseXML;
      // определяем ячейку таблицы
      var reffer = get_xml_tag_value(xml,'reffer');
      var fname = get_xml_tag_value(xml,'fname');
      var td=$j('[reffer="'+reffer+'"] td[fn="'+fname+'"]') ;
      // определяем имя  генерируемого элемента
      var tkey = get_xml_tag_value(xml,'tkey');
      var pkey = get_xml_tag_value(xml,'pkey');
      var clss = get_xml_tag_value(xml,'clss');
      var element_name='obj['+tkey+']['+clss+']['+pkey+']['+fname+']' ;
      var id='el_'+tkey+'_'+pkey+'_'+fname ;
      // определяем дополнительные параметры
      var options=get_xml_tag_attr(xml,'options') ;
      var el_class=td.attr('ec') ;

      var value = get_xml_tag_value(xml,'html');

      td.html('<input name="'+element_name+'" id="'+id+'" class="text '+el_class+'" type="text" value="'+value+'">')
        .removeClass('wait').removeAttr('fn').removeAttr('ec')
        .children('input').width(td.width()-8).focus();

      // дополнительный опции к элементу
      if (options.datepicker==1)
      {  include_datapicker_script() ;
         $j('#'+id).datepicker($j.datepicker.regional["ru"]).datepicker("show"); // добавляем датапикер
      }
       // добавляем датапикер
      if (options.datetimepicker==1)
      {  include_datapicker_script() ;
         $j('#'+id).datetimepicker($j.datepicker.regional["ru"]).datetimepicker("show");
      }

      // заменяем значек редактирования на значек сохранения
      $j('[reffer="'+reffer+'"] td.to_edit').removeClass('to_edit').addClass('to_save') ;
  }
}

 function include_datapicker_script()
 { if ($j.datepicker==undefined)
    { $j("head").append($j('<script type="text/javascript" src="'+_PATH_TO_ADMIN_ADDONS+'/jquery-ui-1.9.2/jquery.ui.datepicker.min.js"></script>'));
      $j("head").append($j('<script type="text/javascript" src="'+_PATH_TO_ADMIN_ADDONS+'/jquery-ui-1.9.2/jquery.ui.datepicker-ru.js"></script>'));
      $j("head").append($j('<script type="text/javascript" src="'+_PATH_TO_ADMIN_ADDONS+'/jquery-ui-1.9.2/jquery-ui-timepicker-addon.js"></script>'));
      $j("head").append($j('<link rel="stylesheet" type="text/css" href="'+_PATH_TO_ADMIN_ADDONS+'/jquery-ui-1.9.2/jquery.ui.datepicker.css"/>'));
      $j("head").append($j('<link rel="stylesheet" type="text/css" href="'+_PATH_TO_ADMIN_ADDONS+'/jquery-ui-1.9.2/jquery-ui-timepicker-addon.css"/>'));
    }
 }

 function include_ckeditor_script()
 { if (CKEDITOR==undefined && _CKEDITOR_!='none' && _CKEDITOR_!="")
    { $j("head").append($j('<script type="text/javascript" src="'+_EXT+'/'+_CKEDITOR_+'/ckeditor.js"></script>'));
    }
 }



 // создаем элемент select ---------------------------------------------------------------------------------------------------------------------------------
 function onclick_gs()
 {  $j(this).removeAttr('el').html('').addClass('wait') ;
    $j.ajax({url:'/ajax.php',type:'POST',dataType:'xml',cache:false,
                 success             :onclick_gs_AJAX_success,
                 data:{cmd           :'admin_editor/get_array_values',
                       reffer        :$j(this).closest('[reffer]').attr('reffer'),
                       fname         :$j(this).attr('fn'),
                       arr_id        :$j(this).attr('ai'),
                       arr_field     :$j(this).attr('af')
                      }
               });
 }

 function onclick_gs_AJAX_success(data,status,link)
 { if (status=='success')
    {   var xml = link.responseXML;
        // определяем ячейку таблицы
        var reffer = get_xml_tag_value(xml,'reffer');
        var fname = get_xml_tag_value(xml,'fname');
        var td=$j('[reffer="'+reffer+'"] td[fn="'+fname+'"]') ;
        // определяем имя  генерируемого элемента
        var tkey = get_xml_tag_value(xml,'tkey');
        var pkey = get_xml_tag_value(xml,'pkey');
        var clss = get_xml_tag_value(xml,'clss');
        var element_name='obj['+tkey+']['+clss+']['+pkey+']['+fname+']' ;
        // определяем дополнительные параметры
        var el_arr_sel=td.attr('as') ; // код выбранного элемента
        // собираем select
        var el_sel='' ; var el_html='' ;
        var elem = xml.getElementsByTagName('arr_value'); // массив значений массива
        $j.each(elem,function(indx,obj)
            { var obj_attr=get_xml_obj_attr(obj) ; // получаем аттрибуты текущего value
              if (obj_attr.id==el_arr_sel) el_sel=' selected' ; else el_sel='' ;
              var title=($j.browser.msie)? obj.text:obj.textContent ;
              el_html+='<option value="'+obj_attr.id+'"'+el_sel+'>'+title+'</option>' ;
            }) ;
        td.html('<select size="1" name="'+element_name+'"><option value=""></option>'+el_html+'</select>')
          .removeClass('wait').removeAttr('fn').removeAttr('ec')
          .children('select').width(td.width()-8).focus();

        // заменяем значек редактирования на значек сохранения
        $j('[reffer="'+reffer+'"] td.to_edit').removeClass('to_edit').addClass('to_save') ;
    }
 }

 // создаем элемент select для parent ---------------------------------------------------------------------------------------------------------------------------------
 function onclick_gts()
 {  $j(this).removeAttr('el').html('').addClass('wait') ;
    $j.ajax({url:'/ajax.php',type:'POST',dataType:'xml',cache:false,
                 success             :onclick_gs_AJAX_success,
                 data:{cmd           :'admin_editor/get_table_clss_values',
                       reffer        :$j(this).closest('[reffer]').attr('reffer'),
                       fname         :$j(this).attr('fn'),
                       as            :$j(this).attr('as')
                      }
               });
 }

//generate_checkbox   ---------------------------------------------------------------------------------------------------------------------------------
function onclick_gc()
{  var img=$j(this).find('img') ;
   var value ;
   var inverse=$j(this).attr('inverse') ;
   var next_img ;
   if (img.attr('alt')=='+')
    { next_img=(inverse==1)? "/on3.gif":"/off3.gif" ;
      img.attr('src',_PATH_TO_ADMIN_IMAGES+next_img).attr('alt','-') ;
      value=0 ;
    }
   else
    { next_img=(inverse==1)? "/off3.gif":"/on3.gif" ;
      img.attr('src',_PATH_TO_ADMIN_IMAGES+next_img).attr('alt','+') ;
      value=1 ;
    }

   $j.ajax({url:'/ajax.php',type:'POST',dataType:'xml',cache:false,
            success             :set_value_AJAX_success,
            data:{cmd           :'admin_editor/set_value',
                  reffer        :$j(this).closest('[reffer]').attr('reffer'),
                  clss          :$j(this).closest('div.list_item').attr('clss'),
                  fname         :$j(this).attr('fn'),
                  value         :value
                 }
          });
}
// для поля "enabled" - отдельные картинки
function onclick_gc2()
{  var img=$j(this).children('img') ;
   var value=0  ;
   var reffer=$j(this).closest('[reffer]').attr('reffer') ;
   if (img.attr('alt')=='+') img.attr('src',_PATH_TO_ADMIN_IMAGES+'/off2.gif').attr('alt','-') ;
   else                    { img.attr('src',_PATH_TO_ADMIN_IMAGES+'/on2.gif').attr('alt','+') ; value=1 ; }

   $j.ajax({url:'/ajax.php',type:'POST',dataType:'xml',cache:false,
            success             :set_value_AJAX_success,
            data:{cmd           :'admin_editor/set_value',
                  reffer        :reffer,
                  clss          :$j(this).closest('div.list_item').attr('clss'),
                  fname         :$j(this).attr('fn'),
                  value         :value
                 }
          });

   // обновляем статус объекта в дереве - включено/выключено
   var fra_tree=top.fra_tree.document ;
   if (fra_tree!=null)
    {   var tree_li=$j(fra_tree).contents().find('li[reffer="'+reffer+'"]') ;
        if (tree_li.length)
        {  if (value) $j(tree_li).removeAttr('off') ;
           else       $j(tree_li).attr('off',true) ;
        }
    }

}

function set_value_AJAX_success(data,status,link)
{   if (status=='success')
        {   var xml = link.responseXML;
            // результат сохранения
            var update_cnt = get_xml_tag_value(xml,'update_cnt');
            var html = get_xml_tag_value(xml,'html');
            if (update_cnt>0) show_notife3({content:'Сохранено'}) ;
        }
}

// generate nulticheck ---------------------------------------------------------------------------------------------------------------------------------
function onclick_gmc()
 {  $j(this).removeAttr('el').html('').addClass('wait') ;
    $j.ajax({url:'/ajax.php',type:'POST',dataType:'xml',cache:false,
                  success             :onclick_gmc_AJAX_success,
                  data:{cmd           :'admin_editor/get_array_values',
                        reffer        :$j(this).closest('[reffer]').attr('reffer'),
                        fname         :$j(this).attr('fn'),
                        arr_id        :$j(this).attr('ai'),
                        arr_field     :$j(this).attr('af')
                       }
                });
 }

 function onclick_gmc_AJAX_success(data,status,link)
 { if (status=='success')
    {   var xml = link.responseXML;
        // определяем ячейку таблицы
        var reffer = get_xml_tag_value(xml,'reffer');
        var fname = get_xml_tag_value(xml,'fname');
        var td=$j('[reffer="'+reffer+'"] td[fn="'+fname+'"]') ;
        // определяем имя  генерируемого элемента
        var tkey = get_xml_tag_value(xml,'tkey');
        var pkey = get_xml_tag_value(xml,'pkey');
        var clss = get_xml_tag_value(xml,'clss');
        var element_name='obj['+tkey+']['+clss+']['+pkey+']['+fname+']' ;
        // определяем дополнительные параметры
        var el_arr_sel=new String(td.attr('as')) ; // выбранные в multicheck коды значений
        var el_arr_sel2=el_arr_sel.split(' ') ;
        // собираем select
        var el_sel='' ; var el_html='' ;
        var elem = xml.getElementsByTagName('arr_value'); // массив значений массива
        $j.each(elem,function(indx,obj)
            { var obj_attr=get_xml_obj_attr(obj) ; // получаем аттрибуты текущего value
              if ($j.inArray(obj_attr.id,el_arr_sel2)!=-1) el_sel=' checked' ; else el_sel='' ;
              var title=($j.browser.msie)? obj.text:obj.textContent ;
              el_html+='<div style="white-space:nowrap;"><input type="checkbox" name="'+element_name+'['+obj_attr.id+']" value="1" '+el_sel+'>&nbsp;'+title+'</div>' ;
            }) ;
        el_html='<div class=left>'+el_html+'</div>' ;

        td.html(el_html).removeClass('wait').removeAttr('fn').removeAttr('ec').children('select').width(td.width()-8).focus();

        // заменяем значек редактирования на значек сохранения
        $j('[reffer="'+reffer+'"] td.to_edit').removeClass('to_edit').addClass('to_save') ;
    }
 }

 function onclick_check_all()
 { if ($j(this).is(':checked')) $j(this).closest('table').find('input[type="checkbox"][name^="_check"]').attr('checked',true)  ;
   else                         $j(this).closest('table').find('input[type="checkbox"][name^="_check"]').removeAttr('checked')  ;
 }

 function onclick_td_check()
{ var input=$j(this).children('input') ;
  if ($j(input).is(':checked')) $j(input).removeAttr('checked')  ;
  else                          $j(input).attr('checked',true)  ;
}

 function onclick_td_check2()
{ var input=$j(this) ;
  if ($j(input).is(':checked')) $j(input).removeAttr('checked')  ;
  else                          $j(input).attr('checked',true)  ;
}

function onclick_to_edit()
{ var reffer=$j(this).attr('reffer') ;
  if (reffer==undefined) reffer=$j(this).closest('tr').attr('reffer') ;
  if ($j(this).attr('link')!=undefined) reffer=$j(this).attr('link') ;
  var a = reffer.split('.');
  parent.window.open('/cab/admin_editor/viewer/'+reffer+'/','obj_show_'+reffer,'status=1,dependent=1,width=800,height=600, resizable=1, scrollbars=yes');
}

function onclick_to_new_window()
{
    parent.window.open($j(this).attr('href'));
    return(false) ;
}

function onclick_to_save()
{ var tr=$j(this).closest('tr') ;
  var div_list_items=$j(this).closest('div.list_item') ;
  $j(tr).wrapInner('<form  method="POST" action="/ajax.php"></form>') ;
  var form=$j(tr).children('form') ;
  $j(form).append('<input type="hidden" name="cmd" value="admin_editor/save_list"><input type="hidden" name="list_id" value="'+div_list_items.attr('id')+'">') ;
  // if (editor)  { $j('textarea[name="'+editor.name+'"]').val(editor.getData()) ;  }
  // проходим по всем textarea в строке и ищем если ли для них открытые редакторы

  //$j.each(CKEDITOR.instances,function(){this.updateElement()})  ;

  $j(form).ajaxForm({url:'/ajax.php',dataType:'xml',success:onclick_to_save_respone});
  $j(form).submit() ;
  $j(form).children('input[name="cmd"]').remove() ;
  $j(form).children('input[name="mode"]').remove() ;
  $j(form).after($j(form).children()) ;
  $j(form).remove() ;
}

function onclick_to_save_respone(responseXML)
{  if (responseXML!=undefined)
   { var html = get_xml_tag_value(responseXML,'html');
     var title = get_xml_tag_value(responseXML,'title');
     //var tr_content = get_xml_tag_value(responseXML,'tr_content');
   }
   else
   { var html=responseXML ;
     var title="<div class=alert>Ошибка</div>" ;
   }

   var data=convert_XML_to_obj(responseXML) ;

   var list_id = get_xml_tag_value(responseXML,'list_id');
   var parent_reffer = get_xml_tag_value(responseXML,'parent_reffer');
   var table_update=0 ;
   var flag_child_reload=0 ;
   if (top.fra_tree!=undefined) var fra_tree=top.fra_tree.document ;

   // обновляем или добавляем переданные строки
   var tr_content = responseXML.getElementsByTagName('tr_content');
   if (tr_content.length && list_id!=undefined && list_id!="") for (var i=0; i<tr_content.length; i++)
    { var attr_XML=tr_content[i].attributes;
      var reffer = attr_XML.getNamedItem("reffer").nodeValue;
      var dat=($j.browser.msie)? tr_content[i].text:tr_content[i].textContent ;
      if ($j('div#'+list_id+' [reffer="'+reffer+'"]').length) { $j('div#'+list_id+' [reffer="'+reffer+'"]').replaceWith(dat) ; table_update=1 ; }
      else if ($j('div#'+list_id+' table.list').length)         { $j('div#'+list_id+' > table.list').append(dat) ; table_update=1 ; }
      //else if ($j('div#'+list_id+' table.list').length)         { $j('div#'+list_id+' > table.list').prepend(dat) ; table_update=1 ; }
    }
}


/*********************************************************************************
 *
 * вспомогательные функции
 *
//********************************************************************************/


function f_preventDefault(e){(e.preventDefault)? e.preventDefault():e.returnValue=false ;}

function get_xml_tag_value(xml,tag_name)
{   var elem = xml.getElementsByTagName(tag_name);
    if (elem.length)
    {   if ($j.browser.msie) return(elem[0].text);
        else                 return(elem[0].textContent);
    }
    else return('');
}

// получаем атрибуеты XML TAG объекта
function get_xml_tag_attr(xml,tag_name)
{   var attr_XML=xml.getElementsByTagName(tag_name).item(0).attributes;
    var attr = new Object();
    if (attr_XML.length) for(var j=0; j<attr_XML.length; j++) attr[attr_XML[j].nodeName]=attr_XML[j].nodeValue ;
    return(attr);
}

// получаем атрибуеты XML OBJ объекта
function get_xml_obj_attr(obj)
{   var attr_XML=obj.attributes;
    var attr = new Object();
    for(var j=0; j<attr_XML.length; j++) attr[attr_XML[j].nodeName]=attr_XML[j].nodeValue ;
    return(attr);
}


function show_obj(tkey,pkey,edit,menu_id)
{
   parent.window.open('fra_viewer.php?tkey='+tkey+'&pkey='+pkey+'&edit='+edit+'&menu_id='+menu_id,'obj_show_'+tkey,'status=1,dependent=1,width=800,height=600, resizable=1, scrollbars=yes');
}

function dump(obj, obj_name)
{ var result = '' ;
 for (var i in obj) result += obj_name + '.' + i + ' = ' + obj[i] + '\n' ;
 return result ;
}



