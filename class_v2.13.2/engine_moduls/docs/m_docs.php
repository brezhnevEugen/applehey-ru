<?php
define('_DOCS_DIR',_DIR_TO_ROOT.'/docs/') ;
define('_DOCS_PATH',_PATH_TO_SITE.'/docs/') ;

$__functions['init'][]='_docs_site_vars' ;
$__functions['boot_site'][]='_docs_site_boot' ;

function DOCS() {return($_SESSION['docs_system']);$_SESSION['docs_system']=new c_docs_system();return($_SESSION['docs_system']);} ;

function _docs_site_vars()
{

}

function _docs_site_boot($options)
{
  create_system_modul_obj('docs',$options) ;

}


class c_docs_system  extends c_system
{
    public $table_docs ;
    public $report_obj ;

    function c_docs_system($create_options=array())
    { global $site_TM_kode ;
      $this->table_name ='obj_'.$site_TM_kode.'_docs' ;
      $this->table_docs ='obj_'.$site_TM_kode.'_docs' ;
      $this->system_title ='Документы';
      parent::__construct($create_options) ;
      //$this->pages_info=array() ;

      // регистрируем обработчики событий
      //ESMO()->reg_event_lister('mo_dopusk_allow',$this,'on_event_mo_allow') ;         //
      //ESMO()->reg_event_lister('fixed_hand_check_allow',$this,'on_event_mo_allow') ;
      //ESMO()->reg_event_lister('fixed_hand_check_denited',$this,'on_event_mo_allow') ;

      REPORTS()->reg_reports($this,'report_log_events','log/report_log_events.php','report_log_events') ;         //
      //REPORTS()->reg_reports($this,'report_log_req','reports/report_log_req.php','report_log_req') ;         //



      //регистрируем документы
      set_time_point('Создание объекта <strong>docs_system</strong>') ;
    }

   //
   //function on_event_mo_allow($data)
   //{  $rec_mo=ESMO()->get_mo_by_id($data['mo_id']) ;
   //   if ($rec_mo['pkey'] and $rec_mo['member_id']) $this->apply_mo_bill($rec_mo) ;
   //}

    function declare_menu_items_3($page)
      {
          /*
          $page->menu['/cab/docs/']=array('title'=>'Документы',
                                             'class'=>'fa fa-file-text-o',
                                             'items'=>array('/cab/docs/'=>array('title'=>'Документы','class'=>'fa fa-users')

                                                           )
                                             ) ;
         */
      }

    //==========================================================================================================================================================================
    // РАБОТА  С документами
    //==========================================================================================================================================================================

    function get_doc_by_id($doc_id)
    { $rec=execSQL_van('select * from  obj_site_docs where pkey='.$doc_id) ;
     if ($rec['pkey']) $this->prepare_public_info_to_doc($rec) ;
     return($rec) ;
    }

    function get_doc_number($doc_id)
    { $num=execSQL_value('select indx from obj_site_docs where pkey='.$doc_id) ;
     return($num) ;
    }

    // создание документа PDF на основе отчета и регистрация его в документах системы
    // создаем документ в системе
    // и сохраняем в нем имя файла
    function doc_create($report_code,$filter_set,$options=array())
    { global $member ;
     $res=array() ;
      $report_obj=REPORTS()->get_report_obj_by_code($report_code) ;
      if ($report_obj->error) $res['error']=$report_obj->error ;
      else
      {   $report_obj->filter=$filter_set ;
         // регистрируем новый документ - нам нужен номер документа для сохранения в заголовке PDF
          $doc_id=$this->reg_doc(array('obj_name'=>$report_obj->title,
                                       'member_id'=>$member->id,
                                       'code'=>'PDF'
                                       ),
                                       $report_code
                                    ) ;
         $doc_number=$this->get_doc_number($doc_id) ;
          $report_obj->title.=' № '.$doc_number ;
          // создаем документ PDF
          $doc_file_name=$report_obj->save_as_PDF(array('file_name'=>$report_code.'_'.$doc_number,'use_doc_dir'=>1)) ;

         // сохраняем информацию по документу в БД
          if ($doc_file_name) $this->update_doc($doc_id,array('obj_name'=>$report_obj->title,
                                                             'doc_name'=>$doc_file_name,
                                                        )
                                          ) ;

         $res['doc_id']=$doc_id ;
       }
     return($res) ;
    }

    function doc_delete($doc_id)
    {   $rec=$this->get_doc_by_id($doc_id) ;
        if ($rec['pkey'] and !$rec['sign'])
      { execSQL_update('delete from obj_site_docs where pkey='.$doc_id) ;
          unlink($rec['__doc_dir']) ;
          return(array('success','Документ удален')) ;
        }
        else if ($rec['sign']) return(array('error','Документ не может быть удален, так как он подписан')) ;
        else                   return(array('error','Документ не найден')) ;
      }

    // $params=
    // $type=report_mo,report_mo_small,report_to_smena,report_mo_smena - по названию скрипта отчета
    function reg_doc($params,$type='',$seria='')
     { // получаем код папки для отчетов данного типа, если нет - создаем папку
       $params['clss']=215 ;
       $parent=execSQL_value('select pkey from obj_site_docs where clss=1 and doc_name="'.$type.'"') ;
       if (!$parent) $parent=adding_rec_to_table('obj_site_docs',array('clss'=>1,'parent'=>1,'obj_name'=>$type,'doc_name'=>$type)) ;
       $params['parent']=$parent ;
       // если передана серия - создаем отдельную папку. Обычно серия идет у путевых листов, выделение серии в отедбную папку нужно для правильной нумерации ПЛ внутри серии
       if ($seria)
       {  $seria_id=execSQL_value('select pkey from obj_site_docs where doc_name="'.$seria.'" and clss=1 and parent='.$parent) ;
          if (!$seria_id) $seria_id=adding_rec_to_table('obj_site_docs',array('clss'=>1,'parent'=>$params['parent'],'obj_name'=>$seria,'doc_name'=>$seria)) ;
          $params['parent']=$seria_id ;
       }
       //print_r($params)  ;
       $id=adding_rec_to_table('obj_site_docs',$params) ;
       //$num=execSQL_value('select indx from  obj_site_docs where pkey='.$id) ;
       //update_rec_in_table('obj_site_docs',array('obj_name'=>$params['obj_name'].' № '.$num),'pkey='.$id) ;
       return($id) ;
     }

     function update_doc($doc_id,$params)
     { // удаляем прежний документ, который был связан с объектом документа- надо ли?
       update_rec_in_table('obj_site_docs',$params,'pkey='.$doc_id) ;
     }

    function sign_doc($doc_id,$sert_id,$sign)
    {
      update_rec_in_table('obj_site_docs',array('sign_member'=>MEMBER()->id,'sign_data'=>time(),'sert_id'=>$sert_id,'sign'=>$sign),'pkey='.$doc_id) ;
      LOGS()->reg_log('Подписание документа',$doc_id) ;
      // закрываем текущую смену врача
      //ACCOUNTS()->member_save($_SESSION['member']->id,array('smena_out'=>time())) ;
      //$_SESSION['member']->info['smena_out']=time() ;
    }

    function get_list_docs($options=array())
    {  $_usl[]='clss=215' ;
       if ($options['member_id']) $_usl[]='member_id='.$options['member_id'] ;
       $usl=implode(' and ',$_usl) ;
       $order=($options['order'])? $options['order']:'c_data desc' ;
       $recs=execSQL('select * from obj_site_docs where '.$usl.' order by '.$order) ;
       if (sizeof($recs)) foreach($recs as $id=>$rec) $this->prepare_public_info_to_doc($recs[$id]) ;
       return($recs) ;
    }


    function prepare_public_info_to_doc(&$rec)
    {  $rec['__doc_dir']=_DIR_TO_ROOT.'/docs/'.$rec['doc_name'] ;
       $rec['_member_name']=ACCOUNTS()->get_member_name_by_id($rec['member_id'])  ;
       $rec['_date_create']=date('d.m.Y H:i',$rec['c_data']) ;
       $rec['_date_podpis']=($rec['sign_data'])? date('d.m.Y H:i',$rec['sign_data']):'-' ;
    }



}



?>