<?php // этот модуль необходимо делать тольео для крупных проектов, для небольшых описание пунктов меню и файлов меню лучше делать в init.php
$__functions['init'][]      ='_docs_admin_vars' ;
$__functions['boot_admin'][] ='_docs_admin_boot' ;
//$__functions['alert'][]   ='_docs_admin_alert' ;
$__functions['install'][]	='_docs_install_modul' ;



function _docs_admin_vars() //
{   //-----------------------------------------------------------------------------------------------------------------------------
	// описание переменных модуля
	//-----------------------------------------------------------------------------------------------------------------------------
    //$_SESSION['ARR_roll'][9]=array('obj_name'=>'Франчайзер',   'index_page'=>'/cab/bill/') ;

	//-----------------------------------------------------------------------------------------------------------------------------
	// описание классов модуля
	//-----------------------------------------------------------------------------------------------------------------------------


    //-----------------------------------------------------------------------------------------------------
    $_SESSION['descr_clss'][215]['name']='Документ' ;
    $_SESSION['descr_clss'][215]['table_code']='docs' ;
    $_SESSION['descr_clss'][215]['parent']=0 ;
    $_SESSION['descr_clss'][215]['fields']['doc_name']=array('type'=>'varchar(255)') ; //  название файла документа
    $_SESSION['descr_clss'][215]['fields']['obj_reffer']=array('type'=>'any_object') ; //  источник документа
    $_SESSION['descr_clss'][215]['fields']['member_id']=array('type'=>'int(11)') ; //  автор документа
    $_SESSION['descr_clss'][215]['fields']['personal_id']=array('type'=>'int(11)') ; // объект документа
    $_SESSION['descr_clss'][215]['fields']['sign_member']=array('type'=>'int(11)') ;  // кто подписал документ
    $_SESSION['descr_clss'][215]['fields']['sign']=array('type'=>'text') ;  // подпись документ
    $_SESSION['descr_clss'][215]['fields']['sign_data']=array('type'=>'timedata') ;  // дата подписания документа
    $_SESSION['descr_clss'][215]['fields']['sert_id']=array('type'=>'varchar(255)') ;  // id подписи ЕЦП
    $_SESSION['descr_clss'][215]['fields']['code']=array('type'=>'varchar(32)') ;  // тип документа

    $_SESSION['descr_clss'][215]['list']['field']['pkey']			=array('title'=>'pkey') ;
    $_SESSION['descr_clss'][215]['list']['field']['indx']			=array('title'=>'indx') ;
    $_SESSION['descr_clss'][215]['list']['field']['obj_name']			=array('title'=>'obj_name') ;
    $_SESSION['descr_clss'][215]['list']['field']['doc_name']			=array('title'=>'doc_name') ;
    $_SESSION['descr_clss'][215]['list']['field']['code']			=array('title'=>'Тип документа') ;
    $_SESSION['descr_clss'][215]['list']['field']['obj_reffer']			        =array('title'=>'obj_reffer','readonly'=>1) ;
    $_SESSION['descr_clss'][215]['list']['field']['member_id']			=array('title'=>'member_id','readonly'=>1) ;
    $_SESSION['descr_clss'][215]['list']['field']['personal_id']		=array('title'=>'personal_id','readonly'=>1) ;
    $_SESSION['descr_clss'][215]['list']['field']['sign_member']			=array('title'=>'sign_member','readonly'=>1) ;
    $_SESSION['descr_clss'][215]['list']['field']['sign']		    =array('title'=>'sign','readonly'=>1) ;
    $_SESSION['descr_clss'][215]['list']['field']['sign_data']		    =array('title'=>'sign_data','readonly'=>1) ;
    $_SESSION['descr_clss'][215]['list']['field']['sert_id']		    =array('title'=>'sert_id','readonly'=>1) ;

    $_SESSION['descr_clss'][215]['no_show_in_tree']=0 ; // не показывать объекты класса в дереве
    $_SESSION['descr_clss'][215]['icons']=_PATH_TO_BASED_CLSS_IMG.'/98.png' ;


	//----------------------------------------------------------------------------------------------------------------------
	// описание пунктов меню админа сайта
	//-----------------------------------------------------------------------------------------------------------------------
}

function _docs_admin_boot($options)
{
    create_system_modul_obj('docs',$options) ;

}


function _docs_install_modul($DOT_root)
{    echo '<h2>Инсталируем модуль <strong>ДОКУМЕНТЫ</strong></h2>' ;

    // документы
    $pattern=array() ;
    $pattern['table_title']		        = 	'Документы' ;
    $pattern['use_clss']		        = 	'1,215' ;
    $pattern['def_recs'][]		        =	array ('parent'=>0,	'clss'=>1,	'enabled'=>1,	'indx'=>0,	'obj_name'=>'Документы') ;
    $pattern['table_name']				= 	'obj_site_docs' ;
    $pattern['table_clss']				= 	101 ;
    $pattern['table_parent']			= 	$DOT_root;
    create_table_by_pattern($pattern) ;

    //$id_list=SETUP_add_list_to_site_list(1,'Тарифы МО','tarif') ;

    //$id_sect=SETUP_add_section_to_site_setting('Биллинг') ;
    //SETUP_add_rec_to_site_setting($id_sect,"Стоимость допуска для клиента",50,'LS_bill_price_client') ;
    //SETUP_add_rec_to_site_setting($id_sect,"Стоимость допуска для ЭСМО",5,'LS_pp_bill_price_esmo') ;

}




?>