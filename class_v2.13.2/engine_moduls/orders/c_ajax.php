<?
include_once(_DIR_TO_ENGINE.'/c_page_XML_AJAX.php') ;
class c_page_ajax extends c_page_XML_AJAX
{
    function del_from_orders()
      { global $orders_system ;
        //damp_array($_POST) ; $this->add_element('show_modal_window','HTML') ;
        if ($_POST['smeta_id']) {  $orders_system->order_delete_goods($_POST['order_id'],$_POST['smeta_id']) ;
                                   $this->add_element('show_notife','Товар успешно удален') ;
                                   $this->add_element('update_page',1) ;
                               }


      }

    function update_order()
     { global $orders_system ;
       //damp_array($_POST) ;
       //$this->add_element('show_modal_window','HTML') ;
       if ($_POST['cart_cnt'])
       { foreach($_POST['cart_cnt'] as $reffer=>$cnt) $orders_system->order_update_goods_cnt($_POST['order_id'],$reffer,$cnt,array('no_update_sum_price'=>0)) ;
         //$orders_system->order_update_sum_price($_POST['pkey']) ;
         $this->add_element('show_notife','Заказ пересчитан') ;
         $this->add_element('update_page',1) ;
       }
     }

    function change_sales_to_goods()
     { global $orders_system,$goods_system,$debug_price ;
       if (!$_POST['code']) { $this->add_element('show_notife','Не указан штрих-код или артикул') ; return ; }
       $cur_order=$orders_system->get_order($_POST['order_id'],array('goods_full_info'=>1)) ;
       $flag=0 ;
       if (sizeof($cur_order['goods'])) foreach($cur_order['goods'] as $rec_order)
       { if ($rec_order['art']==$_POST['code'] or $rec_order['_sortament_rec']['code']==$_POST['code'])
         { // вписываем в запись по товару значение скидки
           $old_price=$rec_order['__reffer_from']['price_info']['__price_4'] ;
           $rec_order['sales_type']=0 ;
           if ($_POST['sales_value'])  { $rec_order['sales_type']=4 ; $rec_order['sales_value']=abs($_POST['sales_value']) ; }
           if ($_POST['sales_value2']) { $rec_order['sales_type']=3 ; $rec_order['sales_value']=abs($_POST['sales_value2']) ; }

           $rec_order['price']=$rec_order['__reffer_from']['price'] ;  // исходная цена товара должна соответствовать цене на момент покупки
           $goods_system->exec_price($rec_order) ;  // говотим цену
           // собираем в массив, все что касается цены
           $price_info=array() ;
           if (sizeof($rec_order)) foreach($rec_order as $name=>$value) if (strpos($name,'__price')!==false) $price_info[$name]=$value ;
           $price_info['__price_used_sales']='admin' ;
           $price_info['price_source']=$rec_order['__reffer_from']['price_info']['price_source'] ;  // исходная цена товара должна соответствовать цене на момент покупки ;
           $price_info['sales_size']=$rec_order['__reffer_from']['price_info']['sales_size'] ;  //

           // сохраняем данные в смене
           update_rec_in_table('obj_site_orders_goods',array('sales_type'=>$rec_order['sales_type'],'sales_value'=>$rec_order['sales_value'],'price_info'=>serialize($price_info)),'pkey='.$rec_order['__reffer_from']['pkey']) ;
           // журнал
           $value_from=($rec_order['__reffer_from']['sales_type']==3)?  $rec_order['__reffer_from']['sales_value'].'%':_format_price($rec_order['__reffer_from']['sales_value']) ;
           $value_to=($rec_order['sales_type']==3)?  $rec_order['sales_value'].'%':_format_price($rec_order['sales_value']) ;
           _event_reg('Заказ: изменение скидки на товар ',$rec_order['obj_name'].' ['.$_POST['code'].'], '.$value_from.' => '.$value_to.' / '._format_price($old_price).' => '._format_price($price_info['__price_4']),$cur_order['_reffer']) ;
           // пересчитываем заказ
           $orders_system->order_update_sum_price($rec_order['pkey']) ;
           $this->add_element('show_notife',$_POST['code'].': ok') ;
           $flag=1 ;
         }
       } else { $this->add_element('show_notife','Заказ не найден') ; return ; }
       if (!$flag) { $this->add_element('show_notife','Позиция заказа с указаннам штрих-кодом или артикулом не найдена ') ; return ; }


       //damp_array($_POST) ;
       //$this->add_element('show_modal_window','HTML') ;
       if ($_POST['cart_cnt'])
       { foreach($_POST['cart_cnt'] as $reffer=>$cnt) $orders_system->order_update_goods_cnt($_POST['order_id'],$reffer,$cnt,array('no_update_sum_price'=>1)) ;
         $orders_system->order_update_sum_price($_POST['pkey']) ;
         $this->add_element('show_notife','Заказ пересчитан') ;
         $this->add_element('update_page',1) ;
       }
     }

  function add_goods_to_order()
  { global $goods_system,$orders_system ;
    if (!$_POST['new_goods_code']) { $this->add_element('show_notife','Не указан штрих-код') ; return ; }
    $exist_id=execSQL_value('select pkey from '.$orders_system->table_orders_goods.' where parent="'.$_POST['order_id'].'" and code="'.$_POST['new_goods_code'].'"') ;
    if ($exist_id) { $this->add_element('show_notife','Данный товар уже есть в заказе') ; return ; }
    $rec_sort=execSQL_van('select * from '.TM_SORTAMENT.' where code="'.$_POST['new_goods_code'].'" ') ;
    //if ($rec_sort['enabled'])
    if ($rec_sort['pkey'])
    { //$rec_goods=execSQL_van('select * from '.TM_GOODS.' where pkey="'.$rec_sort['goods_id'].'" and enabled=1 and _enabled=1') ;
      $rec_goods=execSQL_van('select * from obj_site_goods where pkey='.$rec_sort['goods_id'].' ') ;
      if ($rec_goods['pkey']) { $rec_goods['_sortament_rec']=$rec_sort ;
                                $goods_system->prepare_public_info($rec_goods) ;
                                 $orders_system->order_add_goods($_POST['order_id'],$rec_goods) ;
                                 $this->add_element('show_notife','Товар успешно добавлен в заказ') ;
                                 $this->add_element('update_page',1) ;
                                }
      else $this->add_element('show_notife','Указанный товар не найден') ;


       //$this->add_element('show_modal_window','HTML',array('title'=>111)) ;
    }  //else if ($rec_sort['pkey']) $this->add_element('show_notife','Указанного сортамента нет в наличии') ;
            else $this->add_element('show_notife','Указанный сортамент не  найден') ;

  }

    function  change_shipping()
      { global $orders_system ;
        //damp_array($_POST) ; $this->add_element('show_modal_window','HTML') ;
        if ($_POST['change_shiping']) {  $res=$orders_system->order_change_shiping($_POST['order_id'],$_POST['change_shiping'],$_POST['price_shiping']) ;
                                         $this->add_element('show_notife','Вариант доставки успешно изменен') ;
                                         $this->add_element('update_page',1) ;
                                       }
        return('ok') ;
      }


    function get_panel_create_comment()
    {
        ?><div id="panel_create_comment"><form>
           <textarea name="text"></textarea>
           <button class="v2" cmd=create_comment script=mod/orders reffer="<?echo $_POST['reffer']?>">Добавить</button>
           <!--<button class="cancel">Отмена</button>-->
          </form></div>
          <style type="text/css">
             div#panel_create_comment{width:100%;}
             div#panel_create_comment textarea{width:95%;margin:10px auto;height:200px;}
          </style>

        <?
        $this->add_element('show_modal_window','HTML',array('title'=>'Добавление комментария к заказу')) ;
    }

    function create_comment()
    {   //damp_array($_POST) ;
        global $member ;
        list($pkey,$tkey)=explode('.',$_POST['reffer']) ;
        adding_rec_to_table('obj_site_orders_comment',array('clss'=>15,'parent'=>$pkey,'obj_name'=>$_POST['text'],'account_reffer'=>$member->reffer)) ;
        //$this->add_element('show_modal_window','HTML',array('title'=>'Добавление комментария к заказу')) ;
        $this->add_element('update_page',1) ;
        _event_reg('Добавление комментария к заказу',$_POST['text'],$_POST['reffer']) ;

    }






}
