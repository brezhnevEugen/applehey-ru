<?php
// КУПОНЫ:
// информация по используемому купону сохраняется в массиве cart->kupon_info[number,value,sales,error]. Для сохранения вызывать функцию  check_kupon(number,pin,summa)

$__functions['init'][]='_orders_site_vars' ;
$__functions['boot_site'][]='_orders_site_boot' ;

function _orders_site_vars() //
{   $_SESSION['TM_orders']          = 'obj_'.SITE_CODE.'_orders' ;          $GLOBALS['TM_orders']=$_SESSION['TM_orders'] ;
    $_SESSION['TM_orders_goods']    = 'obj_'.SITE_CODE.'_orders_goods' ;    $GLOBALS['TM_orders_goods']=$_SESSION['TM_orders_goods'] ;
    $_SESSION['TM_orders_files']    = 'obj_'.SITE_CODE.'_orders_files' ;    $GLOBALS['TM_orders_files']=$_SESSION['TM_orders_files'] ;
    $_SESSION['TM_orders_comment']  = 'obj_'.SITE_CODE.'_orders_comment' ;  $GLOBALS['TM_orders_comment']=$_SESSION['TM_orders_comment'] ;

    $_SESSION['init_options']['orders']['debug']=0 ;
    $_SESSION['init_options']['orders']['no_create_new_on_reboot']=0 ;
    $_SESSION['init_options']['orders']['use_capcha_code']=0 ;
    $_SESSION['init_options']['orders']['use_random_order_number']=0 ;
    $_SESSION['init_options']['orders']['price_free_shiping']=10000 ; // стоимость заказа, после которой доставка бесплатная
    // описание услуг заказа
    // array - название массива вариантов услуги
    // include_price_to_order - включать цену услуги в итоговую цену товара
    // include_to_order - включать услугу в состав заказа
    $_SESSION['init_options']['orders']['service']=array() ;
    $_SESSION['init_options']['orders']['service']['shiping']   =array('name'=>'Доставка','array'=>'IL_type_shiping','default_id'=>1,'include_price_to_order'=>1,'include_to_order'=>1) ;
    //$_SESSION['init_options']['orders']['service']['fast_shiping']   =array('name'=>'Срочная доставка','array'=>'IL_fast_shiping','default_id'=>0,'include_price_to_order'=>1,'include_to_order'=>1) ;
    $_SESSION['init_options']['orders']['service']['payments']  =array('name'=>'Оплата','array'=>'IL_payments_type','default_id'=>1,'include_price_to_order'=>0,'include_to_order'=>0) ;
    //$_SESSION['init_options']['orders']['service']['wake_up']   =array('name'=>'Подьем','array'=>'IL_wake_up','default_id'=>1,'include_price_to_order'=>1,'include_to_order'=>1) ;
    //$_SESSION['init_options']['orders']['service']['assembling']=array('name'=>'Сборка','array'=>'IL_assembling','default_id'=>1,'include_price_to_order'=>1,'include_to_order'=>1) ;

    //пример для штучного товаоа
    $_SESSION['init_options']['orders']['to_cart_count_ue']='шт.' ; // обозначение кол-ва товара
    $_SESSION['init_options']['orders']['to_cart_def_count']=1 ; // значение по умолчанию для занесения в корзину
    $_SESSION['init_options']['orders']['to_cart_price_count']=1 ; // кол-во товара за цену товара
    $_SESSION['init_options']['orders']['to_cart_min_count']=1 ; // минимальное отпускаемое кол-во товара
    $_SESSION['init_options']['orders']['to_cart_count_step']=1 ; // шаг отпуска товара
    // пример для развесного продукта
    //$_SESSION['init_options']['orders']['to_cart_count_ue']='г.' ; // обозначение кол-ва товара - в граммах
    //$_SESSION['init_options']['orders']['to_cart_def_count']=100 ; // значение по умолчанию для занесения в корзину - 100 грамм
    //$_SESSION['init_options']['orders']['to_cart_price_count']=100 ; // кол-во товара за цену товара - цена в товаре за 100 грамм
    //$_SESSION['init_options']['orders']['to_cart_min_count']=50 ; // минимальное отпускаемое кол-во товара - 50 грамм
    //$_SESSION['init_options']['orders']['to_cart_count_step']=25 ; // шаг отпуска товара - 50,75,100,125,150


	// статусы заказов в системе заказов
	$_SESSION['list_status_orders']=array('0'=>'Новый','1'=>'Обработано','2'=>'Готово к доставке','3'=>'Отправлено','4'=>'Доставлено','7'=>'Возврат','8'=>'Возврат/Обмен','9'=>'Обмен','5'=>'Отмена','6'=>'Удалено') ;
	$_SESSION['ARR_orders_payments']=array('0'=>'Не оплачен','1'=>'Внесена предоплата','2'=>'Оплачен') ;
	$_SESSION['ARR_orders_info']=array('firstname'=>'Фамилия','lastname'=>'Имя','email'=>'email') ;

	// откуда узнали про сайт
    //$_SESSION['arr_what'][1]=array('admin'=>'','site'=>'') ;
    //$_SESSION['arr_what'][2]=array('admin'=>'Yandex','site'=>'Yandex') ;
    //$_SESSION['arr_what'][3]=array('admin'=>'Rambler','site'=>'Rambler') ;
    //$_SESSION['arr_what'][4]=array('admin'=>'Google','site'=>'Google') ;
    //$_SESSION['arr_what'][5]=array('admin'=>'рассылка email','site'=>'через рассылку e-mail') ;
    //$_SESSION['arr_what'][6]=array('admin'=>'реклама пресса','site'=>'через рекламу в прессе') ;
    //$_SESSION['arr_what'][7]=array('admin'=>'рассылка почта','site'=>'через почтовую рассылку') ;

    // типы фопмы собственности
    //$_SESSION['arr_mem_type'][1]=array('admin'=>'Физ.лицо','site'=>'Физическое лицо') ;
	//$_SESSION['arr_mem_type'][2]=array('admin'=>'Юр.лицо','site'=>'Юридическое лицо') ;
	//$_SESSION['arr_mem_type'][3]=array('admin'=>'ИП','site'=>'Индивидуальный предприниматель') ;

}

function _orders_site_boot($options)
{
	create_system_modul_obj('orders',$options) ; // объект система заказов
    if (!is_object($_SESSION['cart'])) $_SESSION['cart']=new c_cart ; // создаем объект корзина
}

class c_orders_system extends c_system
{
  public $table_orders ; // таблица заказаов
  public $table_orders_goods ; // таблица ссылок на товары в заказах
  public $table_orders_files ; // таблица ссылок на товары в заказах
  public $table_mails ; // таблица шаблонов писем
  public $tkey_table_orders ; // таблица заказаов
  public $tkey_table_orders_goods ; // таблица ссылок на товары в заказах
  public $tkey_table_orders_files ; // таблица ссылок на товары в заказах
  public $use_capcha_code ; //
  public $support_files ; //  1 если можно ззагружать файлы в заказ
  public $use_random_order_number ; //  1 если номер заказа должен генерироваться случайным образом
  public $include_price_shiping_to_order ; //  1 если стоимость доставки входит в сумму заказа
  public $include_shiping_to_order ; //  1 если доставку надо указывать в заказе
  public $include_payments_to_order ; //  1 если доставку надо указывать в заказе
  public $price_free_shiping=100000000000000 ; //  стоимость заказа, после которого доставка бесплатная
  public $default_cart_service=array() ;

 // создание системы заказов
 function c_orders_system($options=array())
   { if (!$options['table_name']) $options['table_name']=$_SESSION['TM_orders'] ;
     parent::c_system($options) ;
     // инициализация переменных
     $this->table_orders=($options['table_orders'])? $options['table_orders']:$_SESSION['TM_orders'] ;
     $this->tkey_table_orders=$_SESSION['pkey_by_table'][$this->table_orders] ;
     $this->table_orders_goods=($options['table_orders_goods'])? $options['table_orders_goods']:$_SESSION['TM_orders_goods'] ;
     $this->table_orders_files=($options['table_orders_files'])? $options['table_orders_files']:$_SESSION['TM_orders_files'] ;
     $this->tkey_table_orders_goods=$_SESSION['pkey_by_table'][$this->table_orders_goods] ;
     $this->tkey_table_orders_files=$_SESSION['pkey_by_table'][$this->table_orders_files] ;
     $this->support_files=($this->tkey_table_orders_files)? 1:0;
     $this->to_cart_count_ue=($options['to_cart_count_ue'])? $options['to_cart_count_ue']:'шт.' ;
     $this->to_cart_def_count=($options['to_cart_def_count'])? $options['to_cart_def_count']:1 ;
     $this->to_cart_price_count=($options['to_cart_price_count'])? $options['to_cart_price_count']:1 ;
     $this->to_cart_min_count=($options['to_cart_min_count'])? $options['to_cart_min_count']:1 ;  //
     $this->to_cart_count_step=($options['to_cart_count_step'])? $options['to_cart_count_step']:1 ;
     $this->use_capcha_code=($options['use_capcha_code'])? $options['use_capcha_code']:0 ;
     $this->use_random_order_number=$options['use_random_order_number'] ;
     $this->include_price_shiping_to_order=$options['include_price_shiping_to_order'] ;
     $this->include_shiping_to_order=$options['include_shiping_to_order'] ; if (!$this->include_shiping_to_order) $this->include_price_shiping_to_order=0 ;
     $this->include_payments_to_order=$options['include_payments_to_order'] ;
     $this->price_free_shiping=$options['price_free_shiping'] ;
     $this->service=$options['service'] ;
     $this->pages_info=array() ;
       //damp_array($this) ;



    //если корзины клиента еще нет, создаем корзину
    if (!is_object($_SESSION['cart'])) { $_SESSION['cart']=new c_cart() ; $GLOBALS['cart']=&$_SESSION['cart'] ; }

     // старый параметр, оставлн по для совместимости
     if ($options['to_cart_def_value']) $this->to_cart_def_count=$options['to_cart_def_value'] ;

    REPORTS()->reg_reports2($this,'Заказы','orders/report_orders.php') ;         //
 }

 function declare_menu_items_1($page)
  {  $page->menu['/cab/reports/orders/']=array('title'=>'Заказы') ;
  }
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 //
 // функции для работы с БД
 //
 //-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    function get_count($usl_select='')
    { $sql='select count(pkey) as cnt from '.$this->table_name.' where clss=82 ' ;
      if ($usl_select) $sql.=' and '.$usl_select ;
      $cnt=execSQL_value($sql) ;
      return($cnt) ;
    }

    function get_count_by_status($usl_select='')
    { $sql='select status,count(pkey) as cnt from '.$this->table_name.' where clss=82 ' ;
      if ($usl_select) $sql.=' and '.$usl_select ;
      $sql.=' group by status' ;
      $cnt_arr=execSQL_row($sql) ;
      return($cnt_arr) ;
    }

    function get_count_by_field($fname,$usl_select='')
    { $sql='select '.$fname.',count(pkey) as cnt from '.$this->table_name.' where clss=82 ' ;
      if ($usl_select) $sql.=' and '.$usl_select ;
      $sql.=' group by '.$fname ;
      $cnt_arr=execSQL_row($sql) ;
      return($cnt_arr) ;
    }

 // $orders_stats=$orders_system->get_orders_stats(array('account_reffer'=>$member->reffer)) ;
 function get_orders_stats($options=array())
 { $arr=array() ;  $usl='' ;
   if (sizeof($options)) foreach($options as $name=>$value) $arr[]=$name.'="'.addslashes($value).'"' ;
   if (sizeof($arr)) $usl=' where '.implode(' and ',$arr) ;
   $recs=execSQL_row('select status,sum(end_price) from '.$this->table_name.$usl.' group by status order by status') ;
   return($recs) ;
 }


 // возвращаем заказ
 // $options['goods_full_info'] : вывести товар заказа только как в корзине или полная информация из базы
 // $options['account_reffer'] : получение заказа только указанного пользователя
 function get_order($id,$options=array())
 {  $order=execSQL_van('select * from '.$this->table_name.' where clss=82 and enabled=1 and pkey='.$id) ;
    if ($options['account_reffer'] and $options['account_reffer']!=$order['account_reffer']) return(prepare_result('access_denited')) ;
    if (!$order['pkey']) return(prepare_result('order_not_found'));
    // получаем дуполнительную инфу по товару
    if ($order['info']) $order['info']=unserialize($order['info']) ;
    $order['goods']=$this->get_orders_goods_full_info($id);
      $order['service']=$this->get_orders_service_full_info($id) ;
    if ($this->support_files) $order['files']=$this->get_orders_files($id) ;
    return($order)  ;
 }

 function prepare_public_info(&$rec)
 {  //if ($rec['name']) $arr[]=$rec['name'] ;
    //if ($rec['forname']) $arr[]=$rec['forname'] ;
    //if ($rec['lastname']) $arr[]=$rec['lastname'] ;

 }

 // возвращаем заказ
 // $options['goods_full_info'] : вывести товар заказа только как в корзине или полная информация из базы
 function get_order_by_uid($uid,$options=array())
 {  $order=execSQL_van('select * from '.$this->table_name.' where clss=82 and enabled=1 and uid="'.$uid.'"') ;
    if ($order['pkey'])
     { $order['goods']=$this->get_orders_goods_full_info($order['pkey']) ;
       $order['service']=$this->get_orders_service_full_info($order['pkey']) ;
     }
    else $order=prepare_result('order_not_found') ;
    return($order)  ;
 }

 function get_orders_goods($id) { return($this->get_orders_goods_full_info($id))  ;}

 // возвращает массив с товарами заказа (все свойства + фото)
 // в массиве __reffer_from - массив записи в корзине по товару
 //  __reffer_from[price_info] - уже разобрано в массив
 function get_orders_goods_full_info($id)
 {  $recs=select_reffers_objs($this->tkey_table_orders_goods,'parent='.$id.' and reffer RLIKE "[0-9].[0-9]" ',array('no_pkey_indx'=>1,'no_group_tkey_clss'=>1,'order'=>'obj_name')) ;
    $result=array() ; $_result=array() ;
    if (sizeof($recs)) foreach($recs as $rec)
     { $_SESSION['goods_system']->prepare_public_info($rec) ;
       if ($rec['__reffer_from']['price_info']) $rec['__reffer_from']['price_info']=unserialize($rec['__reffer_from']['price_info']) ;
       // если в заказе есть reffer сортамента и используется подсистема 1C, получаем информацию по сортаменту и заменяем изображдение товара изображением сортамента

       if ($rec['__reffer_from']['sortament_reffer'] and is_object($_SESSION['1c_system']))
       { $rec['_sortament_rec']=$_SESSION['1c_system']->get_sortament_rec($rec['__reffer_from']['sortament_reffer']) ;
         if ($rec['_sortament_rec']['_image_name'])  $rec['_image_name']=$rec['_sortament_rec']['_image_name'] ;
         if ($rec['_sortament_rec']['_image_tkey'])  $rec['_image_tkey']=$rec['_sortament_rec']['_image_tkey'] ;
         }
       $_result[$rec['parent']][]=$rec ;
     }
    //damp_array($_result) ;
    if (sizeof($_result)) foreach($_result as $arr_to_parent) $result=array_merge($result,$arr_to_parent) ;
    return($result) ;
 }

 // возвращает массив с услугами заказа (все свойства + фото)
 function get_orders_service_full_info($id)
 {  $service=execSQL('select reffer,obj_name,price,cnt,price_info from '.$this->table_orders_goods.' where parent='.$id.' and reffer NOT RLIKE "[0-9].[0-9]" order by pkey') ;
    if (sizeof($service)) foreach($service as $id=>$rec)
        if ($rec['price_info']) $service[$id]['price_info']=unserialize($rec['price_info']) ;
    return($service) ;
 }

 // возвращает массив с услугами заказа (все свойства + фото)
 function get_orders_files($id)
 {  $files=execSQL('select * from '.$this->table_orders_files.' where parent='.$id.' order by pkey') ;
    if (sizeof($files)) foreach($files as $id=>$rec) $files[$id]['__href']=_DOT($this->table_orders_files)->patch_to_file.$rec['file_name'] ;
    //damp_array($files) ;
    //damp_array(_DOT($this->table_orders_files)) ;
    return($files) ;
 }

 function get_comment_order($id)
 {  global $TM_orders_comment ;
    $arr=execSQL('select * from '.$TM_orders_comment.' where parent='.$id.' order by c_data') ;
    return($arr)  ;
 }


 function unsubscribed($hash)
 { $emails=$this->get_subscript_emails() ;
   if (sizeof($emails)) foreach($emails as $email) if (md5($email)==$hash)
   { update_rec_in_table($this->table_name,array('subscript'=>0),'email="'.$email.'"') ;
     _event_reg('Отказ от подписки',$email) ;
   }
 }

 function get_subscript_emails()
 {  $emails=execSQL_line('select distinct(email) from '.$this->table_name.' where subscript=1 and email!=""') ;
    return($emails)  ;
 }

 function get_subscript_count()
 {  $cnt=execSQL_value('select count(distinct(email)) as cnt from '.$this->table_name.' where subscript=1 and email!=""') ;
    return($cnt)  ;
 }

 function _get_orders_goods($id)
 {  $goods=execSQL('select reffer,obj_name,price,cnt,sales_type,sales_value,price_info from '.$this->table_name.' where parent='.$id.' order by pkey') ;
    return($goods)  ;
 }

// возвращает массив первых букв активированных аккаунтов для текущего аккаунта
  function get_clients_chars($fname='obj_name')
  { $_usl=array() ;
    $_usl[]='clss=82';
    $usl=implode(' and ',$_usl) ;
    $sql='select distinct(ORD(UPPER('.$fname.'))) as code from '.$this->table_name.' where '.$usl.' order by obj_name' ;
    $res=execSQL_line($sql) ;
	return($res) ;
  }

 //-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 //
 // КОРЗИНА ЗАКАЗА
 //
 //-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 // проверяем, находиться ли товар в корзине
 function in_cart($reffer)
 {
   return(isset($_SESSION['cart']->goods[$reffer])) ;
 }

 function add_to_cart($use_cart,$reffer,$cnt=-1,$options=array()) // code - штрих код сортамента
  { global $goods_system ;
    $goods_cart_indx=($options['sortament_reffer'])?  $reffer.'.'.$options['sortament_reffer']:$reffer ;
    if (!isset($use_cart->goods[$goods_cart_indx])) // защита от перетирания количества уже ранее добавленного товара
          {     $count=($cnt>0)? $cnt:$this->to_cart_def_count ;
                $arr=explode('.',$reffer) ;
                $goods_reffer=$arr[0].'.'.$arr[1] ;
                $obj_info=get_obj_info($goods_reffer,array('default_tkey'=>$goods_system->tkey)) ;
                $goods_system->prepare_public_info($obj_info) ;
                $obj_info['cart_reffer']=$goods_cart_indx ;
                if (is_object($_SESSION['1c_system'])) // если используется 1с сортамент
                { // если указан определенный сортамент
                  if ($options['sortament_reffer']) $obj_info['_sortament_rec']=$_SESSION['1c_system']->get_sortament_rec($options['sortament_reffer']) ;
                  if ($obj_info['_sortament_rec']['_image_name'])  $obj_info['_image_name']=$obj_info['_sortament_rec']['_image_name'] ;
                  if ($obj_info['_sortament_rec']['_image_tkey'])  $obj_info['_image_tkey']=$obj_info['_sortament_rec']['_image_tkey'] ;
                }
                $this->cart_add_goods($use_cart,$obj_info,$count) ;
          }
     return($goods_cart_indx) ;
  }

 // добавление товара в корзину
 function cart_add_goods($use_cart,$goods_info=array(),$count=1)
  { if (!$goods_info['__cart_cnt']) $goods_info['__cart_cnt']=$count ;
    unset($goods_info['obj_clss_3'],$goods_info['obj_clss_5']) ;
    if (isset($goods_info['_sortament_rec'])) $goods_info['__price_3']=$goods_info['_sortament_rec']['price'] ;
    if ($goods_info['cart_reffer']) $use_cart->goods[$goods_info['cart_reffer']]=$goods_info ; // специально формируемый код когда необходимо несколько раз добавить в корзину один товар
    else if ($goods_info['_reffer']) $use_cart->goods[$goods_info['_reffer']]=$goods_info ;

    $this->cart_update_info($use_cart) ;
  }


  function del_from_cart($use_cart,$id)
  {
    if (is_object($use_cart)) { unset($use_cart->goods[$id]) ; $this->cart_update_info($use_cart) ; }
  }

  // изменяем число товара в корзине
  function cart_update_cnt($use_cart,$param,$cnt=0)
  {
    if (!is_array($param))
    {
      if (!$cnt) unset($use_cart->goods[$param]) ;
      else $use_cart->goods[$param]['__cart_cnt']=$cnt ;
    }
    else if (sizeof($param))
    {  foreach($param as $id=>$cnt)
          if (!$cnt) unset($use_cart->goods[$id]) ;
          else       $use_cart->goods[$id]['__cart_cnt']=$cnt ;
    }
  }

  // обновлени информации по корзине
  function cart_update_info($use_cart='',$options=array())
   {    if (!is_object($use_cart)) $use_cart=&$_SESSION['cart'] ;
        // обновляем дефолтную информацию в корзине
        $this->check_cart_default_values($use_cart) ;
        // получаем суммарные значения по корзине
        list($sum_price,$sales_goods,$sum_count)=$this->get_goods_sum_price_sales_count($use_cart) ;
        /*
        if ($sum_price>=5000)
        { if (sizeof($use_cart->goods)) foreach($use_cart->goods as $id=>$rec)
            { $use_cart->goods[$id]['sales_type']=4 ;
              $use_cart->goods[$id]['sales_value']=$rec['price']-$rec['price_opt'] ;
              $_SESSION['goods_system']->prepare_public_info($use_cart->goods[$id]) ;
            }
        }
        else if (sizeof($use_cart->goods)) foreach($use_cart->goods as $id=>$rec)
            { $use_cart->goods[$id]['sales_type']=0 ;
              $use_cart->goods[$id]['sales_value']=0;
              $_SESSION['goods_system']->prepare_public_info($use_cart->goods[$id]) ;
            }
         */
        list($sum_price,$sales_goods,$sum_count)=$this->get_goods_sum_price_sales_count($use_cart) ;

		// вносим значения в массив
        $use_cart->result_price['goods']=$sum_price ;
        $use_cart->result_price['sales_goods']=-$sales_goods ;
        $use_cart->result_price['sales_member']=-$this->get_sales_by_member($use_cart) ; // ;
        $use_cart->result_price['sales_action']=-$this->get_sales_by_action($use_cart) ; // ;
        $use_cart->sum_count=$sum_count ; // общее число товаров в корзине
        $use_cart->all_count=sizeof($use_cart->goods) ;    // число позиций товара в корзине

        // считаем суммарную цену заказа без услуг, но со скидками
        $use_cart->sum_price=$use_cart->result_price['goods']+$use_cart->result_price['sales_goods']+$use_cart->result_price['sales_member']+$use_cart->result_price['sales_action'] ;  // суммируем массив сметы заказа - без услуг

        // обновляем стоимость услуг на основе уже известной суммы заказа
        $this->update_price_service($use_cart) ;

        // переносим цены услуг в общую смету заказа
        $this->copy_price_from_service_to_result_price() ;

        // расчет итоговой цены заказа
        $this->update_order_end_price($use_cart) ;

        // поля ниже оставлены пока для совместимости, так как используются в шаблонах корзины и оформления заказа
        $this->compatibility_vars($use_cart) ;

        $use_cart->ue=get_case_by_count_1_2_5($use_cart->sum_count,'товар,товара,товаров') ;
   }


    function compatibility_vars($use_cart)
    { if (!is_object($use_cart)) $use_cart=&$_SESSION['cart'] ;

      $use_cart->sales_goods=abs($use_cart->result_price['sales_goods']);
      $use_cart->sales_member=abs($use_cart->result_price['sales_member']);

      $use_cart->price_shiping=$use_cart->result_price['shiping'] ;
      $use_cart->type_shiping=$use_cart->service['shiping']['id'] ;
      $use_cart->text_shiping=$use_cart->service['shiping']['name'] ;

      $use_cart->payments_type=$use_cart->service['payments']['id'] ;


    }

    // вернуть скидку на товар по акции
    function get_sales_by_action($use_cart)
    { $sales_value=0 ;
      // пример назначения скидки при амовывозе скидка на товар - 5%
      //if ($use_cart->type_shiping==4) $sales_value=$use_cart->result_price['goods']/100*$_SESSION['LS_dostavka_sales'] ;
      // пример подчесчета оптовой цены при превышении минимального лимита корзины
      /*if ($use_cart->result_price['goods']>5000)
      {  // добавляем в каждый товар

      }*/
      return($sales_value) ;
    }

    // вернуть скидку клиента на заказ
    function get_sales_by_member($use_cart)
    {
      return(0) ;
    }


 //-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 //
 // ТОВАРЫ ЗАКАЗА
 //
 //-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 // получаем общую цену товара в корзине и общую скидку
 // считаем: sum_count,sum_price,sales_goods
  function get_goods_sum_price_sales_count($use_cart='')
    { if (!is_object($use_cart)) $use_cart=&$_SESSION['cart'] ;
      $sum_count=0 ; $sum_price=0 ; $sales_goods=0 ;
      if (sizeof($use_cart->goods)) foreach($use_cart->goods as $i=>$rec)
		{ // в корзине уже обработанная информация по товару
          $goods_sum_price=$this->exec_price_by_count($use_cart->goods[$i])  ; // выносим расчет цены в отдельную функцию чтобы можно было использовать различные цены при разных количествах

          // скидка на товар будет вычитаться от общекй стоимости товара
          if ($rec['__price_used_sales_value']) $sales_goods+=$rec['__price_used_sales_value']/$this->to_cart_price_count*$rec['__cart_cnt'] ; // цена отдельного товара в корзине в главной валюте * количество

          $sum_count+=$rec['__cart_cnt'] ;     // количество товара
          $sum_price+=$goods_sum_price ; // сумма (число*стоимость) без скидки клиента, и без скидки на товары

		  // считаем партнерский процент отдельно по каждому товару
		  // незвисимо от параметров скидок на товар партнер должен получить свои проценты за вычетом процента кклиента
          //if (is_object($_SESSION['parther_system'])) $_SESSION['parther_system']->set_part_proc_to_goods($use_cart,$goods_sum_price) ;
		}
      //$use_cart->partner_premia=0 ; $use_cart->partner_premia_info='' ; $use_cart->partner_premia_info=$member->info['part_proc'].'/'.$member->info['sales'].': ' ;
      return(array($sum_price,$sales_goods,$sum_count)) ;
    }

  // расчет суммы цены товара по его количеству
  function exec_price_by_count(&$rec)
  {   $price=$rec['__price_3'] ;
      /*if ($rec['cnt_var'])
      { $rec_cnt=$_SESSION['IL_cnt'][$rec['cnt_var']] ; // damp_array($rec_cnt) ;
        // определяем цену на товар в зависимости от кол-ва товара
        if ($rec['price_1']>0 and $rec_cnt['cnt1']<=$rec['__cart_cnt'])  $price=$rec['price_1'] ;
        if ($rec['price_2']>0 and $rec_cnt['cnt2']<=$rec['__cart_cnt'])  $price=$rec['price_2'] ;
        if ($rec['price_3']>0 and $rec_cnt['cnt3']<=$rec['__cart_cnt'])  $price=$rec['price_3'] ;
        if ($rec['price_4']>0 and $rec_cnt['cnt4']<=$rec['__cart_cnt'])  $price=$rec['price_4'] ;
        if ($rec['price_5']>0 and $rec_cnt['cnt5']<=$rec['__cart_cnt'])  $price=$rec['price_5'] ;
      }*/
      $rec['__cart_price']=$price ;
      $res=$price/$this->to_cart_price_count*$rec['__cart_cnt'] ; // цена отдельного товара в корзине в главной валюте * количество



     return($res) ;
  }


 //-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 //
 // УСЛУГИ ЗАКАЗА
 //
 //-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 // сохраняем в текущей корзине тип доставки, автоматически получаем название доставки и стоимость
 function set_type_shiping($type,$use_cart='',$options=array())
 { if (!$use_cart) $use_cart=$_SESSION['cart'] ;
   if (!$type) $type=1 ;
   $this->set_service_in_cart('shiping',$type,$use_cart) ;

 }

 // обновляем стоимость услуг в result_price на основе уже известной суммы заказа
 function update_price_service($use_cart='')
 { if (!is_object($use_cart)) $use_cart=&$_SESSION['cart'] ;
   //************************************************
   // Правило: если сумма заказа >  price_free_shiping - доставка бесплатно
   //************************************************
   //if ($use_cart->result_price['goods']>=$this->price_free_shiping) // если сумма заказа больше порога - доставка бесплатно  - c учетом первоначальной цены товара
  /*
   if ($use_cart->sum_price>=20000) // если сумма заказа больше порога - доставка бесплатно - с утетом цены товара со скидками
   {   $use_cart->service['shiping']['__price_used_sales_value']=$use_cart->service['shiping']['__price'] ;
       //$use_cart->service['shiping']['__price_used_sales_text']='Доставка заказов от '._format_price($this->price_free_shiping).' - бесплатно.' ;
       $use_cart->service['shiping']['__price_used_sales_text']='Доставка заказов более 20000 - бесплатно.' ;
       $use_cart->service['shiping']['price']=0 ;
   }
   elseif ($use_cart->sum_price>=10000) // если сумма заказа больше порога - доставка бесплатно - с утетом цены товара со скидками
   {   $use_cart->service['shiping']['__price_used_sales_value']=200 ;
       $use_cart->service['shiping']['__price_used_sales_text']='Доставка заказов от 10 тыс до 20 тыс - 400 руб.' ;
       $use_cart->service['shiping']['price']=400 ;
   }
   else  // иначе - обновляем запись по доставке       */
   { $cur_id=$use_cart->service['shiping']['id'] ;
     $this->set_service_in_cart('shiping',$cur_id,$use_cart,array('no_cart_update_info'=>1)) ; // no_cart_update_info - чтобы не было зацикливания
   }
   //************************************************
   // Правило: если сумма заказа >  price_free_shiping - сборка бесплатно
   //************************************************
   //if ($use_cart->result_price['goods']>=$this->price_free_shiping) // если сумма заказа больше порога - доставка бесплатно  - c учетом первоначальной цены товара
   /*
   if ($use_cart->sum_price>=$this->price_free_shiping) // если сумма заказа больше порога - доставка бесплатно - с утетом цены товара со скидками
   {   $use_cart->service['assembling']['__price_used_sales_value']=$use_cart->service['assembling']['__price'] ;
       $use_cart->service['assembling']['__price_used_sales_text']='Сборка при заказе от '._format_price($this->price_free_shiping).' - бесплатно' ;
       $use_cart->service['assembling']['price']=0 ;
   }
   else  // иначе - обновляем запись по доставке
   { $cur_id=$use_cart->service['assembling']['id'] ;
     $this->set_service_in_cart('assembling',$cur_id,$use_cart,array('no_cart_update_info'=>1)) ; // no_cart_update_info - чтобы не было зацикливания
   }
   */
 }

 // расчет итоговой цеы заказа - добавляем к стоимости товаров (уже со скидкой) стоимость услуг
 function update_order_end_price($use_cart)
 {  if (!$use_cart) $use_cart=$_SESSION['cart'] ;
    // первоначально стоимость заказа = цена товара со скидкой
    $use_cart->end_price=$use_cart->sum_price;
    // потом включаем в стоимость все остальные услуги по заказу
    if (sizeof($this->service)) foreach($this->service as $code=>$rec)
        if ($rec['include_price_to_order'] and isset($use_cart->service[$code]))  $use_cart->end_price+=$use_cart->service[$code]['price'] ;
 }

 // устанавливаем значение услуги в корзине
 function set_service_in_cart($code,$value,$use_cart='',$options=array())
 { if (!$use_cart) $use_cart=$_SESSION['cart'] ;
   if (isset($this->service[$code]))
     {  $service_params=$this->service[$code] ;
        $arr=$_SESSION[$service_params['array']] ;
        $rec=$arr[$value] ;
        $use_cart->add_service($code,$rec) ;
        if (!$options['no_cart_update_info']) $this->cart_update_info($use_cart) ;
     }
 }

 // удаляем услуга из корзины
 function remove_service_from_cart($code,$use_cart='',$options=array())
 { if (!$use_cart) $use_cart=$_SESSION['cart'] ;
   if (isset($this->service[$code]))
     {  $use_cart->remove_service($code) ;
        if (!$options['no_cart_update_info']) $this->cart_update_info($use_cart) ;
     }
 }


 // копируем цены из массива $cart->service в $cart->result_price
 function copy_price_from_service_to_result_price($use_cart='')
 { if (!is_object($use_cart)) $use_cart=&$_SESSION['cart'] ;
   if (sizeof($use_cart->service)) foreach($use_cart->service as $id=>$rec) $use_cart->result_price[$id]=$rec['price'] ;
 }

 // проверка что в корзину внесены все услуги по умолчанию
 function check_cart_default_values($use_cart='')
 { if (!is_object($use_cart)) $use_cart=&$_SESSION['cart'] ;
    //damp_array($this->service) ;
    // сервисы заданные в настройках модуля
    if ($this->service)  foreach($this->service as $code=>$info) if (!isset($use_cart->service[$code]) and $info['default_id'])
    {  if (sizeof($_SESSION[$info['array']]))
      { $rec=$_SESSION[$info['array']][$info['default_id']] ;
        $use_cart->add_service($code,$rec) ;
      } else if (_IS_SYSTEM_IP) echo '<div class=alert>Для услуги заказа "'.$code.'" заданный массив '.$info['array'].' не задан. Проверьте правильность указания массива в настройках модуля или существования списка</div>' ;
    }
 }

 //-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 //
 // КУПОНЫ
 //
 //-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------


 // проверка купона - если купон настроящий, вернуть сумму скидки, скидку по купону (текст), предупреждение, если купон не действительный
 function check_kupon($number,$pin,$summa)
 { $alert='' ; $sales_summa=0 ; $sales_kupon=0 ;
   $rec=execSQL_van('select * from obj_site_kupons where obj_name like "'.trim($number).'" and clss=83') ;
   if (!$rec['pkey'])                   $alert='number' ;
   elseif ($rec['pin']!=$pin)           $alert='pin' ;
   elseif ($rec['data_1']>time())       $alert='data_1' ;
   elseif ($rec['data_2']<time())       $alert='data_2' ;
   elseif ($rec['order_reffer'])        $alert='used' ;
   else                                 { $sales_summa=round($summa*$rec['sales']/100) ;
                                          $sales_kupon=$rec['sales'] ;
                                          //  учет купона в общей сумме заказа,
                                          //  на основании этой записи будет добавлена информация по купону в смету заказа
                                          //  а после оформления заказа погашен купон
                                          $_SESSION['cart']->result_price['sales_kupon']=$sales_kupon ;
                                        }
   $_SESSION['cart']->kupon_info['number']=trim($number) ;
   $_SESSION['cart']->kupon_info['value']=$sales_summa ;
   $_SESSION['cart']->kupon_info['sales']=$sales_kupon ;
   $_SESSION['cart']->kupon_info['alert']=$alert ;



   return($_SESSION['cart']->kupon_info) ;
 }


 function used_kupon($number,$order_reffer)
 {
   update_rec_in_table('obj_site_kupons',array('order_reffer'=>$order_reffer),'obj_name like "'.$number.'"') ;
 }

 //-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 //
 // РЕГИСТРАЦИЯ ЗАКАЗА
 //
 //-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 // старый формат вызова создания заказа - оставляем пока для совместимости
 function order_create($use_cart,$options=array(),$files=array())
 { if (sizeof($files)) $options['files']=$files ;
   $this->order_create_by_data($use_cart,$options,$options=array()) ;
 }

 // новый формат создания заказа
 // данные для сохранения уже должны быть подготовлены в вызывающей функции
 // в поле info - данные для сериализации
 function order_create_by_data($use_cart,$order,$options=array())
   { global $member ; //damp_array($order) ;
     // проверяем правильность проверочного кода
     if (!$options['no_check_code'] and ($this->use_capcha_code or $options['use_check_code'])) if (!capcha_code_checkit()) return(prepare_result('error_check_code')) ;
     if (!sizeof($use_cart->goods)) return(prepare_result('space_cart')) ;
     $sales_kupon=0 ; $sales_summa=0 ;
     //if ($_POST['kupon']['number']) list($sales_summa,$sales_kupon,$alert)=$this->check_kupon($_POST['kupon']['number'],$_POST['kupon']['pin'],$use_cart->end_price) ;
     $end_price=$use_cart->end_price-$sales_summa ;
	 if ($use_cart->all_count and sizeof($order)) // если существует корзина клиента и заказ не надо открывать
   		 {  // заполняем обязательные поля заказа
            if (!isset($order['parent']))    $order['parent']=1 ;
            if (!isset($order['clss']))      $order['clss']=82 ;   // 82 - заказ
            if (!isset($order['status']))    $order['status']=0 ;   // 0 - новый заказ
            if (!isset($order['enabled']))   $order['enabled']=1 ;
            if (!isset($order['uid']))       $order['uid']=md5(uniqid(rand(),true)) ;
            if (!isset($order['end_price'])) $order['end_price']=$end_price ;  // цена товара в корзине со скидкой клиента и скидкой по купону + стоимость услуг
            if (!isset($order['referer_host'])) $order['referer_host']=$_COOKIE['referer_host'] ; // адрес, с которого клиент зашел на сайт
            if ($member->id and !isset($order['account_reffer'])) $order['account_reffer']=$member->reffer;
             /*
             global $partners_system,$partner_uid ;
             if (isset($partners_system) and $partner_uid)
             { 	$partner_info=$partners_system->get_partner_info_by_uid($partner_uid) ;
                if ($partner_info['pkey'])
             	{ 	$finfo['partner_reff']=$partner_info['_reffer'] ;
                	$finfo['partner_uid']=$partner_uid ;
                	//$prem=$finfo['end_price']-$finfo['price_dost'] ;
                	//echo 'премия='.$prem.'<br>' ;
                	//$finfo['partner_premia']=round($prem/100*$partner_info['part_proc'],2) ; // округдяем до копеек
                	$finfo['partner_premia']=$use_cart->partner_premia ; // премия уже посчитана в корзине
                	$finfo['partner_premia_info']=$use_cart->partner_premia_info ; // обоснование расчета премии
                	//damp_array($finfo) ;
                }
             } */

		   	 //damp_array($finfo) ;
             $order['id']=adding_rec_to_table($this->tkey_table_orders,$order) ; // создаем заказ
             $order['_reffer']=$order['id'].'.'.$this->tkey_table_orders ;
             $order_number=($this->use_random_order_number)? rand(1,10000000):$order['id'] ;
             $order['obj_name']='№ '.$order_number." от ".date("d.m.y G:i",time()) ;

		     // команда корзине скопировать товар в смету заказа (включая информацию по скидкам на товар)   - ??? зачем
             //$use_cart->append_goods_to_order($order['id'],$use_cart) ;
             if (sizeof($use_cart->goods)) foreach($use_cart->goods as $rec) $this->order_append_to_smeta($order['id'],$rec) ;

             // также сюда добавляется информация по доставке, скидкам, купонам
             $this->order_append_service($order['id'],$use_cart) ;

             // добавляем файлы
             if (sizeof($options['files'])) foreach($options['files'] as $rec_file) if ($rec_file['tmp_name']) $this->order_append_file($order['id'],$rec_file) ;

		     // удаляем корзину клиента
             //damp_array($use_cart) ;
             $use_cart->goods=array() ;
             $this->cart_update_info() ;
		     //$use_cart->destroy() ;

		     // обновляем название заказа
		   	 $this->update_order_info($order['id'],array('obj_name'=>$order['obj_name'])) ;

             //if ($_SESSION['cart']->result_price['sales_kupon']) $this->used_kupon($_POST['kupon']['number'],$order['_reffer']) ;

		     // регистрируем событие 'Создание заказа'
		     //$evt_id=_event_reg('Создание заказа','',$order['_reffer'],$partner_info['_reffer']) ; // account_info['_reffer']
		     $evt_id=_event_reg('Заказ: cоздание','',$order['_reffer']) ; // account_info['_reffer']

             // загружаем из базы сохраненный заказ
             $order_info=$this->get_order($order['id']) ;
             $order_info['id']=$order_info['pkey'] ; // временно для совместимости

		     // посылаем уведомление о создании заказа клиенту
		     $this->send_mail_create_order($order_info,array('use_event_id'=>$evt_id)) ;
             // посылаем уведомление о создании заказа админу
		     $this->send_mail_create_order_to_admin($order_info,array('use_event_id'=>$evt_id)) ;


             // отрабатываем событие "создание заказа"
             $this->event_order_on_create($order_info,array('use_event_id'=>$evt_id)) ;
		     //if (isset($partners_system) and $partner_uid and $partner_info['pkey']) $partners_system->send_mail_create_order($order['id'],$partner_info,array('use_event_id'=>$evt_id)) ;


             $result['code']='order_create_success' ;
             $result['type']='success' ;
             $result['rec']=$order_info ;

             return($result) ;

		     // если подключена система аккаунтов, то добавляем клиента в таблицу клиентов
		     // в принципе, это надо сделать до регистрации события "создания заказа" чтобы вписать reffer в линки события
		     //if (isset($_SESSION['account_system']))
		     //{ $account_reffer=$_SESSION['account_system']->get_account_reffer_by_email($finfo['email']) ;
		     //  if (!$account_reffer) $account_reffer=$_SESSION['account_system']->add_account_by_order($this->new_order_reffer) ;
		     //  update_rec_in_table($this->tkey,array('account_reffer'=>$account_reffer),'pkey='.$this->new_order_id) ;
             //
		     //}

		 }

   }

  function update_order_info($id,$rec=array())
  {
     update_rec_in_table($this->tkey_table_orders,$rec,'pkey='.$id) ;
  }


  // добавляем товары в смету заказа
  function order_append_to_smeta($order_id,$rec,$options=array())
  {
     // извлекаем из описания товара всю информацию по ценам
     $price_info=array() ;
     if (sizeof($rec)) foreach($rec as $name=>$value) if (strpos($name,'__price')!==false) $price_info[$name]=$value ;
     if (!sizeof($price_info) and $rec['price']) $price_info['__price']=$rec['price'] ;
     if ($rec['price_source']) $price_info['price_source']=$rec['price_source'] ;
     if ($rec['price_source']>$rec['price'])  $price_info['sales_size']=100-($rec['price']*100/$rec['price_source']) ;

     $str_price_info=(sizeof($price_info))? serialize($price_info):'' ;
     $name=$rec['__name'] ; if (!$name) $name=$rec['obj_name'] ; if (!$name) $name=$rec['name'] ;
     $reffer=$rec['_reffer'] ; if (!$reffer) $reffer=$rec['kod'] ; ;
     $price=$rec['__price_3'] ; if (!$price) $price=$rec['price'] ;
     $cnt=$rec['__cart_cnt'] ; if (!$cnt) $cnt=$rec['cnt'] ; if (!$cnt) $cnt=1 ;
     $finfo=array(	'parent'		=>	$order_id,
                    'clss'			=>	120,   //
                    'enabled'		=>	1,
                    'obj_name' 		=> 	$name,
                    'reffer'		=>	$reffer,
                    'price'			=>	$price,  // сохраняем в заказе цену в главной валюте сайта
                    'cnt'			=>	$cnt,
                    'sales_type'	=>	($rec['sales_type'])? $rec['sales_type']:0, // цена товара в корзине со скидкой клиента
                    'sales_value'	=>  ($rec['sales_value'])? $rec['sales_value']:0,
                    'price_info'    =>   $str_price_info
                 ) ;
      if (isset($rec['_sortament_rec']))
      { $finfo['sortament_reffer']=$rec['_sortament_rec']['_reffer'] ;
        $finfo['code']=$rec['_sortament_rec']['code'] ;
        $finfo['img_id']=$rec['_sortament_rec']['img_id'] ;
      }
     //damp_array($finfo) ;
     adding_rec_to_table($this->tkey_table_orders_goods,$finfo) ; // добавляем товар в заказ
     if ($options['reg_event']) _event_reg('Заказ: добавление товара',$rec['obj_name'].', '._format_price($price).'x '.$cnt.' шт.',$order_id.'.'.$this->tkey,$rec['_reffer']) ;
     /*
     if ($rec['bonus'])
     { $finfo=array(	'parent'		=>	$this->new_order_id,
                    'clss'			=>	120,   //
                    'enabled'		=>	1,
                    'obj_name' 		=> 	$rec['bonus']['name'].'('.$rec['bonus']['info'].') - В ПОДАРОК',
                    'reffer'		=>	$rec['bonus']['reffer'],
                    'price'			=>	0,
                    'cnt'			=>	$rec['__cart_cnt'],
                    'sales_type'	=>	0, // цена товара в корзине со скидкой клиента
                    'sales_value'	=>  0
                 ) ;
      adding_rec_to_table($this->tkey_table_orders_goods,$finfo) ; // добавляем товар в заказ
     } */

  }

  // добавляем услуги в смету заказа: доставка, скидка на заказа, использование купона, сборка товара, подъем на этаж
  //
  function order_append_service($order_id,$use_cart)
  {  // доставка - идет в массиве услуг
     //if ($use_cart->type_shiping and $this->include_shiping_to_order) $this->order_append_to_smeta($order_id,array('kod'=>'shiping','name'=>$use_cart->text_shiping,'price'=>$use_cart->price_shiping)) ;
     // скидка по акции
     if ($use_cart->result_price['sales_action']) $this->order_append_to_smeta($order_id,array('kod'=>'action','name'=>'Скидка по акции','price'=>$use_cart->result_price['sales_action'])) ;
     // срочная доставка
     //if ($use_cart->result_price['fast_shiping']) $this->order_append_to_smeta($order_id,array('kod'=>'fast_shiping','name'=>'Быстрая доставка','price'=>$use_cart->result_price['fast_shiping'])) ;
     // купон
     if ($use_cart->result_price['sales_kupon']) $this->order_append_to_smeta($order_id,array('kod'=>'kupon','name'=>$use_cart->kupon_info['number'].', '.$use_cart->kupon_info['sales'],'price'=>-$use_cart->result_price['sales_kupon'])) ;
     // другие услуги  (доставка, подьем, сборка) - идем по списку всех услуг но добавляем только те, что в корзине
     if (sizeof($this->service)) foreach($this->service as $code=>$rec) if (isset($use_cart->service[$code]) and $rec['include_to_order'])
         $this->order_append_to_smeta($order_id,array('kod'=>$code,'name'=>$use_cart->service[$code]['obj_name'],'price'=>$use_cart->service[$code]['price'])) ;

  }

  function order_append_file($order_id,$rec)
  {  include_once(_DIR_TO_ENGINE.'/admin/i_admin_system.php') ;
     $obj_rec=_CLSS(5)->obj_create(array(),array('parent_reffer'=>$order_id.'.'.$this->tkey)) ;
     $result=obj_upload_file($obj_rec['_reffer'],$rec,array('view_upload'=>0,'debug'=>0,'no_comment'=>1)) ;
     return($result) ;
  }

 // готовим информацию по заказу в формате:  цена кол-во скидка цена
 // все цены в основной валюте сайиа
 // результаты работы функции только выводтся на экран, нигде не сохраняются
 // доработана 25.08.11 в связи с переработкиой функции exec_price
 function prepare_price_info($rec,$options=array())
  {  global $goods_system ;
            // цена отдельного товара в корзине * количество в главной валюте сайта
            $price=$rec['__price_3'] ; if (!$price) $price=$rec['price'] ;
            if ($rec['__cart_price']) $price=$rec['__cart_price'] ;
            $cnt=$rec['__cart_cnt'] ; if (!$cnt) $cnt=$rec['cnt'] ;
		    $goods_sum_price=$price*$cnt/$this->to_cart_price_count ;
	        // формируем подпись и содержание к графе "скидка"
            // необходимо посчитать суммарную скидку на товар с учетом количества товара в корзине и значения to_cart_price_count
            // опция позволяем показывать скидку для одного элемента товара
            $use_cnt=($options['show_sales_to_van_goods'])? 1:$cnt ;

	        switch($rec['__price_used_sales'])
               { case 'member': $sales_value=$rec['__price_used_sales_value']*$use_cnt/$this->to_cart_price_count;
                                $order_info['sales_title']='Скидка клиента <br> '.'<strong>'.$rec['__price_used_sales_text'].'</strong>'  ;
                                //$order_info['sales_text']=$goods_system->format_price($sales_value,array('use_def_val'=>1)) ;
                                $order_info['sales_text']=$goods_system->format_price($rec['__price_used_sales_value'],array('use_def_val'=>1)) ;
                                break ;
                 case 'action': $sales_value=$rec['__price_used_sales_value']*$use_cnt/$this->to_cart_price_count ;
                                $order_info['sales_title']='Скидка на товар :<br><strong>'.$rec['__price_used_sales_text'].'</strong>' ;
                                //$order_info['sales_text']=$goods_system->format_price($sales_value,array('use_def_val'=>1)) ;
                                $order_info['sales_text']=$goods_system->format_price($rec['__price_used_sales_value'],array('use_def_val'=>1)) ;
                                break ;
                 case 'no_sales':$order_info['sales_title']='Скидка : <br><strong>не действует</strong>' ;
                                 $order_info['sales_text']='нет' ;
                                 $sales_value=0 ;
                                 break ;
                 default:  $order_info['sales_title']='-' ;
                           $order_info['sales_text']='' ;
                }

			// показываем цену с наценкой за еденицу товара
			$order_info['price']=$goods_system->format_price($price,array('use_def_val'=>1)) ; // форматируем вывод в соответствии с основной валютой
			$order_info['cnt']=$cnt;
			// вычитаем из общей суммы скидку
			$order_info['sum_price']=$goods_system->format_price($goods_sum_price-$sales_value,array('use_def_val'=>1)) ; //  цена товара в корзине со скидкой
            //damp_array($order_info) ;
    return($order_info);
  }

    function send_mail_create_order($order_info,$options=array())
     {

       global $goods_system,$mail_system ;

       $params=array() ;
       $params['data']=date("d.m.y G:i") ;
       $params['name']=$order_info['name'] ;
       $params['order_name']=$order_info['obj_name'] ;
       $status_usl='http://'._MAIN_DOMAIN.'/orders/status/'.$order_info['uid'] ;
       $params['status_info']='<a href="'.$status_usl.'">'.$status_usl.'</a>' ;
       $params['order_price']=($order_info['end_price'])? 'Сумма к оплате: '.$order_info['end_price'].$goods_system->VL_main_ue:'---' ;
       /// !!!!
       //if ($this->include_price_shiping_to_order)  if (sizeof($order_service['shiping'])) $params['order_price'].=', в том числе доставка "'.$order_service['shiping']['obj_name'].'" - '._format_price($order_service['shiping']['price'],array('null_price_text'=>'Бесплатно')) ;

       $params['order_typ_oplaty']=(isset($order_info['payments_type']))? 'Тип оплаты: '.$_SESSION['IL_payments_type'][$order_info['payments_type']]['obj_name']:'' ;

       if ($order_info['payments_method']=='robox') $params['order_typ_oplaty'].='. Если Вы не оплатили свой заказ сразу, для оплаты перейдите по этой <a href="http://'._MAIN_DOMAIN.'/orders/payments/'.$order_info['uid'].'">ссылке</a>' ;
       //if ($order_info['kupon_number']) list($sales_summa,$sales_kupon,$alert)=$this->check_kupon($order_info['kupon_number'],$_POST['kupon']['pin'],$use_cart->end_price) ;

       $params['order_goods']=$this->get_template_mail_goods_table($order_info) ;
       $params['member_info']=$this->get_template_mail_member_info($order_info) ;

       $mail_system->send_mail_to_pattern($order_info['email'],'orders_system_info_create_order',$params,$options) ;
     }

    function send_mail_create_order_to_admin($order_info,$options=array())
     {

       global $goods_system,$mail_system ;

       $params=array() ;
       $params['data']=date("d.m.y G:i") ;
       $params['name']=$order_info['name'] ;
       $params['order_name']=$order_info['obj_name'] ;
       $status_usl='http://'._MAIN_DOMAIN.'/orders/status/'.$order_info['uid'] ;
       $params['status_info']='<a href="'.$status_usl.'">'.$status_usl.'</a>' ;
       $params['order_price']=($order_info['end_price'])? 'Сумма к оплате: '.$order_info['end_price'].$goods_system->VL_main_ue:'---' ;
       /// !!!!
       //if ($this->include_price_shiping_to_order)  if (sizeof($order_service['shiping'])) $params['order_price'].=', в том числе доставка "'.$order_service['shiping']['obj_name'].'" - '._format_price($order_service['shiping']['price'],array('null_price_text'=>'Бесплатно')) ;
       //damp_array($order_info);
       $params['order_typ_oplaty']=(isset($order_info['payments_type']))? 'Тип оплаты: '.$_SESSION['IL_payments_type'][$order_info['payments_type']]['obj_name']:'' ;
       if ($order_info['payments_method']=='robox' and $order_info['payments_status']==2)
        { $params['order_typ_oplaty'].='<br>Оплаченная сумма: <strong>'.$order_info['payments_amount'].'</strong>' ;
          $params['order_typ_oplaty'].='<br>Ответ Robox: <strong>Оплачено</strong>' ;
          $params['order_typ_oplaty'].='<br>метод оплаты: <strong>'.$order_info['payments_method'].'</strong>' ;
          //$params['order_typ_oplaty'].='<br>Детали оплаты: <strong>'.print_r($order_info['payments_info'],true).'</strong>' ;
        }
        else $params['order_typ_oplaty'].='. Ссылка для оплаты заказа: <a href="http://'._MAIN_DOMAIN.'/orders/payments/'.$order_info['uid'].'">ссылка</a>' ;

       $params['order_goods']=$this->get_template_mail_goods_table_to_admin($order_info) ;
       $params['member_info']=$this->get_template_mail_member_info($order_info,array('show_reffer_info'=>1)) ;

       $files_info='' ;
       if (sizeof($order_info['files']))
         {  $_temp=array() ;
            foreach($order_info['files'] as $file_info) $_temp[]='<a href="'.$file_info['__href'].'">'.$file_info['obj_name'].'</a>' ;
            $files_info='К заказу приложены файлы: '.implode(', ',$_temp).'<br>' ;
         }

       $files_info.='<strong>Печатная форма заказа:</strong> <a href="http://'._MAIN_DOMAIN.'/orders/pdf/'.$order_info['uid'].'.pdf">СКАЧАТЬ</a>' ;
       $params['files_info']=$files_info ;

       $options['reg_events']=1 ;
       //$options['files'][]='http://'._MAIN_DOMAIN.'/orders/pdf/'.$order_info['uid'].'.pdf' ; // не катит, нужен файл на диске реальный
       //$options['use_event_id']=0 ;
       $template_mail=($options['template_mail'])? $options['template_mail']:'orders_system_info_create_order_alert' ;

       if ($order_info['email']!=_EMAIL_ENGINE_DEBUG) $mail_system->send_mail_to_pattern($_SESSION['LS_orders_system_email_alert'],$template_mail,$params,$options) ;
       else                                           $mail_system->send_mail_to_pattern(_EMAIL_ENGINE_DEBUG,$template_mail,$params,$options) ;
     }

     function get_template_mail_goods_table($order_info,$options=array())
     {   $order_goods=&$order_info['goods'] ;
         $order_service=&$order_info['service'] ;
         $text='' ;
         if (sizeof($order_goods))
             { $_temp=array() ;
               $_temp[]='<tr class=header><td>Артикул</td><td>Фото</td><td>Наименование</td><td>Цена</td><td>Количество</td><td>Скидка</td><td>Стоимость</td></tr>' ;
               foreach ($order_goods as $rec)
               {  // переносим информацию по ценам за запись в товару
                  $cart_goods_info=array_merge($rec['__reffer_from'],$rec['__reffer_from']['price_info']) ; unset($cart_goods_info['price_info']) ; //damp_array($cart_goods_info) ;
               	 $price_info=$this->prepare_price_info($cart_goods_info) ; //damp_array($price_info) ;
               	 $href=(strpos($rec['__href'],'http://')!==0)? _PATH_TO_SITE.$rec['__href']:$rec['__href'] ;
                  $name=($rec['__href'])? '<a href="'.$href.'">'.$cart_goods_info['obj_name'].'</a>':$cart_goods_info['obj_name'] ; // obj_name - запись уже не товара, а позиции заказа
                  $stock_info=(!$rec['__stock_info'] and 0)? '<div class=no_stock>Нет в наличии, срок ожидания может составить от 2 до 6 недель</div>':'' ;
                  $img=($rec['_image_name'])?  '<img src="'.img_clone($rec,'small').'" alt="">':'' ;
                  $sortament_info=(is_object($_SESSION['1c_system']) and sizeof($rec['_sortament_rec']))? '<br>'.$_SESSION['1c_system']->get_sortament_info($rec['_sortament_rec'],array('delimiter'=>'<br>')):'' ;
                  // $stock_info=''
                  // ;
                  $_temp[]='<tr><td>'.$rec['__art'].'</td>'.
                                '<td class=left>'.$img.'</td>'.
                            	'<td class=left>'.$name.$sortament_info.$stock_info.'</td>'.
                                '<td style="text-align:right;">'.$price_info['price'].'</td>'.
                                '<td style="text-align:center;">'.$price_info['cnt'].'</td>'.
                                '<td>'.$price_info['sales_title'].'</td>'.
                                '<td style="text-align:right;">'.$price_info['sum_price'].'</td>'.
                            '</tr>' ;
               }

               if (sizeof($order_service)) foreach($order_service as $rec)
               { $price=($rec['price']>0)? _format_price($rec['price'],array('use_if_null'=>'-')):'' ;
                 $_temp[]='<tr><td></td><td></td><td><div class=name>'.$rec['obj_name'].'</td><td style="text-align:right;">'.$price.'</td><td></td><td></td><td style="text-align:right;">'.$price.'</td></tr>' ;
               }
               //if ($order_info['kupon_sales']>0) $_temp[]='<tr><td colspan=2>Скидка по купону № '.$order_info['kupon_number'].'</td><td align=left></td><td>&nbsp;</td><td>&nbsp;</td><td  align=right>'._format_price($order_info['kupon_sales']).'</td></tr>' ;
               $_temp[]='<tr valign=top><td colspan=6>ИТОГО</td><td style="text-align:right;"><strong>'._format_price($order_info['end_price']).'</strong></td></tr>' ;
               $text='<table class="order_goods_info">'.implode('',$_temp).'</table>' ;
             }
        return($text) ;
     }

     function get_template_mail_goods_table_to_admin($order_info,$options=array())
     {   $order_goods=&$order_info['goods'] ;
         $order_service=&$order_info['service'] ;
         $text='' ;
         if (sizeof($order_goods))
             { $_temp=array() ;
               $_temp[]='<tr class=header><th>Штрих-код</th><th>Артикул</th><th>Фото</th><th>Наименование</th><th>Склад</th><th>Цена</th><th>Количество</th><th>Скидка</th><th>Стоимость</th></tr>' ;
               foreach ($order_goods as $rec)
               {  // переносим информацию по ценам за запись в товару
                  $cart_goods_info=array_merge($rec['__reffer_from'],$rec['__reffer_from']['price_info']) ; unset($cart_goods_info['price_info']) ; //damp_array($cart_goods_info) ;
               	 $price_info=$this->prepare_price_info($cart_goods_info) ; //damp_array($price_info) ;
               	 $href=(strpos($rec['__href'],'http://')!==0)? _PATH_TO_SITE.$rec['__href']:$rec['__href'] ;
                  $name=($rec['__href'])? '<a href="'.$href.'">'.$cart_goods_info['obj_name'].'</a>':$cart_goods_info['obj_name'] ; // obj_name - запись уже не товара, а позиции заказа
                  //$stock_info=(!$rec['__stock_info'] and 0)? '<div class=no_stock>Нет в наличии, срок ожидания может составить от 2 до 6 недель</div>':'' ;
                  $img=($rec['_image_name'])?  '<img src="'.img_clone($rec,'small').'" alt="">':'' ;
                  $sortament_info=(is_object($_SESSION['1c_system']) and sizeof($rec['_sortament_rec']))? '<br>'.$_SESSION['1c_system']->get_sortament_info($rec['_sortament_rec'],array('delimiter'=>'<br>')):'' ;
                  $sortament_barcode=(sizeof($rec['_sortament_rec']))? '<img src="http://'._MAIN_DOMAIN.'/orders/barcode'.$rec['_sortament_rec']['code'].'.png">':'' ;
                  // $stock_info=''

                  $sort_info=get_obj_info($rec['__reffer_from']['sortament_reffer']) ;  $stock_recs=array() ;
                  if ($sort_info['pkey']) $stock_recs=execSQL_row('select obj_name,cnt from '.TM_SORTAMENT_STOCK.' where parent="'.$sort_info['pkey'].'"') ;


                  ob_start() ;
                            if (sizeof($stock_recs))
                            { ?><h3>Информация по наличию</h3><table class="stock_info"><?
                                foreach($stock_recs as $title=>$cnt) {?><tr><td><?echo $title?></td><td><?echo $cnt?></td></tr><?}
                                ?></table><?
                            } else if ($sort_info['pkey']) {?><strong>Нет в наличии</strong><?}

                  $stock_info=ob_get_clean()  ;
                  // ;
                  $_temp[]='<tr><td style="text-align:center;">'.$sortament_barcode.'</td>'.
                                '<td>'.$rec['__art'].'</td>'.
                                '<td class=left>'.$img.'</td>'.
                            	'<td class=left>'.$name.$sortament_info.$stock_info.'</td>'.

                                '<td style="text-align:center;">'.$rec['_sortament_rec']['stock'].'</td>'.
                                '<td style="text-align:right;">'.$price_info['price'].'</td>'.
                                '<td style="text-align:center;">'.$price_info['cnt'].'</td>'.
                                '<td>'.$price_info['sales_title'].'</td>'.
                                '<td style="text-align:right;">'.$price_info['sum_price'].'</td>'.
                            '</tr>' ;
               }

               if (sizeof($order_service)) foreach($order_service as $rec)
               { $price=($rec['price']>0)? _format_price($rec['price'],array('use_if_null'=>'-')):'' ;
                 $_temp[]='<tr><td></td><td></td><td></td><td><div class=name>'.$rec['obj_name'].'</td><td></td><td style="text-align:right;">'.$price.'</td><td></td><td></td><td style="text-align:right;">'.$price.'</td></tr>' ;
               }
               //if ($order_info['kupon_sales']>0) $_temp[]='<tr><td colspan=2>Скидка по купону № '.$order_info['kupon_number'].'</td><td align=left></td><td>&nbsp;</td><td>&nbsp;</td><td  align=right>'._format_price($order_info['kupon_sales']).'</td></tr>' ;
               $_temp[]='<tr valign=top><td colspan=8>ИТОГО</td><td style="text-align:right;"><strong>'._format_price($order_info['end_price']).'</strong></td></tr>' ;
               $text='<table  class="order_goods_info">'.implode('',$_temp).'</table>' ;
             }
        return($text) ;
     }

     function get_template_mail_member_info($order_info,$options=array())
     {
         $_temp=array() ;
            								$_temp[]='<tr><td colspan=2  class=header>Информация по покупателю</td></tr>' ;
            if ($order_info['member_typ']) 	$_temp[]='<tr><td>Получатель заказа</td><td>'.$order_info['member_typ'].'</td></tr>' ;
            if ($order_info['org_name']) 	$_temp[]='<tr><td>Организация</td><td>'.$order_info['org_name'].'</td></tr>' ;
         								   	$_temp[]='<tr><td>Контактное лицо</td><td>'.$order_info['name'].'</td></tr>' ;
         								   	$_temp[]='<tr><td>Телефон для связи</td><td>'.$order_info['phone'].'</td></tr>' ;
         								   	$_temp[]='<tr><td>E-mail</td><td>'.$order_info['email'].'</td></tr>' ;
         								   	$_temp[]='<tr><td>Адрес доставки</td><td>'.$order_info['city'].' '.$order_info['adres'].'</td></tr>' ;
            if ($order_info['info']['time_dost']) $_temp[]='<tr><td>Время доставки</td><td>'.$order_info['info']['time_dost'].'</td></tr>' ;
            if ($order_info['info']['trans_select']) $_temp[]='<tr><td>Транспортная компания</td><td>'.$order_info['info']['trans_select'].'</td></tr>' ;
            if ($order_info['rekvezit']) 	$_temp[]='<tr><td>Реквизиты</td><td>'.$order_info['rekvezit'].'</td></tr>' ;
            								$_temp[]='<tr><td>Примечание</td><td>'.$order_info['comment'].'</td></tr>' ;
            //if ($order_info['kupon_number']) $_temp[]='<tr><td>Номер купона</td><td>'.$order_info['kupon_number'].'</td></tr>' ;
            //if ($order_info['kupon_pin'])    $_temp[]='<tr><td>PIN купона</td><td>'.$order_info['kupon_pin'].'</td></tr>' ;
            //if ($order_info['kupon_sales'])  $_temp[]='<tr><td>Скидка по купону</td><td>'.$order_info['kupon_sales'].'</td></tr>' ;

            if ($options['show_reffer_info'] and $order_info['referer_host']) $_temp[]='<tr><td>Откуда переход</td><td>'.$order_info['referer_host'].'</td></tr>' ;

            $text='<table class="order_member_info">'.implode('',$_temp).'</table>' ;
         return($text) ;
     }

 function _send_mail_create_order($order_info,$options=array())
 { global $goods_system,$mail_system ;
   $order_goods=&$order_info['goods'] ;
   $order_service=&$order_info['service'] ;
   //damp_array($order_goods) ;
   //damp_array($order_service) ;

   $params=array() ;
   $params['data']=date("d.m.y G:i") ;
   $params['name']=$order_info['name'] ;
   $params['order_name']=$order_info['obj_name'] ;
   $status_usl='http://'._MAIN_DOMAIN.'/orders/status/'.$order_info['uid'] ;
   $params['status_info']='<a href="'.$status_usl.'">'.$status_usl.'</a>' ;
   $params['order_price']=($order_info['end_price'])? 'Сумма к оплате: '.$order_info['end_price'].$goods_system->VL_main_ue:'---' ;
   /// !!!!
   //if ($this->include_price_shiping_to_order)  if (sizeof($order_service['shiping'])) $params['order_price'].=', в том числе доставка "'.$order_service['shiping']['obj_name'].'" - '._format_price($order_service['shiping']['price'],array('null_price_text'=>'Бесплатно')) ;

   $params['order_typ_oplaty']=(isset($order_info['payments_type']))? 'Тип оплаты: '.$_SESSION['IL_payments_type'][$order_info['payments_type']]['obj_name']:'' ;


   if ($order_info['payments_method']=='robox') $params['order_typ_oplaty'].='. Если Вы не оплатили свой заказ сразу, для оплаты перейдите по этой <a href="http://'._MAIN_DOMAIN.'/orders/payments/'.$order_info['uid'].'">ссылке</a>' ;
   //if ($order_info['kupon_number']) list($sales_summa,$sales_kupon,$alert)=$this->check_kupon($order_info['kupon_number'],$_POST['kupon']['pin'],$use_cart->end_price) ;

   if (sizeof($order_goods))
    { $_temp=array() ;
	  $_temp[]='<tr class=header><td>Артикул</td><td>Фото</td><td>Наименование</td><td>Цена</td><td>Количество</td><td>Скидка</td><td>Стоимость</td></tr>' ;
      foreach ($order_goods as $rec)
      {  // переносим информацию по ценам за запись в товару
         $cart_goods_info=array_merge($rec['__reffer_from'],$rec['__reffer_from']['price_info']) ; unset($cart_goods_info['price_info']) ; //damp_array($cart_goods_info) ;
      	 $price_info=$this->prepare_price_info($cart_goods_info) ; //damp_array($price_info) ;
      	 $href=(strpos($rec['__href'],'http://')!==0)? _PATH_TO_SITE.$rec['__href']:$rec['__href'] ;
         $name=($rec['__href'])? '<a href="'.$href.'">'.$cart_goods_info['obj_name'].'</a>':$cart_goods_info['obj_name'] ; // obj_name - запись уже не товара, а позиции заказа
         $img=($rec['_image_name'])?  '<img src="'.img_clone($rec,'small').'" alt="">':'' ;
          ;
         $_temp[]='<tr><td>'.$rec['__art'].'</td>'.
                       '<td class=left>'.$img.'</td>'.
                   	   '<td class=left>'.$name.'</td>'.
                       '<td align=right>'.$price_info['price'].'</td>'.
                       '<td align=center>'.$price_info['cnt'].'</td>'.
                       '<td>'.$price_info['sales_title'].'</td>'.
                       '<td align=right>'.$price_info['sum_price'].'</td>'.
                   '</tr>' ;
      }

      if (sizeof($order_service)) foreach($order_service as $rec)
      { $price=($rec['price']>0)? _format_price($rec['price'],array('use_if_null'=>'-')):'' ;
        $_temp[]='<tr><td></td><td></td><td><div class=name>'.$rec['obj_name'].'</td><td align=right>'.$price.'</td><td></td><td></td><td align=right>'.$price.'</td></tr>' ;
      }
      //if ($order_info['kupon_sales']>0) $_temp[]='<tr><td colspan=2>Скидка по купону № '.$order_info['kupon_number'].'</td><td align=left></td><td>&nbsp;</td><td>&nbsp;</td><td  align=right>'._format_price($order_info['kupon_sales']).'</td></tr>' ;
      $_temp[]='<tr valign=top><td colspan=6>ИТОГО</td><td align=right><strong>'._format_price($order_info['end_price']).'</strong></td></tr>' ;
      $params['order_goods']='<table>'.implode('',$_temp).'</table>' ;
    } else $params['order_goods']="" ;

   //$options['debug']=2 ;

   $_temp=array() ;
   									$_temp[]='<tr class=header><td colspan=2>Информация по покупателю</td></tr>' ;
   if ($order_info['member_typ']) 	$_temp[]='<tr><td>Получатель заказа</td><td>'.$order_info['member_typ'].'</td></tr>' ;
   if ($order_info['org_name']) 	$_temp[]='<tr><td>Организация</td><td>'.$order_info['org_name'].'</td></tr>' ;
								   	$_temp[]='<tr><td>Контактное лицо</td><td>'.$order_info['name'].'</td></tr>' ;
								   	$_temp[]='<tr><td>Телефон для связи</td><td>'.$order_info['phone'].'</td></tr>' ;
								   	$_temp[]='<tr><td>E-mail</td><td>'.$order_info['email'].'</td></tr>' ;
								   	$_temp[]='<tr><td>Адрес доставки</td><td>'.$order_info['city'].' '.$order_info['adres'].'</td></tr>' ;
   if ($order_info['rekvezit']) 	$_temp[]='<tr><td>Реквизиты</td><td>'.$order_info['rekvezit'].'</td></tr>' ;
   									$_temp[]='<tr><td>Примечание</td><td>'.$order_info['comment'].'</td></tr>' ;
   //if ($order_info['kupon_number']) $_temp[]='<tr><td>Номер купона</td><td>'.$order_info['kupon_number'].'</td></tr>' ;
   //if ($order_info['kupon_pin'])    $_temp[]='<tr><td>PIN купона</td><td>'.$order_info['kupon_pin'].'</td></tr>' ;
   //if ($order_info['kupon_sales'])  $_temp[]='<tr><td>Скидка по купону</td><td>'.$order_info['kupon_sales'].'</td></tr>' ;

   $params['member_info']='<table>'.implode('',$_temp).'</table>' ;

   $mail_system->send_mail_to_pattern($order_info['email'],'orders_system_info_create_order',$params,$options) ;
 }

 function _send_mail_create_order_to_admin($order_info,$options=array())
 { global $goods_system,$mail_system ;
   $order_goods=&$order_info['goods'] ;
   $order_service=&$order_info['service'] ;
   //damp_array($order_goods) ;
   //damp_array($order_service) ;

   $params=array() ;
   $params['data']=date("d.m.y G:i") ;
   $params['name']=$order_info['name'] ;
   $params['order_name']=$order_info['obj_name'] ;
   $status_usl='http://'._MAIN_DOMAIN.'/orders/status/'.$order_info['uid'] ;
   $params['status_info']='<a href="'.$status_usl.'">'.$status_usl.'</a>' ;
   $params['order_price']=($order_info['end_price'])? 'Сумма к оплате: '.$order_info['end_price'].$goods_system->VL_main_ue:'---' ;
   /// !!!!
   //if ($this->include_price_shiping_to_order)  if (sizeof($order_service['shiping'])) $params['order_price'].=', в том числе доставка "'.$order_service['shiping']['obj_name'].'" - '._format_price($order_service['shiping']['price'],array('null_price_text'=>'Бесплатно')) ;

   $params['order_typ_oplaty']=(isset($order_info['payments_type']))? 'Тип оплаты: '.$_SESSION['IL_payments_type'][$order_info['payments_type']]['obj_name']:'' ;

   if ($order_info['payments_method']=='robox' and $order_info['payments_status']==2)
   { $params['order_typ_oplaty'].='<br>Оплаченная сумма: <strong>'.$order_info['payments_amount'].'</strong>' ;
     $params['order_typ_oplaty'].='<br>Ответ Robox: <strong>Оплачено</strong>' ;
     $params['order_typ_oplaty'].='<br>метод оплаты: <strong>'.$order_info['payments_method'].'</strong>' ;
     $params['order_typ_oplaty'].='<br>Детали оплаты: <strong>'.print_r($order_info['payments_info'],true).'</strong>' ;
   }
   else $params['order_typ_oplaty'].='. Ссылка для оплаты заказа: <a href="http://'._MAIN_DOMAIN.'/orders/payments/'.$order_info['uid'].'">ссылка</a>' ;
   //if ($order_info['kupon_number']) list($sales_summa,$sales_kupon,$alert)=$this->check_kupon($order_info['kupon_number'],$_POST['kupon']['pin'],$use_cart->end_price) ;

   if (sizeof($order_goods))
    { $_temp=array() ;
	  $_temp[]='<tr class=header><td>Артикул</td><td>Фото</td><td>Наименование</td><td>Цена</td><td>Количество</td><td>Скидка</td><td>Стоимость</td></tr>' ;
      foreach ($order_goods as $rec)
      {  // переносим информацию по ценам за запись в товару
         $cart_goods_info=array_merge($rec['__reffer_from'],$rec['__reffer_from']['price_info']) ; unset($cart_goods_info['price_info']) ; //damp_array($cart_goods_info) ;
      	 $price_info=$this->prepare_price_info($cart_goods_info) ; //damp_array($price_info) ;
      	 $href=(strpos($rec['__href'],'http://')!==0)? _PATH_TO_SITE.$rec['__href']:$rec['__href'] ;
         $name=($rec['__href'])? '<a href="'.$href.'">'.$cart_goods_info['obj_name'].'</a>':$cart_goods_info['obj_name'] ; // obj_name - запись уже не товара, а позиции заказа
         $img=($rec['_image_name'])?  '<img src="'.img_clone($rec,'small').'" alt="">':'' ;

         $arr=explode(' ',strip_tags($rec['obj_name'])) ;
         $code=trim($arr[sizeof($arr)-1]) ;
         $sort_info=execSQL_van('select * from '.TM_SORTAMENT.' where code like "'.$code.'"') ;
         if ($sort_info['pkey']) $stock_recs=execSQL('select * from '.TM_SORTAMENT_STOCK.' where parent="'.$sort_info['pkey'].'"') ;

         ob_start() ;
                    if (sizeof($stock_recs))
                    { ?><h3>Информация по наличию</h3><table class="stock_info"><?
                        foreach($stock_recs as $rec_stock) {?><tr><td><?echo $rec_stock['obj_name']?></td><td><?echo $rec_stock['cnt']?></td></tr><?}
                        ?></table><?
                    } else {?><strong>Нет в наличии</strong><?}

          $stock_info=ob_get_clean()  ;

         $_temp[]='<tr><td>'.$rec['__art'].'</td>'.
                       '<td class=left>'.$img.'</td>'.
                   	   '<td class=left>'.$name.$stock_info.'</td>'.
                       '<td align=right>'.$price_info['price'].'</td>'.
                       '<td align=center>'.$price_info['cnt'].'</td>'.
                       '<td>'.$price_info['sales_title'].'</td>'.
                       '<td align=right>'.$price_info['sum_price'].'</td>'.
                   '</tr>' ;
      }

      if (sizeof($order_service)) foreach($order_service as $rec)
      { $price=($rec['price']>0)? _format_price($rec['price'],array('use_if_null'=>'-')):'' ;
        $_temp[]='<tr><td></td><td></td><td><div class=name>'.$rec['obj_name'].'</td><td align=right>'.$price.'</td><td></td><td></td><td align=right>'.$price.'</td></tr>' ;
      }
      //if ($order_info['kupon_sales']>0) $_temp[]='<tr><td colspan=2>Скидка по купону № '.$order_info['kupon_number'].'</td><td align=left></td><td>&nbsp;</td><td>&nbsp;</td><td  align=right>'._format_price($order_info['kupon_sales']).'</td></tr>' ;
      $_temp[]='<tr valign=top><td colspan=6>ИТОГО</td><td align=right><strong>'._format_price($order_info['end_price']).'</strong></td></tr>' ;
      $params['order_goods']='<table>'.implode('',$_temp).'</table>' ;
    } else $params['order_goods']="" ;

   //$options['debug']=2 ;

   $_temp=array() ;
   									$_temp[]='<tr class=header><td colspan=2>Информация по покупателю</td></tr>' ;
   if ($order_info['member_typ']) 	$_temp[]='<tr><td>Получатель заказа</td><td>'.$order_info['member_typ'].'</td></tr>' ;
   if ($order_info['org_name']) 	$_temp[]='<tr><td>Организация</td><td>'.$order_info['org_name'].'</td></tr>' ;
								   	$_temp[]='<tr><td>Контактное лицо</td><td>'.$order_info['name'].'</td></tr>' ;
								   	$_temp[]='<tr><td>Телефон для связи</td><td>'.$order_info['phone'].'</td></tr>' ;
								   	$_temp[]='<tr><td>E-mail</td><td>'.$order_info['email'].'</td></tr>' ;
								   	$_temp[]='<tr><td>Адрес доставки</td><td>'.$order_info['city'].' '.$order_info['adres'].'</td></tr>' ;
   if ($order_info['rekvezit']) 	$_temp[]='<tr><td>Реквизиты</td><td>'.$order_info['rekvezit'].'</td></tr>' ;
   									$_temp[]='<tr><td>Примечание</td><td>'.$order_info['comment'].'</td></tr>' ;
   //if ($order_info['kupon_number']) $_temp[]='<tr><td>Номер купона</td><td>'.$order_info['kupon_number'].'</td></tr>' ;
   //if ($order_info['kupon_pin'])    $_temp[]='<tr><td>PIN купона</td><td>'.$order_info['kupon_pin'].'</td></tr>' ;
   //if ($order_info['kupon_sales'])  $_temp[]='<tr><td>Скидка по купону</td><td>'.$order_info['kupon_sales'].'</td></tr>' ;

   $params['member_info']='<table>'.implode('',$_temp).'</table>' ;

   if ($order_info['referer_host']) $_temp[]='<tr><td>Откуда переход</td><td>'.$order_info['referer_host'].'</td></tr>' ;
   $params['member_info']='<table>'.implode('',$_temp).'</table>' ;

   if (sizeof($order_info['files']))
     {  $_temp=array() ;
        foreach($order_info['files'] as $file_info) $_temp[]='<a href="'.$file_info['__href'].'">'.$file_info['obj_name'].'</a>' ;
        $params['files_info']='К заказу приложены файлы: '.implode(', ',$_temp) ;
     }  else $params['files_info']='' ;

   $options['reg_events']=1 ;
   $template_mail=($options['template_mail'])? $options['template_mail']:'orders_system_info_create_order_alert' ;
   //$options['use_event_id']=0 ;
   if ($order_info['email']!=_EMAIL_ENGINE_DEBUG) $mail_system->send_mail_to_pattern($_SESSION['LS_orders_system_email_alert'],$template_mail,$params,$options) ;
   else                                           $mail_system->send_mail_to_pattern(_EMAIL_ENGINE_DEBUG,$template_mail,$params,$options) ;

   $mail_system->send_mail_to_pattern('5207403@mail.ru',$template_mail,$params,$options) ;
 }

 function event_order_on_create($order_info,$options=array()) {}

 // ---------------------------------
 //
 // работа с существующим заказом
 //
 // ---------------------------------

   // открываем заказ для личного кабинета
   // проверить переменные!!!!!
   function order_open($options=array())
   { global $member ;
	 if ($options['id']) // открываем заказ
	 	{  $obj_info=select_objs($this->tkey_table_orders,'*','pkey="'.$options['id'].'" and member="'.$member->reffer.'" and enabled=1',array('no_arr'=>1,'no_slave_table'=>1,'debug'=>0)) ;
           if ($obj_info['pkey'])
           { $this->id=$obj_info['pkey'] ;
             $this->info=$obj_info ;
             $this->reffer=$this->id.'.'.$_SESSION['pkey_by_table'][$this->table_orders] ;
             $this->status=$obj_info['status'] ;
           }
	 	}
   }




  // возвращает информацию по товару в заказе для личного кабинета
  function get_goods_info()
       { //$sql='select pkey as ordd_pkey,price as ordd_price,cnt as ordd_cnt,reffer as ordd_reffer from '.$this->table_orders_goods.' where parent='.$this->new_order_id.'<br>' ;
         //echo $sql ;
         $ordd_goods=execSQL('select pkey as ordd_pkey,price as ordd_price,cnt as ordd_cnt,reffer as ordd_reffer from '.$this->table_orders_goods.' where parent='.$this->new_order_id) ;
         $ordd=select_refs($this->tkey_table_orders_goods,'*',' parent='.$this->new_order_id.' and enabled=1') ;
         if (sizeof($ordd))
         { list($tkey,$usl_pkeys)=each($ordd);
           $ordd_goods_info=select_objs($tkey,'*','pkey in ('.$usl_pkeys.')',array('debug'=>0,'no_out_table'=>1,'no_group_clss'=>1)) ;
               if ($ordd_goods) foreach($ordd_goods as $i=>$rec_ordd)
               { list($pkey,$tkey)=explode('.',$rec_ordd['ordd_reffer']) ;
                 $ordd_goods[$i]=array_merge($rec_ordd,$ordd_goods_info[$tkey][$pkey]) ;
               }

		   return($ordd_goods) ;
		 }
         return(array()) ;
       }

  // обновляем состояние оплаты заказы - вызывается из модуля Robox, Liqpay
  function update_order_payments_status($id,$summa,$method='',$info=array())
    { $comment='' ;
      // payments_amount - сумма, которая была внесена через платежную систему
      // payments_status - платежный статус заказа, 2= заказ оплачен
      // payments_method - метод оплаты (robox или другой платежный сервис)
      $rec['payments_amount']=$summa ;
      $rec['payments_data']=time() ;
      $rec['payments_status']=2 ;  // оплачено
      $rec['payments_method']=$method ;
      $rec['payments_info']=$info ;
      $res=update_rec_in_table($this->table_name,$rec,'pkey='.$id) ;
      $reffer=$id.'.'.$this->tkey ;
      if ($method) $comment=' ('.$method.')' ;
      if ($res) $evt_id=_event_reg('Заказ: оплата',$summa.' рублей'.$comment,$reffer) ; // регистрируем событие
      else      $evt_id=_event_reg('Заказ: не удалось зафиксировать оплату',$summa.' рублей'.$comment,$reffer) ; // регистрируем событие


      // загружаем из базы сохраненный заказ
      $order_info=$this->get_order($id,array('goods_full_info'=>1)) ;
      $order_info['id']=$order_info['pkey'] ; // временно для совместимости
      $this->send_mail_create_order_to_admin($order_info,array('use_event_id'=>$evt_id,'template_mail'=>'orders_system_info_payment_order_alert')) ;


      return($res) ;
    }

  // добавляем новый товар в заказ, пересчитываем стоимость заказа
  function order_add_goods($order_id,$rec)
  {  //$goods=$this->get_orders_goods($order_id) ;
     //if (isset($goods[$rec['_reffer']])) return(0) ;
     $this->order_append_to_smeta($order_id,$rec,array('reg_event'=>1)) ;
     $this->order_update_sum_price($order_id) ;

     return(1) ;
  }

  // добавляем новый товар в заказ, пересчитываем стоимость заказа
  function order_delete_goods($order_id,$smeta_id)
  {  //$goods=$this->get_orders_goods($order_id) ;
     //damp_array($goods) ;
     //if (!isset($goods[$reffer])) return(0)
     $rec=execSQL_van('select * from '.$this->table_orders_goods.' where pkey='.$smeta_id) ;;
     execSQL_update('delete from '.$this->table_orders_goods.' where pkey='.$smeta_id) ;
     _event_reg('Заказ: удаление товара',$rec['obj_name'].', '._format_price($rec['price']).'x '.$rec['cnt'].' шт.',$order_id.'.'.$this->tkey) ;
     $this->order_update_sum_price($order_id) ;
     return(1) ;
  }

  // добавляем новый товар в заказ, пересчитываем стоимость заказа
  function order_update_goods_cnt($order_id,$smeta_id,$cnt,$options)
  {  $rec=execSQL_van('select * from '.$this->table_orders_goods.' where pkey='.$smeta_id) ;
     if ($rec['cnt']!=$cnt)
     { execSQL_update('update '.$this->table_orders_goods.' set cnt="'.$cnt.'" where pkey='.$smeta_id) ;
       _event_reg('Заказ: изменение кол-ва товара',$rec['obj_name'].', '._format_price($rec['price']).'x '.$rec['cnt'].' шт. => '.$cnt.' шт.',$order_id.'.'.$this->tkey) ;
     }
     if (!$options['no_update_sum_price']) $this->order_update_sum_price($order_id) ;
     return(1) ;
  }

  function order_change_shiping($order_id,$type_shipping,$price_shipping)
  {  $order_service=$this->get_orders_service_full_info($order_id) ;
     $new_shipping_name=$_SESSION['IL_type_shiping'][$type_shipping]['obj_name'] ;
     $new_shipping_price=($price_shipping)? $price_shipping:$_SESSION['IL_type_shiping'][$type_shipping]['price'] ;
     $res=update_rec_in_table($this->table_orders_goods,array('obj_name'=>$new_shipping_name,'price'=>$new_shipping_price,'cnt'=>1),'parent='.$order_id.' and reffer="shiping"') ;
     if ($res) _event_reg('Заказ: изменение условий доставки',$order_service['shiping']['obj_name'].', '._format_price($order_service['shiping']['price']).' => '.$new_shipping_name.', '._format_price($new_shipping_price),$order_id.'.'.$this->tkey) ;
     $this->order_update_sum_price($order_id) ;
     return($res) ;
  }




  // обновляем информация по сумме заказа
  function order_update_sum_price($order_id)
  {  $order_info=$this->get_order($order_id,array('goods_full_info'=>1)) ;
     $order_goods=&$order_info['goods'] ;
     $order_service=&$order_info['service'] ;
     $sum_price=0 ;
     $sum_sales=0 ;
     if (sizeof($order_goods)) foreach($order_goods as $rec)
     { $cart_rec=$rec['__reffer_from'] ;
       if (sizeof($cart_rec['price_info'])) $price_info=$cart_rec['price_info'] ;
       else //  обеспечиваем совместимость с заказами сделанными в старых версиях движка
       { $price_info['__price_3']=$cart_rec['price'] ;
         $price_info['__price_used_sales_value']=$cart_rec['sales_value'] ;
       }
       $cur_goods_sales=$price_info['__price_used_sales_value']*$cart_rec['cnt'] ; // скидка по текущему товару
       $cur_goods_price=$price_info['__price_3']*$cart_rec['cnt'] ;                // стоимость товара без скидки
       $sum_sales+=$cur_goods_sales ;  // суммарная скидка по всем товарам
       $sum_price+=$cur_goods_price ;  // сумма стоимости товара без скидки
     }
     $sum_price=$sum_price-$sum_sales ;
     // суммируем сумму всех услуг
     if (sizeof($order_service)) foreach($order_service as $rec) $sum_price=$sum_price+$rec['price'] ;
     $result=execSQL_update('update '.$this->table_name.' set end_price='.$sum_price.' where pkey='.$order_id) ;
     if ($result) _event_reg('Заказ: изменение суммы заказа',_format_price($order_info['end_price']).' => '._format_price($sum_price),$order_info['_reffer']) ;
  }

}

class c_cart
{ public $result_price  ;
  public $all_count  ;   // число позиций
  public $sum_count  ;   // количество товара
  public $sum_price  ;   // сумма (число*стоимость), без скидки клиента, и без скидки на товары
  public $end_price ;	  // cумма товара в корзине с учетом всех скидок
  public $sales_goods ;  // суммарный размер товарной скидки по всем товарам
  public $sales_member ; // суммарный размер клиентской скидки по всем товарам
  public $sales_kupon ; // суммарный размер клиентской скидки по всем товарам
  // доставка
  public $type_shiping=1 ; // текущий тип доставки
  public $price_shiping ; // текущая стоимость доставки
  public $text_shiping ; //
  public $service=array() ; //


  public $goods=array() ; // краткая информация по товару в корзине ($public_info=$this->prepare_goods_info($obj_info))

   // создание корзины
  function c_cart($options=array())
  { $this->sum_price=0;
    $this->result_price=array();
    $this->service=array();
  }

  function destroy()
  { $this->goods=array() ;
    global $orders_system ; $orders_system->cart_update_info() ;
  }



  /*
  function append_goods_to_order($order_id)
  {
    if (sizeof($this->goods)) foreach($this->goods as $rec) $_SESSION['orders_system']->order_append_to_smeta($order_id,$rec) ;
  } */

  // сохраняем в текущей корзине тип доставки, автоматически получаем название доставки и стоимость
  // также тип доставки автоматически сохраняется  в $_SESSION['arr_form_values']['type_shiping']
  // рассчитанные тут параметры price_shiping и  text_shiping далее будут сохранены в смете заказа
  function set_type_chiping($type) {$this->set_type_shiping($type);}
  function set_type_shiping($type) { global $orders_system ;  $orders_system->set_type_shiping($type) ; }

  // добавляем услугу в корзину. Описание услуги добавляется в список услуг, цена добавляется в массив цен
  function add_service($code,$rec)
  { if (!$rec['price']) $rec['price']=0 ;
    $this->service[$code]=array('id'        =>  $rec['id'],
                                'obj_name'  =>  ($rec['admin'])? $rec['admin']:$rec['obj_name'],
                                'price'     =>  $rec['price'],
                                'method'    =>  $rec['method'],
                                '__price'   =>  $rec['price']) ;
    $this->service[$code]['__price']=$rec['price'] ;
  }

  function remove_service($code)
  {
    unset($this->service[$code]);
    unset($this->result_price[$code]) ;
  }


}

// пока вынесено просто в класс, далее сделать полноценную систему
class parther_system
{
    // начисляем партнерский процент на сумму заказа
    function set_part_proc_to_goods($use_cart,$goods_sum_price)
    { if ($_SESSION['member']->info['part_proc'])
        { $value=$_SESSION['member']->info['part_proc']*$goods_sum_price/100 ;
          $use_cart->partner_premia+=$value ;// суммируем клиентскую скидку
          $use_cart->partner_premia_info.=$goods_sum_price.'*'.$_SESSION['member']->info['part_proc'].'%='.$value.', ';
        }
}

}



?>