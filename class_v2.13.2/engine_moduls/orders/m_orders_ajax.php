<?php
include_once (_DIR_TO_ENGINE.'/c_page_XML_AJAX.php') ;
include_once (_DIR_TO_ENGINE.'/admin/i_admin_system.php') ;
class c_editor_orders_ajax extends c_page_XML_AJAX
{
   function get_list_orders($doc,$xml)
   { $options=array() ; $usl='' ; $arr_fields=array() ;  $arr_fields_goods=array() ;  $_usl_filter=array() ; $_usl_filter_goods=array() ;
     $title='Заказы ' ;
     if ($_POST['search_text'] and $_POST['target'])
     {   $arr_words=explode(' ',trim($_POST['search_text'])) ; // разбираем на пробелы
         switch($_POST['target'])
          { case 'number': $arr_fields=array('obj_name') ; $title.=' - номер или дата содержит "'.$_POST['search_text'].'"' ; break ;
            case 'client': $arr_fields=array('name','phone','email') ; $title.=' - ФИО, телефон, или почта содержит "'.$_POST['search_text'].'"' ; break ;
            case 'adres':  $arr_fields=array('adres') ; $title.=' - адрес содержит "'.$_POST['search_text'].'"' ; break ;
            case 'rekv':   $arr_fields=array('rekvezit') ; $title.=' - реквизиты содержат "'.$_POST['search_text'].'"' ;  break ;
            case 'goods':  $arr_fields_goods=array('art','obj_name') ; break ;
          }
         if (sizeof($arr_words)) foreach($arr_words as $word)
         { $_usl2=array() ;$_usl_goods=array() ;
           if (sizeof($arr_fields)) foreach($arr_fields as $fname) $_usl2[]="UCASE(".$fname.") rlike UCASE('[[:<:]]".mb_strtoupper($word)."')" ;
           if (sizeof($arr_fields_goods)) foreach($arr_fields_goods as $fname) $_usl_goods[]="UCASE(".$fname.") rlike UCASE('[[:<:]]".mb_strtoupper($word)."')" ;
           if (sizeof($_usl2)) $_usl_filter[]='('.implode(' or ',$_usl2).')' ;
           if (sizeof($_usl_goods)) $_usl_filter_goods[]='('.implode(' or ',$_usl_goods).')' ;
         }

         $res_orders_ids=array() ;
         //damp_array($_usl_filter_goods,1,-1) ;
         if (sizeof($_usl_filter_goods))
         {  $_usl_filter_goods[]='clss=200' ;
            $arr_goods_ids=execSQL('select pkey from obj_site_goods where '.implode(' and ',$_usl_filter_goods)) ;
            if (sizeof($arr_goods_ids)) foreach($arr_goods_ids as $rec_goods)
            {  $orders_ids=execSQL_line('select parent from obj_site_orders_goods where reffer="'.$rec_goods['_reffer'].'"') ;
               if (sizeof($orders_ids)) $res_orders_ids=array_merge($res_orders_ids,$orders_ids) ;
            }
            if (sizeof($res_orders_ids)) $_usl_filter[]='pkey in ('.implode(',',$res_orders_ids).')' ;
         }
         //damp_array($_usl_filter,1,-1) ;


     }
     if ($_POST['usl'])                  $usl=stripslashes($_POST['usl']) ;
     if ($_POST['show_van_rec_as_item']) $options['show_van_rec_as_item']=$_POST['show_van_rec_as_item'] ;
     if (sizeof($_usl_filter))           $options['usl_filter']=implode(' and ',$_usl_filter) ;
     $options['default_order']='c_data desc' ;
     $options['title']=$title ;
     $options['debug']=2 ;
     //echo 'usl='.$usl.'<br>' ;
     //damp_array($options,1,-1) ;
     // получаем и показывем строки
     $pages_info=_CLSS(82)->show_list_items($_SESSION['orders_system']->tkey,$usl,$options) ;
      // выводим информацию по отображенным строкам
      add_element($doc,$xml,'list_id',$_POST['list_id']) ;
      add_element($doc,$xml,'from_pos',$pages_info['from']) ;
      add_element($doc,$xml,'to_pos',$pages_info['to']) ;
      add_element($doc,$xml,'all_pos',$pages_info['all']) ;
      add_element($doc,$xml,'next_page',$pages_info['next_page']) ;
      add_element($doc,$xml,'info_count_text','1...'.$pages_info['to'].' из '.$pages_info['all'])  ;
   }
}
?>