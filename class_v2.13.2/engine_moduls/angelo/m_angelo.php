<?php
$__functions['init'][]='_angelo_site_vars' ;
$__functions['boot_site'][]='_angelo_site_boot' ;

// разовые пропуска выдаются только на зону текущего бюро пропусков
// центрольное бюро пропусков может выдать разовый пропуск на любую зону
function ANGELO() {return($_SESSION['angelo_system']);$_SESSION['angelo_system']=new c_angelo_system();return($_SESSION['angelo_system']);} ;



function _angelo_site_vars()
{   // роли модуля
    $_SESSION['ARR_roll'][10]=array('obj_name'=>'Начальник СБ',   'index_page'=>'/cab/bp/req/one/') ;
    $_SESSION['ARR_roll'][11]=array('obj_name'=>'Сотрудник СБ',   'index_page'=>'/cab/bp/req/one/') ;
    $_SESSION['ARR_roll'][12]=array('obj_name'=>'Сотрудник ОП',   'index_page'=>'/cab/bp/req/one/') ;


    $_SESSION['ARR_doc_type']=array( 1=>'Паспорт',
                                     2=>'Водительское удостоверение',
                                     3=>'Другое'
                                    ) ;

    $_SESSION['ARR_pol']=array( 0=>'Мужской',
                                1=>'Женский'
                               ) ;

}

function _angelo_site_boot($options)
{
  create_system_modul_obj('angelo',$options) ;
}


class c_angelo_system  
{
   public $table_personal ;
   public $table_persons ;
   public $tkey_personal ;
   public $tkey_personal_img ;
   public $table_req_one;
   public $table_req_one_persons ;

   function c_angelo_system($create_options=array())
   { global $site_TM_kode ;
     $this->table_name ='obj_'.$site_TM_kode.'_persons' ;
     

     //регистрируем отчеты
     $this->system_title ='Работа фотографов';
     REPORTS()->reg_reports($this,'stats1',      'angelo_photo/report_stats1.php','report_stats1') ;         //
     REPORTS()->reg_reports($this,'stats2',      'angelo_photo/report_stats2.php','report_stats2') ;         //

     // REPORTS()->reg_reports($this,'persons',        'aspmo_angelo/report_persons.php','report_persons') ;         //
     //REPORTS()->reg_reports($this,'persons_blacklist','aspmo_angelo/report_persons_blacklist.php','report_persons_blacklist') ;         //
     //REPORTS()->reg_reports($this,'report_pass',     'aspmo_angelo/report_pass.php','report_pass') ;         //
     //REPORTS()->reg_reports($this,'pass_one',       'aspmo_angelo/report_pass_one.php','report_pass_one') ;         //
     //REPORTS()->reg_reports($this,'pass_time',      'aspmo_angelo/report_pass_time.php','report_pass_time') ;         //
     //REPORTS()->reg_reports($this,'car_pass_time',      'bp_car/report_car_pass_time.php','report_car_pass_time') ;         //
     //REPORTS()->reg_reports($this,'car_req_time',      'bp_car/report_car_req_time.php','report_car_req_time') ;         //
     //REPORTS()->reg_reports($this,'cars',               'bp_car/report_cars.php','report_cars') ;         //
     //REPORTS()->reg_reports($this,'req_one',        'aspmo_angelo/report_req_one.php','report_req_one') ;         //
     //REPORTS()->reg_reports($this,'req_time',       'aspmo_angelo/report_req_time.php','report_req_time') ;         //
     //REPORTS()->reg_reports($this,'pass_stats',     'aspmo_angelo/report_pass_stats.php','report_pass_stats') ;         //
     //REPORTS()->reg_reports($this,'report_inside',     'aspmo_angelo/report_inside.php','report_inside') ;         //
     //REPORTS()->reg_reports($this,'report_ext_orgs',     'aspmo_angelo/report_ext_orgs.php','report_ext_orgs') ;         //


     //ENGINE()->reg_ext('aspmo_angelo','Бюро пропусков') ;
     //ENGINE()->reg_ext('bp_car','Бюро пропусков - автомобили') ;
     //ESMO()->reg_reports($this,'bill_mo','aspmo_bill/report_bill_mo.php','report_bill_mo') ;         //

     //$this->pages_info=array() ;

   }

   function declare_menu_items_1($page)
   {   $suff=(MEMBER()->cur_zone_id)? '[zone_id='.MEMBER()->cur_zone_id.']/':'' ;
       /*
       $page->menu['/cab/angelo/req/one/']=array('title'=>'Разовые пропуска',
                                             'info_id'=>'info_req_one',
                                          'items'=>array('/cab/bp/req/one/'.$suff=>array('title'=>'Список заявок','info_id'=>'info_req_one_status_2'),
                                                         '/cab/bp/req/one/to_moderate/'.$suff=>array('title'=>'Список заявок на согласование','info_id'=>'info_req_one_status_1'),
                                                         '/cab/bp/report/pass/one/'.$suff=>'Разовые пропуска'
                                                        )
                                         ) ;
       $page->menu['/cab/bp/req/time/']=array('title'=>'Временные пропуска',
                                          'items'=>array(// временнные пропуска на сотрудников
                                                         '/cab/bp/req/time/'.$suff=>'Список заявок',
                                                         '/cab/bp/req/time/create/'.$suff=>'Создать заявку',
                                                         '/cab/bp/report/pass/time/'.$suff=>'Временные пропуска',
                                                         // временные пропуска на авто
                                                         '/cab/reports/car_req_time/'.$suff=>'Список заявок на автомобиль',
                                                         '/cab/bp_car/req/time/create/'.$suff=>'Создать заявку на автомобиль',
                                                         '/cab/reports/car_pass_time/'.$suff=>'Временные пропуска на авто',
                                                        )
                                         ) ;

       $page->menu['/cab/bp/pass/tmc/register/']=array('title'=>'Другие пропуска',
                                          'items'=>array('/cab/bp/pass/tmc/register/'.$suff=>'Пропуск ТМЦ',
                                                         //'/cab/bp/req/time/create/'.$suff=>'Создать заявку',
                                                         //'/cab/bp/report/pass/time/'.$suff=>'Временные пропуска'
                                                        )
                                         ) ;
       //$page->menu['/cab/bp/static/']=array('title'=>'Постоянные пропуска',
       //                                   'items'=>array('/cab/bp/van/'=>'Выдача постоянного пропуска')
       //                                  ) ;
       */

   }

   function declare_menu_items_2($page)
   {
       $page->menu['/cab/reports/stats1/']=array('title'=>'Сводная статистика') ;

       /*$page->menu['/cab/bp/report/']=array('title'=>'Справочники',
                                            'class'=>'fa fa-binoculars',
                                             'items'=>array(//'/cab/bp/report/personals/'=>'Сотрудники',
                                                            '/cab/bp/report/persons/'=>'Посетители',
                                                            '/cab/bp/report/persons/blacklist/'=>'Черный список',
                                                            '/cab/bp/report/req/one/'=>'Заявки на разовые пропуска',
                                                            '/cab/bp/report/req/time/'=>'Заявки на временные пропуска',
                                                            '/cab/bp/report/pass/one/'=>'Разовые пропуска',
                                                            '/cab/bp/report/pass/time/'=>'Временные пропуска'
                                                            )
                                         ) ;

       /*$page->menu['/cab/bp/report/']['items']['/cab/bp/report/persons/blacklist/']=array('title'=>'Черный список',
                                                                                        'items'=>array('/cab/bp/personals/black_list/add/'=>'Добавить в черный список',
                                                                                                      )
                                                                                        ) ;

       /*
       $page->menu['/cab/bp/report/']['items']['/cab/bp/report/personals/']=array('title'=>'Сотрудники',
                                                                                  'items'=>array('/cab/bp/report/personals/'=>'Поиск сотрудников',
                                                                                                 '/cab/bp/personals/create/'=>'Добавить сотрудника',
                                                                                                )
                                                                                  ) ;
      */

   }

    function declare_menu_items_4($page)
    {
        //$page->menu['/cab/setting/']['items']['/cab/bp/setting/moderation/']=array('title'=>'Согласование заявок на разовые пропуска') ;
        //$page->menu['/cab/setting/']['items']['/cab/bp/setting/arm_zone/']=array('title'=>'Задание местонахождения АРМ') ;
    }

//==============================================================================================================================
//
// ЗАЯВКИ НА РАЗОВЫЕ ПРОПУСКА
//
//==============================================================================================================================


  // регистрация заявки на разовый пропуск
  // вывывается только из портала
  function register_req_one($data,$recs_persons)
  {  $rec_autor=execSQL_van('select * from obj_site_personals where pkey='.$data['autor_id']) ;
     //damp_array($data,1,-1) ;
     $time_from=($data['time_from']<time())? time():$data['time_from'] ;
     $now=getdate($time_from) ;
     $time_from=mktime(0,0,0,$now['mon'],$now['mday'],$now['year']);
     $time_to=mktime(23,59,59,$now['mon'],$now['mday'],$now['year']);
     if ($data['to_personal_name']) $data['to_personal_id']=0 ;
     $cnt_allow=0 ;  $arr_pers_to_moderation=array() ; $req_status=1 ; // на утверждении
     $req_id=adding_rec_to_table('obj_site_request_pass',
                             array('parent'=>3,
                                   'clss'=>503,
                                   'autor_id'=>$rec_autor['pkey'],
                                   'autor_HR_ID'=>$rec_autor['HR_ID'],
                                   'autor_name'=>$rec_autor['obj_name'],
                                   'to_zone_id'=>$data['to_zone_id'],
                                   'to_org_id'=>$data['to_org_id'],
                                   'to_org_name'=>$data['to_org_name'],
                                   'to_otdel_id'=>$data['to_otdel_id'],
                                   'to_personal_id'=>$data['to_personal_id'],
                                   'to_personal_name'=>$data['to_personal_name'],
                                   'time_from'=>$time_from,
                                   'time_to'=>$time_to,
                                   'status'=>0,  // по умолчанию заявка ожидает модерации
                                   'comment'=>$data['comment']
                             ),array('debug'=>0))  ;
     $req_reffer=$req_id.'.324' ;
     // проверяем, модерируется ли зона верхнего уровня
     $zone_is_moderation=SKD()->check_zone_to_moderation($data['to_zone_id']) ;
     //
     foreach($recs_persons as $rec_person)
     { // по умолнянию заявка на персону одобрена
       $status=2 ;
       // но исключение составляют зоны доступа, на которые требуется модерация посторонних лиц
       if ($zone_is_moderation and !$rec_person['personal_id']) $status=1 ;

       adding_rec_to_table('obj_site_request_pass_persons',
          array('parent'=>$req_id,
                'clss'=>504,
                'obj_name'=>$rec_person['name1'].' '.$rec_person['name2'].' '.$rec_person['name3'],
                'name1'=>$rec_person['name1'],
                'name2'=>$rec_person['name2'],
                'name3'=>$rec_person['name3'],
                'personal_id'=>($rec_person['personal_id'])? $rec_person['personal_id']:0,
                'status'=>$status // на утверждаении
          ),array('debug'=>0))  ;

         if ($status==2) $cnt_allow++ ; ;
       // составляем список ID посетителей, которые будут отправлены на модерацию
       if ($status==1) $arr_pers_to_moderation[]='<li>'.$rec_person['name1'].' '.$rec_person['name2'].' '.$rec_person['name3'].'</li>' ;
     }
     // если все персоны в зявке одобрены - выводим заявку как одобренную
     if ($cnt_allow==sizeof($recs_persons))
     {  $req_status=2 ;   // статус 2 - заявка одобрена
        update_rec_in_table('obj_site_request_pass',array('status'=>$req_status),'pkey='.$req_id) ;
     }
      // регистрируем событие
      LOGS()->reg_log('Создание заявки на РП',$_SESSION['ARR_req_status'][$req_status],array('_data'=>$data,'reffers'=>array($rec_autor['_reffer'],$req_reffer))) ;

      // если есть посетители, которых надо модерировать - отправляем заявку на модерацию
      if (sizeof($arr_pers_to_moderation)) SKD()->send_req_moderation_email($data['to_zone_id'],$req_id,$arr_pers_to_moderation) ;
      return($req_id) ;


  }

  function get_req_one_by_id($id)
  {
      $rec=execSQL_van('select * from view_req_one where pkey='.$id) ;
      if ($rec['pkey']) $this->prepare_public_info_to_req_one($rec) ;
      return($rec) ;
  }

    function prepare_public_info_to_req_one(&$rec,$options=array())
        { $rec['__href']='/cab/bp/req/one/'.$rec['pkey'].'/' ;
          $rec['__href_window']='/window/bp/req/one/'.$rec['pkey'].'/' ;
          if (!isset($rec['_persons'])) $rec['_persons']=execSQL('select * from '.BP()->table_req_one_persons.' where parent='.$rec['pkey'].' order by pkey') ;
          //if ($rec['pkey']) $rec['_persons']=execSQL('select pkey,obj_name,name1,name2,name3,status from obj_site_request_pass_persons where parent='.$rec['pkey'].' order by pkey') ;
          if (sizeof($rec['_persons'])) foreach($rec['_persons'] as $id_person=>$rec_person) $rec['_persons'][$id_person]['_status_name']=$_SESSION['ARR_req_status'][$rec_person['status']] ;

          if ($rec['autor_id']) $rec['_autor']=PERSONAL()->get_personal_by_id($rec['autor_id']) ;
          $rec['_status_name']=$_SESSION['ARR_req_status'][$rec['status']] ;
          if ($rec['to_org_name']) $rec['_to_org_name']=$rec['to_org_name'] ;
          if ($rec['to_personal_name']) $rec['_to_personal_name']=$rec['to_personal_name'] ;
          if ($rec['to_personal_working']) $rec['_to_personal_working']=$rec['to_personal_working'] ;


          if (isset(SKD()->tree[$rec['to_zone_id']]))
          { $arr_names=SKD()->tree[$rec['to_zone_id']]->get_arr_parent_name(array('to_level'=>1))  ;
            if (!sizeof($arr_names)) $arr_names[]=SKD()->tree[$rec['to_zone_id']]->name ;
            $rec['_to_zone_name']=implode(' / ',$arr_names);
          }

        }

    // получить заявку на пропуск на основании ID записи по человеку в заявке
    function get_req_one_person($id,$options=array())
    { $rec_req=array() ;
      if ($id)
       { $rec_req=execSQL_van('select * from view_req_one_persons where id='.$id) ;
         $this->prepare_public_info_to_req_one_person($rec_req) ;
       }
      return($rec_req) ;
    }

    function prepare_public_info_to_req_one_person(&$rec,$options=array())
        { $rec['__href']='/cab/bp/req/one/'.$rec['pkey'].'/' ;
          $rec['__href_window']='/window/bp/req/one/'.$rec['pkey'].'/' ;

          $rec['_person_status_name']=$_SESSION['ARR_req_status'][$rec['person_status']] ;
          if (time()>$rec['time_to'] and !$rec['pass_id']) $rec['_person_status_name'].=', просрочена' ;
          $rec['_status_name']=$_SESSION['ARR_req_status'][$rec['status']] ;
          if (time()>$rec['time_to'] and !$rec['pass_id']) $rec['_status_name'].=', просрочена' ;

          $rec['_srok']=date('d.m.Y',$rec['time_from']) ;
          $from=getdate($rec['time_from']) ;
          $to=getdate($rec['time_to']) ;
          if ($from['mon']=!$to['mon'] or $from['mday']!=$to['mday'] or $from['year']!=$to['year'])  $rec['_srok'].=' - '.date('d.m.Y',$rec['time_to']) ;

          // в целях совместимости с заявками на временные пропуска, в которых сразу указана ID персоны
          $rec['_person']['name1']=$rec['name1'];
          $rec['_person']['name2']=$rec['name2'];
          $rec['_person']['name3']=$rec['name3'];

          if ($rec['to_org_name']) $rec['_to_org_name']=$rec['to_org_name'] ;
          if ($rec['to_personal_name']) $rec['_to_personal_name']=$rec['to_personal_name'] ;

          if (isset(SKD()->tree[$rec['to_zone_id']]))
          {   $arr_names=SKD()->tree[$rec['to_zone_id']]->get_arr_parent_name(array('to_level'=>1))  ;
              if (!sizeof($arr_names)) $arr_names[]=SKD()->tree[$rec['to_zone_id']]->name ;
              $rec['_to_zone_name']=implode(' / ',$arr_names);
          }

          if ($rec['ext_info']) $rec['_ext_info']=unserialize($rec['ext_info']) ;
          if ($rec['pass_id']) $rec['_pass_info']='Пропуск № '.$rec['pass_id'].'<br>'.$_SESSION['ARR_pass_status'][$rec['pass_status']] ;

        }

//==============================================================================================================================
//
//  РАЗОВЫЕ ПРОПУСКА
//
//==============================================================================================================================

    // создаем разовый пропуск
      function create_pass_one($person_reffer,$form_data)
      { list($person_id,$tkey)=explode('.',$person_reffer) ;

        $time_from=time();           // доступ с текущего времени
        $now=getdate($time_from) ;
        $time_to=mktime(23,59,59,$now['mon'],$now['mday'],$now['year']); // и до конца текущего дня
        $data=array('parent'=>1,
                    'clss'=>500,
                    'member_id'=>MEMBER()->id,
                    'personal_id'=>$person_id,
                    'arm_id'=>MEMBER()->cur_zone_id,
                    'time_from'=>time(),
                    'time_to'=>$time_to,
                    'obj_name'=>$form_data['personal_name'],
                    'person_fio'=>$form_data['person_fio'],
                    'person_company'=>$form_data['person_company'],
                    'person_working'=>$form_data['person_working'],
                    'status'=>3, // выдан
                    'uid'=>md5(uniqid(rand(),true)),
                    'uid2'=>$this->get_UID_to_QR_to_pass_one()
                   ) ; // damp_array($form_data,1,-1) ;
        if (isset($form_data['to_zone_id']))  $data['to_zone_id']=$form_data['to_zone_id'] ;
        if (isset($form_data['to_org_id']))   $data['to_org_id']=$form_data['to_org_id'] ;
        if (isset($form_data['to_org_name']))   $data['to_org_name']=$form_data['to_org_name'] ;
        if (isset($form_data['to_otdel_id'])) $data['to_otdel_id']=$form_data['to_otdel_id'] ;
        if (isset($form_data['to_personal_name']))  $data['to_person_name']=$form_data['to_personal_name'] ;
        if (isset($form_data['to_personal_id']))  $data['to_personal_id']=$form_data['to_personal_id'] ;
        if (isset($form_data['comment']))     $data['comment']=$form_data['comment'] ;
        if (isset($form_data['portfel']))     $data['ext_info']['Пронос'][]='Портфель' ;
        if (isset($form_data['notebook']))    $data['ext_info']['Пронос'][]='Ноутбук' ;


        $pass_id=adding_rec_to_table($this->table_pass_van,$data,array('debug'=>0)) ;
        // вписываем в заявку номер выданного пропуска - в запись по персоне
        if ($form_data['req_id']) update_rec_in_table(BP()->table_req_van_persons,array('pass_id'=>$pass_id),'pkey='.$form_data['req_id']) ;
        $pass_reffer=$pass_id.'.'._DOT($this->table_pass_van)->pkey ;
        $req_reffer=$form_data['req_id'].'.'._DOT(BP()->table_req_van_persons)->pkey ;
        LOGS()->reg_log('Создание разового пропуска','',array('reffers'=>array($person_reffer,$pass_reffer,$req_reffer))) ;

        return($pass_id) ;
      }

    // изменяем разовый пропуск
      function save_pass_van($pass_id,$form_data)
      { ob_start() ;
        //list($person_id,$tkey)=explode('.',$person_reffer) ;
        $rec_pass=BP()->get_pass_one_by_id($_POST['pass_id']) ;
        //damp_array($form_data) ;

        //$time_from=time();           // доступ с текущего времени
        //$now=getdate($time_from) ;
        //$time_to=mktime(23,59,59,$now['mon'],$now['mday'],$now['year']); // и до конца текущего дня
        $data=array() ;   $arr_change=array() ;
        if (isset($form_data['to_zone_id']) and $form_data['to_zone_id']!=$rec_pass['to_zone_id']) { $data['to_zone_id']=$form_data['to_zone_id'] ; $arr_change['Зона доступа']=SKD()->get_full_zone_path($rec_pass['to_zone_id']).' => '.SKD()->get_full_zone_path($data['to_zone_id']) ; }
        if ($form_data['k_komu']=='UK')
        { $rec_personal=PERSONAL()->get_personal_by_id($form_data['to_personal_id']) ;
          if (isset($form_data['to_personal_id']) and $rec_personal['pkey'] and $rec_personal['pkey']!=$rec_pass['to_personal_id'])
          {  $data['to_personal_id']=$rec_personal['pkey'] ; $arr_change['К кому']=$rec_pass['_to_personal_name'].' => '.$rec_personal['obj_name'].', '.$rec_personal['_org_name'].', '.$rec_personal['_otdel_name'] ;
             $data['to_org_id']=$rec_personal['_org_id'] ;
             $data['to_otdel_id']=$rec_personal['_otdel_id'] ;
             $data['to_person_name']='' ;
             $data['to_org_name']='' ;
          }
        }
        else
        { if (isset($form_data['to_person_name']) and $form_data['to_person_name']!=$rec_pass['to_person_name']) { $data['to_person_name']=$form_data['to_person_name'] ; $arr_change['К кому - ФИО']=$rec_pass['to_person_name'].' => '.$form_data['to_person_name'] ; }
          if (isset($form_data['to_org_name']) and $form_data['to_org_name']!=$rec_pass['to_org_name']) { $data['to_org_name']=$form_data['to_org_name'] ; $arr_change['К кому - Организация']=$rec_pass['to_org_name'].' => '.$form_data['to_org_name'] ; }
          $data['to_personal_id']=0 ;
          $data['to_org_id']=0 ;
          $data['to_otdel_id']=0;
        }
        //if (isset($form_data['to_org_id']))   $data['to_org_id']=$form_data['to_org_id'] ;
        //if (isset($form_data['to_org_name']))   $data['to_org_name']=$form_data['to_org_name'] ;
        //if (isset($form_data['to_otdel_id'])) $data['to_otdel_id']=$form_data['to_otdel_id'] ;
        //if (isset($form_data['to_personal_name']))  $data['to_person_name']=$form_data['to_personal_name'] ;
        //if (isset($form_data['to_personal_id']))  $data['to_personal_id']=$form_data['to_personal_id'] ;
        if (isset($form_data['comment']) and $form_data['comment']!=$rec_pass['comment'])  {  $data['comment']=$form_data['comment'] ; $arr_change['Комментарий']=$rec_pass['comment'].' => '.$data['comment'] ; }
        //if (isset($form_data['portfel']))
        $data['ext_info']=$rec_pass['_ext_info'] ;
        if ($form_data['portfel'] and !$rec_pass['_ext_info']['Пронос']['portfel']) { $arr_change['Пронос портфеля']='Нет => Да' ; $data['ext_info']['Пронос']['portfel']='Портфель' ; }
        if (!$form_data['portfel'] and $rec_pass['_ext_info']['Пронос']['portfel']) { $arr_change['Пронос портфеля']='Да => Нет' ; unset($data['ext_info']['Пронос']['portfel']) ;  }
        if ($form_data['notebook'] and !$rec_pass['_ext_info']['Пронос']['notebook']) { $arr_change['Пронос ноутбука']='Нет => Да' ; $data['ext_info']['Пронос']['notebook']='Ноутбук' ; }
        if (!$form_data['notebook'] and $rec_pass['_ext_info']['Пронос']['notebook']) { $arr_change['Пронос ноутбука']='Да => Нет' ; unset($data['ext_info']['Пронос']['notebook']) ;  }

        if (sizeof($data))
        { $res=update_rec_in_table($this->table_pass_van,$data,'pkey='.$rec_pass['pkey'],array('debug'=>0)) ;
        }


          unset($res['sql']) ;
          damp_array($res,1,-1) ;
         /* ?><strong>data</strong><?
          damp_array($data,1,-1) ;
          ?><strong>form_data</strong><?
          damp_array($form_data,1,-1) ;
          ?><strong>ext_info</strong><?
          damp_array($rec_pass['ext_info'],1,-1) ;*/
          $text=ob_get_clean()  ;
          LOGS()->reg_log('Изменение пропуска',$text,array('data'=>$arr_change,'reffers'=>array($rec_pass['_reffer']))) ;

        // вписываем в заявку номер выданного пропуска - в запись по персоне
        //if ($form_data['req_id']) update_rec_in_table(BP()->table_req_van_persons,array('pass_id'=>$pass_id),'pkey='.$form_data['req_id']) ;

        return($pass_id) ;
      }

     function get_UID_to_QR_to_pass_one()
     {  $rand=rand(1,1000000) ;
        $id=execSQL_van('select pkey from '.BP()->table_pass_one.' where uid2='.$rand) ;
        if ($id)
        { $rand=rand(1000000,10000000) ;
          $id=execSQL_van('select pkey from '.BP()->table_pass_one.' where uid2='.$rand) ;
          if ($id)
          { $rand=rand(10000000,100000000) ;
            $id=execSQL_van('select pkey from '.BP()->table_pass_one.' where uid2='.$rand) ;
          }
        }
        $text=sprintf ("%010d",$rand) ; // дополняем пустыми значениями для фиксированного длины UID пропуска
        return($text) ;
     }

     function get_UID_to_QR_to_pass_time()
     {  $rand=rand(1,1000000) ;
        $id=execSQL_van('select pkey from '.BP()->table_pass_time.' where uid2='.$rand) ;
        if ($id)
        { $rand=rand(1000000,10000000) ;
          $id=execSQL_van('select pkey from '.BP()->table_pass_time.' where uid2='.$rand) ;
          if ($id)
          { $rand=rand(10000000,100000000) ;
            $id=execSQL_van('select pkey from '.BP()->table_pass_time.' where uid2='.$rand) ;
          }
        }
        $text=sprintf ("%010d",$rand) ; // дополняем пустыми значениями для фиксированного длины UID пропуска
        return($text) ;
     }

     function get_pass_one_by_id($id,$options=array())
     { $pass_number=(int) $id ;
       ob_start() ;
       //$options['debug']=0 ;
       $rec=execSQL_van('select * from '.$this->view_pass_one.' where pkey="'.$pass_number.'"',$options) ;
       if ($rec['pkey']) $this->prepare_public_info_to_pass_one($rec,$options) ;
       $text=ob_get_clean()  ;
       if ($options['debug']) echo $text ;
       //LOGS()->reg_log('get_pass_one_by_id',$text,array('','reffers'=>array($rec['_reffer'],$rec['_person']['_reffer']))) ;
       return($rec) ;
     }

     function get_pass_one_by_uid($uid,$options=array())
     { $rec=array() ;  $pass_number=(int) $uid ;
       ob_start() ;
       //$options['debug']=2 ;
       if (mb_strlen($uid)==10) $rec=execSQL_van('select * from '.$this->view_pass_one.' where uid2="'.$pass_number.'"',$options) ;
       //else if (strlen($uid)==31) $rec=execSQL_van('select * from '.$this->view_pass_one.' where uid like "'.$pass_number.'%"',$options) ;
       //elseif (strlen($uid)==32) $rec=execSQL_van('select * from '.$this->view_pass_one.' where uid="'.$pass_number.'"',$options) ;
       if ($rec['pkey']) $this->prepare_public_info_to_pass_one($rec,$options) ;
       //if ($options['debug']) echo 'pass_uid(id): "'.$uid.'"<br>длина пропуска: '.mb_strlen($uid).'<br>Результат поиска: '.(($rec['pkey'])? '<span class="green">'.$rec['pkey'].'</span>':'<span class="red">Не найден</span>') ;
       //$text=ob_get_clean()  ;
       //if ($options['debug']) echo $text ;
       //LOGS()->reg_log('get_pass_one_by_uid',$text,array('','reffers'=>array($rec['_reffer'],$rec['_person']['_reffer']))) ;
       return($rec) ;
     }

     // возвращем список пропусков для указанных персон (ids)
     function get_pass_van_history($person_id,$options=array())
     { $recs=execSQL('select * from view_pass_one_history where _person_id='.$person_id.' order by c_data desc') ;
       if (sizeof($recs)) foreach($recs as $i=>$rec)   $this->prepare_public_info_to_pass_one($recs[$i],$options) ;
       return($recs) ;
     }



     function prepare_public_info_to_pass_one(&$rec,$options=array())
     { $rec['__href']='/cab/bp/pass/one/'.$rec['pkey'].'/' ;
       $rec['__href_window']='/window/bp/pass/one/'.$rec['pkey'].'/' ;
       $rec['_person']=$this->get_personal_by_id($rec['personal_id'],array('debug'=>0)) ;
       if ($rec['_person']['pkey'])
       { $rec['_object_id']=$rec['_person']['pkey'];
         $rec['_object_name']=$rec['_person']['obj_name'];
         $rec['_object_reffer']=$rec['_person']['_reffer'];
       }

       // для случая, когда название организации и принимающей персоны указаны строкой
       if ($rec['to_org_name']) $rec['_to_org_name']=$rec['to_org_name'] ;
       if ($rec['to_person_name']) $rec['_to_personal_name']=$rec['to_person_name'] ;
       //
       $rec['_to_personal_name_FIO']=ENGINE()->convert_full_name_to_FIO($rec['_to_personal_name']) ;

       if (isset(SKD()->tree[$rec['to_zone_id']]))
       {   $arr_names=SKD()->tree[$rec['to_zone_id']]->get_arr_parent_name(array('to_level'=>1))  ;
           if (!sizeof($arr_names)) $arr_names[]=SKD()->tree[$rec['to_zone_id']]->name ;
           $rec['_to_zone_name']=implode(' / ',$arr_names);
           $rec['_allow_zones']=SKD()->tree[$rec['to_zone_id']]->get_arr_child() ;
       }
       if ($rec['ext_info']) $rec['_ext_info']=unserialize($rec['ext_info']) ;

       $rec['_status']=$_SESSION['ARR_pass_status'][$rec['status']] ;
       $rec['_srok']=date('d.m.Y',$rec['time_from']) ;
       $from=getdate($rec['time_from']) ;
       $to=getdate($rec['time_to']) ;
       if ($from['mon']=!$to['mon'] or $from['mday']!=$to['mday'] or $from['year']!=$to['year'])  $rec['_srok'].=' - '.date('d.m.Y',$rec['time_to']) ;

       // данные доступы через вьювер
       //if (!$rec['time_in']) $rec['time_in']=execSQL_value('select c_data from '.SKD()->table_gate_events.' where pass_type=1 and pass_id='.$rec['pkey'].' and access_type=0') ;
       //if (!$rec['time_out']) $rec['time_out']=execSQL_value('select c_data from '.SKD()->table_gate_events.' where pass_type=1 and pass_id='.$rec['pkey'].' and access_type=1') ;

     }


//==============================================================================================================================
//
//  ВРЕМЕННЫЕ ПРОПУСКА
//
//==============================================================================================================================

  // создаем запрос на временный пропуск - по факту черновик временного пропуска
  function create_req_time($obj_reffer,$form_data)
  { list($id,$tkey)=explode('.',$obj_reffer) ;
    $time_from=strtotime($form_data['data_from']) ;
    $time_to=strtotime($form_data['data_to'].' 23:59:59') ;
    $data=array('parent'=>1,
                'member_id'=>MEMBER()->id,
                'time_from'=>$time_from,
                'time_to'=>$time_to,
                'obj_name'=>$form_data['personal_name'],
                'contact_name'=>$form_data['contact_name'],
                'contact_phone'=>$form_data['contact_phone'],
                'status'=>1 // черновик
               ) ;
    if ($tkey==_DOT('obj_site_persons')->pkey) { $data['personal_id']=$id ; $data['clss']=501 ; }
    if ($tkey==_DOT('obj_site_cars')->pkey) { $data['personal_id']=$id ; $data['clss']=507 ; }
    if (isset($form_data['boss_type']))   $data['boss_type']=$form_data['boss_type'] ;
    if (isset($form_data['boss_id']))   $data['boss_id']=$form_data['boss_id'] ;

    //damp_array($data) ;

    if (isset($form_data['comment']))     $data['comment']=$form_data['comment'] ;

    $pass_id=adding_rec_to_table($this->table_pass_time,$data,array('debug'=>0)) ;
    // создаем записи по зонам доступа в таблицe PDP, 2 -  временный пропуск
    if (sizeof($form_data['zone'])) foreach($form_data['zone'] as $zone_id=>$access) if ($access)
        $this->create_pdp_rec($id,$zone_id,$pass_id,2,$time_from,$time_to) ;

    if ($data['clss']==501)
    {   $rec_pass=BP()->get_pass_time_by_id($pass_id) ;
        LOGS()->reg_log('Создание заявки на временный пропуск','',array('reffers'=>$rec_pass['_reffers'])) ;
    }
    if ($data['clss']==507)
    {   $rec_pass=BP()->get_pass_car_time_by_id($pass_id) ;
        LOGS()->reg_log('Создание заявки на временный пропуск на автомобиль','',array('reffers'=>$rec_pass['_reffers'])) ;
    }
    return($pass_id) ;
  }

  // редактируем параметры временного пропуска
  function save_pass_time($rec_pass,$form_data)
  { $time_from=strtotime($form_data['data_from']) ;
    $time_to=strtotime($form_data['data_to'].' 23:59:59') ;
    $data=array() ; $log=array() ; $zone_add=array() ; $zone_del=array() ;
    if (isset($form_data['arm_id'])) $data['arm_id']=$form_data['arm_id'] ;
    if (isset($form_data['person_fio'])) $data['person_fio']=$form_data['person_fio'] ;
    if (isset($form_data['person_company'])) $data['person_company']=$form_data['person_company'] ;
    if (isset($form_data['person_working'])) $data['person_working']=$form_data['person_working'] ;
    if (isset($form_data['s_data'])) $data['s_data']=$form_data['s_data'] ;

    //проверяем наличия изменений в зонах доступа
    //print_r($form_data['zone']) ;  echo '<br>' ;
    //print_r($rec_pass['_main_zones']) ;  echo '<br>' ;
    if (sizeof($form_data['zone'])) foreach($form_data['zone'] as $zone_id=>$value) if (!isset($rec_pass['_main_zones'][$zone_id])) $zone_add[]=SKD()->get_full_zone_path($zone_id) ;
    if (sizeof($rec_pass['_main_zones'])) foreach($rec_pass['_main_zones'] as $zone_id=>$value) if (!isset($form_data['zone'][$zone_id])) $zone_del[]=SKD()->get_full_zone_path($zone_id) ;
    //print_r($zone_add) ;  echo '<br>' ;
    //print_r($zone_del) ;  echo '<br>' ;

    if (sizeof($zone_add) or sizeof($zone_del))
    { // удаляем старые записи по зонам доступа в таблице PDP
      $this->delete_pdp_rec(array('person_id'=>$rec_pass['personal_id'],'pass_id'=>$rec_pass['pkey'],'pass_type'=>2)) ;
      // создаем записи по зонам доступа в таблицe PDP, 2 -  временный пропуск
      if (sizeof($form_data['zone'])) foreach($form_data['zone'] as $zone_id=>$access) if ($access)
       $this->create_pdp_rec($rec_pass['personal_id'],$zone_id,$rec_pass['pkey'],2,$time_from,$time_to) ;
    }
    if (sizeof($zone_add)) $log['Добавлены зоны доступа']=$zone_add;
    if (sizeof($zone_del)) $log['Удалены зоны доступа']=$zone_del;

    if ($rec_pass['time_from']!=$time_from)           { $data['time_from']=$time_from ;   $log['Срок действия, от']=date('d.m.Y',$rec_pass['time_from']).' => '.date('d.m.Y',$time_from);}
    if ($rec_pass['time_to']!=$time_to)               { $data['time_to']=$time_to ;       $log['Срок действия, до']=date('d.m.Y',$rec_pass['time_to']).' => '.date('d.m.Y',$time_to);}
    if ($rec_pass['comment']!=$form_data['comment'])  { $data['comment']=$form_data['comment'] ;       $log['Комментарий']=$rec_pass['comment'].' => '.$form_data['comment'];}
    if ($rec_pass['contact_name']!=$form_data['contact_name'])  { $data['contact_name']=$form_data['contact_name'] ;       $log['Контактное лицо']=$rec_pass['contact_name'].' => '.$form_data['contact_name'];}
    if ($rec_pass['contact_phone']!=$form_data['contact_phone'])  { $data['contact_phone']=$form_data['contact_phone'] ;       $log['Контактный телефон']=$rec_pass['contact_phone'].' => '.$form_data['contact_phone'];}
    if ($rec_pass['boss_type']!=$form_data['boss_type'])  { $data['boss_type']=$form_data['boss_type'] ;       $log['Тип заказчика']=$rec_pass['boss_type'].' => '.$form_data['boss_type'];}
    if ($rec_pass['boss_id']!=$form_data['boss_id'])  { $data['boss_id']=$form_data['boss_id'] ;       $log['id заказчика']=$rec_pass['boss_id'].' => '.$form_data['boss_id'];}
    if (sizeof($log))
    { update_rec_in_table($this->table_pass_time,$data,'pkey='.$rec_pass['pkey']) ;
      LOGS()->reg_log('Изменение временного пропуска','Временный пропуск '.$rec_pass['pkey'].' '.$rec_pass['_person_fio'],array('data'=>$log,'reffers'=>$rec_pass['_reffers'])) ;
    }

  }

  function get_req_time_by_id($id)
  { $rec=execSQL_van('select * from '.$this->view_req_time.' where pkey='.$id) ;
    if ($rec['pkey']) $this->prepare_public_info_to_pass_time($rec) ;
    return($rec) ;
  }

  function get_pass_time_by_id($uid,$options=array())
  { $pass_number=$uid ;
    //$pass_number=(int) $id ;
    if (mb_strlen($uid)==10) $rec=execSQL_van('select * from '.$this->view_pass_time.' where uid2="'.$uid.'"',$options) ;
    else                 $rec=execSQL_van('select * from '.$this->view_pass_time.' where pkey="'.$pass_number.'"',$options) ;
    //$rec=execSQL_van('select * from '.$this->view_pass_time.' where pkey='.$id.'') ;
    if ($rec['pkey']) $this->prepare_public_info_to_pass_time($rec,$options) ;
    return($rec) ;
  }

  function get_pass_time_by_uid($uid,$options=array())
  { $pass_number=(int) $uid ;
    if (strlen($uid)==10) $rec=execSQL_van('select * from '.$this->view_pass_time.' where uid2="'.$pass_number.'"',$options) ;
    else                  $rec=execSQL_van('select * from '.$this->view_pass_time.' where pkey="'.$pass_number.'"',$options) ;
    if ($rec['pkey']) $this->prepare_public_info_to_pass_time($rec,$options) ;
    return($rec) ;
  }

  function get_pass_car_time_by_id($id,$options=array())
  { $pass_number=(int) $id ;
    $rec=execSQL_van('select * from '.$this->view_pass_car_time.' where pkey="'.$pass_number.'"',$options) ;
    if ($rec['pkey']) $this->prepare_public_info_to_pass_time($rec,$options) ;
    return($rec) ;
  }

  function get_pass_car_time_by_uid($uid,$options=array())
  { //ob_start() ;
    $pass_number=(int) $uid ;
    $rec=array() ; //$options['debug']=2 ;
    if (strlen($uid)==10) $rec=execSQL_van('select * from '.$this->view_pass_car_time.' where uid2="'.$pass_number.'"',$options) ;
    if ($rec['pkey']) $this->prepare_public_info_to_pass_time($rec,$options) ;
    //$text=ob_get_clean() ;
    //LOGS()->reg_log('get_pass_car_time_by_uid',$text) ;
    return($rec) ;
  }

 // возвращем список пропусков для указанных персон (ids)
 /* не используется
 function get_pass_time_list_to_persons_ids($ids,$options=array())
 { $recs=array() ;
   if ($ids)
   { $recs=execSQL('select pkey as id,indx,personal_id,time_from,time_to,status from '.$this->table_pass_time.' where personal_id in ('.$ids.') order by c_data desc') ;
     if (sizeof($recs)) foreach($recs as $i=>$rec)
     { $recs[$i]['_status']=$_SESSION['ARR_pass_status'][$rec['status']] ;
       $recs[$i]['_srok']=date('d.m.Y',$rec['time_from']).' - '.date('d.m.Y',$rec['time_to']) ;
     }
   }
   return($recs) ;
 } */

 function prepare_public_info_to_pass_time(&$rec,$options=array())
 { //ob_start() ;
   if ($rec['clss']==501)
   { $rec['__href']='/cab/bp/pass/time/'.$rec['pkey'].'/' ;
     $rec['__href_window']='/window/bp/pass/time/'.$rec['pkey'].'/' ;
     $rec['_person']=$this->get_personal_by_id($rec['personal_id'],array('debug'=>0)) ;
     $rec['_reffers']=array($rec['_reffer'],$rec['_person']['_reffer']) ;

     if ($rec['_person']['pkey'])
       {  //$rec['_person']['pkey']=$rec['_person_id'] ;
          //$rec['_person']['obj_name']=$rec['_person_fio'] ;
          //$rec['_person']['company']=$rec['_person_company'] ;

          $rec['_object_id']=$rec['_person']['pkey'];
          $rec['_object_name']=$rec['_person']['obj_name'];
          $rec['_object_reffer']=$rec['_person']['_reffer'];
          $rec['_object_in_black_list']=($rec['_person']['black_list'])? 1:0;
       }
   }
   if ($rec['clss']==507)
   { $rec['__href']='/cab/bp_car/pass/time/'.$rec['pkey'].'/' ;
     $rec['__href_window']='/window/bp_car/pass/time/'.$rec['pkey'].'/' ;
     if ($rec['boss_type']==1 and $rec['boss_id']) { $rec['_boss_name']=$rec['_UK_boss_name'] ; $rec['_boss_reffer']=$rec['boss_id'].'.'._DOT(PERSONAL()->table_personal)->pkey ; }
     if ($rec['boss_type']==3 and $rec['boss_id']) { $rec['_boss_name']=$rec['_OT_boss_name'] ; $rec['_boss_reffer']=$rec['boss_id'].'.'._DOT(BP()->table_personal)->pkey ; }
     if ($rec['_car_owner_type']==1 and $rec['_car_UK_owner_name'])     { $rec['_car_owner_name']=$rec['_car_UK_owner_name'] ;     $rec['_car_owner_reffer']=$rec['_car_owner_id'].'.'._DOT(PERSONAL()->table_personal)->pkey ; }
     if ($rec['_car_owner_type']==2 and $rec['_car_IND_UK_owner_name']) { $rec['_car_owner_name']=$rec['_car_IND_UK_owner_name'] ; $rec['_car_owner_reffer']=$rec['_car_owner_id'].'.'._DOT(PERSONAL()->table_personal)->pkey ; }
     if ($rec['_car_owner_type']==2 and $rec['_car_IND_UK_owner_org'])  { $rec['_car_owner_org']=$rec['_car_IND_UK_owner_org'] ;    }
     if ($rec['_car_owner_type']==3 and $rec['_car_OT_owner_name'])     { $rec['_car_owner_name']=$rec['_car_OT_owner_name'] ;     $rec['_car_owner_reffer']=$rec['_car_owner_id'].'.'._DOT(BP()->table_personal)->pkey ; }
     if ($rec['_car_owner_type']==4 and $rec['_car_IND_OT_owner_name']) { $rec['_car_owner_name']=$rec['_car_IND_OT_owner_name'] ; $rec['_car_owner_reffer']=$rec['_car_owner_id'].'.'._DOT(BP()->table_personal)->pkey ; }
     $rec['_car_type_name']=$_SESSION['ARR_car_type'][$rec['_car_type']] ;
     $rec['_car_reffer']=$rec['personal_id'].'.'._DOT(BP()->table_cars)->pkey ;
     $rec['_car_owner_type_name']=$_SESSION['ARR_car_owner_type'][$rec['_car_owner_type']] ;
     $rec['_boss_type_name']=$_SESSION['ARR_car_boss_type'][$rec['boss_type']] ;
     $rec['_reffers']=array($rec['_reffer'],$rec['_car_reffer'],$rec['_car_owner_reffer'],$rec['_boss_reffer']) ;
     // используем унифицированные переменные для обработки данных пропуска в СКД
     $rec['_object_id']=$rec['_car_id'];
     $rec['_object_name']=$rec['_car_number'];
     $rec['_object_reffer']=$rec['_car_reffer'];
   }

   $rec['_status']=$_SESSION['ARR_pass_status'][$rec['status']] ;
   $rec['_srok']=date('d.m.Y',$rec['time_from']).' - '.date('d.m.Y',$rec['time_to']) ;
   $rec['_time_from']=date('d.m.Y H:i:s',$rec['time_from'])  ;
   $rec['_time_to']=date('d.m.Y H:i:s',$rec['time_to'])  ;
   //$rec['_allow_zones']=execSQL('select zone_id,time_from,time_to from '.BP()->table_pdp.' where person_id='.$rec['personal_id'].' and pass_id='.$rec['pkey']) ;
   $zones=execSQL('select zone_id,time_from,time_to from '.BP()->table_pdp.' where person_id='.$rec['personal_id'].' and pass_id='.$rec['pkey']) ;
   if (sizeof($zones)) foreach($zones as $z_rec)
    {  if (is_object(SKD()->tree[$z_rec['zone_id']])) $arr=SKD()->tree[$z_rec['zone_id']]->get_arr_child() ;
       if (sizeof($arr)) foreach($arr as $zone_id) $rec['_allow_zones'][]=$zone_id;//=array('zone_id'=>$zone_id,'time_from'=>$z_rec['time_from'],'time_to'=>$z_rec['time_to']) ;
        $rec['_main_zones'][$z_rec['zone_id']]=SKD()->get_full_zone_path($z_rec['zone_id']) ;

    }
    if (sizeof($rec['_main_zones'])) $rec['_to_zone_name']=implode('<br>',$rec['_main_zones']) ;
    //$rec['_all_zone']=$zones ;
   //$text=ob_get_clean() ;
   //LOGS()->reg_log('prepare_public_info_to_pass_time',$text) ;
 }

 function activate_pass_time($id)
 { update_rec_in_table($this->table_pass_time,
                       array('status'=>3,
                             'uid'=>md5(uniqid(rand(),true)),
                             'uid2'=>$this->get_UID_to_QR_to_pass_time()
                             ),
                       'pkey='.$id) ;
   $rec_pass=$this->get_pass_time_by_id($id) ;
   LOGS()->reg_log('Создание временного пропуска','',array('reffers'=>$rec_pass['_reffers'])) ;
   return($id) ;
 }

 function activate_car_pass_time($rec_pass)
 { update_rec_in_table($this->table_pass_time,
                       array('status'=>3,
                             'uid'=>md5(uniqid(rand(),true)),
                             'uid2'=>$this->get_UID_to_QR_to_pass_time()
                            ),
                       'pkey='.$rec_pass['pkey']) ;
   LOGS()->reg_log('Создание временного пропуска на автомобиль','',array('reffers'=>$rec_pass['_reffers'])) ;
 }


//==============================================================================================================================
//
//  ПОСТОЯННЫЕ ПРОПУСКА
//
//==============================================================================================================================

 function get_pass_static_by_id($id,$options=array())
 { $rec=array() ;
   $id=(int)$id ;
   if ($id>0)
   { $rec=execSQL_van('select * from '.PERSONAL()->table_name.' where PROPUSK_NUMBER="'.$id.'" and clss=211 and enabled=1',$options) ;
   if ($rec['pkey']) $this->prepare_public_info_to_pass_static($rec) ;
   }
   return($rec) ;
 }

 function prepare_public_info_to_pass_static(&$rec)
 { $aa=$rec ;
   $rec['_person']=$aa ; // защита от вложения массива в себя через указатель
   if ($rec['_person']['pkey'])
   {  $rec['_person_id']=$rec['_person']['pkey'] ;
      $rec['_person_fio']=$rec['_person']['obj_name'] ;
      $rec['_person_company']=$rec['_person']['company'] ;
      $rec['_object_id']=$rec['_person_id'];
      $rec['_object_name']=$rec['_person_fio'];
      $rec['_object_reffer']=$rec['_person_reffer'];
   }
   $rec['_status']=$_SESSION['ARR_pass_status'][$rec['status']] ;
   $rec['_srok']=date('d.m.Y',$rec['time_from']).' - '.date('d.m.Y',$rec['time_to']) ;
   $rec['_allow_zones']=execSQL('select zone_id,time_from,time_to from '.BP()->table_pdp.' where person_id='.$rec['pkey'].' and pass_id='.$rec['pkey']) ;
   $rec['time_from']=$rec['c_data'] ;
   $rec['time_to']=$rec['c_data']+100000000 ;
 }





//==============================================================================================================================
//
//  РАБОТА С ПРАВАМИ ДОСТУПА
//
//==============================================================================================================================

  // добавляем права доступы персоны
  function create_pdp_rec($person_id,$zone_id,$pass_id,$pass_type,$time_from,$time_to)
  {   $id=0 ;
      $zone_arr=(is_array($zone_id))? $zone_id:array($zone_id) ;
      if (sizeof($zone_arr)) foreach($zone_arr as $zone_id)
      $id=adding_rec_to_table(BP()->table_pdp,array('clss'=>505,
                                                 'person_id'=>$person_id,
                                                 'zone_id'=>$zone_id,
                                                 'pass_id'=>$pass_id,
                                                 'pass_type'=>$pass_type,
                                                 'time_from'=>$time_from,
                                                 'time_to'=>$time_to
                                              )
                        ) ;
      return($id);
  }

  // удаляем права доступы персоны или пропуска
  function delete_pdp_rec($params)
  {   $arr=array() ; $usl='' ;
      if (sizeof($params)) foreach($params as $key=>$value) $arr[]=$key.'="'.$value.'"' ;
      if (sizeof($arr)) $usl=implode(' and ',$arr) ;
      if (!$usl) return(false) ;

      ob_start() ;
      $sql='delete from '.BP()->table_pdp.' where clss=505 and '.$usl ;
      execSQL_update($sql,2) ;
      $text=ob_get_clean() ;
      //LOGS()->reg_log('Удаление прав доступа',$text) ;
      return(true) ;
  }

//==============================================================================================================================
//
//  РАБОТА С АВТО
//
//==============================================================================================================================

    function get_car_by_id($id,$options=array())
    {   list($pkey,$tkey)=explode('.',$id) ;
        $rec=array() ;
        if ($pkey)
        { $rec=execSQL_van('select * from '.$this->view_cars.' where pkey="'.addslashes($pkey).'"') ;
          if ($rec['pkey'] and !$options['no_public_info']) $this->prepare_public_info_to_car($rec,$options) ;
        }
        return($rec) ;
    }

    function get_car_id_by_number($number,$options=array())
    {   $id=execSQL_value('select pkey from '.$this->view_cars.' where obj_name like "'.addslashes($number).'"') ;
        return($id) ;
    }

    function get_car_by_number($number,$options=array())
    {   $rec=execSQL_van('select * from '.$this->view_cars.' where obj_name like "'.addslashes($number).'"') ;
        if ($rec['pkey'] and !$options['no_public_info']) $this->prepare_public_info_to_car($rec,$options) ;
        return($rec) ;
    }

    function get_cars_by_number($number,$options=array())
    {   $_usl=array() ;
        $_usl[]='clss=506' ;
        $_usl[]=ENGINE()->prepare_search_usl_mit_translit($number,'obj_name') ;
        $recs=execSQL('select * from '.$this->view_cars.' where '.implode(' and ',$_usl)) ;
        if (sizeof($recs) and !$options['no_public_info']) foreach($recs as $i=>$rec) $this->prepare_public_info_to_car($recs[$i],$options) ;
        return($recs) ;
    }

    function prepare_public_info_to_car(&$rec,$options=array())
    {   $rec['__href']='/cab/bp_car/car/'.$rec['pkey'].'/' ;
        $rec['__href_window']='/window/bp_car/car/'.$rec['pkey'].'/' ;
        switch($rec['owner_type'])
        { case 1: $rec['_owner_name']=$rec['_UK_owner_name'] ;  $rec['_owner_reffer']=$rec['owner_id'].'.'._DOT(PERSONAL()->table_personal)->pkey ;  break ;
          case 2: $rec['_owner_name']=$rec['_IND_UK_owner_name'] ; $rec['_owner_org']=$rec['_IND_UK_owner_org'] ;  $rec['_owner_reffer']=$rec['owner_id'].'.'._DOT(PERSONAL()->table_personal)->pkey ;  break ;
          case 3: $rec['_owner_name']=$rec['_OT_owner_name'] ; $rec['_owner_reffer']=$rec['owner_id'].'.'._DOT(BP()->table_personal)->pkey ;  break ;
          case 4: $rec['_owner_name']=$rec['_IND_OT_owner_name'] ; $rec['_owner_reffer']=$rec['owner_id'].'.'._DOT(BP()->table_personal)->pkey ;
                  break ;
        }
        $rec['_type_name']=$_SESSION['ARR_car_type'][$rec['type']] ;
        $rec['_owner_type_name']=$_SESSION['ARR_car_owner_type'][$rec['owner_type']] ;


    }

   function car_create($form_data,$options=array())
   { $data=array('parent'=>1,'clss'=>506) ; // damp_array($form_data,1,-1) ;
     if (isset($form_data['number']))          $data['obj_name']=$form_data['number'] ;
     if (isset($form_data['model']))           $data['model']=$form_data['model'] ;
     if (isset($form_data['type']))            $data['type']=$form_data['car_type'] ;
     if (isset($form_data['owner_type']))      $data['owner_type']=$form_data['owner_type'] ;

     switch($form_data['owner_type'])
       { case 1: $data['owner_id']=$form_data['owner_UK_id'] ;     break ;
         case 2: $data['owner_id']=$form_data['owner_IND_UK_id'] ;  break ;
         case 3: $data['owner_id']=$this->get_persons_org_id_by_name($form_data['owner_OT_name']) ;  break ;  // поиск по названиюю организации, если не найдена - будет добавлена
         case 4: $data['owner_id']=$this->get_person_id_by_name_and_dr($form_data['driver']) ;   break ; // поиск по ФИО + ДР, если не найдено - будет добавлено
       }

     $reffer=adding_rec_to_table($this->table_cars,$data,array('return_reffer'=>1,'debug'=>$options['debug'])) ;
     LOGS()->reg_log('Добавление автомобиля',$data['obj_name'],array('reffers'=>array($reffer),'data'=>$form_data)) ;
     list($id,$tkey)=explode(',',$reffer) ;
     return($id) ;
   }

   function car_save($reffer,$form_data)
    { $data=array() ; $arr_change=array() ;
      ob_start() ;
      $rec=$this->get_car_by_id($reffer) ; $res=0 ;
      if ($rec['pkey'])
      {
        if (isset($form_data['number']) and $form_data['number']!=$rec['obj_name'])       { $data['obj_name']=$form_data['number'] ; $arr_change['Гос.номер']=$rec['obj_name'].' => '.$form_data['number'] ; }
        if (isset($form_data['model'])  and $form_data['model']!=$rec['model'])           { $data['model']=$form_data['model'] ; $arr_change['Модель']=$rec['model'].' => '.$form_data['model'] ; }
        if (isset($form_data['car_type'])   and $form_data['car_type']!=$rec['type'])             { $data['type']=$form_data['car_type'] ; $arr_change['Тип авто']=$rec['type'].' => '.$form_data['car_type'] ; }
        if (isset($form_data['owner_type'])   and $form_data['owner_type']!=$rec['owner_type'])             { $data['owner_type']=$form_data['owner_type'] ; $arr_change['Тип владельца']=$rec['owner_type'].' => '.$form_data['owner_type'] ; }
        if (isset($form_data['owner_id'])   and $form_data['owner_id']!=$rec['owner_id'])             { $data['owner_id']=$form_data['owner_id'] ; $arr_change['ID владельца']=$rec['owner_id'].' => '.$form_data['owner_id'] ; }

        switch($form_data['owner_type'])
        { case 1: $data['owner_id']=$form_data['owner_UK_id'] ;     break ;
          case 2: $data['owner_id']=$form_data['owner_IND_UK_id'] ;  break ;
          case 3: $data['owner_id']=$this->get_persons_org_id_by_name($form_data['owner_OT_name']) ;  break ;  // поиск по названиюю организации, если не найдена - будет добавлена
          case 4: $data['owner_id']=$this->get_person_id_by_name_and_dr($form_data['driver']) ;   break ; // поиск по ФИО + ДР, если не найдено - будет добавлено
        }

        //damp_array($data,1,-1) ;
        //damp_array($arr_change,1,-1) ;
        if (sizeof($data)) $res=update_rec_in_table($this->table_cars,$data,'pkey='.$rec['pkey'],array('debug'=>0)) ;
      }
      $text=ob_get_clean() ;   echo $text ;
      if (sizeof($arr_change)) LOGS()->reg_log('Изменение авто',$text,array('data'=>$arr_change,'reffers'=>array($rec['_reffer']))) ;
      return($res) ;
    }


    function get_persons_org_id_by_name($name)
    { if (!$name) return(1) ;
      $id=execSQL_value('select pkey from '.$this->table_personal.' where clss=207 and obj_name="'.addslashes($name).'"') ;
      if (!$id) $id=adding_rec_to_table('obj_site_persons',array('parent'=>1,'clss'=>207,'obj_name'=>$name)) ;
      return($id) ;
    }

//==============================================================================================================================
//
//  РАБОТА С ПОСЕТИТЕЛЯМИ
//
//==============================================================================================================================

    function get_personal_by_id($id,$options=array())
    {   list($pkey,$tkey)=explode('.',$id) ;   //$options['debug']=2 ;
        $rec=array() ;
        if ($pkey)
        { $rec=execSQL_van('select * from '.$this->view_persons.' where pkey="'.$pkey.'"',$options['debug']) ;
          if ($rec['pkey'] and !$options['no_public_info']) $this->prepare_public_info_to_person($rec,$options) ;
        }
        return($rec) ;
    }


  // ищет сотрудника по заданному полю
  function get_personal($fields=array(),$options=array())
  {   $arr=array() ; $_usl=array() ;
      $_usl[]='clss=511' ;
      $_usl[]='_enabled=1' ;
      //if (sizeof($fields)) foreach($fields as $fname=>$fvalue) $arr[]=$fname.'="'.str_replace(array("'",'"','(',')','',"\n"),'',$fvalue).'"' ;
      if (sizeof($fields)) foreach($fields as $fname=>$fvalue) $arr[]=$fname.'="'.addslashes($fvalue).'"' ;
      if (!$options['search_mode']) $options['search_mode']='or' ;
      switch($options['search_mode'])
      { case 'or': $_usl[]='('.implode(' or ',$arr).')' ; break ;
        case 'and': $_usl[]='('.implode(' and ',$arr).')' ; break ;
      }

      if ($options['dop_usl']) $_usl[]=$options['dop_usl'] ;
      if ($options['only_enabled']) $_usl[]='enabled=1' ;
      $usl_select=implode(' and ',$_usl) ;
      $rec=select_db_recs($this->table_personal,$usl_select,array('no_arr'=>1,'debug'=>$options['debug'])) ;
      if ($rec['pkey']) $this->prepare_public_info_to_person($rec,$options) ;
      return($rec) ;
  }
    
 function get_persons_by_name_or_doc($data,$options=array())
  { $arr=array() ; $_usl=array() ; $recs=array() ;
    if (trim($data['name1'])) $arr[]=trim($data['name1']) ;
    if (trim($data['name2'])) $arr[]=trim($data['name2']) ;
    if (trim($data['name3'])) $arr[]=trim($data['name3']) ;
    if (sizeof($arr)) $_usl[]='obj_name like "'.trim(implode(' ',$arr).'%"') ;  ;
    if (trim($data['dr'])) $_usl[]='dr like "%'.trim($data['dr']).'"' ;
    if (trim($data['doc_seria'])) $_usl[]='doc_seria like "'.trim($data['doc_seria']).'"' ;
    if (trim($data['doc_number'])) $_usl[]='doc_number like "'.trim($data['doc_number']).'"' ;
    if (sizeof($_usl)) $recs=execSQL('select * from '.$this->table_personal.' where clss=511 and '.implode(' and ',$_usl),0,1) ;
    if (sizeof($recs)) foreach($recs as $i=>$rec) $this->prepare_public_info_to_person($recs[$i]) ;
    return($recs) ;
  }

  // ищет персону по имени, дню рождения, документу
  // если не найдена - добавляет новую запись
  // если найдена - будут обновлены вторичные поля персонала
  function get_person_id_by_name_and_dr($data,$options=array())
  { $arr=array() ; $_usl=array() ; $id=0 ;
    if (trim($data['name1'])) $arr[]=trim($data['name1']) ;
    if (trim($data['name2'])) $arr[]=trim($data['name2']) ;
    if (trim($data['name3'])) $arr[]=trim($data['name3']) ;
    if (sizeof($arr)) $_usl[]='obj_name like "'.trim(implode(' ',$arr).'%"') ;  ;
    if (trim($data['dr'])) $_usl[]='dr like "%'.trim($data['dr']).'"' ;
    if (sizeof($_usl)) $id=execSQL_value('select pkey from '.$this->table_personal.' where clss=511 and '.implode(' and ',$_usl),0,1) ;
    if ($id)  $this->personal_save($id,$data) ;
    else      $id=$this->personal_create($data,array('debug'=>0)) ;
    return($id) ;
  }


function prepare_public_info_to_person(&$rec,$options=array())
  { $rec['__href']='/cab/bp/person/'.$rec['pkey'].'/' ;
    $rec['__href_window']='/window/bp/person/'.$rec['pkey'].'/' ;
    // полный путь к фото
    $rec['_image_dir']=_DOT($this->tkey_personal_img)->dir_to_file.'source/'.$rec['_image_name'] ;
    if ($options['get_stats_cnt'])
    { $rec['_cnt_pass']=execSQL('select count(pkey) from '.$this->table_pass_van.' where personal_id='.$rec['pkey']) ;

    }
    // вписываем данные по компании и отделу

    // определяем группы, в которые входит сотрудник, по ID сотрудника
    //$arr=get_arr_links_recs($rec['_reffer']) ; //print_2x_arr($arr) ;
    //if (sizeof($arr)) foreach($arr as $rec_links) if ($rec_links['parent']==$_SESSION['ID_LIST_GROUP'])
        ////$rec['_groups'][]=array('id'=>$rec_links['id'],'obj_name'=>$rec_links['obj_name']) ; // список групп в виде двумерного массива
    //    $rec['_groups'][$rec_links['id']]=$rec_links['obj_name'] ;

    // определяем группы, в которые входит сотрудник, по ID должности
    //if ($rec['working'])
    //{ $working_reffer=$_SESSION['IL_works'][$rec['working']]['_reffer'] ;
    //  $arr=get_arr_links_recs($working_reffer) ;// print_2x_arr($arr) ;
    //  if (sizeof($arr)) foreach($arr as $rec_links) if ($rec_links['parent']==$_SESSION['ID_LIST_GROUP'])
     //     //$rec['_groups'][]=array('id'=>$rec_links['id'],'obj_name'=>$rec_links['obj_name']) ;  // список групп в виде одномерного массива
     //     $rec['_groups'][$rec_links['id']]=$rec_links['obj_name'] ;
    //}


    //if ($options['set_rules']) ESMO_RULES()->set_mo_rules_to_personal($rec,$options) ; // вписываем в запись критерии медосмотра, соотвествующие текущему сотруднику


    if ($options['set_QR_code'])
    {
        //$rec['_rec_car']=$rec_rec ;
        $value=(isset($rec[$options['set_QR_code']]))? $rec[$options['set_QR_code']]:$options['set_QR_code'] ;
        $rec['_qrcode_src']='http://'._MAIN_DOMAIN.'/qrcode.php?id='.urlencode($value).'&dest=terminal' ;
        $source_qrcode=imagecreatefrompng($rec['_qrcode_src']) ;
        //var_dump($source_qrcode) ;
        $im_convas  = imagecreatetruecolor(300,400);
        imagefill($im_convas,0,0,hexdec('FFFFFF'));
        //if (!imagecopymerge($im_convas,$source,2,2,0,0,$w1,$h1,100)) echo 'Не удалось наложить картинку<br>';
        //ImageDestroy($source);
        //$options['angle']=0 ;

          if (sizeof($source_qrcode))
           { $w_l=imagesx($source_qrcode)  ; $h_l=imagesy($source_qrcode) ;
             if (!imagecopymerge($im_convas,$source_qrcode,0,0, 0, 0,$w_l,$h_l,100))  echo 'Не удалось наложить лого<br>';
             ImageDestroy($source_qrcode);
           }

        //imagefttext($im_convas,12,0,30,320,hexdec('000000'),_DIR_TO_ROOT.'/images/PT-Sans/PTC55F-webfont.ttf','Номер авто: '.$car_number) ;
        if ($options['set_QR_code_text'])
        {
            imagefttext($im_convas,12,0,30,330,hexdec('000000'),_DIR_TO_ROOT.'/images/PT-Sans/PTC55F-webfont.ttf','Номер ВУ: '.$rec['driving']) ;
            imagefttext($im_convas,12,0,30,360,hexdec('000000'),_DIR_TO_ROOT.'/images/PT-Sans/PTC55F-webfont.ttf',$options['set_QR_code_text']) ;

        }
        else
        {
            imagefttext($im_convas,12,0,30,330,hexdec('000000'),_DIR_TO_ROOT.'/images/PT-Sans/PTC55F-webfont.ttf',$rec['_org_name']) ;
            imagefttext($im_convas,12,0,30,360,hexdec('000000'),_DIR_TO_ROOT.'/images/PT-Sans/PTC55F-webfont.ttf','Номер ВУ: '.$rec['driving']) ;

        }
        ob_start() ;
        imagepng($im_convas);
        //imagejpeg($source_qrcode);
        $res=ob_get_clean()  ;
        //$rec['_qrcode_source']=base64_encode(file_get_contents($rec['_qrcode_src'])) ;
        $rec['_qrcode_source']=base64_encode($res) ;
    }

    //damp_array($rec,1,-1) ;
  }

    function personal_create($form_data,$options=array())
    { $data=array('parent'=>1,'clss'=>511) ; // damp_array($form_data,1,-1) ;
      if (isset($form_data['name1']))           $data['obj_name']=$form_data['name1'].' '.$form_data['name2'].' '.$form_data['name3'] ;
      if (isset($form_data['name1']))           $data['name1']=$form_data['name1'] ;
      if (isset($form_data['name2']))           $data['name2']=$form_data['name2'] ;
      if (isset($form_data['name3']))           $data['name3']=$form_data['name3'] ;
      if (isset($form_data['doc_type']))        $data['doc_type']=$form_data['doc_type'] ;
      if (isset($form_data['doc_seria']))       $data['doc_seria']=$form_data['doc_seria'] ;
      if (isset($form_data['doc_number']))      $data['doc_number']=$form_data['doc_number'] ;
      if (isset($form_data['dr']))              $data['dr']=$form_data['dr'] ;
      if (isset($form_data['pol']))             $data['pol']=$form_data['pol'] ;
      if (isset($form_data['company']))         $data['company']=$form_data['company'] ;
      if (isset($form_data['company']))         $data['parent']=$this->get_persons_org_id_by_name($form_data['company']) ;
      if (isset($form_data['city']))            $data['city']=$form_data['city'] ;
      if (isset($form_data['working']))         $data['working']=$form_data['working'] ;
      if (isset($form_data['personal_id']))     $data['personal_id']=$form_data['personal_id'] ;
      if (isset($form_data['black_list']))   $data['black_list']=$form_data['black_list'] ;
      //if (isset($form_data['to_org_id']))       $data['to_org_id']=$form_data['to_org_id'] ;
      //if (isset($form_data['to_otdel_id']))     $data['to_otdel_id']=$form_data['to_otdel_id'] ;
      //if (isset($form_data['to_personal_id']))  $data['to_personal_id']=$form_data['to_personal_id'] ;
      //LOGS()->get_log_events()

      $member_reffer=adding_rec_to_table($this->table_personal,$data,array('return_reffer'=>1,'debug'=>$options['debug'])) ;
      LOGS()->reg_log('Создание новой персоны',$data['obj_name'],array('reffers'=>array($member_reffer),'_data'=>$form_data)) ;
      list($id,$tkey)=explode('.',$member_reffer) ;
      return($id) ;
    }

    function personal_save($reffer,$form_data)
     { $data=array() ; $arr_change=array() ;  $fio='' ;
       //ob_start() ;
       $rec=$this->get_personal_by_id($reffer) ; $res=0 ; // damp_array($rec,1,-1) ;
       if ($rec['pkey'])
       {
         if (isset($form_data['name1']) and $form_data['name1']!=$rec['name1'])                          { $data['name1']=$form_data['name1'] ; $arr_change['Фамилия']=$rec['name1'].' => '.$form_data['name1'] ; }
         if (isset($form_data['name2']) and $form_data['name2']!=$rec['name2'])                          { $data['name2']=$form_data['name2'] ; $arr_change['Имя']=$rec['name2'].' => '.$form_data['name2'] ; }
         if (isset($form_data['name3']) and $form_data['name3']!=$rec['name3'])                          { $data['name3']=$form_data['name3'] ; $arr_change['Отчество']=$rec['name3'].' => '.$form_data['name3'] ; }

         $arr=array() ;
         if (isset($form_data['name1'])) $arr[]=$form_data['name1'] ;
         if (isset($form_data['name2'])) $arr[]=$form_data['name2'] ;
         if (isset($form_data['name3'])) $arr[]=$form_data['name3'] ;
         if (sizeof($arr))
         { $fio=implode(' ',$arr) ;
           if ($fio!=$rec['obj_name'])  $data['obj_name']=$fio ;
         }

         if (isset($form_data['dr']) and $form_data['dr']!=$rec['dr'])                          { $data['dr']=$form_data['dr'] ; $arr_change['Дата рождения']=$rec['dr'].' => '.$form_data['dr'] ; }
         if (isset($form_data['pol']) and $form_data['pol']!=$rec['pol'])                       { $data['pol']=$form_data['pol'] ; $arr_change['Пол']=$rec['pol'].' => '.$form_data['pol'] ; }
         if (isset($form_data['doc_type']) and $form_data['doc_type']!=$rec['doc_type'])        { $data['doc_type']=$form_data['doc_type'] ; $arr_change['Тип документа']=$rec['doc_type'].' => '.$form_data['doc_type'] ; }
         if (isset($form_data['doc_seria']) and $form_data['doc_seria']!=$rec['doc_seria'])     { $data['doc_seria']=$form_data['doc_seria'] ; $arr_change['Серия документа']=$rec['doc_seria'].' => '.$form_data['doc_seria'] ; }
         if (isset($form_data['doc_number']) and $form_data['doc_number']!=$rec['doc_number'])  { $data['doc_number']=$form_data['doc_number'] ; $arr_change['Номер документа']=$rec['doc_number'].' => '.$form_data['doc_number'] ; }

         if (isset($form_data['company']) and $form_data['company']!=$rec['company'])   { $data['company']=$form_data['company'] ; $arr_change['Компания']=$rec['company'].' => '.$form_data['company'] ; }
         if (isset($form_data['city']) and $form_data['city']!=$rec['city'])            { $data['city']=$form_data['city'] ; $arr_change['Город']=$rec['city'].' => '.$form_data['city'] ; }
         if (isset($form_data['working']) and $form_data['working']!=$rec['working'])   { $data['working']=$form_data['working'] ; $arr_change['Должность']=$rec['working'].' => '.$form_data['working'] ; }
         if (isset($form_data['black_list']) and $form_data['black_list']!=$rec['black_list'])   { $data['black_list']=$form_data['black_list'] ; $arr_change['Черный список']=$rec['black_list'].' => '.$form_data['black_list'] ; }

         $data['parent']=$this->get_persons_org_id_by_name($form_data['company']) ;

         if (sizeof($data)) $res=update_rec_in_table($this->table_personal,$data,'pkey='.$rec['pkey'],array('debug'=>0)) ;
         //unset($res['sql']) ;
         //damp_array($res,1,-1)  ;
         //trace() ;
       }
       //$text=ob_get_clean() ;
       if (sizeof($data)) LOGS()->reg_log('Изменение посетителя','',array('data'=>$arr_change,'reffers'=>array($rec['_reffer']))) ;
       return($res) ;
     }


    function check_person_to_black_list($name1,$name2='',$name3='',$dr='')
    {  $_usl=array() ;
       $_usl[]='clss=511' ;
       if ($name1) $_usl[]='name1 like "'.$name1.'"' ;
       if ($name2) $_usl[]='name2 like "'.$name2.'"' ;
       if ($name3) $_usl[]='name3 like "'.$name3.'"' ;
       if ($dr) { $add_dr=explode('.',$dr) ; $_usl[]='dr like "%'.$add_dr[2].'"' ; }
       $usl=implode(' and ',$_usl) ;
       $recs=execSQL_line('select pkey from '.BP()->table_personal.' where '.$usl.' and black_list=1') ;
       if (sizeof($recs)) return(implode(',',$recs)) ;
       else return('') ;

    }

    function send_notife_email_by_person_in_blacklist($person_data,$ids)
       {   ob_start()
           ?><h2>Попытка выдачи пропуска человеку из черного списка</h2>
             <strong>Оператор:</strong><br>
             <?echo MEMBER()->name.' ('.MEMBER()->working.')<br><br>'?>

             <strong>Данные посетителя:</strong>
             <table class="basic">
                 <tr><td>Номер заявки</td><td><?echo $person_data['req_id']?></td></tr>
                 <tr><td>Фамилия</td><td><?echo $person_data['name1']?></td></tr>
                 <tr><td>Имя</td><td><?echo $person_data['name2']?></td></tr>
                 <tr><td>Отчество</td><td><?echo $person_data['name3']?></td></tr>
                 <tr><td>Дата рождения</td><td><?echo $person_data['dr']?></td></tr>
                 <tr><td>Пол</td><td><?echo ($person_data['pol'])? 'Ж':'М'?></td></tr>
                 <tr><td>Документ</td><td><?echo $_SESSION['ARR_doc_type'][$person_data['doc_type']]?></td></tr>
                 <tr><td>Серия</td><td><?echo $person_data['doc_seria']?></td></tr>
                 <tr><td>Номер</td><td><?echo $person_data['doc_number']?></td></tr>
                 <tr><td>Компания</td><td><?echo $person_data['company']?></td></tr>
                 <tr><td>Город</td><td><?echo $person_data['city']?></td></tr>
                 <tr><td>Должность</td><td><?echo $person_data['working']?></td></tr>
             </table>
             <strong>Совпадения с черным списком</strong><?

           include_once(_DIR_EXT.'/aspmo_angelo/report_persons_blacklist.php') ;
           $report_obj=new report_persons_blacklist() ;
           $report_obj->no_view_title=1;
           $report_obj->view_panel_info=0;
           $report_obj->filter['person_id']=$ids;
           $report_obj->view_as_HTML(array('no_a')) ;

           $text=ob_get_clean() ;
           MAILS()->send_mail($_SESSION['LS_email_event_blacklist'],'Черный список',$text) ;
       }


    function send_email_notyfe_by_person_from_blacklist($name1,$name2='',$name3='',$dr='',$recs)
    {

    }

    function get_name_by_id($id,$options=array())
    {   $name=execSQL_value('select obj_name from '.$this->table_personal.' where pkey='.$id) ;
        return($name) ;
    }





}



?>