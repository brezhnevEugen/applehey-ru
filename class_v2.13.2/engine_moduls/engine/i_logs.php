<?

function LOGS(){ if (!is_object($GLOBALS['logs'])) $GLOBALS['logs']=new c_logs();return($GLOBALS['logs']);}

class c_logs
{

    //==========================================================================================================================================================================
    // ЖУРНАЛ СОБЫТИЙ
    //==========================================================================================================================================================================

    // добавляем запись по МО в таблицу, возвращает id записи
    function reg_log($name,$comment,$options=array())
      { global $member ;
        $rec['c_data']=time() ;
        $rec['clss']=251 ;
        $rec['obj_name']=$name ;
        $rec['IP']=IP ;
        if ($comment)                       $rec['comment']=$comment ;
        if (isset($options['terminal_id'])) $rec['terminal_id']=$options['terminal_id'] ;
        if (isset($options['terminal_ip'])) $rec['terminal_ip']=$options['terminal_ip'] ;
        if (isset($options['terminal'])) {  $rec['terminal_id']=$options['terminal']['id'] ; $rec['terminal_ip']=$options['terminal']['ip'] ; }
        if (isset($options['time_exec']))   $rec['time_exec']=$options['time_exec'];
        if (isset($options['mo']))          $rec['mo_id']=$options['mo']['pkey'] ;
        if (isset($options['mo_id']))       $rec['mo_id']=$options['mo_id'] ;
        if (isset($options['personal_id'])) $rec['member_id']=$options['personal_id'] ;
        if (isset($options['member_id']))   $rec['member_id']=$options['member_id'] ;
        if (isset($options['member']))      $rec['member_id']=$options['member']['pkey'] ;
        if (isset($options['data']))        $rec['data']=$options['data'] ;
        if ($member->id)                    $rec['user_id']=$member->id ;

        if (sizeof($options['reffers'])) foreach($options['reffers'] as $id=>$reffer) $rec['reffer'.($id+1)]=$reffer ;

        $id=adding_rec_to_table(ENGINE()->table_mo_log,$rec,array('no_return_id'=>1)) ;
        return($id) ;
      }


    // добавляем запись по МО в таблицу, возвращает id записи
    function reg_file_log($name,$cont)
      {  $name.='_'.date('Y_m_d_H_i_s',time()).'.html' ;
         //$name.='.html' ;
         file_put_contents(_DIR_TO_ROOT.'/logs/'.$name,$cont) ;
         return('http://'._MAIN_DOMAIN.'/logs/'.$name) ;
      }

     // возвражает журнал событий для проекта/задания
         function get_log_events($options=array())
         { $_usl=array('clss=251') ;
           if ($options['mo_id']) $_usl[]='mo_id='.$options['mo_id'] ;
           if ($options['personal_id']) $_usl[]='(user_id='.$options['personal_id'].' or member_id='.$options['personal_id'].')' ;
           if ($options['terminal_id']) $_usl[]='terminal_id='.$options['terminal_id'] ;
           if ($options['reffer']) $_usl[]='(reffer1='.$options['reffer'].' or reffer2='.$options['reffer'].' or reffer3='.$options['reffer'].' or reffer4='.$options['reffer'].' or reffer5='.$options['reffer'].')';
           if ($options['cmd']) $_usl[]='obj_name="'.$options['cmd'].'"' ;
           if ($options['text']) $_usl[]='(comment like "%'.$options['text'].'%" or data like "%'.$options['text'].'%")' ;

           $usl=implode(' and ',$_usl) ;
           $order=($options['order'])? $options['order']:' pkey desc' ;
           $recs=execSQL('select * from '.ENGINE()->table_mo_log.' where '.$usl.' order by '.$order.' limit 500') ;
           return($recs) ;
         }

    //==========================================================================================================================================================================
    // МОНИТОР
    //==========================================================================================================================================================================

     function get_last_monitor_mess_id($group=0)
     {   $usl_group=($group)? ' and grp in ('.$group.')':'' ;
         $last_id=execSQL_value("SELECT max(pkey) as pkey from obj_site_mess where clss=260 ".$usl_group) ;
         return($last_id) ;
     }

     function reg_monitor_mess($group,$arr_mess)
     {   //$arr_mess=str_replace('15.25.35.1','dev.kvzrm.ru',$arr_mess) ;
         adding_rec_to_table('obj_site_mess',array('clss'=>260,'parent'=>$group,'grp'=>$group,'obj_name'=>$arr_mess),array('no_return_id'=>1,'no_auto_indx'=>1)) ;
     }

     function get_monitor_mess($group,$from_id)
     { $recs=array() ;
       if ($group)
       { $usl_group=' and grp in ('.$group.')' ;
         $recs=execSQL('select pkey,obj_name from obj_site_mess where clss=260 and pkey>'.$from_id.' '.$usl_group) ;
       if (sizeof($recs)) foreach($recs as $i=>$rec) $recs[$i]['_data']=unserialize($rec['obj_name']) ;
       }
       return($recs) ;

     }

    //==========================================================================================================================================================================
     // РАБОТА С ЖУРНАЛОМ ОБМЕНА
     //==========================================================================================================================================================================

     // добавляем запись по звпросу в таблицу, возвращает id записи
     function log_request($IP1,$IP2,$cmd,$mess1,$mess2,$options=array())
       { $rec['c_data']=$options['time_rec'] ;
         $rec['r_data']=time() ;
         $rec['clss']=253 ;
         $rec['cmd']=$cmd ;
         $rec['IP1']=$IP1 ;
         $rec['mess1']=$mess1 ;
         $rec['IP2']=$IP2 ;
         $rec['mess2']=$mess2 ;

         if ($options['terminal_id']) $rec['terminal_id']=$options['terminal_id'] ;
         if ($options['personal_id']) $rec['personal_id']=$options['personal_id'] ;
         if ($options['size1'])       $rec['size1']=$options['size1'] ;
         if ($options['size2'])       $rec['size2']=$options['size2'] ;
         if ($options['mo_id']) $rec['mo_id']=$options['mo_id'] ;
         if ($options['event_id'])    $rec['evt_id']=$options['event_id'] ;
         if ($options['debug_text']) $rec['debug_text']=$options['debug_text'] ;

         if (sizeof($options['reffers'])) foreach($options['reffers'] as $id=>$reffer) $rec['reffer'.($id+1)]=$reffer ;

         //execSQL_update('delete from '.$this->table_mo_req.' where clss=253)
         $id=adding_rec_to_table_fast(ENGINE()->table_mo_req,$rec) ;
         return($id) ;
       }

       // добавляем запись по звпросу в файл, возвращает id записи
      function logf_request($fname,$IP1,$IP2,$uri,$cmd,$mess1,$mess2)
       { ob_start() ;
           echo "URI:".$uri."\n"  ;
           echo "IP1:".$IP1."\n"  ;
           echo "IP2:".$IP2."\n"  ;
           echo "cmd:".$cmd."\n"  ;
           echo "------------------------------------------------------------------------------------------\n" ;
           print_r($mess1) ;
           echo "------------------------------------------------------------------------------------------\n" ;
           print_r($mess2) ;
         $text=ob_get_clean() ;
         $flog_name='/logs/log_'.$fname.'.txt' ;
         file_put_contents(_DIR_TO_ROOT.$flog_name,$text) ;


         //$id=adding_rec_to_table(ENGINE()->table_mo_req,$rec) ;
         return($flog_name) ;
       }

      // возвражает журнал событий для проекта/задания
      function get_log_request($options=array())
      { $limit=($options['count'])? $options['count']:100 ;
        //damp_array($options) ;
        $_usl[]='clss=253' ;
        if ($options['personal_id']) $_usl[]='personal_id='.$options['personal_id'] ;
        if ($options['personal_reffer']) $_usl[]='(reffer1="'.$options['personal_reffer'].'" or '.
                                                 ' reffer2="'.$options['personal_reffer'].'" or '.
                                                 ' reffer3="'.$options['personal_reffer'].'" or '.
                                                 ' reffer4="'.$options['personal_reffer'].'" or '.
                                                 ' reffer5="'.$options['personal_reffer'].'")' ;
        if ($options['mo_id']) $_usl[]='mo_id='.$options['mo_id'] ;
        if ($options['terminal_id']) $_usl[]='terminal_id='.$options['terminal_id'] ;
        if ($options['cmd']) $_usl[]='cmd="'.$options['cmd'].'"' ;
        if ($options['text']) $_usl[]='(mess1 like "%'.$options['text'].'%" or mess2 like "%'.$options['text'].'%")' ;
        $usl=implode(' and ',$_usl) ;
        //if ($options['mo'])
        $recs=execSQL('select * from '.ENGINE()->table_mo_req.' where '.$usl.' order by pkey desc limit '.$limit) ;
        return($recs) ;
      }


}

?>