<?php
include_once('i_logs.php') ;
include_once('i_lists.php') ;
include_once('i_io.php') ;

$__functions['init'][]='_engine_site_vars' ;
$__functions['boot_site'][]='_engine_site_boot' ;

function ENGINE() {return($_SESSION['engine_system']);$_SESSION['engine_system']=new c_engine_system();return($_SESSION['engine_system']);} ;


function _engine_site_vars()
{   // где-то надо это поместить, раз этот модуль все равно всегда должен быть первым
    $_SESSION['ARR_roll']=array() ;
    $_SESSION['ARR_roll'][1]=array('obj_name'=>'Администратор',          '_index_page'=>'/cab/') ;
    //$_SESSION['ARR_roll'][2]=array('obj_name'=>'Обычный персонал',        '_index_page'=>'/cab/') ;
    //$_SESSION['ARR_roll'][3]=array('obj_name'=>'Расширенный персонал',        '_index_page'=>'/cab/') ;
    //$_SESSION['ARR_roll'][4]=array('obj_name'=>'Контролер',        '_index_page'=>'/cab/') ;
    //$_SESSION['ARR_roll'][2]=array('obj_name'=>'Фельдшер',               'index_page'=>'/cab/rmv/',            'use_auch_as_smena'=>1, 'smena_monopole_mode'=>0, 'report_template'=>'report_mo_smena') ;
    // => ESMO:          $_SESSION['ARR_roll'][2]=array('obj_name'=>'Фельдшер',               'index_page'=>'/cab/rmv/',            'use_auch_as_smena'=>1, 'smena_monopole_mode'=>0, 'report_template'=>'report_mo_smena') ;
    // => ESMO:          $_SESSION['ARR_roll'][3]=array('obj_name'=>'Фельдшер') ;
    //                   $_SESSION['ARR_roll'][4]=array('obj_name'=>'Табельщик') ;
    // => TO:            $_SESSION['ARR_roll'][5]=array('obj_name'=>'Техник',                 'index_page'=>'/cab/rmt/',            'use_auch_as_smena'=>1, 'smena_monopole_mode'=>0, 'report_template'=>'report_to_smena') ;
    // => CLIENT:        $_SESSION['ARR_roll'][6]=array('obj_name'=>'Клиент',                 'index_page'=>'/cab/client/') ;
    // => CLIENT:        $_SESSION['ARR_roll'][7]=array('obj_name'=>'Менеджер по клиентам',   'index_page'=>'/cab/manager/') ;
    // => MINE:          $_SESSION['ARR_roll'][8]=array('obj_name'=>'Ламповщик',              'index_page'=>'/cab/rml/') ;
    // => ESMO_BILLING:  $_SESSION['ARR_roll'][9]=array('obj_name'=>'Франчайзер',   'index_page'=>'/cab/bill/') ;
    // => SKD:           $_SESSION['ARR_roll'][10]=array('obj_name'=>'Начальник СБ',   'index_page'=>'/cab/manager/') ;
    // => SKD:           $_SESSION['ARR_roll'][11]=array('obj_name'=>'Сотрудник СБ',   'index_page'=>'/cab/manager/') ;
    // => SKD:           $_SESSION['ARR_roll'][12]=array('obj_name'=>'Сотрудник БП',   'index_page'=>'/cab/manager/') ;

    $_SESSION['ARR_monitor']=array(   0=>'SSE',
                                      1=>'AJAX'
                                       ) ;

    $_SESSION['init_options']['engine']['debug']=0 ;


    $_SESSION['init_options']['default']['pages']['options']=array('pages_num_format'=>"%'1d",'page_max_number_count'=>20,'page_number_rasdel'=>'&nbsp;&nbsp;|&nbsp;&nbsp;','page_image_prev'=>'/'._CLASS_.'/images/arr_left_dark.gif','page_image_next'=>'/'._CLASS_.'/images/arr_right_dark.gif') ;
    $_SESSION['init_options']['default']['pages']['options']['page_mode']=0 ;
    $_SESSION['init_options']['default']['pages']['size']=array('100'=>'100 позиций на странице') ;

    //$_SESSION['ARR_chet_operation_types']=array(1=>'Поступление',2=>'Списание') ;
    //$_SESSION['ARR_chet_types']=array(0=>'Расходный',2=>'Приходный') ;

     ////
}

function _engine_site_boot($options)
{
  create_system_modul_obj('engine',$options) ;

}


class c_engine_system
{
    public $table_bill ;
    public $engine_list ;
    public $list_active_ext=array() ;

    function c_engine_system($create_options=array())
    {   global $site_TM_kode ;
        $this->table_mo_log ='obj_'.$site_TM_kode.'_mo_log' ;
        $this->table_mo_req ='obj_'.$site_TM_kode.'_req_log' ;

        $this->reg_ext('setting','Настройки') ;
        //$this->reg_ext('debug','Отладка') ;
        //$this->reg_ext('docs','Сохраненные документы') ;

    }

    function declare_menu_items_4($page)
    {
        $page->menu['/cab/setting/']=array('title'=>'Настройки',
                                           'class'=>'fa fa-sliders',
                                           'items'=>array('/cab/setting/accounts/'=>array('title'=>'Персонал',
                                                                                        'class'=>'fa fa-users',
                                                                                        'items'=>array('/cab/setting/accounts/'=>'Список персонала',
                                                                                                       '/cab/setting/account/new/'=>array('title'=>'Добавить персонал','class'=>'fa fa-user-plus')
                                                                                                       )
                                                                                       ),

                                                         '/cab/setting/right/'=>'Права доступа',
                                                         '/cab/setting/update/'=>'Обновление'
                                                        )
                                          ) ;

    }

    function declare_menu_items_5($page)
    {
        /*
        $page->menu['/cab/debug/']=array('title'=>'Отладка',
                                          'class'=>'fa fa-cogs',
                                          'items'=>array('/cab/debug/'=>'Журнал обмена',
                                                          '/cab/debug/log_mo/'=>array('title'=>'Лог событий','class'=>'fa fa-users'),
                                                          '/cab/debug/log_XML/'=>array('title'=>'Лог шлюза','class'=>'fa fa-users'),
                                                          '/cab/debug/work/'=>array('title'=>'Лог работы','class'=>'fa fa-users'),
                                                          '/cab/debug/HR/'=>array('title'=>'HR','class'=>'fa fa-users',
                                                                  'items'=>array('/cab/debug/HR/'=>'HR - шлюз',
                                                                                 '/cab/debug/HR/import/'=>'HR - импорт'
                                                                                 )
                                                                  ),
                                                          '/cab/debug/db/'=>array('title'=>'Тест запросов к БД','class'=>'fa fa-users'),
                                                          '/cab/debug/server/'=>'Загрузка сервера',
                                                          '/cab/debug/info/'=>'Информация'
                                                         )
                                           ) ;
        //$this->menu['/cab/terminal/']='Терминалы' ;
         */
    }

    function reg_ext($ext_name,$ext_title)
    {
      $this->list_active_ext[$ext_name]=$ext_title ;
    }




    function get_age_by_dr($dr)
    { list($d,$m,$y)=explode('.',$dr)  ;
      if($m > date('m') || $m == date('m') && $d > date('d')) $age=date('Y') - $y - 1;
      else $age=date('Y') - $y;
      if ($age<0) $age=0 ;
      return($age) ;
    }

    // дата идет в формате 01/01/10, переводим в формат обычной даты
    function get_dr_age($str_data)
    {  $str_data=str_replace(array('/','-'),'.',$str_data) ;
       $arr=explode('.',$str_data) ;
       $dr='' ; $age=0 ;
       if (sizeof($arr)==3)
        { $now_year=(int)date('y') ;
          $_y=(int)$arr[2] ;
          if (strlen($arr[2])==4)  $yaer=$_y ;
          else if ($now_year>$_y)  $yaer=(int)'20'.$arr[2] ;
          else                     $yaer=(int)'19'.$arr[2] ;
          $month=(int)$arr[1] ;
          $day=(int)$arr[0] ;
          $dr=sprintf("%02d.%02d.%04d", $day, $month, $yaer);
          $tt = mktime(0,0,0,$month,$day,date('Y'));   // дата день рождения в этом году
          $now_year=(int)date('Y') ;
          $age=$now_year-$yaer ;
          if (time()<$tt) $age=$age-1 ;
        }
       return(array($dr,$age)) ;
    }


    function get_rand_string($count)
    {  $arr=array() ;
     for($i=0;$i<$count;$i++)  $arr[]=rand(1,9) ;
     return(implode('',$arr)) ;
    }

    //  преобразовываем русские вуквы в англиские аналоги
    function translit($text)
    { if(preg_match("/[А-я]/", $text))
      { $trans = array("а" => "a", "в" => "b", "е" => "e","к" => "k","м" => "m","н" => "h","о" => "o","р" => "p","с" => "c","т" => "t","х" => "x","у" => "y",
                       "А" => "A","В" => "B","Е" => "E","К" => "K","М" => "M","Н" => "H","О" => "O","Р" => "P","С" => "C","Т" => "T","Х" => "X","У" => "Y");
        $res=strtr($text, $trans);
      }
      else $res=$text;
      return($res) ;
    }

    //  преобразовываем англиский вуквы в русские аналоги
    function translit2($text)
    { if(preg_match("/[A-z]/", $text))
      { $trans = array("а" => "a", "в" => "b", "е" => "e","к" => "k","м" => "m","н" => "h","о" => "o","р" => "p","с" => "c","т" => "t","х" => "x","у" => "y",
                       "А" => "A","В" => "B","Е" => "E","К" => "K","М" => "M","Н" => "H","О" => "O","Р" => "P","С" => "C","Т" => "T","Х" => "X","У" => "Y");
        $trans=array_flip($trans) ;
        return strtr($text, $trans);
      }
      else return $text;
    }

    function convert_full_name_to_FIO($name)
    {  $arr=explode(' ',$name) ;
       $res=$arr[0].' '.mb_substr($arr[1],0,1).'.'.mb_substr($arr[2],0,1).'.' ;
       return($res) ;
    }

    function send_cmd_reboot_client()
    {  //$xmpp_message['ajax_cmd']='aspmo/reboot' ;
       //$xmpp_message['member_id']=$member->id ;  // кто отправил команду, чтобы потом не перегружать себя
       //$text_json=json_encode($xmpp_message,JSON_UNESCAPED_UNICODE) ;
       // отправляем данные всем сотрудникам АСПМО
       //$recs_member=$this->get_list_members() ;
       //if (sizeof($recs_member)) foreach($recs_member as $member_info) if ($member_info['XMPP_login']) $this->send_XMPP_message($member_info['XMPP_login'],'aspmo_server','12345',$text_json);
       // вписываем в записи аккаунтов флаг требования провести перезагруку
       if (MEMBER()->cur_group_id) { execSQL_update('update '.ACCOUNTS()->table_account.' set reboot=1 where parent='.MEMBER()->cur_group_id) ; return(1) ;}
       else  return(0)  ;
    }

    function device_send_async_cmd($terminal_id,$cmd,$options=array())
      { $__times['start']=get_formatted_microtime() ;
        $sleep=($options['pause'])? $options['pause']:0 ;
        $params=array() ;
        $params['cmd']=$cmd ;
        $params['sleep']=$sleep ;
        $params['terminal_id']=$terminal_id ;
        if (is_array($options['params'])) $params=array_merge($params,$options['params']) ;
        $comm='http://'._MAIN_DOMAIN.'/terminal.php?'.http_build_query($params) ;
        exec('bash -c "/usr/bin/wget  \"'.$comm.'\" > /dev/null 2>&1 &"',$output); // тоже работает но не async
        $__times['end']=get_formatted_microtime() ;
        $time_int=$__times['end']-$__times['start'] ;
        $text=$cmd ;
        if ($sleep) $text.=' (пауза '.$sleep.' сек)' ;
        $text.='<br>'.$comm ;
        LOGS()->reg_log('Асинхронная команда  устройству',$text,array('terminal_id'=>$terminal_id,'time_exec'=>$time_int)) ;
      }


    function terminal_send_async_cmd($terminal_id,$cmd,$options=array())
    { $__times['start']=get_formatted_microtime() ;
      $sleep=($options['pause'])? $options['pause']:0 ;
      $params=array() ;
      $params['cmd']=$cmd ;
      $params['sleep']=$sleep ;
      $params['terminal_id']=$terminal_id ;
      if (is_array($options['params'])) $params=array_merge($params,$options['params']) ;
      $comm='http://'._MAIN_DOMAIN.'/terminal.php?'.http_build_query($params) ;
      exec('bash -c "/usr/bin/wget  \"'.$comm.'\" > /dev/null 2>&1 &"',$output); // тоже работает но не async
      $__times['end']=get_formatted_microtime() ;
      $time_int=$__times['end']-$__times['start'] ;
      $text=$cmd ;
      if ($sleep) $text.=' (пауза '.$sleep.' сек)' ;
      $text.='<br>'.$comm ;
      LOGS()->reg_log('Асинхронная команда терминалу',$text,array('terminal_id'=>$terminal_id,'time_exec'=>$time_int)) ;
    }

    // запрос к терминалу на перезагрузку
    function terminal_send_data($terminal_rec,$cmd,$var,$debug=0)
    {  $port=($terminal_rec['port'])? $terminal_rec['port']:$_SESSION['LS_setting_term_port'] ;
       $put_server=(!$terminal_rec['virt'])?  $terminal_rec['ip'].':'.$port:_MAIN_DOMAIN ;
       $put_req=(!$terminal_rec['virt'])?  '/':'/equipment_api' ;
       $put_params=(!$terminal_rec['virt'])?  'operation='.$cmd:'operation='.$cmd ;

       if ($debug)
       { ?><h2>Отправлено на терминал: <?echo $cmd?></h2><?
          echo 'id: '.$terminal_rec['id'].'<br><br>' ;
          echo 'ip: '.$put_server.'<br>' ;
          echo 'req: '.$put_req.'<br>' ;
          echo 'params: '.$put_params.'<br><br>' ;
          damp_array($var,1,-1) ;
       }
       $text_json=json_encode($var) ;

       $response_json=IO()->doPut('http://',$put_server,$put_req,$put_params,$text_json) ;

       $res=json_decode($response_json,true);
       if ($debug)
       { ?><h2>Получено с терминала</h2><?
         if (is_array($res)) damp_array($res,1,-1) ; else echo $response_json  ;
       }
       return($res) ;
    }





}

function send_cmd_reboot_client() { ENGINE()->send_cmd_reboot_client() ; ; }



?>