<?
function IO(){ if (!is_object($GLOBALS['IO'])) $GLOBALS['IO']=new c_io();return($GLOBALS['IO']);}

class c_io
{
    // ОТПРАВЛЯЕМ ЗАПРОС на адрес IP
    //function doPut($IP,$path,$cmd,$fields,$options=array())
    function doPut($protokol,$IP,$req='',$get_data='',$post_data='',$options=array())
    { $__times['start']=get_formatted_microtime() ;
      $server_url=$protokol.$IP.$req ;
      if ($get_data) $server_url.='?'.$get_data ;
      $post_data = (is_array($post_data)) ? http_build_query($post_data) : $post_data;

      if($ch = curl_init($server_url))
      {
         //curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
         curl_setopt($ch, CURLOPT_REFERER, $_SERVER['HTTP_HOST']);
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
         curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Length: ' . strlen($post_data),
                                                    'Content-Type: application/json'
                                                    )
                    );
         curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
         $res=curl_exec($ch);

         $status = curl_getinfo($ch, CURLINFO_HTTP_CODE); //echo '$status='.$status.'<br>' ;
         if ($status!=200)
         { echo '<div class=alert>Ошибка ответа сервера: '.$status.'<br>'.$server_url.'<br>' ;
           print_r($post_data) ;
           echo '</div>' ;
         }
        $size=curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD);
        //echo 'CURLINFO_CONTENT_LENGTH_DOWNLOAD: '.$size.'<br>' ;

         // если не отправляем команду на себя - регистрируем запрос
         parse_str($get_data,$get_params) ;
         $__times['end']=get_formatted_microtime() ;
         $time_int=$__times['end']-$__times['start'] ;
         if (!$options['no_log_request']) LOGS()->log_request(IP,$IP,$get_params['operation'],$post_data,$res,array('debug_text'=>'время запроса: '.$time_int.' сек.')) ;

         curl_close($ch);

         return $res;
      }
      else return false;
    }

    function send_async_cmd($cmd,$options=array())
    { $__times['start']=get_formatted_microtime() ;
      $params=array('cmd'=>$cmd) ;
      if (is_array($options)) $params=array_merge($params,$options) ;
      $comm='http://'._MAIN_DOMAIN.'/async.php?'.http_build_query($params) ;
      exec('bash -c "/usr/bin/wget  \"'.$comm.'\" > /dev/null 2>&1 &"',$output); // тоже работает но не async
      $__times['end']=get_formatted_microtime() ;
      $time_int=$__times['end']-$__times['start'] ;
      $text=$cmd ;
      //$text.='<br>'.$comm ;
      //$text=http_build_query($params) ;
      LOGS()->reg_log('ASYNC CMD SET',$text,array('time_exec'=>$time_int)) ;
    }
}

?>