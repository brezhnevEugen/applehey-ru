<?
function LISTS(){ if (!is_object($GLOBALS['lists'])) $GLOBALS['lists']=new c_lists();return($GLOBALS['lists']);}

class c_lists
{
    function get_rec_id_by_name($name,$clss,$parent,$table_name)
    {  $usl_select='clss='.$clss.' and parent='.$parent.' and enabled=1 and UPPER(obj_name) like "'.$name.'"' ;
       $key_field=_CLSS($clss)->get_key_field() ;
       $rec=execSQL_van('select '.$key_field.',obj_name from '.$table_name.' where '.$usl_select) ;
       if (!$rec[$key_field])
       { $rec=_CLSS($clss)->obj_create(array('tkey'=>_DOT($table_name)->pkey,'parent'=>$parent,'obj_name'=>$name)) ;
         reboot_engine() ;
       }

       return($rec[$key_field]) ;
    }

    //==========================================================================================================================================================================
     // РАБОТА СО СПИСКАМИ НА САЙТЕ
     //==========================================================================================================================================================================

      function list_rec_create($list_id,$rec,$clss=20)
      {  $tkey=_table_id(TM_LIST) ;
         $rec['tkey']=$tkey ;
         $rec['parent']=$list_id ;
         $res=_CLSS($clss)->obj_create($rec) ;
         return($res) ;
      }

      function list_rec_delete($reffer)
      { list($pkey,$tkey)=explode('.',$reffer) ;
        $res=_CLSS(20)->obj_delete($tkey,$pkey) ;
        return($res) ;
      }

      function list_rec_copy($reffer)
      { //list($pkey,$tkey)=explode('.',$reffer) ;
        $rec=get_obj_info($reffer) ;
        unset($rec['_reffer'],$rec['_parent_reffer'],$rec['pkey']) ;
        $res=_CLSS(20)->obj_create($rec) ;
        return($res) ;
      }

      function list_rec_update($list_name,$id,$rec)
      {  $res=0 ;
         $rec_list=$_SESSION[$list_name][$id] ;
         if ($rec['name']) { $rec['obj_name']=$rec['name'] ; unset($rec['name']) ; }
         if ($rec_list['pkey'])
          { update_rec_in_table($rec_list['tkey'],$rec,'pkey='.$rec_list['pkey']) ;
            $_SESSION[$list_name][$id]=array_merge($_SESSION[$list_name][$id],$rec) ;
            $res=1 ;
          }
         return($res) ;
      }
}

?>