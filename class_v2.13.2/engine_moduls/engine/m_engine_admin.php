<?php // этот модуль необходимо делать тольео для крупных проектов, для небольшых описание пунктов меню и файлов меню лучше делать в init.php
$__functions['init'][]      ='_engine_admin_vars' ;
$__functions['boot_admin'][] ='_engine_admin_boot' ;
//$__functions['alert'][]   ='_engine_admin_alert' ;
$__functions['install'][]	='_engine_install_modul' ;



function _engine_admin_vars() //
{   //-----------------------------------------------------------------------------------------------------------------------------
	// описание переменных модуля
	//-----------------------------------------------------------------------------------------------------------------------------
    //$_SESSION['ARR_roll'][9]=array('obj_name'=>'Франчайзер',   'index_page'=>'/cab/bill/') ;

	//-----------------------------------------------------------------------------------------------------------------------------
	// описание классов модуля
	//-----------------------------------------------------------------------------------------------------------------------------



    //-----------------------------------------------------------------------------------------------------
    $_SESSION['descr_clss'][216]['name']='Права доступа' ;
    $_SESSION['descr_clss'][216]['table_code']='permit' ;
    $_SESSION['descr_clss'][216]['parent']=0 ;
    $_SESSION['descr_clss'][216]['fields']['obj_name']=array('type'=>'varchar(255)') ; //  название скрипта
    $_SESSION['descr_clss'][216]['fields']['rol_id']=array('type'=>'int(11)') ; //  доступ

    //-----------------------------------------------------------------------------------------------------
    $_SESSION['descr_clss'][251]['name']='Журнал событий' ;
    $_SESSION['descr_clss'][251]['parent']=0 ;
    $_SESSION['descr_clss'][251]['fields']['с_data']=array('type'=>'timedata','data_format'=>'d.m.y G:i') ; // время начала сенса
    $_SESSION['descr_clss'][251]['fields']['terminal_id']=array('type'=>'varchar(32)') ; // ID терминала
    $_SESSION['descr_clss'][251]['fields']['terminal_ip']=array('type'=>'varchar(15)') ; // IP терминала
    $_SESSION['descr_clss'][251]['fields']['member_id']=array('type'=>'int(11)') ; // код сотрудника организации
    $_SESSION['descr_clss'][251]['fields']['user_id']=array('type'=>'int(11)') ; // код сотрудника AСПМО
    $_SESSION['descr_clss'][251]['fields']['mo_id']=array('type'=>'int(11)') ; // код текущей сессии МО
    $_SESSION['descr_clss'][251]['fields']['obj_name']=array('type'=>'varchar(255)') ; // этап медосмотра
    $_SESSION['descr_clss'][251]['fields']['comment']='text' ;
    $_SESSION['descr_clss'][251]['fields']['data']='serialize' ;
    $_SESSION['descr_clss'][251]['fields']['time_exec']='float(10,5)' ;
    $_SESSION['descr_clss'][251]['fields']['reffer1']=array('type'=>'any_object')  ;
    $_SESSION['descr_clss'][251]['fields']['reffer2']=array('type'=>'any_object')  ;
    $_SESSION['descr_clss'][251]['fields']['reffer3']=array('type'=>'any_object')  ;
    $_SESSION['descr_clss'][251]['fields']['reffer4']=array('type'=>'any_object')  ;
    $_SESSION['descr_clss'][251]['fields']['reffer5']=array('type'=>'any_object')  ;
    $_SESSION['descr_clss'][251]['fields']['IP']=array('type'=>'varchar(15)')  ;
    $_SESSION['descr_clss'][251]['child_to']=array(1) ;
    $_SESSION['descr_clss'][251]['parent_to']=array(3) ;
    $_SESSION['descr_clss'][251]['no_show_in_tree']=0 ; // не показывать объекты класса в дереве
    $_SESSION['descr_clss'][251]['list']['field']['с_data']			=array('title'=>'Время начала сенса','td_class'=>'left') ;
    $_SESSION['descr_clss'][251]['list']['field']['terminal_id']    =array('title'=>'Интерфейс','td_class'=>'left') ;
    $_SESSION['descr_clss'][251]['list']['field']['member_id']	    =array('title'=>'Работник','td_class'=>'left') ;
    $_SESSION['descr_clss'][251]['list']['field']['obj_name']	    =array('title'=>'Операция','td_class'=>'left') ;
    $_SESSION['descr_clss'][251]['list']['field']['comment']	    =array('title'=>'Комментарий','td_class'=>'left') ;
    $_SESSION['descr_clss'][251]['details']=$_SESSION['descr_clss'][251]['list'] ;

    //-----------------------------------------------------------------------------------------------------
    $_SESSION['descr_clss'][253]['name']='Журнал обмена' ;
    $_SESSION['descr_clss'][253]['parent']=0 ;
    $_SESSION['descr_clss'][253]['fields']['с_data']=array('type'=>'timedata','data_format'=>'d.m.y G:i') ; // время запроса
    $_SESSION['descr_clss'][253]['fields']['r_data']=array('type'=>'timedata','data_format'=>'d.m.y G:i') ; // время ответа
    $_SESSION['descr_clss'][253]['fields']['IP1']=array('type'=>'varchar(19)') ; // IP запроса
    $_SESSION['descr_clss'][253]['fields']['cmd']=array('type'=>'varchar(32)') ; // команда
    $_SESSION['descr_clss'][253]['fields']['mess1']=array('type'=>'longtext') ; // запрос
    $_SESSION['descr_clss'][253]['fields']['status']=array('type'=>'int(11)') ; // статус ответа - 200,404,500
    $_SESSION['descr_clss'][253]['fields']['IP2']=array('type'=>'varchar(19)') ; // IP ответа
    $_SESSION['descr_clss'][253]['fields']['mess2']=array('type'=>'longtext') ; // ответ
    $_SESSION['descr_clss'][253]['fields']['terminal_id']=array('type'=>'int(11)') ; // id терминала
    $_SESSION['descr_clss'][253]['fields']['mo_id']=array('type'=>'int(11)') ; // id MO
    $_SESSION['descr_clss'][253]['fields']['personal_id']=array('type'=>'int(11)') ; // id Персонала
    $_SESSION['descr_clss'][253]['fields']['evt_id']=array('type'=>'int(11)') ; // id события в журнале событий
    $_SESSION['descr_clss'][253]['fields']['message']=array('type'=>'varchar(32)') ; // message, fail
    $_SESSION['descr_clss'][253]['fields']['size1']=array('type'=>'int(11)') ; //
    $_SESSION['descr_clss'][253]['fields']['size2']=array('type'=>'int(11)') ; //
    $_SESSION['descr_clss'][253]['fields']['debug_text']=array('type'=>'text') ; //
    $_SESSION['descr_clss'][253]['fields']['reffer1']=array('type'=>'any_object')  ;
    $_SESSION['descr_clss'][253]['fields']['reffer2']=array('type'=>'any_object')  ;
    $_SESSION['descr_clss'][253]['fields']['reffer3']=array('type'=>'any_object')  ;
    $_SESSION['descr_clss'][253]['fields']['reffer4']=array('type'=>'any_object')  ;
    $_SESSION['descr_clss'][253]['fields']['reffer5']=array('type'=>'any_object')  ;

    $_SESSION['descr_clss'][253]['list']['field']['с_data']          =array('title'=>'Дата от') ;
    $_SESSION['descr_clss'][253]['list']['field']['status']          =array('title'=>'status') ;
    $_SESSION['descr_clss'][253]['list']['field']['IP1']            =array('title'=>'IP1') ;
    $_SESSION['descr_clss'][253]['list']['field']['cmd']            =array('title'=>'cmd') ;
    $_SESSION['descr_clss'][253]['list']['field']['mess1']            =array('title'=>'mess1','view'=>'view_mess1', 'script'=>'mod/aspmo/i_field_views.php') ;
    $_SESSION['descr_clss'][253]['list']['field']['IP2']            =array('title'=>'IP2') ;
    $_SESSION['descr_clss'][253]['list']['field']['mess2']            =array('title'=>'mess2','view'=>'view_mess2', 'script'=>'mod/aspmo/i_field_views.php') ;


    //-----------------------------------------------------------------------------------------------------
    $_SESSION['descr_clss'][255]['name']='Отчет' ;
    $_SESSION['descr_clss'][255]['parent']=0 ;
    $_SESSION['descr_clss'][255]['fields']['с_data']=array('type'=>'timedata','data_format'=>'d.m.y G:i') ; // время запроса
    $_SESSION['descr_clss'][255]['fields']['r_data']=array('type'=>'timedata','data_format'=>'d.m.y G:i') ; // время ответа
    $_SESSION['descr_clss'][255]['fields']['member_id']=array('type'=>'int(11)') ; // IP запроса
    $_SESSION['descr_clss'][255]['fields']['value']=array('type'=>'longtext') ; // команда
    $_SESSION['descr_clss'][255]['fields']['mo_ids']=array('type'=>'longtext') ; // список id mo, которые проходят по отчету
    $_SESSION['descr_clss'][255]['fields']['p_data']=array('type'=>'timedata','data_format'=>'d.m.y G:i') ; // время подписания
    $_SESSION['descr_clss'][255]['fields']['podpis']=array('type'=>'text') ; // слепок эцп
    $_SESSION['descr_clss'][255]['fields']['info']=array('type'=>'text') ; // параметры создания отчета
    $_SESSION['descr_clss'][255]['fields']['comment']=array('type'=>'text') ; // комментарий автора отчета

    //-----------------------------------------------------------------------------------------------------
    $_SESSION['descr_clss'][256]['name']='Шаблон отчета' ;
    $_SESSION['descr_clss'][256]['parent']=20 ;
    $_SESSION['descr_clss'][256]['fields']['template_name']=array('type'=>'varchar(255)') ; // IP запроса

    $_SESSION['descr_clss'][256]['list']['field']['id']		        	=array('title'=>'Код.') ;
    $_SESSION['descr_clss'][256]['list']['field']['enabled']			=array('title'=>'Сост.') ;
    $_SESSION['descr_clss'][256]['list']['field']['obj_name']			=array('title'=>'Название','td_class'=>'left') ;
    $_SESSION['descr_clss'][256]['list']['field']['template_name']      =array('title'=>'Шазвание шаблона') ;

    $_SESSION['descr_clss'][260]['name']='Сообщение в монитор' ;
    $_SESSION['descr_clss'][260]['parent']=0 ;
    $_SESSION['descr_clss'][260]['table_code']='mess' ;

    $_SESSION['descr_clss'][260]['fields']['obj_name']=array('type'=>'serialize') ;
    $_SESSION['descr_clss'][260]['fields']['grp']=array('type'=>'int(11)') ; //



    

}

function _engine_admin_boot($options)
{
    create_system_modul_obj('engine',$options) ;

}


function _engine_install_modul($DOT_root)
{    echo '<h2>Инсталируем модуль <strong>ЯДРО СИСТЕМЫ</strong></h2>' ;

    // Журнал событий
    $pattern=array() ;
    $pattern['table_title']		        = 	'Журнал событий' ;
    $pattern['use_clss']		        = 	'1,251' ;
    $pattern['table_name']				= 	'obj_site_mo_log' ;
    $pattern['table_clss']				= 	101 ;
    $pattern['table_parent']			= 	$DOT_root;
    create_table_by_pattern($pattern) ;

    // Журнал событий
    $pattern=array() ;
    $pattern['table_title']		        = 	'Журнал обмена' ;
    $pattern['use_clss']		        = 	'1,253' ;
    $pattern['table_name']				= 	'obj_site_req_log' ;
    $pattern['table_clss']				= 	101 ;
    $pattern['table_parent']			= 	$DOT_root;
    create_table_by_pattern($pattern) ;

    // сообщения SSE
    $pattern=array() ;
    $pattern['table_title']		        = 	'Стек сообщений' ;
    $pattern['use_clss']		        = 	'260' ;
    //$pattern['def_recs'][]		        =	array ('parent'=>0,	'clss'=>1,	'enabled'=>1,	'indx'=>0,	'obj_name'=>'Аккаунты') ;
    $pattern['table_name']				= 	'obj_site_mess' ;
    $pattern['table_clss']				= 	101 ;
    $pattern['table_parent']			= 	$DOT_root;
    create_table_by_pattern($pattern) ;

    // разграничение доступа пользователей к скриптам
    $pattern=array() ;
    $pattern['table_title']		        = 	'Разграничение доступа' ;
    $pattern['use_clss']		        = 	'216' ;
    $pattern['def_recs'][]		        =	array ('parent'=>0,	'clss'=>1,	'enabled'=>1,	'indx'=>0,	'obj_name'=>'Права доступа') ;
    $pattern['table_name']				= 	'obj_site_permit' ;
    $pattern['table_clss']				= 	101 ;
    $pattern['table_parent']			= 	$DOT_root;
    create_table_by_pattern($pattern) ;


     $file_items=array() ;
     $file_items['aspmo_setup.php']['include'][]				='_DIR_EXT."/aspmo_setup/m_aspmo_setup_frames.php"' ;
     $file_items['aspmo_setup.php']['options']['title']		='"Служебные операции"' ;
     $file_items['aspmo_setup.php']['options']['use_class']	='"c_aspmo_setup"' ;
     $file_items['aspmo_setup_tree.php']['include'][]			='_DIR_EXT."/aspmo_setup/m_aspmo_setup_frames.php"' ;
     $file_items['aspmo_setup_viewer.php']['include'][]		='_DIR_EXT."/aspmo_setup/m_aspmo_setup_frames.php"' ;
     create_menu_item($file_items) ;

}




?>