<?php // этот модуль необходимо делать тольео для крупных проектов, для небольшых описание пунктов меню и файлов меню лучше делать в init.php
$__functions['init'][]      ='_personal_admin_vars' ;
$__functions['boot_admin'][] ='_personal_admin_boot' ;
//$__functions['alert'][]   ='_personal_admin_alert' ;
$__functions['install'][]	='_personal_install_modul' ;



function _personal_admin_vars()
{

    //Юр.лицо
    $_SESSION['descr_clss'][207]['name']='Организация' ;
    $_SESSION['descr_clss'][207]['parent']=1 ;
    $_SESSION['descr_clss'][207]['fields']['obj_name']=array('type'=>'varchar(255)') ;
    $_SESSION['descr_clss'][207]['fields']['full_name']=array('type'=>'varchar(256)') ;
    //$_SESSION['descr_clss'][207]['fields']['email']=array('type'=>'varchar(128)') ;
    //$_SESSION['descr_clss'][207]['fields']['phone']=array('type'=>'varchar(32)') ;
    //$_SESSION['descr_clss'][207]['fields']['phone2']=array('type'=>'varchar(32)') ;
    //$_SESSION['descr_clss'][207]['fields']['email']=array('type'=>'varchar(255)') ;
    //$_SESSION['descr_clss'][207]['fields']['inn']=array('type'=>'varchar(16)') ;
    //$_SESSION['descr_clss'][207]['fields']['kpp']=array('type'=>'varchar(16)') ;
    //$_SESSION['descr_clss'][207]['fields']['bik']=array('type'=>'varchar(16)') ;
    //$_SESSION['descr_clss'][207]['fields']['bank']=array('type'=>'varchar(255)') ;
    //$_SESSION['descr_clss'][207]['fields']['ks']=array('type'=>'varchar(32)') ;
    //$_SESSION['descr_clss'][207]['fields']['rs']=array('type'=>'varchar(32)') ;
    //$_SESSION['descr_clss'][207]['fields']['adres1']=array('type'=>'varchar(255)') ;
    //$_SESSION['descr_clss'][207]['fields']['adres2']=array('type'=>'varchar(255)') ;
    //$_SESSION['descr_clss'][207]['fields']['director_fio']=array('type'=>'varchar(255)') ;
    //$_SESSION['descr_clss'][207]['fields']['director_working']=array('type'=>'varchar(255)') ;
    //$_SESSION['descr_clss'][207]['fields']['buh_fio']=array('type'=>'varchar(255)') ;
    //$_SESSION['descr_clss'][207]['fields']['buh_working']=array('type'=>'varchar(255)') ;
    //$_SESSION['descr_clss'][207]['fields']['read_only']=array('type'=>'int(1)') ;
    //$_SESSION['descr_clss'][207]['fields']['client_id']=array('type'=>'int(11)') ;
    $_SESSION['descr_clss'][207]['fields']['demo']=array('type'=>'int(1)') ;
    $_SESSION['descr_clss'][207]['fields']['HR_ID']=array('type'=>'int(11)') ;


    $_SESSION['descr_clss'][207]['list']['field']['pkey']=array('title'=>'ID') ;
    $_SESSION['descr_clss'][207]['list']['field']['enabled']=array('title'=>'Состояние') ;
    //$_SESSION['descr_clss'][207]['list']['field']['read_only']=array('title'=>'Запрет изменений') ;
    $_SESSION['descr_clss'][207]['list']['field']['obj_name']=array('title'=>'Наименование') ;
    //$_SESSION['descr_clss'][207]['list']['field']['full_name']=array('title'=>'Полное наименование') ;
    //$_SESSION['descr_clss'][207]['list']['field']['email']=array('title'=>'email') ;
    //$_SESSION['descr_clss'][207]['list']['field']['phone']=array('title'=>'Телефон') ;
    //$_SESSION['descr_clss'][207]['list']['field']['phone2']=array('title'=>'Доп.телефон') ;
    //$_SESSION['descr_clss'][207]['list']['field']['inn']=array('title'=>'ИНН') ;
    //$_SESSION['descr_clss'][207]['list']['field']['kpp']=array('title'=>'КПП') ;
    //$_SESSION['descr_clss'][207]['list']['field']['bik']=array('title'=>'БИК') ;
    //$_SESSION['descr_clss'][207]['list']['field']['bank']=array('title'=>'Банк') ;
    //$_SESSION['descr_clss'][207]['list']['field']['ks']=array('title'=>'Корреспонденский счет') ;
    //$_SESSION['descr_clss'][207]['list']['field']['rs']=array('title'=>'Расчетный счет') ;
    //$_SESSION['descr_clss'][207]['list']['field']['adres1']=array('title'=>'Юр.адрес') ;
    //$_SESSION['descr_clss'][207]['list']['field']['adres2']=array('title'=>'Почт.адрес') ;
    //$_SESSION['descr_clss'][207]['list']['field']['director_fio']=array('title'=>'ФИО директора') ;
    //$_SESSION['descr_clss'][207]['list']['field']['director_working']=array('title'=>'Должность директора') ;
    //$_SESSION['descr_clss'][207]['list']['field']['buh_fio']=array('title'=>'ФИО бухгалтера') ;
    //$_SESSION['descr_clss'][207]['list']['field']['buh_working']=array('title'=>'Должность бухгалтера') ;
    //$_SESSION['descr_clss'][207]['list']['field']['client_id']=array('title'=>'Client ID') ;
    $_SESSION['descr_clss'][207]['list']['field']['demo']=array('title'=>'demo') ;


    $_SESSION['descr_clss'][208]['name']='Подразделение' ;
    $_SESSION['descr_clss'][208]['parent']=1 ;
    $_SESSION['descr_clss'][208]['fields']['obj_name']=array('type'=>'varchar(255)') ;
    $_SESSION['descr_clss'][208]['fields']['read_only']=array('type'=>'int(1)') ;
    $_SESSION['descr_clss'][208]['fields']['HR_ID']=array('type'=>'int(11)') ;


    $_SESSION['descr_clss'][208]['list']['field']['pkey']=array('title'=>'ID') ;
    $_SESSION['descr_clss'][208]['list']['field']['enabled']=array('title'=>'Состояние') ;
    $_SESSION['descr_clss'][208]['list']['field']['obj_name']=array('title'=>'Наименование') ;
    $_SESSION['descr_clss'][208]['list']['field']['read_only']=array('title'=>'Запрет изменений') ;



    //-----------------------------------------------------------------------------------------------------
    $_SESSION['descr_clss'][211]['name']='Персонал компании' ;
    $_SESSION['descr_clss'][211]['parent']=1 ;
    $_SESSION['descr_clss'][211]['parent_to']=array(3) ;
    $_SESSION['descr_clss'][211]['child_to']=array(207,208) ;
    $_SESSION['descr_clss'][211]['fields']=array('obj_name'=>'varchar(255)') ;
    $_SESSION['descr_clss'][211]['fields']['name1']=array('type'=>'varchar(255)') ;
    $_SESSION['descr_clss'][211]['fields']['name2']=array('type'=>'varchar(255)') ;
    $_SESSION['descr_clss'][211]['fields']['name3']=array('type'=>'varchar(255)') ;
    $_SESSION['descr_clss'][211]['fields']['working']=array('type'=>'indx_select','array'=>'IL_works') ;
    $_SESSION['descr_clss'][211]['fields']['working_name']=array('type'=>'varchar(255)') ;
    $_SESSION['descr_clss'][211]['fields']['propusk']=array('type'=>'int(11)') ;
    $_SESSION['descr_clss'][211]['fields']['driving']=array('type'=>'varchar(32)') ;
    $_SESSION['descr_clss'][211]['fields']['permit']=array('type'=>'varchar(32)') ;
    $_SESSION['descr_clss'][211]['fields']['tab_number']=array('type'=>'varchar(32)') ;
    $_SESSION['descr_clss'][211]['fields']['adres']=array('type'=>'text') ;
    $_SESSION['descr_clss'][211]['fields']['phone']=array('type'=>'varchar(32)') ;
    $_SESSION['descr_clss'][211]['fields']['dr']='varchar(10)' ;
    $_SESSION['descr_clss'][211]['fields']['age']='int(11)' ;
    $_SESSION['descr_clss'][211]['fields']['pol']=array('type'=>'int(1)','default'=>0) ;
    $_SESSION['descr_clss'][211]['fields']['s_min']='int(11)' ;
    $_SESSION['descr_clss'][211]['fields']['s_max']='int(11)' ;
    $_SESSION['descr_clss'][211]['fields']['d_min']='int(11)' ;
    $_SESSION['descr_clss'][211]['fields']['d_max']='int(11)' ;
    $_SESSION['descr_clss'][211]['fields']['t_min']='float(10,1)' ;
    $_SESSION['descr_clss'][211]['fields']['t_max']='float(10,1)' ;
    $_SESSION['descr_clss'][211]['fields']['p_min']='int(11)' ;
    $_SESSION['descr_clss'][211]['fields']['p_max']='int(11)' ;
    $_SESSION['descr_clss'][211]['fields']['a']='int(11)' ; // 1 - не производить алкотест ; 2 - делать алкотест
    $_SESSION['descr_clss'][211]['fields']['n']='int(11)' ; // 1 - не производить наркотест ; 2 - делать наркотест
    $_SESSION['descr_clss'][211]['fields']['smena_s']='varchar(5)' ; // время начала смены
    $_SESSION['descr_clss'][211]['fields']['smena_e']='varchar(5)' ; // время окончания смены
    $_SESSION['descr_clss'][211]['fields']['full_access']='int(1)' ; // проход всегда
    $_SESSION['descr_clss'][211]['fields']['demo_access']='int(1)' ; // демо-доступ
    $_SESSION['descr_clss'][211]['fields']['ext']='int(1)' ; // внесен по доп.спискам
    $_SESSION['descr_clss'][211]['fields']['ext_info']='serialize' ; // внесен по доп.спискам
    $_SESSION['descr_clss'][211]['fields']['uid']='varchar(128)' ; // внесен по доп.спискам
    $_SESSION['descr_clss'][211]['fields']['upl_img']=array('type'=>'serialize') ; // доп.инфо
    $_SESSION['descr_clss'][211]['fields']['s_medspravka']=array('type'=>'timedata') ; // дата выдача медсправки
    $_SESSION['descr_clss'][211]['fields']['status']=array('type'=>'indx_select','array'=>'IL_status') ; // уволен, отпуск, болен
    $_SESSION['descr_clss'][211]['fields']['fired']=array('type'=>'timedata') ; // дата увольнения
    $_SESSION['descr_clss'][211]['fields']['fired_data']=array('type'=>'timedata') ; // дата увольнения
    $_SESSION['descr_clss'][211]['fields']['_image_name']=array('type'=>'varchar(255)') ;
    $_SESSION['descr_clss'][211]['fileds']['client_id']='int(11)';
    $_SESSION['descr_clss'][211]['fields']['sign_id']=array('type'=>'int(11)') ;

    // поля для стыковки с HR
    //$_SESSION['descr_clss'][211]['fields']['person_id']=array('type'=>'int(11)') ; // person_id по HR
    $_SESSION['descr_clss'][211]['fields']['iNet_ID']=array('type'=>'int(11)') ;
    $_SESSION['descr_clss'][211]['fields']['iNet_name']=array('type'=>'varchar(255)') ;
    $_SESSION['descr_clss'][211]['fields']['HR_ID']=array('type'=>'int(11)') ;
    $_SESSION['descr_clss'][211]['fields']['ASSIGNMENT_ID']=array('type'=>'int(11)') ;
    $_SESSION['descr_clss'][211]['fields']['EMP_NUMBER']=array('type'=>'varchar(32)') ;
    $_SESSION['descr_clss'][211]['fields']['ASS_NUMBER']=array('type'=>'varchar(32)') ;
    $_SESSION['descr_clss'][211]['fields']['PROPUSK_NUMBER']=array('type'=>'int(11)') ;
    //$_SESSION['descr_clss'][211]['fields']['snils']=array('type'=>'int(11)') ; // snils по HR
    //$_SESSION['descr_clss'][211]['fields']['table_number']=array('type'=>'int(11)') ; // table_number по HR



    //$_SESSION['descr_clss'][211]['child_to']=array(1) ;
    //$_SESSION['descr_clss'][211]['parent_to']=array(3) ;
    $_SESSION['descr_clss'][211]['no_show_in_tree']=0 ; // не показывать объекты класса в дереве
    $_SESSION['descr_clss'][211]['icons']=_PATH_TO_BASED_CLSS_IMG.'/98.png' ;
    $_SESSION['descr_clss'][211]['list']['field']['pkey']			=array('title'=>'ID') ;
    $_SESSION['descr_clss'][211]['list']['field']['HR_ID']			=array('title'=>'HR_ID') ;
    $_SESSION['descr_clss'][211]['list']['field']['PROPUSK_NUMBER']			=array('title'=>'PROPUSK_NUMBER') ;
    $_SESSION['descr_clss'][211]['list']['field']['ASSIGNMENT_ID']			=array('title'=>'ASSIGNMENT_ID') ;
    $_SESSION['descr_clss'][211]['list']['field']['ASS_NUMBER']			=array('title'=>'ASS_NUMBER') ;
    $_SESSION['descr_clss'][211]['list']['field']['EMP_NUMBER']			=array('title'=>'EMP_NUMBER') ;
    $_SESSION['descr_clss'][211]['list']['field']['uid']			=array('title'=>'UID') ;
    $_SESSION['descr_clss'][211]['list']['field']['enabled']			=array('title'=>'Сост.') ;
    $_SESSION['descr_clss'][211]['list']['field']['obj_name']			=array('title'=>'ФИО','td_class'=>'left') ;
    $_SESSION['descr_clss'][211]['list']['field']['working_name']		=array('title'=>'Должность','td_class'=>'left') ;
    $_SESSION['descr_clss'][211]['list']['field']['dr']			        =array('title'=>'Дата рождения','td_class'=>'left') ;
    $_SESSION['descr_clss'][211]['list']['field']['propusk']			=array('title'=>'Номер пропуска','td_class'=>'left') ;
    //$_SESSION['descr_clss'][211]['list']['field']['tab_number']			=array('title'=>'Табельный номер','td_class'=>'left') ;
    $_SESSION['descr_clss'][211]['list']['field']['status']			    =array('title'=>'Статус','td_class'=>'left') ;
    $_SESSION['descr_clss'][211]['details']=$_SESSION['descr_clss'][211]['list'] ;

    //-----------------------------------------------------------------------------------------------------
    $_SESSION['descr_clss'][212]['name']='Запись в табеле' ;
    $_SESSION['descr_clss'][212]['parent']=0 ;
    $_SESSION['descr_clss'][212]['fields']=array('member_id'=>'int(11)','data'=>'int(11)','smena'=>'int(11)') ;
    $_SESSION['descr_clss'][212]['child_to']=array(1) ;
    $_SESSION['descr_clss'][212]['no_show_in_tree']=0 ; // не показывать объекты класса в дереве
    $_SESSION['descr_clss'][212]['icons']=_PATH_TO_BASED_CLSS_IMG.'/98.png' ;

    //-----------------------------------------------------------------------------------------------------
    $_SESSION['descr_clss'][214]['name']='Запись в табеле HR' ;
    $_SESSION['descr_clss'][214]['table_code']='tabel_HR' ;
    $_SESSION['descr_clss'][214]['parent']=0 ;
    $_SESSION['descr_clss'][214]['fields']=array('table_id'=>'int(11)','complex'=>'text') ;
    $_SESSION['descr_clss'][214]['fields']['person_id']=array('type'=>'int(11)') ;
    $_SESSION['descr_clss'][214]['fields']['date_start']=array('type'=>'timedata','data_format'=>'d.m.y G:i') ; // время начала смены
    $_SESSION['descr_clss'][214]['fields']['date_end']=array('type'=>'timedata','data_format'=>'d.m.y G:i') ; // время окончания смены
    $_SESSION['descr_clss'][214]['fields']['guests_allowed']=array('type'=>'int(1)') ; // 1 = разрешено проводить гостей
    $_SESSION['descr_clss'][214]['fields']['access_type']=array('type'=>'int(1)') ; // 0 =resticted 1=free

    $_SESSION['descr_clss'][214]['no_show_in_tree']=0 ; // не показывать объекты класса в дереве
    $_SESSION['descr_clss'][214]['icons']=_PATH_TO_BASED_CLSS_IMG.'/98.png' ;




}

function _personal_admin_boot($options)
{
  create_system_modul_obj('personal') ;
}


function _personal_install_modul($DOT_root)
{   echo '<h2>Инсталируем модуль <strong>ПЕРСОНАЛ</strong></h2>' ;

    // персонал компании
    $pattern=array() ;
    $pattern['table_title']		        = 	'Клиенты компании' ;
    $pattern['use_clss']		        = 	'1,200' ;
    $pattern['def_recs'][]		        =	array ('parent'=>0,	'clss'=>1,	'enabled'=>1,	'indx'=>0,	'obj_name'=>'Персонал компании') ;
    $pattern['table_name']				= 	'obj_site_members' ;
    $pattern['table_clss']				= 	101 ;
    $pattern['table_parent']			= 	$DOT_root;
    $id_DOT_personal=create_table_by_pattern($pattern) ;

    $pattern=array() ;
    $pattern['table_title']				= 	'Фото' ;
    $pattern['use_clss']				= 	'3' ;
    $pattern['table_name']				= 	'obj_site_members_image' ;
    $pattern['table_clss']				= 	101 ;
    $pattern['table_parent']			= 	$id_DOT_personal;
    $pattern['dir_to_image']			= 	'/public/personals/';
    create_table_by_pattern($pattern) ;


    // создавем вьюверы
       $sql='DROP VIEW view_members' ;
       execSQL_update($sql,array('debug'=>2)) ;
       $sql='create view view_members as
                   select  t1.*,
                           (select t3.file_name from obj_site_members_image t3 where t3.parent=t1.pkey and  t3.clss=3 and t3.enabled=1 and not (t3.file_name="" or t3.file_name is null)  order by t3.indx limit 1) as _image_name
                   from obj_site_members t1

                   ' ;
       $sql='create view view_members as
                   select  t1.*
                   from obj_site_members t1

                   ' ;
       execSQL_update($sql,array('debug'=>2)) ;


}




?>