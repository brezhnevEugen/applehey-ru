<?php
$__functions['init'][]='_personal_site_vars' ;
$__functions['boot_site'][]='_personal_site_boot' ;

function PERSONAL() {return($_SESSION['personal_system']);$_SESSION['personal_system']=new c_personal_system();return($_SESSION['personal_system']);} ;



function _personal_site_vars()
{


    $_SESSION['init_options']['personal']['debug']=0 ;
	//$_SESSION['init_options']['goods']['patch_mode']='TREE_NAME' ;
    $_SESSION['init_options']['personal']['patch_mode']='NAME' ;
	$_SESSION['init_options']['personal']['root_dir']	='/cab/personal/' ;
    $_SESSION['init_options']['personal']['clss_items']='211' ;
    //$_SESSION['init_options']['personal']['usl_show_items']='enabled=1 and _enabled=1' ;
    $_SESSION['init_options']['personal']['usl_show_items']='_enabled=1' ; // показываем всех сотрудников, уволенных и действующих
    $_SESSION['init_options']['personal']['tree_fields']='pkey,parent,clss,indx,enabled,obj_name' ;

    
    // определяем условия для формирование дерева
    //$_SESSION['init_options']['personal']['tree']['debug']=0 ;
    $_SESSION['init_options']['personal']['tree']['clss']='1,207,208' ;
    $_SESSION['init_options']['personal']['tree']['include_space_section']=1 ;
    $_SESSION['init_options']['personal']['tree']['order_by']='indx' ;
    $_SESSION['init_options']['personal']['tree']['get_count_by_clss']=0 ;

    $_SESSION['img_pattern']['personal'][400]['resize']=array('width'=>400,'resampled'=>1,'quality'=>100,'back_color'=>'ffffff');
    $_SESSION['img_pattern']['personal'][400]['no_photo']='/images/def_400.jpg';
    $_SESSION['img_pattern']['personal'][200]['resize']=array('width'=>200,'height'=>266,'fields'=>0,'resampled'=>1,'quality'=>100,'back_color'=>'ffffff');
    $_SESSION['img_pattern']['personal'][200]['no_photo']='/images/def_200.jpg';
    $_SESSION['img_pattern']['personal']['small']['no_photo']='/images/def_small.jpg';

}

function _personal_site_boot($options)
{ if (isset($options['personal_system'])) $options=$options['personal_system'] ;
  //damp_array($options) ;
  create_system_modul_obj('personal',$options) ;
}


//class c_personal_system  extends c_system_catalog
class c_personal_system  extends c_system
{
 public $table_personal ;
 public $view_personal ;
 public $person_dir_name ;

 function c_personal_system($create_options=array())
 { global $site_TM_kode ;
   $this->table_name ='obj_'.$site_TM_kode.'_members' ;
   $this->table_personal ='obj_'.$site_TM_kode.'_members' ;
   $this->view_personal ='view_members' ;
   $this->tkey_personal =_DOT($this->table_personal)->pkey ;
   $this->table_tabel ='obj_'.$site_TM_kode.'_tabel' ;
   parent::__construct($create_options) ;

   //регистрируем отчеты
   $this->system_title ='Персонал';
   //REPORTS()->reg_reports($this,'report_clients',      'reports/report_clients.php','report_clients') ;         //
   //REPORTS()->reg_reports($this,'report_clients_favorite',      'reports/report_clients_favorite.php','report_clients_favorite') ;         //
   //REPORTS()->reg_reports($this,'report_clients_exists',      'reports/report_clients_exists.php','report_clients_exists') ;         //
   //REPORTS()->reg_reports($this,'report_clients_my',      'reports/report_clients_my.php','report_clients_my') ;         //
   //REPORTS()->reg_reports($this,'persons',        'aspmo_bp/report_persons.php','report_persons') ;         //

   ENGINE()->reg_ext('personal','Клиенты') ;


   //parent::c_system_catalog($create_options) ;
   $this->person_dir_name=($create_options['person_dir_name'])? $create_options['person_dir_name']:'person' ;
 }

  function declare_menu_items_2($page)
  {   //$page->menu['/cab/reports/report_clients/']=array('title'=>'Список клиентов','class'=>'fa fa-users') ;
      //$page->menu['/cab/reports/report_clients_my/']=array('title'=>'Мои клиенты','class'=>'fa fa-user') ;
      //$page->menu['/cab/reports/report_clients_favorite/']=array('title'=>'Избранные клиенты','class'=>'fa fa-flag') ;
      //$page->menu['/cab/reports/report_clients_exists/']=array('title'=>'Посетившие клиенты','class'=>'fa fa-clock-o') ;
      //$page->menu['/cab/personal/create/']=array('title'=>'Добавить клиента','class'=>'fa fa-plus-circle') ;
  }


 function declare_menu_items_3($page)
  {
      /*
      $page->menu['/cab/reports/']=array('title'=>'Отчеты',
                                    'class'=>'fa fa-file-text-o',
                                    //'items'=>array('/cab/reports/'=>array('title'=>'Создать отчет','class'=>'fa fa-users'))
                                    ) ;
    */
  }

    function personal_save($reffer,$form_data)
    { $rec=$this->get_personal_by_id($reffer) ; $res=0 ; // damp_array($rec,1,-1) ;
      if ($rec['pkey'])
      { $data=array() ;
        if (isset($form_data['propusk']))  $data['propusk']=$form_data['propusk'] ;
        if (isset($form_data['driving']))  $data['driving']=$form_data['driving'] ;
        if (isset($form_data['tab_number']))  $data['tab_number']=$form_data['tab_number'] ;
        if (isset($form_data['name']))     $data['obj_name']=$form_data['name'] ;
        if (isset($form_data['dr']))       $data['dr']=$form_data['dr'] ;
        if (isset($form_data['pol']))      $data['pol']=$form_data['pol'] ;
        if (isset($form_data['adres']))    $data['adres']=$form_data['adres'] ;
        if ($form_data['org_id'])          $data['parent']=$form_data['org_id'] ;
        if ($form_data['otdel_id'])        $data['parent']=$form_data['otdel_id'] ;
        if ($form_data['otdel_name'])      $data['parent']=LISTS()->get_rec_id_by_name($form_data['otdel_name'],208,$form_data['org_id'],$this->table_personal) ;
        if (($form_data['working']))       $data['working']=$form_data['working'] ;
        if ($form_data['working_name'])    $data['working']=LISTS()->get_rec_id_by_name($form_data['working_name'],20,32,TM_LIST) ;
        else if (isset($form_data['working_name'])) $data['working']='' ;
        if ($data['dr'])                   $data['age']=ENGINE()->get_age_by_dr($data['dr']) ;
        if (isset($form_data['full_access'])) $data['full_access']=$form_data['full_access'] ;
        if (isset($form_data['demo_access'])) $data['demo_access']=$form_data['demo_access'] ;
        if (isset($form_data['smena_s']))  $data['smena_s']=$form_data['smena_s'] ;
        if (isset($form_data['smena_e']))  $data['smena_e']=$form_data['smena_e'] ;
        if (isset($form_data['s_medspravka']))  $data['s_medspravka']=strtotime($form_data['s_medspravka']) ;
        if ($form_data['fired'])           $data['enabled']=0 ;
        else                               $data['enabled']=1 ;
        if ($rec['enabled'] and $form_data['fired'])
        { $data['fired_data']=time() ;
          LOGS()->reg_log('Измение данных сотрудника',$rec['obj_name'].' => УВОЛЕН',array('personal_id'=>$rec['pkey'])) ;
        }
        if (!$rec['enabled'] and !$form_data['fired'])
        { $data['fired_data']=0 ;
          LOGS()->reg_log('Измение данных сотрудника',$rec['obj_name'].' => ВОССТАНОВЛЕН',array('personal_id'=>$rec['pkey'])) ;
        }
        //damp_array($form_data);
        //damp_array($data);
        $res=update_rec_in_table($this->table_personal,$data,'pkey='.$rec['pkey'],array('debug'=>0)) ;
      }
      return($res) ;
    }

    function personal_create($form_data)
    { $data=array('parent'=>1,'clss'=>211) ; // damp_array($form_data,1,-1) ;
      if (isset($form_data['propusk']))  $data['propusk']=$form_data['propusk'] ;
      if (isset($form_data['driving']))  $data['driving']=$form_data['driving'] ;
      if (isset($form_data['tab_number']))  $data['tab_number']=$form_data['tab_number'] ;
      if (isset($form_data['name']))     $data['obj_name']=$form_data['name'] ;
      if (isset($form_data['dr']))       $data['dr']=$form_data['dr'] ;
      if (isset($form_data['pol']))      $data['pol']=$form_data['pol'] ;
      if (isset($form_data['adres']))    $data['adres']=$form_data['adres'] ;
      if (isset($form_data['s_medspravka']))    $data['s_medspravka']=$form_data['s_medspravka'] ;
      if (isset($form_data['company']))  $data['parent']=$form_data['company'] ;
      //if (isset($form_data['otdel']))    $data['parent']=$form_data['otdel'] ;
      if ($form_data['org_id'])          $data['parent']=$form_data['org_id'] ;
      if ($form_data['otdel_id'])        $data['parent']=$form_data['otdel_id'] ;
      if ($form_data['otdel_name'])      $data['parent']=LISTS()->get_rec_id_by_name($form_data['otdel_name'],208,$form_data['org_id'],$this->table_personal) ;
      if (isset($form_data['working_name'])) $data['working']=LISTS()->get_rec_id_by_name($form_data['working_name'],20,32,TM_LIST) ;
      if (isset($form_data['working']))  $data['working']=$form_data['working'] ;
      if ($data['dr'])                   $data['age']=ENGINE()->get_age_by_dr($data['dr']) ;
      if ($form_data['to_extlist'])      $data['ext']=1 ;
      if ($form_data['full_access'])      $data['full_access']=1 ;
      if ($form_data['demo_access'])      $data['demo_access']=1 ;
      if (isset($form_data['smena_s']))  $data['smena_s']=$form_data['smena_s'] ;
      if (isset($form_data['smena_e']))  $data['smena_e']=$form_data['smena_e'] ;
      if (isset($form_data['s_medspravka']))  $data['s_medspravka']=strtotime($form_data['s_medspravka']) ;
      //damp_array($data,1,-1) ;
      $member_reffer=adding_rec_to_table($this->table_personal,$data,array('return_reffer'=>1,'debug'=>0)) ;
      return($member_reffer) ;
    }

    function get_personal_list($options=array())
    { $_usl=array() ;
      $_usl[]='clss=211' ;
      $_usl[]='enabled=1' ;
      if ($options['ext']) $_usl[]='ext=1' ;
      if ($options['id'] and is_array($options['id'])) $_usl[]='pkey in ('.implode(',',$options['id']).')' ;
      if ($options['id'] and !is_array($options['id'])) $_usl[]='pkey in ('.$options['id'].')' ;
      $usl=implode(' and ',$_usl) ;
      $sort_mode=$options['order']? $options['order']:'obj_name' ;
      $recs=select_db_recs($this->table_personal,$usl,array('order_by'=>$sort_mode,'count'=>$options['count'])) ;
      if (sizeof($recs)) foreach($recs as $id=>$rec) $this->prepare_public_info_to_personal($recs[$id]) ;
      return($recs) ;
    }


    function get_personal_by_id($id,$options=array())
    {   list($pkey,$tkey)=explode('.',$id) ;
        $rec=array() ;
        if ($pkey)
        { $rec=execSQL_van('select * from '.$this->view_personal.' where pkey="'.$pkey.'"') ;
          if ($rec['pkey'] and !$options['no_public_info']) $this->prepare_public_info_to_personal($rec,$options) ;
        }
        return($rec) ;
    }

    function set_personal_sign($personal_id, $sign_id)
    {   
        if ($personal_id > 0 && $sign_id > 0) {
            $res = update_rec_in_table($this->table_personal,array('sign_id'=>$sign_id),'pkey='.$personal_id) ;
        }
        
        return $res;
    }
    

    function get_personal_by_URL($URL)
    {  $arr=explode('.',$URL) ;
       $rec=$this->get_personal_by_id($arr[0])  ;
       return($rec) ;
    }

    // ищет сотрудника по пропуску, номеру ВУ или табельному номеру
    function get_personal($fields=array(),$options=array())
    {   $arr=array() ;
        if (sizeof($fields)) foreach($fields as $fname=>$fvalue)
        {   $text_en=ENGINE()->translit(str_replace(array("'",'"','(',')',' ',"\n"),'',$fvalue)) ;
            $text_ru=ENGINE()->translit2(str_replace(array("'",'"','(',')',' ',"\n"),'',$fvalue)) ;
            $arr[]='('.$fname.'="'.$text_en.'" or '.$fname.'="'.$text_ru.'")' ;
        }
        $usl_select='('.implode(' or ',$arr).')' ;
        if ($options['dop_usl']) $usl_select.=' and '.$options['dop_usl'] ;
        $rec=execSQL_van('select * from '.$this->view_personal.' where '.$usl_select) ;
        if ($rec['pkey'] and !$options['no_public_info']) $this->prepare_public_info_to_personal($rec,$options) ;
        if (!$rec['pkey']) $rec['_reason']='Сотрудник не найден (#203)' ;
        return($rec) ;
    }

      function get_personal2($fields=array(),$options=array())
    {   $arr=array() ; $_usl=array() ;
        $_usl[]='clss=211' ;
        $_usl[]='_enabled=1' ;
        if (sizeof($fields)) foreach($fields as $fname=>$fvalue) $arr[]=$fname.'="'.str_replace(array("'",'"','(',')',' ',"\n"),'',$fvalue).'"' ;
        $_usl[]='('.implode(' or ',$arr).')' ;
        if ($options['dop_usl']) $_usl[]=$options['dop_usl'] ;
        if ($options['only_enabled']) $_usl[]='enabled=1' ;
        $usl_select=implode(' and ',$_usl) ;
        $rec=select_db_recs($this->table_personal,$usl_select,array('no_arr'=>1,'debug'=>0)) ;
        if ($rec['pkey']) $this->prepare_public_info_to_personal($rec,$options) ;
        return($rec) ;
    }

    function get_personals($fields=array(),$options=array())
    { $arr=array() ;
      if (sizeof($fields)) foreach($fields as $fname=>$fvalue)
      {   $text_en=ENGINE()->translit(str_replace(array("'",'"','(',')',' ',"\n"),'',$fvalue)) ;
          $text_ru=ENGINE()->translit2(str_replace(array("'",'"','(',')',' ',"\n"),'',$fvalue)) ;
          $arr[]='('.$fname.'="'.$text_en.'" or '.$fname.'="'.$text_ru.'")' ;
      }
      $usl_select='('.implode(' or ',$arr).')' ;
      if ($options['dop_usl']) $usl_select.=' and '.$options['dop_usl'] ;
      $sort_mode=$options['order']? $options['order']:'obj_name' ;
      $recs=select_db_recs($this->table_personal,$usl_select,array('order_by'=>$sort_mode,'count'=>$options['count'])) ;
      if (sizeof($recs)) foreach($recs as $id=>$rec) $this->prepare_public_info_to_personal($recs[$id]) ;
      return($recs) ;
    }



    function check_propusk($id)
    {  $rec=execSQL_van('select pkey,obj_name,ext from '.$this->table_personal.' where propusk="'.$id.'"') ;
       return($rec) ;

    }

    function prepare_public_info(&$rec,$options=array())
    {
      $this->prepare_public_info_to_personal($rec,$options) ;
    }

    function prepare_public_info_to_personal(&$rec,$options=array())
    { // полный путь к фото
      $rec['_image_dir']=_DOT($this->table_personal.'_image')->dir_to_file.'source/'.$rec['_image_name'] ;
      $rec['_work_name']=$_SESSION['IL_works'][$rec['working']]['obj_name'] ;
      $rec['__href']='/cab/personal/person/'.$rec['pkey'].'/' ;
      if ($rec['ext_info']) { $rec['_ext_info']=unserialize($rec['ext_info']) ; unset($rec['ext_info']) ; }
      $rec['_pol']=($rec['pol'])? 'Жен.':'Муж.' ;
      if (!$options['no_get_all_img']) $rec['obj_clss_3']=execSQL('select * from '.$this->table_personal.'_image where parent='.$rec['pkey'].' order by indx') ;
    }

     function get_name_by_id($id,$options=array())
     {   $name=execSQL_value('select obj_name from '.$this->table_personal.' where pkey='.$id) ;
         return($name) ;
     }




}



?>