<?php
define('_SITE_CREATE_FULL_MODEL',1) ; // загружать для сайта полную модель классов,, опция обызательна для модуля account!!!

$__functions['init'][]		='_account_site_vars' ;
$__functions['boot_site'][]	='_account_site_boot' ;

function _account_site_vars()
{	$_SESSION['TM_users']='obj_'.SITE_CODE.'_users' ; $GLOBALS['TM_users']=$_SESSION['TM_users'] ;
    $_SESSION['TM_account']='obj_'.SITE_CODE.'_account' ; $GLOBALS['TM_account']=$_SESSION['TM_account'] ;

    $_SESSION['init_options']['account']['debug']=0 ;
    $_SESSION['init_options']['account']['use_capcha_code']=0 ; // требовать ввоб проверочного кода при авторизации
    $_SESSION['init_options']['account']['sequre_pass_mode']=0 ;            // хранить ли пароли в формате MD5
    $_SESSION['init_options']['account']['use_activation']=1 ;  // обязательная активация аккаунта после регистрации на сайте

    $_SESSION['ARR_ACCOUNT_STATUS']=array('1'=>'Клиент','2'=>'Фотограф','3'=>'Администратор') ;
}

function _account_site_boot($options)
{
	/* Заремировано 20.10.2010 - в связи с разделением систем account_system и member
	 * account_system - подсистема для работы с объектом $member
	 * $member - объект текущий пользователь, может быть только один на сайте
	 *
	 * if (!isset($account_system) or !$account_system->tkey) - перерь имеем право переиницилизировать систему аккаунтов
	 */

   create_system_modul_obj('account',$options) ; // создание объекта account_system

}

class c_account_system extends c_system
{
  var $use_capcha_code ;
  var $sequre_pass_mode ;
  var $use_activation ;


 // конструктор системы аккаунтов
 function c_account_system($options=array())
 {  if (!$options['table_name']) $options['table_name']=$_SESSION['TM_account'] ;
    $options['no_def_pages_info']=1 ;
    parent::c_system($options) ;

    $this->parent=1;
    $this->use_capcha_code=$options['use_capcha_code'] ;
    $this->sequre_pass_mode=$options['sequre_pass_mode'] ;
    $this->use_activation=$options['use_activation'] ;
    $this->on_after_create() ;
 }

 function prepare_public_info(&$rec,$options=array())
 {
   _CLSS($rec['clss'])->prepare_public_info($rec) ;
     
     
 }



 //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 //
 // функции обращения к БД
 //
 //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
 function get_account_rec_by_login_and_pass($login,$pass)
 {  //ob_start() ;
    $login=stripslashes(trim($login)) ;
    $pass=stripslashes(trim($pass)) ;
    $rec=execSQL_van('select * from '.$this->table_name.' where login="'.trim($login).'" and password="'.trim($pass).'" and enabled=1') ;
    //if (!$rec['pkey']) $rec=execSQL_van('select * from obj_site_personal where login="'.trim($login).'" and password="'.trim($pass).'" and enabled=1',2) ;
    if ($rec['pkey']) $this->prepare_public_info($rec) ;
    //$text=ob_get_clean() ;
    //_event_reg('get_account_rec_by_login_and_pass',$text) ;
    return($rec) ;
 }

 function get_account_rec_by_id($id,$options=array())
 {
   $rec=execSQL_van('select * from '.$this->table_name.' where pkey="'.$id.'"');
   if ($rec['pkey']) $this->prepare_public_info($rec) ;
   if ($options['no_login_pass']) unset($rec['login'],$rec['password']) ;
   return($rec) ;
 }

 function get_account_rec_by_reffer($reffer,$options=array())
 { list($pkey,$tkey)=explode('.',$reffer) ;
   $rec=execSQL_van('select * from '.$this->table_name.' where pkey="'.$pkey.'"');
   if ($rec['pkey']) $this->prepare_public_info($rec) ;
   if ($options['no_login_pass']) unset($rec['login'],$rec['password']) ;
   return($rec) ;
 }

 function get_account_rec_by_login($login,$options=array())
 {
   $rec=execSQL_van('select * from '.$this->table_name.' where login="'.$login.'"');
   if ($rec['pkey']) $this->prepare_public_info($rec) ;
   if ($options['no_login_pass']) unset($rec['login'],$rec['password']) ;
   return($rec) ;
 }

 function get_account_rec_by_act_code($code,$options=array())
 { $code=addslashes(trim($code)) ;
   $rec=execSQL_van('select * from '.$this->table_name.' where act_code="'.$code.'"');
   if ($rec['pkey']) $this->prepare_public_info($rec) ;
   if ($options['no_login_pass']) unset($rec['login'],$rec['password']) ;
   return($rec) ;
 }

 function get_account_rec_by_email($code,$options=array())
 { $code=addslashes(trim($code)) ;
   $rec=execSQL_van('select * from '.$this->table_name.' where email="'.$code.'"');
   if ($rec['pkey']) $this->prepare_public_info($rec) ;
   if ($options['no_login_pass']) unset($rec['login'],$rec['password']) ;
   return($rec) ;
 }

 function activate_account($id)
 {
   update_rec_in_table($this->tkey,array('enabled'=>1,'act_code'=>''),'pkey='.$id) ;
 }


 function account_append_file($account_id,$rec)
 { include_once(_DIR_TO_ENGINE.'/admin/i_admin_system.php') ;
   $obj_rec=_CLSS(5)->obj_create(array(),array('parent_reffer'=>$account_id.'.'.$this->tkey)) ;
   $result=obj_upload_file($obj_rec['_reffer'],$rec,array('view_upload'=>0,'debug'=>0,'no_comment'=>1)) ;
   return($result) ;
 }

 function update_member_info($member)
 { $rec=execSQL_van('select * from '.$this->table_name.' where pkey="'.$member->id.'"');
   $member->update_info($rec) ;
 }


 
  //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 //
 // АВТОРИЗАЦИЯ и ВЫХОД из аккаунта
 //
 //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 // функция входа теперь работает с объектом $member
 function login($login,$pass)
 { if (!$_SESSION['member']->id)
     if ($login and $pass)
     { if ($this->use_capcha_code) if (!capcha_code_checkit()) return(prepare_result('error_check_code')) ;
       $db_pass=($this->sequre_pass_mode)? $this->get_sequre_pass($pass):$pass ;
       $rec=$this->get_account_rec_by_login_and_pass($login,$db_pass) ;
       if ($rec['pkey']) $result=$this->autorize($rec) ;
       else              {  _event_reg('Ошибка авторизации','Логин:<strong>'.$login.'</strong><br>Пароль:<strong>'.$pass.'</strong><br>') ; // регистрируем событие 'Вход на сайт'
                            $result=prepare_result('member_not_found') ;
                         }
     }
     else $result=prepare_result('space_field') ;
   else $result=prepare_result('no_login_for_member') ;
   return($result) ;
 }

 // функция выхода теперь также работает с обхектом member
 function logout()
 {  unset($_SESSION['member']) ;
    $_SESSION['member']=new c_member() ; // создаем неавторизированного пользователя
    $this->on_logout() ;
    return(prepare_result('logout_success')) ;
 }

 // создание объекта member по записи $rec и вся необходимая обвязка
 function autorize($rec,$options=array())
 { $_SESSION['member']=new c_member($rec) ;  // создаем авторизированного пользователя
   $this->on_login() ; // отработка события - авторизация на сайте
   $event_name=($options['event_name'])? $options['event_name']:'Авторизация на сайте' ;
   _event_reg($event_name,$_SESSION['member']->name) ; // регистрируем событие 'Вход на сайт через пароль'
   return(prepare_result('login_success')) ;
     }

 //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 //
 // СОЗДАНИЕ и АКТИВАЦИЯ аккаунта по данным из формы
 //
 //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 // создаем новый аакаунт на основании данных формы
 // новый аккаунт создается в состоянии disabled
 // возвращаемые значения:
 // dubl_email - такое email уже есть в базе
 // массив $info - данные аккаунта для регистарации
 //    $info['email'] - email, единственное обязательное поле
 //    $info['login'] - логин, если не задан = email
 //    $info['password'] - пароль, если не задан, генериться автоматом
 //
 function create_account_by_form($info,$options=array())
 { //damp_array($info) ;
   if ($_SESSION['member']->id) return(prepare_result('no_create_for_member')) ;
   // если поле логин не заполнено, значит логин - email
   if (!$info['login']) $info['login']=$info['email'] ;
   // проверяем заполнение обязательных полей - логина и email
   if ($info['login']=='' or $info['email']=='') return(prepare_result('space_field')) ;

   // проверяем правильность проверочного кода
   if (!$options['no_capcha']) if (!capcha_code_checkit()) return(prepare_result('error_check_code')) ;

   // проверяем адрес почты, занят или нет
   $eml_info=execSQL_van('select * from '.$this->table_name.' where email="'.$info['email'].'"');
   if ($eml_info['pkey']) return(prepare_result('dubl_email')) ;
   // проверяем логин, занят или нет
   $eml_info=execSQL_van('select * from '.$this->table_name.' where login="'.$info['login'].'"');
   if ($eml_info['pkey']) return(prepare_result('dubl_login')) ;

   // если пароля еще не задано, сгенерить его самостоятель
   if (!$info['password'])  $info['password']=$this->generate_password() ;
   // если имя аккаунта не указан используем логин клиента
   if (!$info['name']) $info['name']=$info['login'] ;

   // генерируем код активации для клиента
   if ($this->use_activation) $info['act_code']=$this->get_activation_code() ;
   //damp_array($info) ;

   // регистрируем новый аккаунт в базе
   $result=$this->reg_in_db($info,$options) ;
   return($result) ;
 }


function create_account($info,$options=array())
 { // если поле логин не заполнено, значит логин - email
   if (!$info['login']) $info['login']=$info['email'] ;
   // проверяем заполнение обязательных полей - логина и email
   if ($info['login']=='' or $info['email']=='') return(prepare_result('space_field')) ;
   // проверяем адрес почты, занят или нет
   $eml_info=execSQL_van('select * from '.$this->table_name.' where email="'.$info['email'].'"');
   if ($eml_info['pkey']) return(prepare_result('dubl_email')) ;
   // проверяем логин, занят или нет
   $eml_info=execSQL_van('select * from '.$this->table_name.' where login="'.$info['login'].'"');
   if ($eml_info['pkey']) return(prepare_result('dubl_login')) ;
   // если пароля еще не задано, сгенерить его самостоятель
   if (!$info['password'])  $info['password']=$this->generate_password() ;
   // если имя аккаунта не указан используем логин клиента
   if (!$info['obj_name']) $info['obj_name']=$info['login'] ;
   // генерируем код активации для клиента
   if ($this->use_activation) $info['act_code']=$this->get_activation_code() ;
   // регистрируем новый аккаунт в базе
   $result=$this->reg_in_db($info,$options) ;
   return($result) ;
 }

 // при активации аккаунта делаем enabled=1 и act_code=""
 function activation_account_by_form($info)
 { if ($info['code']=='') return(prepare_result('space_field')) ;
   $account_info=$this->get_account_rec_by_act_code($info['code']);
   if ($account_info['pkey'])
   { update_rec_in_table($this->tkey,array('enabled'=>1,'act_code'=>''),'pkey='.$account_info['pkey']) ;
     $reffer=$account_info['pkey'].'.'.$this->tkey ;
     $evt_id=_event_reg('Активация аккаунта','',$reffer) ; // регистрируем событие
     $this->send_mail_activation_success($account_info,array('use_event_id'=>$evt_id)) ;
     //damp_array($member) ;
     $this->on_after_autorize() ;
     // регистрируем событие 'Вход на сайт через пароль'
     _event_reg('Авторизация на сайте') ;
     $result=prepare_result('activation_success') ;
     $result['rec']=$account_info ;
     return($result) ;
   } else return(prepare_result('activation_code_not_found')) ;
 }

 // создает в таблице клиентов новую запись, создает в журнале событий запись по входу клиента в систему
 // возврат:
 // space_field - не заполнены все обязательные поля
 // db_error - ошибка при добавлении записи в базу данных
 function reg_in_db($info,$options=array())
 { $new_rec=array() ;
   //echo 'Сохраняем информацию в БД<br>' ; damp_array($info) ;

   if (!$info['parent'])    $info['parent']=1 ;
   if (!$info['clss'])      $info['clss']=86 ;
   if (!$info['obj_name'])  { $info['obj_name']=$info['name'] ; unset($info['name']) ;  }
   $info['enabled']         =($info['act_code'])? 0:1;

   if (!$info['login'] or !$info['obj_name'] or !$info['email']) return(prepare_result('space_field')) ;
   // сохранем данные в таблице. Сериализуемые данные будут обработы автоматически.
   $id=adding_rec_to_table($this->tkey,$info) ;
   if (!$id) return(prepare_result('db_error')) ;
   // получаем запись из таблиц
   $account_rec=$this->get_account_rec_by_id($id) ;
   $options['use_event_id']=_event_reg('Регистрация аккаунта','',$account_rec['_reffer']) ; // регистрируем событие 'Вход на сайт'
   if ($info['act_code']) $this->send_mail_activation_code($account_rec,$options) ;
   else                   $this->send_mail_account_create_info($account_rec,$options) ;

   $result=prepare_result('create_success') ;
   $result['rec']=$account_rec ;

   $this->on_create_account($account_rec) ;
   return($result) ;
 }


function forgot_pass($email)
 { // проверяем заполнение обязательных полей
   if (!$email) return(prepare_result('space_field')) ;

   // проверяем правильность проверочного кода
   if (!capcha_code_checkit()) return(prepare_result('error_check_code')) ;

   // получем информацию по аккаунту
   $account_info=$this->get_account_rec_by_email($email);

   if (!$account_info['pkey']) return(prepare_result('member_not_found')) ;

   //$account_info['password']=rand(111111,999999) ;
   //update_rec_in_table($this->tkey,array('password'=>$account_info['password']),'pkey='.$account_info['pkey']) ;

   // регистрируем событие
   $evt_id=_event_reg('Восстановление пароля','',$account_info['_reffer']) ;
   // отправляем почтой пароль
   $this->send_main_forgot_password($account_info,array('use_event_id'=>$evt_id)) ;

   return(prepare_result('forgot_success')) ; // return(array('forgot_success',$account_info)) ;
 }

// ================================================================================================================================================================================================

// измерение свойства аккаунта в БД
// member - или текущий объект или код аккаунта (id или reffer)
// fname - имя поля
// new_value - новое значение поля
// alt_value - старое значение поля, если не указать и member is object то alt_value будет получено из него, иначе через запрос в базу
// title - назначение поля, если не указать , будет использовано fname

// функция возвращает результат в виде массива type-text-code

function change_member_params($member_id,$fname,$new_value,$alt_value="...",$title="...")
{  if (is_object($member_id)) $member_id=$member_id->id ;  // если вместо $member_id  передан объект member
   list($member_id,$tkey)=explode('.',$member_id) ; // преобразования reffer в id

   if ($fname=='login')     return($this->change_login($member_id,$new_value,$alt_value)) ;
   if ($fname=='password')  return($this->change_pass($member_id,$new_value)) ;
   if ($fname=='obj_name')  return($this->change_name($member_id,$new_value,$alt_value)) ;
   if ($fname=='email')     return($this->change_email($member_id,$new_value,$alt_value)) ;

   if ($title=='...')       $title=$fname ;
   if ($alt_value=='...')   $alt_value=execSQL_value('select '.$fname.' from '.$this->table_name.' where pkey='.$member_id) ;

   //echo 'id='.$id.'<br>fname='.$fname.'<br>new_value='.$new_value.'<br>' ;
   // сохраняем в базе
   update_rec_in_table($this->tkey,array($fname=>$new_value),'pkey='.$member_id) ;
   // регистрируем событие
   $account_reffer=$member_id.'.'.$this->tkey ;
   _event_reg('Измененение данных аккаунта',$title.': '.$alt_value.' => '.$new_value,$account_reffer) ;
   // если иизменяем данные текущего аккаунта, сохраняем данные по изменения в самом аккааунте
   if ($_SESSION['member']->id==$member_id) $_SESSION['member']->info[$fname]=$new_value ;
   //
   return(array('type'=>'success','text'=>$title.' успешно изменен(а)','code'=>$fname.'_change_success')) ;
}


function change_login($member_id,$new_value,$alt_value='...')
{ if (is_object($member_id)) $member_id=$member_id->id ;  // если вместо $member_id  передан объект member
  list($member_id,$tkey)=explode('.',$member_id) ; // преобразования reffer в id
  if ($alt_value=='...')   $alt_value=execSQL_value('select login from '.$this->table_name.' where pkey='.$member_id) ;
  // проверяем, изменилось ли значение поля
  if (check_nochange_form_value($new_value,$alt_value)) return(array('type'=>'success','text'=>'Значение не изменилось','code'=>'nochange')) ;
  // проверяем заполнение обязательных полей
  if ($new_value=='')                               return(array('type'=>'error','text'=>'Не указан логин','code'=>'space_field')) ;
  // проверка прав доступа
  if ($res=$this->check_access($member_id)!='ok')   return(array('type'=>'error','text'=>'Доступ запрещен','code'=>$res)) ;
  // проверяем логин, занят или нет
  if ($this->check_login($new_value)!='free')    return(array('type'=>'error','code'=>'login_is_busy','rec'=>array('login'=>$new_value))) ;  ;
  // сохраняем в базе
  update_rec_in_table($this->tkey,array('login'=>$new_value),'pkey='.$member_id) ;
  // регистрируем событие
  $account_reffer=$member_id.'.'.$this->tkey ;
  $evt_id=_event_reg('Измененение данных аккаунта','Логин: '.$alt_value.' => '.$new_value,$account_reffer) ;
  // сохраняем данные в аккаунте
  if ($_SESSION['member']->id==$member_id) $this->update_member_info($_SESSION['member']) ;
  // отправляем почтовое уведомление
  $this->send_mail_change_login($member_id,array('use_event_id'=>$evt_id)) ;
  // возвращаем событие успешного обловления
  return(array('type'=>'success','text'=>'Логин успешно изменен','code'=>'login_change_success')) ;  ;
}

function change_name($member_id,$new_value,$alt_value='...')
{ if (is_object($member_id)) $member_id=$member_id->id ;  // если вместо $member_id  передан объект member
  list($member_id,$tkey)=explode('.',$member_id) ; // преобразования reffer в id
  if ($alt_value=='...')   $alt_value=execSQL_value('select obj_name from '.$this->table_name.' where pkey='.$member_id) ;
  // проверяем, изменилось ли значение поля
  if (check_nochange_form_value($new_value,$alt_value)) return(array('type'=>'success','text'=>'Значение не изменилось','code'=>'nochange')) ;
  // проверяем заполнение обязательных полей
  if ($new_value=='')                               return(array('type'=>'error','text'=>'Не указано имя','code'=>'space_field')) ;
  // проверка прав доступа
  if ($res=$this->check_access($member_id)!='ok')   return(array('type'=>'error','text'=>'Доступ запрещен','code'=>$res)) ;
  // сохраняем в базе
  update_rec_in_table($this->tkey,array('obj_name'=>$new_value),'pkey='.$member_id) ;
  // регистрируем событие
  $account_reffer=$member_id.'.'.$this->tkey ;
  $evt_id=_event_reg('Измененение данных аккаунта','Имя: '.$alt_value.' => '.$new_value,$account_reffer) ;
  // сохраняем данные в аккаунте
    if ($_SESSION['member']->id==$member_id) $this->update_member_info($_SESSION['member']) ;
  // отправляем почтовое уведомление
  $this->send_mail_change_name($member_id,array('use_event_id'=>$evt_id)) ;
  // возвращаем событие успешного обловления
  return(array('type'=>'success','text'=>'Имя успешно изменено','code'=>'name_change_success')) ;
}

function change_email($member_id,$new_value,$alt_value='...')
{ if (is_object($member_id)) $member_id=$member_id->id ;  // если вместо $member_id  передан объект member
  list($member_id,$tkey)=explode('.',$member_id) ; // преобразования reffer в id
  if ($alt_value=='...')   $alt_value=execSQL_value('select email from '.$this->table_name.' where pkey='.$member_id) ;
  // проверяем, изменилось ли значение поля
  if (check_nochange_form_value($new_value,$alt_value))  return(array('type'=>'success','text'=>'Значение не изменилось','code'=>'nochange')) ;
  // проверяем заполнение обязательных полей
  if ($new_value=='')                               return(array('type'=>'error','text'=>'Не указан email','code'=>'space_field')) ;
  // проверка прав доступа
  if ($res=$this->check_access($member_id)!='ok')   return(array('type'=>'error','text'=>'Доступ запрещен','code'=>'access_denited')) ;
  // проверяем логин, занят или нет
  if ($res=$this->check_email($new_value)!='free')  return(array('type'=>'error','code'=>'email_is_busy','rec'=>array('email'=>$new_value))) ;  ;
  // сохраняем в базе
  update_rec_in_table($this->tkey,array('email'=>$new_value),'pkey='.$member_id) ;
  // регистрируем событие
  $account_reffer=$member_id.'.'.$this->tkey ;
  $evt_id=_event_reg('Измененение данных аккаунта','Email: '.$alt_value.' => '.$new_value,$account_reffer) ;
  // сохраняем данные в аккаунте
  if ($_SESSION['member']->id==$member_id) $this->update_member_info($_SESSION['member']) ;
  // отправляем почтовое уведомление
  $this->send_mail_change_email($member_id,$alt_value,array('use_event_id'=>$evt_id)) ;
  // возвращаем событие успешного обловления
  return(array('type'=>'success','text'=>'Email успешно изменен','code'=>'email_change_success')) ;
}

function change_pass($member_id,$new_value)
{ if (is_object($member_id)) $member_id=$member_id->id ;  // если вместо $member_id  передан объект member
  list($member_id,$tkey)=explode('.',$member_id) ; // преобразования reffer в id
  $alt_value=execSQL_value('select password from '.$this->table_name.' where pkey='.$member_id) ;
  // проверяем, изменилось ли значение поля
  if (check_nochange_form_value($new_value,$alt_value))  return(array('type'=>'success','text'=>'Значение не изменилось','code'=>'nochange')) ;
  // проверяем заполнение обязательных полей
  if ($new_value=='')                               return(array('type'=>'error','code'=>'space_field')) ;
  // проверка прав доступа текущего аккаунта к аккаунту $member
  if ($res=$this->check_access($member_id)!='ok')  return(array('type'=>'error','code'=>'access_denited')) ;
  // сохраняем в базе
  update_rec_in_table($this->tkey,array('password'=>$new_value),'pkey='.$member_id) ;
  // регистрируем событие
  $account_reffer=$member_id.'.'.$this->tkey ;
  $evt_id=_event_reg('Измененение данных аккаунта','Пароль: '.$new_value,$account_reffer) ;
  // сохраняем данные в аккаунте
  if ($_SESSION['member']->id==$member_id) $this->update_member_info($_SESSION['member']) ;
  // отправляем почтовое уведомление
  $this->send_mail_change_password($member_id,array('use_event_id'=>$evt_id)) ;
  // возвращаем событие успешного обловления
  return(array('type'=>'success','code'=>'pass_change_success')) ;
}

// сохраняем доп.информациб по пользователю
// особенность работы с serialize
// перед сохранением массива в serialize необходимо проверить что поля из формы не обработаны слешами при включенной директиве magic_quotes_gpc
// если директива включена - удаляем слеши из значения посредством stripslashes
// также при сохранении серилизованной строки в БД через update_rec_in_table необходимо передать опцию no_stripslashes=1
// чтобы в случае magic_quotes_gpc=1 из сохраняемой строки не были удалены слешы перед применением к строке mysql_real_escape_string

function change_member_dop_info($member_id,$fname,$new_value,$alt_value="...",$title="...") { $this->change_member_ext_info($member_id,$fname,$new_value,$alt_value,$title);}
function change_member_ext_info($member_id,$fname,$new_value,$alt_value="...",$title="...")
{ if (is_object($member_id)) $member_id=$member_id->id ;  // если вместо $member_id  передан объект member
  list($member_id,$tkey)=explode('.',$member_id) ; // преобразования reffer в id
  // получаем сохраненное значение
  $value=execSQL_value('select ext_info from '.$this->table_name.' where pkey='.$member_id) ;
  if ($value) $arr=unserialize($value) ; else $arr=array() ;
  if ($title=='...')       $title=$fname ;
  if ($alt_value=='...')   $alt_value=$arr[$fname];
  // проверяем, изменилось ли значение поля
  if (check_nochange_form_value($new_value,$alt_value))   return(array('type'=>'success','text'=>'Значение не изменилось','code'=>'nochange')) ;
  // очищаем значение от слешей, если добавлены автоматом - в массив должны попасть значения как ввел пользователь
  if (get_magic_quotes_gpc()) $new_value=stripslashes($new_value) ;
  // проверка прав доступа
  if ($this->check_access($member_id)!='ok')    return(array('type'=>'error','code'=>'access_denited')) ;

  $arr[$fname]=$new_value ; $value=serialize($arr) ; //echo 'serialize value='.$value.'<br>' ;
  // сохраняем в базе
  update_rec_in_table($this->tkey,array('ext_info'=>$value),'pkey='.$member_id,array('no_stripslashes'=>1)) ;
  // регистрируем событие
  $account_reffer=$member_id.'.'.$this->tkey ;
  _event_reg('Измененение данных аккаунта',$title.': '.$alt_value.' => '.$new_value,$account_reffer) ;
  // сохраняем данные в аккаунте
  if ($_SESSION['member']->id==$member_id) $this->update_member_info($_SESSION['member']) ;
  return(array('type'=>'success','code'=>'account_info_change_success')) ;
}

function change_member_info($member_id,$fname,$value='')
{  if (!is_array($fname)) $rec[$fname]=$value ;
   else                   $rec=$fname ;
   update_rec_in_table($this->tkey,$rec,'pkey='.$member_id) ;
}



// ================================================================================================================================================================================================



 // функции - обработчики событий
 function on_after_autorize() {} //
 function on_create_account($new_account=array()) 	{return(0);} //
 function on_before_login() 	{} //
 function on_after_login() 	{} //
 function on_after_logout() 	{} //

 // отправка - письма - на восстановление пароля по логину. Письмо уходит на почту логина
 function send_main_forgot_password($info,$options=array())
 { $params=array() ;
   $params['site_name']=_MAIN_DOMAIN ;
   $params['data']=date("d.m.y G:i") ;
   $params['name']=$info['obj_name'] ;
   $params['login']=$info['login'] ;
   $params['password']=$info['password'] ;
   //damp_array($params) ;
   //damp_array($info) ;
   _send_mail_to_pattern($info['email'],'account_system_forgot_password',$params,$options) ;
 }

// отправка - письма - на изменение пароля для текущего пользователя. Письмо уходит на почту логина
function send_mail_change_password($id,$options=array())
{ $info=$this->get_account_info($id) ;
  $params=array() ;
  $params['data']=date("d.m.y G:i") ;
  $params['name']=$info['obj_name'] ;
  $params['login']=$info['login'] ;
  $params['password']=$info['password'] ;

  _send_mail_to_pattern($info['email'],'account_system_change_password',$params,$options) ;
}

// отправка - письма - на изменение пароля для текущего пользователя. Письмо уходит на почту логина
function send_mail_change_login($id,$options=array())
{ $info=$this->get_account_info($id) ;
  $params=array() ;
  $params['data']=date("d.m.y G:i") ;
  $params['name']=$info['obj_name'] ;
  $params['login']=$info['login'] ;
  $params['password']=$info['password'] ;

  _send_mail_to_pattern($info['email'],'account_system_change_login',$params,$options) ;
}

// отправка - письма - на изменение имени для текущего пользователя. Письмо уходит на почту логина
function send_mail_change_name($id,$options=array())
{ $info=$this->get_account_info($id) ;
  $params=array() ;
  $params['data']=date("d.m.y G:i") ;
  $params['name']=$info['obj_name'] ;

  _send_mail_to_pattern($info['email'],'account_system_change_name',$params,$options) ;
}

// отправка - письма - на изменение email для текущего пользователя. Письмо уходит на почту логина
function send_mail_change_email($id,$alt_email,$options=array())
{ $info=$this->get_account_info($id) ;
  $params=array() ;
  $params['data']=date("d.m.y G:i") ;
  $params['name']=$info['obj_name'] ;
  $params['alt_email']=($alt_email)? $alt_email:'не указан' ;
  $params['new_email']=($info['email'])? $info['email']:'не указан' ;

  _send_mail_to_pattern($alt_email,'account_system_change_email',$params,$options) ;
  _send_mail_to_pattern($info['email'],'account_system_change_email',$params,$options) ;
}

// письмо о создании аккаунта через активацию на сайте
 function send_mail_activation_code($info,$options=array())
 { $params=array() ;
   $params['data']=date("d.m.y G:i") ;
   $params['name']=$info['obj_name'] ;
   $params['login']=$info['login'] ;
   $params['email']=$info['email'] ;
   $params['password']=$info['password'] ;
   $params['act_code']=$info['act_code'] ;
   $params['email_site']=$_SESSION['email_site'] ;
   $params['act_href']='<a href="'._PATH_TO_SITE.'/account/activation/'.$info['act_code'].'">'._PATH_TO_SITE.'/account/activation/'.$info['act_code'].'</a>' ;
   $params['act_page']='<a href="'._PATH_TO_SITE.'/account/activation/">'._PATH_TO_SITE.'/account/activation/</a>' ;
   $params['account_info']=$info['__info'] ;
   //если в проект регистрация аккаунта с разрешения администрации сайта, то уведомление отправляем на указанный в email_for_activation_alert email
   if ($options['email_for_activation_alert'])  _send_mail_to_pattern($options['email_for_activation_alert'],'account_system_activation_code',$params,$options) ;
   else
   { // отправляем уведомление клиенту
     _send_mail_to_pattern($info['email'],'account_system_activation_code',$params,$options) ;
     // отправляем уведомление клиенту
   _send_mail_to_pattern($_SESSION['LS_account_system_email_alert'],'account_system_create_alert',$params,$options) ;
  }
  }

// письмо о создании аккаунта без активации на сайте - письмо уходит только клиенту, так как создавать такие аккаунты модет только менежер
function send_mail_account_create_info($info,$options=array())
 { global $mail_system,$email_site ;
   $params=$options['mail_params'] ;
   $params['data']=date("d.m.y G:i") ;
   $params['name']=$info['obj_name'] ;
   $params['login']=$info['login'] ;
   $params['email']=$info['email'] ;
   $params['password']=$info['password'] ;
   $params['email_site']=$email_site ;
   $params['account_info']=$info['__info'] ;

   //damp_array($params) ;
   $mail_system->send_mail_to_pattern($info['email'],'account_system_create_account',$params,$options) ;

   _send_mail_to_pattern($_SESSION['LS_account_system_email_alert'],'account_system_create_alert',$params,$options) ;
   // временнно
   /*
   $css='<style type="text/css">'.file_get_contents('http://'._MAIN_DOMAIN.'/style_email.css').'</style>' ;
   list($email_theme,$email_text)=$mail_system->get_mail_to_pattern('account_system_create_account',$params) ;
   $email_text=$css.$email_text ;    // добавлям css-стили в письмо
   $email_text=$mail_system->replace_pattern_array_values($email_text,$params) ;
   // отправляем только уведомление клиенту, менеджеру ничего не отправляем
   $mail_system->send_mail_to_pattern($info['email'],$email_theme,$email_text,$options) ;
   */
  }

  // сообщение менеджерам о активации аккаунта
  function send_mail_activation_success($info,$options=array())
  { $params=array() ;
        $params['name']=$info['obj_name'] ;
        $params['login']=$info['login'] ;
        // сообщение о успешной активации уходит клиенту
    _send_mail_to_pattern($info['email'],'account_system_activate_alert_user',$params,$options) ;
        $options['use_event_id']=0 ;
        // и на системный почтовый ящик
    _send_mail_to_pattern($_SESSION['LS_account_system_email_alert'],'account_system_activate_alert',$params,$options) ;
       }


// ================================================================================================================================================================================================
// получение информации по аккаунтам
// ================================================================================================================================================================================================

// получение информации по аккаунту
 // reffer - код аккаунта в виде ссылки или id
 // возвращает массив полей аккаунта, поле info - разобранное в массив
 function get_account_info($reffer)
 { list($id,$tkey)=explode('.',$reffer) ;
   if ($tkey and $tkey!=$this->tkey) return ;
   $account_info=execSQL_van('select * from '.$this->table_name.' where pkey="'.$id.'"') ;
   if ($account_info['pkey']) $this->prepare_public_info($account_info) ;
   return($account_info) ;
 }

 function get_account_info_by_email($email)
 { $account_info=execSQL_van('select * from '.$this->table_name.' where email="'.stripcslashes($email).'"') ;
   if ($account_info['pkey']) $this->prepare_public_info($account_info) ;
   return($account_info) ;
 }

 function get_subscript_emails()
 {  $emails=execSQL_line('select email from '.$this->table_name.' where subscript=1 and email!=""') ;
    return($emails)  ;
 }

 function get_subscript_count()
 {  $cnt=execSQL_value('select count(pkey) as cnt from '.$this->table_name.' where subscript=1 and email!=""') ;
    return($cnt)  ;
 }

 function unsubscribed($hash)
 { $emails=$this->get_subscript_emails() ;
   if (sizeof($emails)) foreach($emails as $email) if (md5($email)==$hash)
   { update_rec_in_table($this->table_name,array('subscript'=>0),'email="'.$email.'"') ;
     _event_reg('Отказ от подписки',$email) ;
   }
 }

 function generate_password()
 {
    return(rand(111111,999999)) ;
 }

 function get_sequre_pass($pass)
 {
   return(md5($pass)) ;
 }

 function get_activation_code()
 {
   return(md5(uniqid(rand(),true))) ;
 }


 function check_login($value)
 { $pkey=execSQL_value('select pkey from '.$this->table_name.' where login="'.trim($value).'"');
   return ($pkey)? $pkey:'free' ; // free=>ok
 }

function check_email($value)
 { $pkey=execSQL_value('select pkey from '.$this->table_name.' where email="'.trim($value).'"');
   return ($pkey)? $pkey:'free' ;
 }

// проверка прав доступа текущего аккаунта к аккаунту mem2
function check_access($member_id)
{ // по умолчанию, текущий пользователь имеет доступ только к своему аккаунту
  // сделать потом проверку, раздешать доступ к аккаунту, только есть это свой аккаунт или аккаунт является менеджером этого аккаунта
  if (_ENGINE_MODE=='admin') return('ok') ;
  if ($_SESSION['member']->id==$member_id) return('ok') ;
  else                                     return('access_denited') ;
}

 function on_after_create() {} //
 function on_create($new_account=array()) 	{return(0);} //
 function on_login() 	{} //
 function on_logout() 	{} //


}




?>