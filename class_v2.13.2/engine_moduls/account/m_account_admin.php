<?php
$__functions['init'][]		='_account_admin_vars' ;
$__functions['install'][]	='_account_install_modul' ;
$__functions['boot_admin'][]='_account_admin_boot' ;

function _account_admin_vars() //
{   
	//-----------------------------------------------------------------------------------------------------------------------------
	// описание переменных модуля
	//-----------------------------------------------------------------------------------------------------------------------------

	//-----------------------------------------------------------------------------------------------------------------------------
	// описание классов модуля
	//-----------------------------------------------------------------------------------------------------------------------------

    // запись в таблице аккаунтов
	$_SESSION['descr_clss'][86]['name']='Аккаунт' ;
	$_SESSION['descr_clss'][86]['parent']=0 ;
	$_SESSION['descr_clss'][86]['fields']=array('login'=>'varchar(255)','password'=>'varchar(255)','email'=>'varchar(255)','act_code'=>'varchar(255)','reffer'=>'any_object','info'=>'serialize','status'=>'int(11)') ;
    $_SESSION['descr_clss'][86]['fields']['ext_info']='serialize';
    $_SESSION['descr_clss'][86]['fields']['status']=array('type'=>'indx_select','array'=>'ARR_ACCOUNT_STATUS') ;
	$_SESSION['descr_clss'][86]['on_change_event']['password']='on_change_event_clss_86_password' ;
    $_SESSION['descr_clss'][86]['child_to']=array(1) ;
    $_SESSION['descr_clss'][86]['parent_to']=array() ;
    $_SESSION['descr_clss'][86]['no_show_in_tree']=1 ; // не показывать объекты класса в дереве

    $_SESSION['descr_clss'][86]['menu'][]=array("name" => "Свойства",				"type" => 'prop') ;
    $_SESSION['descr_clss'][86]['menu'][]=array("name" => "История",				 "cmd" => 'panel_account_history') ;


    $_SESSION['descr_clss'][86]['icons']=_PATH_TO_BASED_CLSS_IMG.'/98.png' ;

	$_SESSION['descr_clss'][86]['list']['field']['enabled']				=array('title'=>'Сост.') ;
	$_SESSION['descr_clss'][86]['list']['field']['obj_name']			=array('title'=>'Имя','td_class'=>'left') ;
	$_SESSION['descr_clss'][86]['list']['field']['login']				=array('title'=>'Логин') ;
	$_SESSION['descr_clss'][86]['list']['field']['password']			=array('title'=>'Пароль') ;
	$_SESSION['descr_clss'][86]['list']['field']['email']				=array('title'=>'email') ;
	$_SESSION['descr_clss'][86]['list']['field']['act_code']			=array('title'=>'Код активизации','td_class'=>'left') ;

    $_SESSION['descr_clss'][86]['details']=$_SESSION['descr_clss'][86]['list'] ;
    $_SESSION['descr_clss'][86]['details']['field']['info']				=array('title'=>'Дополнительная информация') ;

	//Клиент
	$_SESSION['descr_clss'][89]['name']='Клиент' ;
	$_SESSION['descr_clss'][89]['parent']=86 ;
	$_SESSION['descr_clss'][89]['fields']=array('phone'=>'varchar(255)','subscript'=>'int(1)','account_reffer'=>'any_object','sales_value'=>'int(11)','city'=>'varchar(32)') ;

    $_SESSION['descr_clss'][89]['icons']=_PATH_TO_BASED_CLSS_IMG.'/98.png' ;

	//Частное лицо
	$_SESSION['descr_clss'][87]['name']='Частное лицо' ;
	$_SESSION['descr_clss'][87]['parent']=89 ;
	$_SESSION['descr_clss'][87]['fields']=array('adres'=>'text','id_discount_card'=>'text') ;

    $_SESSION['descr_clss'][87]['menu'][]=array("name" => "Свойства",				"type" => 'prop') ;
    //$_SESSION['descr_clss'][87]['menu'][]=array("name" => "Заказы",				    "cmd" => 'account_admin/panel_account_orders') ;
    //$_SESSION['descr_clss'][87]['menu'][]=array("name" => "Тикеты",				    "cmd" => 'account_admin/panel_account_support') ;
    //$_SESSION['descr_clss'][87]['menu'][]=array("name" => "История",				 "cmd" => 'account_admin/panel_account_history') ;
    $_SESSION['descr_clss'][87]['menu'][]=array("name" => "Заказы",				    "cmd" => 'panel_account_orders') ;
    $_SESSION['descr_clss'][87]['menu'][]=array("name" => "Тикеты",				    "cmd" => 'panel_account_support') ;
    $_SESSION['descr_clss'][87]['menu'][]=array("name" => "История",				 "cmd" => 'panel_account_history') ;


    $_SESSION['descr_clss'][87]['icons']=_PATH_TO_BASED_CLSS_IMG.'/98.png' ;

	$_SESSION['descr_clss'][87]['list']['field']['enabled']				=array('title'=>'Сост.') ;
	$_SESSION['descr_clss'][87]['list']['field']['obj_name']			=array('title'=>'Фамилия Имя','td_class'=>'left') ;
    $_SESSION['descr_clss'][87]['list']['field']['login']		    	=array('title'=>'Логин') ;
	$_SESSION['descr_clss'][87]['list']['field']['sales_value']		    =array('title'=>'Скидка','td_class'=>'left') ;
	$_SESSION['descr_clss'][87]['list']['field']['subscript']			=array('title'=>'Подписка') ;
	$_SESSION['descr_clss'][87]['list']['field']['email']				=array('title'=>'email','td_class'=>'left') ;
	$_SESSION['descr_clss'][87]['list']['field']['phone']				=array('title'=>'Тел.','td_class'=>'left') ;
	$_SESSION['descr_clss'][87]['list']['field']['adres']				=array('title'=>'Адрес<br>доставки','td_class'=>'left') ;
	$_SESSION['descr_clss'][87]['list']['field']['id_discount_card']	=array('title'=>'Номер дисконтной карты','edit_element'=>'input','td_class'=>'left') ;
	$_SESSION['descr_clss'][87]['list']['field']['status']	            =array('title'=>'Роль') ;
	//$_SESSION['descr_clss'][87]['list']['field']['account_reffer']		=array('title'=>'Аккаунт') ; // менеджер


	$_SESSION['descr_clss'][87]['details']['field']['enabled']			=array('title'=>'Сост.') ;
	$_SESSION['descr_clss'][87]['details']['field']['obj_name']			=array('title'=>'Фамилия Имя','class'=>'small') ;
	$_SESSION['descr_clss'][87]['details']['field']['sales_value']		=array('title'=>'Скидка') ;
	$_SESSION['descr_clss'][87]['details']['field']['subscript']		=array('title'=>'Подписка') ;
	$_SESSION['descr_clss'][87]['details']['field']['id_discount_card'] =array('title'=>'Номер дисконтной карты','edit_element'=>'input') ;

	$_SESSION['descr_clss'][87]['details']['field']['__group_title1']	=array('title'=>'Данные для авторизации') ;
    $_SESSION['descr_clss'][87]['details']['field']['login']			=array('title'=>'Логин') ;
    $_SESSION['descr_clss'][87]['details']['field']['password']			=array('title'=>'Пароль') ;


	$_SESSION['descr_clss'][87]['details']['field']['__group_title2']	=array('title'=>'Контактные данные') ;
	$_SESSION['descr_clss'][87]['details']['field']['email']			=array('title'=>'e-mail') ;
	$_SESSION['descr_clss'][87]['details']['field']['phone']			=array('title'=>'Тел.') ;
	$_SESSION['descr_clss'][87]['details']['field']['adres']			=array('title'=>'Адрес<br>доставки','td_class'=>'left') ;

    $_SESSION['descr_clss'][87]['details']['field']['__group_title2']	=array('title'=>'Дополнительная информация') ;
    $_SESSION['descr_clss'][87]['details']['field']['info']				=array('title'=>'Дополнительная информация') ;

	//Юр.лицо
	$_SESSION['descr_clss'][88]['name']='Организация' ;
	$_SESSION['descr_clss'][88]['parent']=89 ;
	$_SESSION['descr_clss'][88]['fields']=array('contact_name'=>'varchar(255)','adres1'=>'text','adres2'=>'text','rekvezit'=>'text','inn'=>'varchar(255)','kpp'=>'varchar(255)','rs'=>'varchar(255)','ks'=>'varchar(255)','bik'=>'varchar(255)','fax'=>'varchar(255)','site'=>'varchar(255)') ;

    $_SESSION['descr_clss'][88]['menu'][]=array("name" => "Свойства",				"type" => 'prop') ;
    //$_SESSION['descr_clss'][88]['menu'][]=array("name" => "Заказы",				    "cmd" => 'account_admin/panel_account_orders') ;
    //$_SESSION['descr_clss'][88]['menu'][]=array("name" => "Тикеты",				    "cmd" => 'account_admin/panel_account_support') ;
    //$_SESSION['descr_clss'][88]['menu'][]=array("name" => "История",				 "cmd" => 'account_admin/panel_account_history') ;
    $_SESSION['descr_clss'][88]['menu'][]=array("name" => "Заказы",				    "cmd" => 'panel_account_orders') ;
    $_SESSION['descr_clss'][88]['menu'][]=array("name" => "Тикеты",				    "cmd" => 'panel_account_support') ;
    $_SESSION['descr_clss'][88]['menu'][]=array("name" => "История",				 "cmd" => 'panel_account_history') ;


    $_SESSION['descr_clss'][88]['icons']=_PATH_TO_BASED_CLSS_IMG.'/98.png' ;

	$_SESSION['descr_clss'][88]['list']['field']['enabled']					=array('title'=>'Сост.') ;
	$_SESSION['descr_clss'][88]['list']['field']['obj_name']				=array('title'=>'Организация','td_class'=>'left') ;
	$_SESSION['descr_clss'][88]['list']['field']['contact_name']			=array('title'=>'Контактное лицо','td_class'=>'left') ;
    $_SESSION['descr_clss'][88]['list']['field']['login']			=array('title'=>'Логин') ;
	$_SESSION['descr_clss'][88]['list']['field']['sales_value']				=array('title'=>'Скидка') ;
	$_SESSION['descr_clss'][88]['list']['field']['subscript']				=array('title'=>'Подписка') ;
	$_SESSION['descr_clss'][88]['list']['field']['email']					=array('title'=>'email','td_class'=>'left','read_only'=>1) ;
	$_SESSION['descr_clss'][88]['list']['field']['phone']					=array('title'=>'Тел.','td_class'=>'left') ;

	$_SESSION['descr_clss'][88]['details']['field']['enabled']				=array('title'=>'Сост.') ;
	$_SESSION['descr_clss'][88]['details']['field']['obj_name']				=array('title'=>'Организация','class'=>'small') ;
	$_SESSION['descr_clss'][88]['details']['field']['sales_value']			=array('title'=>'Скидка') ;
	$_SESSION['descr_clss'][88]['details']['field']['subscript']			=array('title'=>'Подписка') ;

	$_SESSION['descr_clss'][88]['details']['field']['__group_title1']		=array('title'=>'Данные для авторизации') ;
	$_SESSION['descr_clss'][88]['details']['field']['login']				=array('title'=>'Логин','read_only'=>1) ;
   	$_SESSION['descr_clss'][88]['details']['field']['password']			    =array('title'=>'Пароль','read_only'=>1) ;

	$_SESSION['descr_clss'][88]['details']['field']['__group_title2']		=array('title'=>'Контактные данные') ;
    $_SESSION['descr_clss'][88]['details']['field']['contact_name']		    =array('title'=>'Контактное лицо','class'=>'left') ;
	$_SESSION['descr_clss'][88]['details']['field']['email']				=array('title'=>'email','read_only'=>1) ;
	$_SESSION['descr_clss'][88]['details']['field']['phone']				=array('title'=>'Тел.') ;

	$_SESSION['descr_clss'][88]['details']['field']['__group_title3']		=array('title'=>'Данные по организации') ;
	$_SESSION['descr_clss'][88]['details']['field']['adres1']				=array('title'=>'Юр.адрес','class'=>'big') ;
	$_SESSION['descr_clss'][88]['details']['field']['adres2']				=array('title'=>'Факт.адрес','class'=>'big') ;
	$_SESSION['descr_clss'][88]['details']['field']['rekvezit']				=array('title'=>'Реквизиты','class'=>'big') ;

    $_SESSION['descr_clss'][88]['details']['field']['__group_title2']	=array('title'=>'Дополнительная информация') ;
    $_SESSION['descr_clss'][88]['details']['field']['ext_info']				=array('title'=>'Дополнительная информация') ;



	// 90 - ранее ПБОЮЛ
    // 91 - магазин
	$_SESSION['descr_clss'][91]['name']='Магазин' ;
	$_SESSION['descr_clss'][91]['parent']=89 ;
	$_SESSION['descr_clss'][91]['fields']=array('adres'=>'text','city'=>'varchar(255)','region'=>'varchar(255)','vip'=>'int(1)','info'=>'serialize') ;

    $_SESSION['descr_clss'][91]['icons']=_PATH_TO_BASED_CLSS_IMG.'/98.png' ;

	$_SESSION['descr_clss'][91]['list']['field']['enabled']					=array('title'=>'Сост.') ;
	$_SESSION['descr_clss'][91]['list']['field']['account_reffer']		    =array('title'=>'Аккаунт') ;
	$_SESSION['descr_clss'][91]['list']['field']['obj_name']				=array('title'=>'Наименование','td_class'=>'left') ;
	$_SESSION['descr_clss'][91]['list']['field']['vip']						=array('title'=>'VIP','td_class'=>'left') ;
	$_SESSION['descr_clss'][91]['list']['field']['region']					=array('title'=>'Регион','td_class'=>'left') ;
	$_SESSION['descr_clss'][91]['list']['field']['city']					=array('title'=>'Город','td_class'=>'left') ;
    $_SESSION['descr_clss'][91]['list']['field']['adres']					=array('title'=>'Адрес','td_class'=>'left') ;
	$_SESSION['descr_clss'][91]['list']['field']['email']					=array('title'=>'email','td_class'=>'left') ;
	$_SESSION['descr_clss'][91]['list']['field']['phone']					=array('title'=>'Тел.','td_class'=>'left') ;

	$_SESSION['descr_clss'][91]['details']=$_SESSION['descr_clss'][91]['list'] ;
	$_SESSION['descr_clss'][91]['details']['field']['worktime']				=array('title'=>'Время работы') ;
	$_SESSION['descr_clss'][91]['details']['field']['ICQ']					=array('title'=>'ICQ') ;
	$_SESSION['descr_clss'][91]['details']['field']['web']					=array('title'=>'web') ;
	$_SESSION['descr_clss'][91]['details']['field']['dostavka']				=array('title'=>'Наличие доставки') ;
	$_SESSION['descr_clss'][91]['details']['field']['dostavka_text']		=array('title'=>'Описание доставки') ;
	$_SESSION['descr_clss'][91]['details']['field']['kredit']				=array('title'=>'Наличие кредита') ;
	$_SESSION['descr_clss'][91]['details']['field']['info']		            =array('title'=>'Доп информация') ;


	//----------------------------------------------------------------------------------------------------------------------
	// описание пунктов меню админа сайта
	//-----------------------------------------------------------------------------------------------------------------------
    $_SESSION['menu_admin_site']['Модули'][]=array('name'=>'Аккаунты','href'=>'editor_accounts.php','icon'=>'menu/group.jpg') ;
	//$menu_admin_site['Модули'][]=array('name'=>'Клиенты','href'=>'editor_users.php','icon'=>'menu/group.jpg') ;

	// описание меню фрейма объекта
	//-----------------------------------------------------------------------------------------------------------------------
	global $menu_set;
	$menu_set['any'][87][]=array("name" => "Информация",			"action" => 2) ; // частное лицо
	$menu_set['any'][87][]=array("name" => "Входы",				"action" => 3) ;
	$menu_set['any'][87][]=array("name" => "Заказы",				"action" => 4) ;
	$menu_set['any'][87][]=array("name" => "История",				"action" => 5) ;
	$menu_set['any'][87][]=array("name" => "Служба поддержки",	"action" => 6) ;


}

function _account_admin_boot($options)
{
	create_system_modul_obj('account',$options) ; // создание объекта account_system
}

function _account_install_modul($DOT_root)
{   global $admin_dir ;
	echo '<h2>Инсталируем модуль <strong>Система аккаунтов</strong></h2>' ;

    $file_items=array() ;
    // editor_accounts.php
    $file_items['editor_accounts.php']['include'][]='_DIR_TO_ENGINE."/admin/c_site.php"' ;
    $file_items['editor_accounts.php']['options']['use_table_code']='"TM_account"' ;
    $file_items['editor_accounts.php']['options']['title']='"Аккаунты на сайте"' ;
    $file_items['editor_accounts.php']['options']['use_class']='"c_editor_obj"' ;
    create_menu_item($file_items) ;

    $file_items=array() ;
    // editor_accounts.php
    $file_items['editor_accounts.php']['include'][]				='_DIR_TO_MODULES."/account/m_account_frames.php"' ;
    $file_items['editor_accounts.php']['options']['use_table_code']='"TM_account"' ;
    $file_items['editor_accounts.php']['options']['title']		='"Аккаунты"' ;
    $file_items['editor_accounts_tree.php']['include'][]		='_DIR_TO_MODULES."/account/m_account_frames.php"' ;
    $file_items['editor_accounts_viewer.php']['include'][]		='_DIR_TO_MODULES."/account/m_account_frames.php"' ;
    $file_items['editor_accounts_ajax.php']['include'][]		='_DIR_TO_MODULES."/account/m_account_ajax.php"' ;
    create_menu_item($file_items) ;
    
    
	// клиенты
	$pattern['table_title']		= 	'Аккаунты' ;
	$pattern['use_clss']		= 	'1,87,88' ;
	$pattern['def_recs'][]		=	array ('parent'=>0,	'clss'=>1,	'enabled'=>1,	'indx'=>0,	'obj_name'=>'Аккаунты') ;

   	global $TM_account ;
   	$pattern['table_name']				= 	$TM_account ;
	$pattern['table_clss']				= 	101 ;
	$pattern['table_parent']			= 	$DOT_root;
   	create_table_by_pattern($pattern) ;

    SETUP_get_img('/account/admin_img_menu/group.jpg',$admin_dir.'img/menu/group.jpg') ;

    create_dir('/account/') ;
    SETUP_get_file('/account/system_dir/htaccess.txt','/account/.htaccess');
    SETUP_get_file('/account/system_dir/index.php','/account/index.php');
    SETUP_get_file('/account/system_dir/activation.php','/account/activation.php');
    SETUP_get_file('/account/system_dir/create.php','/account/create.php');
    SETUP_get_file('/account/system_dir/forgot.php','/account/forgot.php');
    SETUP_get_file('/account/system_dir/login.php','/account/login.php');
    SETUP_get_file('/account/class/c_account.php','/class/c_account.php');
    SETUP_get_file('/account/class/c_account_activation.php','/class/c_account_activation.php');
    SETUP_get_file('/account/class/c_account_create.php','/class/c_account_create.php');
    SETUP_get_file('/account/class/c_account_forgot.php','/class/c_account_forgot.php');
    SETUP_get_file('/account/class/c_account_login.php','/class/c_account_login.php');
    $list_templates_file=array('form_accounts') ;
    SETUP_get_templates('/account/',$list_templates_file) ;

    import_setting_from_xml(_PATH_TO_SERVER_SETUP.'?m=account&s=setting',array('parent_tkey'=>$_SESSION['pkey_by_table'][TM_SETTING],'parent_pkey'=>1,'check_unique_name'=>1)) ;
    import_setting_from_xml(_PATH_TO_SERVER_SETUP.'?m=account&s=mails',array('parent_tkey'=>$_SESSION['pkey_by_table'][TM_MAILS],'parent_pkey'=>1,'check_unique_name'=>1)) ;

}

//-----------------------------------------------------------------------------------------------------------------------------
// Функции - обработчики событий
//-----------------------------------------------------------------------------------------------------------------------------

 function on_change_event_clss_86_password($tkey,$pkey,$fvalue)
 {  //global $list_status_account ;
   // Сохраняем в журнал событий изменение статуса заказа
   $obj_info=select_db_obj_info($tkey,$pkey) ;
   if ($obj_info['status']==$fvalue) return(0) ; // возвращаем, что значение не изменилось
   //damp_array($obj_info) ;
   //echo 'Обрабатываем событие: измерение статуса для pkey='.$pkey.'<br>' ;

    global $events_system ; $evt_id=$events_system->reg('Изменение данных аккаунта','Изменен пароль аккаунта '.$obj_info['obj_name'].' с "'.$obj_info['password'].'" на "'.$fvalue.'"',$obj_info['_reffer']) ;
    /*
	global  $mail_system,$LS_public_site_name,$LS_mail_bottom_podpis,$patch_to_site ;
	$params=array() ;
    $params['site_name']=$LS_public_site_name ;
    $params['name']=$obj_info['name'] ;
    $params['order_name']=$obj_info['obj_name'] ;
    $params['alt_status']=$list_status_account[$obj_info['status']] ;
    $params['new_status']=$list_status_account[$fvalue] ;
    $params['patch_to_site']=$patch_to_site ;
    $params['mail_bottom_podpis']=$LS_mail_bottom_podpis ;

    $mail_system->send_mail_to_pattern($obj_info['email'],'account_system_info_change_order_status',$params,array('debug'=>0,'use_event_id'=>$evt_id)) ;

    echo 'Изменение статуса заказа "'.$obj_info['obj_name'].'" зарегистрированно в журнале событий, на адрес <strong>'.$obj_info['email'].'</strong> отправлено уведомление о изменении статуса заказа<br>' ;

    */


    return(1) ; // возвращаем, что значение изменилось

 }

//-----------------------------------------------------------------------------------------------------------------------------
// Функции - панели в редакторе
//-----------------------------------------------------------------------------------------------------------------------------

function panel_account_history($options=array())
{
  $reffer=$options['pkey'].'.'.$options['tkey'] ;
  show_history('links like \'%'.$reffer.'%\' or reffer=\''.$reffer.'\'') ;
}

function panel_account_orders($options=array())
{ global $TM_orders ;
  _CLSS(82)->show_list_items($TM_orders,'account_reffer="'.$options['pkey'].'.'.$options['tkey'].'"') ;
}

function panel_account_support($options=array())
{ global $TM_support ;
  _CLSS(16)->show_list_items($TM_support,'account_reffer="'.$options['pkey'].'.'.$options['tkey'].'"') ;
}






?>