<?php
$__functions['init'][]		='_1c_admin_vars' ;
$__functions['install'][]	='_1c_install_modul' ;
$__functions['boot_admin'][]='_1c_admin_boot' ;

include('clss_65.php') ;

function _1c_admin_vars() //
{
   //Сортамент товара
       $_SESSION['descr_clss'][65]['name']='Сортамент товара' ;
       $_SESSION['descr_clss'][65]['table_code']='sortament' ;
       $_SESSION['descr_clss'][65]['parent']=0 ;
       $_SESSION['descr_clss'][65]['fields']['section_id']=array('type'=>'int(11)') ;                           // код товара
       $_SESSION['descr_clss'][65]['fields']['goods_id']=array('type'=>'int(11)') ;                           // код товара
       $_SESSION['descr_clss'][65]['fields']['art']=array('type'=>'varchar(32)') ;                            // fартикул
       $_SESSION['descr_clss'][65]['fields']['1c_id']=array('type'=>'varchar(73)') ;                          // код-1С
       $_SESSION['descr_clss'][65]['fields']['code']=array('type'=>'varchar(32)') ;                           // штрих-код
       $_SESSION['descr_clss'][65]['fields']['stock']=array('type'=>'int(11)') ;                              // склад
       //$_SESSION['descr_clss'][65]['fields']['stock2']=array('type'=>'int(1)') ;                              // склад - принудительно
       $_SESSION['descr_clss'][65]['fields']['price']=array('type'=>'float(10,2)') ;                          // цена

       $_SESSION['descr_clss'][65]['fields']['categor']=array('type'=>'indx_select','array'=>'IL_categor') ;  //  мужской / женский / детский
       $_SESSION['descr_clss'][65]['fields']['size']=array('type'=>'varchar(10)') ;                           // размер
       $_SESSION['descr_clss'][65]['fields']['color']=array('type'=>'indx_select','array'=>'IL_colors') ;     // цвет
       $_SESSION['descr_clss'][65]['fields']['color_name']=array('type'=>'varchar(255)') ;                    // цвет
       $_SESSION['descr_clss'][65]['fields']['img_id']=array('type'=>'int(11)') ;                             // id изображения
       $_SESSION['descr_clss'][65]['fields']['kadr']=array('type'=>'int(11)') ;                               // номер фото
       $_SESSION['descr_clss'][65]['fields']['new']=array('type'=>'int(1)') ;                               // новинка
       $_SESSION['descr_clss'][65]['fields']['price_source']=array('type'=>'float(10,2)') ;                   // цена оригинала
       $_SESSION['descr_clss'][65]['fields']['sales_size']=array('type'=>'int(11)') ; // размер максимальной скидки на сортамент товара в процентах
       $_SESSION['descr_clss'][65]['fields']['sezon']=array('type'=>'varchar(32)') ;
       $_SESSION['descr_clss'][65]['fields']['sezon_id']=array('type'=>'indx_select','array'=>'IL_sezon') ;
       $_SESSION['descr_clss'][65]['fields']['brand_id']=array('type'=>'indx_select','array'=>'IL_brands') ;
       $_SESSION['descr_clss'][65]['fields']['sostav']=array('type'=>'varchar(255)') ;                          // код-1С
       $_SESSION['descr_clss'][65]['fields']['type']=array('type'=>'int(11)') ;                                 // тип товара


       $_SESSION['descr_clss'][65]['child_to']=array(200) ;
       $_SESSION['descr_clss'][65]['parent_to']=array(3) ;




    $_SESSION['descr_clss'][65]['child_to']=array(200) ;

        $_SESSION['descr_clss'][65]['list']['field']['pkey']=array('title'=>'Код') ;
        $_SESSION['descr_clss'][65]['list']['field']['enabled']=array('title'=>'Состояние') ;
        $_SESSION['descr_clss'][65]['list']['field']['indx']=array('title'=>'Позиция') ;
        $_SESSION['descr_clss'][65]['list']['field']['code']=array('title'=>'Штрих-код') ;
        //$_SESSION['descr_clss'][65]['list']['field']['1c_id']=array('title'=>'код-1С') ;
        $_SESSION['descr_clss'][65]['list']['field']['goods_id']=array('title'=>'Код товара<br>на сайте') ;
        $_SESSION['descr_clss'][65]['list']['field']['obj_name']=array('title'=>'Наименование') ;
        //$_SESSION['descr_clss'][65]['list']['field']['art']=array('title'=>'Артикул') ;
        $_SESSION['descr_clss'][65]['list']['field']['price']=array('title'=>'Цена') ;
        $_SESSION['descr_clss'][65]['list']['field']['price_source']=array('title'=>'Цена Исходная') ;
        $_SESSION['descr_clss'][65]['list']['field']['stock']=array('title'=>'Склад по 1С') ;
        //$_SESSION['descr_clss'][65]['list']['field']['stock2']=array('title'=>'Склад свой') ;
        $_SESSION['descr_clss'][65]['list']['field']['categor']=array('title'=>'Категория') ;
        $_SESSION['descr_clss'][65]['list']['field']['sezon']			    ='Сезон' ;
        $_SESSION['descr_clss'][65]['list']['field']['sezon_id']			    ='Сезон ID' ;
        $_SESSION['descr_clss'][65]['list']['field']['type']			    ='Тип ID' ;
        $_SESSION['descr_clss'][65]['list']['field']['new']			    ='Новинки' ;

        $_SESSION['descr_clss'][65]['list']['field']['color']=array('title'=>'Цвет') ;
        //$_SESSION['descr_clss'][65]['list']['field']['color_name']=array('title'=>'Цвет') ;
        $_SESSION['descr_clss'][65]['list']['field']['size']=array('title'=>'Размер') ;
        $_SESSION['descr_clss'][65]['list']['field'][]=array('title'=>'Фото','cmd'=>'show_sortament_image','script'=>'mod/1c/i_1c.php') ;
        $_SESSION['descr_clss'][65]['list']['field']['kadr']			    ='Кадр' ;



       $_SESSION['descr_clss'][65]['details']=$_SESSION['descr_clss'][65]['list'] ;
       $_SESSION['descr_clss'][65]['details']['field']['1c_id']=array('title'=>'Код 1С') ;

       //остатки товара
       $_SESSION['descr_clss'][66]['name']='Остаток товара' ;
       $_SESSION['descr_clss'][66]['table_code']='stock' ;
       $_SESSION['descr_clss'][66]['parent']=0;
       $_SESSION['descr_clss'][66]['fields']['obj_name']=array('type'=>'varchar(32)') ;
       $_SESSION['descr_clss'][66]['fields']['cnt']=array('type'=>'int(11)') ;
       $_SESSION['descr_clss'][66]['child_to']=array(66) ;
       $_SESSION['descr_clss'][66]['parent_to']=array() ;

       $_SESSION['descr_clss'][66]['list']['field']['pkey']=array('title'=>'Код') ;
       $_SESSION['descr_clss'][66]['list']['field']['enabled']=array('title'=>'Состояние') ;
       $_SESSION['descr_clss'][66]['list']['field']['obj_name']=array('title'=>'Склад') ;
       $_SESSION['descr_clss'][66]['list']['field']['cnt']=array('title'=>'Остаток') ;

       $_SESSION['descr_clss'][66]['details']=$_SESSION['descr_clss'][66]['list'] ;

	//----------------------------------------------------------------------------------------------------------------------
	// описание пунктов меню админа сайта
	//-----------------------------------------------------------------------------------------------------------------------

	$_SESSION['menu_admin_site']['Модули']['1c']=array('name'=>'1c','href'=>'editor_1c.php','icon_ext'=>_PATH_TO_MODULES.'/1c/img/1c.png') ;

    //ext_panel_register(array('frame'=>array('table_code'=>'goods','clss'=>200),'menu_item'=>array('name'=>'Сортамент','script'=>'mod/1c/i_1c.php','cmd'=>'panel_goods_sortament'))) ;
    ext_panel_register(array('options'=>array('tkey'=>136,'clss'=>200),'menu_item'=>array('name'=>'Сортамент','script'=>'mod/1c/i_1c.php','cmd'=>'panel_goods_sortament'))) ;
    //ext_panel_register(array('frame'=>array('table_code'=>'goods2','clss'=>200),'menu_item'=>array('name'=>'Сортамент','script'=>'mod/1c/i_1c.php','cmd'=>'panel_goods_sortament'))) ;


}

function _1c_admin_boot($options)
{   $options['no_upload_rec']=1 ;
	create_system_modul_obj('1c',$options) ;
}


function _1c_install_modul($DOT_root)
{   echo '<h2>Инсталируем модуль <strong>Интеграция 1с</strong></h2>' ;

    $file_items=array() ;
    // editor_1c.php
    $file_items['editor_1c.php']['include'][]='_DIR_TO_MODULES."/1c/m_1c_frames.php"' ;
    $file_items['editor_1c.php']['options']['system']='"1c_system"' ;
    $file_items['editor_1c.php']['options']['title']='"Интеграция 1с"' ;
    $file_items['editor_1c_tree.php']['include'][]='_DIR_TO_MODULES."/1c/m_1c_frames.php"' ;
    $file_items['editor_1c_viewer.php']['include'][]='_DIR_TO_MODULES."/1c/m_1c_frames.php"' ;

    create_menu_item($file_items) ;

	$pattern=array() ;
	$pattern['table_title']				= 	'1с' ;
	$pattern['use_clss']				=	'1,65' ;
	$pattern['def_recs'][]				=	array ('parent'=>0,	'clss'=>1,	'enabled'=>1,	'indx'=>0,	'obj_name'=>'Товары 1с') ;
   	$pattern['table_name']				= 	TM_SORTAMENT ;
	$pattern['table_clss']				= 	101 ;
	$pattern['table_parent']			= 	$DOT_root;
    $id=create_table_by_pattern($pattern) ;

    $pattern=array() ;
    $pattern['table_title']				= 	'Склад' ;
    $pattern['use_clss']				= 	'66' ;
    $pattern['table_name']				= 	TM_SORTAMENT_STOCK ;
    $pattern['table_clss']				= 	101 ;
    $pattern['table_parent']			= 	$id;
    create_table_by_pattern($pattern) ;
}



?>