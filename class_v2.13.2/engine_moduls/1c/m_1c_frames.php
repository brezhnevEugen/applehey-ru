<?php
include_once(_DIR_TO_ENGINE."/admin/c_site.php") ;
include_once(_DIR_TO_ENGINE."/admin/i_clss_func.php") ;
include_once(_DIR_TO_ENGINE.'/admin/c_menu_tabs.php') ;

include_once('i_1c.php') ;
include_once(_DIR_EXT.'/angel-discount/sync_1c_angel.php') ;



class c_editor_1c extends c_editor_obj
{
  function body($options=array()) { $this->body_frame_2x(array('fra_tree'=>_PATH_TO_ADMIN.'/editor_1c_tree.php')) ; }
}


class c_editor_1c_tree extends c_fra_tree
{

    function body($options=array())
     {?><body id=tree_objects>
         <ul class="tree_root" on_click_script="editor_1c_viewer.php">
            <li clss=1>1с</li>
            <ul><li cmd=sync_1c>Синхронизация с 1С</li>
                <!--<li cmd=covert_sortament_to_goods>Подцепить свободный сортамент к товарам</li>-->
                <li clss="1">Отладка и тестирование</li>
                <ul>
                    <li cmd=unzip_export_1c>Распаковать последнюю версию выгрузки 1С</li>
                    <li cmd=import_section_from_1c>Импорт разделов из 1С-XML</li>
                    <li cmd=import_sortament_from_1C_XML>Загрузить сортамент из 1C-XML</li>
                    <li cmd=import_goods_from_sortament>Создать товары на основе сортамента</li>
                    <li cmd=import_price_from_1c>Импорт цен и склада из 1C</li>
                    <li cmd=dublicate_info_from_1c>Перенос цен в товары</li>

                    <li cmd=check_1c_files>Проверить статус выгрузки</li>
                    <li cmd=group_goods_by_artikle>Объеденить товары по артикулам</li>


                    <li cmd=import_price_from_1C_XML>Загрузить цены из 1C-XML</li>
                    <li cmd=dublicate_price>Перенести цены и склад из сортамента в товары</li>

                    <li cmd=check_goods_artikle>Проверка артикулов товара</li>
                    <li cmd=group_goods_by_artikle>Сгруппировать товары по артикулу</li>
                    <li cmd=check_goods_name>Проверка наименований товара</li>
                    <li cmd=find_XML_item>Поиск в выгрузке 1С</li>
                    <li cmd=link_goods_sortament>Сопоставить товар и сортамент 1С по артикулу</li>
                    <li cmd=select_color_from_name>Выделить цвет из названия сортамента</li>
                </ul>
                <li clss="1">Контроль</li>
                <ul>
                    <li cmd=show_free_sortament>Показать сортамент без товара</li>
                    <li cmd=show_bad_sortament>Показать сортамент c несуществующими товарами</li>
                    <li cmd=show_free_goods>Показать товар без сортамента</li>
                    <li cmd=show_free_sortament_group_by_art>Показать сортамент без товара сгруппированый по артикулу</li>
                    <li cmd=show_goods_not_art>Показать товары без артикула</li>
                    <li cmd=show_sort_info>Показать информацию по сортаменту</li>
                    <li cmd=show_goods_sort_sezon>Показать товары с сортаментов разных сезонов</li>
                    <li cmd=show_goods_not_sezon>Показать товары без сезона</li>
                    <li cmd=show_goods_not_type>Показать товары без типа</li>
                    <li cmd=show_sort_not_type>Показать сортамент без типа</li>
                    <li cmd=show_section_not_type_M>Показать разделы без типа (M)</li>
                    <li cmd=show_section_not_type_W>Показать разделы без типа (W)</li>
                    <li cmd=show_section_not_type_K>Показать разделы без типа (K)</li>
                </ul>
                <li clss="1">Контроль выгрузки 1с</li>
                <ul>
                    <li cmd=show_section_info>Разделы 1С - разделы на сайте</li>
                    <li cmd=show_section_info_error>Непопоставленные Разделы 1С</li>
                    <li cmd=show_section_type_not_exist>Разделы с несуществущим типом товара</li>
                    <li cmd=check_type_sort_to_section>Соответстие типа сортамента разделу</li>
                    <li cmd=check_type_goods_to_section>Соответстие типа товара разделу</li>
                    <li cmd=check_type_goods_to_name>Соответстие типа товара названию</li>
                    <li cmd=check_type_goods_not_exist>Товары с несуществующим типом</li>
                    <li cmd=set_goods_type_by_section>Задать тип товаров на основе типа разделов</li>
                    <li cmd=view_cnt_goods_by_sezon>Показать число товаров по сезонам</li>
                    <li cmd=view_cnt_photo_by_sezon>Показать число фото по сезонам</li>
                </ul>
            </ul>
         </body>
     <?
   }

}


class c_editor_1c_viewer extends c_fra_based
{
  public $top_menu_name='top_menu_list_1c' ;
  public $system_name='1c_system' ; // временно, пока во всех  editor_1c_viewer.php не будет  прописана эта опция: $options['system']='1c_system' ;

 function body(&$options=array()) {$this->body_frame($options) ; }


    function sync_1c()
     {   set_time_limit(0) ;
         //upload_1c_XML_offer() ;
         $sunc=new sunc_1c_angel() ;
         $sunc->sync_1c() ;
     }

    function unzip_export_1c()
     {   set_time_limit(0) ;
         //upload_1c_XML_offer() ;
         $sunc=new sunc_1c_angel() ;
         $sunc->unzip_export_1c() ;
     }

    function import_section_from_1c()
     {   set_time_limit(0) ;
         //upload_1c_XML_offer() ;
         $sunc=new sunc_1c_angel() ;
         $sunc->import_section_from_1c() ;
     }

    function import_sortament_from_1C_XML()
     {   set_time_limit(0) ;
         //upload_1c_XML_offer() ;
         $sunc=new sunc_1c_angel() ;
         $sunc->import_sotrament_from_1c() ;
     }

     function import_goods_from_sortament()
     {   set_time_limit(0) ;
         $sunc=new sunc_1c_angel() ;
         $sunc->import_goods_from_sortament() ;
     }

     function import_price_from_1c()
     {   set_time_limit(0) ;
         $sunc=new sunc_1c_angel() ;
         $sunc->import_price_from_1c() ;
     }

     function dublicate_info_from_1c()
     {   set_time_limit(0) ;
         $sunc=new sunc_1c_angel() ;
         $sunc->dublicate_info_from_1c() ;
     }



  function sync_1c2()
  { ob_start() ;
    $this->extract_zip_1c() ;
    //$this->check_1c_files() ;
    upload_1c_XML_offer() ;
    $mode=import_price_from_XML() ;  // 1 - 'Только измемения' ; 0 -  'Все данные' ;
    $text_mode=($mode)? 'Только измемения':'Все данные' ;
    covert_sortament_to_goods() ;
    $text=ob_get_clean()  ;
    $name=date('d_m_Y_H_i_s',time()).'.html' ;
    file_put_contents(_DIR_TO_ROOT.'/1c/logs/'.$name,$text) ;
    _event_reg('Импорт из 1С','<a href="/1c/logs/'.$name.'" target=_blank>Отчет</a> - '.$text_mode) ;
    echo $text ;
  }


  function extract_zip_1c()
  {  unzip_export_1c() ;
     return ;


      //          execSQL_update('update '.TM_GOODS.' set tkey=205') ;
   //      execSQL_update('update '.TM_GOODS_IMAGES.' set tkey=210') ;
     ?><h1>Распаковываем последний архив</h1><?
     //ob_start() ;
      $list_files=get_files_list(_DIR_TO_ROOT.'/1c/1cobmen/export/',array('name_pattern'=>'offers')) ; //damp_array($list_files,1,-1) ;
      if (sizeof($list_files))
      { rsort($list_files) ; // более новый файл будет в конце
        $import_zip_file=$list_files[0] ;
        $time=filemtime($import_zip_file) ;
        echo 'Распаковываем <strong>'.basename($import_zip_file).'</strong> [загружен '.date('d.m.Y H:i',$time).']<br>' ;
        unzip_arhive($import_zip_file,_DIR_TO_ROOT.'/1c/1cobmen/work/') ;
        $fname=str_replace('.zip','',basename($import_zip_file)) ;
        rename(_DIR_TO_ROOT.'/1c/1cobmen/work/'.$fname.'.xml',_DIR_TO_ROOT.'/1c/1cobmen/work/offers.xml') ;
        //unlink($import_zip_file) ;
        //echo 'Архив выгрузки удален<br>' ;
      } else echo 'Папка выгрузки 1С пуста<br>' ;


      $list_files=get_files_list(_DIR_TO_ROOT.'/1c/1cobmen/export/',array('name_pattern'=>'import')) ; //damp_array($list_files,1,-1) ;
      if (sizeof($list_files))
      { rsort($list_files) ; // более новый файл будет в конце
        $import_zip_file=$list_files[0] ;
        $time=filemtime($import_zip_file) ;
        echo 'Распаковываем <strong>'.basename($import_zip_file).'</strong> [загружен '.date('d.m.Y H:i',$time).']<br>' ;
        unzip_arhive($import_zip_file,_DIR_TO_ROOT.'/1c/1cobmen/work/') ;
        $fname=str_replace('.zip','',basename($import_zip_file)) ;
        rename(_DIR_TO_ROOT.'/1c/1cobmen/work/'.$fname.'.xml',_DIR_TO_ROOT.'/1c/1cobmen/work/import.xml') ;
        //unlink($import_zip_file) ;
        //echo 'Архив выгрузки удален<br>' ;
      }
     //$text=ob_get_clean() ;

     //_event_reg('Распраковка архива выгрузки 1С',$text) ;
     //echo $text ;
  }

 function check_1c_files()
 {
   $xml=simplexml_load_file(_DIR_TO_ROOT.'/1c/1cobmen/work/offers.xml') ;
   $arr_attr=$xml->ПакетПредложений->attributes() ;
   if ($arr_attr['СодержитТолькоИзменения']=='true') echo 'Только измемения' ; else echo 'Все данные' ;
   echo '<br>' ;

   $xml=simplexml_load_file(_DIR_TO_ROOT.'/1c/1cobmen/work/import.xml') ;
   $arr_attr=$xml->Каталог->attributes() ;
   if ($arr_attr['СодержитТолькоИзменения']=='true') echo 'Только измемения' ; else echo 'Все данные' ;
   echo '<br>' ;



   $xml=simplexml_load_file(_DIR_TO_ROOT.'/1c/offers63569201666.xml') ;
   $arr_attr=$xml->ПакетПредложений->attributes() ;
   if ($arr_attr['СодержитТолькоИзменения']=='true') echo 'Только измемения' ; else echo 'Все данные' ;
   echo '<br>' ;

   $xml=simplexml_load_file(_DIR_TO_ROOT.'/1c/import63570302713.xml') ;
   $arr_attr=$xml->Каталог->attributes() ;
   if ($arr_attr['СодержитТолькоИзменения']=='true') echo 'Только измемения' ; else echo 'Все данные' ;
   //print_r($arr_attr) ;
   //        /ПакетПредложений СодержитТолькоИзменения="true"
   //<Каталог СодержитТолькоИзменения="false">
   //damp_array($arr_attr,1,-1) ;
 }


    function find_XML_item()
    {
        ?><h2>Информация по сортаменту</h2>
        <form>
            Код 1С <input type="text" name="code"> или артикул <input type="text" name="art"> <input type="submit" cmd=find_XML_item_apply value="Найти" validate="form"></form>

        <?
        $list_files=get_files_list(_DIR_TO_ROOT.'/1c/export/',array('name_pattern'=>'xml')) ; //damp_array($list_files,1,-1) ;
        damp_array($list_files,1,-1) ;

    }

    function find_XML_item_apply()
    {
      $list_files=get_files_list(_DIR_TO_ROOT.'/1c/export/',array('name_pattern'=>'xml')) ; //damp_array($list_files,1,-1) ;
      //damp_array($list_files,1,-1) ;
      //damp_array($_POST);
      if ($_POST['code']) echo 'ищем штрих-код:'.$_POST['code'].'<br>' ;
      if ($_POST['art']) echo 'ищем артикул:'.$_POST['art'].'<br>' ;
      //damp_string($_POST['art']) ;
      //damp_string('06-2-DS-R-000351') ;
      ?><table><?
      if (sizeof($list_files)) foreach($list_files as $fname)
      { ?><h2><?echo $fname?></h2><?
        $xml=simplexml_load_file($fname) ;
        if (sizeof($xml->Каталог->Товары->Товар))  foreach ($xml->Каталог->Товары->Товар as $goods) // проходим по списку сортаментов, группируем их в товары
           { $code=(string)$goods->Штрихкод ;
             $art=trim((string)$goods->Артикул) ;
             $show=0 ;
             if ($_POST['code'] and $code==$_POST['code']) $show=1 ;
             if ($_POST['art'] and $_POST['art']==$art) $show=1 ;
             //echo $art.'<br>' ; var_dump($art) ; var_dump($_POST['art']) ;
             if ($show) {?><textarea style="width:100%;height:100px;"><?print_r($goods) ;?></textarea><?}
             //break ;
           }
        if (sizeof($xml->ПакетПредложений->Предложения->Предложение)) foreach ($xml->ПакетПредложений->Предложения->Предложение as $offer=>$price_info)
            { $code=(string)$price_info->Штрихкод ;
              $art=trim((string)$price_info->Артикул) ;
              $show=0 ;
              if ($_POST['code'] and $code==$_POST['code']) $show=1 ;
              if ($_POST['art'] and $_POST['art']==$art) $show=1 ;
              //echo $art.'<br>' ; var_dump($art) ; var_dump($_POST['art']) ;
              if ($show) {?><textarea style="width:100%;height:100px;"><?print_r($price_info) ;?></textarea><?}
              //break ;
            }



      }
      ?></table><?
    }



 function import_price_from_1C_XML()
 {
     import_price_from_XML() ;
 }

 function covert_sortament_to_goods()
 {
     covert_sortament_to_goods() ;
 }

 function dublicate_price()
 {
     dublicate_price() ;
 }

 function link_goods_sortament()
 {
    execSQL_update('update '.TM_SORTAMENT.' t1 set t1.goods_id=(select t2.pkey from '.TM_GOODS.' t2 where t2.art!="" and t2.art=t1.art and (t2.is_sortament=0 or t2.is_sortament is null))',array('debug'=>2)) ;
 }
   /*
 function link_goods_sortament_by_name()
 {
    execSQL_update('update '.TM_SORTAMENT.' t1 set t1.goods_id=(select t2.pkey from '.TM_GOODS.' t2 where t2.obj_name!="" and t2.obj_name=t1.obj_name and (t2.is_sortament=0 or t2.is_sortament is null)) where (goods_id is null or goods_id=0)',array('debug'=>2)) ;
 }   */

 function check_goods_artikle()
 {  ?><h2>Выявляем товары (is_sortament=0) c неуникальым артикулом</h2><?
    $recs=execSQL('select art,count(pkey) as cnt from '.TM_GOODS.' where clss=200 and art!="" and not art is NULL and (is_sortament=0 or is_sortament is null) group by art having cnt>1') ;
    if (sizeof($recs)) foreach($recs as $rec)
    { ?><h2>Товары с артикулом "<?echo $rec['art']?>" - <?echo $rec['cnt']?> шт.</h2><?
      _CLSS(200)->show_list_items(TM_GOODS,'art="'.$rec['art'].'"') ;
    }
    else echo 'ВСЕ товары имеют уникальные артикулы' ;
    //damp_array($recs) ;
 }

 function check_goods_name()
 {  ?><h2>Выявляем товары (is_sortament=0) c неуникальым наименованием</h2><?
    $recs=execSQL('select obj_name,count(pkey) as cnt from '.TM_GOODS.' where clss=200 and obj_name!="" and not obj_name is NULL and (is_sortament=0 or is_sortament is null) group by obj_name having cnt>1') ;
    if (sizeof($recs)) foreach($recs as $rec)
    { ?><h2>Товары с наименованием "<?echo $rec['obj_name']?>" - <?echo $rec['cnt']?> шт.</h2><?
      _CLSS(200)->show_list_items(TM_GOODS,'obj_name="'.$rec['obj_name'].'"') ;
    }
    else echo 'ВСЕ товары имеют уникальные наименования' ;
    //damp_array($recs) ;
 }

 function group_goods_by_artikle()
 { $arr_goods_old_group=array() ;
   $arr_goods_old=execSQL('select pkey,art,obj_name from '.TM_GOODS.' where clss=200 and art!="" and not art is null and (is_sortament=0 or is_sortament is null) order by pkey') ;
   echo 'В базе '.sizeof($arr_goods_old).' товаров с артикулами<br>' ;     //Обработано 1965 позиций выгрузки, найдено 1156 товаров с сортаментом
   if (sizeof($arr_goods_old)) foreach($arr_goods_old as $pkey=>$rec) $arr_goods_old_group[trim($rec['art'])][$pkey]=$rec['obj_name'] ;
   echo 'В базе '.sizeof($arr_goods_old_group).' уникальных артикулов<br>' ;     //Обработано 1965 позиций выгрузки, найдено 1156 товаров с сортаментом
   if (sizeof($arr_goods_old_group)) foreach($arr_goods_old_group as $art=>$arr_ids) if (sizeof($arr_ids)>1)
   { list($main_id,$main_rec)=each($arr_ids) ;
     // переносим названия товара в название фото
     if (sizeof($arr_ids)) foreach($arr_ids as $pkey=>$name)
     {  //$sql='update '.TM_GOODS_IMAGES.' set obj_name="'.addslashes($name).'",parent='.$main_id.' where parent='.$pkey ;
        $res=update_rec_in_table(TM_GOODS_IMAGES,array('obj_name'=>addslashes($name),'parent'=>$main_id),'parent='.$pkey) ;
     }
     unset($arr_ids[$main_id]) ;
     // переносим картинки в основной товар
     execSQL_update('update '.TM_GOODS_IMAGES.' set parent='.$main_id.' where parent in ('.implode(',',array_keys($arr_ids)).')') ;
     // удаляем вторичный товар
     execSQL_update('delete from '.TM_GOODS.' where pkey in ('.implode(',',array_keys($arr_ids)).')') ;
     echo 'Объединен товар артикул <strong>'.$art.'</strong><br>' ;
   }
 }

 function group_goods_by_name()
 { $arr_goods_old_group=array() ;
   $arr_goods_old=execSQL('select pkey,art,obj_name from '.TM_GOODS.' where clss=200 and obj_name!="" and not obj_name is null and (is_sortament=0 or is_sortament is null) order by pkey') ;
   echo 'В базе '.sizeof($arr_goods_old).' товаров с наименованиями<br>' ;     //Обработано 1965 позиций выгрузки, найдено 1156 товаров с сортаментом
   if (sizeof($arr_goods_old)) foreach($arr_goods_old as $pkey=>$rec) $arr_goods_old_group[trim($rec['obj_name'])][$pkey]=$rec['art'] ;
   echo 'В базе '.sizeof($arr_goods_old_group).' уникальных наименований<br>' ;     //Обработано 1965 позиций выгрузки, найдено 1156 товаров с сортаментом

   if (sizeof($arr_goods_old_group)) foreach($arr_goods_old_group as $name=>$arr_ids) if (sizeof($arr_ids)>1)
   { list($main_id,$main_rec)=each($arr_ids) ;

     // переносим названия товара в название фото
     if (sizeof($arr_ids)) foreach($arr_ids as $pkey=>$name) execSQL_update('update '.TM_GOODS_IMAGES.' set obj_name="'.addslashes($name).'",parent='.$main_id.' where parent='.$pkey) ;
       echo 'Объединен товар  <strong>'.$name.'</strong> = ['.sizeof($arr_ids).'], артикулы '.implode(',',$arr_ids).'<br>' ;
     unset($arr_ids[$main_id]) ;

     // переносим товар в товар
     //execSQL_update('update '.TM_GOODS.' set parent='.$main_id.',is_sortament=1 where pkey in ('.implode(',',array_keys($arr_ids)).')') ;
     // переносим картинки в основной товар
     //execSQL_update('update '.TM_GOODS_IMAGES.' set parent='.$main_id.' where parent in ('.implode(',',array_keys($arr_ids)).')') ;

   }
 }

 function show_free_sortament()
 {
    ?><h2>Сортамент 1С без товара на сайте</h2><?
    _CLSS(65)->show_list_items(TM_SORTAMENT,'(goods_id is null or goods_id=0)') ;
 }

function show_sortament_not_price()
 {
    ?><h2>Сортамент 1С без цены</h2><?
    _CLSS(65)->show_list_items(TM_SORTAMENT,'price=0') ;
 }

 function show_bad_sortament()
 {
    ?><h2>Сортамент 1С c goods_id на несуществующие товары</h2><?
    _CLSS(65)->show_list_items(TM_SORTAMENT,'not goods_id in (select pkey from '.TM_GOODS.')') ;
 }

 function show_free_goods()
 {
    ?><h2>Товары сайта без сортамента в 1С</h2><?
    _CLSS(200)->show_list_items(TM_GOODS,'not pkey in (select goods_id from '.TM_SORTAMENT.' where goods_id>0) and enabled=1 and _enabled=1 and (is_sortament=0 or is_sortament is null)',array('order'=>'parent')) ;

    //_CLSS(65)->show_list_items(TM_SORTAMENT,'(goods_id is null or goods_id=0)') ;
 }

 function show_goods_not_art()
 {
    ?><h2>Товары на сайте без артикула, включенные к показу на сайте</h2><?
    _CLSS(200)->show_list_items(TM_GOODS,'(art is null or art="") and enabled=1') ;

   //$recs=execSQL('select pkey,obj_name,')
 }


 function show_free_sortament_group_by_art()
 {
    ?><h2>Сортамент 1С без товара на сайте сгруппированный по артикулу, более одного товара в группе:</h2><?
    $recs=execSQL_row('select art,count(pkey) as cnt from '.TM_SORTAMENT.' where (goods_id is null or goods_id=0) group by art having cnt>1') ;
    //damp_array($recs) ;
    if (sizeof($recs)) foreach($recs as $art=>$count)
    {  ?><h2><?echo $art?></h2><?
       _CLSS(65)->show_list_items(TM_SORTAMENT,'art="'.$art.'"',array('buttons'=>array())) ;
    }
 }

 function select_color_from_name()
 {
     $arr_art_uni=execSQL_row('select art,count(pkey) as cnt from '.TM_SORTAMENT.' where clss=65 group by art having cnt>1') ;
     //damp_array($arr_art_uni) ; return ;
        ?><table class="debug"><?
        if (sizeof($arr_art_uni))  foreach($arr_art_uni as $art=>$cnt)
        {    $recs=execSQL_row('select pkey,obj_name from '.TM_SORTAMENT.' where art="'.$art.'"') ;

             $arr_res=array() ;  list($id,$name)=each($recs) ;  reset($recs);
                 ?><tr><th colspan="4"><h3><?echo $name?></h3></th></tr><?
                 foreach($recs as $sort_id=>$value)
                 { $value=str_replace(array('(',')'),array(' ( ',' ) '),$value) ;
                   $arr=explode(' ',$value) ;
                   if (sizeof($arr)) foreach($arr as $str) if (trim($str)) $arr_res[$str]++ ;
                 }

                 foreach($recs as $sort_id=>$value)
                 { $value=str_replace(array('(',')'),array(' ( ',' ) '),$value) ;
                   $arr=explode(' ',$value) ;  $arr_str_main=array() ;  $arr_str_second=array() ; $new_value_main='' ; $new_value_second='' ;
                   if (sizeof($arr)) foreach($arr as $str) if ($arr_res[$str]>1) $arr_str_main[]=$str ; else $arr_str_second[]=$str ;
                   if (sizeof($arr_str_main))  $new_value_main=implode(' ',$arr_str_main) ;
                   if (sizeof($arr_str_second))  $new_value_second=implode(' ',$arr_str_second) ;

                     $new_value_main=trim(str_replace(array('  ','( )'),array(' ',''),$new_value_main)) ;
                     $new_value_second=trim(str_replace(array('  ','( )'),array(' ',''),$new_value_second)) ;
                     //if (!$new_value_second) $new_value_second=$new_value_main ;
                   echo '<tr><td>'.$sort_id.'</td><td class="left">'.$value.'</td><td class="left">'.$new_value_main.'</td><td>'.$new_value_second ;
                   //       if ($sort_id==1178) damp_string()
                            //if ($sort_id==1677) damp_array($arr_res,1,-1) ;
                     echo '</td></tr>' ;
                    execSQL_update('update '.TM_SORTAMENT.' set color_name="'.addslashes($new_value_second).'" where pkey='.$sort_id) ;
                 }
        }
        ?></table><?
 }



 function show_sort_info()
 {
     ?><h2>Информация по сортаменту</h2><form><input type="text" name="code"><input type="submit" cmd=show_sort_info_apply value="Проверить" validate="form"></form><?

 }

 function show_sort_info_apply()
 {
     //damp_array($_POST) ;
     ?><h2>Информация по сортаменту "<?echo $_POST['code']?>"</h2><?
     $recs=execSQL('select * from '.TM_SORTAMENT.' where code="'.$_POST['code'].'" or art="'.$_POST['code'].'"') ;
     print_2x_arr($recs) ;

     if (sizeof($recs))
     { foreach($recs as $rec) $arr[]=$rec['goods_id'] ;
       ?><h2>Информация по товару сортамента "<?echo $_POST['code']?>"</h2><?
       $rec=execSQL('select * from '.TM_GOODS.' where pkey in ("'.implode(',',$arr).'")') ;
       print_2x_arr($rec) ;
     }

     if (sizeof($recs))
     { ?><h2>Информация по складу"<?echo $_POST['code']?>"</h2><?
       $rec=execSQL('select * from obj_site_1c_stock where parent in ("'.implode(',',array_keys($recs)).'")') ;
       print_2x_arr($rec) ;
     }
 }

 function show_goods_sort_sezon()
 { //damp_array($_POST) ;
   if ($_POST['action']=='split_goods_by_sezon')
   {
       /*РАЗДЛЕНИЕ ТОВАРОВ ПО СЕЗОНАМ*/
          ob_start() ;
          $recs=execSQL('select t1.goods_id,t1.art,t1.obj_name,count(distinct(t1.sezon_id)) as cnt,(select count(t2.pkey) from obj_site_goods_image t2 where t2.parent=t1.goods_id) as cnt_image,(select enabled from obj_site_goods where pkey=t1.goods_id) as goods_enabled from obj_site_1c t1 where goods_id>0 group by t1.goods_id having cnt>1',2) ;
          foreach($recs as $rec) if ($rec['goods_id'])
          {  $sezons=execSQL_line('select distinct(sezon_id) from obj_site_1c where goods_id='.$rec['goods_id']) ;// print_r($sezons) ;
             //$sort_rec=execSQL('select * from obj_site_1c where goods_id='.$rec['goods_id'],2) ;
             $ext_goods=execSQL_van('select * from obj_site_goods where pkey="'.$rec['goods_id'].'"') ;
             $source_goods_id=$ext_goods['pkey'] ;
             unset($ext_goods['pkey'],$ext_goods['_reffer'],$ext_goods['_parent_reffer']) ;

             $sort_rec=execSQL_van('select * from obj_site_1c where goods_id='.$rec['goods_id'].' and sezon_id='.$sezons[0].' limit 1') ;
             $ext_goods['obj_name']=$sort_rec['obj_name'].', сезон '.$sort_rec['sezon'] ;
             $ext_goods['sezon_id']=$sort_rec['sezon_id'] ;
             $ext_goods['sezon']=$sort_rec['sezon'] ;
             $ext_goods['url_name']=_CLSS($ext_goods['clss'])->prepare_url_obj($ext_goods) ;
             echo 'Сохранили в исходную запись по товару: арт.<strong>'.$ext_goods['art'].'</strong>: '.$ext_goods['obj_name'].', sezon_id='.$ext_goods['sezon_id'].', '.$ext_goods['url_name'].'<br>' ;
             //damp_array($ext_goods) ;


             unset($sezons[0]) ;
             foreach($sezons as $sezon_id)
             { $sort_rec=execSQL_van('select * from obj_site_1c where goods_id='.$rec['goods_id'].' and sezon_id='.$sezon_id.' limit 1') ;
              $ext_goods['obj_name']=$sort_rec['obj_name'].', сезон '.$sort_rec['sezon'] ;
              $ext_goods['sezon_id']=$sort_rec['sezon_id'] ;
              $ext_goods['sezon']=$sort_rec['sezon'] ;
              $ext_goods['url_name']=_CLSS($ext_goods['clss'])->prepare_url_obj($ext_goods) ;
              echo 'Добавляем новую запись по товару арт.<strong>'.$ext_goods['art'].'</strong>: '.$ext_goods['obj_name'].', sezon_id='.$ext_goods['sezon_id'].', '.$ext_goods['url_name'].'<br>' ;

              // добавляем копию существующего товара
              $new_id=adding_rec_to_table('obj_site_goods',$ext_goods,array('debug'=>0)) ;
              //damp_array($ext_goods) ;
              echo '<span class=green>Добавили новый товар id='.$new_id.'</span><br>' ;
              update_rec_in_table('obj_site_1c',array('goods_id'=>$new_id),'art="'.$ext_goods['art'].'" and sezon_id='.$sezon_id,array('no_use_descr_obj_tables'=>1)) ;
              // получаем фото старого товара
              $recs_images=execSQL('select * from obj_site_goods_image where parent='.$source_goods_id) ;
                if (sizeof($recs_images)) foreach($recs_images as $rec_img)
                { unset($rec_img['pkey'],$rec_img['_reffer'],$rec_img['_parent_reffer']) ;
                  $rec_img['parent']=$new_id ;
                  // добавляем копию существующего изображения
                  $img_id=adding_rec_to_table('obj_site_goods_image',$rec_img) ;
                  echo '<span class=green>Добавили новое изображение  id='.$img_id.'</span><br>' ;
                }
             }
             //echo

          }
          $text=ob_get_clean() ; echo $text ;
          $fname='report_'.date('d_m_Y_H_i_s').'.html' ;
             $report_dir=_DIR_TO_ROOT.'/1c/reports/' ;
             file_put_contents($report_dir.$fname,$text) ;
             echo '<a href="/1c/reports/'.$fname.'" target=_blank>Отчет</a>' ;
          return ;
   }

   $recs=execSQL_row('select goods_id,count(distinct(sezon_id)) as cnt from '.TM_SORTAMENT.'  group by goods_id having cnt>1') ;
   if (sizeof($recs))
   {
      _CLSS(200)->show_list_items(TM_GOODS,'pkey in ('.implode(',',array_keys($recs)).')',array('buttons'=>array())) ;
      ?><button cmd="show_goods_sort_sezon" action="split_goods_by_sezon">Разделить товары по сезонам</button><?

   }
 }

  function show_goods_not_sezon()
  {
      _CLSS(200)->show_list_items(TM_GOODS,"(sezon_id is null or sezon_id=0 or sezon is null or sezon='')",array('buttons'=>array())) ;
  }

  function show_goods_not_type()
  {
      _CLSS(200)->show_list_items(TM_GOODS,"(type is null or type=0)",array('buttons'=>array())) ;
  }

  function show_sort_not_type()
  {
      _CLSS(65)->show_list_items(TM_SORTAMENT,"(section_id is null or section_id=0)",array('buttons'=>array())) ;
  }

  function show_section_not_type_M()
  {
      global $category_system ;
            //$id=_IL('IL_brands')->get_id_of_value('Christian Dior',array('append_value_is_not_found'=>1,'append_value_clss'=>222)) ;
            ///echo 'id='.$id.'<br>' ;
            $recs=execSQL('select t1.pkey,t1.obj_name,
                          (select count(t2.pkey) from obj_site_goods t2  where t2.parent=t1.pkey and t2.m=1 and t2.clss=200 and t2.enabled=1 and t2._enabled=1 and t2.stock>0 and t2._image_name!="") as cnt_active_goods
                           from obj_site_goods t1
                           where (t1.type_m=0 or t1.type_m is null) and clss in (1,10) and pkey>1 having cnt_active_goods>0',2) ;

            $recs_section_m_ids=$category_system->tree[773]->get_arr_child() ;
            if (sizeof($recs_section_m_ids)) foreach($recs_section_m_ids as $id) $arr_categor[$category_system->tree[$id]->name]=$id ;
            //damp_array($arr_categor) ;
            //echo $recs_section_m_ids ;

            if (sizeof($recs)) foreach($recs as $rec_section) if ($rec_section['cnt_active_goods'])
            {   $section_name=$rec_section['obj_name'] ;
                $section_id=$rec_section['pkey'] ;
                if ($section_name=='Рубашка') $section_name='Рубашки' ;
                if ($section_name=='Майка') $section_name='Майки' ;
                if ($section_name=='Куртка') $section_name='Куртки' ;
                if ($section_name=='Жилеты') $section_name='Жилет' ;
                if ($section_name=='Пальто') $section_name='Пальто и Плащи' ;
                if ($section_name=='Костюм') $section_name='Костюмы' ;
                if ($section_name=='Галстук') $section_name='Галстуки' ;
                if ($section_name=='Набор трусов') $section_name='Трусы' ;
                if ($section_name=='Сумка') $section_name='Сумки' ;
                if ($section_name=='Визитница') $section_name='Визитницы' ;
                if ($section_name=='Шляпа') $section_name='Шляпы' ;
                if ($section_name=='Чехол IPad') $section_name='Чехлы iPad и IPhone' ;
                if ($section_name=='Портмоне') $section_name='Кошельки и портмоне' ;
                if ($section_name=='Пиджак') $section_name='Пиджаки' ;
                if ($section_name=='Шарф') $section_name='Шарфы' ;
                if ($section_name=='Ремень') $section_name='Ремни' ;
                if (!isset($arr_categor[$section_name])) echo 'Не задан тип товара для раздела 1С "<span class="red">'.$section_name.'</span>"<br>' ;
                else
                {   echo 'Автоматическое проставление типа для "'.$section_name.'"<br>' ;
                    update_rec_in_table('obj_site_goods',array('type_m'=>$arr_categor[$section_name]),'pkey='.$section_id,array('debug'=>0)) ;

                }

            }
  }
  function show_section_not_type_W()
  {
      global $category_system ;
            $recs=execSQL('select t1.pkey,t1.obj_name,
                          (select count(t2.pkey) from obj_site_goods t2  where t2.parent=t1.pkey and t2.m=1 and t2.clss=200 and t2.enabled=1 and t2._enabled=1 and t2.stock>0 and t2._image_name!="") as cnt_active_goods
                           from obj_site_goods t1
                           where (t1.type_w=0 or t1.type_w is null) and clss in (1,10) and pkey>1 having cnt_active_goods>0',2) ;

      $recs_section_ids=$category_system->tree[774]->get_arr_child() ;
            if (sizeof($recs_section_ids)) foreach($recs_section_ids as $id) $arr_categor[$category_system->tree[$id]->name]=$id ;

            if (sizeof($recs)) foreach($recs as $rec_section) if ($rec_section['cnt_active_goods'])
            {   $section_name=$rec_section['obj_name'] ;
                $section_id=$rec_section['pkey'] ;
                //if ($section_name=='Рубашка') $section_name='Рубашки' ;
                //if ($section_name=='Майка') $section_name='Майки' ;
                //if ($section_name=='Куртка') $section_name='Куртки' ;
                //if ($section_name=='Жилеты') $section_name='Жилет' ;
                //if ($section_name=='Пальто') $section_name='Пальто и Плащи' ;
                //if ($section_name=='Костюм') $section_name='Костюмы' ;
                //if ($section_name=='Галстук') $section_name='Галстуки' ;
                //if ($section_name=='Набор трусов') $section_name='Трусы' ;
                //if ($section_name=='Сумка') $section_name='Сумки' ;
                //if ($section_name=='Визитница') $section_name='Визитницы' ;
                //if ($section_name=='Шляпа') $section_name='Шляпы' ;
                if ($section_name=='Чехол IPhone') $section_name='Чехлы iPad и IPhone' ;
                //if ($section_name=='Портмоне') $section_name='Кошельки и портмоне' ;
                //if ($section_name=='Пиджак') $section_name='Пиджаки' ;
                //if ($section_name=='Шарф') $section_name='Шарфы' ;
                //if ($section_name=='Ремень') $section_name='Ремни' ;
                if (!isset($arr_categor[$section_name])) echo 'Не задан тип товара для раздела 1С "<span class="red">'.$section_name.'</span>"<br>' ;
                else
                {   echo 'Автоматическое проставление типа для "'.$section_name.'"<br>'  ;
                    update_rec_in_table('obj_site_goods',array('type_w'=>$arr_categor[$section_name]),'pkey='.$section_id,array('debug'=>0)) ;

                }

            }
  }

  function show_section_not_type_K()
  {
      global $category_system ;
            $recs=execSQL('select t1.pkey,t1.obj_name,
                          (select count(t2.pkey) from obj_site_goods t2  where t2.parent=t1.pkey and t2.k=1 and t2.clss=200 and t2.enabled=1 and t2._enabled=1 and t2.stock>0 and t2._image_name!="") as cnt_active_goods
                           from obj_site_goods t1
                           where (t1.type_k=0 or t1.type_k is null) and clss in (1,10) and pkey>1 having cnt_active_goods>0',2) ;

      $recs_section_ids=$category_system->tree[774]->get_arr_child() ;
            if (sizeof($recs_section_ids)) foreach($recs_section_ids as $id) $arr_categor[$category_system->tree[$id]->name]=$id ;

            if (sizeof($recs)) foreach($recs as $rec_section) if ($rec_section['cnt_active_goods'])
            {   $section_name=$rec_section['obj_name'] ;
                $section_id=$rec_section['pkey'] ;
                //if ($section_name=='Рубашка') $section_name='Рубашки' ;
                //if ($section_name=='Майка') $section_name='Майки' ;
                //if ($section_name=='Куртка') $section_name='Куртки' ;
                //if ($section_name=='Жилеты') $section_name='Жилет' ;
                //if ($section_name=='Пальто') $section_name='Пальто и Плащи' ;
                //if ($section_name=='Костюм') $section_name='Костюмы' ;
                //if ($section_name=='Галстук') $section_name='Галстуки' ;
                //if ($section_name=='Набор трусов') $section_name='Трусы' ;
                //if ($section_name=='Сумка') $section_name='Сумки' ;
                //if ($section_name=='Визитница') $section_name='Визитницы' ;
                //if ($section_name=='Шляпа') $section_name='Шляпы' ;
                if ($section_name=='Чехол IPhone') $section_name='Чехлы iPad и IPhone' ;
                //if ($section_name=='Портмоне') $section_name='Кошельки и портмоне' ;
                //if ($section_name=='Пиджак') $section_name='Пиджаки' ;
                //if ($section_name=='Шарф') $section_name='Шарфы' ;
                //if ($section_name=='Ремень') $section_name='Ремни' ;
                if (!isset($arr_categor[$section_name])) echo 'Не задан тип товара для раздела 1С "<span class="red">'.$section_name.'</span>"<br>' ;
                else
                {   echo 'Автоматическое проставление типа для "'.$section_name.'"<br>'  ;
                    update_rec_in_table('obj_site_goods',array('type_w'=>$arr_categor[$section_name]),'pkey='.$section_id,array('debug'=>0)) ;

                }

            }
  }

    function show_text_sort_info($cnt_off,$cnt_on)
     {
         if ($cnt_off or $cnt_on)   echo '<br>сорт.: <span>'.(($cnt_off)? $cnt_off:'-').'</span> / <span class=green>'.(($cnt_on)? $cnt_on:'-').'</span>';
     }

     function show_text_goods_info($type_id,$cnt_on,$cnt_all)
     {   global $category_system ;
         $cnt_off=$cnt_all-$cnt_on ;
         if ($cnt_off or $cnt_on)   echo '<br>тов.: <span title="Отключенных товаров без типа">'.(($cnt_off)? $cnt_off:'-').'</span> / <span title="Включенных товаров без типа" class="'.((isset($category_system->tree[$type_id]))? 'green bold':'red bold').'">'.(($cnt_on)? $cnt_on:'-').'</span>';
         $res=($cnt_on and !isset($category_system->tree[$type_id]))? 1:0;
         return($res) ;
     }

     function show_section_info()
     {
         global $category_system ;
         ?><p>Показывает наличие активных товаров в несопоставленных разделах. Товары из несопоставленных разделов не будут показаны на сайте. </p><?
         ?><p>Показан сопоставленный раздел для каждой категории товара (М, Ж, Д)</p><?
         ?><p><span class="green bold">Жирным зеленым</span> - активный товар, показан на сайте </p><?
         ?><p><span class="red bold">Жирным красный</span> - активный товар, не показан на сайте </p><?
         $recs=execSQL('select pkey as id,obj_name,type_m,type_w,type_k from obj_site_goods where clss in (1,10) and obj_name not like "%Осень%" and obj_name not like "%Зима%" and obj_name not like "%Весна%" and obj_name not like "%Лето%" order by pkey') ;
         if (sizeof($recs)) foreach($recs as $rec)
         {  if ($rec['type_m'] and !isset($category_system->tree[$rec['type_m']]))
            { execSQL_update('update obj_site_goods set type_m=0 where pkey='.$rec['id']) ;
              echo 'Сброшен несуществующий тип раздела type_m='.$rec['type_m'].' для раздела '.$rec['obj_name'].' ['.$rec['pkey'].']<br>' ;
            }
            if ($rec['type_w'] and !isset($category_system->tree[$rec['type_w']]))
            { execSQL_update('update obj_site_goods set type_w=0 where pkey='.$rec['id']) ;
              echo 'Сброшен несуществующий тип раздела type_w='.$rec['type_w'].' для раздела '.$rec['obj_name'].' ['.$rec['pkey'].']<br>' ;
            }
            if ($rec['type_k'] and !isset($category_system->tree[$rec['type_k']]))
            { execSQL_update('update obj_site_goods set type_k=0 where pkey='.$rec['id']) ;
              echo 'Сброшен несуществующий тип раздела type_k='.$rec['type_k'].' для раздела '.$rec['obj_name'].' ['.$rec['pkey'].']<br>' ;
            }
         }

         $recs=execSQL('select t1.pkey as id,t1.obj_name,type_m,type_w,type_k,
                       (select (t2.obj_name) from obj_site_goods t2 where t2.pkey=t1.parent) as parent_name,
                       (select count(t2.pkey) from obj_site_1c t2 where t2.section_id=t1.pkey and t2.categor=1 and enabled=1 and price>0 and stock>0) as cnt_sort_m_on,
                       (select count(t2.pkey) from obj_site_1c t2 where t2.section_id=t1.pkey and t2.categor=1 and (enabled=0 or price=0 or stock=0)) as cnt_sort_m_off,
                       (select count(t2.pkey) from obj_site_1c t2 where t2.section_id=t1.pkey and t2.categor=2 and enabled=1 and price>0 and stock>0) as cnt_sort_w_on,
                       (select count(t2.pkey) from obj_site_1c t2 where t2.section_id=t1.pkey and t2.categor=2 and (enabled=0 or price=0 or stock=0)) as cnt_sort_w_off,
                       (select count(t2.pkey) from obj_site_1c t2 where t2.section_id=t1.pkey and t2.categor=3 and enabled=1 and price>0 and stock>0) as cnt_sort_k_on,
                       (select count(t2.pkey) from obj_site_1c t2 where t2.section_id=t1.pkey and t2.categor=3 and (enabled=0 or price=0 or stock=0)) as cnt_sort_k_off,
                       (select count(t2.pkey) from view_goods t2 where t2.parent=t1.pkey and t2.m=1 and t2.enabled=1 and t2.price>0 and t2.stock>0 and t2._image_name!="") as cnt_sort_goods_m_on,
                       (select count(t2.pkey) from view_goods t2 where t2.parent=t1.pkey and t2.m=1) as cnt_sort_goods_m_all,
                       (select count(t2.pkey) from view_goods t2 where t2.parent=t1.pkey and t2.w=1 and t2.enabled=1 and t2.price>0 and t2.stock>0 and t2._image_name!="") as cnt_sort_goods_w_on,
                       (select count(t2.pkey) from view_goods t2 where t2.parent=t1.pkey and t2.w=1) as cnt_sort_goods_w_all,
                       (select count(t2.pkey) from view_goods t2 where t2.parent=t1.pkey and t2.k=1 and t2.enabled=1 and t2.price>0 and t2.stock>0 and t2._image_name!="") as cnt_sort_goods_k_on,
                       (select count(t2.pkey) from view_goods t2 where t2.parent=t1.pkey and t2.k=1) as cnt_sort_goods_k_all
                       from obj_site_goods t1
                       where t1.clss in (1,10) and t1.obj_name not like "%Осень%" and t1.obj_name not like "%Зима%" and t1.obj_name not like "%Весна%" and t1.obj_name not like "%Лето%" order by t1.pkey') ;
         $recs2=group_by_field('obj_name',$recs) ;

         ?><table class="left debug"><?
         foreach($recs2 as $name=>$recs_sections)
         {
             ?><tr><th colspan=4><h2><?echo $name?></h2></th></tr><tr><th>Раздел 1С</th><th>Мужское</th><th>Женское</th><th>Детское</th></tr><?
             if (sizeof($recs_sections)) foreach($recs_sections as $rec_sec)
             {   ?><tr><td><?echo $rec_sec['parent_name']?><br>id.<?echo $rec_sec['id']?></td><?
                 ?><td><?echo $category_system->tree[$rec_sec['type_m']]->name ;
                         $this->show_text_sort_info($rec_sec['cnt_sort_m_off'],$rec_sec['cnt_sort_m_on']) ;
                         $this->show_text_goods_info($rec_sec['type_m'],$rec_sec['cnt_sort_goods_m_on'],$rec_sec['cnt_sort_goods_m_all']) ;
                       ?></td><?
                 ?><td><?echo $category_system->tree[$rec_sec['type_w']]->name ;
                        /*if (isset($category_system->tree[$rec_sec['type_w']]) and $category_system->tree[$rec_sec['type_w']]->get_parent_obj(1)!=774) echo '<div class=red>error</div>' ;*/
                        $this->show_text_sort_info($rec_sec['cnt_sort_w_off'],$rec_sec['cnt_sort_w_on']) ;
                        $this->show_text_goods_info($rec_sec['type_w'],$rec_sec['cnt_sort_goods_w_on'],$rec_sec['cnt_sort_goods_w_all']) ;
                           //if ($rec_sec['id']==4597) damp_array($rec_sec) ;
                 ?></td><?
                 ?><td><?echo $category_system->tree[$rec_sec['type_k']]->name ;
                       /*if (isset($category_system->tree[$rec_sec['type_k']]) and $category_system->tree[$rec_sec['type_k']]->get_parent_obj(1)!=775) echo '<div class=red>error</div>' ;*/
                       $this->show_text_sort_info($rec_sec['cnt_sort_k_off'],$rec_sec['cnt_sort_k_on']) ;
                       $this->show_text_goods_info($rec_sec['type_k'],$rec_sec['cnt_sort_goods_k_on'],$rec_sec['cnt_sort_goods_k_all']) ;
                 ?></td><?

                 ?></tr><?
             }
             ?><tr><td></td><td><button class="v2" cmd="get_panel_select_type_goods" script="mod/category" no_send_form=1 cat="1" sect_name="<?echo $name?>">Задать тип товара</button></td>
                                         <td><button class="v2" cmd="get_panel_select_type_goods" script="mod/category" no_send_form=1 cat="2" sect_name="<?echo $name?>">Задать тип товара</button></td>
                                         <td><button class="v2" cmd="get_panel_select_type_goods" script="mod/category" no_send_form=1 cat="3" sect_name="<?echo $name?>">Задать тип товара</button></td>
                             </tr><?
         }
         ?></table><?



         if (sizeof($recs) and 0) foreach($recs as $rec_section) if ($rec_section['cnt_active_goods'])
         {   $section_name=$rec_section['obj_name'] ;
             $section_id=$rec_section['pkey'] ;
             if ($section_name=='Рубашка') $section_name='Рубашки' ;
             if ($section_name=='Майка') $section_name='Майки' ;
             if ($section_name=='Куртка') $section_name='Куртки' ;
             if ($section_name=='Жилеты') $section_name='Жилет' ;
             if ($section_name=='Пальто') $section_name='Пальто и Плащи' ;
             if ($section_name=='Костюм') $section_name='Костюмы' ;
             if ($section_name=='Галстук') $section_name='Галстуки' ;
             if ($section_name=='Набор трусов') $section_name='Трусы' ;
             if ($section_name=='Сумка') $section_name='Сумки' ;
             if ($section_name=='Визитница') $section_name='Визитницы' ;
             if ($section_name=='Шляпа') $section_name='Шляпы' ;
             if ($section_name=='Чехол IPad') $section_name='Чехлы iPad и IPhone' ;
             if ($section_name=='Портмоне') $section_name='Кошельки и портмоне' ;
             if ($section_name=='Пиджак') $section_name='Пиджаки' ;
             if ($section_name=='Шарф') $section_name='Шарфы' ;
             if ($section_name=='Ремень') $section_name='Ремни' ;
             if (!isset($arr_categor[$section_name])) echo $section_name.'<br>' ;
             else update_rec_in_table('obj_site_goods',array('type_m'=>$arr_categor[$section_name]),'pkey='.$section_id,array('debug'=>2)) ;

         }
     }


     function show_section_info_error()
     {
         global $category_system ;

         $recs=execSQL('select t1.pkey as id,t1.obj_name,type_m,type_w,type_k,
                       (select (t2.obj_name) from obj_site_goods t2 where t2.pkey=t1.parent) as parent_name,
                       (select count(t2.pkey) from obj_site_1c t2 where t2.section_id=t1.pkey and t2.categor=1 and enabled=1 and price>0 and stock>0) as cnt_sort_m_on,
                       (select count(t2.pkey) from obj_site_1c t2 where t2.section_id=t1.pkey and t2.categor=1 and (enabled=0 or price=0 or stock=0)) as cnt_sort_m_off,
                       (select count(t2.pkey) from obj_site_1c t2 where t2.section_id=t1.pkey and t2.categor=2 and enabled=1 and price>0 and stock>0) as cnt_sort_w_on,
                       (select count(t2.pkey) from obj_site_1c t2 where t2.section_id=t1.pkey and t2.categor=2 and (enabled=0 or price=0 or stock=0)) as cnt_sort_w_off,
                       (select count(t2.pkey) from obj_site_1c t2 where t2.section_id=t1.pkey and t2.categor=3 and enabled=1 and price>0 and stock>0) as cnt_sort_k_on,
                       (select count(t2.pkey) from obj_site_1c t2 where t2.section_id=t1.pkey and t2.categor=3 and (enabled=0 or price=0 or stock=0)) as cnt_sort_k_off,
                       (select count(t2.pkey) from view_goods t2 where t2.parent=t1.pkey and t2.m=1 and t2.enabled=1 and t2.price>0 and t2.stock>0 and t2._image_name!="") as cnt_sort_goods_m_on,
                       (select count(t2.pkey) from view_goods t2 where t2.parent=t1.pkey and t2.m=1) as cnt_sort_goods_m_all,
                       (select count(t2.pkey) from view_goods t2 where t2.parent=t1.pkey and t2.w=1 and t2.enabled=1 and t2.price>0 and t2.stock>0 and t2._image_name!="") as cnt_sort_goods_w_on,
                       (select count(t2.pkey) from view_goods t2 where t2.parent=t1.pkey and t2.w=1) as cnt_sort_goods_w_all,
                       (select count(t2.pkey) from view_goods t2 where t2.parent=t1.pkey and t2.k=1 and t2.enabled=1 and t2.price>0 and t2.stock>0 and t2._image_name!="") as cnt_sort_goods_k_on,
                       (select count(t2.pkey) from view_goods t2 where t2.parent=t1.pkey and t2.k=1) as cnt_sort_goods_k_all
                       from obj_site_goods t1
                       where clss in (1,10) and obj_name not like "%Осень%" and obj_name not like "%Зима%" and obj_name not like "%Весна%" and obj_name not like "%Лето%" order by pkey') ;
         $recs2=group_by_field('obj_name',$recs) ;

         ?><table class="left debug"><?
         foreach($recs2 as $name=>$recs_sections)
         {
             $err=0 ;
             ob_start() ;
             if (sizeof($recs_sections)) foreach($recs_sections as $rec_sec)
             {   ?><tr><td><?echo $rec_sec['parent_name'].'<br><a target=_blank href="http://angelo-store.ru/admin/fra_viewer.php?table_code=goods&clss=10&reffer='.$rec_sec['id'].'.136">id.'.$rec_sec['id']?></a></td><?
                 ?><td><?echo $category_system->tree[$rec_sec['type_m']]->name ;
                         $this->show_text_sort_info($rec_sec['cnt_sort_m_off'],$rec_sec['cnt_sort_m_on']) ;
                         $err+=$this->show_text_goods_info($rec_sec['type_m'],$rec_sec['cnt_sort_goods_m_on'],$rec_sec['cnt_sort_goods_m_all']) ;
                          if ($rec_sec['id']==3936)
                          { $recs=execSQL('select t2.pkey,t3.obj_name as type_name,t2.obj_name,t2.art,t2.type as type_id from view_goods t2 left join obj_site_category t3 on t3.pkey=t2.type where t2.parent=3936 and t2.m=1 and t2.enabled=1 and t2.price>0 and t2.stock>0 and t2._image_name!=""') ;
                            print_2x_arr($recs) ;
                          }

                       ?></td><?
                 ?><td><?echo $category_system->tree[$rec_sec['type_w']]->name ;
                        /*if (isset($category_system->tree[$rec_sec['type_w']]) and $category_system->tree[$rec_sec['type_w']]->get_parent_obj(1)!=774) echo '<div class=red>error</div>' ;*/
                        $this->show_text_sort_info($rec_sec['cnt_sort_w_off'],$rec_sec['cnt_sort_w_on']) ;
                        $err+=$this->show_text_goods_info($rec_sec['type_w'],$rec_sec['cnt_sort_goods_w_on'],$rec_sec['cnt_sort_goods_w_all']) ;
                 ?></td><?
                 ?><td><?echo $category_system->tree[$rec_sec['type_k']]->name ;
                       /*if (isset($category_system->tree[$rec_sec['type_k']]) and $category_system->tree[$rec_sec['type_k']]->get_parent_obj(1)!=775) echo '<div class=red>error</div>' ;*/
                       $this->show_text_sort_info($rec_sec['cnt_sort_k_off'],$rec_sec['cnt_sort_k_on']) ;
                       $err+=$this->show_text_goods_info($rec_sec['type_k'],$rec_sec['cnt_sort_goods_k_on'],$rec_sec['cnt_sort_goods_k_all']) ;
                 ?></td><?

                 ?></tr><?
             }
             ?><tr><td></td><td><button class="v2" cmd="get_panel_select_type_goods" script="mod/category" no_send_form=1 cat="1" mode='sect_1c' sect_name="<?echo $name?>">Задать тип товара</button></td>
                            <td><button class="v2" cmd="get_panel_select_type_goods" script="mod/category" no_send_form=1 cat="2" mode='sect_1c' sect_name="<?echo $name?>">Задать тип товара</button></td>
                            <td><button class="v2" cmd="get_panel_select_type_goods" script="mod/category" no_send_form=1 cat="3" mode='sect_1c' sect_name="<?echo $name?>">Задать тип товара</button></td>
                </tr><?
             $text=ob_get_clean() ;
             if ($err)
             { ?><tr><th colspan=4><h2><?echo $name?></h2></th></tr><tr><th>Раздел 1С</th><th>Мужское</th><th>Женское</th><th>Детское</th></tr><?
               echo $text ;
             }

         }
         ?></table><?


     }

   function set_goods_type_by_section()
   {
     ?><h1>Задание типа товара на основе типа раздела</h1>
      <button cmd="set_goods_type_by_section_apply" action="only_space_type">Только для товаров с пустым type</button>
      <button cmd="set_goods_type_by_section_apply" action="all_goods">Для всех товаров</button>
     <?
   }

   function set_goods_type_by_section_apply()
   {   damp_array($_POST,1,-1) ;
       ?><h1>Задание типа товара на основе типа раздела</h1><?
       if ($_POST['action']=='all_goods') execSQL_update('update obj_site_goods set type=0',1) ;
       if ($_POST['action']=='all_goods') execSQL_update('update obj_site_1c set type=0') ;
       $recs=execSQL('select pkey,obj_name,type_m,type_w,type_k from obj_site_goods where clss=10',1) ;
       $usl_type='' ; $arr_goods_ids=array() ;   $arr_sort_ids=array() ;
       if ($_POST['action']=='only_space_type')
       {  $arr_goods_ids=execSQL_line('select pkey from obj_site_goods where (type=0 or type is null) and clss=200') ;
          $arr_sort_ids=execSQL_line('select pkey from obj_site_1c where (type=0 or type is null)') ;
          $usl_type=' and (type=0 or type is null)' ;
       }


       if (sizeof($recs)) foreach($recs as $rec)
       {  if ($rec['type_m']) execSQL_update('update obj_site_goods set type='.$rec['type_m'].' where parent='.$rec['pkey'].' and m=1'.$usl_type) ;
          if ($rec['type_w']) execSQL_update('update obj_site_goods set type='.$rec['type_w'].' where parent='.$rec['pkey'].' and w=1'.$usl_type) ;
          if ($rec['type_k']) execSQL_update('update obj_site_goods set type='.$rec['type_k'].' where parent='.$rec['pkey'].' and k=1'.$usl_type) ;
          if ($rec['type_m']) execSQL_update('update obj_site_1c set type='.$rec['type_m'].' where section_id='.$rec['pkey'].' and categor=1'.$usl_type) ;
          if ($rec['type_w']) execSQL_update('update obj_site_1c set type='.$rec['type_w'].' where section_id='.$rec['pkey'].' and categor=2'.$usl_type) ;
          if ($rec['type_k']) execSQL_update('update obj_site_1c set type='.$rec['type_k'].' where section_id='.$rec['pkey'].' and categor=3'.$usl_type) ;
          echo $rec['obj_name'].'<br>' ;

       }
       // второй этап - скореектировать типы согласно названию
       global $category_system ;

        $rec_types_m=execSQL_row('select obj_name,type from obj_site_cat_1c where parent=2 and type>0') ;
        $rec_types_w=execSQL_row('select obj_name,type from obj_site_cat_1c where parent=298 and type>0') ;
        $rec_types_k=execSQL_row('select obj_name,type from obj_site_cat_1c where parent=594 and type>0') ;

        //print_r($rec_types_m) ;
        $sql='select pkey,obj_name,type,m,w,k from obj_site_goods where clss=200' ;
        if (sizeof($arr_goods_ids)) $sql.=' and pkey in ('.implode(',',$arr_goods_ids).')' ;
        $recs=execSQL($sql,0,1) ;
        $res=array() ;
        ?><table><?
        if (sizeof($recs)) foreach($recs as $rec)
        {  $arr=explode(',',$rec['obj_name']) ;
           $type_name=$arr[0] ;
           $type_by_name=$rec['type'] ;
           if ($rec['m'])  $type_by_name=$rec_types_m[$type_name] ;
           if ($rec['w'])  $type_by_name=$rec_types_w[$type_name] ;
           if ($rec['k'])  $type_by_name=$rec_types_k[$type_name] ;
           if ($type_by_name!=$rec['type'])
           { ?><tr><td><?echo $rec['pkey']?><td><?echo $rec['obj_name']?></td><td><?echo $category_system->tree[$rec['type']]->name?></td><td> => </td><td><?echo $category_system->tree[$type_by_name]->name?></td><td><?echo $type_by_name?></td><? ;
             ?><td><?if ($type_by_name)
                    { $res_upd=array() ;
                      $res_upd['исправлено товаров']=execSQL_update('update obj_site_goods set type='.$type_by_name.' where pkey='.$rec['pkey']) ;
                      $res_upd['исправлено сортаментов']=execSQL_update('update obj_site_1c set type='.$type_by_name.' where goods_id='.$rec['pkey']) ;
                      damp_array($res_upd,1,-1) ;
                    }
              ?><td><?
             ?></tr><?
           }

            $res[$arr[0]][$category_system->tree[$rec['type']]->name]++ ;
        }
        ?></table><?
        damp_array($res) ;







   }


   function prepare_sql_req1($categor,$type_name)
   {
      $sql='select t4.pkey as section2_id,t4.obj_name as section2_name,
            t3.pkey as section_id,t3.obj_name as section_name,
            t1.pkey,t1.obj_name,t1.categor,t1.art,t1.code, t1.type as sort_type_id,
            t2.obj_name as sort_type_name,
            t3.'.$type_name.' as section_type_id,
            t5.obj_name as section_type_name
            from obj_site_1c t1
            left join obj_site_category t2 on t2.pkey=t1.type
            left join obj_site_goods t3 on t3.pkey=t1.section_id
            left join obj_site_goods t4 on t4.pkey=t3.parent
            left join obj_site_category t5 on t5.pkey=t3.'.$type_name.'
            where t1.categor='.$categor.' and t1.enabled=1 and t1.stock>0 and t1.price>0 and t1.type!=(select t2.'.$type_name.' from obj_site_goods t2 where t2.pkey=t1.section_id)' ;
       return($sql) ;
   }

   function check_type_sort_to_section()
   {   ?><p>Показаны сортаменты, у которых тип не соответствует типу раздела, в котором находиться сортамент</p><?
       ?><h2>Сортамент мужской</h2><?
       $res=execSQL($this->prepare_sql_req1(1,'type_m'),0,1) ;
       print_2x_arr($res) ;
       ?><h2>Сортамент женский</h2><?
       $res=execSQL($this->prepare_sql_req1(2,'type_w'),0,1) ;
       print_2x_arr($res) ;
       ?><h2>Сортамент детский</h2><?
       $res=execSQL($this->prepare_sql_req1(3,'type_k'),0,1) ;
       print_2x_arr($res) ;
   }

   function check_type_goods_to_section()
   {   ?><p>Показаны товары, у которых тип не соответствует типу раздела, в котором находиться товар</p><?
       ?><h2>Товар мужской</h2><?
       $res=execSQL($this->prepare_sql_req2('m=1','type_m'),0,1) ;
       print_2x_arr($res) ;
       ?><h2>Товар женский</h2><?
       $res=execSQL($this->prepare_sql_req2('w=1','type_w'),0,1) ;
       print_2x_arr($res) ;
       ?><h2>Товар детский</h2><?
       $res=execSQL($this->prepare_sql_req2('k=1','type_k'),0,1) ;
       print_2x_arr($res) ;
       //$stats['err_type_in_sort']=sizeof(execSQL('select goods_id,count(type) as cnt_type from obj_site_1c where enabled=1 and stock>0 and price>0 group by type having cnt_type>1')) ;
       //damp_array($stats,1,-1) ;
   }

    function prepare_sql_req2($categor,$type_name)
    {
       $sql='select t4.pkey as section2_id,t3.pkey as section_id,t1.pkey,t4.obj_name as section2_name,t3.obj_name as section_name,
             t1.obj_name,t1.art,t1._image_name,t1.stock,t1.price,t1.m,t1.w,t1.k,t1.type as goods_type_id,
             t2.obj_name as goods_type_name,
             t3.'.$type_name.' as section_type_id,
             t5.obj_name as section_type_name
             from obj_site_goods t1
             left join obj_site_category t2 on t2.pkey=t1.type
             left join obj_site_goods t3 on t3.pkey=t1.parent
             left join obj_site_goods t4 on t4.pkey=t3.parent
             left join obj_site_category t5 on t5.pkey=t3.'.$type_name.'
             where t1.'.$categor.' and t1.enabled=1 and t1.stock>0 and t1.price>0 and t1._image_name!="" and t1.type!=(select t2.'.$type_name.' from obj_site_goods t2 where t2.pkey=t1.parent)' ;
        return($sql) ;
    }

   function check_type_goods_to_name()
   {   ?><p>Показаны товары, у которых тип не соответствует названию товара</p><?
       ?><h2>Товар мужской</h2><?
       $res=execSQL($this->prepare_sql_req4('m=1',773),0,1) ;
       print_2x_arr($res) ;
       ?><h2>Товар женский</h2><?
       $res=execSQL($this->prepare_sql_req4('w=1',774),0,1) ;
       print_2x_arr($res) ;
       ?><h2>Товар детский</h2><?
       $res=execSQL($this->prepare_sql_req4('k=1',775),0,1) ;
       print_2x_arr($res) ;
       //$stats['err_type_in_sort']=sizeof(execSQL('select goods_id,count(type) as cnt_type from obj_site_1c where enabled=1 and stock>0 and price>0 group by type having cnt_type>1')) ;
       //damp_array($stats,1,-1) ;
   }

    function prepare_sql_req4($categor,$cat_root)
    {  global $category_system  ;
       $sql='select t1.obj_name,t1.art,t1._image_name,t1.stock,t1.price,t1.m,t1.w,t1.k,t1.type as goods_type_id,
             t2.obj_name as section_type_name,
             t2.goods_name as section_goods_name,
             t1.obj_name as goods_name
             from obj_site_goods t1
             left join obj_site_category t2 on t2.pkey=t1.type and t2.parent in ('.$category_system->tree[$cat_root]->get_list_child().')
             where t1.'.$categor.' and t1.enabled=1 and t1.stock>0 and t1.price>0 and t1._image_name!="" and not t1.obj_name like concat (t2.goods_name,"%")' ;
        return($sql) ;
    }

   function check_type_goods_not_exist()
   {   ?><p>Показаны товары, c несуществующим типом товара</p><?
       ?><h2>Товар мужской</h2><?
       $res=execSQL($this->prepare_sql_req3('m','type_m'),0,1) ;
       print_2x_arr($res) ;
       ?><h2>Товар женский</h2><?
       $res=execSQL($this->prepare_sql_req3('w','type_w'),0,1) ;
       print_2x_arr($res) ;
       ?><h2>Товар детский</h2><?
       $res=execSQL($this->prepare_sql_req3('k','type_k'),0,1) ;
       print_2x_arr($res) ;
       //$stats['err_type_in_sort']=sizeof(execSQL('select goods_id,count(type) as cnt_type from obj_site_1c where enabled=1 and stock>0 and price>0 group by type having cnt_type>1')) ;
       //damp_array($stats,1,-1) ;
   }

    function prepare_sql_req3($categor)
    {  global $category_system ;
       $types_ids='' ;
       switch($categor)
       { case 'm': $types_ids=$category_system->tree[773]->get_list_child() ; break ;
         case 'w': $types_ids=$category_system->tree[774]->get_list_child() ; break ;
         case 'k': $types_ids=$category_system->tree[775]->get_list_child() ; break ;
       }
       $sql='select t4.pkey as section2_id,t4.obj_name as section2_name,
             t3.pkey as section_id,t3.obj_name as section_name,
             t1.pkey,t1.art,t1.obj_name,t1._image_name,t1.stock,t1.price,t1.m,t1.w,t1.k,t1.type as goods_type_id,
             t5.obj_name as section_type_m,
             t6.obj_name as section_type_w,
             t7.obj_name as section_type_k
             from obj_site_goods t1
             left join obj_site_goods t3 on t3.pkey=t1.parent
             left join obj_site_goods t4 on t4.pkey=t3.parent
             left join obj_site_category t5 on t5.pkey=t3.type_m
             left join obj_site_category t6 on t6.pkey=t3.type_w
             left join obj_site_category t7 on t7.pkey=t3.type_k
             where t1.'.$categor.'=1 and t1.enabled=1 and t1.stock>0 and t1.price>0 and t1._image_name!="" and t1.type not in ('.$types_ids.')' ;
        return($sql) ;
    }

    function show_section_type_not_exist()
    {  global $category_system ;
       $types_ids_m=$category_system->tree[773]->get_list_child() ;
       $types_ids_w=$category_system->tree[774]->get_list_child() ;
       $types_ids_k=$category_system->tree[775]->get_list_child() ;
       $sql='select t2.pkey as section_id,t2.obj_name as section_name,
             t1.pkey,t1.obj_name,
             t1.type_m,t3.obj_name as type_m_name,
             t1.type_w,t4.obj_name as type_w_name,
             t1.type_k,t5.obj_name as type_k_name
             from obj_site_goods t1
             left join obj_site_goods t2 on t2.pkey=t1.parent
             left join obj_site_category t3 on t3.pkey=t1.type_m
             left join obj_site_category t4 on t4.pkey=t1.type_w
             left join obj_site_category t5 on t5.pkey=t1.type_k
             where t1.clss=10 and ((t1.type_m>0 and t1.type_m not in ('.$types_ids_m.')) or (t1.type_w>0 and t1.type_w not in ('.$types_ids_w.')) or (t1.type_k>0 and t1.type_k not in ('.$types_ids_k.')))' ;
       $res=execSQL($sql,0,1) ;
       print_2x_arr($res) ;
       if (!sizeof($res)) echo 'Все разделы привязаны с существующим типам товаров' ;

    }

    function view_cnt_goods_by_sezon()
    {
        ?><h1>Число выводимых товаров по сезонам общее</h1><?
        $recs=execSQL('select sezon,count(pkey) from view_goods group by sezon order by sezon',0,1) ;
        print_2x_arr($recs) ;
        ?><h1>Число выводимых товаров по сезонам мужские</h1><?
        $recs=execSQL('select sezon,count(pkey) from view_goods where m=1 group by sezon order by sezon',0,1) ;
        print_2x_arr($recs) ;
        ?><h1>Число выводимых товаров по сезонам женские</h1><?
        $recs=execSQL('select sezon,count(pkey) from view_goods where w=1 group by sezon order by sezon',0,1) ;
        print_2x_arr($recs) ;
        ?><h1>Число выводимых товаров по сезонам детские</h1><?
        $recs=execSQL('select sezon,count(pkey) from view_goods where k=1 group by sezon order by sezon',0,1) ;
        print_2x_arr($recs) ;
    }

    function view_cnt_photo_by_sezon()
    {
        ?><h1>Число фото товара по сезонам общее</h1><?
        $recs=execSQL('select t1.sezon,sum((select count(pkey) from obj_site_goods_image t2 where t2.parent=t1.pkey and t2.enabled=1)) as cnt_photo from obj_site_goods t1 where t1._image_name!="" group by t1.sezon order by t1.sezon',0,1) ;
        print_2x_arr($recs) ;
        ?><h1>Число фото товара по сезонам мужские</h1><?
        $recs=execSQL('select t1.sezon,sum((select count(pkey) from obj_site_goods_image t2 where t2.parent=t1.pkey and t2.enabled=1)) as cnt_photo from obj_site_goods t1 where t1._image_name!="" and t1.m=1 group by t1.sezon order by t1.sezon',0,1) ;
        print_2x_arr($recs) ;
        ?><h1>Число фото товара по сезонам женские</h1><?
        $recs=execSQL('select t1.sezon,sum((select count(pkey) from obj_site_goods_image t2 where t2.parent=t1.pkey and t2.enabled=1)) as cnt_photo from obj_site_goods t1 where t1._image_name!="" and t1.w=1 group by t1.sezon order by t1.sezon',0,1) ;
        print_2x_arr($recs) ;
        ?><h1>Число фото товара по сезонам детские</h1><?
        $recs=execSQL('select t1.sezon,sum((select count(pkey) from obj_site_goods_image t2 where t2.parent=t1.pkey and t2.enabled=1)) as cnt_photo from obj_site_goods t1 where t1._image_name!="" and t1.k=1 group by t1.sezon order by t1.sezon',0,1) ;
        print_2x_arr($recs) ;
    }


}

?>