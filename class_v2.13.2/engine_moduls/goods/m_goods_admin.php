<?php
$__functions['init'][]		='_goods_admin_vars' ;
$__functions['install'][]	='_goods_install_modul' ;
$__functions['boot_admin'][]='_goods_admin_boot' ;

include('clss_2.php') ;
include('clss_10.php') ;
include('clss_222.php') ;

function _goods_admin_vars() //
{
    global $TM_goods ;
    ext_panel_register(array('frame'=>array('table_code'=>'goods','is_root'=>1),'menu_item'=>array('name'=>'Операции','script'=>'mod/goods/i_goods_root_operation_actions.php','cmd'=>'panel_goods_root_operation_actions'))) ;


	//-----------------------------------------------------------------------------------------------------------------------------
	// описание переменных модуля
	//-----------------------------------------------------------------------------------------------------------------------------
    global $TM_goods_image ;
    $_SESSION['arr_filter'][$TM_goods][] = array('name'=>'нет') ;
    $_SESSION['arr_filter'][$TM_goods][] = array('name'=>'Отключенные','sql'=>'where t.enabled=0') ;
    $_SESSION['arr_filter'][$TM_goods][] = array('name'=>'Цена=0','sql'=>'where t.price=0 and t.clss=8') ;
    $_SESSION['arr_filter'][$TM_goods][] = array('name'=>'Наценка=0','sql'=>'where t.koof=0 and t.clss=8') ;
    $_SESSION['arr_filter'][$TM_goods][] = array('name'=>'Выгружаемые в Yandex','sql'=>'where t.yandex>0') ;
    $_SESSION['arr_filter'][$TM_goods][] = array('name'=>'Новинки','sql'=>' where t.new>0') ;
    $_SESSION['arr_filter'][$TM_goods][] = array('name'=>'Хиты продаж','sql'=>'where t.top>0') ;
    $_SESSION['arr_filter'][$TM_goods][] = array('name'=>'Скидка не действует','sql'=>'where t.sales_type=2') ;
    $_SESSION['arr_filter'][$TM_goods][] = array('name'=>'Скидка в процентах','sql'=>'where t.sales_type=3') ;
    $_SESSION['arr_filter'][$TM_goods][] = array('name'=>'Скидка на сумму','sql'=>'where t.sales_type=4') ;
    $_SESSION['arr_filter'][$TM_goods][] = array('name'=>'Без фото','sql'=>'where not exists (select pkey from '.$TM_goods_image.' t2 where t2.parent=t.pkey) and t.clss=200') ;

    $_SESSION['arr_find_on_field'][$TM_goods][1]  	= array('obj_name') ;
    $_SESSION['arr_find_on_field'][$TM_goods][10] 	= array('obj_name','manual') ;
    $_SESSION['arr_find_on_field'][$TM_goods][2] 	= array('pkey','obj_name','art','manual') ;
    $_SESSION['arr_find_on_field'][$TM_goods][200] 	= array('pkey','obj_name','art','manual') ;

	//global $arr_tree_clss_names ; 		$arr_tree_clss_names[200]=array('<span class=black>','art','</span> - ','obj_name') ; // должно описываться в описании класса

    // скидка клиента отменена, так как используется по умолчается
    $_SESSION['list_type_sales']=array('2'=>'Скидка запрещена','3'=>'Назначенная скидка, %','4'=>'Назначенная скидка, сумма') ;

	//-----------------------------------------------------------------------------------------------------------------------------
	// описание классов модуля
	//-----------------------------------------------------------------------------------------------------------------------------
    // 2 - товар
    $_SESSION['descr_clss'][2]['name']='Товар' ;
	$_SESSION['descr_clss'][2]['parent']=0 ;
	$_SESSION['descr_clss'][2]['parent_to']=array(3,5,6) ;
	$_SESSION['descr_clss'][2]['fields']=array('art'=>'varchar(255)','price'=>'float(10,2)','koof'=>'float(10,2)','val'=>'int(11)','stock'=>'int(11)','sales_type'=>'int(11)','sales_value'=>'float(10,2)','intro'=>'text','manual'=>'text','top'=>'int(11)','new'=>'int(11)','yandex'=>'int(1)','rambler'=>'int(1)','url_name'=>'varchar(255)') ;
    $_SESSION['descr_clss'][2]['fields']['price_site']='float(10,2)' ; // вычисляемое поле для сортировки на сайте товаров по цене
    $_SESSION['descr_clss'][2]['fields']['price_on']='int(1)' ; // флаг, что цена не указана - для размещение товаров без цены в конце списка при сортировке по цене
    //$_SESSION['descr_clss'][2]['fields']['gender']='int(11)' ;

	//$_SESSION['descr_clss'][2]['fields']['brand']='int(11)';
    //$_SESSION['descr_clss'][2]['on_create_event']='clss_2_on_create_event' ;
    $_SESSION['descr_clss'][2]['on_change_event']['price']='clss_2_on_change_event_price' ;
    $_SESSION['descr_clss'][2]['SEO_tab']=1 ;

    $_SESSION['descr_clss'][2]['menu'][]=array("name" => "Свойства",				"type" => 'prop') ;
    $_SESSION['descr_clss'][2]['menu'][]=array("name" => "Добавить",				"type" => 'structure') ;


	$_SESSION['descr_clss'][2]['list']['field']['pkey']			='Код' ;
	$_SESSION['descr_clss'][2]['list']['field']['enabled']		='Сост.' ;
	$_SESSION['descr_clss'][2]['list']['field']['indx']			='Позиция' ;
	$_SESSION['descr_clss'][2]['list']['field']['new']			=array('title'=>'Новинка','class'=>'small') ;
	$_SESSION['descr_clss'][2]['list']['field']['top']			=array('title'=>'Хит','class'=>'small') ;
	$_SESSION['descr_clss'][2]['list']['field']['art']			='Артикул' ;
    //$_SESSION['descr_clss'][2]['list']['field']['type']		    ='Тип товара' ;
    //$_SESSION['descr_clss'][2]['list']['field']['gender']		=array('title'=>'Бренд','indx_select'=>'IL_proizvoditeli') ;
	$_SESSION['descr_clss'][2]['list']['field']['obj_name']		=array('title'=>'Наименование') ;
	$_SESSION['descr_clss'][2]['list']['field']['price']		=array('title'=>'Цена') ;
	$_SESSION['descr_clss'][2]['list']['field']['koof']			=array('title'=>'Наценка','class'=>'small') ;
	//$_SESSION['descr_clss'][2]['list']['field']['url_name']		=array('title'=>'url') ;
	//$_SESSION['descr_clss'][2]['list']['field']['val']			=array('title'=>'Валюта','indx_select'=>'VL_arr') ;
	//$_SESSION['descr_clss'][2]['list']['field'][]				=array('title'=>'Цены','edit_element'=>'clss_all_prices','td_class'=>'left') ;
  	//$_SESSION['descr_clss'][2]['list']['field']['yandex']		=array('title'=>'ya') ;
 	//$_SESSION['descr_clss'][2]['list']['field']['rambler']		=array('title'=>'ra') ;
 	//$_SESSION['descr_clss'][2]['list']['field']['sales_type']=array('title'=>'Тип скидки','indx_select'=>'list_type_sales') ;
	//$_SESSION['descr_clss'][2]['list']['field']['sales_value']	=array('title'=>'Размер скидки','class'=>'small') ;


 	$_SESSION['descr_clss'][2]['details']['field']['pkey']=array('title'=>'Код') ;
 	$_SESSION['descr_clss'][2]['details']['field']['parent']=array('title'=>'Подраздел') ;
 	$_SESSION['descr_clss'][2]['details']['field']['enabled']=array('title'=>'Сост.') ;
 	$_SESSION['descr_clss'][2]['details']['field']['indx']=array('title'=>'Позиция') ;
	$_SESSION['descr_clss'][2]['details']['field']['new']=array('title'=>'Новинка') ;
	$_SESSION['descr_clss'][2]['details']['field']['top']=array('title'=>'Хит') ;
  	$_SESSION['descr_clss'][2]['details']['field']['art']=array('title'=>'Артикул') ;
	$_SESSION['descr_clss'][2]['details']['field']['obj_name']=array('title'=>'Наименование') ;
	$_SESSION['descr_clss'][2]['details']['field']['price']=array('title'=>'Цена') ;
	$_SESSION['descr_clss'][2]['details']['field']['koof']=array('title'=>'Наценка') ;
	//$_SESSION['descr_clss'][2]['details']['field']['val']=array('title'=>'Валюта','indx_select'=>'ue_arr_code'/*,'indx_select_title'=>'name'*/) ;
 	//$_SESSION['descr_clss'][2]['details']['field']['sales_type']=array('title'=>'Тип скидки','indx_select'=>'list_type_sales') ;
    //$_SESSION['descr_clss'][2]['details']['field']['sales_value']=array('title'=>'Размер скидки','edit_element'=>1,'size'=>6) ;
	//$_SESSION['descr_clss'][2]['details']['field']['clss_all_prices']=array('title'=>'Цены','class'=>'left') ;
  	//$_SESSION['descr_clss'][2]['details']['field']['stock']=array('title'=>'Склад','edit_element'=>1,'size'=>1) ;
  	//$_SESSION['descr_clss'][2]['details']['field']['yandex']=array('title'=>'ya','edit_element'=>3) ;
 	//$_SESSION['descr_clss'][2]['details']['field']['rambler']=array('title'=>'ra','edit_element'=>3) ;
 	//$_SESSION['descr_clss'][2]['details']['field']['intro_is_html']=array('title'=>'Краткое описание','use_HTML_editor'=>1) ;
 	$_SESSION['descr_clss'][2]['details']['field']['intro']=array('title'=>'Краткое описание','use_HTML_editor'=>1) ;
 	//$_SESSION['descr_clss'][2]['details']['field']['manual_is_html']=array('title'=>'Описание','use_HTML_editor'=>1) ;
 	$_SESSION['descr_clss'][2]['details']['field']['manual']=array('title'=>'Описание','use_HTML_editor'=>1) ;

    // 10 - раздел с описанием
    $_SESSION['descr_clss'][10]=array() ;
    $_SESSION['descr_clss'][10]['name']='Раздел каталога' ;
	$_SESSION['descr_clss'][10]['parent']=1 ;
	$_SESSION['descr_clss'][10]['fields']=array('intro'=>'text','manual'=>'text','href'=>'varchar(255)','url_dir'=>'varchar(255)') ;
	$_SESSION['descr_clss'][10]['fields']['subdomain']='varchar(255)' ;

    $_SESSION['descr_clss'][10]['SEO_tab']=1 ;

    $_SESSION['descr_clss'][10]['menu'][]=array("name" => "Добавить",				"type" => 'structure') ;
    $_SESSION['descr_clss'][10]['menu'][]=array("name" => "Свойства",				"type" => 'prop') ;
    $_SESSION['descr_clss'][10]['menu'][]=array("name" => "Операции",               "cmd" => 'panel_goods_section_operation', 'script'=>'mod/goods/i_goods_section_operation.php','icon'=>'obj_operat.gif') ;
    //$_SESSION['descr_clss'][10]['menu'][]=array("name" => "Поиск",                "cmd" => 'child_search','icon'=>'find_icon.gif') ;

	$_SESSION['descr_clss'][10]['list']['field']['pkey']		='Код' ;
	$_SESSION['descr_clss'][10]['list']['field']['enabled']		='Сост.' ;

	$_SESSION['descr_clss'][10]['list']['field']['indx']		='Позиция'  ;
	$_SESSION['descr_clss'][10]['list']['field']['obj_name']	=array('title'=>'Наименование','edit_element'=>'input','class'=>'big','td_class'=>'left') ;

    //$_SESSION['descr_clss'][10]['list']['field']['subdomain']   =array('title'=>'Поддомен','td_class'=>'left','suff'=>'.'._MAIN_DOMAIN) ;

	$_SESSION['descr_clss'][10]['details']=$_SESSION['descr_clss'][10]['list'] ;
	$_SESSION['descr_clss'][10]['details']['field']['obj_name']	=array('title'=>'Наименование','edit_element'=>'input','class'=>'big','td_class'=>'left') ;
    $_SESSION['descr_clss'][10]['details']['field']['href']		=array('title'=>'Директория','td_class'=>'left') ;
    //$_SESSION['descr_clss'][10]['details']['field']['url_name']	=array('title'=>'Директория','td_class'=>'left') ;
	$_SESSION['descr_clss'][10]['details']['field'][]			=array('title'=>'Путь в каталоге',					'edit_element'=>'clss_patch') ;
	$_SESSION['descr_clss'][10]['details']['field']['intro']	=array('title'=>'Краткое описание','class'=>'small','use_HTML_editor'=>1) ;
	$_SESSION['descr_clss'][10]['details']['field']['manual']	=array('title'=>'Описание',		'class'=>'big','use_HTML_editor'=>1) ;

    //Бренд
    $_SESSION['descr_clss'][62]['name']='Бренд' ;
    $_SESSION['descr_clss'][62]['parent']=10 ;
    $_SESSION['descr_clss'][62]['fields']=array('intro'=>'text') ;
    $_SESSION['descr_clss'][62]['fields']['country_id']=array('type'=>'indx_select','array'=>'IL_country') ;
    //$_SESSION['descr_clss'][62]['fields']['postav_id']=array('type'=>'indx_select','array'=>'IL_postav') ;
    $_SESSION['descr_clss'][62]['fields']['source_url']=array('type'=>'varchar(500)') ;
    $_SESSION['descr_clss'][62]['fields']['source_id']=array('type'=>'int(11)') ;
    $_SESSION['descr_clss'][62]['child_to']=array(1,10) ;
    $_SESSION['descr_clss'][62]['parent_to']=array(3,200,63,64,10,5) ;
    $_SESSION['descr_clss'][62]['on_create_event']=array('script'=>'mod/goods/i_events.php','cmd'=>'clss_62_on_create_event') ;
    $_SESSION['descr_clss'][62]['on_change_event']['country_id']=array('script'=>'mod/goods/i_events.php','cmd'=>'clss_62_on_change_event_country') ;
    $_SESSION['descr_clss'][62]['on_change_event']['enabled']=array('script'=>'mod/goods/i_events.php','cmd'=>'clss_62_on_change_event_enabled') ;

    $_SESSION['descr_clss'][62]['list']['field']['pkey']=array('title'=>'Код') ;
    $_SESSION['descr_clss'][62]['list']['field']['enabled']=array('title'=>'Состояние') ;
    $_SESSION['descr_clss'][62]['list']['field']['indx']=array('title'=>'Позиция') ;
    $_SESSION['descr_clss'][62]['list']['field']['obj_name']=array('title'=>'Наименование') ;
    $_SESSION['descr_clss'][62]['list']['field']['country_id']=array('title'=>'Страна-производитель') ;
    //$_SESSION['descr_clss'][62]['list']['field']['postav_id']=array('title'=>'Поставщик') ;
    //$_SESSION['descr_clss'][62]['list']['field']['_alltype']=array('title'=>'Типы дочерних<br>объектов','td_class'=>'left','view'=>'view_child_types','script'=>'mod/category/i_field_views.php') ;
    $_SESSION['descr_clss'][62]['list']['field']['source_url']=array('title'=>'source_url','td_class'=>'left') ;


    $_SESSION['descr_clss'][62]['details']=$_SESSION['descr_clss'][62]['list'] ;
    $_SESSION['descr_clss'][62]['details']['field']['intro']=array('title'=>'Описание','use_HTML_editor'=>1) ;    

    $_SESSION['descr_clss'][63]['name']='Коллекция' ;
    $_SESSION['descr_clss'][63]['parent']=10 ;
    $_SESSION['descr_clss'][63]['fields']['intro']='text' ;
    $_SESSION['descr_clss'][63]['fields']['manual']='text' ;
    $_SESSION['descr_clss'][63]['fields']['source_url']=array('type'=>'varchar(500)') ;
    //$_SESSION['descr_clss'][63]['fields']['dest']=array('type'=>'multichange','array'=>'IL_dest') ;
    $_SESSION['descr_clss'][63]['parent_to']=array(200,63,11,3,5,10) ;
    $_SESSION['descr_clss'][63]['child_to']=array(201,63) ;
    
    $_SESSION['descr_clss'][63]['list']=$_SESSION['descr_clss'][10]['list'] ;
    $_SESSION['descr_clss'][63]['list']['field']['source_url']=array('title'=>'source_url','td_class'=>'left') ;
        //$_SESSION['descr_clss'][63]['list']['field']['dest']		=array('title'=>'Назначение') ;
    
    $_SESSION['descr_clss'][63]['details']=$_SESSION['descr_clss'][120]['list'] ;
    $_SESSION['descr_clss'][63]['details']['field']['intro']		=array('title'=>'Краткое описание',	'use_HTML_editor'=>1) ;
    $_SESSION['descr_clss'][63]['details']['field']['manual']		=array('title'=>'Описание',			'class'=>'big','use_HTML_editor'=>1) ;

    $_SESSION['descr_clss'][62]['details']=$_SESSION['descr_clss'][62]['list'] ;
    $_SESSION['descr_clss'][62]['details']['field']['intro']=array('title'=>'Описание','use_HTML_editor'=>1) ;

    $_SESSION['descr_clss'][64]['name']='Серия' ;
    $_SESSION['descr_clss'][64]['parent']=10 ;
    $_SESSION['descr_clss'][64]['fields']['intro']='text' ;
    $_SESSION['descr_clss'][64]['fields']['manual']='text' ;
    $_SESSION['descr_clss'][64]['fields']['source_url']=array('type'=>'varchar(500)') ;
    //$_SESSION['descr_clss'][63]['fields']['dest']=array('type'=>'multichange','array'=>'IL_dest') ;
    $_SESSION['descr_clss'][64]['parent_to']=array(200,63,11,3,5,10) ;
    $_SESSION['descr_clss'][64]['child_to']=array(201,63) ;

    $_SESSION['descr_clss'][64]['list']=$_SESSION['descr_clss'][10]['list'] ;
    $_SESSION['descr_clss'][64]['list']['field']['source_url']=array('title'=>'source_url','td_class'=>'left') ;
        //$_SESSION['descr_clss'][63]['list']['field']['dest']		=array('title'=>'Назначение') ;

    $_SESSION['descr_clss'][64]['details']=$_SESSION['descr_clss'][64]['list'] ;
    $_SESSION['descr_clss'][64]['details']['field']['intro']		=array('title'=>'Краткое описание',	'use_HTML_editor'=>1) ;
    $_SESSION['descr_clss'][64]['details']['field']['manual']		=array('title'=>'Описание',			'class'=>'big','use_HTML_editor'=>1) ;


	//----------------------------------------------------------------------------------------------------------------------
	// описание пунктов меню админа сайта
	//-----------------------------------------------------------------------------------------------------------------------

    $_SESSION['menu_admin_site']['Модули']['goods']=array('name'=>'Товары','href'=>'editor_goods.php','icon'=>'menu/goods2.jpg') ;
//	$_SESSION['menu_admin_site']['Модули'][]=array('name'=>'Выгрузки товара','href'=>'viewer_market.php','icon'=>'menu/goods_market.jpg') ;

	//----------------------------------------------------------------------------------------------------------------------
	// описание функций вывода полей объектов класса
	//-----------------------------------------------------------------------------------------------------------------------

	//----------------------------------------------------------------------------------------------------------------------
	// описание меню фрейма объекта
	//-----------------------------------------------------------------------------------------------------------------------

}

function _goods_admin_boot($options)
{
	//create_system_modul_obj('goods',array('no_create_model'=>1)) ;
	$options['no_upload_rec']=1 ;
	$options['tree'][include_space_section]=1 ; 
	create_system_modul_obj('goods',$options) ;
}


function _goods_install_modul($DOT_root)
{   echo '<h2>Инсталируем модуль <strong>Каталог товаров</strong></h2>' ;

    $file_items=array() ;
    //editor_goods.php
    //$file_items['editor_goods.php']['include'][]='_DIR_TO_ENGINE."/admin/c_site.php"' ;
    //$file_items['editor_goods.php']['options']['title']='"Каталог товара"' ;
    //$file_items['editor_goods.php']['options']['use_class']='"c_editor_obj"' ;
    
    $file_items['editor_goods.php']['include'][]				='_DIR_TO_MODULES."/goods/m_goods_frames.php"' ;
    $file_items['editor_goods.php']['options']['use_table_code']='"TM_goods"' ;
    $file_items['editor_goods.php']['options']['title']		    ='"Каталог"' ;
    $file_items['editor_goods_tree.php']['include'][]			='_DIR_TO_MODULES."/goods/m_goods_frames.php"' ;
    $file_items['editor_goods_viewer.php']['include'][]		    ='_DIR_TO_MODULES."/goods/m_goods_frames.php"' ;
    $file_items['editor_goods_ajax.php']['include'][]		    ='_DIR_TO_MODULES."/goods/m_goods_ajax.php"' ;
    

    // viewer_market.php
    $file_items['viewer_market.php']['include'][]				='_DIR_TO_MODULES."/goods/m_market_frames.php"' ;
    $file_items['viewer_market.php']['options']['title']		='"Контроль выгрузки"' ;
    $file_items['viewer_market_tree.php']['include'][]			='_DIR_TO_MODULES."/goods/m_market_frames.php"' ;
    $file_items['viewer_market_viewer.php']['include'][]		='_DIR_TO_MODULES."/goods/m_market_frames.php"' ;
    create_menu_item($file_items) ;

	global $TM_goods ;
   	$pattern=array() ;
	$pattern['table_title']				= 	'Товарная' ;
	$pattern['use_clss']				= 	'10,2' ;
	$pattern['def_recs'][]				=	array ('parent'=>0,'clss'=>10,'enabled'=>1,'obj_name'=>'Товар') ;
   	$pattern['table_name']				= 	$TM_goods ;
	$pattern['table_clss']				= 	101 ;
	$pattern['table_parent']			= 	$DOT_root;
   	$id_DOT_goods=create_table_by_pattern($pattern) ;

	global $TM_goods_image ;
	$pattern=array() ;
	$pattern['table_title']				= 	'Фотки' ;
	$pattern['use_clss']				= 	'3' ;
   	$pattern['table_name']				= 	$TM_goods_image ;
	$pattern['table_clss']				= 	101 ;
	$pattern['table_parent']			= 	$id_DOT_goods;
	$pattern['dir_to_image']			= 	'public/catalog/';
   	create_table_by_pattern($pattern) ;

    SETUP_get_img('/goods/admin_img_menu/goods2.jpg',_PATH_TO_ADMIN.'/img/menu/goods2.jpg') ;

    create_dir('/catalog/') ;
    SETUP_get_file('/goods/system_dir/htaccess.txt','/catalog/.htaccess');
    SETUP_get_file('/goods/system_dir/index.php','/catalog/index.php');
    SETUP_get_file('/goods/system_dir/hits.php','/catalog/hits.php');
    SETUP_get_file('/goods/system_dir/new.php','/catalog/new.php');
    SETUP_get_file('/goods/system_dir/price.php','/catalog/price.php');
    SETUP_get_file('/goods/system_dir/search.php','/catalog/search.php');
    SETUP_get_file('/goods/system_dir/sales.php','/catalog/sales.php');

    SETUP_get_file('/goods/class/c_catalog.php','/class/c_catalog.php');
    $list_templates_file=analize_class_file_to_templates('/class/c_catalog.php') ;
    SETUP_get_templates('/goods/',$list_templates_file) ;

    SETUP_get_file('/goods/class/c_catalog_new.php','/class/c_catalog_new.php');
    $list_templates_file=analize_class_file_to_templates('/class/c_catalog_new.php') ;
    SETUP_get_templates('/goods/',$list_templates_file) ;

    SETUP_get_file('/goods/class/c_catalog_hits.php','/class/c_catalog_hits.php');
    $list_templates_file=analize_class_file_to_templates('/class/c_catalog_hits.php') ;
    SETUP_get_templates('/goods/',$list_templates_file) ;

    SETUP_get_file('/goods/class/c_catalog_search.php','/class/c_catalog_search.php');
    $list_templates_file=analize_class_file_to_templates('/class/c_catalog_search.php') ;
    SETUP_get_templates('/goods/',$list_templates_file) ;

    $id_sect=SETUP_add_section_to_site_setting('Курсы валют') ;
    SETUP_add_rec_to_site_setting($id_sect,'Курс доллара',25,'LS_kurs_dol') ;
    SETUP_add_rec_to_site_setting($id_sect,'Курс евро',40,'LS_kurs_eur') ;
    SETUP_add_rec_to_site_setting($id_sect,'Курс рубля',1,'LS_kurs_rur') ;

    // создавем вьювер товавров
    $sql='DROP VIEW view_goods_all' ;
    execSQL_update($sql,array('debug'=>2)) ;
    $sql='create view view_goods_all as
                 select  t1.*
                 from obj_site_goods t1
                 where t1.clss=200 
        ' ;
    execSQL_update($sql,array('debug'=>2)) ;
}

function _goods_rasdel_operation()
{
	echo 'Операции для раздела товара' ;
}

  // информация по цене товара во всех валютах сайта
  function on_view_show_all_prices(&$rec)
  {  global $goods_system ;
  	 if (!($rec['price']>0)) return ;
     $goods_system->exec_price($rec,array('use_sales'=>1)) ;  // опция use_sales не имеет смысла для поля price
     $show_price=$rec['__price_4'] ;
  	 $text='<div style="white-space: nowrap;">' ;
  	 if (sizeof($_SESSION['VL_arr'])) foreach($_SESSION['VL_arr'] as $i=>$rec_price) $text.=$goods_system->format_price($show_price,array('use_val'=>$i)).'<br>' ;
  	  else $text.=$show_price ;
  	 $text.='</div>' ;
  	 return($text) ;
  }

  function on_view_show_val(&$rec,&$tkey,&$pkey,&$options,&$fname,&$text)
  { if (!$text) return ;
    $ue=($rec['val'])? $_SESSION['VL_arr'][$rec['val']]['ue']:$_SESSION['VL_arr'][$_SESSION['VL_def_DB']]['ue'] ;
    $text.=' '.$ue ;
  }

  function on_view_show_sales_type(&$rec,&$tkey,&$pkey,&$options,&$fname,&$text)
  { $ue=($rec['val'])? $_SESSION['VL_arr'][$rec['val']]['ue']:$_SESSION['VL_arr'][$_SESSION['VL_def_DB']]['ue'] ;
    if ($rec['sales_type']==3) $text.=' %' ;
    if ($rec['sales_type']==4) $text.=' '.$ue ;
  }

  // обновляем значение price_site после изменения price - price_size используется для сортировки по цене, если в админке цена задается в разных валютах, а также для учета
  // скидки на товар
  function clss_2_on_change_event_price($tkey,$pkey,&$fvalue)
  { $rec=execSQL_van('select * from '._DOT($tkey)->table_name.' where pkey='.$pkey) ;
    if ($rec['pkey']) { $_SESSION['goods_system']->exec_price($rec) ; // damp_array($rec) ;
                        update_rec_in_table($tkey,array('price_site'=>$rec['__price_4']),'pkey='.$pkey) ;
                      }
    return(1) ;
  }


?>