<?
include_once(_DIR_TO_ENGINE.'/class/clss_0.php') ;
class clss_2 extends clss_0
{
    // подготовка url для страницы объекта на основе полей объекта
    // производиться при подготовке поля url_name, которое заполняется автоматички при каждом измерении полей записи
    function prepare_url_obj($rec)
    {   //$url_name=substr(safe_text_to_url($rec['obj_name'],$rec['tkey']),0,32).'_'.$rec['pkey'] ;
        $url_name=safe_text_to_url($rec['obj_name'],$rec['tkey']) ;
        return($url_name) ;
    }

   // обновление price_site, price_on при изменении цены товара
   function before_save_rec($tkey,$pkey,&$rec) // перед сохранением записи по объекту в базу через админку
    {   if (isset($rec['price']) and isset(_DOT($tkey)->field_info['price_site']))
           { $rec_source=execSQL_van('select * from '._DOT($tkey)->table_name.' where pkey='.$pkey) ;
             $rec_summary=array_merge($rec_source,$rec) ;
             if (!isset($rec_summary['val'])) $rec_summary['val']=$_SESSION['VL_def_DB'] ;  // внимание!!! если $rec['val']=0, а $_SESSION['VL_def_DB']=1, то будет затерно правильное значение!
             $_SESSION['goods_system']->exec_price($rec_summary) ; // damp_array($rec_source) ;
             if ($rec_summary['__price_4']) { $rec['price_site']=$rec_summary['__price_4'] ;  $rec['price_on']=1 ; }
             else                           { $rec['price_site']=0 ;  $rec['price_on']=0 ; }
           }
    }

    // после создания объекта прописываем бренд в его поле
    function on_create_event($obj_info)
    { global $TM_goods ;
      if (!isset(_DOT($obj_info['tkey'])->field_info['brand'])) return ;
      $arr_parents=get_obj_parents_ids($obj_info['_reffer']) ;
      execSQL_update('update '.$TM_goods.' set brand="'.$arr_parents[1].'" where pkey='.$obj_info['pkey']) ;
      global $goods_system ;
      // на всякий случай обновляем все оптом
      foreach($goods_system->tree['root']->list_obj as $brand_id)
      { $arr_child_sections=$goods_system->tree[$brand_id]->get_list_child() ;
        //echo  $brand_id.':'.$arr_child_sections.'<br>' ;
        execSQL_update('update '.$TM_goods.' set brand="'.$brand_id.'" where clss=200 and parent in ('.$arr_child_sections.')') ;
      }
    }



     // возвращает значение поля fname массива объекта класса. Отдельная функция необходима, т.к. иногда есть необходимость корректировать значения поля перед отдачей в вывод
    function get_rec_field($rec,$fname,$options=array())
     {  global $category_system ;
        if (strpos($fname,'props_')!==false)
         {  $arr=explode('_',$fname) ;
            $props_id=$arr[1]  ;
            $props_info=$category_system->get_array_props_info($rec) ;
            $props_tkey=_DOT($rec['tkey'])->list_clss[6] ;
            if ($props_tkey)
            { $props_rec=execSQL_van('select pkey,id,value,num,str from '._DOT($props_tkey)->table_name.' where parent='.$rec['pkey'].' and id='.$props_id) ;
              if ($props_rec['pkey']) switch($props_info[$props_id]['edit_element'])
              { case 'input_num': return($props_rec['num']*1) ; break ;
                case 'input_str': return($props_rec['str']) ; break ;
                default:          return($props_rec['value']) ;
              }
            }
            return('') ; //return($props_info[$props_id]['edit_element']) ;
         }
       else return(parent::get_rec_field($rec,$fname,$options)) ;
     }


    function panel_fast_search($tkey,$options=array())
    { $clss=($options['clss'])? $options['clss']:$this->clss ;
      ?><div id=panel_search script_url="<?echo _PATH_TO_ADMIN?>/ajax.php" cmd="list_items_search" clss=<?echo $clss?> tkey=<?echo $tkey?> show_van_rec_as_item=0>
                  <strong>ПОИСК:</strong>
                    <input type=text name=text_search class="text"><img src="<?echo _PATH_TO_ENGINE?>/admin/images/find_icon.gif" class=go_search>
                    <input type=radio name=target_search value=obj_name checked>Наименованию
                    <input type=radio name=target_search value=art>Артикулу
                </div>
      <?
    }

    function prepare_search_usl($target,$search_text)
          { $options=array() ; $usl='' ; $arr_fields=array() ;  $_usl_filter=array() ;
            $title=$this->name() ;
            if ($search_text and $target)
            {   $arr_words=explode(' ',trim($search_text)) ; // разбираем на пробелы
                switch($target)
                 { case 'obj_name': $arr_fields=array('obj_name') ; $title.=' - наименование содержит "'.$search_text.'"' ; break ;
                   case 'art': $arr_fields=array('art') ; $title.=' - автикул содержит "'.$search_text.'"' ; break ;
                 }
                if (sizeof($arr_words)) foreach($arr_words as $word)
                { $_usl2=array() ;
                  if (sizeof($arr_fields)) foreach($arr_fields as $fname) $_usl2[]="UCASE(".$fname.") rlike UCASE('[[:<:]]".mb_strtoupper($word)."')" ;
                  if (sizeof($_usl2)) $_usl_filter[]='('.implode(' or ',$_usl2).')' ;
                }
            }
            if ($_POST['usl'])                  $usl=stripslashes($_POST['usl']) ;
            if ($_POST['show_van_rec_as_item']) $options['show_van_rec_as_item']=$_POST['show_van_rec_as_item'] ;
            if (sizeof($_usl_filter))           $options['usl_filter']=implode(' and ',$_usl_filter) ;
            $options['default_order']='obj_name' ;
            $options['title']=$title ;
            return(array($usl,$options)) ;
          }
}

?>