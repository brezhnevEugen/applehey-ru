<?
include_once(_DIR_EXT.'/angel-discount/sync_1c.php')  ;
// подключение панели в меню
// ext_panel_register(array('frame'=>array('table_code'=>'goods','is_root'=>1),'menu_item'=>array('name'=>'Операции','script'=>'mod/goods/i_goods_root_operation_actions.php','cmd'=>'panel_goods_root_operation_actions'))) ;

// операции
 function panel_goods_root_operation_actions()
    { ?><table class="left"><?
        /*?><tr><td>Внести значения brand_id и series_id в записи товара</td><td><button cmd="update_brand_id" script="mod/goods/i_goods_root_operation_actions.php">Выполнить</button></td></tr><?
        ?><tr><td>Обновить поле price_site</td><td><button cmd="update_price_site" script="mod/goods/i_goods_root_operation_actions.php">Выполнить</button></td></tr><?
        ?><tr><td>Показать товары без бренда</td><td><button cmd="view_no_brand_goods"  script="mod/goods/i_goods_root_operation_actions.php">Выполнить</button></td></tr><?
        ?><tr><td>Показать товары без колллекции</td><td><button cmd="view_no_collection_goods"  script="mod/goods/i_goods_root_operation_actions.php">Выполнить</button></td></tr><?
        */

        ?><tr><td>Sync 1c</td><td><button cmd="angel-discount/sync_1c">Выполнить</button></td></tr><?
        ?><tr><td>Распаковать архив импорта из 1С</td><td><button cmd="angel-discount/unzip_export_1c"  script="sync_1c.php">Выполнить</button></td></tr><?
        ?><tr><td>Импорт разделов из 1C</td><td><button cmd="angel-discount/import_section_from_1c"  script="sync_1c.php">Выполнить</button></td></tr><?
        ?><tr><td>Импорт сортамента из 1C</td><td><button cmd="angel-discount/import_sotrament_from_1c"  script="sync_1c.php">Выполнить</button></td></tr><?
        ?><tr><td>Импорт цен и склада из 1C</td><td><button cmd="angel-discount/import_price_from_1c"  script="sync_1c.php">Выполнить</button></td></tr><?
        ?><tr><td>Создание товаров на основе сортамента</td><td><button cmd="angel-discount/import_goods_from_sortament"  script="sync_1c.php">Выполнить</button></td></tr><?
        ?><tr><td>Импорт кадров из Excel</td><td><button cmd="angel-discount/import_info_from_excel"  script="import_kadr.php">Выполнить</button></td></tr><?
        ?><tr><td>Загрузка фото по номеру кадра</td><td><button cmd="angel-discount/import_img_by_kadr"  script="import_photo.php">Выполнить</button></td></tr><?

        ?><tr><td>Продублировать price, category из сортамента в товар</td><td><button cmd="angel-discount/dublicate_info_from_1c"  script="sync_1c.php">Выполнить</button></td></tr><?
        ?><tr><td>Показать таблицу соответствий кадр-сортамент</td><td><button cmd="view_arr_kadr_sortament"  script="mod/goods/i_goods_root_operation_actions.php">Выполнить</button></td></tr><?
        ?><tr><td>Статистика по загрузке фото</td><td><button cmd="show_stats_by_img"  script="mod/goods/i_goods_root_operation_actions.php">Выполнить</button></td></tr><?
        ?><tr><td>Показать фото не загруженные на сайт</td><td><button cmd="show_image_bez_kadra"  script="mod/goods/i_goods_root_operation_actions.php">Выполнить</button></td></tr><?
        ?><tr><td>Показать все кадры</td><td><button cmd="show_kadr"  script="mod/goods/i_goods_root_operation_actions.php">Выполнить</button></td></tr><?
        ?><tr><td>Показать дубликаты 1С ID</td><td><button cmd="show_dubl_1c_id"  script="mod/goods/i_goods_root_operation_actions.php">Выполнить</button></td></tr><?
        ?><tr><td>Показать дубликаты кадров у разных товаров</td><td><button cmd="show_dubl_kadr"  script="mod/goods/i_goods_root_operation_actions.php">Выполнить</button></td></tr><?
        ?><tr><td>Проверить имена фото</td><td><button cmd="check_photo" script="mod/goods/i_goods_root_operation_actions.php">Выполнить</button></td></tr><?
        ?><tr><td>Проверить имена фото</td><td><button cmd="check_photo" script="mod/goods/i_goods_root_operation_actions.php">Выполнить</button></td></tr><?


        /*?><tr><td>Проверка файла выгрузки цен</td><td><button cmd="check_price_from_1c"  script="mod/goods/i_goods_root_operation_actions.php">Выполнить</button></td></tr><?*/
        ?><tr><td>Показать разделы без типов</td><td><button cmd="check_section_to_types"  script="mod/goods/i_goods_root_operation_actions.php">Выполнить</button></td></tr><?

        ?><tr><td>Показать товары без типа</td><td><button cmd="view_no_types_goods"  script="mod/goods/i_goods_root_operation_actions.php">Выполнить</button></td></tr><?
        ?><tr><td>Показать товары без пола</td><td><button cmd="view_no_pol_goods"  script="mod/goods/i_goods_root_operation_actions.php">Выполнить</button></td></tr><?

        ?><tr><td>Соединение типов товаров</td><td><button cmd="working_types" script="mod/goods/i_goods_root_operation_actions.php">Выполнить</button></td></tr><?



      ?></table><?
    }



function view_no_brand_goods()
{ global $TM_goods ;
  _CLSS(200)->show_list_items($TM_goods,'clss=200 and (brand_id=0 or brand_id is null)',array()) ;
}

function view_no_collection_goods()
{ global $TM_goods ;
  _CLSS(200)->show_list_items($TM_goods,'clss=200 and (series_id=0 or series_id is null)',array()) ;
}

function view_no_types_goods()
{ global $TM_goods ;
  _CLSS(200)->show_list_items($TM_goods,'clss=200 and (type=0 or type is null)',array()) ;
}

function view_no_pol_goods()
{ global $TM_goods ;
  _CLSS(200)->show_list_items($TM_goods,'clss=200 and ((m is null or m=0) and (w is null or w=0) and (k is null or k=0))',array()) ;
}

function working_types()
{
  ?><h1>Соединяем типы товаров</h1>
    Код типа товара 1 <input type="text" name="id_type_1">
    Код типа товара 2 <input type="text" name="id_type_2">
    <button cmd="working_types_apply"  script="mod/goods/i_goods_root_operation_actions.php">Добавить тип 1 в тип 2</button>
  <?
}

function working_types_apply()
{ global $category_system ;
  ?><h1>Соединяем типы товаров</h1><?
  $id_type1=$_POST['id_type_1']  ;
  $id_type2=$_POST['id_type_2']  ;
  if (!$id_type1 or !$id_type2) { $this->working_types() ; return ; } ;
  echo 'Берем товары типа <strong>'.$category_system->tree[$id_type1]->name.'</strong>, назначаем им тип <strong>'.$category_system->tree[$id_type2]->name.'</strong>, тип товара <strong>'.$category_system->tree[$id_type1]->name.'</strong> удаляем?<br>' ;
  echo 'Внимание! Операцию нельзя будет откатить назад!'
  ?><button cmd="working_types_apply2"  script="mod/goods/i_goods_root_operation_actions.php" id_type_1="<?echo $id_type1?>" id_type_2="<?echo $id_type2?>">Да, подтверждаю</button><?
}

function working_types_apply2()
{ global $category_system ;
  ?><h1>Соединяем типы товаров</h1><?
  $id_type1=$_POST['id_type_1']  ;
  $id_type2=$_POST['id_type_2']  ;
  echo '<h2>Меняем тип товара у товаров</h2><br>' ;
  execSQL_update('update obj_site_goods set type='.$id_type2.' where type='.$id_type1,array('debug'=>2))  ;
  echo '<h2>Выключаем старый тип товара</h2><br>' ;
  execSQL_update('update obj_site_category set enabled=0 where pkey='.$id_type1,array('debug'=>2))  ;
  echo '<h2>Меняем тип товара в таблице соответствий типов товара 1С и типов товара сайта</h2><br>' ;
  execSQL_update('update obj_site_list set type='.$id_type2.' where clss=208 and type='.$id_type1,array('debug'=>2))  ;
  echo '<br><br><br>ГОТОВО! Перегрузите сайт через главную страницу, чтобы увидеть изменения!' ;
}



function check_photo()
{
    ?><h1>Проверяем имена фото</h1>
      <textarea cols="100" rows="20" name="img_names"></textarea><br>
      <button cmd=check_photo_apply script="mod/goods/i_goods_root_operation_actions.php" table_reffer="<?echo $_POST['table_reffer']?>">Проверить</button><br><br><?
}

function check_photo_apply()
{
    //echo $_POST['img_names'] ;
    $arr_names=explode("\n",$_POST['img_names']) ; $img_upload=0 ; $img_no_upload=0 ;
    //damp_array($arr_names) ;
    if (sizeof($arr_names)) foreach($arr_names as $img_name)
    {   $img_name=trim($img_name) ;
        echo '<strong>'.$img_name.'</strong> - ' ;

        $name=(str_replace(array('086A','086a','IMG_'),'',$img_name)) ;
        $rec_img=execSQL_van('select pkey,file_name,parent from obj_site_goods_image where (obj_name like "086A'.$name.'" or obj_name like "086a'.$name.'" or obj_name like "086A'.$name.'" or obj_name like "IMG_'.$name.'" or obj_name like "'.$name.'")') ;
        if ($rec_img['pkey'])
        {  ?>Товар <a href="/admin/fra_viewer.php?obj_reffer=<?echo $rec_img['parent']?>.136" action="new_window"><? echo $rec_img['parent'] ?></a>&nbsp;&nbsp;&nbsp;<?
           echo '<br>' ;
           $img_upload++ ;
        }
        else
        {  echo '<span class="red">НЕ ЗАГРУЖЕНО</span>' ;
           $img_no_upload++ ;
           $kadr=str_replace(array('086A','086a','IMG_','.jpg'),'',$img_name) ;
           $kadr_info=execSQL('select pkey as id,goods_id,sortament_id,kadr,goods_enabled,file from obj_site_stats_kadr where kadr="'.$kadr.'"') ;
           if ($kadr_info['pkey']) damp_array($kadr_info,1,-1) ;
           echo '<br>' ;
        }
    }

    echo '<h2>ИТОГ</h2>' ;
    echo 'Проверено фото - <strong>'.sizeof($arr_names).'</strong><br>' ;
    echo 'Загружено фото - <strong>'.$img_upload.'</strong><br>' ;
    echo 'Не загружено фото - <strong>'.$img_no_upload.'</strong><br>' ;
    check_photo() ;

}


/*=====================================================================================================================================================================================================================================
 *
 * УТИЛИТА ДЛЯ ПРИНУДИТЕЛЬНОГО ОБНОВЛЕНИЯ ПОЛЯ price_site
 *
 *=====================================================================================================================================================================================================================================*/

function update_price_site()
{   echo $_SESSION['goods_system']->tkey ;
    ?><h1>Заполняем (обновляем) поле "price_site"</h1>
    Если на сайте цены на товары в базе заданы в разных валютах, для сортировки по цене необходимо иметь поле, в котором цена будет представлена в абсолютной величине.<br>
    Функция заполняет поле site_price, используя метод goods_system->exec_price().<br><br>
    <button cmd=update_price_site_apply script="mod/goods/i_goods_root_operation_actions.php" table_reffer="<?echo $_POST['table_reffer']?>">Заполнить</button><br><br>
  <?

}

function update_price_site_apply($options=array())
{ //damp_array($_POST) ;
  //damp_array($options) ;
  list($table_id,$temp)=explode('.',$_POST['table_reffer']) ;
  $table_name=($options['table_name'])?  $options['table_name']:_DOT($table_id)->table_name ;
  //echo 'table_name='.$table_name.'<br>' ;
  //return ;
  ?><h1>Заполняем (обновляем) поле "price_site"</h1><?
  $list_goods=execSQL('select pkey as id,price,koof,val,sales_type,sales_value,obj_name from '.$table_name.' order by pkey') ;
  execSQL_update('update '.$table_name.' set price_site=NULL,price_on=NULL') ;
  $update=0 ;
  ?><table class=left><tr class=clss_header><td>Наименование</td><td>Цена</td><td>Валюта</td><td>Значение скидки</td><td>Цена на сайте</td></tr><?
  if (sizeof($list_goods)) foreach($list_goods as $rec)
  { if (!$rec['val']) $rec['val']=$_SESSION['VL_def_DB'] ;
    $_SESSION['goods_system']->exec_price($rec) ; // damp_array($rec) ;
    if ($rec['__price_4'])
    { $res=update_rec_in_table($table_name,array('price_site'=>$rec['__price_4'],'price_on'=>1),'pkey='.$rec['id']) ;
    $update+=$res['update'] ;
  }
    ?><tr><td><?echo $rec['obj_name']?></td>
          <td><?if ($rec['price']>0) echo $rec['price']?></td>
          <td><?if ($rec['price']>0) echo $_SESSION['VL_arr'][$rec['val']]['YA']?></td>
          <td><?if ($rec['sales_value']>0) echo $rec['sales_value'].(($rec['sales_type']==3)? '%':$_SESSION['VL_def_DB_ue'])?></td>
          <td><?if ($rec['__price_4']>0) echo $rec['__price_4']?></td>
      </tr>
    <?
  }
  ?></table><?
  echo 'Заполнены цены для '.$update.' товаров<br>' ;
  //damp_array($res) ;
  //print_2x_arr($list_goods) ;
  return ;
}


function object2array($object) { return @json_decode(@json_encode($object),1); }

/*=====================================================================================================================================================================================================================================
 *
 * ПРОВЕРКА offers.xml
 *
 *=====================================================================================================================================================================================================================================*/
/*
function check_price_from_1c()
{   ?><h2>Анализ offers.xml</h2><?
    $xml=simplexml_load_file(_DIR_TO_ROOT.'/1c/1cbitrix/offers.xml') ;
    $cnt=0 ; $arr=array() ;  $arr_demo=array() ;
    foreach ($xml->ПакетПредложений->Предложения->Предложение as $price_info)
    { $xml_array=object2array($price_info);
      $xml_array=array_merge($xml_array,$xml_array['Цены']['Цена']) ;
      unset($xml_array['Цены']) ;
      if (sizeof($xml_array)) foreach($xml_array as $fname=>$fvalue) $arr[$fname][$fvalue]=$fvalue ;
      if ($cnt<100) $arr_demo[]=$xml_array ;
      $cnt++ ;
    }
    if (sizeof($arr)) foreach($arr as $fname=>$arr_fname) echo '<strong>'.$fname.'</strong>: '.sizeof($arr_fname).' уникальных значений<br>' ;
    print_2x_arr($arr_demo) ;
} */




function show_stats_by_img()
{
   ?><h2>Статистика по числу изображений у товаров</h2><?

   ?><h3>Информация по числу фото для товара (число загруженных фото/кол-во товаров)</h3><?
   $stats=execSQL('select parent,count(pkey) as cnt from obj_site_goods_image group by parent order by cnt desc') ;
   $stats_cnt=array() ;
   if (sizeof(stats)) foreach($stats as $rec) $stats_cnt[$rec['cnt']]++ ;
   if (sizeof($stats_cnt)) foreach($stats_cnt as $cnt_img=>$cnt_goods) echo 'Товаров, у которых загружено '.$cnt_img.' фото: '.$cnt_goods.' шт. <br>' ;

   ?><h3>Информация по загруженным фото</h3><?
    $cnt1=execSQL_value('select count(distinct(obj_name)) from  obj_site_goods_image') ;
    $cnt2=0 ;
   $list_files=get_files_list(_DIR_TO_ROOT.'/photo2/') ;
   ?>Подготовлено фото для загрузки: <strong><? echo sizeof($list_files) ?></strong><br><?
   ?>Загружено фото в товары: <strong><? echo $cnt1 ?></strong><br><?
   ?>Не задан кадр для изображения: <strong><? echo sizeof($list_files)-$cnt1 ?></strong><br><?

   ?><h3>Информация по заданным кадрам</h3><?
    $cnt1=0 ; $cnt2=0 ;
   $list_files=get_files_list(_DIR_TO_ROOT.'/photo2/') ;
   $img_list=array() ;
   if (sizeof($list_files))  foreach($list_files as $file_name) $img_list[str_replace(array('086A','.jpg'),'',basename($file_name))]=array('fname'=>$file_name,'cnt'=>0) ;


   $recs_kadr=execSQL_row('select pkey,kadr from '.TM_SORTAMENT.' where kadr!=""') ;
   if (sizeof($recs_kadr)) foreach($recs_kadr as $kadr)
   { $kadr=trim($kadr) ;
     if (isset($img_list[$kadr])) $cnt1++ ; else $cnt2++ ;
   }
   ?>Задано кадров изображений: <strong><? echo sizeof($recs_kadr) ?></strong><br><?
   ?>Найдено изображений по номеру кадра: <strong><? echo $cnt1 ?></strong><br><?
   ?>Не найдено изобрадений: <strong><? echo $cnt2 ?></strong><br><?

}

/*=====================================================================================================================================================================================================================================
 *
 * ПРОВЕРКА, ВСЕМ ЛИ РАЗДЕЛАМ НАЗНАЧЕНЫ ТИПЫ ТОВАРОВ
 *
 *=====================================================================================================================================================================================================================================*/

function check_section_to_types()
{
  $recs=execSQL('select t1.pkey,t1.obj_name,t1.1c_id,
                (select count(t2.pkey) from obj_site_goods t2 where t2.clss=10 and t2.parent=t1.pkey) as cnt_subsections,
                (select count(t3.pkey) from obj_site_goods t3 where t3.clss=200 and t3.parent=t1.pkey) as cnt_goods
                from obj_site_goods t1 where t1.clss=10 and (t1.type is null or t1.type=0)') ;
  print_2x_arr($recs) ;

}


function show_image_bez_kadra()
{   $list_photo2=array() ;$list_photo22=array() ;
    $list_photo=get_files_list(_DIR_TO_ROOT.'/photo2/') ;
    if (sizeof($list_photo)) foreach($list_photo as $id=>$file_name)
    { $kadr=str_replace(array('086A','.jpg'),'',basename($file_name)) ;
      $list_photo2[$kadr]=strtolower(basename($file_name))  ;
      $list_photo22[$kadr]=basename($file_name)  ;
    }
    echo 'В папке исходников находиться <strong>'.sizeof($list_photo2).'</strong> фото<br>' ;
    //damp_array($list_photo2) ;
    $recs_kadr=execSQL_row('select kadr,pkey from '.TM_SORTAMENT.' where kadr!=""') ;
    $recs_images=execSQL('select pkey,obj_name,parent from obj_site_goods_image') ;
    if (sizeof($recs_images)) foreach($recs_images as $rec) $list_images2[strtolower($rec['obj_name'])]=$rec ;
    //damp_array($list_images2) ;
    echo 'На сайт загружено <strong>'.sizeof($list_images2).'</strong> фото<br>' ;
    //damp_array($list_images2) ;
    $i=0 ;

    //echo $list_images2['086a4627.jpg'] ;

    ?><table><?

    if (sizeof($list_photo2)) foreach($list_photo2 as $kadr=>$img_name) if (!isset($list_images2[$img_name]))
    {   $i++ ;
        $fname=$list_photo22[$kadr] ;
        ?><tr>  <td><?echo $i?></td>
                <td><?echo $fname?></td>
                <td><?echo $kadr?></td>
                <td><a href="/photo2/<? echo $fname?>" target="_blank"><img src="/photo2/<? echo $fname?>" width=100></a></td>
                <td><?if (isset($recs_kadr[$kadr])) echo $recs_kadr[$kadr]?></td>
                <td><? if ($_SESSION['ARR_kard_sortament'][$kadr]['file'])
                        if (!$_SESSION['ARR_kard_sortament'][$kadr]['sortament_id'])  echo 'Есть кадр, нет товара в базе' ;
                        else damp_array($_SESSION['ARR_kard_sortament'][$kadr],1,-1) ;

                    ?></td>
          </tr><?
    }
    ?></table><?
}
function show_kadr()
{   $list_photo2=array() ;
    $list_photo=get_files_list(_DIR_TO_ROOT.'/photo2/') ;
    if (sizeof($list_photo)) foreach($list_photo as $id=>$file_name)
    { $img_name=str_replace(array('086A','.jpg'),'',basename($file_name)) ;
      $list_photo2[$img_name]=basename($file_name)  ;
    }

    //damp_array($list_photo2) ;
    $recs_kadr=execSQL('select t1.pkey,t1.kadr,t1.parent,t1.code,t2.obj_name,t2.art from '.TM_SORTAMENT.' t1, obj_site_goods t2 where not t1.kadr is null and t1.kadr!="" and t2.pkey=t1.parent order by t1.kadr asc') ;
    $recs_images=execSQL_row('select parent,count(pkey) from obj_site_goods_image group by parent') ;
    //damp_array($recs_images) ;
    $i=0 ;  $n=0 ; $k=0 ;  $last_parent=0 ; $last_kadr=0 ;
    ?><table><tr><th>№</th><th>Номер кадра</th><th>Название товара</th><th>Штрих-код сортамента</th><th>Фото от фотографа</th><th>Фото на сайте</th><th>Разные товары<br>для одного фото</th></tr><?

    if (sizeof($recs_kadr)) foreach($recs_kadr as $rec)
    {   $i++ ;
        $fname1='086A'.$rec['kadr'].'.jpg' ;
        $fname2=$rec['kadr'].'.jpg' ;

        ?><tr>  <td><?echo $i?></td>
                <td><?echo $rec['kadr']?></td>
                <td class="left"><?echo $rec['obj_name']?></td>
                <td class="left"><?echo $rec['code']?></td>
                <td><?if (file_exists(_DIR_TO_ROOT.'/photo2/'.$fname1) or file_exists(_DIR_TO_ROOT.'/photo2/'.$fname2)) echo '+' ; else { echo '<div class="red">нет</div>' ; $n++ ; }?></td>
                <td><?if (isset($recs_images[$rec['parent']])) echo '+' ; else { echo '<div class="red">нет</div>' ; $k++ ; }?></td>
                <td><?if ($rec['kadr']==$last_kadr and $rec['parent']!=$last_parent)  echo '<div class="red">ДА</div>' ; ?></td>
          </tr><?
        $last_parent=$rec['parent'] ;
        $last_kadr=$rec['kadr'] ;
    }
    ?></table><?
    echo 'Всего указано <strong>'.$i.'</strong> кадров для сортаментов<br>'  ;
    echo 'Для <strong>'.$n.'</strong> кадров отсутствуют фото<br>'  ;
    echo 'Для <strong>'.$k.'</strong> сортаментов отсутствуют фото<br>'  ;
}


function show_dubl_1c_id()
{   ?><h2>Дубликаты 1С ID</h2><?
    $recs=execSQL_row('select 1c_id,count(pkey) as cnt from '.TM_SORTAMENT.' group by 1c_id HAVING cnt>1') ;
    damp_array($recs,1,-1) ;
    ?><h2>Дубликаты Штрих-код</h2><?
    $recs=execSQL_row('select code,count(pkey) as cnt from '.TM_SORTAMENT.' group by code HAVING cnt>1') ;
    damp_array($recs,1,-1) ;
    ?><h2>Дубликаты кадров в файлах XLS</h2><?
    $recs=execSQL('select kadr,count(pkey) as cnt,count(distinct(goods_id)) as cnt_goods from obj_site_stats_kadr group by kadr HAVING cnt>1 and kadr>0 and cnt_goods>1',0,1) ;
    ?><table class="debug"><tr><th>Номер кадра</th><th>Число<br>дубликатов</th><th>Файл:строка</th><th>Штрих-код</th><th>ID<br>товара на сайте</th><th>ID<br>сортамента на сайте</th><th>Название товара</th><th>Название сортамента</th><th>Прикрепленное<br>к товару фото</th></tr><?
    if (sizeof($recs)) foreach($recs as $i=>$rec)
    {  echo '<tr>' ;
        ?><td rowspan="<?echo $rec['cnt']?>"><?echo $rec['kadr']?></td>
            <td rowspan="<?echo $rec['cnt']?>"><?echo $rec['cnt']?></td><?
       $sources=execSQL('select file,line,code,goods_id,(select obj_name from obj_site_goods where pkey=goods_id) as goods_name,sortament_id,(select obj_name from '.TM_SORTAMENT.' where pkey=sortament_id) as sortament_name_name,img_name from obj_site_stats_kadr where kadr="'.$rec['kadr'].'"',0,1) ;
       if (sizeof($sources)) foreach($sources as $rec_s)
       { ?><td class="left"><?echo $rec_s['file'].':'.$rec_s['line']?></td>
           <td><?echo $rec_s['code']?></td>
           <td><?echo $rec_s['goods_id']?></td>
           <td><?echo $rec_s['sortament_id']?></td>
           <td class="left"><?echo $rec_s['goods_name']?></td>
           <td class="left"><?echo $rec_s['sortament_name_name']?></td>
           <td><?echo $rec_s['img_name']?></td>
         <?
         echo '</tr>' ;
       }

    }
    ?></table><?
}

function show_dubl_kadr()
{  global $TM_goods ;
   $recs=execSQL('select kadr,count(distinct(parent)) as cnt_parent from '.TM_SORTAMENT.' where clss=65 and kadr!="" group by kadr having cnt_parent>1 order by kadr') ;
   if (sizeof($recs)) foreach($recs as $kadr=>$rec)
   { ?><h2>КАДР <?echo $kadr?></h2><?
     _CLSS(207)->show_list_items('obj_site_stats_kadr','kadr='.$kadr,array('buttons'=>array())) ;

     $recs_goods=execSQL_line('select parent from '.TM_SORTAMENT.' where kadr="'.$kadr.'"') ;
     //damp_array($recs_goods,1,-1) ;
     _CLSS(200)->show_list_items($TM_goods,'pkey in ('.implode(',',$recs_goods).')',array('buttons'=>array())) ;
   }

   //$recs_goods=execSQL('select ')
}


// Неправильной номер кадра
// 2000344826333 - 5945
// 2000344837568 - 5954
// 2000344395273 - 2000344395310 - 26757
// 2000344799101 - 2000344727319 - 26869


function view_arr_kadr_sortament()
{
  //damp_array($_SESSION['ARR_kard_sortament'],1,-1) ;
  $recs=execSQL('select pkey as id,file,line,kadr,img_name,code,art,goods_id,sortament_id,goods_enabled from obj_site_stats_kadr') ;
  print_2x_arr($recs) ;
}


// фото с ошибками
// Платье, D18666 5878, - кадр 7658
// Платье, D18648 5667, - кадр 7558
// Платье, L5JD85 G7RI4, - кадр 7592
// Платье, 32131444, - кадр 7622
// Жакет, FR323535-V02332R, - кадр 7685
//

?>