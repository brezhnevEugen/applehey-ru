<?

function panel_goods_section_operation() // Импорт объектов в класс
{  // системные массивы
  global $obj_info,$tkey ;
  ?><br><table>
      <tr><td class="td_space" colspan=2>&nbsp;</td></tr>
      <tr class=clss_header><td colspan=2>Информация</td></tr>
      <tr><td class="left">Информация по дочерним объектам</td><td class='center'><button class=button cmd=child_obj_info script="ext/info/child_obj_info.php">Далее</button></td></tr>

      <tr><td class="td_space" colspan=2>&nbsp;</td></tr>
      <tr class=clss_header><td colspan=2>Найти и заменить</td></tr>
      <tr><td class="left">Поиск по имени, артикулу: <input type="text" name="text_search" class="text big"></td><td class='center'><button class=button cmd=panel_search_goods script="mod/goods/i_menu_panels.php">Далее</button></td></tr>
      <tr><td class="left">Изменение типа товара</td><td class='center'><button class=button cmd=panel_change_goods_types script="mod/category">Далее</button></td></tr>
      <tr><td class="left">Замена фрагмента текста в текстовом поле для дочерних объектов</td><td class='center'><button class=button cmd=dialog_text_replace mode=dialog>Далее</button></td></tr>
      <tr><td class="left">Замена значения поля для дочерних объектов</td><td class='center'><button class=button cmd=dialog_value_replace mode=dialog>Далее</button></td></tr>
      <tr><td class="left">Установка значения характеристики для дочерних объектов</td><td class='center'><button class=button cmd=dialog_props_replace mode=dialog>Далее</button></td></tr>
      <tr><td class="td_space" colspan=2>&nbsp;</td></tr><tr class=clss_header><td colspan=2>Анализ текстов</td></tr>
      <tr><td class="left">Проверка доступности ссылок в текстах</td><td class='center'><button cmd=get_outer_link mode=dialog>Далее</button></td></tr>

      <tr class=clss_header><td colspan=2>Импорт-экспорт объектов</td></tr>
       <!--<tr><td class="left">Импорт из Excel </td><td class='center'><button cmd=import_from_excel>Далее</button></td></tr>-->
	   <!--<tr><td class="left">Импорт из Excel XML</td><td class='center'><button class=button cmd=import_from_xml mode=dialog>Далее</button></td></tr>-->
	   <tr><td class="left">Импорт объектов из файла CLSS.XML</td><td class='center'><button class=button cmd=import_from_xml mode=dialog>Далее</button></td></tr>
	   <tr><td class="left">Экспорт объектов в файл CLSS.XML</td><td class='center'><button class=button cmd=import_from_xml mode=dialog>Далее</button></td></tr>
	   <tr><td class="left">Экспорт в файл CSV</td>
       <td class='center'><button class=button mode=dialog cmd=export_obj_to_csv>Далее</button></td></tr>
       <tr><td class="left">Экспорт в файл EXCEL</td><td class='center'><button class=button mode=dialog cmd=export_obj_to_csv>Далее</button></td></tr>
       <tr><td class="left">Импорт цен из файла EXCEL</td><td class='center'><button class=button cmd=import_price_from_excel mode=dialog>Далее</button></td></tr>
       <tr><td class="left">Импорт склада из файла EXCEL</td><td class='center'><button class=button cmd=import_stock_from_excel mode=dialog>Далее</button></td></tr>
       <tr><td class="td_space" colspan=2>&nbsp;</td></tr>
       <?if (isset(_DOT($tkey)->list_clss[3])) {?>
         <tr class=clss_header><td colspan=2>Импорт изображений в каталог</td></tr>
         <tr><td class="left">Создание объектов по фото</td><td class='center'><button class=button cmd=create_obj_by_photo mode=dialog>Далее</button></td></tr>
         <tr><td class="left">Загрузить фото из директории на сервере</td><td class='center'><button class=button cmd=dialog_upload_images_from_dir mode=dialog>Далее</button></td></tr>
         <tr><td class="td_space" colspan=2>&nbsp;</td></tr>
       <?}?>
        <? /*
 *

	      <tr><td class="left">Группировка <input class='text' name="count" type=text class=text  value="10" size=2 > и более объектов в подраздел</td>
       <td class='center'><button onclick="exe_cmd('group_by_name')">OK</button></td></tr>
	       <tr><td class="left">Информация по составу объекта</td>
       <td class='center'><button onclick="exe_cmd('obj_get_info')">OK</button></td></tr>
         <tr><td class="left">Сгруппировать объекты по дате создания</td>
       <td class='center'><button onclick="exe_cmd('group_by_cdata')">OK</button></td></tr>
         <tr><td class="left">Собрать все потерянные объекты</td>
       <td class='center'><button onclick="exe_cmd('find_pot')">OK</button></td></tr>

         <tr><td class="left">Создать/обновить прямые страницки разделов</td>
       <td class='center'><button onclick="exe_cmd('create_page_rasdel')">OK</button></td></tr>
         <tr><td class="left">Обнулить склад</td>
       <td class='center'><button onclick="exe_cmd('clear_stock')">OK</button></td></tr>
         <tr><td class="left">Обнулить новинки</td>
       <td class='center'><button onclick="exe_cmd('clear_new')">OK</button></td></tr>
         <tr><td class="left">Обнулить хиты продаж</td>
       <td class='center'><button onclick="exe_cmd('clear_hits')">OK</button></td></tr>

         <tr><td class="left">Групповое изменение поля для <select size="1" name="gc_clss"><?if (sizeof(_DOT($tkey)->list_clss)) foreach(_DOT($tkey)->list_clss as $clss=>$tkey_clss){?><option value="<?echo $clss?>"><?echo $_SESSION['descr_clss'][$clss]['name']?></option><?}?></select></td>
       <td class='center'><button class=button cmd=exe_operation mode=dialog onclick="exe_cmd_dialog('dialog_group_change')">OK</button></td></tr>



      <tr><td class="td_space" colspan=2>&nbsp;</td></tr>
      <tr class=clss_header><td>Операции с объектом</td></tr>
	       <tr><td class="left">Изменить класс объекта (<?echo $obj_info['clss']?>) на  <input class='text' name="new_clss" type=text class=text  value="" size=2 ></td>
       <td><button onclick="exe_cmd('set_new_clss')">OK</button></td></tr>
	       <tr><td class="left">Изменить код объекта (<?echo $obj_info['pkey']?>) на  <input class='text' name="new_pkey" type=text class=text  value="" size=2 ></td>
       <td><button onclick="exe_cmd('set_new_pkey')">OK</button></td></tr>
          */ /*?>
      <tr><td class="td_space" colspan=2>&nbsp;</td></tr>
      <?// print_r(_DOT($tkey)->list_clss) ;
        if (isset(_DOT($tkey)->list_clss[3]))
      { $list_clone=_DOT(_DOT($tkey)->list_clss[3])->list_clone_img() ;
        //damp_array($list_clone) ;

      ?>
      <tr><td class='grp_clss_header' colspan=2>Операции с изображениями</td></tr>
   <tr><td class="tdl_data_on">Добавить разделам (1,10) фото потомков (rand)</td><td class='td_data_on'><button onclick="exe_cmd('get_child_photo')">OK</button></td></tr>

      <?}?>    */?>
  </table>
  <input name="get_file_count" type="hidden" value="3">

  <?
  //if ($_SESSION['descr_clss'][$obj_info['clss']]['operations'] and function_exists($_SESSION['descr_clss'][$obj_info['clss']]['operations']))  $_SESSION['descr_clss'][$obj_info['clss']]['operations']() ;

}

?>