<?php
include_once(_DIR_TO_ENGINE."/admin/c_site.php") ;
include_once(_DIR_TO_ENGINE."/admin/i_clss_func.php") ;
include_once(_DIR_TO_ENGINE.'/admin/c_menu_tabs.php') ;

// редактор заказов
class c_editor_goods extends c_editor_obj
{
  function body($options=array()) { $this->body_frame_2x(array('fra_tree'=>_PATH_TO_ADMIN.'/editor_goods_tree.php','title'=>$options['title'])) ; }
}


class c_editor_goods_tree extends c_fra_tree
{ public $system_name='goods_system' ;


 function body($options=array())
  { if (!$this->tkey) { echo 'Не задана рабочая таблица.' ; _exit() ; }
    $this->check_reffer() ; // проверяем, откуда происходит вызов скрипта
    ?><body id=tree_objects><div id=tree_menu><?$this->top_menu() ;?></div><?
    ?><ul class="tree_root" on_click_script="editor_goods_viewer.php" >
                <li clss=1>Группы товара</li>
                <ul>
                    <li clss=1 cmd=view_new>Новинки</li>
                    <li clss=1 cmd=view_hits>Хиты продаж</li>
                    <li clss=1 cmd=view_sales>Со скидкой</li>
                    <li clss=1 cmd=view_lost_costen>Товары без цены</li>
                    <li clss=1 cmd=view_no_image>Товары без картинок</li>
                    <li clss=1 cmd=view_no_files>Товары без инструкций</li>
                    <li clss=1 cmd=view_no_manual>Товары без описания</li>
                </ul>
     </ul>
    <?
    $usl_root=($_GET['root_id'])?  'pkey='.$_GET['root_id']:'parent=0' ;
    $this->generate_object_tree($this->tkey,array('usl_root'=>$usl_root,'no_show_cnt'=>0)); // генерим начало дерева
    ?><script type="text/javascript">$j('ul.tree_root > li:first-child').each(tree_item_click)</script><? // имитируем клик по корню
    ?></body><?
  }

}

//-----------------------------------------------------------------------------------------------------------------------------
// ЗАКАЗЫ
//-----------------------------------------------------------------------------------------------------------------------------

class c_editor_goods_viewer extends c_fra_based
{ public $system_name='goods_system' ;
  public $top_menu_name='top_menu_list_goods' ;
  public $body_class='no_main_menu' ;

 function show_page_obj_header() {}

 function block_main()
 {  global $obj_info,$goods_system ;
    $this->show_page_obj_header($obj_info) ;

    $options=array() ; $usl_select='' ;

    //$not_include_xim_an=' and parent not in ('.$goods_system->tree[165]->get_list_child().')' ;
    //$not_include_teplovisor=' and parent not in ('.$goods_system->tree[3178]->get_list_child().')' ;

    switch($this->cmd)
     { case 'view_new':     $usl_select='new>0' ; $options['order']='new' ; $title='Новинки' ;  break ;
       case 'view_hits':    $usl_select='top>0' ; $options['order']='top' ; $title='Хиты продаж' ; break ;
       case 'view_sales':   $usl_select='sales_value>0' ; $options['order']='sales_value' ; $title='Товары со скидкой' ;  break ;
       case 'view_lost_costen':   $usl_select='price=0' ; $options['order']='parent' ; $title='Товары без цены' ;  break ;
       case 'view_no_image':    $ids=execSQL_line('select pkey from '.$goods_system->table_name.' where not pkey in (select distinct(parent) from '.$goods_system->table_name.'_image)') ;
                                $usl_select='pkey in ('.implode(',',$ids).')' ; $options['order']='parent' ; $title='Товары без картинок' ;
                                break ;
       case 'view_no_files':    $ids=execSQL_line('select pkey from '.$goods_system->table_name.' where not pkey in (select distinct(parent) from '.$goods_system->table_name.'_files)') ;
                                $usl_select='pkey in ('.implode(',',$ids).')' ; $options['order']='parent' ; $title='Товары без инструкций' ;
                                break ;
       case 'view_no_manual':   $usl_select="(manual='' or manual is null)" ; $options['order']='parent' ; $title='Товары без описания' ;
                                break ;
     }
    $options['buttons']=array() ;
    ?><p class='obj_header'><?echo $title?></p><?
    if ($usl_select) _CLSS(200)->show_list_items($this->tkey,$usl_select,$options) ;
    //else echo '<div class="alert">Не задано условие выборки товара</div>' ;

 }

 // меню фрейма
 function _show_page_obj_menu($obj_info)
    {  // создаем объект top_menu
       if (!is_object($_SESSION[$this->top_menu_name])) $_SESSION[$this->top_menu_name]=new c_top_menu() ;
       // формируем набор пунктом меню
       $menu_set=array() ;
       // пункты меню для корня дерева
       if ($_GET['pkey']=='root') $menu_set[]=array("name" => 'Поиск по заказам','cmd' =>'show_list_items','mode'=>'last_items','count'=>20) ;
       // или если выбран один из временных интервалов
       else
       { $time_usl=$this->get_time_usl($this->start_time,$this->end_time) ;
         $arr_status=$_SESSION['goods_system']->get_count_by_status($time_usl) ;
         /// !!! внимание - переделать, чтобы были вкладки "ТИПЫ ОПЛАТЫ"
         $arr_oplata=$_SESSION['goods_system']->get_count_by_field('payments_method',$time_usl) ;
         if (sizeof($arr_status)) foreach($arr_status as $status=>$cnt) {$menu_set[]=array("name" => $_SESSION['list_status_goods'][$status].'('.$cnt.')','cmd' =>'show_list_items','status'=>$status) ; }
         if ($arr_oplata['robox']) $menu_set[]=array("name" => 'Robox ('.$arr_oplata['robox'].')',"cmd" =>'show_list_items','robox'=>1) ;

         global $TM_goods ;
         $market_cnt=execSQL_value('select count(pkey) from '.$TM_goods.' where ya_id>0 and '.$time_usl);
         if ($market_cnt) $menu_set[]=array("name" => 'Маркет ('.$market_cnt.')',"cmd" =>'show_list_items','market'=>1) ;

         $menu_set[]=array("name" => 'Все ('.array_sum($arr_status).')',"cmd" =>'show_list_items') ;
       }
       // выводим меню
       $this->cur_menu_item=$_SESSION[$this->top_menu_name]->show($menu_set) ;  // показываем меню
       return($this->cur_menu_item) ;
    }

  function show_list_items()
  { $options=array() ;
    if ($this->start_time or $this->end_time)  $_usl[]=$this->get_time_usl($this->start_time,$this->end_time) ;
    if (isset($this->cur_menu_item['status'])) $_usl[]='status='.$this->cur_menu_item['status'] ;
    if (isset($this->cur_menu_item['robox']))  $_usl[]='payments_method="robox"' ;
    if (isset($this->cur_menu_item['market']))  $_usl[]='ya_id>0' ;
    if (sizeof($_usl)) $usl=implode(' and ',$_usl) ;
    $title='Заказы '.$this->get_time_title($this->start_time,$this->end_time) ;
    if (isset($this->cur_menu_item['status'])) $title.=' в состоянии "'.$_SESSION['list_status_goods'][$this->cur_menu_item['status']].'"' ;
    if (isset($this->cur_menu_item['robox'])) $title.=' оплаченные через Robox' ;
    if (isset($this->cur_menu_item['market'])) $title.=' оформленные через Yandex Маркет' ;
    if ($this->cur_menu_item['mode']=='last_items')
    { $options['count']=$this->cur_menu_item['count'] ;
      $options['order']='c_data desc' ;
      $title.=' - последние '.$this->cur_menu_item['count'] ;
    }
    $options['default_order']='c_data desc' ;
    $options['title']=$title ;
    $options['buttons']=array('save','delete') ;
    _CLSS(82)->show_list_items($this->tkey,$usl,$options) ;
  }

  function panel_fast_search()
    { ?><div id=panel_search script_url="<?echo _PATH_TO_ADMIN?>/editor_goods_ajax.php" cmd="get_list_goods">
            <strong>ПОИСК:</strong>
            <input type=text name=text_search class="text"><img src="<?echo _PATH_TO_ENGINE?>/admin/images/find_icon.gif" class=go_search>
            <input type=radio name=target_search value=number>По номеру, дате
            <input type=radio name=target_search value=client checked>По клиенту
            <input type=radio name=target_search value=adres>По адресу
            <input type=radio name=target_search value=rekv>По реквизитам
            <!--<input type=radio name=target_search value=goods>По товару-->
        </div>
     <?
    }
}

?>