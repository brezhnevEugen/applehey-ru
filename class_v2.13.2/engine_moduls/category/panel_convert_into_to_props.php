<?
// утилита для распозновая свойств из текстового описания
function panel_convert_into_to_props()
  { global $goods_system,$category_system ;
    $ids=$goods_system->tree[501]->get_list_child() ;
    execSQL_update('update obj_site_goods set temp=0 where clss=200 and  parent in ('.$ids.') ') ;
    $recs=execSQL('select pkey,parent,clss,obj_name,type,comment,source_url from obj_site_goods where clss=200 and comment>"" and parent in ('.$ids.') and temp=0') ;
    //setlocale(LC_ALL,'Russian_russia.1251') ;
    //print_2x_arr($recs) ;
    $cnt_success=0 ;
    if (sizeof($recs)) foreach($recs as $rec)
    { if (preg_match_all('/<td>(.+?)<\/td>/is',$rec['comment'],$matches,PREG_SET_ORDER))
      {   $i=0 ; $name='' ; $value="" ; $res=array() ;
          foreach($matches as $match)
          { if (!$i) $name=trim(strip_tags($match[1])) ; else $value=trim(strip_tags($match[1]))  ;
            $i++ ;
            if ($i==2)
            { if ($name=='Ширина, см') $name='Ширина' ;
              if ($rec['type']==613 and $value=='Прямоугольная') $value='Прямоугольный' ;
              if ($rec['type']==614 and $value=='Прямоугольная') $value='Прямоугольный' ;
              if ($rec['type']==615 and $value=='Прямоугольная') $value='Прямоугольный' ;
              $res[$name]=array('text_value'=>$value,'prop_id'=>'','prop_value'=>'','prop_text'=>'','error'=>'') ; $name='' ; $value="" ; $i=0 ;

            }
          }
          if (isset($res['Размер']))
          {  $arr=explode('х',$res['Размер']['text_value']) ;
             $res['Размер']['text_value']=($arr[0]/10).'x'.($arr[1]/10) ;
             $res['Ширина']=array('text_value'=>$arr[1]/10,'prop_id'=>'','prop_value'=>'','prop_text'=>'','error'=>'') ;
             $res['Длина']=array('text_value'=>$arr[0]/10,'prop_id'=>'','prop_value'=>'','prop_text'=>'','error'=>'') ;
             unset($res['Размер']) ;
          }
          unset($res['Производитель']) ;

          //damp_array($res) ;
          $props_info=$category_system->get_array_props_info($rec) ; $found_props=0 ;  // damp_array($props_info) ;
          if (sizeof($props_info)) foreach($props_info as $prop_rec) if (isset($res[$prop_rec['title']]))
          {  //damp_array($prop_rec,1,-1) ;
             $text_value=$res[$prop_rec['title']]['text_value'] ; //echo '$text_value='.$text_value.'<br>' ;
             $prop_id=$prop_rec['id'] ; $name_arr=array() ;
             if ($prop_rec['indx_select'])
             {   $indx_arr=$_SESSION[$prop_rec['indx_select']] ;
                 if (sizeof($indx_arr)) foreach($indx_arr as $rec_indx)
                 { if (strnatcasecmp(translit($rec_indx['obj_name']),translit($text_value))==0)
                    {  $res[$prop_rec['title']]['prop_id']=$prop_id ;
                       $res[$prop_rec['title']]['prop_value']=$rec_indx['id'] ;
                       $res[$prop_rec['title']]['prop_text']=$rec_indx['obj_name'] ;
                       $found_props++ ;
                    }
                   $name_arr[]=$rec_indx['obj_name'] ;
                 }
                 if (!$res[$prop_rec['title']]['prop_id']) $res[$prop_rec['title']]['error']=implode(', ',$name_arr) ;
             }
             if ($prop_rec['edit_element']=='input_num')
             { $res[$prop_rec['title']]['prop_id']=$prop_id ;
               $res[$prop_rec['title']]['prop_value']=$text_value ;
               $found_props++ ;
             }
             if ($prop_rec['edit_element']=='checkbox')
             { $res[$prop_rec['title']]['prop_id']=$prop_id ;
               if ($text_value=='Да') $res[$prop_rec['title']]['prop_value']=1 ;
               if ($text_value=='Нет') $res[$prop_rec['title']]['prop_value']=0 ;
               $res[$prop_rec['title']]['prop_text']=$text_value ;
               $found_props++ ;
             }
          }
          if ($found_props<sizeof($res))
          { ?><h2><?echo $rec['obj_name']?></h2><?
            echo 'Тип товара: '.$category_system->tree[$rec['type']]->name.' ('.$rec['type'].')<br>' ;
            echo $rec['source_url'].'<br>' ;
            /*?><textarea rows=10 cols="100"><?echo $rec['comment']?></textarea><?*/
            print_2x_arr($res) ;
          }
          else $cnt_success++ ;
          /*
          execSQL_update('delete from obj_site_goods_props where parent='.$rec['pkey']) ;
          if (sizeof($res)) foreach($res as $rec_prop) if ($rec_prop['prop_id'] and $rec_prop['prop_value'])
          { //damp_array($rec) ;
            $prop_id=$rec_prop['prop_id'] ;
            $prop_value=$rec_prop['prop_value'] ;
            $category_system->save_prop_value($rec['pkey'],$rec['type'],$prop_id,$prop_value,1,$props_info[$prop_id]) ;
          } */

          execSQL_update('update obj_site_goods set temp=1 where clss=200 and  parent in ('.$ids.') ') ;
      }


    }
    echo 'Проверено '.sizeof($recs).' товаров<br>' ;
    echo 'Распознано '.$cnt_success.' товаров<br>' ;
}

?>