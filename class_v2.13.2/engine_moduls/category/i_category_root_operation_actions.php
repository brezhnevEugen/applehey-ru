<?
// подключение панели в меню
// ext_panel_register(array('frame'=>array('table_code'=>'category','is_root'=>1),'menu_item'=>array('name'=>'Операции','script'=>'mod/category/i_category_root_operation_actions.php','cmd'=>'panel_category_root_operation_actions'))) ;

// операции
 function panel_category_root_operation_actions()
    { ?><table class="left"><?
        ?><tr><td>Импортировать свойства товаров из списков сайта</td><td><button cmd="import_props_from_list"  script="mod/category/i_category_root_operation_actions.php">Выполнить</button></td></tr><?
        ?><tr><td>Преобразовать значение поля "type" в линки</td><td><button cmd="convert_type_to_links"  script="mod/category/i_category_root_operation_actions.php">Выполнить</button></td></tr><?
        ?><tr><td>Показать связанные через links типы товаров и разделы каталога</td><td><button cmd="view_links_goods_types"  script="mod/category/i_category_root_operation_actions.php">Выполнить</button></td></tr><?
        ?><tr><td>Обновить тип товаров по названию товара</td><td><button cmd="update_goods_type_by_obj_name"  script="mod/category/i_category_root_operation_actions.php">Выполнить</button></td></tr><?
      ?></table><?

    }

function import_props_from_list($menu_item)
 { global $category_system ;
     //damp_array($menu_item) ;
     $id_types=array() ;
     $recs_121=execSQL('select * from obj_site_list where clss=121',2) ;
     $recs_123=execSQL('select * from obj_site_list where clss=123',2) ;
     execSQL_update('delete from '.$category_system->table_name.' where temp=1') ;
     if (sizeof($recs_121)) foreach($recs_121 as $rec)
     {  $id_types[$rec['pkey']]=adding_rec_to_table($category_system->tkey,array('parent'=>$menu_item['pkey'],'clss'=>60,'enabled'=>$rec['enabled'],'indx'=>$rec['indx'],'obj_name'=>$rec['obj_name'],'c_data'=>$rec['c_data'],'r_data'=>$rec['r_data'],'id'=>$rec['id'],'url_name'=>$rec['url_name'],'temp'=>1)) ;
        // удаляем запись по типу из списков
        //execSQL_update('delete from obj_site_list where pkey='.$rec['pkey']) ;
     }
     if (sizeof($recs_123)) foreach($recs_123 as $rec)
     {  adding_rec_to_table($category_system->tkey,array('parent'=>$id_types[$rec['parent']],'clss'=>61,'enabled'=>$rec['enabled'],'indx'=>$rec['indx'],'obj_name'=>$rec['obj_name'],'c_data'=>$rec['c_data'],'r_data'=>$rec['r_data'],'id'=>$rec['id'],'url_name'=>$rec['url_name'],'input'=>$rec['input'],'list_id'=>$rec['list_id'],'temp'=>1)) ;
        // цепляем на корень списки, относящиеся к свойствам товара
        //if ($rec['list_id']) execSQL_update('update obj_site_list set parent=1 where pkey='.$rec['list_id']) ;
        // удаляем запись по свойству из списков
        //execSQL_update('delete from obj_site_list where pkey='.$rec['pkey']) ;
     }
 }

function convert_type_to_links()
 { global $goods_system ;
   ?><h2>Преобразовать значение поля "type" в линки</h2><?
   $recs=execSQL('select pkey,obj_name,type from obj_site_goods where type>0',2) ;


 }

function view_links_goods_types()
{ global $goods_system,$category_system ;
  $recs=execSQL('select t.pkey as id,t.o_id,t.o_tkey,t.o_clss,t.p_id,t.p_tkey,t.p_clss,tg1.obj_name as g_name1,tg2.obj_name as g_name2,tc1.obj_name as c_name1,tc2.obj_name as c_name2 from '.TM_LINK.' t
                 left join obj_site_goods tg1 on tg1.pkey=t.o_id
                 left join obj_site_goods tg2 on tg2.pkey=t.p_id
                 left join obj_site_category tc1 on tc1.pkey=t.o_id
                 left join obj_site_category tc2 on tc2.pkey=t.p_id
                 where (t.o_tkey='.$goods_system->tkey.' and t.p_tkey='.$category_system->tkey.') or (t.o_tkey='.$category_system->tkey.' and t.p_tkey='.$goods_system->tkey.')
                ') ;
  print_2x_arr($recs) ;
}

function update_goods_type_by_obj_name()
{  global $category_system ;
   $list_goods=execSQL('select pkey,obj_name,source_url from obj_site_goods where clss=200 and type=0') ;
   if (sizeof($list_goods)) foreach($list_goods as $rec)
   { $type=0 ;
     if (stripos($rec['obj_name'],'Плитка')!==false) $type=612 ;
     elseif (stripos($rec['obj_name'],'плитка')!==false) $type=612 ;
     elseif (stripos($rec['obj_name'],'Мозаика')!==false) $type=677 ;
     elseif (stripos($rec['obj_name'],'мозаика')!==false) $type=677 ;
     elseif (stripos($rec['obj_name'],'Керамогранит')!==false) $type=676 ;
     elseif (stripos($rec['obj_name'],'керамогранит')!==false) $type=676 ;
     elseif (stripos($rec['obj_name'],'Керамоганит')!==false) $type=676 ;
     elseif (stripos($rec['obj_name'],'Керамогаранит')!==false) $type=676 ;
     elseif (stripos($rec['obj_name'],'Клинкер')!==false) $type=499 ;
     elseif (stripos($rec['obj_name'],'Подступенок')!==false) $type=499 ;
     elseif (stripos($rec['obj_name'],'Керамическая ступень')!==false) $type=499 ;
     elseif (stripos($rec['obj_name'],'Ступень')!==false) $type=499 ;
     elseif (stripos($rec['obj_name'],'Декор')!==false) $type=608 ;
     elseif (stripos($rec['obj_name'],'декор')!==false) $type=608 ;
     elseif (stripos($rec['obj_name'],'Decor')!==false) $type=608 ;
     elseif (stripos($rec['obj_name'],'Бордюр')!==false) $type=607 ;
     elseif (stripos($rec['obj_name'],'бордюр')!==false) $type=607 ;
     elseif (stripos($rec['obj_name'],'Борюр')!==false) $type=607 ;
     elseif (stripos($rec['obj_name'],'Бусинки')!==false) $type=607 ;
     elseif (stripos($rec['obj_name'],'Карандаш')!==false) $type=607 ;
     elseif (stripos($rec['obj_name'],'Вставка')!==false) $type=609 ;
     elseif (stripos($rec['obj_name'],'вставка')!==false) $type=609 ;
     elseif (stripos($rec['obj_name'],'Панно')!==false) $type=610 ;
     elseif (stripos($rec['obj_name'],'панно')!==false) $type=610 ;
     elseif (stripos($rec['obj_name'],'Ковер')!==false) $type=610 ;
     elseif (stripos($rec['obj_name'],'Плинтус')!==false) $type=611 ;
     elseif (stripos($rec['obj_name'],'плинтус')!==false) $type=611 ;
     if ($type)
     { update_rec_in_table('obj_site_goods',array('type'=>$type),'pkey='.$rec['pkey']) ;
       echo '<strong>'.$rec['obj_name'].'</strong>  => '.'<strong>'.$category_system->tree[$type]->name.'</strong><br>' ;
     }
     else echo ''.$rec['obj_name'].': <strong>'.$rec['source_url'].'</strong><br>' ;
   }
}

?>