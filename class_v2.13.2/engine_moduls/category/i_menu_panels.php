<?

 // панель свойств типа товара
 // $_SESSION['descr_clss'][60]['menu'][]=array("name" => "Свойства",'script'=>'mod/category/i_menu_panel.php',"cmd" =>'panel_props_of_type') ;
 function panel_props_of_type()
  { global $obj_info ;
    $cnt=_CLSS(61)->show_list_items($obj_info['tkey'],'parent='.$obj_info['pkey'],array('cur_obj_rec'=>$obj_info,'debug'=>0)) ;
    if (!$cnt['all']) {?><button mode="append_viewer_main" cmd="create_obj" clss="61" parent_reffer="<?echo $obj_info['_reffer']?>">Добавить свойство</button><?}
  }

 // панель списов для типа товара
 // $_SESSION['descr_clss'][60]['menu'][]=array("name" => "Списки",'script'=>'mod/category/i_menu_panels.php',"cmd" =>'panel_list_of_type') ;
 function panel_list_of_type()
  { global $obj_info ;
   // получаем id списков всех свойств
   $list_props=select_db_recs($obj_info['tkey'],'parent='.$obj_info['pkey'].' and clss=61 and input=1',array('no_image'=>1)) ;
   $tkey_list=_DOT(TM_LIST)->pkey ;
   //print_2x_arr($list_props) ;
   if (sizeof($list_props)) foreach($list_props as $rec_prop) // идем по списку свойств
   {  ?><h2><?echo $rec_prop['obj_name']?></h2><?
      _CLSS(20)->show_list_items($tkey_list,'parent='.$rec_prop['list_id'],array('no_table_header'=>1,'title'=>$rec_prop['obj_name'],'no_icon_edit'=>1,'no_icon_photo'=>1,'cur_obj_rec'=>array('_reffer'=>$rec_prop['list_id'].'.'.$tkey_list))) ;
       echo '<br>' ;
   }
  }

// показываем товары текущего типа
function panel_view_goods_of_type()
  { global $obj_info,$TM_goods,$obj_list_brands ;
    //$obj_list_brands=get_obj_info('1366.50') ; //damp_array($obj_list_brands) ;
    _CLSS(200)->show_list_items($TM_goods,'clss=200 and type='.$obj_info['pkey'],array('debug'=>0,'_cur_obj_rec'=>$obj_list_brands)) ;


  }

// показываем товары текущего типа
function panel_view_goods_null_type()
  { global $obj_info,$TM_goods,$goods_system ;
    $ids=$goods_system->tree[$obj_info['pkey']]->get_list_child() ;
    //echo '$ids='.$ids.'<br>' ;
    _CLSS(200)->show_list_items($TM_goods,'clss=200 and (type=0 or type is null) and parent in ('.$ids.')',array('debug'=>0)) ;
    //_CLSS(200)->show_list_items($TM_goods,'clss=200 and (obj_name="" or obj_name is null) and source_url like \'%terracorp.ru%\' and parent in ('.$ids.')',array('debug'=>0)) ;
    //_CLSS(200)->show_list_items($TM_goods,'clss=200 and (obj_name="" or obj_name is null) and source_url like \'%santehmega.com%\' ',array('debug'=>0)) ;
  }

function panel_prop_list_values($options)
  { $rec_prop=$GLOBALS['cur_page']->obj_info ;
    //damp_array($rec_prop) ;
    $tkey=_DOT(TM_LIST)->pkey ;
    switch($rec_prop['input'])
    { case 1:   ?><h2>Тип поля - <strong>элемент списка</strong></h2><?
                _CLSS(20)->show_list_items($tkey,'parent='.$rec_prop['list_id'],array('cur_obj_rec'=>array('_reffer'=>$rec_prop['list_id'].'.'.$tkey,'debug'=>2))) ; // показываем значения из списка, list_id которого указан в записи
                break ;
      case 2:   ?><h2>Тип поля - <strong>checkbox</strong></h2><?
                echo 'Поле выводится с помощью элемента <strong>checkbox</strong>' ;
                break ;
      case 3:   ?><h2>Тип поля - <strong>число</strong></h2><?
                $recs=execSQL_line('select distinct(num) from obj_site_goods_props where type='.$rec_prop['parent'].' and id='.$rec_prop['id'].' order by num') ;
                damp_array($recs,1,-1) ;
                break ;
      case 4:   ?><h2>Тип поля - <strong>строка</strong></h2><?

                $recs=execSQL_row('select str,count(pkey) from obj_site_goods_props where type='.$rec_prop['parent'].' and id='.$rec_prop['id'].' group by str order by str') ;
                ?>Значение свойства, число значений<?
                damp_array($recs,1,-1) ;
                break ;
    }
  }

 function panel_prop_operations()
 {  global $obj_info ;
    $list_lists=execSQL('select pkey,obj_name from '.TM_LIST.' where parent=1 and clss=21') ;
    ?><h1>Изменение типа поля</h1><?
    ?><h2>Текущий тип поля - <strong><?echo $_SESSION['ARR_61_edit_elements'][$obj_info['input']]?></strong></h2>
         <select name="type_to">
             <option value="1">В список</option>
             <option value="2">В флажок</option>
             <option value="3">В число</option>
             <option value="3">В строку</option>
         </select><br>
         Для списка выберите имя существующего списка или "новый список"
         <select name="list_id"><option value="0">Новый список</option><?if (sizeof($list_lists)) foreach($list_lists as $rec){?><option value="<?echo $rec['pkey']?>"><?echo $rec['obj_name']?></option><?}?>
         </select>

     <button class=v1 cmd=panel_prop_change_type script="mod/category/i_menu_panels.php">Изменить</button>
    <?  //damp_array($_SESSION['index_list_names']) ;
 }

 function panel_prop_change_type()
 { $rec_prop=get_obj_info($_POST['reffer']) ;
   $rec_type=get_obj_info($rec_prop['_parent_reffer']) ;
   //damp_array($_POST) ; damp_array($rec_prop) ; damp_array($rec_type) ;
   ?>Текущий тип поля - <strong><?echo $_SESSION['ARR_61_edit_elements'][$rec_prop['input']]?></strong><br>
      Новый тип поля - <strong><?echo $_SESSION['ARR_61_edit_elements'][$_POST['type_to']]?></strong><br>
   <?
   if ($_POST['type_to']!=$rec_prop['input']) switch($_POST['type_to'])
   { case 1: $values=execSQL_line('select distinct(str) from obj_site_goods_props where type='.$rec_prop['parent'].' and id='.$rec_prop['id'].' order by str') ;
             if (sizeof($values)) foreach($values as $id=>$val) if ($val!='None') $values[$id]=trim($val) ;
             if (!$_POST['list_id'])
             { // создаем новый список
               $list_info['tkey']=_DOT(TM_LIST)->pkey;
               $list_info['parent']=1 ; // список - в корнень списков
               $list_info['obj_name']=$rec_type['obj_name'].' - '.$rec_prop['obj_name'] ;
               $list_info['hidden']=1 ;
               //$options['list_items']=implode("\n",$values)  ;
               $list_rec=_CLSS(21)->obj_create($list_info) ;
               $list_id=$list_rec['pkey'] ;
             } else $list_id=$_POST['list_id'] ;
             //damp_array($list_rec) ;
             if ($list_id)
             { // меняем тип свойства
               update_rec_in_table($rec_prop['tkey'],array('list_id'=>$list_id,'input'=>1),'pkey='.$rec_prop['pkey']) ;
               // загружаем индексы и значения списка
               if (sizeof($values)) foreach($values as $val)
               {  $id=_IL($list_id)->get_id_of_value($val,array('append_value_is_not_found'=>1)) ;
                  if ($id) echo '<strong>'.$val.' => '.$id.'</strong> для prop_id='.$rec_prop['id'].', type='.$rec_type['pkey'].'<br>' ;
                   { $res=update_rec_in_table('obj_site_goods_props',array('value'=>$id,'str'=>''),'str="'.$val.'" and id='.$rec_prop['id'].' and type='.$rec_type['pkey']) ;
                     unset($res['sql']) ;
                     print_1x_arr($res) ;
                   }
               }
                 // меняем тип свойства в таблице свойст объектов
               //
             }
             break ;
      case 2: break ;
      case 3: // ПЕРЕВОДИМ В СТРОКУ В ЧИСЛО
              // меняем тип свойства
              update_rec_in_table($rec_prop['tkey'],array('input'=>3),'pkey='.$rec_prop['pkey']) ;
              // переносим значения в другой столбец
              $values=execSQL_line('select distinct(str) from obj_site_goods_props where type='.$rec_prop['parent'].' and id='.$rec_prop['id'].' order by str') ;
              if (sizeof($values)) foreach($values as $id=>$val)
              { echo '<strong>'.$val.'</strong> для prop_id='.$rec_prop['id'].', type='.$rec_type['pkey'].'<br>' ;
                $res=update_rec_in_table('obj_site_goods_props',array('num'=>$val,'str'=>''),'str="'.$val.'" and id='.$rec_prop['id'].' and type='.$rec_type['pkey']) ;
                unset($res['sql']) ;
                print_1x_arr($res) ;
              }
              break ;

   } else echo '<div class="alert">Тип поля не изменен</div>' ;
 }

 function panel_dump_props()
 {  global $obj_info ;
    $rec_type=get_obj_info($obj_info['_parent_reffer']) ;
    $recs=execSQL('select pkey as _pkey,id,value,num,str,type from obj_site_goods_props where id='.$obj_info['id'].' and type='.$rec_type['pkey']) ;
    print_2x_arr($recs) ;

 }

?>