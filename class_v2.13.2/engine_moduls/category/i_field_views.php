<?

// вывод числа товаров в категории
// $_SESSION['descr_clss'][60]['list']['field']['_view_count']	=array('title'=>'Кол-во товаров','view'=>'panel_view_count_of_type', 'script'=>'mod/category/i_field_views.php') ;
function panel_view_count_of_type($rec,$options=array())
 { $count=execSQL_value('select count(pkey) from obj_site_goods where type='.$rec['pkey'].' and clss=200') ;
   //execSQL_value('select * from obj_site_goods where type='.$rec['pkey'].' and clss=200',2) ;
   return($count) ;
 }
function panel_view_count_of_type_act($rec,$options=array())
 { $count=execSQL_value('select count(pkey) from view_goods where type='.$rec['pkey'].' and clss=200') ;
   //execSQL_value('select * from obj_site_goods where type='.$rec['pkey'].' and clss=200',2) ;
   return($count) ;
 }

// показ основной категории  товара в разделе
// $_SESSION['descr_clss'][10]['fields']['type']=array('type'=>'int(11)','view'=>'view_main_type','script'=>'mod/category/i_field_views.php') ;
// $_SESSION['descr_clss'][10]['list']['field']['type']='Основной тип товара' ;
function view_main_type($rec,$options=array())
{ global $category_system ;
  $text=$category_system->tree[$rec['type']]->name ;
  // возвращаем параметры для td, так как для открывтмя окна нужен клик по ячейке
  return(array($text,'','class=v2 cmd="get_panel_select_main_type" script="mod/category" no_send_form=1')) ;
}

// показ дополнит категории  товара в разделе
// $_SESSION['descr_clss'][10]['list']['field']['_stype']=array('title'=>'Дополнительные типы','view'=>'view_second_type','script'=>'mod/category/i_field_views.php') ;
 function view_second_type($rec,$options=array())
 { $arr_links=get_arr_links_names($rec) ;
   $str_links=(sizeof($arr_links))? implode('<br>',$arr_links):'' ;
   $text='<div id="links_to_'.$rec['pkey'].'">'.$str_links.'</div><div class="v2" cmd="get_panel_select_second_type" script="mod/category" no_send_form=1>+</div>' ;
   // возвращаем только текст, так как для открытия окна нужно кликнуть по +
   return($text) ;
 }

// показ дополнит категории  товара в разделе
// $_SESSION['descr_clss'][10]['list']['field']['_alltype']=array('title'=>'Типы дочерних<br>объектов','td_class'=>'left','view'=>'view_child_types','script'=>'mod/category/i_field_views.php') ;
 function view_child_types($rec,$options=array())
 { global $category_system ;
   ob_start() ;
   $recs=$category_system->get_types_count_info($rec['pkey']) ;
   if (sizeof($recs))foreach($recs as $type=>$count)
   { $name=($type)? $category_system->tree[$type]->name:'Товары без типа' ;
     $arr[]='<span class=nowrap>'.$name.' ['.$count.']</span>' ;
   }
   if (sizeof($arr)) echo implode('<br>',$arr) ;
   $text=ob_get_clean() ;
   return($text) ;
 }

// показ вех категорий товара
// $_SESSION['descr_clss'][200]['fields']['type']=array('type'=>'int(11)','default'=>0,'view'=>'view_all_goods_type','script'=>'mod/category/i_field_views.php') ;
function view_all_goods_type($rec,$options=array())
{ global $category_system ;
  $text='' ;
  if ($rec['type'] and $category_system->tree[$rec['type']]->enabled)  $text=$category_system->tree[$rec['type']]->name ;

  // возвращаем параметры для td, так как для открывтмя окна нужен клик по ячейке
  return(array($text,'','class=v2 cmd="get_panel_select_type_goods" script="mod/category" no_send_form=1')) ;
}

//показ свойств товара
function view_goods_props($rec,$options=array())
{  global $category_system ;
   $arr=$category_system->get_array_props_info($rec) ; // возвращаем список описания свойст для товрара по типу товара (поле type)
   $text=(sizeof($arr))? show_table_props_value($arr,$rec['tkey'],$rec['pkey'],$rec['clss']):'' ;
   //$text.=(sizeof($arr))? '<button class=v2 no_send_form=1 cmd=category#get_props_panel reffer="'.$rec['_reffer'].'">Свойства</button>':'' ;
   return($text) ;
}

// показ числа использования свойства в товарах
// $_SESSION['descr_clss'][61]['list']['field'][]	    	        =array('title'=>'Задано значений','view'=>'get_count_type_values','script'=>'mod/category/i_field_views.php') ;
function get_count_type_values($rec,$options=array())
{ //damp_array($rec) ;
  $value=execSQL_value('select count(pkey) from '.$_SESSION['TM_goods_props'].' where id='.$rec['id'].' and type='.$rec['parent']) ;
  return($value) ;
}

function get_type_indx_list_values($rec,$options=array())
 { $value='' ;
   if ($rec['input']==1)
    {  $arr_name=$_SESSION['index_list_names'][$rec['list_id']] ;// echo $arr_name.'<br>' ;
       if ($arr_name)
       { $arr=array() ;
       if (sizeof($_SESSION[$arr_name])) foreach($_SESSION[$arr_name] as $rec) $arr[]=$rec['obj_name'] ;
       $value=implode(', ',$arr) ;
       } else $value='Не найден набор значений' ;
    }
   if ($rec['input']==3)
   {  $info=execSQL_van('select min(num) as min,max(num) as max from '.$_SESSION['TM_goods_props'].' where id='.$rec['id'].' and num!=0 and type='.$rec['parent']) ;
      $value='от '.($info['min']*1).' до '.($info['max']*1) ;
   }
   //damp_array($rec) ;
   return($value) ;
 }


function view_photo_info($rec,$options)
{ //trace() ;
   $photo_info=execSQL_van('select * from obj_site_stats_uploads  where goods_id='.$rec['parent'].' and img_name="'.$rec['obj_name'].'"') ;
   if ($photo_info['pkey'])
   { $autor_info=execSQL_van('select obj_name from obj_site_account where pkey='.$photo_info['member_id']) ;
     $text='<strong>'.$autor_info['obj_name'].'</strong><br>Загружено '.date('d.m.Y H:i',$photo_info['c_data']).'<br> из '.$photo_info['file'] ;
   } else $text='Нет данных' ;
   return($text) ;



}



?>