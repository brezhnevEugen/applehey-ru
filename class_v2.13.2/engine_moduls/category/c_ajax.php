<?
include_once(_DIR_TO_ENGINE.'/c_page_XML_AJAX.php') ;
class c_page_ajax extends c_page_XML_AJAX
{
    function get_props_panel()
    {  global $category_system ;
       $this->add_element('success','modal_window') ;
       $this->add_element('modal_panel_width','600') ;
       $this->add_element('title','Свойства объекта') ;

       $rec=get_obj_info($_POST['reffer']) ;
       ?><h1><?echo $rec['obj_name']?></h1><?
       //damp_array($_POST) ;
       //damp_array($rec) ;
       $arr=$category_system->get_array_props_info($rec) ; // возвращаем список описания свойст для товрара по типу товара (поле type у товара или родителя товара)
       echo show_table_props_value($arr,$rec['tkey'],$rec['pkey'],$rec['clss']) ;
    }

    // возвращем массив для выпадающего списка свойства эдемента
    // передается только reffer объекта и id свойства
    // все остальные данные необходимо получить из базы самостоятельно
    // ответ функции перехватывает admin/script.js:onclick_gmc2_AJAX_success
    function get_props_edit()
        { global $category_system ;
          $obj_rec=array() ;
          if (isset($_POST['reffer'])) $obj_rec=get_obj_info($_POST['reffer']) ;
          if ($obj_rec)
          {   $this->add_element('prop_id',$_POST['prop_id']) ;
              // получаем информацию по свойству на основе $_POST['reffer'] и $_POST['prop_id']
              $props_rec=$category_system->get_array_props_info($obj_rec) ;
              $prop_info=$props_rec[$_POST['prop_id']] ; //print_r($prop_info) ;

              $text='' ;
              if ($prop_info['indx_select'])
              {   $arr=$_SESSION[$prop_info['indx_select']] ;
                  // получаем массив значений для свойства
                  $prop_values=execSQL_line('select value from obj_site_goods_props where parent='.$obj_rec['pkey'].' and id='.$_POST['prop_id']) ;
                  $str=array() ;
                  if (sizeof($arr)) foreach($arr as $id=>$rec)
                  { $checked=(in_array($id,$prop_values))? ' checked':'' ;
                    $title=(is_array($rec))? $rec['obj_name']:$rec ;
                    $str[]='<label style="white-space:nowrap;"><input type="checkbox" class=v2 cmd=set_prop_value script="mod/category" prop_id="'.$_POST['prop_id'].'"  value="'.$id.'" '.$checked.' no_send_form=1>&nbsp;'.$title.'</label>' ;
                  }
                  if (sizeof($str)) $text='<div class=left>'.implode('<br>',$str).'</div>';
              }
              if ($prop_info['edit_element']=='input_num')
              {   $prop_value=execSQL_value('select num from obj_site_goods_props where parent='.$obj_rec['pkey'].' and id='.$_POST['prop_id']) ;
                  $text='<input type="text" class="text v2" cmd=set_prop_value script="mod/category" value="'.($prop_value*1).'" prop_id="'.$_POST['prop_id'].'"  no_send_form=1>' ;
              }
              if ($prop_info['edit_element']=='input_str')
              {   $prop_value=execSQL_value('select str from obj_site_goods_props where parent='.$obj_rec['pkey'].' and id='.$_POST['prop_id']) ;
                  $text='<input type="text" class="text big v2" cmd=set_prop_value script="mod/category" value="'.htmlspecialchars($prop_value).'" prop_id="'.$_POST['prop_id'].'"  no_send_form=1>' ;
              }

              if ($text) $this->add_element('prop_'.$obj_rec['pkey'].'_'.$_POST['prop_id'].'.HTML',$text) ;
          }
        }



     // ajax сохранение данных для свойств объектов, сохряняет значения свойств напрямую, минуя save_list_obj
     // reffer:11117.136 - объект (товар)
     // prop_id:1 - свойство
     // value:3 - выбранный элемент
     // checked:1 - статус выбранного элемента
     function set_prop_value()
     {  global $category_system,$goods_system ;
         $obj_rec=array() ; //print_r($_POST) ;
        //if (isset($_POST['reffer'])) $obj_parent=get_obj_parent($_POST['reffer']) ;
        if (isset($_POST['reffer'])) $obj_rec=get_obj_info($_POST['reffer']) ;
        $arr_tree_clss=explode(',',$goods_system->tree_clss) ;
        if ($obj_rec and $_POST['prop_id'])
        { $type=$category_system->get_obj_type($obj_rec) ;
          $prop_info=$_SESSION['IL_goods_types'][$type]['_props'][$_POST['prop_id']] ;// print_r($prop_info) ;
          if (!in_array($obj_rec['clss'],$arr_tree_clss)) $category_system->save_prop_value($obj_rec['pkey'],$type,$_POST['prop_id'],$_POST['value'],$_POST['checked'],$prop_info) ;
          else
          {  $ids=$goods_system->tree[$obj_rec['pkey']]->get_list_child() ;
             $goods_ids=execSQL_line('select pkey from obj_site_goods where clss in ('.$goods_system->clss_items.') and type='.$type.' and parent in ('.$ids.')') ;
             if (sizeof($goods_ids)) foreach($goods_ids as $goods_id)
                 $category_system->save_prop_value($goods_id,$type,$_POST['prop_id'],$_POST['value'],$_POST['checked'],$prop_info) ;
             print_r($goods_ids) ;
          }
        }


     }



   /*
   function get_types_in_select()
   { global $category_system ;
     ob_start() ;
     $rec=get_obj_info($_POST['reffer']) ;
     ?><select><? echo $category_system->tree['root']->get_select_option($rec['type'])?></select><?
     $text=ob_get_clean() ;
     $this->add_element('td.HTML',$text,array('reffer'=>$_POST['reffer'],'fn'=>$_POST['fn'])) ;
     $this->add_element('td.HTML',$text,array('reffer'=>$_POST['reffer'],'fn'=>$_POST['fn'])) ;
   } */

   // вернуть модальную панель выбора основного типа товаров
   function get_panel_select_main_type()
   { $rec=get_obj_info($_POST['reffer']) ;
     include('panel_select_obj_to_link.php') ;
     $sel_types=($rec['type'])? array($rec['type']):array() ;
     panel_select_obj_to_link(array('reffer'=>$_POST['reffer'],'mode'=>'main','select_ids'=>$sel_types)) ;
     $this->success='modal_window' ;
     $this->modal_panel_title='Выбор основной категории товара' ;
   }

   // вернуть модальную панель выбора дополнительного типа товара
   function get_panel_select_second_type()
   {  $rec=get_obj_info($_POST['reffer']) ;
      if (!$rec['type'])
      { $this->add_element('success','show_notife',array('type'=>'info','pos_x'=>'center','pos_y'=>'center')) ;
        echo 'Сначала необходимо задать основную категорию товара' ;
      }
      else
      { include('panel_select_obj_to_link.php') ;
        $str_links=get_linked_ids_to_rec($_POST['reffer']) ;
        $select_type_ids=($str_links)? explode(',',$str_links):array() ;
        panel_select_obj_to_link(array('reffer'=>$_POST['reffer'],'mode'=>'second','select_ids'=>$select_type_ids)) ;
        $this->success='modal_window' ;
        $this->modal_panel_title='Выбор дополнительной категории товара' ;
      }
   }

   // обновление основного типа товара раздела
   // $_POST['reffer'] - раздел
   // $_POST['value'] - тип товара
   function update_main_type()
   {  global $TM_goods,$category_system,$goods_system ;
      $rec=get_obj_info($_POST['reffer']) ;
      $type=$_POST['value'] ;
      if ($rec['pkey'])  update_rec_in_table($TM_goods,array('type'=>$type),'pkey='.$rec['pkey']) ;
      // после задания основного типа товара необходимо переопределить основной тип товара у дочерних товаров
      $list_childs=$goods_system->tree[$rec['pkey']]->get_list_child() ;
      execSQL_update('update '.$TM_goods.' set type='.$type.' where parent in ('.$list_childs.') and clss in (2,200) and (type is null or type=0)') ;

      $this->add_element('td.HTML',$category_system->tree[$type]->name,array('reffer'=>$_POST['reffer'],'fn'=>'type')) ;
   }

   // $_POST['reffer'] - раздел
   // $_POST['link_reffer'] - категория

   function update_second_type()
   {  if ($_POST['checked']) create_link($_POST['reffer'],$_POST['link_reffer']) ;
      else                   delete_link($_POST['reffer'],$_POST['link_reffer']) ;
      // получаем список слинкованных категорий - они заменят старый список в ячейке
      $arr_links=get_arr_links_names($_POST['reffer']) ;
      $str_links=(sizeof($arr_links))? implode('<br>',$arr_links):'' ;
      list($pkey,$tkey)=explode('.',$_POST['reffer']) ;
      $text='<div id="links_to_'.$pkey.'">'.$str_links.'</div>' ;
      $this->add_element('links_to_'.$pkey,$text) ;
   }

   // обновление основного типа товара раздела
   // $_POST['reffer'] - раздел
   // $_POST['value'] - тип товара
   function update_goods_type()
   {  global $category_system ;
      $rec=get_obj_info($_POST['reffer']) ;
      $arr['type']=$_POST['value'] ;
      if ($rec['pkey']) update_rec_in_table($rec['tkey'],$arr,'pkey='.$rec['pkey']) ;

      $_text=array() ;
      if ($arr['type'])  $_text[]=$category_system->tree[$arr['type']]->name ;
      $text=(sizeof($_text))? implode('<br>',$_text):'' ;

      $this->add_element('td.HTML',$text,array('reffer'=>$_POST['reffer'],'fn'=>'type')) ;
   }


   function update_sect_1c_type()
   {  ob_start() ;
      global $category_system ;
       $fname='' ; $parent=0 ; $usl_cat='' ;
      switch($_POST['cat'])
      { case 1: $parent=2 ; $fname='type_m' ; $usl_cat=' and m=1' ; break ;
        case 2: $parent=298 ; $fname='type_w' ; $usl_cat=' and w=1' ; break ;
        case 3: $parent=594 ; $fname='type_k' ; $usl_cat=' and k=1' ;break ;
      }
      // обновляем запись в таблице соответствий категорий 1С и сайта
      if ($parent) update_rec_in_table('obj_site_cat_1c',array('type'=>$_POST['type_section']),'obj_name="'.$_POST['sec_name'].'" and parent='.$parent,array('debug'=>2)) ;
      // обновляем запись в раздеах
      if ($fname) update_rec_in_table('obj_site_goods',array($fname=>$_POST['type_section']),'obj_name="'.$_POST['sec_name'].'" and clss=10',array('debug'=>2)) ;
      // обновляем типы у товаров и сортамента на оставе данных в разделах каталога
      if ($fname)
      {   $sect_ids=execSQL_line('select pkey from obj_site_goods where '.$fname.'='.$_POST['type_section']) ;
          if (sizeof($sect_ids))
          { execSQL_update('update obj_site_goods set type='.$_POST['type_section'].' where (type=0 or type is null) '.$usl_cat.' and parent in ('.implode(',',$sect_ids).')',2) ;
            execSQL_update('update obj_site_1c set type='.$_POST['type_section'].' where (type=0 or type is null) and  categor='.$_POST['cat'].' and section_id in ('.implode(',',$sect_ids).')',2) ;
          }

      }



       //
     $text=ob_get_clean() ;
     _event_reg('Измерение сопоставление раздела',$text) ;
     $this->add_element('show_notify','Изменено') ;
     return ;




      $rec=get_obj_info($_POST['reffer']) ;
      $arr['type']=$_POST['value'] ;
      update_rec_in_table($rec['tkey'],$arr,'pkey='.$rec['pkey']) ;
      // обновляем тип товара у всех товаров внутри разделов с таким названиые согласно пола текущего списка
      // получаем id всех разделов с названием текущего раздела 1С
      $sections_ids=execSQL_line('select pkey from obj_site_goods where clss=10 and obj_name="'.$rec['obj_name'].'"') ;
       $usl=''; $res1=0 ;$res2=0 ;
      switch($rec['parent'])
        { case 2:   $usl=' and m=1' ; break ;
          case 298: $usl=' and w=1' ; break ;
          case 594: $usl=' and k=1' ; break ;
        }
      if ($usl and sizeof($sections_ids))
      { $res1=execSQL_update('update obj_site_goods set type='.$arr['type'].' where clss=200 and enabled=1 and _image_name!="" and parent in ('.implode(',',$sections_ids).') '.$usl) ;
        $res2=execSQL_update('update obj_site_goods set type='.$arr['type'].' where clss=200 and parent in ('.implode(',',$sections_ids).') '.$usl) ;

      }
      // обновляем название типа в ячейке
      $text=$category_system->tree[$arr['type']]->name ;
      if ($res1) $text.='<div class="green">Заданы типы у <strong>'.$res1.'</strong> активных товаров</div>';
      if ($res2) $text.='<div class="green">Заданы типы у <strong>'.$res2.'</strong> неактивных товаров</div>';
      $this->add_element('td.HTML',$text,array('reffer'=>$_POST['reffer'],'fn'=>'type')) ;

   }



   function get_panel_select_type_goods()
   {  global $category_system ;
      $rec=get_obj_info($_POST['reffer']) ;
      // надо выяснить какого типа его родитель
      //$allow_type_ids=$this->get_types_ids($rec['_parent_reffer']) ;
      $select_type_ids=array() ;
      if ($rec['type'])  $select_type_ids[]=$rec['type'] ;
      $root_type_id=1 ;
      //echo 'id='.$rec['pkey'].'<br>' ;
      //print_r($rec) ;
      //print_r($_POST) ;
      $rec=get_obj_info($_POST['reffer']) ;
       //damp_array($rec) ;
      if ($rec['clss']==200)
      { if ($rec['m']==1) $_POST['cat']=1 ;
        if ($rec['w']==1) $_POST['cat']=2 ;
        if ($rec['k']==1) $_POST['cat']=3 ;

      }
      //echo 'cat='.$_POST['cat'] ;
      switch($_POST['cat'])
      { case 1: $root_type_id=773 ; break ;
        case 2: $root_type_id=774 ; break ;
        case 3: $root_type_id=775 ; break ;
      }
      $allow_type_ids=array() ;
      include('panel_select_obj_to_link.php') ;
      $mode=($_POST['mode'])? $_POST['mode']:'goods' ; 
      panel_select_obj_to_link(array('reffer'=>$_POST['reffer'],'sect_name'=>$_POST['sect_name'],'cat'=>$_POST['cat'],'mode'=>$mode,'select_ids'=>$select_type_ids,'include_ids'=>$allow_type_ids,'root_id'=>$root_type_id)) ;
      $this->success='modal_window' ;
      $this->modal_panel_title='Выбор категории товара' ;
   }

   function get_types_ids($reffer)
   {  global $goods_system ;
      list($pkey,$tkey)=explode('.',$reffer) ;   $type=array() ; $main_type=0 ;
      if (isset($GLOBALS['parents_type'][$reffer])) return($GLOBALS['parents_type'][$reffer]) ;
      else if (isset($goods_system->tree[$pkey]))
         { // вытаскиваем основной тип товара
           $parent_ids=$goods_system->tree[$pkey]->get_list_parent() ;
           $arr_type=execSQL_row('select pkey,type from '.$goods_system->table_name.' where pkey in ('.$parent_ids.') order by pkey') ;
           $art_parent_ids=array_reverse(explode(',',$parent_ids)) ;
           if (sizeof($art_parent_ids)) foreach($art_parent_ids as $parent_id) if ($arr_type[$parent_id]) { $main_type=$arr_type[$parent_id] ; break ; }
           // вытаскиваем дополнительные типы товара
           if ($main_type)
            { $type[]=$main_type  ;
              $str_links=get_linked_ids_to_rec($parent_id.'.'.$tkey,array('link_clss'=>60)) ;
              //echo '$str_links='.$str_links.'<br>' ;
              if ($str_links)
              { $arr_type_sec=explode(',',$str_links) ;
                if (sizeof($arr_type_sec)) foreach($arr_type_sec as $type_sec_id) if ($type_sec_id!=$main_type) $type[]=$type_sec_id ;
              }
            }
           // запоминмаем в GLOBAL чтобы при следующем товаре у аналогичного parent не делать второй раз звапрос
           $GLOBALS['parents_type'][$reffer]=$type ;
         }
         return($type) ;
   }

}
