<?php
$__functions['init'][]		='_category_admin_vars' ;
$__functions['install'][]	='_category_install_modul' ;
$__functions['boot_admin'][]='_category_admin_boot' ;

// настройка добавления свойств для товаров
// 2. Добавить в таблицу "списки" поддержку классов 60 и 61 (in)
// 3. Добавить в таблицу "товары" поддерку класса 6 (out)
// 3. Создать в списках список "Типы товаров", код списка "goods_types"
// 4. Добавить в init.php: _DOT($TM_list)->rules[]=array('to_pkey'=>.....,'only_clss'=>60) ;  // в таблицe "списки" в объект "Типы товаров" можно добавлять только "типы товаров"
// 5. Добавить в init.php для clss 10 и clss2:
//
//      $_SESSION['descr_clss'][10]['list']['field']['type']		    ='Тип товара' ;

//     $_SESSION['descr_clss'][2]['list']['field'][]                     =array('title'=>'Характеристики',   'on_view'=>'view_goods_props') ;
//
// 6. ВНИМАНИЕ!!! у таблицы obj_site_goods_props необходимо удалить индекс ALTER TABLE obj_site_goods_props DROP UNIQUE  parent_id (parent,id) если он существует
//
//
//
//
//
//
//

function _category_admin_vars() //
{
    global $TM_category ;
    $_SESSION['ARR_ed_izm']=array(1=>'М2',2=>'шт.',3=>'МП',4=>'уп.') ;
    $_SESSION['ARR_61_edit_elements']=array(1=>'Cписок',2=>'Флажок',3=>'Число',4=>'Строка') ;
    $_SESSION['TM_goods_props']='obj_'.SITE_CODE.'_goods_props' ;

    // подключаем вкладку операций для корня каталогизатора
    ext_panel_register(array('frame'=>array('table_code'=>'category','is_root'=>1),'menu_item'=>array('name'=>'Операции','script'=>'mod/category/i_category_root_operation_actions.php','cmd'=>'panel_category_root_operation_actions'))) ;
    //ext_panel_register(array('frame'=>array('table_code'=>'goods','clss'=>array(10,201,210)),'menu_item'=>array('name'=>'Товары без типа','script'=>'mod/category/i_menu_panels.php','cmd'=>'panel_view_goods_null_type'))) ;

    /*
    ext_panel_register(array('frame'=>array('table_code'=>'category','is_root'=>1),
                             'menu_items'=>array(array("name" => "Добавить",				"type" => 'structure'),
                                                 array("name" => "Свойства",				"type" => 'prop')
                                                )
                            )
    ) ;
    */

	//-----------------------------------------------------------------------------------------------------------------------------
	// описание переменных модуля
	//-----------------------------------------------------------------------------------------------------------------------------
    //$_SESSION['arr_find_on_field'][$TM_category][1]  	= array('obj_name') ;
    //$_SESSION['arr_find_on_field'][$TM_category][10] 	= array('obj_name','manual') ;
    //$_SESSION['arr_find_on_field'][$TM_category][2] 	= array('pkey','obj_name','art','manual') ;
    //$_SESSION['arr_find_on_field'][$TM_category][200] 	= array('pkey','obj_name','art','manual') ;

	//global $arr_tree_clss_names ; 		$arr_tree_clss_names[200]=array('<span class=black>','art','</span> - ','obj_name') ; // должно описываться в описании класса

	//-----------------------------------------------------------------------------------------------------------------------------
	// описание классов модуля
	//-----------------------------------------------------------------------------------------------------------------------------
    // характеристика
    $_SESSION['descr_clss'][6]['name']='Характеристика' ;
   	$_SESSION['descr_clss'][6]['fields']=array('id'=>'int(11)','type'=>'int(11)','value'=>'int(11)','num'=>'float(10,2)','str'=>'varchar(255)') ;
   	$_SESSION['descr_clss'][6]['parent']=0 ;
   	$_SESSION['descr_clss'][6]['child_to']=array(2,200) ;
    $_SESSION['descr_clss'][6]['no_show_in_tree']					=1 ;
    $_SESSION['descr_clss'][6]['no_create_by_list']					=1 ;
    $_SESSION['descr_clss'][6]['no_show_dialog_to_create']		    =1 ;

   	$_SESSION['descr_clss'][6]['list']['field']['enabled']					='Сост.' ;
   	$_SESSION['descr_clss'][6]['list']['field']['indx']						='Позиция'  ;
   	$_SESSION['descr_clss'][6]['list']['field']['obj_name']					='Наименование' ;
   	$_SESSION['descr_clss'][6]['list']['field']['id']				        =array('title'=>'Код свойства','read_only'=>1) ;
   	$_SESSION['descr_clss'][6]['list']['field']['value']				    =array('title'=>'Значение свойства','read_only'=>1) ;

    // 60 - Тип товара
    $_SESSION['descr_clss'][60]['name']='Тип товара' ;
	$_SESSION['descr_clss'][60]['parent']=1 ;
	$_SESSION['descr_clss'][60]['fields']=array('intro'=>'text','manual'=>'text','href'=>'varchar(255)','url_name'=>'varchar(255)') ;
	$_SESSION['descr_clss'][60]['fields']['goods_name']=array('type'=>'varchar(255)') ;
	$_SESSION['descr_clss'][60]['fields']['top']=array('type'=>'int(1)') ;
	$_SESSION['descr_clss'][60]['fields']['ed_izm']=array('type'=>'indx_select','array'=>'ARR_ed_izm') ;
	$_SESSION['descr_clss'][60]['fields']['subdomain']='varchar(255)' ;
	$_SESSION['descr_clss'][60]['fields']['view_coll']='int(1)' ;
	//$_SESSION['descr_clss'][60]['fields']['1c_name']='varchar(255)' ; // название раздела, используемое в 1С является типом товара. Но название кривые, они должны быть исправлены
    $_SESSION['descr_clss'][60]['parent_to']=array(61,3,60) ;
    $_SESSION['descr_clss'][60]['child_to']=array(1,60) ;

    //$_SESSION['descr_clss'][60]['SEO_tab']=1 ;
    $_SESSION['descr_clss'][60]['menu']=array() ;
    $_SESSION['descr_clss'][60]['menu'][]=array("name" => "Добавить",				"type" => 'structure') ;
    //$_SESSION['descr_clss'][60]['menu'][]=array("name" => "Свойства",				"type" => 'prop') ;
    //$_SESSION['descr_clss'][60]['menu'][]=array("name" => "Операции",               "cmd" => 'show_list_operation') ;
    //$_SESSION['descr_clss'][60]['menu'][]=array("name" => "Информация","action" => 4,'icon'=>'obj_operat.gif') ;
    //$_SESSION['descr_clss'][60]['menu'][]=array("name" => "Поиск",     "cmd" => 'child_search','icon'=>'find_icon.gif') ;
    $_SESSION['descr_clss'][60]['menu'][]=array("name" => "Свойства",'script'=>'mod/category/i_menu_panels.php',"cmd" =>'panel_props_of_type') ;
    $_SESSION['descr_clss'][60]['menu'][]=array("name" => "Списки",'script'=>'mod/category/i_menu_panels.php',"cmd" =>'panel_list_of_type') ;
    $_SESSION['descr_clss'][60]['menu'][]=array("name" => "Товары",'script'=>'mod/category/i_menu_panels.php','cmd'=>'panel_view_goods_of_type') ;

	$_SESSION['descr_clss'][60]['list']['field']['pkey']		='Код' ;
	$_SESSION['descr_clss'][60]['list']['field']['enabled']		='Сост.' ;

	$_SESSION['descr_clss'][60]['list']['field']['indx']		='Позиция'  ;
	$_SESSION['descr_clss'][60]['list']['field']['obj_name']	=array('title'=>'Наименование типа','class'=>'big','td_class'=>'left') ;
	$_SESSION['descr_clss'][60]['list']['field']['goods_name']	=array('title'=>'Наименование товара','class'=>'big','td_class'=>'left') ;
	//$_SESSION['descr_clss'][60]['list']['field']['ed_izm']	    =array('title'=>'Цена за') ;
	$_SESSION['descr_clss'][60]['list']['field']['_view_count']	=array('title'=>'товаров всего','view'=>'panel_view_count_of_type', 'script'=>'mod/category/i_field_views.php') ;
	$_SESSION['descr_clss'][60]['list']['field']['_view_count2']	=array('title'=>'товаров активных','view'=>'panel_view_count_of_type_act', 'script'=>'mod/category/i_field_views.php') ;
	//$_SESSION['descr_clss'][60]['list']['field']['top']	        =array('title'=>'Показывать в каталоге') ;
	//$_SESSION['descr_clss'][60]['list']['field']['1c_name']	=array('title'=>'Название в 1С','read_only'=>1,'td_class'=>'left') ;

	$_SESSION['descr_clss'][60]['details']=$_SESSION['descr_clss'][60]['list'] ;
	$_SESSION['descr_clss'][60]['details']['field']['obj_name']	=array('title'=>'Наименование','edit_element'=>'input','class'=>'big','td_class'=>'left') ;
    $_SESSION['descr_clss'][60]['details']['field']['href']		=array('title'=>'Директория','td_class'=>'left') ;
	$_SESSION['descr_clss'][60]['details']['field']['intro']	=array('title'=>'Краткое описание','class'=>'small','use_HTML_editor'=>1) ;
	$_SESSION['descr_clss'][60]['details']['field']['manual']	=array('title'=>'Описание',		'class'=>'big','use_HTML_editor'=>1) ;



    // свойство типа товара
    $_SESSION['descr_clss'][61]=array() ;
    $_SESSION['descr_clss'][61]['name']='Cвойство для типа товара' ;
    $_SESSION['descr_clss'][61]['parent']=20 ;
    $_SESSION['descr_clss'][61]['fields']['top']=array('type'=>'int(1)') ;
    $_SESSION['descr_clss'][61]['fields']['input']=array('type'=>'indx_select','array'=>'ARR_61_edit_elements') ;
    $_SESSION['descr_clss'][61]['fields']['unit']='varchar(10)' ;  // pkey выпадающего списка
    $_SESSION['descr_clss'][61]['fields']['list_id']='int(11)' ;  // pkey выпадающего списка
    $_SESSION['descr_clss'][61]['fields']['id']='int(11)' ;
    $_SESSION['descr_clss'][61]['parent_to']=array() ;
    $_SESSION['descr_clss'][61]['child_to']=array(60) ;
    //$_SESSION['descr_clss'][61]['no_show_in_tree']=1;
    $_SESSION['descr_clss'][61]['menu'][]=array("name" => "Список значений","cmd" => 'panel_prop_list_values','script'=>'mod/category/i_menu_panels.php') ;
    $_SESSION['descr_clss'][61]['menu'][]=array("name" => "Операции","cmd" => 'panel_prop_operations','script'=>'mod/category/i_menu_panels.php') ;
    $_SESSION['descr_clss'][61]['menu'][]=array("name" => "Дамп свойств","cmd" => 'panel_dump_props','script'=>'mod/category/i_menu_panels.php') ;

    $_SESSION['descr_clss'][61]['list']['field']['id']			    ='Код'  ;
    $_SESSION['descr_clss'][61]['list']['field']['indx']			='Позиция'  ;
    $_SESSION['descr_clss'][61]['list']['field']['enabled']		='Состояние' ;
	$_SESSION['descr_clss'][61]['list']['field']['obj_name']		=array('title'=>'Наименование','edit_element'=>'input','td_class'=>'left') ;
	$_SESSION['descr_clss'][61]['list']['field']['url_name']		=array('title'=>'код в URL','edit_element'=>'input','td_class'=>'left') ;
	$_SESSION['descr_clss'][61]['list']['field']['top']	    	    =array('title'=>'Основная хар-ка') ;
	$_SESSION['descr_clss'][61]['list']['field']['input']	    	=array('title'=>'Тип поля','read_only'=>1) ;
	$_SESSION['descr_clss'][61]['list']['field']['unit']	    	=array('title'=>'Ед.изм.') ;
	$_SESSION['descr_clss'][61]['list']['field'][]	    	        =array('title'=>'Задано значений','view'=>'get_count_type_values','script'=>'mod/category/i_field_views.php') ;
	$_SESSION['descr_clss'][61]['list']['field'][]	                =array('title'=>'Элементы списка','view'=>'get_type_indx_list_values','script'=>'mod/category/i_field_views.php') ;

    $_SESSION['descr_clss'][61]['details']=$_SESSION['descr_clss'][61]['list'] ;


    // дополнение для разделов товарного каталога
    $_SESSION['descr_clss'][10]['fields']['type']               =array('type'=>'int(11)','view'=>'view_main_type','script'=>'mod/category/i_field_views.php') ;
    $_SESSION['descr_clss'][10]['fields']['source_url']         =array('type'=>'varchar(500)') ;

    $_SESSION['descr_clss'][10]['list']['field']['type']        ='Основной тип товара' ;
    $_SESSION['descr_clss'][10]['list']['field']['_stype']      =array('title'=>'Дополнительные типы','view'=>'view_second_type','script'=>'mod/category/i_field_views.php') ;
    $_SESSION['descr_clss'][10]['list']['field']['_alltype']    =array('title'=>'Типы дочерних<br>объектов','td_class'=>'left','view'=>'view_child_types','script'=>'mod/category/i_field_views.php') ;
    $_SESSION['descr_clss'][10]['list']['field']['source_url']  =array('title'=>'source_url','td_class'=>'left') ;



    $_SESSION['descr_clss'][10]['details']['field']['type']='Основной тип товара' ;
    $_SESSION['descr_clss'][10]['details']['field']['_stype']=array('title'=>'Дополнительные типы','view'=>'view_second_type','script'=>'mod/category/i_field_views.php') ;
    $_SESSION['descr_clss'][10]['details']['field'][]=array('title'=>'Задать свойства товаров<div class="alert">Внимание! Заданные здесь свойства<br>будет назначены всем товарам<br>раздела основного типа</div>',   'on_view'=>'view_goods_props') ;
    $_SESSION['descr_clss'][10]['details']['field']['source_url']=array('title'=>'source_url','td_class'=>'left') ;

    $_SESSION['descr_clss'][200]['fields']['type']=array('type'=>'int(11)','default'=>0,'view'=>'view_all_goods_type','script'=>'mod/category/i_field_views.php') ;
    $_SESSION['descr_clss'][200]['fields']['type2']=array('type'=>'int(11)','default'=>0) ;
    $_SESSION['descr_clss'][200]['fields']['type3']=array('type'=>'int(11)','default'=>0) ;
    $_SESSION['descr_clss'][200]['fields']['brand_id']='int(11)' ;
    $_SESSION['descr_clss'][200]['fields']['series_id']='int(11)' ;
    $_SESSION['descr_clss'][200]['fields']['country_id']='int(11)' ;

    // добавить в init.php
    //$_SESSION['descr_clss'][200]['list']['field']['type']				='Тип товара' ;
    //$_SESSION['descr_clss'][200]['list']['field'][]                    =array('title'=>'Свойства',   'on_view'=>'view_goods_props') ;



	//----------------------------------------------------------------------------------------------------------------------
	// описание пунктов меню админа сайта
	//-----------------------------------------------------------------------------------------------------------------------

    $_SESSION['menu_admin_site']['Модули']['category']=array('name'=>'Каталогизатор','href'=>'editor_category.php','_icon'=>'menu/category2.jpg') ;

	//----------------------------------------------------------------------------------------------------------------------
	// описание функций вывода полей объектов класса
	//-----------------------------------------------------------------------------------------------------------------------

	//----------------------------------------------------------------------------------------------------------------------
	// описание меню фрейма объекта
	//-----------------------------------------------------------------------------------------------------------------------

}

function _category_admin_boot($options)
{
	create_system_modul_obj('category',$options) ;
}


function _category_install_modul($DOT_root)
{   echo '<h2>Инсталируем модуль <strong>Каталогизатор</strong></h2>' ;

    $file_items=array() ;
    //editor_category.php

    $file_items['editor_category.php']['include'][]='_DIR_TO_ENGINE."/admin/c_site.php"' ;
    $file_items['editor_category.php']['options']['use_table_code']='"TM_category"' ;
    $file_items['editor_category.php']['options']['title']='"Каталогизатор"' ;
    $file_items['editor_category.php']['options']['use_class']='"c_editor_obj"' ;
    create_menu_item($file_items) ;
    /*
    $file_items=array() ;
    // editor_category.php
    $file_items['editor_category.php']['include'][]				='_DIR_TO_MODULES."/category/m_category_frames.php"' ;
    $file_items['editor_category.php']['options']['use_table_code']='"TM_category"' ;
    $file_items['editor_category.php']['options']['title']		='"Каталогизатор"' ;
    $file_items['editor_category_tree.php']['include'][]		='_DIR_TO_MODULES."/category/m_category_frames.php"' ;
    $file_items['editor_category_viewer.php']['include'][]		='_DIR_TO_MODULES."/category/m_category_frames.php"' ;
    $file_items['editor_category_ajax.php']['include'][]		='_DIR_TO_MODULES."/category/m_category_ajax.php"' ;
    create_menu_item($file_items) ;
    */

	global $TM_category ;
   	$pattern=array() ;
	$pattern['table_title']				= 	'Каталогизатор' ;
	$pattern['use_clss']				= 	'1,60,61' ;
	$pattern['def_recs'][]				=	array ('parent'=>0,'clss'=>60,'enabled'=>1,'obj_name'=>'Товар') ;
   	$pattern['table_name']				= 	$TM_category ;
	$pattern['table_clss']				= 	101 ;
	$pattern['table_parent']			= 	$DOT_root;
   	$id_DOT_category=create_table_by_pattern($pattern) ;

	global $TM_category_image ;
	$pattern=array() ;
	$pattern['table_title']				= 	'Изображения' ;
	$pattern['use_clss']				= 	'3' ;
   	$pattern['table_name']				= 	$TM_category_image ;
	$pattern['table_clss']				= 	101 ;
	$pattern['table_parent']			= 	$id_DOT_category;
	$pattern['dir_to_image']			= 	'public/category/';
   	create_table_by_pattern($pattern) ;

    SETUP_get_img('/category/admin_img_menu/category2.jpg',_PATH_TO_ADMIN.'/img/menu/category2.jpg') ;

    //create_dir('/category/') ;
    //SETUP_get_file('/category/system_dir/htaccess.txt','/category/.htaccess');
    //SETUP_get_file('/category/system_dir/index.php','/category/index.php');
    //SETUP_get_file('/category/system_dir/hits.php','/category/hits.php');
    //SETUP_get_file('/category/system_dir/new.php','/category/new.php');
    //SETUP_get_file('/category/system_dir/price.php','/category/price.php');
    //SETUP_get_file('/category/system_dir/search.php','/category/search.php');
    //SETUP_get_file('/category/system_dir/sales.php','/category/sales.php');

    //SETUP_get_file('/category/class/c_category.php','/class/c_category.php');
    //$list_templates_file=analize_class_file_to_templates('/class/c_category.php') ;
    //SETUP_get_templates('/category/',$list_templates_file) ;

  // создавем вьювер товавров
      $sql='DROP VIEW view_category_all' ;
      execSQL_update($sql,array('debug'=>2)) ;
      $sql='create view view_category_all as
                   select  t1.*
                   from obj_site_category t1
                   where t1.clss=60 
          ' ;
      execSQL_update($sql,array('debug'=>2)) ;
}

function _category_rasdel_operation()
{
	echo 'Операции для раздела товара' ;
}

include_once(_DIR_TO_CLSS.'/clss_0.php') ;
class clss_6 extends clss_0
{
   function show_list_items($tkey,$usl,$options=array())
   {   $rec=$options['cur_obj_rec'] ;
       ?><div class=item reffer="<?echo $rec['_reffer']?>"><?
       include_once(_DIR_TO_MODULES.'/category/i_field_views.php') ;
       echo view_goods_props($rec,$tkey,$rec['pkey']) ;
       ?></div><?
       //damp_array($arr) ;
       //clss_2_on_view_props($rec,$tkey,$rec['pkey']) ;
   }
}


include_once(_DIR_TO_CLSS.'/clss_20.php') ;
include_once(_DIR_TO_CLSS.'/clss_1.php') ;
class clss_60 extends clss_1  // класс - тип товара
{

}

class clss_61 extends clss_20  // класс -  свойство типа товара
{
    function obj_create_dialog()
     { $form_id='form_obj_create_dialog_clss_'.$this->clss ;
       ?><form method="POST" action="ajax.php" id=<?echo $form_id?>>
             <table class="debug left">
                <tr><td>Название cвойства</td><td><input type=text class="text big" name=new_obj[obj_name]></td></tr>
                <tr><td>Тип свойства</td><td><input type=radio name=new_obj[input] value=1 checked> Набор значений <input type=radio name=new_obj[input] value=2> Флажок <input type=radio name=new_obj[input] value=3> Число <input type=radio name=new_obj[input] value=4> Строка
                                             <div id=select_values>
                                                 <div class=title>Укажите значения свойства, каждое с новой строки</div>
                                                 <textarea class="text" name=new_obj[list_items]></textarea>
                                             </div>
                                        </td>
                </tr>
             </table>
             <button class=button_green mode=append_viewer_main cmd=create_obj_after_dialog clss="<?echo $this->clss?>" parent_reffer="<?echo $_POST['parent_reffer']?>">Добавить</button>
             и <? show_select_action('after_clss_'.$_POST['clss'].'_create_action',array('go_list'=>'перейти к списку свойств','go_this'=>'перейти к созданному свойству','go_create'=>'создать еще одно свойство')) ;?>
         </form>
         <style type="text/css">
           div#select_values div.title{font-style:italic;font-weight:bold;font-size:11px;margin:10px 0 5px 0;}
           div#select_values textarea{width:500px;height:150px;}
         </style>
         <script type="text/javascript">
           $j('form#<?echo $form_id?> input[type="radio"]').live('click',function(){if($j(this).val()==1) $j('div#select_values').show();else $j('div#select_values').hide();})
         </script>
       <?
       return(0) ;
     }

  // создаем просто свойства, если выбран тип "список значений", создаем новый список и заполняем его значениями
  function create_obj_after_dialog()
  { $options['debug']=0 ;
    $options['parent_reffer']=$_POST['parent_reffer'] ;
    $prop_info['obj_name']=$_POST['new_obj']['obj_name'] ;  // obj_name,input
    $prop_info['url_name']=safe_text_to_url($_POST['new_obj']['obj_name']) ;  // obj_name,input
    $prop_info['input']=$_POST['new_obj']['input'] ;  // obj_name,input
    //damp_array($_POST) ;
    // вызываем базовую функцию создания объекта
    $rec_prop=parent::obj_create($prop_info,$options) ;
    //damp_array($rec_prop) ;
    $rec_type=get_obj_info($_POST['parent_reffer']) ;

    // создаем новый список и заполняем его значениями
    if ($rec_prop['input']==1)
    { $list_info['tkey']=_DOT(TM_LIST)->pkey;
      $list_info['parent']=1 ; // список - в корнень списков
      $list_info['hidden']=1 ;
      $list_info['obj_name']=$rec_type['obj_name'].' - '.$rec_prop['obj_name'] ;
      $options=array() ;
      $options['list_items']=$_POST['new_obj']['list_items'] ;

      //$list_info['debug']=0 ;
      $list_rec=_CLSS(21)->obj_create($list_info,$options) ;
      update_rec_in_table($rec_prop['tkey'],array('list_id'=>$list_rec['pkey']),'pkey='.$rec_prop['pkey']) ;
    }
    // обновляем список свойств в записи свойств в сессии - чтобы не перегружать админку
    $_SESSION['category_system']->reload_list_types() ;

    return($rec_prop) ;
  }

  function after_save_rec($tkey,$pkey)
  {
    $_SESSION['category_system']->reload_list_types() ;
  }

  function get_rec_field($rec,$fname)
  {  $value='' ;
     switch($fname)
     { case 'list_values': if ($rec['input']==1)
                           {  $arr_name=$_SESSION['index_list_names'][$rec['list_id']] ;
                              $arr=array() ;
                              if (sizeof($_SESSION[$arr_name])) foreach($_SESSION[$arr_name] as $rec) $arr[]=$rec['obj_name'] ;
                              $value=implode(', ',$arr) ;
                           }
                          break ;
       default:           $value=parent::get_rec_field($rec,$fname) ;
     }
    return($value) ;
  }


  function view_list_values($options)
  { $rec_prop=$GLOBALS['cur_page']->obj_info ;
    $tkey=_DOT(TM_LIST)->pkey ;
    switch($rec_prop['input'])
    { case 1:   _CLSS(20)->show_list_items($tkey,'parent='.$rec_prop['list_id'],array('cur_obj_rec'=>array('_reffer'=>$rec_prop['list_id'].'.'.$tkey,'debug'=>2))) ; // показываем значения из списка, list_id которого указан в записи
                break ;
      case 2:   echo 'Поле выводится с помощью элемента <strong>checkbox</strong>' ;
                break ;
    }
  }
  /*
  function name($parent_reffer)
  { $rec=get_obj_info($parent_reffer) ;
    //damp_array($rec) ;
    return($rec['obj_name'].' -') ;
  } */

  function on_delete_event($obj_info,$options=array())
  {  $sql='delete from '.$_SESSION['TM_goods_props'].' where id='.$obj_info['id'].' and type='.$obj_info['parent'] ;
     $cnt=execSQL_update($sql,array('debug'=>0)) ;
     if ($cnt) echo 'Удалено <strong>'.$cnt.'</strong> записей свойств товаров<br>' ;
     parent::on_delete_event($obj_info,$options) ;
  }
}


// arr - набор свойств для выбранного типа товара
function show_table_props_value(&$arr,$tkey,$pkey,$clss)
{  $values=execSQL('select id,value,num,str from '.$_SESSION['TM_goods_props'].' where parent='.$pkey) ;
   //damp_array($values) ;
   $text='<table class="list_props" reffer="'.$pkey.'.'.$tkey.'">' ;
   if (sizeof($arr)) foreach($arr as $id=>$rec_prop)
   {  $cur_text='' ;  $td_attr_arr=array() ;
      $text.='<tr>' ;
      $text.='<td>'.$rec_prop['title'].'</td>' ;
      // свойство - набор элементов
      if ($rec_prop['indx_select'])
      { $prop_values=execSQL_line('select value from obj_site_goods_props where parent='.$pkey.' and id='.$id) ;
        $arr_prop_value=array() ;
        if (sizeof($_SESSION[$rec_prop['indx_select']])) foreach($_SESSION[$rec_prop['indx_select']] as $id_list=>$rec_list)
          if (in_array($id_list,$prop_values)) $arr_prop_value[]='<li>'.$rec_list['obj_name'].'</li>' ;
        if (sizeof($arr_prop_value)) $cur_text='<ul class=left>'.implode('',$arr_prop_value).'</ul>' ;
        $cur_text='<div class="v2" cmd="get_props_edit" script="mod/category" prop_id="'.$id.'" no_send_form=1>'.$cur_text.'</div>' ;
		$td_attr_arr[]='id=prop_'.$pkey.'_'.$id ;
      }
      // свойство - флажок
      if ($rec_prop['edit_element']=='checkbox')
      { $value=$values[$id]['value'] ;
        $checked=($value)? 'checked':'' ;
        $cur_text='<input type=checkbox class=v2 cmd="set_prop_value" script="mod/category" prop_id="'.$id.'" no_send_form=1 value=1 '.$checked.'>' ;
        $td_attr_arr[]='id=prop_'.$pkey.'_'.$id ;
      }
      // свойство - число
      if ($rec_prop['edit_element']=='input_num')
      {  $value=(floatval($values[$id]['num'])!=0)? $values[$id]['num']:'' ;
         $cur_text='<div class=v2 cmd="get_props_edit" script="mod/category" prop_id="'.$id.'" no_send_form=1>'.$value.'</div>' ;
         $td_attr_arr[]='id=prop_'.$pkey.'_'.$id ;
      }
      // свойство - текстовое поле
      if ($rec_prop['edit_element']=='input_str')
      { $cur_text='<div class=v2 cmd="get_props_edit" script="mod/category" prop_id="'.$id.'" no_send_form=1>'.$values[$id]['str'].'</div>' ;
        $td_attr_arr[]='id=prop_'.$pkey.'_'.$id ;
      }

      if (sizeof($td_attr_arr))       $td_attr=implode(' ',$td_attr_arr) ; else $td_attr='' ;

      $text.='<td '.$td_attr.'>'.$cur_text.'</td>' ;
      $text.='</tr>' ;
   }
   $text.='</table>' ;
   return($text);
}






?>