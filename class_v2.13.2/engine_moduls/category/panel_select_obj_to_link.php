<?
// $options:
//  mode       - main
//             - second
//             - goods
// select_ids   - массив id checked элементов
// allow_ids   - массив id visible эдментов
function panel_select_obj_to_link($options=array())
{  global $category_system ;
   $root_type_id=($options['root_id'])? $options['root_id']:1 ;
   // damp_array($options,1,-1) ;
   if ($root_type_id!=1) $list_sections=execSQL('select pkey,parent,obj_name from obj_site_category where clss in (1,60) and enabled=1 and pkey in ('.$category_system->tree[$root_type_id]->get_list_child().') order by obj_name') ; // where pkey in (1,330,401,331,332,333,402,403,404)') ;
   else $list_sections=execSQL('select pkey,parent,obj_name from obj_site_category where clss in (1,60) and enabled=1  order by obj_name') ; // where pkey in (1,330,401,331,332,333,402,403,404)') ;
  //$list_sections=execSQL('select pkey,parent,clss,obj_name from obj_site_goods where clss!=2 and clss!=200 and clss!=11') ; // where pkey in (1,330,401,331,332,333,402,403,404)') ;
  $arr_sectons=array() ;
  //$querytime_before=fixed_time() ;
  $list_sections_to_convert=$list_sections ; // $list_sections_to_convert - в нем остануться только элементы, которые не нашли родителя
  convert_list_to_tree($list_sections_to_convert,$arr_sectons,1) ;   // 0 - c корня, 1 - пропускаем корень
  //damp_array($arr_sectons) ;
  //echo 'Размер массива: '.sizeof($list_sections).'<br>' ;
  //fixed_time($querytime_before,'Построение дерева заняло - ','') ;
  if (sizeof($options['select_ids'])) { $GLOBALS['arr_linked_ids']=array_flip($options['select_ids']) ; }
  else $GLOBALS['arr_linked_ids']=array() ;
  //damp_array($GLOBALS['arr_linked_ids']) ;
  $title=($options['mode']=='main' or $options['mode']=='sect_1c')? 'Выберите основной тип товара':'Выберите дополнительные типы товара' ;
  switch($options['mode'])
     { case 'main':     $title='Выберите основной тип товара. Все новые товары будут созданы с этим типом.' ; break ;
       case 'second':   $title='Выберите дополнительные типы товара' ; break ;
       case 'goods':    $title='Выберите тип товара для текущей позиции' ;  break ;
     }


  ?><div id=panel_select_obj_to_link reffer="<?echo $options['reffer']?>">
    <div class="info"><?echo $title?></div>
    <form><div class="panel_scrollbar">
            <input type="hidden" name="sec_name" value="<?echo $options['sect_name']?>">
            <input type="hidden" name="cat" value="<?echo $options['cat']?>">
            <input type="hidden" name="reffer" value="<?echo $options['reffer']?>">
        <? show_tree_recursive($arr_sectons,$list_sections,$options['mode'],0,$options['include_ids']) ;?>
        </div>
     </form></div>
     <style type="text/css">
         div.item{margin:2px 0;}
         div.item div.name.level_0{font-size:13px;font-weight:bold;color:black;margin-top:5px;text-transform:uppercase;}
         div.item div.name.level_1{font-size:12px;color:black;margin-top:3px;}
         div.item div.name input[type=checkbox]{position:relative;top:-1px;}
         div.item div.name:hover{color:black;text-decoration:underline;}
         div.item div.subitems{margin:0 0 0 10px;}
         div.panel_scrollbar { width: 490px; height:300px; overflow:auto ;margin: 0 0 0;padding-left:10px;border-top:1px solid #eeeeee;border-bottom:1px solid #eeeeee;border-left:1px solid #eeeeee;  }
     </style>

     <?
}

  function show_tree_recursive(&$arr,&$list,$mode,$level,$allow_ids=array())
  {  switch($mode)
     { case 'main':     $input_type='radio' ;    $input_name='type_main' ; $ajax_cmd='update_main_type' ; break ;
       case 'second':   $input_type='checkbox' ; $input_name='type_second' ; $ajax_cmd='update_second_type' ; break ;
       case 'goods':    $input_type='checkbox' ; $input_name='type_goods[]' ; $ajax_cmd='update_goods_type' ; break ;
       case 'sect_1c':  $input_type='radio' ;    $input_name='type_section' ; $ajax_cmd='update_sect_1c_type' ; break ;
     }
     $cnt=0 ;
     if (sizeof($arr)) foreach ($arr as $id=>$child_ids)
     {  ?><div class="item"><?
        $checked=isset($GLOBALS['arr_linked_ids'][$id])? 'checked':'' ;
        $cnt_item=0 ;
        ob_start() ;
        if (!sizeof($allow_ids) or in_array($id,$allow_ids)) {$cnt_item++ ;?><div class="name level_<?echo $level?>"><label><input name="<?echo $input_name?>" type="<?echo $input_type?>" <?echo $checked?> class=v2 cmd=<?echo $ajax_cmd?> script="mod/category" link_reffer="<?echo $list[$id]['_reffer']?>" value="<?echo $id?>"> <?echo $list[$id]['obj_name'] ;?></label></div><?}
        if (sizeof($child_ids)) {?><div class="subitems"><? $cnt_item+=show_tree_recursive($child_ids,$list,$mode,$level+1,$allow_ids) ; ?></div><?}
        $text=ob_get_clean() ;
        if ($cnt_item) { echo $text ; $cnt++ ; }

        ?></div><?
     }
    return($cnt) ;

  }
?>