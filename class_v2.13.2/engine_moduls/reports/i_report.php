<?
define('_REPORTS_TEMP_DIR',_DIR_TO_ROOT.'/tmp/') ;
define('_REPORTS_TEMP_PATH',_PATH_TO_SITE.'/tmp/') ;
define('_DOCS_DIR',_DIR_TO_ROOT.'/docs/') ;
define('_DOCS_PATH',_PATH_TO_SITE.'/docs/') ;
include_once(_DIR_TO_ENGINE.'/c_system.php') ;
class c_report extends c_system
{ public $editing=false ;

  public $filter=array() ;
  public $cnt_items ;
  public $first_obj=1 ;
  public $report_paginator=1 ;
  public $no_view_title=0 ;
  public $view_panel_info=1 ;

  public $use_datepicker=1 ;
  public $use_jwplayer=0 ;
  public $use_fusioncharts=0 ;

    public $PDF_page_orientation='L' ; // L P
    public $PDF_page_format='A4' ;
    public $PDF_page_keepmargins=false ;
    public $PDF_page_tocpage=false ;
    public $PDF_show_header=true ;

  // поля для работы с Excel
  public $file_name ;
  public $pExcel ;
  public $aSheet ;
  public $style ;
  public $title ;
  public $first_col_char=65 ; // A
  public $first_row=1 ;
  public $cur_col=65 ; // A
  public $cur_row=1 ;
  public $view_h1_title=true ;
  public $view_by_space_filter=0 ;

  function __construct($create_options=array())
  { parent::__construct($create_options);
    if (_CUR_FILTER) $this->filter=REPORTS()->parse_url(_CUR_FILTER,array()) ;
  }

    //public $temp_dir=_DIR_TO_ROOT.'/tmp/' ;
  //public $temp_path=_PATH_TO_SITE.'/tmp/' ;

 function panel_buttons()
 { ?><div id="panel_buttons"><input type="submit" class="button v2" cmd="set_filter" value="<?echo ($this->editing)? 'Получить данные':'Сформировать отчет'?>">&nbsp;&nbsp;<?
   if ($this->filter and !$this->editing){?><button class="button v2" cmd="report/save_as_doc">Сохранить в документах</button>&nbsp;&nbsp;
                        <button class="button v2" cmd="report/view_as_html">HTML</button>&nbsp;&nbsp;
                        <button class="button v2" cmd="report/view_as_pdf">PDF</button>&nbsp;&nbsp;
                        <?if ($this->allow_export_to_excel){?><button class="button v2" cmd="report/view_as_xls">Excel</button>&nbsp;&nbsp;<?}?>
                        <?if ($this->allow_export_to_excel){?><button class="button v2" cmd="report/view_as_CSV">Excel CSV</button>&nbsp;&nbsp;<?}?>
                        <button class="button v2" cmd="report/to_print">Печать</button>&nbsp;&nbsp;
                      <?}
   ?></div><?
 }

 function panel_button_apply()
 { ?><div id="panel_buttons"><input type="submit" class="button v2" cmd="set_filter" value="<?echo ($this->editing)? 'Получить данные':'Применить'?>"></div><?
 }
 function panel_buttons_action()
 { ?><div id="panel_buttons"><?
   if ($this->filter and !$this->editing){?><button class="button v2" cmd="report/save_as_doc"><i class="fa fa-floppy-o"></i></button>&nbsp;&nbsp;
                        <button class="button v2" cmd="report/view_as_html"><i class="fa fa-file-text-o"></i></button>&nbsp;&nbsp;
                        <button class="button v2" cmd="report/view_as_pdf"><i class="fa fa-file-pdf-o"></i></button>&nbsp;&nbsp;
                        <?if ($this->allow_export_to_excel){?><button class="button v2" cmd="report/view_as_xls">Excel</button>&nbsp;&nbsp;<?}?>
                        <?if ($this->allow_export_to_excel){?><button class="button v2" cmd="report/view_as_CSV">Excel CSV</button>&nbsp;&nbsp;<?}?>
                        <button class="button v2" cmd="report/to_print"><i class="fa fa-print"></i></button>&nbsp;&nbsp;
                      <?}
   ?></div><?
       }

 function panel_info_filter()
 { $_str=array() ; $info=array() ; $title='' ;
   //if ($this->filter['member'])                           $info['механик']=ACCOUNTS()->get_member_name_by_id($this->filter['member'] )  ;
   if ($this->filter['data_from'])                        $info['c']=$this->filter['data_from'] ;
   if ($this->filter['data_to'])                          $info['по']=$this->filter['data_to'] ;
   if (sizeof($info)) foreach($info as $title=>$value) $_str[]=$title.': <strong>'.$value.'</strong>' ;
   if (sizeof($_str)) $title='<p class=center>'.implode(' ',$_str).'</p>' ;
   return $title ;
   }

 // вызывает метод view_as_HTML,view_as_PDF или view_as_XLS в зависимости от типа текущего документа
  function print_template($list_recs,$method_name,$options)
     {
       if (method_exists($this,$method_name)) $this->$method_name($list_recs,$options) ;
     }

    function view_as_HTML($options=array())
    { if (!$this->no_view_title){?><h2 class="center"><?echo $this->title?></h2><?}
      echo $this->panel_info_filter() ;
      $this->HTML_report_body($options) ;

    }

    // основная функция в которорой будет фрмироваться тело отчета
    function HTML_report_body($options=array())
    { $options['panel_select_pages']=$this->report_paginator ;
      $options['panel_diapazon_page']=$this->report_paginator ;
      $options['debug']=0 ;

      $this->show_list_items('','print_template_HTML',$options) ;
    }

    function view_as_PDF($options=array())
    { $doc_file_name=$this->save_as_PDF($options) ;
      $doc_file_path=_REPORTS_TEMP_PATH.$doc_file_name;
      ?><iframe src="<?echo $doc_file_path?>"  style="width:100%; height:700px;margin-top:20px;" frameborder="0"></iframe><?
    }

    // use_doc_dir=1 => использовать для сохранения папку docs
    function save_as_PDF($options=array())
    { ob_start() ;
      $this->report_paginator=0 ;
      $this->view_as_HTML($options) ;
      //$this->show_list_items('','print_template_HTML',array('order'=>'pkey','panel_select_pages'=>0,'panel_diapazon_page'=>0,'debug'=>0)) ;
      $html=ob_get_clean() ;
      $file_name=($options['file_name'])? $options['file_name']:'report_'.md5(rand(1,100000)) ;
      $doc_file_name=$this->create_PDF_file($file_name,$this->title,$html,$this->panel_info_filter(),$options) ;
      return($doc_file_name) ;
            }

    function view_as_XLS($options=array())
    {  // подготовить страницу
        set_time_limit(0) ;
        $this->XLS_prepare_Sheet() ;
        $this->show_list_items('','print_template_XLS',array('order'=>'pkey','panel_select_pages'=>0,'debug'=>0)) ;
        ob_clean() ; // чисти вывод
        $this->XLS_put() ;
        //$file_name='temp_'.md5(rand(1,100000)).'.xls' ;
        //$this->XLS_save(_PATH_TO_SITE.'/tmp/'.$file_name) ;
        // регистрируем в журнале
        //LOGS()->reg_log('XLS export',$cmd,array('member_id'=>MEMBER()->id,'data'=>$_GET)) ;
    }

    public $file_csv ;
    function view_as_CSV($options=array())
    {  // подготовить страницу
        $fname='report_'.date('d_m_Y_H_i_s').'.csv' ;
        $this->file_csv = fopen(_DIR_TO_ROOT.'/temp/'.$fname, 'w');
        $this->show_list_items('','print_template_CSV',array('order'=>'pkey','panel_select_pages'=>0,'debug'=>0)) ;
        ob_clean() ; // чисти вывод
        fclose($this->file_csv);
        $cont=file_get_contents(_DIR_TO_ROOT.'/temp/'.$fname) ;
        header('Content-Type: application/vnd.ms-excel;name="'.$this->title.'.csv"');
        header('Content-Disposition: attachment;filename="'.$this->title.'.csv"');
        header('Cache-Control: max-age=0');
        echo $cont ;
        }

    function prepare_to_print($options=array())
    { // сохраняем HTML  во временный файл
      ob_start() ;
      $this->report_paginator=0 ;
      $this->view_as_HTML($options) ;
      $html=ob_get_clean() ;
      $file_name='temp_'.md5(rand(1,100000)) ;
      //$doc_file_name=$this->create_HTML_file($file_name,$html,$this->title,$this->panel_info_filter()) ;
      $doc_file_name=$this->create_HTML_file($file_name,$html,'',$this->panel_info_filter()) ;
      $doc_file_path=_REPORTS_TEMP_PATH.$doc_file_name;
      // показываем  этот временный файл в отдельном фрейме
      ?><iframe src="<?echo $doc_file_path?>"  style="width:100%; height:700px;margin-top:20px;" frameborder="0"></iframe><div id="div_backgroudPrint"></div><?
      // отправляем фрейм на печать
      ?><script type="text/javascript">
                       $j('div#div_backgroudPrint').printPage({url:'<?echo $doc_file_path?>'}) ;
                       $j('div#div_backgroudPrint').click() ;
                     </script><?
      }


  //==========================================================================================================================================================================
   // формирование отчета
   //==========================================================================================================================================================================

    // информация о текущей выборке при постраничной  HTML выборке
    function panel_info_select($options=array(),$mode='')
    { $_str2=array() ; $info2=array() ;
      //damp_array($options,1,-1) ;
      if ($options['count_obj'])                             $info2['всего строк']=$options['count_obj'];
      if ($options['first_obj'] and $options['last_obj'])    $info2['показаны строки']=$options['first_obj'].' по '.$options['last_obj'];
      //damp_array($info,1,-1) ;
      if (sizeof($info2)) foreach($info2 as $title=>$value) $_str2[]=$title.': <strong>'.$value.'</strong>' ;
      if (sizeof($_str2)) echo '<div class="panel_info_select '.$mode.'">'.implode(', ',$_str2).'</div>' ;
    }

    function create_HTML_file($doc_name,$html,$report_title='',$report_info='',$options=array())
    {   $html_css ='<style>'.file_get_contents(_DIR_TO_CLASS.'/style_print.css').'</style>' ;
        $title=($report_title)? $title='<h2 class="center">'.$report_title.'</h2><p class="center">'.$report_info.'</p>':'' ;

        $doc_file_name=$doc_name.'.html' ;
        $doc_dir=_REPORTS_TEMP_DIR.$doc_file_name ;
        file_put_contents($doc_dir,$html_css.$title.$html);
        return($doc_file_name) ;
    }

          // сохраняем отчет в виде PDF
   function create_PDF_file($doc_name,$report_title,$html,$report_info='',$options=array())
    {  // Include the main TCPDF library (search for installation path).
            require_once(_DIR_TO_ROOT.'/AddOns/tcpdf/tcpdf.php');

            // create new PDF document
            $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

            // set document information
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->SetAuthor('ESMO');
            $pdf->SetTitle($report_title);
            //$pdf->SetSubject('TCPDF Tutorial');
            //$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
       $pdf->setPrintHeader(false);
       $pdf->setPrintFooter(false);

            // set default header data
       if ($this->PDF_show_header)
       { $report_info=($report_info)? strip_tags($report_info)."\n".'подготовлен: '.MEMBER()->name.' '.date('d.m.Y H:i'):'подготовлен: '.MEMBER()->name.' '.date('d.m.Y H:i') ;
       $pdf->SetHeaderData('esmo.png', 30, $report_title,$report_info);

            // set header and footer fonts
            $pdf->setHeaderFont(Array('dejavusans', '', PDF_FONT_SIZE_MAIN));
            $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
       }

            // set default monospaced font
            $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

            // set margins
       //$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
       $pdf->SetMargins(5, 5, 5,5);
       //$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
       $pdf->SetHeaderMargin(0);
       //$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
       $pdf->SetFooterMargin(0);

            // set auto page breaks
       //$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
       $pdf->SetAutoPageBreak(TRUE, 0);

            // set image scale factor
       $pdf->setImageScale(2);

            // set some language-dependent strings (optional)
            //if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
            //    require_once(dirname(__FILE__).'/lang/eng.php');
            //    $pdf->setLanguageArray($l);
            //}

            // ---------------------------------------------------------

            // set font
            $pdf->SetCellPadding(0);
       $pdf->SetFont('freeserif', '', 11); // любой шрифт из папки /AddOns/tcpdf/fonts/
       //$pdf->SetFont('stsongstdlight', '', 12); // любой шрифт из папки /AddOns/tcpdf/fonts/
       //$pdf->setFontSpacing(-2);

            // add a page
       $pdf->AddPage($this->PDF_page_orientation, $this->PDF_page_format);

       $html_css ='<style type="text/css">'.file_get_contents(_DIR_TO_CLASS.'/style_pdf.css').'</style>' ;

            // output the HTML content
            //$pdf->writeHTML($html2, true, false, true, false, '');

            // writeHTML($html, $ln=true, $fill=false, $reseth=false, $cell=false, $align='')
            $pdf->writeHTML($html_css.$html, true, false, true, false, '');
       //$pdf->writeHTML($html, true, false, true, false, '');



            //Close and output PDF document
            $doc_file_name=$doc_name.'.pdf' ;
       $doc_dir=(($options['use_doc_dir'])? _DOCS_DIR:_REPORTS_TEMP_DIR).$doc_file_name ;

            $pdf->Output($doc_dir, 'F');

        return($doc_file_name) ;
    }

    function prepare_url($filter,$options=array())
    {   $url=array() ; $_str_url=array() ;  $res_url=$options['base_url'] ;  //damp_array($options,1,-1) ;
        if (sizeof($filter)) foreach($filter as $key=>$value) if ($value) switch($key)
        { case 'member':      $url['member']=$value ; break ;
          case 'doc_type':    $url['doc_type']=$value ; break ;
          case 'data':        $url['data']=$value ; break ;
          case 'check':       $url['check']=1 ; break ;
          default:            $url[$key]=$value ;
        }
        if (sizeof($url)) foreach($url as $key=>$value) if ($value!=1) $_str_url[]=$key.'='.$value ; else  $_str_url[]=$key ;
        if (sizeof($_str_url))  $res_url.='['.implode(',',$_str_url).']/';
        return($res_url) ;
    }

    function parse_url($url,$options=array())
    {  $params=array() ; $filter=array() ;
       $str=str_replace(array($options['base_url'],'/','[',']'),'',$url) ;
       $arr_params=explode(',',$str) ;
       if (sizeof($arr_params)) foreach($arr_params as $str_params)
       { $arr=explode('=',$str_params) ;
         if (sizeof($arr)==2) $params[$arr[0]]=$arr[1] ;
         else                 $params[$str_params]=1 ;
       }
      if (sizeof($params)) foreach($params as $key=>$value) switch($key)
      {  case 'member':         $filter['member']=$value ; break ;
         case 'doc_type':       $filter['doc_type']=$value ; break ;
         case 'data':           $filter['data']=$value ; break ;
         default:               $filter[$key]=$value ;
      }
      return($filter) ;
    }

  //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   //
   //  сттандрантые операции подготовки XLS
   //
   //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  //подготовка страницы
  function XLS_prepare_Sheet()
  { set_include_path(_DIR_TO_ADDONS.'/PHPExcel/');
    include_once(_DIR_TO_ADDONS.'/PHPExcel/PHPExcel.php');
    include_once(_DIR_TO_ADDONS.'/PHPExcel/PHPExcel/IOFactory.php');

    $this->pExcel = new PHPExcel();
    $this->pExcel->setActiveSheetIndex(0);
    $this->aSheet = $this->pExcel->getActiveSheet();

    //настройки для шрифтов
    $this->style['Font'] = array('font'=>array('name'=>'Arial Cyr','size'=>'8','bold'=>false));
    $this->style['boldFont'] = array('font'=>array('name'=>'Arial Cyr','size'=>'10','bold'=>true));
    $this->style['center'] = array('font'=>array('name'=>'Arial Cyr','size'=>'8'),'alignment'=>array('horizontal'=>PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'=>PHPExcel_Style_Alignment::VERTICAL_TOP));
    $this->style['left'] = array('font'=>array('name'=>'Arial Cyr','size'=>'8'),'alignment'=>array('horizontal'=>PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'=>PHPExcel_Style_Alignment::VERTICAL_TOP));
  }

  function XLS_print_table_header()
  { // заголовок документа
    //$this->aSheet->setTitle($this->title);

    $cur_col_char=$this->first_col_char ;
    if (sizeof($this->fields)) foreach($this->fields as $fname=>$options)
    { $col_char=iconv('windows-1251','UTF-8',chr($cur_col_char)) ;
      $this->aSheet->getColumnDimension($col_char)->setWidth($options['width']); //
      $this->aSheet->setCellValue($col_char.$this->cur_row,$options['title']);
      $this->aSheet->getStyle($col_char.$this->cur_row)->applyFromArray($this->style['boldFont'])->applyFromArray($this->style['center']);
      $cur_col_char++ ;
    }
    $this->cur_row++ ;
  }

  //отдаем пользователю в браузер
  function XLS_put()
  { include("PHPExcel/Writer/Excel5.php");
    $objWriter = new PHPExcel_Writer_Excel5($this->pExcel);
    header('Content-Type: application/vnd.ms-excel;name="'.$this->title.'.xls"');
    header('Content-Disposition: attachment;filename="'.$this->title.'.xls"');
    header('Cache-Control: max-age=0');
    $objWriter->save('php://output');
  }

  //отдаем сохраняем в файл
  function XLS_save($fname)
  { include("PHPExcel/Writer/Excel5.php");
    $objWriter = new PHPExcel_Writer_Excel5($this->pExcel);
    $objWriter->save($fname);
  }

  function get_filter_to_params($params)
  { $temp_filter=array_merge($this->filter,$params) ;
    $dir_filter=REPORTS()->prepare_url($temp_filter) ;
    return($this->url_report_mo.$dir_filter) ;
  }

 function get_time_range_by_filter_to_cur_day()
 { // если дата задана через фильтр, используем её. Иначе используем текущую дату время. Используем getdate для разненесения значений в массив
   $data_from=($this->filter['data_from'])? getdate(strtotime($this->filter['data_from'])):getdate() ;
   $data_to=($this->filter['data_to'])? getdate(strtotime($this->filter['data_to'])):getdate() ;
   // если дата задана через фильтр, преобразовыаем в текст без коррекции. Если была использована текущая дата - добавляем границы текущего дня. Испольщуем mktime
   $fime_from2=($this->filter['data_from'])? mktime($data_from['hours'],$data_from['minutes'],0,$data_from['mon'],$data_from['mday'],$data_from['year']):mktime(0,0,0,$data_from['mon'],$data_from['mday'],$data_from['year']);
   $fime_to2=($this->filter['data_to'])? mktime($data_to['hours'],$data_to['minutes'],0,$data_to['mon'],$data_to['mday'],$data_to['year']):mktime(23,59,59,$data_to['mon'],$data_to['mday'],$data_to['year']);
   return(array($fime_from2,$fime_to2)) ;
 }

 //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 //
 //  обвязка для вывода страниц
 //
 //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

}

?>