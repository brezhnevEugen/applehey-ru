<?php
include_once('i_report.php') ;
define('_REPORTS_TEMP_DIR',_DIR_TO_ROOT.'/tmp/') ;
define('_REPORTS_TEMP_PATH',_PATH_TO_SITE.'/tmp/') ;

$__functions['init'][]='_reports_site_vars' ;
$__functions['boot_site'][]='_reports_site_boot' ;

function REPORTS() {return($_SESSION['reports_system']);$_SESSION['reports_system']=new c_reports_system();return($_SESSION['reports_system']);} ;


function _reports_site_vars()
{
    $_SESSION['init_options']['reports']['debug']=0 ;
}

function _reports_site_boot($options)
{
  create_system_modul_obj('reports',$options) ;

}


class c_reports_system
{
    public $reports_list ;

    function c_reports_system($create_options=array())
    {
      ENGINE()->reg_ext('reports','Отчеты') ;
    }

    //REPORTS()->reg_reports2($this,'stats1','angelo_photo/report_stats1.php','report_stats1') ;         //
    function reg_reports($system,$name,$script_dir,$class_name)
    {  $this->reports_list[$name]=array('system'=>$system->system_name,'script_dir'=>$script_dir,'class_name'=>$class_name) ;
       //damp_array($this->reports_list) ;
    }

    //REPORTS()->reg_reports2($this,'Товары','stats1','angelo_photo/report_stats1.php','report_stats1') ;         //
    function reg_reports2($system,$section_title,$script_dir)
    {   $arr=explode('/',$script_dir) ;
        $arr2=explode('.',$arr[1]) ;
        $class_name=$arr2[0] ;
        $name=str_replace('report_','',$class_name) ;
        $this->reports_list[$name]=array('system'=>$system->system_name,'script_dir'=>$script_dir,'class_name'=>$class_name,'section_title'=>$section_title) ;
       //damp_array($this->reports_list) ;
    }

    function declare_menu_items_3($page)
      {

          $page->menu['/cab/reports/']=array('title'=>'Отчеты',
                                        'class'=>'fa fa-file-text-o',
                                        //'items'=>array('/cab/reports/'=>array('title'=>'Создать отчет','class'=>'fa fa-users'))
                                        ) ;
      }

    function get_report_obj_by_code($code)
    {  $rec_report=$this->reports_list[$code] ;
       $report_obj=null ; $error='' ;
       if (sizeof($rec_report))
       { $script_dir=$rec_report['script_dir'] ;
         $class_name=$rec_report['class_name'] ;
         if (file_exists(_DIR_EXT.'/'.$script_dir))
           { include_once(_DIR_EXT.'/'.$script_dir) ;
             if (class_exists($class_name))
             { $report_obj=new $class_name() ;

             } else $error='Не обнаружена класс отчета '.$code;
           } else $error='Не обнаружен скрипт отчета '.$code;
       }
       if (!is_object($report_obj)) { $report_obj=new stdClass() ; $report_obj->error=$error ; }
       return($report_obj) ;
    }

    function prepare_url($filter,$options=array())
    {   $url=array() ; $_str_url=array() ;  $res_url=$options['base_url'] ;  //damp_array($options,1,-1) ;
        if (sizeof($filter)) foreach($filter as $key=>$value) if ($value !== '' and $value)
        { if (is_array($value)) $_str_url[]=$key.'=('.implode(';',$value).')' ;
          elseif ($value!=1) $_str_url[]=$key.'='.$value ;
          else  $_str_url[]=$key ;
        }
        //if (sizeof($url)) foreach($url as $key=>$value) if ($value!=1) $_str_url[]=$key.'='.$value ; else  $_str_url[]=$key ;
        if (sizeof($_str_url))  $res_url.='['.implode(',',$_str_url).']/';
        //$res_url=str_replace(' ','-',$res_url) ;
        return($res_url) ;
    }

    function parse_url($url,$options=array())
     {  $params=array() ; $filter=array() ;
        $str=str_replace(array($options['base_url'],'/','[',']'),'',$url) ;
        $arr_params=explode(',',$str) ;
        if (sizeof($arr_params)) foreach($arr_params as $str_params)
        { $arr=explode('=',$str_params) ;
          if (sizeof($arr)==2) { if (strpos($arr[1],'(')!==false and strpos($arr[1],')')!==false)
                                 { $arr2=explode(';',str_replace(array('(',')'),'',$arr[1])) ;
                                   $filter[$arr[0]]=array_flip($arr2);
                                   foreach($filter[$arr[0]] as $id=>$value) $filter[$arr[0]][$id]=1 ;
                                 }
                                 else $filter[$arr[0]]=$arr[1] ;
                               }
          else                 if ($str_params) $filter[$str_params]=1 ;
        }
       //damp_array($filter,1,-1) ;
       return($filter) ;
     }





}



?>