<?
// аккаунт
include_once(_DIR_TO_ENGINE.'/class/clss_0.php') ;
class clss_86 extends clss_0
{  // вносим в объект $member инфомацию из массива $rec
   function update_member_info(&$member,$rec)
   {   $member->info=$rec ;
       $member->id=$rec['pkey'] ;
       $member->reffer=$rec['_reffer'];
       $member->parent=$rec['parent'] ;
       $member->login=$rec['login'] ;
       $member->name=$rec['obj_name'];

       // aspmo
       $member->working=$rec['_rol_name'];
       //$member->right=$rec['_right'] ;

       if ($rec['smena_in']) $member->_smena_in=date('d.m.Y H:i',$rec['smena_in']) ;
       if ($rec['smena_out']) $member->_smena_out=date('d.m.Y H:i',$rec['smena_out']) ;
       $member->_smena_status=$_SESSION['ARR_smena_status'][$rec['smena']] ;
       //damp_array($member,1,-1) ;
   }

    // после создания объекта создаем для него расчетный счет
    // что-то не работает
    function on_create_event($obj_info)
    { if (is_object($_SESSION['esmo_billing_system'])) $_SESSION['esmo_billing_system']->clss_210_on_create_event($obj_info)  ;
    }
}

// клиент
class clss_89 extends clss_86
{
}

// частное лицо
class clss_87 extends clss_89
{
  function prepare_public_info(&$rec)
    { parent::prepare_public_info($rec) ;
      $rec['__name']=$rec['obj_name'] ;    // Иванов Сергей Федорович
      $rec['__contact_name']=$rec['obj_name'] ; // Иванов Сергей Федорович
      $rec['__member_type']='Частное лицо' ;
    }

}

// организация
class clss_88 extends clss_89
{

  function prepare_public_info(&$rec)
  { parent::prepare_public_info($rec) ;
    $rec['__name']=$rec['contact_name'].', '.$rec['obj_name'] ;    // Иванов Сергей Федорович, ООО "Рога и копыта"
    $rec['__contact_name']=$rec['contact_name'] ; // Иванов Сергей Федорович
    $rec['__member_type']='Организация' ;
  }

  // формируем информацию о клиенте в виде подготовленные для вывода table
  // $rec - запись по аккаунту
  function prepare_public_account_info(&$rec)
   { $info_db=array() ; $arr_info=array() ;
     if ($rec['info'] and is_array($rec['info']))  $info_db=$rec['info'] ;  // старое поле
     if ($rec['info'] and !is_array($rec['info']))  $info_db=unserialize($rec['info']) ;  // старое поле
     if ($rec['ext_info'] and is_array($rec['ext_info']))  $info_db=$rec['ext_info'] ;
     if ($rec['ext_info'] and !is_array($rec['ext_info'])) $info_db=unserialize($rec['ext_info']) ;  // новое поле

     $arr_info['Организация']=$rec['obj_name'] ;
     if ($rec['email']) $arr_info['Email']=$rec['email'] ;
     if ($rec['phone']) $arr_info['Телефон']=$rec['phone'] ;
     if (sizeof($info_db)) $arr_info=array_merge($arr_info,$info_db) ;
     $rec['__info']=damp_serial_data($arr_info);
     unset($rec['info'],$rec['ext_info']) ;
   }

}

?>