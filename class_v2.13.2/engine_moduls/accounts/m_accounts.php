<?php
$__functions['init'][]='_accounts_site_vars' ;
$__functions['boot_site'][]='_accounts_site_boot' ;

include_once('i_class_86_87_88_89.php') ;

function ACCOUNTS() {return($_SESSION['accounts_system']);$_SESSION['accounts_system']=new c_accounts_system();return($_SESSION['accounts_system']);} ;


function _accounts_site_vars()
{

    $_SESSION['ARR_smena_status']=array (0=>'close',
                                         1=>'active',
                                         2=>'pause',
                                         3=>'out',
                                         4=>'unactive'
                                        ) ;


    $_SESSION['init_options']['accounts']['debug']=0 ;
    //$_SESSION['init_options']['goods']['patch_mode']='TREE_NAME' ;
    $_SESSION['init_options']['accounts']['patch_mode']='NAME' ;
    $_SESSION['init_options']['accounts']['root_dir']	='/cab/personal/' ;
    $_SESSION['init_options']['accounts']['clss_items']='86' ;
    //$_SESSION['init_options']['accounts']['usl_show_items']='enabled=1 and _enabled=1' ;
    //$_SESSION['init_options']['accounts']['usl_show_items']='_enabled=1' ; // показываем всех сотрудников, уволенных и действующих
    $_SESSION['init_options']['accounts']['tree_fields']='pkey,parent,clss,indx,enabled,obj_name' ;


    // определяем условия для формирование дерева
    $_SESSION['init_options']['accounts']['tree']['debug']=0;
    $_SESSION['init_options']['accounts']['tree']['clss']='1' ;
    $_SESSION['init_options']['accounts']['tree']['include_space_section']=1 ;
    $_SESSION['init_options']['accounts']['tree']['order_by']='indx' ;
    $_SESSION['init_options']['accounts']['tree']['get_count_by_clss']=0 ;
    $_SESSION['init_options']['accounts']['usl_show_sections']='enabled=1' ;


}

function _accounts_site_boot($options=array())
{
  create_system_modul_obj('accounts',$options) ;

}


class c_accounts_system extends c_system_catalog
{

    function c_accounts_system($create_options=array())
    { global $site_TM_kode ;
      $this->table_name ='obj_'.$site_TM_kode.'_account' ;
      $this->table_account ='obj_'.$site_TM_kode.'_account' ;
      parent::__construct($create_options) ;

    // регистрируем обработчики событий
    //ESMO()->reg_event_lister('mo_dopusk_allow',$this,'on_event_mo_allow') ;         //
    //ESMO()->reg_event_lister('fixed_hand_check_allow',$this,'on_event_mo_allow') ;
    //ESMO()->reg_event_lister('fixed_hand_check_denited',$this,'on_event_mo_allow') ;

    //регистрируем документы
    }

  function declare_menu_items_1($page)
  {
          
  }    


    //==========================================================================================================================================================================
      // АВТОРИЗАЦИЯ и ВЫХОД из аккаунта
      //==========================================================================================================================================================================

    // функция входа  работает с объектом $member
    function member_login($login,$pass,$options=array())
    { if (!$_SESSION['member']->id)
        if ($login and $pass)
        { $rec=$this->get_member_by_login_and_pass($login,$pass) ;

          if ($_SESSION['LS_no_auth_pass_for_token'] and $rec['pkey'] and sizeof($rec['_token']) and !$rec['auth_by_pass'])
          {  _event_reg('Ошибка авторизации','Аккаунт имеет токен для авторизации - Логин:<strong>'.$login.'</strong><br>Пароль:<strong>'.$pass.'</strong><br>') ; // регистрируем событие 'Вход на сайт'
              $result=prepare_result('login_only_by_token') ;

          }
          elseif ($rec['pkey']) {  // проверяем наличие уже запущенного кабинета - надо сделать исключение для администраторов
                               //$is_start=$this->get_doctor_available2($rec['XMPP_login']) ;
                               //$is_start=$this->get_doctor_available2($rec['XMPP_login']) ;
                               // поставиь проверку что доктор не идет по другой смене
                               //if ($is_start and 0) $result=prepare_result('member_is_start') ;  // сообщение что пользователь уже залогинен на другом устройстве
                               //else
                               $result=$this->member_autorize($rec) ;  // авторизуем пользователя
                            }
          else              {  _event_reg('Ошибка авторизации','Логин:<strong>'.$login.'</strong><br>Пароль:<strong>'.$pass.'</strong><br>') ; // регистрируем событие 'Вход на сайт'
                               $result=prepare_result('member_not_found') ;
                            }
        }
        else $result=prepare_result('space_field') ;
      else $result=prepare_result('no_login_for_member') ;

      return($result) ;
    }

    // функция выхода теперь также работает с обхектом member
    function member_logout()
    {  $member_name=$_SESSION['member']->name.' ['.$_SESSION['member']->login.']' ;
       LOGS()->reg_log('Выход персонала',$member_name,array('member_id'=>$_SESSION['member']->id)) ;
       $this->member_save($_SESSION['member']->id,array('ip'=>'','time_logout'=>time(),'smena'=>0)) ;
       unset($_SESSION['member']) ;
       $_SESSION['member']=new c_member() ; // создаем неавторизированного пользователя
       return(prepare_result('logout_success')) ;
    }

    // smena=0,1 - признак активной смены
    // smena_in - время начала смены
    // smena_out - время окончания смены
    // комбинации полей после авторизации сотрудников, с активным флагом в роли use_auch_as_smena
    // smena=0   - close, смена закрыта
    // smena=1   - active, смена активна
    // smena=2   - paused, смена приостановлена другим пользователем
    // smena=3   - out, смена закрыта по операции закрытия смены

    // задаем активную смену для персонала в его группе
    function  member_smena_in($smena_in=0)
    { if (!$smena_in) $smena_in=time() ;
      // сбрасываем смену у всех
      if (MEMBER()->smena_monopole_mode) execSQL_update('update obj_site_account set smena=2 where clss='.MEMBER()->info['clss'].' and parent='.MEMBER()->info['parent'].' and rol='.MEMBER()->info['rol'].' and smena=1') ;
      // ставим себе начало смены
      execSQL_update('update obj_site_account set smena_in='.$smena_in.',smena_out=0,smena=1 where pkey='.MEMBER()->id) ;
      // данные аккаунта будут заново прочитаны из базу
      $this->update_member_info($_SESSION['member']) ;
    }

    function  member_smena_out()
    {   $this->member_save(MEMBER()->id,array('smena_out'=>time(),'smena'=>3)) ; //смена закрыта, пользователь может только разлогиниться
        $this->update_member_info($_SESSION['member']) ; // данные аккаунта будут заново прочитаны из базу
        $member_name=MEMBER()->name.' ['.MEMBER()->login.']' ;
        LOGS()->reg_log('Смена персонала',$member_name.' - окончание смены',array('member_id'=>MEMBER()->id)) ;
    }

     // создание объекта member по записи $rec и вся необходимая обвязка
     function member_autorize($rec,$options=array())
     { $_SESSION['member']=new c_member($rec) ;  // создаем авторизированного пользователя. Данные из $rec будут внесены в $_SESSION['member']
       $_SESSION['member']->use_system='aspmo_system' ;
       $_SESSION['member']->login_time=time() ;
       $_SESSION['smena_podpis']=0 ;

       $_SESSION['member']->rol=$rec['rol']   ;
       $_SESSION['member']->use_auch_as_smena=$_SESSION['ARR_roll'][$rec['rol']]['use_auch_as_smena']   ;
       $_SESSION['member']->smena_monopole_mode=$_SESSION['ARR_roll'][$rec['rol']]['smena_monopole_mode']   ;
       $_SESSION['member']->cur_group_id=$rec['parent']  ; //$cur_group_rec['pkey'] ;
       $_SESSION['member']->cur_group_name=execSQL_value('select obj_name from obj_site_account where pkey="'.$rec['parent'].'"') ; //$cur_group_rec['obj_name'] ;

       // необходимо обновить поля по записи аккауента в базе
       $data=array('ip'=>$_SERVER['REMOTE_ADDR'],'time_login'=>time(),'time_logout'=>0,'sess_id'=>session_id()) ;
       $this->member_save($_SESSION['member']->id,$data) ;
       $this->update_member_info($_SESSION['member']) ; // данные аккаунта будут заново прочитаны из базу
       $member_name=$_SESSION['member']->name.' ['.$_SESSION['member']->login.']' ;
       LOGS()->reg_log('Вход персонала',$member_name,array('member_id'=>$rec['pkey'])) ;

       ob_start()  ;
       // проверяем работает ли данная роль по сменам - пока только для врачей
       if ($_SESSION['member']->use_auch_as_smena and $rec['rol']==2)
       { // проверяем нет ли другого активного логина на этой роли с открытой сменой
         $doctor_id=ESMO()->check_doctor(MEMBER()->cur_group_id,array('debug'=>0)) ;
         if ($doctor_id and $_SESSION['member']->smena_monopole_mode)
         {   $rec_active_doctor=$this->get_member_by_id($doctor_id) ;
             if ($doctor_id==$rec['pkey']) // если активная смена - наша
             {  $this->member_smena_in($rec['smena_in'])  ;
                $text_log='<div class="green">'.$member_name.' - автоматическое возобновление смены после аварийного закрытия браузера</div>' ;
             }
             else //есть активная смена другого врача
             {  // отмечаем что смена открыта у другого пользователя
               execSQL_update('update obj_site_account set smena=4 where pkey='.MEMBER()->id) ;  // 4 - UNACTIVE
               $text_log='<div class="red">'.$member_name.' - Автоматическое открытие смены заблокировано, смена уже открыта сотрудником '.$rec_active_doctor['obj_name'].'</div>' ;
             }
         }
         // активных смен нет - открываем свою сменe
         else if ($rec['smena_in'] and !$rec['smena_out']) // если была своя ранее открытая смена
         {
           $this->member_smena_in($rec['smena_in'])  ;
           $text_log=$member_name.' - Автоматическое возобновление смены после выхода из браузера' ;
         }
         else
         {

           $this->member_smena_in()  ;
           $text_log=$member_name.' - Нормальное открытие смены после авторизации' ;
         }
       }
       $debug_text=ob_get_clean() ;
       $this->update_member_info($_SESSION['member']) ; // данные аккаунта будут заново прочитаны из базу
       LOGS()->reg_log('Смена персонала',$text_log.'<br>'.$debug_text,array('member_id'=>$rec['pkey'])) ;


       $event_name=($options['event_name'])? $options['event_name']:'Авторизация через логин/пароль' ;

       _event_reg($event_name,$_SESSION['member']->name) ; // регистрируем событие 'Вход на сайт через пароль'
       return(prepare_result('login_success')) ;
      }


      // обновить инфрмацию объекта $member
      function update_member_info($member)
      { $rec=$this->get_member_by_id($member->id,array('get_right'=>1)) ;  // damp_array($rec) ;
        $member->update_info($rec) ; // будет выхвана функция clss_200->update_member_info
      }

       function change_pass($member_id,$new_value)
       { if (is_object($member_id)) $member_id=$member_id->id ;  // если вместо $member_id  передан объект member
         list($member_id,$tkey)=explode('.',$member_id) ; // преобразования reffer в id
         $old_value=execSQL_value('select password from '.$this->table_account.' where pkey='.$member_id) ;
         // проверяем, изменилось ли значение поля
         if (check_nochange_form_value($new_value,$old_value))  return(array('type'=>'error','text'=>'Значение не изменилось','code'=>'nochange')) ;
         // проверяем заполнение обязательных полей
         if ($new_value=='')                               return(array('type'=>'error','code'=>'space_field')) ;
         // проверка прав доступа текущего аккаунта к аккаунту $member
         //if ($res=$this->check_access($member_id)!='ok')  return(array('type'=>'error','code'=>'access_denited')) ;
         // сохраняем в базе
         update_rec_in_table($this->table_account,array('password'=>$new_value),'pkey='.$member_id) ;
         // регистрируем событие
         $account_reffer=$member_id.'.'.$this->tkey ;
         $evt_id=_event_reg('Измененение данных аккаунта','Пароль: '.$new_value,$account_reffer) ;
         // сохраняем данные в аккаунте
         if ($_SESSION['member']->id==$member_id) $this->update_member_info($_SESSION['member']) ;
         // отправляем почтовое уведомление
         //$this->send_mail_change_password($member_id,array('use_event_id'=>$evt_id)) ;
         // возвращаем событие успешного обловления
         return(array('type'=>'success','code'=>'pass_change_success')) ;
       }

      //==========================================================================================================================================================================
      // РАБОТА С СОТРУДНИКАМИ АСПМО
      //==========================================================================================================================================================================

      // получаем id группы на АРМ которой сейчас работает сотрудник на основании IP адреса его машины
      function get_member_cur_group_rec()
      { $arm_ip=$_SERVER['REMOTE_ADDR'] ;
        $arm_rec=execSQL_van('select * from obj_site_account where pkey=(select parent from obj_site_account where clss=205 and enabled=1 and ip="'.$arm_ip.'")') ;
        return($arm_rec) ;
      }

      // получаем  АРМ по его IP
      function get_ARM_rec_by_IP($IP)
      { $arm_rec=execSQL_van('select * from obj_site_account where clss=205 and enabled=1 and ip="'.$IP.'")') ;
        return($arm_rec) ;
      }

      function get_group_name($id)
      { $name=execSQL_value('select obj_name from obj_site_account where pkey="'.$id.'"') ;
        return($name) ;
      }

      function get_member_by_login_and_pass($login,$pass)
      {  $login=stripslashes(trim($login)) ;
          $pass=stripslashes(trim($pass)) ;
          $rec=execSQL_van('select * from '.$this->table_account.' where login="'.trim($login).'" and password="'.trim($pass).'" and enabled=1') ;
          if ($rec['pkey']) $this->prepare_public_info_to_member($rec,array('get_right'=>1)) ;
          return($rec) ;
      }

      // получение информации по прсоналу АСПМО
      function get_member_by_id($id,$options=array())
      { list($id,$tkey)=explode('.',$id) ;
        $tkey=_DOT('obj_site_account')->pkey ;
        $usl_clss=($options['clss'])? ' and clss='.$options['clss']:'' ;
        //$rec=execSQL_van('select * from obj_site_account where pkey="'.$id.'" '.$usl_clss) ;
        $rec=get_obj_info($id.'.'.$tkey) ;
        if ($rec['pkey']) $this->prepare_public_info_to_member($rec,$options) ;
        return($rec);
      }

      // получение информации по прсоналу АСПМО
      function get_member_name_by_id($id,$options=array())
      { list($id,$tkey)=explode('.',$id) ;
        if (isset($GLOBALS['account_names'][$id])) return($GLOBALS['account_names'][$id]) ;
        $usl_clss=($options['clss'])? ' and clss='.$options['clss']:'' ;
        $rec=execSQL_van('select * from obj_site_account where pkey="'.$id.'" '.$usl_clss) ;
        //if ($rec['pkey']) $this->prepare_public_info_to_member($rec,$options) ;
        //return($rec['_rol_name'].' '.$rec['obj_name']);
        $GLOBALS['account_names'][$id]=$rec['obj_name'] ;
        return($rec['obj_name']);
      }

      // получение информации по прсоналу АСПМО
      function get_members_by_ids($ids,$options=array())
      { $usl_clss=($options['clss'])? ' and clss='.$options['clss']:'' ;
        $recs=execSQL('select * from obj_site_account where pkey in ('.$ids.') '.$usl_clss) ;
        if (sizeof($recs)) foreach($recs as $id=>$rec) $this->prepare_public_info_to_member($recs[$id],$options) ;
        return($recs);
      }

      function prepare_public_info_to_member(&$rec,$options=array())
      { $rec['__href']='/cab/setting/account/'.$rec['pkey'].'/' ;
        $rec['_rol_name']=$_SESSION['ARR_roll'][$rec['rol']]['obj_name'] ;
        //$rec['_token']=execSQL('select pkey as id,user_xkey,user_ykey,r_xkey,r_ykey,token_ID from obj_site_account_out_150 where parent='.$rec['pkey'],0,1) ;
        if ($rec['ext_info']) {$rec['_ext_info']=unserialize($rec['ext_info']) ; unset($rec['ext_info']) ; }
        /*
        if ($options['get_right'] and $rec['rol'])
        { $rol_reffer=$_SESSION['ARR_roll'][$rec['rol']]['_reffer'] ;
          $links=get_arr_links_to_reffer($rol_reffer,array('clss'=>97,'debug'=>0)) ;
          if (sizeof($links)) foreach($links as $rec_link)
            { if ($rec_link['obj_clss']==97)  $rec['_right'][$_SESSION['menu_system']->tree[$rec_link['obj_pkey']]->rec['title']]=1 ;
              if ($rec_link['link_clss']==97) $rec['_right'][$_SESSION['menu_system']->tree[$rec_link['link_pkey']]->rec['title']]=1 ;
            }

        } */
        //if (_DOT($rec['tkey'])->list_clss[3]) $stamp_rec=execSQL_van('select pkey,tkey,file_name from obj_site_account_image where parent='.$rec['pkey'].' and alt="stamp"') ;
        //if ($stamp_rec['pkey']) $rec['stamp']=$stamp_rec ;
        if (sizeof($rec['obj_clss_3'])) foreach($rec['obj_clss_3'] as $i=>$rec_img)
            switch($rec_img['alt'])
            { case 'stamp':    $rec['stamp']=$rec_img ; unset($rec['obj_clss_3'][$i]) ; break ;
              case 'signature':    $rec['signature']=$rec_img ; unset($rec['obj_clss_3'][$i]) ; break ;
            }



        if (_DOT($rec['tkey'])->list_clss[5])
        { $sert_rec=execSQL_van('select pkey,tkey,file_name from obj_site_account_files where parent='.$rec['pkey'].' and obj_name="root_sertificat"') ;
          if ($sert_rec['pkey']) $rec['sert_file']=_DOT($sert_rec['tkey'])->dir_to_file.'/'.$sert_rec['file_name'] ;
        }
        //damp_array($rec,1,-1) ;
      }

      function get_list_members($options=array())
      { $_usl=array() ;
        //$_usl[]=($options['group_id'])? 'parent='.$options['group_id']:'parent='.MEMBER()->cur_group_id ;
        $_usl[]='pkey>1' ;
        if ($options['rol']) $_usl[]='rol='.$options['group_id'] ;
        if (!$options['and_disabled']) $_usl[]='enabled=1' ;
        $usl=implode(' and ',$_usl) ;
        $recs=execSQL('select * from obj_site_account where (rol in (1,2) or status in (1,2,3)) and '.$usl.' order by rol,obj_name') ;
        if (sizeof($recs)) foreach($recs as $id=>$rec) $this->prepare_public_info_to_member($recs[$id],$options) ;
        return($recs) ;
      }

      //обновляем данные аккаунта в базе
      function member_save($id,$form_data)
      { $rec=$this->get_member_by_id($id) ; $res=0 ;
        $data=array() ;
        if ($form_data['ext_info']) $data['ext_info']=$rec['_ext_info'] ; // если будут изменены дополнительные поля, надо сохрянять весь массив доп. полей
        if ($rec['pkey'])
        {
          if (sizeof($form_data)) foreach($form_data as $fname=>$value)
              {  if ($fname!='ext_info') {  if($rec[$fname]!=$value) $data[$fname]=$value; }  // сохраняем только изменишиеся поля
                 else if (sizeof($value)) foreach($value as $ext_fname=>$ext_value) $data['ext_info'][$ext_fname]=$ext_value; //  для доп.полей сохраняем все
              }
          //damp_array($data) ;
          // если задана смена - сбрасываем смену у остальных врачей
          //if ($data['smena']) update_rec_in_table('obj_site_account',array('smena'=>0),'clss='.$rec['clss'].' and parent='.$rec['parent'].' and rol='.$rec['rol'],array('debug'=>0)) ;
          if (sizeof($data)) $res=update_rec_in_table('obj_site_account',$data,'pkey='.$rec['pkey'],array('debug'=>0)) ;
          else $res=0 ;

        }
        return($res) ;
      }

      function member_create($form_data)
      { $data=array('parent'=>MEMBER()->cur_group_id,'clss'=>86) ;
        if (!$form_data['rol'])  return(array('error'=>'rol_not_set')) ;
        if (!$form_data['login'])  $form_data['login']=safe_text_to_url($form_data['name']) ;
        if (!$form_data['password'])  $form_data['password']=rand(1,100000) ;
        if (!$form_data['parent'])  $form_data['parent']=MEMBER()->cur_group_id ;

        $login_exist=execSQL_value('select pkey from obj_site_account where login="'.$form_data['login'].'" and clss=210') ;
        if ($login_exist)  return(array('error'=>'login_exist')) ;

        $data['access_pass']=md5(rand(1,1000)) ;

        $data['obj_name']=$form_data['name'] ;
        $data['rol']=$form_data['rol'] ;
        $data['login']=$form_data['login'] ;
        $data['password']=$form_data['password'] ;


        $result['member_id']=adding_rec_to_table('obj_site_account',$data) ;
        $result['name']=$data['obj_name'] ;
        $result['login']=$data['login'] ;
        $result['password']=$data['password'] ;

        return($result) ;
      }


      function member_delete($id)
      { $member_rec=$this->get_member_by_id($id) ;
        $res=execSQL_update('delete from obj_site_account where pkey='.$id.' and clss=210') ;
        //$result=$this->XMPP_action_user('delete',array('login'=>$member_rec['XMPP_login'],'pass'=>$member_rec['XMPP_password'])) ;
          $result=array() ;
        return(array_merge($result,$res)) ;
      }



      //==========================================================================================================================================================================
      // РАБОТА с ГРУППАМИ
      //==========================================================================================================================================================================

       function group_create($form_data)
       { $result=_CLSS(20)->obj_create($form_data,array('parent_reffer'=>_IL('IL_groups')->id.'.40')) ; // !!! потом обязательно убрать привяку к ID
         return($result) ;
       }

       function get_list_groups($options=array())
          { $recs=execSQL('select * from '.TM_LIST.' where clss=20 and enabled=1 and parent='._IL('IL_groups')->id.' order by indx,obj_name') ;
            //if (sizeof($recs)) foreach($recs as $id=>$rec) $this->prepare_public_info_to_member($recs[$id],$options) ;
            return($recs) ;
          }

        function get_group_by_URL($URL)
        {  $arr=explode('.',$URL) ;
           $rec=$this->get_group_by_id($arr[0])  ;
           return($rec) ;
        }

        function get_group_by_id($id)
        {   $rec=$_SESSION['IL_groups'][$id] ;
            if ($rec['pkey']) $this->prepare_public_info_to_group($rec) ;
            return($rec) ;
        }

        function prepare_public_info_to_group(&$rec)
        {

        }


    //==========================================================================================================================================================================
      // РАБОТА С ТЕРМИНАЛАМИ И РАБОЧИМИ МЕСТАМИ ФЕЛЬДШЕРА
      //==========================================================================================================================================================================

       function get_list_workgoups()
       { $recs=execSQL('select * from '.$this->table_account.' where clss=206 and enabled=1') ;
         return($recs) ;

       }

       function ARM_create($group_id,$name,$IP)
       {
         $id=adding_rec_to_table('obj_site_account',array('obj_name'=>$name,'clss'=>205,'parent'=>$group_id,'ip'=>$IP)) ;
         return($id) ;
       }


       function arm_save($data)
       {
         if (sizeof($data))foreach($data as $id=>$rec)
             $res[]=update_rec_in_table('obj_site_account',array('id'=>$rec['id'],'obj_name'=>$rec['name']),'pkey='.$id) ;
       }



    function allow_right_rol($rol_id,$class_name)
     { $id=execSQL_value('select pkey from obj_site_permit where clss=216 and obj_name="'.$class_name.'" and rol_id="'.$rol_id.'"') ;
       if (!$id) adding_rec_to_table('obj_site_permit',array('clss'=>216,'parent'=>1,'obj_name'=>$class_name,'rol_id'=>$rol_id)) ;
     }

     function decline_right_rol($rol_id,$class_name)
     { execSQL_update('delete from obj_site_permit where clss=216 and obj_name="'.$class_name.'" and rol_id="'.$rol_id.'"') ;
     }

     function get_right_rol($rol_id,$class_name)
     { $id=execSQL_value('select pkey from obj_site_permit where clss=216 and obj_name="'.$class_name.'" and rol_id="'.$rol_id.'"') ;
       return($id) ;
     }



}

function is_member_right()
{  if (MEMBER()->info['rol']==1 or  MEMBER()->info['rol']==2 or MEMBER()->info['rol']==3) return(1) ;
   return(1) ;
    //if (is_object($_SESSION['member'])) return($_SESSION['member']->right[$name]) ; else return(0) ;

}





?>