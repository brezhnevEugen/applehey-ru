<?php // этот модуль необходимо делать тольео для крупных проектов, для небольшых описание пунктов меню и файлов меню лучше делать в init.php
$__functions['init'][]      ='_accounts_admin_vars' ;
$__functions['boot_admin'][] ='_accounts_admin_boot' ;
//$__functions['alert'][]   ='_accounts_admin_alert' ;
$__functions['install'][]	='_accounts_install_modul' ;



function _accounts_admin_vars() //
{   //-----------------------------------------------------------------------------------------------------------------------------
	// описание переменных модуля
	//-----------------------------------------------------------------------------------------------------------------------------
    //$_SESSION['ARR_roll'][9]=array('obj_name'=>'Франчайзер',   'index_page'=>'/cab/bill/') ;
    $_SESSION['menu_admin_site']['Модули']['account']=array('name'=>'Аккаунты','href'=>'editor_accounts.php','icon'=>'menu/group.jpg') ;
    //$_SESSION['menu_admin_site']['Модули'][]=array('name'=>'Аккаунты','href'=>'editor_account.php') ;

	//-----------------------------------------------------------------------------------------------------------------------------
	// описание классов модуля
	//-----------------------------------------------------------------------------------------------------------------------------

    // запись в таблице аккаунтов
    $_SESSION['descr_clss'][86]['name']='Аккаунт' ;
    $_SESSION['descr_clss'][86]['parent']=0 ;
    $_SESSION['descr_clss'][86]['fields']=array('login'=>'varchar(255)','password'=>'varchar(255)','email'=>'varchar(255)','act_code'=>'varchar(255)','reffer'=>'any_object','info'=>'serialize','reboot'=>'int(1)') ;
    $_SESSION['descr_clss'][86]['fields']['rol']=array('type'=>'indx_select','array'=>'ARR_roll') ;
    $_SESSION['descr_clss'][86]['fields']['ext_info']='serialize';

    $_SESSION['descr_clss'][86]['fields']['auth_by_pass']='int(1)';

    //$_SESSION['descr_clss'][86]['on_change_event']['password']='on_change_event_clss_86_password' ;
    $_SESSION['descr_clss'][86]['child_to']=array(1) ;
    $_SESSION['descr_clss'][86]['parent_to']=array() ;
    $_SESSION['descr_clss'][86]['no_show_in_tree']=1 ; // не показывать объекты класса в дереве
    
    $_SESSION['descr_clss'][86]['list']['field']['pkey']			=array('title'=>'ID') ;
   	$_SESSION['descr_clss'][86]['list']['field']['enabled']			=array('title'=>'Сост.') ;
   	$_SESSION['descr_clss'][86]['list']['field']['obj_name']			=array('title'=>'ФИО','td_class'=>'left') ;
   	$_SESSION['descr_clss'][86]['list']['field']['rol']		 	    =array('title'=>'Роль','td_class'=>'left') ;
   	$_SESSION['descr_clss'][86]['list']['field']['login']				=array('title'=>'Логин') ;
    $_SESSION['descr_clss'][86]['list']['field']['password']			=array('title'=>'Пароль') ;
    $_SESSION['descr_clss'][86]['list']['field']['ip']			        =array('title'=>'Текущий сеанс','readonly'=>1) ;
    $_SESSION['descr_clss'][86]['list']['field']['monitor']			=array('title'=>'Режим монитора') ;
    $_SESSION['descr_clss'][86]['list']['field']['time_login']			=array('title'=>'Вход','readonly'=>1) ;
    $_SESSION['descr_clss'][86]['list']['field']['time_logout']		=array('title'=>'Выход','readonly'=>1) ;
    $_SESSION['descr_clss'][86]['list']['field']['smena_in']			=array('title'=>'Начало смены','readonly'=>1) ;
    $_SESSION['descr_clss'][86]['list']['field']['smena_out']		    =array('title'=>'Окончание смены','readonly'=>1) ;
    $_SESSION['descr_clss'][86]['list']['field']['smena']		        =array('title'=>'Активная смена') ;

    // ключи для rutokenweb
    $_SESSION['descr_clss'][150]['name']='ключи для rutokenweb' ;
    $_SESSION['descr_clss'][150]['parent']=86 ;
    $_SESSION['descr_clss'][150]['fields']['user_xkey']='varchar(64)';
    $_SESSION['descr_clss'][150]['fields']['user_ykey']='varchar(64)';
    $_SESSION['descr_clss'][150]['fields']['r_xkey']='varchar(64)';
    $_SESSION['descr_clss'][150]['fields']['r_ykey']='varchar(64)';
    $_SESSION['descr_clss'][150]['fields']['token_ID']='varchar(32)';

    //-----------------------------------------------------------------------------------------------------
    $_SESSION['descr_clss'][210]=array() ;
    $_SESSION['descr_clss'][210]['name']='Персонал компании' ;
   	$_SESSION['descr_clss'][210]['parent']=86 ;
   	$_SESSION['descr_clss'][210]['fields']=array('obj_name'=>'varchar(255)') ;
   	$_SESSION['descr_clss'][210]['fields']['obj_name']='varchar(255)';
   	$_SESSION['descr_clss'][210]['fields']['rol']=array('type'=>'indx_select','array'=>'ARR_roll') ;
    $_SESSION['descr_clss'][210]['fields']['ip']=array('type'=>'varchar(21)') ;
    $_SESSION['descr_clss'][210]['fields']['arm_id']=array('type'=>'int(11)') ;
    $_SESSION['descr_clss'][210]['fields']['sess_id']=array('type'=>'varchar(32)') ;
    $_SESSION['descr_clss'][210]['fields']['time_login']=array('type'=>'timedata','data_format'=>'d.m.y G:i') ;
    $_SESSION['descr_clss'][210]['fields']['time_logout']=array('type'=>'timedata','data_format'=>'d.m.y G:i') ;
    $_SESSION['descr_clss'][210]['fields']['smena_in']=array('type'=>'timedata','data_format'=>'d.m.y G:i') ;
    $_SESSION['descr_clss'][210]['fields']['smena_out']=array('type'=>'timedata','data_format'=>'d.m.y G:i') ;
    $_SESSION['descr_clss'][210]['fields']['smena']=array('type'=>'indx_select','array'=>'ARR_smena_status') ; // активная смена
    $_SESSION['descr_clss'][210]['fields']['sert_id']=array('type'=>'varchar(128)') ;
    $_SESSION['descr_clss'][210]['fields']['sert_name']=array('type'=>'varchar(128)') ;
    $_SESSION['descr_clss'][210]['fields']['sert_ud']=array('type'=>'varchar(128)') ; // удостоверяющий центр
    $_SESSION['descr_clss'][210]['fields']['sert_from']=array('type'=>'timedata','data_format'=>'d.m.y G:i') ; // срок дейтствия ЭЦП
    $_SESSION['descr_clss'][210]['fields']['sert_to']=array('type'=>'timedata','data_format'=>'d.m.y G:i') ; // срок дейтствия ЭЦП
    $_SESSION['descr_clss'][210]['fields']['access_pass']=array('type'=>'varchar(32)') ;
    $_SESSION['descr_clss'][210]['fields']['email']=array('type'=>'varchar(128)') ;
    $_SESSION['descr_clss'][210]['fields']['phone']=array('type'=>'varchar(128)') ;
    $_SESSION['descr_clss'][210]['fields']['contact_name']=array('type'=>'varchar(128)') ;
    $_SESSION['descr_clss'][210]['fields']['monitor']=array('type'=>'indx_select','array'=>'ARR_monitor') ;
    $_SESSION['descr_clss'][210]['fields']['UID']=array('type'=>'varchar(32)') ;

    $_SESSION['descr_clss'][210]['child_to']=array(206,1) ;
    $_SESSION['descr_clss'][210]['parent_to']=array(3,5) ;
    $_SESSION['descr_clss'][210]['no_show_in_tree']=0 ; // не показывать объекты класса в дереве
    $_SESSION['descr_clss'][210]['icons']=_PATH_TO_BASED_CLSS_IMG.'/98.png' ;
   	$_SESSION['descr_clss'][210]['list']['field']['pkey']			=array('title'=>'ID') ;
   	$_SESSION['descr_clss'][210]['list']['field']['enabled']			=array('title'=>'Сост.') ;
   	$_SESSION['descr_clss'][210]['list']['field']['obj_name']			=array('title'=>'ФИО','td_class'=>'left') ;
   	$_SESSION['descr_clss'][210]['list']['field']['rol']		 	    =array('title'=>'Роль','td_class'=>'left') ;
   	$_SESSION['descr_clss'][210]['list']['field']['login']				=array('title'=>'Логин') ;
    $_SESSION['descr_clss'][210]['list']['field']['password']			=array('title'=>'Пароль') ;
    $_SESSION['descr_clss'][210]['list']['field']['ip']			        =array('title'=>'Текущий сеанс','readonly'=>1) ;
    $_SESSION['descr_clss'][210]['list']['field']['monitor']			=array('title'=>'Режим монитора') ;
    $_SESSION['descr_clss'][210]['list']['field']['time_login']			=array('title'=>'Вход','readonly'=>1) ;
    $_SESSION['descr_clss'][210]['list']['field']['time_logout']		=array('title'=>'Выход','readonly'=>1) ;
    $_SESSION['descr_clss'][210]['list']['field']['smena_in']			=array('title'=>'Начало смены','readonly'=>1) ;
    $_SESSION['descr_clss'][210]['list']['field']['smena_out']		    =array('title'=>'Окончание смены','readonly'=>1) ;
    $_SESSION['descr_clss'][210]['list']['field']['smena']		        =array('title'=>'Активная смена') ;

    $_SESSION['descr_clss'][210]['details']=$_SESSION['descr_clss'][210]['list'] ;
    $_SESSION['descr_clss'][210]['details']['field']['sert_id']=array('title'=>'id сертификата') ;
    $_SESSION['descr_clss'][210]['details']['field']['sert_name']=array('title'=>'Название сертификата') ;
    $_SESSION['descr_clss'][210]['details']['field']['sert_ud']=array('title'=>'Удостоверяющий центр') ;
    $_SESSION['descr_clss'][210]['details']['field']['sert_from']=array('title'=>'Срок действия, от') ;
    $_SESSION['descr_clss'][210]['details']['field']['sert_to']=array('title'=>'Срок действия, до') ;

    

}

function _accounts_admin_boot($options)
{
    create_system_modul_obj('accounts',$options) ;

}


function _accounts_install_modul($DOT_root)
{   echo '<h2>Инсталируем модуль <strong>АККАУНТЫ</strong></h2>' ;

    // аккаунты ЭСМО
    $pattern=array() ;
    $pattern['table_title']		        = 	'Аккаунты' ;
    $pattern['use_clss']		        = 	'1,86' ;
    $pattern['def_recs'][]		        =	array ('parent'=>0,	'clss'=>1,	'enabled'=>1,	'indx'=>0,	'obj_name'=>'ЭСМО') ;
    $pattern['table_name']				= 	'obj_site_account' ;
    $pattern['table_clss']				= 	101 ;
    $pattern['table_parent']			= 	$DOT_root;
    $id_DOT_account=create_table_by_pattern($pattern) ;

    // аккаунты ЭСМО - фото
    $pattern=array() ;
    $pattern['table_title']				= 	'Фото' ;
    $pattern['use_clss']				= 	'3' ;
    $pattern['table_name']				= 	'obj_site_account_image' ;
    $pattern['table_clss']				= 	101 ;
    $pattern['table_parent']			= 	$id_DOT_account;
    $pattern['dir_to_image']			= 	'public/catalog/';
    create_table_by_pattern($pattern) ;

    // аккаунты ЭСМО - файлы
    $pattern=array() ;
    $pattern['table_title']				= 	'Файлы' ;
    $pattern['use_clss']				= 	'5' ;
    $pattern['table_name']				= 	'obj_site_account_files' ;
    $pattern['table_clss']				= 	101 ;
    $pattern['table_parent']			= 	$id_DOT_account;
    $pattern['dir_to_image']			= 	'/public/catalog/';
    create_table_by_pattern($pattern) ;

    $file_items=array() ;
    // editor_accounts.php
    $file_items['editor_accounts.php']['include'][]='_DIR_TO_ENGINE."/admin/c_site.php"' ;
    $file_items['editor_accounts.php']['options']['use_table_code']='"TM_account"' ;
    $file_items['editor_accounts.php']['options']['title']='"ЭСМО' ;
    $file_items['editor_accounts.php']['options']['use_class']='"c_editor_obj"' ;
    create_menu_item($file_items) ;

    $id_sect=SETUP_add_section_to_site_setting('Авторизация') ;
    SETUP_add_rec_to_site_setting($id_sect,"Авторизация по паролю",1,'LS_auth_by_pass') ;
    SETUP_add_rec_to_site_setting($id_sect,"Авторизация по Рутокен ЭЦП",1,'LS_auth_by_token') ;
    SETUP_add_rec_to_site_setting($id_sect,"Имитация ЭЦП",0,'LS_evulate_ECP') ;

}




?>