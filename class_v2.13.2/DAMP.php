<?
$arr=array(1,2,3) ;
DUMP()->dump_array($arr) ;

$arr=array(array(1,2,3),array(4,5,6),array(7,8,9)) ;
DUMP()->dump_array($arr) ;

$order['order_name']='Название заказа' ;
$order['order_type']='Готовая продукция' ;
$order['shipping_date']='2016-03-03 00:00:00' ;
$order['shipping_place']='оставить на производстве' ;
$order['stuff']='фанера' ;
$order['thickness']='2мм' ;
$order['grinding']='1' ;
$order['painting']='С одной стороны' ;
$order['color']='зелёный' ;
$order['glitter']='1' ;
$order['urgency']='0' ;
$order['description']='что-то там описал' ;
$order['account']='5748875489875743' ;
$order['manager']='Александр Шилин' ;
$order['lead_id']='1888728' ;
DUMP()->dump_array($order) ;


function DUMP() {$dump_obj=new c_DUMP();return($dump_obj);}
class c_DUMP
{
    // вывод двухмерного массива
    function print_2x_arr(&$arr=array(),$options=array())
   	{ if (!is_array($options)) { $title=$options ; $options=array() ; }
         if (!isset($options['no_show_index'])) $options['no_show_index']=0 ;
         if (sizeof($arr))
         { ?><table class="basic fz_small left debug print_2x_arr tablesorter"><thead><?
           // собираем все ключи всех таблиц
           $sum_titles=array() ;
           foreach ($arr as $finfo) if (sizeof($finfo)) $sum_titles=array_merge($sum_titles,array_keys($finfo)) ; else $sum_titles=array() ;
           // удаляем повторяющеся поля
           $ftitles=array_unique($sum_titles) ;

           if ($title) echo '<tr class=clss_header><td colspan='.(sizeof($ftitles)+1).'>'.$title.'</td><tr>' ;
           echo '<tr class=td_header>' ;
           if (!$options['no_show_index']) echo '<th>&nbsp;</th>' ;
           foreach ($ftitles as $title) echo '<th>'.$title.'</th>' ; ?></tr></thead><tbody><?
           foreach ($arr as $fname=>$finfo)
            {  ?><tr>
                 <?if (!$options['no_show_index']){?><td><? echo $fname?></td><?}
              	  foreach ($ftitles as $title)
                 { $value=$finfo[$title] ;
                   ?><td><? if (is_array($finfo[$title])) $this->print_1x_arr($finfo[$title]) ;
                            else if (!is_object($finfo[$title])) echo ($value) ;
                            else { echo 'Объект класса '.get_class($value) ; }
                     ?></td><?
                 }
               ?></tr><?
            }
           ?></tbody></table><?
         } else echo 'array()<br/>';
   	}

    function print_1x_arr($arr=array())
   	{ if (sizeof($arr))
         { ?><table class="left debug $this->print_1x_arr"><?
           foreach ($arr as $fname=>$finfo)
            { ?><tr><td><? echo $fname?></td><td><?
                    if (is_array($finfo)) $this->print_1x_arr($finfo) ;
                    else if (is_object($finfo)) { if (get_class($finfo)=='stdClass') damp_array($finfo,1,-1) ;
                                                  else echo 'Объект "'.get_class($finfo).'"'  ;/*else print_r($finfo) ;*/
                                                 }
                    else
                    {   if (strlen($finfo)<=128) echo htmlentities($finfo) ;
                        else if (base64_decode($finfo, true)) {?><img alt="" src="data:image/jpeg;base64,<?echo $finfo?>"  alt=""><?}
                        else {?><div class="short_view"><?
                            //echo htmlentities($finfo)
                            echo ($finfo)
                            ?></div><?}
                    }
               ?></td></tr><?
            }
           ?></table><?
         } else echo 'array()<br/>';
   	}

    // $mode = 1: выводить массивы через 1x, =2: выводить массивы через 2x
    function dump_array($arr,$mode=1)
    { if (sizeof($arr))
      {  ?><style>
            table.debug {table-layout:fixed;border-collapse:collapse;empty-cells:show;margin:10px 0;background:white;}
            table.debug td{border:1px solid gray;padding:5px;}
            table.debug td strong{color:black;}
          </style>
          <table class="left debug damp_array"><?
        if ((is_array($arr) or is_object($arr)) and sizeof($arr))
        { foreach ($arr as $key=>$value) if (!is_object($value))
         { ?><tr><?
           if (!is_array($value) and !is_object($value))
           { ?><td><? echo $key ; ?></td><td><?
                 //if (strlen($value)<=128) echo htmlentities($value) ;
                 if (strlen($value)<=128 or $key=='debug_db') echo ($value) ;
                 else if (base64_decode($value, true)) {?><img alt="" src="data:image/jpeg;base64,<?echo $value?>"  alt=""><?}
                 else {?><div class="short_view"><?
                     //echo htmlentities($value)
                     echo ($value)
                     ?></div><?}
               ?></td><? ; } // если значение не массив
     		else // значение - массив
     		{ list($key1,$value1)=each($value) ; // получаем первый элемент массива
             reset($value) ;
             if (!is_array($value1)) { ?><td><strong><? echo $key ; ?></strong></td><td><? $this->print_1x_arr($value) ; ?></td><? }  // если массив состоит из строк - выводим его
             else
             { list($key2,$value2)=each($value1) ; // получаем первый элемент дочернего массива
             	reset($value1) ;
              	if (!is_array($value2)) { ?><td><strong><? echo $key ; ?></strong></td><td><? if ($mode==1) $this->print_1x_arr($value) ; else $this->print_2x_arr($value) ;?></td><? }
               else
               { ?><td><strong><? echo $key ; ?></strong></td><td><? damp_array($value,$mode,-1) ; ?></td><? }
     		  }
   		}
   		?></tr><?
   	  }
         else
         {
           ?><tr><td><? echo $key ; ?></td><td><? if (is_object($value)) echo 'object "<strong>'.get_class($value).'"</strong>' ; ?></td></tr><? ;

         }

        }
    	 ?></table><?
      }
    }

    function damp_string($str)
    { $arr2=array() ;
      $arr=str_split($str) ;
      if (sizeof($arr)) foreach($arr as $i=>$ch) $arr2[$i]=array($ch,ord($ch)) ;
      $this->print_2x_arr($arr2) ;
    }
}
?>