<?php
define('_ENGINE_MODE','admin') ;
include_once($_SERVER['DOCUMENT_ROOT']."/ini/patch.php")  ;
include_once(_DIR_TO_ENGINE."/i_admin.php") ;
include_once(_DIR_TO_ENGINE."/i_system.php") ;
//
$options=array() ;
$options['class_file']='this' ;
$reboot_all=1 ;
show_page($options) ;
class c_fancyupload //extends c_page
{
  function show()
  {     set_time_limit(0) ;
        include_once(_DIR_TO_ENGINE."/admin/i_img_working.php") ;
        ob_start() ;
        $result = array();


        $result['time'] = date('r');
        $result['addr'] = substr_replace(gethostbyaddr($_SERVER['REMOTE_ADDR']), '******', 0, 6);
        $result['agent'] = $_SERVER['HTTP_USER_AGENT'];

        if (count($_GET)) {$result['get'] = $_GET;}
        if (count($_POST)) {$result['post'] = $_POST;}
        if (count($_FILES)) {$result['files'] = $_FILES;}

        // Проверка загруженного файла
        $error = false;
        /*
        $max_upload = (int)(ini_get('upload_max_filesize')) ;
        $max_post = (int)(ini_get('post_max_size'));
        $memory_limit = (int)(ini_get('memory_limit'));
        $upload_mb = min($max_upload, $max_post, $memory_limit);
        if (!$error && $_FILES['Filedata']['size'] > $upload_mb * 1024 * 1024) $error = 'Превышен размер ограничения на размер загружаемого файла () '.$upload_mb.'Mb!';
        */

        if (!$error and (!isset($_FILES['Filedata']) || !is_uploaded_file($_FILES['Filedata']['tmp_name']))) $error = 'Ошибка загрузки';
         /*
        if (!$error && !($size = @getimagesize($_FILES['Filedata']['tmp_name']) ) ) { $error = 'Please upload only images, no other files are supported.';}

        if (!$error && !in_array($size[2], array(1, 2, 3, 7, 8) ) ) {$error = 'Please upload only images of type JPEG, GIF or PNG.';}

        if (!$error && ($size[0] < 25) || ($size[1] < 25)) { $error = 'Please upload an image bigger than 25px.'; }
           */

        // Обработка загруженного файла
        $desct_dir=_DIR_TO_ROOT._TEMP_UPLOAD_DIR ;
        $php_tmp_name=basename($_FILES['Filedata']['tmp_name']) ;
        $dest_fname_source=$_POST['UID'].'_'.$php_tmp_name.'.jpg' ;
        // сохраняем оригинал под временным именем
        move_uploaded_file($_FILES['Filedata']['tmp_name'], $desct_dir.$php_tmp_name);
        $img_info = @getimagesize($desct_dir.$php_tmp_name);
        $id_type=@exif_imagetype($desct_dir.$php_tmp_name) ;
        $method='' ;
        if ($id_type==2 or $id_type==1 or $id_type==3 or $id_type==15) { $method='GD' ; $dest_fname_source=$_POST['UID'].'_'.$php_tmp_name.'.jpg' ; }
        if ($id_type==6)                                               { $method='IM' ; $dest_fname_source=$_POST['UID'].'_'.$php_tmp_name.'.gif' ; }
        if (!$method)
        { $error='<div class="green">Сохраните изображение в другом формате и попробуйте еще раз.</div>' ;
          copy($desct_dir.$php_tmp_name,_DIR_TO_ROOT.'/error_img/'.$_FILES['Filedata']['name']) ;
          _event_reg('Загрузка НЕ изображения',$_FILES['Filedata']['name']) ;
        }
        //damp_array($img_info) ;
        // сразу уменьшаем размер оригинала под заданный размер
        else
        {
           if ($method=='IM')
           {  $image = new Imagick($desct_dir.$php_tmp_name);
              $image->setImageFormat("gif");
              if ($img_info[0]>=1200)
                { $image->thumbnailImage(1200,1200,true,true);
                  $image->writeImage($desct_dir.$dest_fname_source);
                }
                else copy($desct_dir.$php_tmp_name,$desct_dir.$dest_fname_source) ;

                $image->thumbnailImage(200,200,true,true);
                $image->writeImage($desct_dir.'200_'.$dest_fname_source);

                $image->thumbnailImage(100,100,true,true);
                $image->writeImage($desct_dir.'small_'.$dest_fname_source);

                $image->clear();
                $image->destroy();
           }

           if ($method=='GD')
           {   if ($img_info[0]>=1200)
                { $options['resize']=array('width'=>1200,'resampled'=>1,'back_color'=>'FFFFFF','quality'=>70);
                  $options['output_fname']=$desct_dir.$dest_fname_source ;
                  $res=working_image($desct_dir.$php_tmp_name,$options) ;
                }
                else
                { copy($desct_dir.$php_tmp_name,$desct_dir.$dest_fname_source) ;
                  $res=1 ;
                }
                if (!$res) echo '<span class="red bold"> Не удалось обработать изображение (несовместимый формат или файл поврежден) </span><br>' ;
                else // делаем две превьшки изображения - small и 200
                { $options['resize']=$_SESSION['img_pattern']['Клиенты'][200]['resize'] ;
                  $options['resize']=array('width'=>200,'resampled'=>1,'quality'=>100,'back_color'=>'#FFFFFF');
                  $options['output_fname']=$desct_dir.'200_'.$dest_fname_source ; ;
                  working_image($desct_dir.$dest_fname_source,$options) ;

                  $options['resize']=array('width'=>100,'height'=>100,'fields'=>1,'resampled'=>1,'back_color'=>'FFFFFF','quality'=>100);
                  $options['output_fname']=$desct_dir.'small_'.$dest_fname_source ; ;
                  working_image($desct_dir.$dest_fname_source,$options) ;
                }
           }
        }
        // формируем текст результата загрузки файла
        if ($error) $return = array('status' => '0','error' => $error);
        else
        { $return = array( 'status' => '1','name' => $_FILES['Filedata']['name']);
          // Our processing, we get a hash value from the file
          $return['hash'] =  md5_file($desct_dir.$php_tmp_name);
          // ... and if available, we get image data
          $img_info = getimagesize($desct_dir.$php_tmp_name);
          if (sizeof($img_info))
          { $return['width']    = $img_info[0];
            $return['height']   = $img_info[1];
            $return['mime']     = $img_info['mime'] ;
            $return['preview_url']  = _TEMP_UPLOAD_DIR.'small_'.$dest_fname_source;
            $return['img_name']  = $dest_fname_source;;
          }
        }
        // удаляем исходный файл
        unlink($desct_dir.$php_tmp_name) ;
        $return['trace'] = nl2br(ob_get_clean());



        // Output

        /**
         * Again, a demo case. We can switch here, for different showcases
         * between different formats. You can also return plain data, like an URL
         * or whatever you want.
         *
         * The Content-type headers are uncommented, since Flash doesn't care for them
         * anyway. This way also the IFrame-based uploader sees the content.
         */

        if (isset($_REQUEST['response']) && $_REQUEST['response'] == 'xml')
        {	// header('Content-type: text/xml');
        	// Really dirty, use DOM and CDATA section!
        	echo '<response>';
        	foreach ($return as $key => $value)  echo "<$key><![CDATA[$value]]></$key>";
        	echo '</response>';
        }
        else
        {	// header('Content-type: application/json');
        	echo json_encode($return);
        }
   }

}
?>