function panel_image_preview_on(){$j('div#panel_image_preview').addClass('active').html('<img src="'+$j(this).attr('src')+'">')}
function panel_image_preview_off(){$j('div#panel_image_preview').removeClass('active');}

$j(document).ready(function()
{
  $j('div.list_mo_sessions td.photo div.img img').hover(panel_image_preview_on,panel_image_preview_off) ;

  $j('input.datepicker').datepicker($j.datepicker.regional["ru"]);
  //$j('input.datetimepicker').datepicker($j.datepicker.regional["ru"]);
  $j('input.datetimepicker').datetimepicker($j.datepicker.regional["ru"]);

  $j(".select2").select2() ;

    // открыть расширенную панель фильтра
    $j("#button_open_right_panel").click(function()
    { $j('table#block_center td#center_right div#block_right').removeClass('hidden'); // правая панель
      $j('table#block_center td#center_left div#panel_report_view > div.inner').addClass('hidden');  // кнопка "открыть расширенный фильтр"
      $j('table#block_center td#center_right div#button_close_right_panel').removeClass('hidden');
      $j('table#block_center td#center_right div#button_hidden_right_panel').removeClass('hidden');
      send_ajax_request({cmd:'save_filter_state',value:1,report_code:$j(this).attr('report_code')});
    }) ;

    // закрыть расширенную панель фильтра
    $j("#button_close_right_panel").click(function()
    { $j('table#block_center td#center_right div#block_right').addClass('hidden'); // правая панель
      $j('table#block_center td#center_left div#panel_report_view > div.inner').removeClass('hidden');  // кнопка "открыть расширенный фильтр"
      $j('table#block_center td#center_right div#button_close_right_panel').addClass('hidden'); // кнопка "закрыть расширенный филтр"
      $j('table#block_center td#center_right div#button_show_right_panel').addClass('hidden');  // кнопка "скрыть расширенный филтр"
      $j('table#block_center td#center_right div#button_hidden_right_panel').addClass('hidden');
      send_ajax_request({cmd:'save_filter_state',value:0,report_code:$j(this).attr('report_code')});
    }) ;

  // показать расширенную панель филтра
  $j("#button_show_right_panel").click(function()
  { console.log('button_show_right_panel') ;
    $j('table#block_center td#center_right div#block_right').removeClass('hidden');
    $j('table#block_center td#center_right div#button_hidden_right_panel').removeClass('hidden');
    $j('table#block_center td#center_right div#button_close_right_panel').removeClass('hidden');
    $j('table#block_center td#center_right div#button_show_right_panel').addClass('hidden');
    send_ajax_request({cmd:'save_filter_state',value:1,report_code:$j(this).attr('report_code')});
  }) ;

  // скрыть расширенную панель фильтра
  $j("#button_hidden_right_panel").click(function()
  {  console.log('button_hidden_right_panel') ;
     $j('table#block_center td#center_right div#block_right').addClass('hidden');
     $j('table#block_center td#center_right div#button_hidden_right_panel').addClass('hidden');
     $j('table#block_center td#center_right div#button_close_right_panel').addClass('hidden');
     $j('table#block_center td#center_right div#button_show_right_panel').removeClass('hidden');
     send_ajax_request({cmd:'save_filter_state',value:2,report_code:$j(this).attr('report_code')});
  }) ;


  $j('a.new_window').live('click',function()
  { //  var newWin=window.open($j(this).attr('href'),$j(this).attr('title'),'status=1,dependent=1,width=1920,height=1000,left=1920,top=200, resizable=1, scrollbars=yes') ;
      var newWin=window.open($j(this).attr('href'),$j(this).attr('title'),'status=1,dependent=1,width=1600,height=800,left=0,top=200, resizable=1, scrollbars=yes') ;
      //var newWin=window.open($j(this).attr('href'),'main_window') ;
      newWin.focus() ;
      return(false) ;
  })

  $j('.close_window').live('click',function()
  {   window.close() ;
  })

  $j('a#show_window_monitor').live('click',function()
  {   var width = 1350;
      var newWin=window.open('','MO_MONITOR','resizable=1,scrollbars=1,fullscreen=0,height=1000,width=' + width + '  , left=' + screen.width + ', toolbar=0, menubar=0,status=1');
      newWin.location = $j(this).attr('href') ;
      newWin.focus() ;
      return(false) ;
  })


function popup_params(width, height) {
    var a = typeof window.screenX != 'undefined' ? window.screenX : window.screenLeft;
    var i = typeof window.screenY != 'undefined' ? window.screenY : window.screenTop;
    var g = typeof window.outerWidth!='undefined' ? window.outerWidth : document.documentElement.clientWidth;
    var f = typeof window.outerHeight != 'undefined' ? window.outerHeight: (document.documentElement.clientHeight - 22);
    var h = (a < 0) ? window.screen.width + a : a;
    var left = parseInt(h + ((g - width) / 2), 10);
    var top = parseInt(i + ((f-height) / 2.5), 10);
    return 'width=' + width + ',height=' + height + ',left=' + left + ',top=' + top + ',scrollbars=1';
}





  $j('div.short_view').live('click',function()  {  $j(this).toggleClass('open') ;}) ;

  if ($j('input[mask]').length) $j('input[mask]').each(function()
   {$j(this).mask($j(this).attr('mask'));
    $j(this).focus(function() { setCaretToPos(this,0);});

   }) ;
  /*
  $j.extend($j.inputmask.defaults.definitions,
  {       'A': {
               validator: "[A-Za-z0-9]",
               cardinality: 1,
               casing: "upper" //auto uppercasing
           }
       });


   $j(":input").inputmask();
   */

}) ;

function setSelectionRange(input, selectionStart, selectionEnd) {
  if (input.setSelectionRange) {
    input.focus();
    input.setSelectionRange(selectionStart, selectionEnd);
  }
  else if (input.createTextRange) {
    var range = input.createTextRange();
    range.collapse(true);
    range.moveEnd('character', selectionEnd);
    range.moveStart('character', selectionStart);
    range.select();
  }
}

function setCaretToPos (input, pos) {
  setSelectionRange(input, pos, pos);
}





