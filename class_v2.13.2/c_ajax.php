<?
include(_DIR_TO_ENGINE.'/c_page_XML_AJAX.php') ;
class c_page_ajax extends c_page_XML_AJAX
{
   function set_filter()
    {
      $url=REPORTS()->prepare_url($_POST['filter'],array('base_url'=>$_POST['filter_dir'])) ;
      $this->go_url=$url ;
      //damp_array($_POST,1,-1) ; echo $url ; $this->add_element('show_modal_window','HTML') ;
    }

    function save_setting()
    {  if (sizeof($_POST['setting'])) foreach($_POST['setting'] as $name=>$value) update_site_setting($name,$value) ;
      $this->add_element('show_notife','Сохранено',array('type'=>'info')) ;
      send_cmd_reboot_client() ;
    }

    //==========================================================================================================================================================================
     // РАБОТА СО СПИСКАМИ
     //==========================================================================================================================================================================

     function list_rec_update()
     {
         $res=array() ;
         //$this->add_element('success','modal_window') ; damp_array($_POST) ;
         if (sizeof($_POST['list_rec'])) foreach($_POST['list_rec'] as $id=>$rec) $res[]=LISTS()->list_rec_update($_POST['list_name'],$id,$rec) ;
         if (sizeof($res)) foreach($res as $res_info) if ($res_info['found'] and $res_info['update']) $this->add_element('show_notife','Сохранено',array('type'=>'info')) ;
     }

     function list_rec_create()
     {
       if ($_POST['new_rec'])
       { if (is_object(_IL($_POST['list_name'])))
         { $list_id=_IL($_POST['list_name'])->id ;
           $res=LISTS()->list_rec_create($list_id,array('obj_name'=>$_POST['new_rec'])) ;
           if ($res['pkey']) $this->add_element('update_page',1) ;
         }
       }
     }

     function list_rec_delete()
     {
       LISTS()->list_rec_delete($_POST['reffer']) ;
       $this->add_element('update_page',1) ;
       //$this->add_element('success','modal_window') ; damp_array($res) ;
     }


    function save_filter_state()
    {
        $_SESSION['filter_status'][$_POST['report_code']]=$_POST['value'];
    }

    function reboot()
      { reboot_engine() ;
        ACCOUNTS()->update_member_info(MEMBER()) ;
        $this->add_element('update_page',1) ;
      }


}
