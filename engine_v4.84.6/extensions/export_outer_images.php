<?
// 5.08.11 - переработка, изменено формирования каталога назначения
 function export_outer_images()
 {
    $show_fname=$_POST['show_fname'] ;
    $table_name=_DOT($_POST['tkey'])->table_name ;
    $value=execSQL_value('select '.$show_fname.' from '.$table_name.' where pkey='.$_POST['pkey']) ;

    $arr_outer_images=pc_image_extractor($value) ;

    $not_found_img=array() ; $ext_img=array() ;
    if (sizeof($arr_outer_images)) foreach($arr_outer_images as $rec)
    {  $arr=parse_url($rec[0]) ;
       // если изображение находиться на текущем сайте
       if (!$arr['host'] or $arr['host']==_MAIN_DOMAIN or $arr['host']==_BASE_DOMAIN) { if (!file_exists(_DIR_TO_ROOT.$arr['path'])) $not_found_img[]=$rec[0] ; }
       // изображение находиться на врешнем хосте
       else $ext_img[]=$rec[0] ;
    }

    $table_code=str_replace('obj_','',$table_name) ;
    if (sizeof($ext_img)) foreach($ext_img as $name)
    { $source_file=$name ;
      //убираем все переносы строк
      $source_file=preg_replace('/\n/','',$source_file) ;
      $source_file=preg_replace('/\r/','',$source_file) ;
      $source_file=urldecode($source_file) ;

      $desc_dir=_DIR_TO_PUBLIC.'/'.$table_code.'/' ;
      if (!is_dir($desc_dir) and !create_dir($desc_dir)) { echo '<span class=red>Не удалось создать подкаталог текущей таблицы '.$desc_dir.'</span><br>' ; $desc_dir=_DIR_TO_PUBLIC.'/' ; }
      $desct_name=safe_file_names($_POST['pkey'].'_'.basename($source_file)) ;
      echo 'Копируем <strong>'.$source_file.'</strong> в <span class=green>'.hide_server_dir($desc_dir).$desct_name.'</span><br>' ;
      if (is_file($desc_dir.$desct_name))  if (!unlink($desc_dir.$desct_name)) { echo '&nbsp;&nbsp;Не удается заменить файл '.$desc_dir.$desct_name; continue ; }
      $img_context=file_get_contents($source_file) ;
      if (!$img_context) { echo '<span class=red>Не удалось получить удаленный файл:'.$source_file.'</span><br>' ; continue ; }
      $size=file_put_contents($desc_dir.$desct_name,$img_context) ;
      if (!$size) { echo '<span class=red>Не удается загрузить файл '.$desc_dir.$desct_name.'</span><br>'; continue ; }
      else echo '<span class=green>Загружено: '.round($size/1024).' кБ</span><br>' ;
      //echo 'Меняем '.$source_file.' на '.hide_server_dir($desc_dir.$desct_name).'<br>' ;
      $value=str_replace($name,hide_server_dir($desc_dir.$desct_name),$value) ;
    }
    $finfo=array($show_fname=>$value) ;
    //damp_array($finfo) ;
    update_rec_in_table($_POST['tkey'],$finfo,'pkey='.$_POST['pkey']) ;
    /*?><script type="text/javascript">$j('div#viewer_main button[cmd="save_list"]').remove();$j('div#viewer_main button[cmd="export_outer_images"]').remove();</script><?*/
    ?><br><button class="button" cmd="go_url" url="<?echo $_SERVER['HTTP_REFERER']?>">Вернуться к редактору</button><?

 }
?>