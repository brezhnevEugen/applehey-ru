<?
function import_from_drupal()
{ ?><h1>Импорт данных из Drupal</h1><?
  // ИМПОРТ БРЕНДОВ
  //$recs_brands=execSQL('select * from node where type="manufacturers"',2) ;
  //$recs_brands=execSQL('select * from term_node where nid=77',2) ;
  //$recs_brands=execSQL('select * from term_hierarchy where tid=271',2) ;

  // import_sections() ; // разделы каталога
  // import_goods() ; // товары
   //import_goods_images() ; // фото товаров
  //import_goods_files() ; // файлы товаров
  // import_news() ; // новости
  import_brands() ; // импорт описаний брендов
}


function import_sections()
{
  // в таблицах term_data, term_hierarchy - описание подразделов и их иеархия
  $recs_sections=execSQL('select t1.tid,t1.vid,t2.parent,t1.name,t1.description,t3.field_description_value from term_data t1 left join term_hierarchy t2 on t2.tid=t1.tid left join term_fields_term t3 on t3.tid=t1.tid',2) ;
  //print_2x_arr($recs_sections) ;
  //return ;
  execSQL_update('delete from obj_site_goods where parent!=0') ;
  execSQL_update('delete from obj_site_list where parent=2') ;
  if (sizeof($recs_sections)) foreach($recs_sections as $rec)
  {  $add_rec=array() ;
     if ($rec['vid']==1)
     { $add_rec['pkey']=$rec['tid'] ;
       $add_rec['clss']=10 ; // раздел каталога или элемент списка
       $add_rec['obj_name']=$rec['name'] ;
       $add_rec['parent']=($rec['parent'])? $rec['parent']:1;
       $add_rec['intro']=$rec['field_description_value'] ;
       $add_rec['manual']=$rec['description'] ;
       adding_rec_to_table('obj_site_goods',$add_rec) ;
     }

     if ($rec['vid']==2)
     { $add_rec['id']=$rec['tid'] ;
       $add_rec['clss']=20 ; // элемент списка
       $add_rec['obj_name']=$rec['name'] ;
       $add_rec['parent']=2;
       adding_rec_to_table('obj_site_list',$add_rec) ;
     }

  }

}

function import_goods()
{
    $recs_goods=execSQL('  select
                                t11.nid,
                                t11.type,
                                t11.title as obj_name,
                                t11.created as c_data,
                                t11.changed as r_data,
                                '/* используется в брендах
                                t6.field_image_m_fid,
                                t6.field_image_m_list,
                                t6.field_linl_value,
                                */.'
                                t8.field_guarantee_value as warrancy,
                                t8.field_osob_value as manual,
                                t8.field_new_value as new,
                                t8.field_gosreestr_value as reestr,
                                t12.body as intro,

                                t18.model as art,
                                t18.sell_price as price

                                from node t11
                                '/*left join content_type_manufacturers t6 on t6.nid=t11.nid*/.'
                                left join content_type_product t8 on t8.nid=t11.nid
                                left join node_revisions t12 on t12.nid=t11.nid
                                left join term_node t17 on t17.nid=t11.nid
                                left join uc_products t18 on t18.nid=t11.nid

                                where type="product"
                                order by tid') ;


        execSQL_update('delete from obj_site_goods where clss=2') ;
        if (sizeof($recs_goods)) foreach($recs_goods as $rec)
        {  $arr_parents=execSQL('select t1.tid,t2.obj_name as rasdel_name,t3.obj_name as brand_name from term_node t1
                                 left join obj_site_goods t2 on t2.pkey=t1.tid
                                 left join obj_site_list t3 on t3.id=t1.tid where t1.nid='.$rec['nid']) ;
           $i=1 ;
           if (sizeof($arr_parents)) foreach($arr_parents as $rec_parent)
           {  if ($rec_parent['rasdel_name'])
               {  $parent_fname=($i==1)? 'parent':'parent'.$i ;
                  $rec[$parent_fname]=$rec_parent['tid'] ;
                  $i++ ;
               }
              if ($rec_parent['brand_name']) $rec['brand']=$rec_parent['tid'] ;
           }
           echo 'Добавлен '.$rec['obj_name'].'<br>' ;
           $rec['clss']=2 ;
           unset($rec['type']) ;
           $id=adding_rec_to_table('obj_site_goods',$rec) ;
           $res[$id]=array('nid'=>$rec['nid'],'name'=>$rec['obj_name'],'parent'=>$rec['parent'],'parent2'=>$rec['parent2'],'parent3'=>$rec['parent3'],'parent4'=>$rec['parent4'],'parent5'=>$rec['parent5']) ;
        }
        print_2x_arr($res) ;
}

function import_goods_images()
{
    execSQL_update('delete from obj_site_goods_image where clss=3') ;
      $recs_images=execSQL('select t1.fid,t1.filename,t2.nid,t3.pkey as parent,t3.obj_name,t2.field_image_cache_data from files t1 left join content_field_image_cache t2 on t2.field_image_cache_fid=t1.fid left join obj_site_goods t3 on t3.nid=t2.nid where t2.nid>0',1) ;
      if (sizeof($recs_images)) foreach($recs_images as $rec) if ($rec['filename']!='nophoto.gif')
          {  $img_info=unserialize($rec['field_image_cache_data']) ;
             $add_rec=array() ;
             $add_rec['clss']=3 ; // раздел каталога или элемент списка
             $add_rec['obj_name']=$img_info['title'] ;
             $add_rec['alt']=$img_info['alt'] ;
             $add_rec['file_name']=$rec['filename'] ;
             $add_rec['parent']=$rec['parent'];
             //$add_rec['manual']=$rec['description'] ;
             //damp_array($add_rec) ;
             adding_rec_to_table('obj_site_goods_image',$add_rec) ;
             print_r($add_rec) ; echo '<br>' ;
          }
}

function import_goods_files()
{   execSQL_update('delete from obj_site_goods_files where clss=5') ;
    $recs_files=execSQL('select t1.fid,t1.filename,t2.nid,t3.pkey as parent,t3.obj_name from files t1 left join content_field_pdf t2 on t2.field_pdf_fid=t1.fid left join obj_site_goods t3 on t3.nid=t2.nid where t2.nid>0',1) ;
     if (sizeof($recs_files)) foreach($recs_files as $rec) //if ($rec['filename']!='nophoto.gif')
         {  $add_rec=array() ;
            $add_rec['clss']=5 ;
            $add_rec['obj_name']=$rec['filename'] ;
            $add_rec['file_name']=$rec['filename'] ;
            $add_rec['parent']=$rec['parent'];
            adding_rec_to_table('obj_site_goods_files',$add_rec) ;
            print_r($add_rec) ; echo '<br>' ;
         }
}


function import_news()
{  $recs=execSQL('  select
                               t11.nid,
                               t11.type,
                               t11.title as obj_name,
                               t11.created as c_data,
                               t11.changed as r_data,
                               t12.body as value

                               from node t11
                               left join content_type_product t8 on t8.nid=t11.nid
                               left join node_revisions t12 on t12.nid=t11.nid

                               where type="news"
                               order by tid') ;



       execSQL_update('delete from obj_site_news where clss=9') ;
       if (sizeof($recs)) foreach($recs as $rec)
       {  $rec['clss']=9 ;
          $rec['parent']=1;
          $id=adding_rec_to_table('obj_site_news',$rec) ;
          echo $id.'<br>' ;
       }
}


function import_brands()
{   execSQL_update('update obj_site_list set enabled=1 where parent=2') ;
    $list_brands=execSQL('select * from obj_site_list where clss=20 and parent=2',1) ;
    if (sizeof($list_brands)) foreach($list_brands as $rec_brand)
{
        $rec_info=execSQL('  select
                                    t11.nid,
                                    t11.title as obj_name,
                                    t11.created as c_data,
                                    t11.changed as r_data,
                                    t6.field_image_m_fid,
                                    t6.field_linl_value as url,
                                    t12.body as intro,
                                        t12.teaser as manual,
                                    t13.filename as filename


                                    from node t11
                                    left join content_type_manufacturers t6 on t6.nid=t11.nid
                                    left join node_revisions t12 on t12.nid=t11.nid
                                    left join files t13 on t13.fid=t6.field_image_m_fid
                                        left join node_revisions t15 on UCASE(t15.nid)=UCASE(t11.nid)

                                        where t11.title="%'.$rec_brand['obj_name'].'%"
                                    order by t11.nid',2) ;


    }


}

?>