<?
function get_outer_link()
 { global $send_mail_by_error,$obj_info ; $send_mail_by_error=0 ;
   ?><h1>Проверка доступности ссылок в текстах</h1>
     <p>Будет проведена проверка доступности внешних и внутренних ссылок в текстах текущего объекта "<strong><?echo $obj_info['obj_name']?></strong>" и его дочерних объектов</p>
     <p><strong>ВНИМАНИЕ!</strong> При большом количестве проверямеых объектов проверка может занять некоторое время. Не закрывайте эту страницу браузера, пока проверка не будет окончена.</p>
     <br>
     <button class=button cmd=get_outer_link_2>Проверить</button>
   <?
 }

 function get_outer_link_2()
 { global $send_mail_by_error,$obj_info ; $send_mail_by_error=0 ; $is_show_title=0 ;
   ?><h1>Проверка доступности ссылок в текстах</h1><?
   $arr_childs_pkeys=select_db_obj_all_child_pkeys($obj_info['tkey'],$obj_info['pkey']) ;
   //damp_array($arr_childs_pkeys) ;
   if (sizeof($arr_childs_pkeys)) foreach($arr_childs_pkeys as $tkey=>$clss_arr)
   { ?><h2>Проверяем таблицу "<?echo _DOT($tkey)->title?>"</h2><?
     if (sizeof($clss_arr)) foreach($clss_arr as $clss=>$pkeys)
     { ?><h3>Проверяем объекты "<?echo _CLSS($clss)->name?>"</h3><?
        $recs=execSQL(' select * from '._DOT($tkey)->table_name.' where pkey in ('.$pkeys.') order by pkey') ;
        echo '<strong>Будет проверено  '.sizeof($recs).' объектов '._CLSS($clss)->name.'</strong><br><br>' ;
        $link_cnt=0 ;
        ?><table class=left><tr class=clss_header><td>Поле</td><td>Тип ссылки</td><td>Хост</td><td>URL</td><td>Статус</td></tr><?
         if (sizeof($recs)) foreach($recs as $rec) foreach($rec as $fname=>$value) if (_CLSS($rec['clss'])->fields[$fname]=='text')
          {  $arr_links=pc_link_extractor($value) ;
             $arr_img=pc_image_extractor($value) ;
             $fname_title=_CLSS($rec['clss'])->view['list']['field'][$fname]['title'] ;
             if (!$fname_title) $fname_title=_CLSS($rec['clss'])->view['details']['field'][$fname]['title'] ;
             if (!$fname_title) $fname_title=$fname ;
             $is_show_title=0 ;
             if (sizeof($arr_links)) foreach($arr_links as $link_info)
                  { if (!$is_show_title) {?><tr class=clss_header><td colspan=5><?echo generate_ref_obj_viewer($rec)?></td></tr><? $is_show_title=1 ;}
                    show_link_info($link_info[0],'Ссылка',$fname_title) ;
                    $link_cnt++ ;
                  }
             if (sizeof($arr_img)) foreach($arr_img as $img_info)
                  { if (!$is_show_title) {?><tr class=clss_header><td colspan=5><?echo generate_ref_obj_viewer($rec)?></td></tr><? $is_show_title=1 ;}
                    show_link_info($img_info[0],'Изображение',$fname_title) ;
                    $link_cnt++ ;
                  }

          }
        if (!$link_cnt) {?><tr><td colspan=5>Ссылок не найдено</td></tr><?}
        ?></table><?



     }
   }

 }

 function show_link_info($link,$title,$fname_title)
 {  $arr_url=parse_url($link) ;
    $file_exs=404 ;
    $url='' ;
    ob_start() ;

    if ((!$arr_url['scheme'] and !$arr_url['host']) or $arr_url['host']==_MAIN_DOMAIN or $arr_url['host']==_BASE_DOMAIN)
    { $file_exs=@file_exists(_DIR_TO_ROOT.$arr_url['path']) ;
      $url=_PATH_TO_SITE.$arr_url['path'] ;
    }
    else if ($arr_url['scheme'] and $arr_url['host'])
    { $file_exs=get_page($arr_url['scheme'].'://'.$arr_url['host'].$arr_url['path'],array('return_only_code'=>1,'debug'=>0)) ;
      $url=$arr_url['scheme'].'://'.$arr_url['host'].$arr_url['path'] ;
    }
    ob_end_clean() ;
    ?><tr><td><? echo $fname_title?></td>
          <td><?echo $title?></td>
          <td class=right><? if ($arr_url['scheme']) echo $arr_url['scheme'].'://' ; echo $arr_url['host']?></td>
          <td><a href="<?echo $url?>" target=_blank><?echo urldecode($arr_url['path'])?></a></td>
          <td><?echo ($file_exs)? '<span class=green>OK</span>':'<span class=red>404</span>'?></td>
      </tr>
    <?
 }


 //damp_array($arr_url,1,-1) ;

         /*
           //$i++ ; if ($i>10) break ;
           //$res[]=array($arr[0]=>$arr[1]) ;
           //echo '<h2>'.$rec['obj_name'].'</h2>'  ;
           $arr_url=parse_url($link_info[0]) ;
           if (strpos($link_info[0],'/catalog')!==false) break ;
           print_r($arr_url) ;
           if (!$arr_url['scheme'] or ($arr_url['scheme']=='http' and strpos($arr_url['host'],'mir-technics.ru')!==false))
           { $path=str_replace('../../','/',$arr_url['path']) ;
             $arr_alt_path=explode('/',$path) ;
             $new_url='' ;
             //print_r($arr_alt_path) ;
             if ($arr_alt_path[1]=='category')
             { $section_id=$arr_alt_path[2] ;
               if (isset($_SESSION['goods_system']->tree[$section_id])) { $new_url=$_SESSION['goods_system']->tree[$section_id]->href ; ?><span class=green><? echo $new_url ; ?></span><?  }
               else                                         { ?><span class=red>не найдено</span><? }
             }
             if ($arr_alt_path[1]=='product')
             { $goods_id=$list_id[$arr_alt_path[2]]['pkey'] ;
               $section_id=execSQL_value('select parent from obj_site_goods where pkey='.$goods_id) ;
               $url_name=$list_id[$arr_alt_path[2]]['url_name'] ;
               if (isset($_SESSION['goods_system']->tree[$section_id])) { $new_url=$_SESSION['goods_system']->tree[$section_id]->href.$url_name.'.html' ; ?><span class=green><? echo $new_url ; ?></span><?  }
               else                                         { ?><span class=red>не найдено</span><? }
             }

             if ($new_url) { $rec['manual']=str_replace($link_info[0],$new_url,$rec['manual']) ; $update_arr['manual']=$rec['manual'] ; }
           }
           if (!$arr_url['scheme'] and !$arr_url['host'] and !file_exists(_DIR_TO_ROOT.$arr_url['path']))  // есть только path
           { // надо перетянуть файл на который ссылка со старого сайта
             $alt_url='http://mir-technics.ru'.$arr_url['path'] ;
             $new_dir=_DIR_TO_ROOT.'/public/catalog/'.safe_file_names(basename($arr_url['path'])) ;
             $new_path=hide_server_dir($new_dir) ;
             echo '<br>' ;

             $cnt=0 ;
             if (!file_exists($new_dir)) $cnt=copy($alt_url,$new_dir) ; else $cnt=1 ;
             if ($cnt) { $rec['manual']=str_replace($link_info[0],$new_path,$rec['manual']) ; $update_arr['manual']=$rec['manual'] ; echo '<span class=green>OK</span>' ; }
             else      { echo '<span class=red>Не удалось сохранить файл <strong>'.$alt_url.'</strong> в '.$new_path.'</span> alt_url='.$alt_url.'<br>new_dir='.$new_dir.'<br>' ; ;}
           }

           //echo ' ('.$rec['obj_name'].')<br>' ;
         */
         //}

         //$arr_img=pc_image_extractor($rec['manual']) ;
         //if (sizeof($arr_img)) foreach($arr_img as $img_info)
         //{
             //damp_array($arr_url,1,-1) ;
             /*
             if (!file_exists(_DIR_TO_ROOT.$arr_url['path']))
             {   print_r($arr_url) ;

                 if (!$arr_url['scheme'] and !$arr_url['host']) $alt_url='http://mir-technics.ru'.$arr_url['path'] ;  // есть только path
                 else                                           $alt_url=$img_info[0] ;

                 $new_dir=_DIR_TO_ROOT.'/public/catalog/'.$rec['pkey'].'_'.safe_file_names(basename($arr_url['path'])) ;
                 $new_path=hide_server_dir($new_dir) ;

                 if (!file_exists($new_dir)) $cnt=copy($alt_url,$new_dir) ; else $cnt=1 ;
                 if ($cnt) { $rec['manual']=str_replace($img_info[0],$new_path,$rec['manual']) ; $update_arr['manual']=$rec['manual'] ; echo '<span class=green>OK</span>' ; }
                 else      { echo '<span class=red>Не удалось сохранить файл <strong>'.$alt_url.'</strong> в '.$new_path.'</span> alt_url='.$alt_url.'<br>new_dir='.$new_dir.'<br>' ; ;}
             }
             */
           //  echo '<br>' ;
         //}

         //if (sizeof($update_arr)) update_rec_in_table('obj_site_goods',$update_arr,'pkey='.$rec['pkey']) ;

     //}
     //
     //$i=$i+100 ;
     //echo 'i='.$i.'<br>' ;
     //if ($i>200)
     //break ;
?>