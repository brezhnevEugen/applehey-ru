<?



//UPDATE  `obj_site_goods4` SET  `enabled` =  '1', `_image_name` = NULL WHERE `obj_site_goods4`.`pkey` =16 LIMIT 1
//#1442 - Can't update table 'obj_site_goods4' in stored function/trigger because it is already used by statement which invoked this stored function/trigger.
 // функция для обновления _image_name
  function create_procedure_DESCRIBE_all_tables($table_name)
  {      $func_name=$table_name.'__update_image_name'  ;
         $img_table_name='obj_site_goods_image4' ;
         $sql="CREATE PROCEDURE DESCRIBE_all_tables(out tablename varchar(255),out fname varchar(255),out ftype varchar(255))
                 begin
                   declare obj_site_goods_DS cursor for DESCRIBE obj_site_goods ;
                   open obj_site_goods_DS;
                   repeat fetch obj_site_goods_DS into table_name;
                    if not done  then
                        DESCRIBE table_name ;
                    end if;
                    until done end repeat;
                    close childs;
                 end" ;
         execSQL_update("drop procedure if exists ".$func_name) ;
         execSQL_update($sql) ;
         echo '<div class=green>Cоздана процедура <strong>'.$table_name.'__update_image_name</strong></div>' ;
  }



 function create_views()
 {
     //ALTER TABLE  `obj_site_goods4` ADD  `_image_name` VARCHAR( 255 ) NULL DEFAULT NULL
     ?><button class=button cmd="create_views">Повторить</button><?
     //  автоинсремен indx в пределах своего parent
     create_trigger('obj_site_goods_image4','BEFORE','INSERT',array('indx_indx'=>1)) ;
     // автозаполнение _image_name при добавлении/измеренении/удалениии фото
     create_procedure_update_image_name('obj_site_goods4') ;
     create_trigger('obj_site_goods_image4','AFTER','INSERT',array('update_image_name'=>1)) ;
     create_trigger('obj_site_goods_image4','AFTER','UPDATE',array('update_image_name'=>1)) ;
     create_trigger('obj_site_goods_image4','AFTER','DELETE',array('update_image_name'=>1)) ;
     // обновление parent_enabled
     create_procedure_update_parent_enabled('obj_site_goods4') ;
     // производить обновленение таблицы из своего триггера нельзя((((
     //create_trigger('obj_site_goods4','BEFORE','UPDATE',array('update_parent_enabled2'=>1)) ;




    //$sql='insert into obj_site_goods_image4(parent,obj_name,file_name) values(30,"test","image_name_30")' ;
    //execSQL_update($sql) ;
    $recs=execSQL('select * from obj_site_goods_image4 order by parent,indx') ;
    print_2x_arr($recs) ;
    $recs=execSQL('select * from obj_site_goods4 order by pkey') ;
    print_2x_arr($recs) ;
    return ;

   // вьюверы не оправдали себя - долго пересоздаются после изменения исходных таблиц


   //$func_name='obj_site_goods_update_enabled'  ;

   $sql="ALTER VIEW view_goods_sections AS
        SELECT t1.pkey,t1.parent,t1.obj_name,t1.url_name,
               (select t2.file_name from obj_site_goods_image t2 where t2.parent=t1.pkey and t2.enabled=1 and t2.file_name!='' and not t2.file_name is null order by t2.indx limit 1) as _image_name
        FROM obj_site_goods t1 WHERE t1.clss in (1,10) and t1.enabled=1";
   //echo $sql.'<br>' ;
   //execSQL_update($sql) ;
   $sql="ALTER VIEW view_goods_sections3 AS
        SELECT t1.pkey,t1.parent,t1.obj_name,t1.url_name
        FROM obj_site_goods t1 WHERE t1.clss in (1,10) and t1.enabled=1";
   //echo $sql.'<br>' ;
   //execSQL_update($sql) ;
   $sql="ALTER VIEW view_goods_sections2 AS select * from view_goods_sections v1 where (select v2.pkey from view_goods_sections v2  where v2.pkey=v1.parent)>0" ;
   //execSQL_update($sql) ;
   $sql="ALTER VIEW view_goods_items AS
          SELECT t1.pkey,t1.parent,t1.obj_name,t1.url_name,t1.price
          FROM obj_site_goods t1 WHERE t1.clss=200 and t1.enabled=1 and (select v2.pkey from view_goods_sections2 v2 where v2.pkey=t1.parent)>0";
   execSQL_update($sql) ;
   //              (select t2.file_name from obj_site_goods_image t2 where t2.parent=t1.pkey and t2.enabled=1 and t2.file_name!='' and not t2.file_name is null order by t2.indx limit 1) as _image_name


   ?><h2>Получение через вьювер SECTON без image</h2><?
   $recs=execSQL('select * from view_goods_sections3 order by obj_name',1) ;
   ?><h2>Получение через вьювер SECTON enabled=1</h2><?
   $recs=execSQL('select * from view_goods_sections order by obj_name',1) ;
   ?><h2>Получение через вьювер SECTON parent_enabled=1</h2><?
   $recs=execSQL('select * from view_goods_sections2 order by obj_name',1) ;
   ?><h2>SELECT c SUBSELECT</h2><?
   $recs2=execSQL("SELECT t1.pkey,t1.parent,t1.obj_name,t1.url_name,
                   (select t2.file_name from obj_site_goods_image t2 where t2.parent=t1.pkey and t2.enabled=1 and t2.file_name!='' and not t2.file_name is null order by t2.indx limit 1) as _image_name
                   FROM obj_site_goods t1 WHERE t1.clss in (1,10) and t1.enabled=1",1) ;
  ?><h2>Два SELECT</h2><?
  $recs3=execSQL("SELECT t1.pkey,t1.parent,t1.obj_name,t1.url_name FROM obj_site_goods t1 WHERE t1.clss in (1,10) and t1.enabled=1",1) ;
  $recs4=execSQL("select t2.pkey,t2.parent,t2.indx,t2.file_name from obj_site_goods_image t2 where t2.parent in (".implode(',',array_keys($recs3)).") and t2.enabled=1 and t2.file_name!='' and not t2.file_name is null order by t2.parent,t2.indx",1) ;

  ?><h2>get goods over view_goods_items</h2><?
  $recs5=execSQL('select * from view_goods_items',1) ;
  ?><h2>get goods over select + subselect</h2><?
  $recs6=execSQL("SELECT t1.pkey,t1.parent,t1.obj_name,t1.url_name,t1.price,
                 (select t2.file_name from obj_site_goods_image t2 where t2.parent=t1.pkey and t2.enabled=1 and t2.file_name!='' and not t2.file_name is null order by t2.indx limit 1) as _image_name
                 FROM obj_site_goods t1 WHERE t1.clss in (200) and t1.enabled=1",1) ;



   //print_2x_arr($recs5) ;
   return ;



   $this->create_procedure_update_parent_enabled() ;
   $this->create_procedure_get_list_child() ;

   $list=execSQL('SHOW PROCEDURE STATUS;',1,-1) ;
   print_2x_arr($list) ;

   execSQL('call obj_site_goods_update_parent_enabled(13543,-1);') ;
   execSQL('call obj_site_goods_get_list_child(13543,@a);') ;
   $res=execSQL_value('select @a;',2) ;
   //execSQL('call obj_site_goods_get_list_child(13545,@a);') ;
   //$res=execSQL_value('select @a;',2) ;
   //execSQL('call obj_site_goods_get_list_child(13546,@a);') ;
   //$res=execSQL_value('select @a;',2) ;
   //echo $res.'<br>' ;
   $list=execSQL('select pkey,clss,obj_name,enabled,parent_enabled from obj_site_goods where pkey in ('.$res.')') ;
   print_2x_arr($list) ;

   $_sql="
   CREATE DEFINER=CURRENT_USER PROCEDURE `GetSectionId`(IN IdDocument INTEGER,
   IN SectionName CHAR (128),
   OUT IdSection INTEGER)
   BEGIN
            /*-----------------------------------------------------------------------------------------*/
            DECLARE id_section_parent INTEGER;
            DECLARE parent_section_name VARCHAR (128) DEFAULT NULL;
            /*-----------------------------------------------------------------------------------------*/

            SET max_sp_recursion_depth = 10;
            SET IdSection = NULL;
            SELECT `id` INTO IdSection FROM `_document_sections` WHERE (`id_document` = IdDocument AND `name` = SectionName);

            IF IdSection IS NULL THEN
                SET parent_section_name = GetParentSection(SectionName);
          IF (parent_section_name IS NOT NULL) THEN
                    CALL GetSectionId(IdDocument, parent_section_name, id_section_parent);
                    SET IdSection = id_section_parent;
                END IF;

                INSERT INTO _document_sections ...;
                SET IdSection = LAST_INSERT_ID();
            END IF;

        END $$
    " ;


    $sql2="CREATE procedure ".$func_name."(in id int,out update_cnt int)
            begin
            declare this_id int;
            declare cnt_bye int;
            declare cnt_plan int;
            declare indx int default 0;
            declare done int default 0;
            declare accounts cursor for select pkey from obj_site_goods where country=cur_country and enabled=1 and card_id>0 and summa>=cur_prog_price order by rand() ;
            declare continue handler for sqlstate '02000' set done=1;

            open accounts;
            repeat
            fetch accounts into this_id;
            if not done and indx<count_account then
                select count(pkey) into cnt_bye  from ".$table_name." where parent=this_id and result=1 limit 1;
                select count(pkey) into cnt_plan from obj_site_progs_out_204 where account_id=this_id and prog_id=cur_prog_id and parent=cur_plan_id;
            if cnt_bye=0 and cnt_plan=0 then
                set indx=indx+1;
            insert into obj_site_progs_out_204 (parent,clss,c_data,enabled,account_id,prog_id,typeDevice) values (cur_plan_id,204,cur_data,1,this_id,cur_prog_id,device);
                   set cur_data=cur_data+time_int;
            end if ;
            end if;
            until done end repeat;
            close accounts;
            set update_cnt=indx ;
            end" ;


 }








?>