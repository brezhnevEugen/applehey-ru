<?php
//
 // Загрузка фото из директории на сайте
 //
 // ДОРАБОТАТЬ:
 // - добавить проверку на существование файла
 // - добавить поиск объектов для изображения только в текущем каталоге и его дочерних объектах++
 function dialog_upload_images_from_dir()
 { global $obj_info ;
   ?><h1>Загрузка фото объектов из директории на сервере</h1>
   <span class=bold>Шаг 1 из 3</span><br><br>
   <div class=intro>Данная функция позволяет произвести загрузку множества изображений за одну операцию.<br>
   Изображения должны быть предварительно закачаны на сервер в корневую директорию ftp.<br>
   Имена файлов должны соотвествовать именам (артикулам, кодам) соотвествующих объектов.<br></div>
   Укажите тип объектов для которых будет загружено фото <? _DOT($obj_info['tkey'])->show_select_to_list_clss($obj_info['_reffer'],$obj_info['clss'],array('no_show_clss'=>array(3,5))) ; ?>
   <button class=button cmd=dialog_upload_images_from_dir_step_2>Продолжить</button>
   <?
 }

 function dialog_upload_images_from_dir_step_2()
 { global $select_clss ;
   $obj_clss=_CLSS($select_clss);
   ?><h1>Загрузка фото объектов на сервер архивом</h1>
   <span class=bold>Шаг 2 из 3</span><br><br>
   <input name="select_clss" type="hidden" value="<?echo $select_clss?>">
   Выбранный тип объекта: <?echo $obj_clss->name;?><br>
   Укажите поле <?echo $obj_clss->name?>, по которому будет опеределятся принадлежность фото  <? $obj_clss->show_select_to_list_field(array('no_show_field'=>array('clss','parent','c_data','r_data'))) ;?>
   <button class=button cmd=dialog_upload_images_from_dir_step_3>Продолжить</button>
   <?
 }


 function dialog_upload_images_from_dir_step_3()
 { global $obj_info,$select_clss,$select_fname ;
   $tkey=_DOT($obj_info['tkey'])->list_clss[$select_clss] ;
   $table_name=_DOT($tkey)->table_name ;
   echo 'tkey='.$tkey.'<br>' ;
   //$root_list_dir='' ;
   $list_obj=scandir(_DIR_TO_ROOT.'/ftp/') ;
   //damp_array($list_obj) ;
   $file_list=array() ;
   if (sizeof($list_obj)) foreach($list_obj as $i=>$fname) if ($fname!='.' and $fname!='..' and !is_dir($cur_obj)) $file_list[]=$fname ;
   //damp_array($file_list);
   ?><h1>Загрузка фото объектов на сервер архивом</h1>
   <input name="select_clss" type="hidden" value="<?echo $select_clss?>">
   <input name="select_fname" type="hidden" value="<?echo $select_fname?>">
   <span class=bold>Шаг 3 из 3</span><br><br>
   Обнаружено <? echo sizeof($file_list)?> файлов<br>
   <? if(sizeof($file_list)) foreach($file_list as $fname)
   	  { $arr=explode('.',$fname) ;
   	    $rec=execSQL_van('select pkey,obj_name from '.$table_name.' where clss='.$select_clss.' and '.$select_fname.'="'.$arr[0].'"') ;
   	    if ($rec['pkey']) { echo '<br>Для <span class=bold>'.$fname.'</span> найден объект: <span class=bold>'.$rec['obj_name'].'</span> ['.$rec['pkey'].'] ' ;
					   	    $reffer=$rec['pkey'].'.'.$tkey ;
					   	    $file_info=array('tmp_name'=>_DIR_TO_ROOT.'/ftp/'.$fname,'name'=>$fname) ;
					   	    obj_upload_image($reffer,$file_info,array('debug'=>0)) ;
   	    				  }
   	    else       		  echo '<br>Для <span class="bold red">'.$fname.'</span> объект не найден ' ;
        //break ;
   	  }

   ?>
   <br>
   <button class=button cmd=dialog_upload_images_from_dir_step_3>Повторить</button>
   <button class=button cmd=dialog_upload_images_from_dir_step_4>Продолжить</button>
   <?
 }
?>