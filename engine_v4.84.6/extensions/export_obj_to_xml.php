<?
 // экспорт текущего объекта в XML
 function export_obj_to_xml()
 { if (!sizeof($_POST['_check'])) return ;
   $file_name='export_'.get_month_year(time(),'d_m_y_h_i_s')  ;
   // создаем DOM-XML объект
   include_once(_DIR_ENGINE_EXT.'/XML/export_OBJ_to_XML_doc.php') ;
   $doc=export_OBJ_to_XML_doc($_POST['_check']) ;
   header("Content-Disposition: attachment; filename=\"".$file_name.".xml\"");
   header("Content-Type: application/x-force-download; name=\"".$file_name.".xml\"");
   header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
   header('Pragma: public');
   // сливаем файл
   echo $doc->saveXML() ;
 }
?>