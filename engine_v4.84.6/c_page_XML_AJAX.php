<?php
//  отличия от c_page_XML:
//  - делает автоматический вывод ряда параметров, необходимых для корректной работы AJAX
//  - вывод отладку по используемому скрипту и расширению
include_once('c_page_XML.php') ;
class c_page_XML_AJAX extends c_page_XML
{ public $prev_HTML; //  код в модулях, выводимый в поток при подклбчении подулей (код вне функций). Выводится в зависимости от типа текущей страницы (HTML, XML)

  // переменные для замещения $this->add_element
  public $success; // modal_window,
  public $title; //
  public $no_close_button; //
  public $modal_panel_width; //
  public $modal_panel_pos_x; //
  public $modal_panel_pos_y; //
  public $modal_panel_target; // id элемента, относительного котороо будет спозиционировано модальное окно
  public $modal_panel_title; //
  public $go_url; //
  public $update_page; //
  public $after_close_func; //
  public $checked_member=0; //
  public $use_macros=1; //

  function check_member()
  { //global $member ;
    //if (!$member->id) { $this->add_element('go_url','/account/') ; return(0) ; }
    return(1) ;
  }

  function show()
   {  if ($this->check_member())
   {  if (isset($_POST['cmd']))             $this->add_element('cmd',$_POST['cmd']) ;
      if (isset($_POST['mode']))            $this->add_element('mode',$_POST['mode']) ;
      if (isset($_POST['id']))              $this->add_element('id',$_POST['id']) ;
      if (isset($_POST['reffer']))          $this->add_element('reffer',str_replace('ref_','',$_POST['reffer'])) ;
      if (isset($_POST['parent_reffer']))   $this->add_element('parent_reffer',$_POST['parent_reffer']) ;
      if (isset($_POST['value']))           $this->add_element('value',$_POST['value']) ;
      if (isset($_POST['list_id']))         $this->add_element('list_id',$_POST['list_id']) ;
      if (isset($_POST['clss']))            $this->add_element('clss',$_POST['clss']) ;
      if (isset($_POST['modal_panel_width'])) $this->add_element('modal_panel_width',$_POST['modal_panel_width']) ;

      // транслируем обратно все параметры $_POST, которые не являются массивам - часть параметров системные, часть могут потребоваться для обновления запроса
      //if (sizeof($_POST)) foreach($_POST as $name=>$value) if (!is_array($value)) $this->add_element($name,$value) ;

      $this->add_element('script_file',hide_server_dir($this->script_file)) ;
      $this->add_element('class_name',$this->class_name) ;

      // выполняем команду
      // если указано ext, будет подключен скрипт ajax из указанного расширения
      ob_start() ;
       if ($_POST['cmd'] and $_POST['cmd']!="-") $this->exec_cmd($_POST['cmd']) ;
       if ($_POST['func']) $this->exec_func($_POST['func']) ;

      $text=ob_get_clean() ;

      if ($this->success)           $this->add_element('success',$this->success) ;
      if ($this->modal_panel_title) $this->add_element('title',$this->modal_panel_title) ;
      if ($this->no_close_button)   $this->add_element('no_close_button',$this->no_close_button) ;
      if ($this->modal_panel_width) $this->add_element('modal_panel_width',$this->modal_panel_width) ;
      if ($this->modal_panel_pos_x) $this->add_element('modal_panel_pos_x',$this->modal_panel_pos_x) ;
      if ($this->modal_panel_pos_y) $this->add_element('modal_panel_pos_y',$this->modal_panel_pos_y) ;
      if ($this->modal_panel_target) $this->add_element('modal_panel_target',$this->modal_panel_target) ;

      if ($this->go_url)            $this->add_element('go_url',$this->go_url) ;
      if ($this->update_page)       $this->add_element('update_page',$this->update_page) ;
      if ($this->after_close_func)  $this->add_element('after_close_func',$this->after_close_func) ;

      $text.=$this->prev_HTML ;
      //$text=str_replace(array("\n","\r"),'',$text) ;
      $this->add_element('html',$text) ;
      $this->add_element('text',strip_tags($text)) ;
      }
      ob_start() ;
      echo $this->doc->saveXML() ;
      $text=ob_get_clean()  ;

      if ($this->use_macros and $_SESSION['__fast_edit_mode']!='enabled')
      { //if (is_array($_SESSION['global_replace_worlds']) and sizeof($_SESSION['global_replace_worlds'])) $page_HTML=str_replace(array_keys($_SESSION['global_replace_worlds']),$_SESSION['global_replace_worlds'],$text) ;
        include_once(_DIR_ENGINE_EXT.'/macros/macros.php') ;
        macros_apply($text) ;
      }
      //if (!_IS_SYSTEM_IP) echo hide_server_dir($text) ;
      //else                echo $text ;
     echo $text ;
   }

   function exec_func($func) {}

   function exec_cmd($cmd)
   {   $func_name='AJAX_'.$cmd ;
       $func_name2=$cmd ;
       $result=1 ;
       if (method_exists($this,$cmd)) { $this->add_element('exec_method',$cmd) ; $this->$cmd($this->doc,$this->xml) ; }
       else if (method_exists($this,$func_name)) { $this->add_element('exec_method',$func_name) ; $this->$func_name($this->doc,$this->xml) ; }
       else if (function_exists($func_name)) { $this->add_element('exec_func',$func_name) ; $func_name($this->doc,$this->xml) ; }
       else if (function_exists($func_name2)) { $this->add_element('exec_func',$func_name2) ; $func_name2($this->doc,$this->xml) ; }
       else { $this->add_element('html','"'.$func_name.'" or "'.$func_name2.'" not_found in '.hide_server_dir($this->script_file)) ; $result=0 ; }
       return($result) ;
   }

   function print_template()
   {  if (!$_POST['template']) return ;
      $options=array() ;
      if ($_POST['ext']) $options['ext']=$_POST['ext'] ;
      $res=print_template($_POST['template'],$options) ;
      if ($res['title']) $this->add_element('title',$res['title']) ;
      if ($res['width']) $this->add_element('modal_panel_width',$res['width']) ;
   }

   function add_function_out($func_name,$ext='')
   {   if (!$ext) $ext=$_POST['ext'];
       include_once(_DIR_EXT.'/'.$ext.'/'.$func_name.'.php') ;
       ob_start() ; $func_name() ; $func_out=ob_get_clean() ;
       $this->add_element($func_name,$func_out) ;
   }

   // асинхронная отправка почты
   function send_turn_mail()
   {  global $mail_system  ;
      $mail_system->exec_asyns_turn() ;
   }

}

?>