<?
 function db_procedure()
 {
   ?><button cmd="create_procedure_get_list_child" script="ext/db/procedure.php">Создать процедуру get_list_child</button><?
   ?><button cmd="create_procedure_get_list_parents" script="ext/db/procedure.php">Создать процедуру get_list_parents</button><?
 }


 // функция для получения списка всех потомков
 function create_procedure_get_list_child()
 {      $func_name='obj_site_goods_get_list_child'  ;
        $sql="CREATE procedure  ".$func_name."(in id int,out childs_id_str varchar(255))
                begin
                declare ch varchar(255);
                declare child_id int(11);
                declare child_childs varchar(255);
                declare done int default 0;
                declare childs cursor for select pkey from obj_site_goods where parent in(id) order by pkey ;
                declare continue handler for sqlstate '02000' set done=1;
                set childs_id_str='0' ;
                open childs;
                repeat
                fetch childs into child_id;
                if not done  then
                    call obj_site_goods_get_list_child(child_id,child_childs);
                    if (child_childs!='0') then set childs_id_str=concat(childs_id_str,',',child_childs) ; end if ;
                end if;
                until done end repeat;
                close childs;
                set childs_id_str=concat(childs_id_str,',',id) ;
                end" ;
        execSQL_update("drop procedure if exists ".$func_name) ;
        execSQL_update($sql) ;
        echo 'создана процедура <strong>'.$func_name.'</strong><br>' ;
 }

 // функция для получения списка всех потомков
 function create_procedure_get_list_parents()
 {      $func_name='obj_site_goods_get_list_parents'  ;
        $sql="CREATE procedure  ".$func_name."(in id int,out parent_id_str varchar(255))
                begin
                declare ch varchar(255);
                declare parent_id int(11);
                declare parents_ids varchar(255);
                declare done int default 0;
                declare parents cursor for select parent from obj_site_goods where pkey=id;
                declare continue handler for sqlstate '02000' set done=1;
                set parent_id_str='0' ;
                open parents;
                repeat
                fetch parents into parent_id;
                if not done  then
                    if (parent_id!=NULL) then call obj_site_goods_get_list_parents(parent_id,parents_ids); end if ;
                end if;
                until done end repeat;
                set parent_id_str=concat(parent_id,',',parents_ids) ;
                close parents;
                end" ;
        execSQL_update("drop procedure if exists ".$func_name) ;
        execSQL_update($sql) ;
        echo 'создана процедура <strong>'.$func_name.'</strong><br>' ;
 }
?>