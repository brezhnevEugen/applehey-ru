<?
 include_once($GLOBALS['dir_to_engine'].'admin/i_setup.php') ;

 function install_modules()
 { global $__functions ;
   ?><h1>Выберите опции инсталяции модулей</h1><?
   ?><input type = "checkbox" name=install_options[install_script_to_admin] value=1>Создаем скрипты для пунктов меню админки<br>
     <input type = "checkbox" name=install_options[install_copy_script] value=1>Создаем директорию модуля с входными скриптами<br>
     <input type = "checkbox" name=install_options[install_create_table] value=1>Создаем таблицы баз данных и проверяем список поддерживаемых классов<br>
     <input type = "checkbox" name=install_options[install_XML_export] value=1>Импортируем настройки сайта, списки и шаблоны писем<br>
     <input type = "hidden" name=install_options[install_from_admin] value=1>
     <br>
     <h1>Выберите инсталируемый модуль</h1><?
   if (sizeof($__functions['install'])) foreach($__functions['install'] as $func_name) {?><input type = "checkbox" name=install_select[<?echo $func_name?>] value=1><?echo $func_name?><br><?}
   ?><br><button class=button cmd=install_modules_apply script="ext/setup/install_modules.php">Выполнить инсталяцию</button><?
 }

 function install_modules_apply()
 { global $__functions,$install_options,$install_select;
   $func_name='' ;
   ?><h1>Инсталяция модулей</h1><?
    //echo 'Опции инсталятора:' ; damp_array($install_options,1,-1) ;
    //echo 'Функции инсталятора:' ; damp_array($install_select,1,-1) ;

   $_SESSION['list_db_tables']=get_db_info() ;
   if (!isset($_SESSION['list_db_tables']['obj_descr_obj_tables'])) { echo 'Создайте сначала системные таблицы' ; return ; }

   include_once(_DIR_TO_ROOT.'/ini/'._INIT_) ;

   $id_DOT=execSQL_value('select pkey from obj_descr_obj_tables where clss=103 and table_name="'.SITE_CODE.'"') ;
   if (!$id_DOT) { echo 'Не найден раздел объектных таблиц с кодом "'.SITE_CODE.'"' ; return ; }

   //damp_array($__functions['install']) ;
   $list_functions=($install_options['install_from_admin'])? array_keys($install_select):$__functions['install'] ;
   if (sizeof($list_functions)) foreach($list_functions as $func_name) $func_name($id_DOT) ;
   if ($install_options['install_from_admin']){?><div class=alert>Инсталяция окончена, перезагрузите админку</div><?}
 }
?>