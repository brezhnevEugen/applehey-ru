<?
function panel_setting_fonts()
{ if (!$_SESSION['ext_setting_fonts']['font_size'])  $_SESSION['ext_setting_fonts']['font_size']=40 ;
  if (!$_SESSION['ext_setting_fonts']['test_text'])  $_SESSION['ext_setting_fonts']['test_text']='В чащах юга жил бы цитрус? Да, но фальшивый экземпляр!' ;

  $font_size=$_SESSION['ext_setting_fonts']['font_size'] ;
  $test_text=$_SESSION['ext_setting_fonts']['test_text'] ;

  ?><div id="panel_setting_fonts">
        <link rel="stylesheet" type="text/css" href="<?echo _PATH_TO_ENGINE?>/ext/setting_fonts/style.css" charset="utf-8">
        <div id="fixed_top_panel">
            <div id="panel_title">Настройка шрифтов сайта</div>
  <?
  // ВЫВОДИМ МЕНЮ
  $top_menu_name='top_menu_setting_fonts' ;
  if (!isset($_SESSION[$top_menu_name])) $_SESSION[$top_menu_name]=new c_top_menu() ;
  // формируем набор пунктом меню
  $menu_set[]=array('name'=>'Шрифты, загруженные на сайт','code'=>'uploads');
  //$menu_set[]=array('name'=>'Выбранные шрифты','code'=>'saved_fonts');
  //$menu_set[]=array('name'=>'Шрифты google','code'=>'google');
  $menu_options=array() ;
  // выводим меню
  $menu_item=$_SESSION[$top_menu_name]->show($menu_set,$menu_options) ;  // показываем меню
  //damp_array($menu_item,1,-1) ;

  ?></div><?
  ?><div class="inner"><?


  switch($menu_item['code'])
  { case 'uploads':  panel_setting_fonts_view_uploads($font_size,$test_text) ; break ;
    case 'google':   //panel_setting_fonts_view_uploads($font_size,$test_text) ; break ;
    case 'saved_fonts':  panel_setting_fonts_view_saved_fonts($test_text) ; break ;

  }

  ?></div></div><?
}

// выводим список шрифтов, найденных в папке /fonts/
function panel_setting_fonts_view_uploads($font_size,$test_text)
{ ?><div id="panel_setting_fonts_view_uploads">
    <div id="font_setting">
    <form>
      Размер шрифта <input type="text" name="font_size" value="<?echo $font_size?>">px Текст <input type="text" name=test_text value="<?echo $test_text?>">
      <input class=v2 type="submit" value="Изменить" validate="form" cmd="setting_fonts|set_view_params">
    </form>
   </div><?

  $list=get_all_files(_DIR_TO_ROOT.'/fonts/',array('name_pattern'=>'*.css')) ;
  $tags=array('body'=>'<body>','p'=>'<p>','h1'=>'<h1>','h2'=>'<h2>','h3'=>'<h3>','h4'=>'<h4>');
  if (sizeof($list)) foreach($list as $fname)
  { $cont=file_get_contents($fname) ;
    if (preg_match_all('/@font-face\s*{\s*font-family:\s*\'(.+?)\'\s*;\s*(.+?)}/is',$cont,$match)) if (sizeof($match[1]))
    { ?><div class="item"><div class="file_name"><input class=css_src type="text" style="width:500px;font-size:11px;border:none;font-weight:bold;color:black;" value="<?echo hide_server_dir($fname)?>"><?
      ?><link rel="stylesheet" type="text/css" href="<?echo hide_server_dir($fname)?>" charset="utf-8"></div><?
      foreach($match[1] as $font_name)
        {?><div class="subitem">
                <div class=font_name><?echo $font_name?></div>
                <div class=font_demo style="font-family:'<?echo $font_name?>';font-size:<?echo $font_size?>px;"><?echo $test_text?></div>
          </div><?
        }
      ?></div><?
    }
  }
  ?></div><?
}

function panel_setting_fonts_view_saved_fonts($test_text)
{ ?><div id="panel_setting_fonts_view_saved_fonts"><?
  list($pkey,$tkey)=explode('.',$_GET['reffer']) ;
  $tags=array('body'=>'<body>','p'=>'<p>','h1'=>'<h1>','h2'=>'<h2>','h3'=>'<h3>','h4'=>'<h4>');
  foreach($tags as $id=>$tag)
  { $rec_setting=execSQL_van('select * from '.TM_SETTING.' where parent='.$pkey.' and comment="LS_font_to_tags_'.$id.'"') ;
    if ($rec_setting['pkey'])
    {   $font=unserialize($rec_setting['value']) ; //damp_array($font);
        if (!$font['font_color'])  $font['font_color']='000000' ;
        ?><div class="item">
              <link rel="stylesheet" type="text/css" href="<?echo $font['css_file']?>" charset="utf-8">
              <div class=tag_name><?echo htmlentities($tag)?></div>
              <div class=file_name><h2><?echo $font['css_file']?></h2></div>
              <div class=font_name><?echo $font['font_name']?></div>
              <div class=font_demo style="font-family:'<?echo $font['font_name']?>';font-size:<?echo $font['font_size']?>px;color:#<?echo $font['font_color']?>;"><?echo $test_text?></div>
              <div class=select_size>
                  <form>
                      Размер шрифта: <input type="text" name="font_size" tag="<?echo $id?>" value="<?echo $font['font_size']?>">
                      Цвет шрифта: <input type="text" name="font_color" tag="<?echo $id?>" value="<?echo $font['font_color']?>">
                      <input class=v2 type="submit" value="Изменить" setting_id="<?echo $rec_setting['pkey']?>" validate="form" cmd="setting_fonts|set_tag_params">
                  </form></div>
          </div><?
     }
  }
  //..$recs=execSQL('select * from '.TM_SETTING.' where parent='.$pkey,2) ;
  //damp_array($_GET) ;
  ?></div><?
}

?>