<?

// создаем объекты на основе XML описания. Родитель - obj_info
// объект помещается в текущую таблицу (global $tkey)
// или в собставнную таблицу $OBJ->tkey - при опции 'export_as_is' - используется при восстановлении объектов
// use_self_pkey = $save_pkey

 function import_OBJ_from_XML(&$dom,$options=array())
 {   $save_pkey=0 ; $parent_pkey=1 ;
     if ($options['export_as_is'])  $save_pkey=1 ;
     if ($options['use_self_pkey']) $save_pkey=1 ;
     if ($options['save_pkey'])     $save_pkey=1 ;
     if ($options['to_obj_reffer']) list($parent_pkey,$parent_tkey)=explode('.',$options['to_obj_reffer']) ;
     if ($options['parent_pkey'])   $parent_pkey=$options['parent_pkey'] ;
     if ($options['parent_tkey'])   $parent_tkey=$options['parent_tkey'] ;
     //$debug=1 ;
     $arr_pkeys=array() ;
	  // получаем массив всех объектов
	  $objs_xml=$dom->getElementsByTagName("obj");
	  $all_count=0 ;
	  if ($objs_xml->length)
	  { //echo '<strong>Импортирование объектов</strong><br>' ;
        foreach ($objs_xml as $obj_xml)
	    {
		    $obj=array() ; $obj_pkey=0 ; $obj_tkey=0 ; $obj_parent_tkey=0 ; $obj_parent=0 ;  $obj_dir_to_file='' ;  $obj_patch_to_file='' ;
	        // собираем в массив все свойства объекта
			foreach($obj_xml->childNodes as $prop_xml)
			{ //$value=trim(iconv('utf-8','windows-1251',$prop_xml->nodeValue)) ;
	          $value=trim($prop_xml->nodeValue) ;
	          switch ($prop_xml->nodeName)
	          {  case 'pkey'		: $obj_pkey=$value ;
	          						  if ($save_pkey) $obj['pkey']=$value ; break ;             // код объекта
	             case 'obj'			: break ;
	             case 'table'		: break ;
	             case 'tkey'		: $obj_tkey=$value ;	break ;             	// код таблицы объекта
	             case 'parent_tkey'	: $obj_parent_tkey=$value ; break ;             // код таблицы родителя объекта
	             case 'enabled'		: $obj['enabled']=($value=='on')? 1:0 ;break ;
	             case 'parent'		: $obj_parent=$value ; break ;             // код родителя объекта
	             case '_dir_to_file_': $obj_dir_to_file=$value ; break ;             // код родителя объекта
	             case '_patch_to_file_': $obj_patch_to_file=$value ; break ;             // код родителя объекта
	             default			: //$obj[$prop_xml->nodeName]=addslashes($value) ;
	                                  $obj[$prop_xml->nodeName]=$value ;
	          }
	         //if ($debug) echo 'обработано свойство '.$prop_xml->nodeName.'='.$value.'<br>' ;
	        }
	        if (!$obj_parent_tkey) $obj_parent_tkey=$obj_tkey ;

	        if ($options['debug_import'])  { echo '<strong>Собран объект: '.$obj['obj_name'].'</strong><br>' ; print_r($obj) ; echo '<br>' ;  }
	        //echo 'Проверяем наличие записи в таблице соотвествий для родительского объекта<br>' ;
	        //echo 'Код текущего объекта:'.$obj_pkey.'.'.$obj_tkey.'<br>' ;
	        //echo 'Код родительского объекта:'.$obj_parent.'.'.$obj_parent_tkey.'<br>' ;

	        //echo 'Текущая таблица соотвествий: tkey source - pkey source : tkey desct  - pkey desc' ; damp_array($arr_pkeys,1,-1) ; echo '<br>' ;
	        if (sizeof($arr_pkeys[$obj_parent_tkey][$obj_parent]))
                   { $use_tkey=$arr_pkeys[$obj_parent_tkey][$obj_parent]['tkey'] ; $obj['parent']=$arr_pkeys[$obj_parent_tkey][$obj_parent]['pkey'] ; }
	          else { $use_tkey=$parent_tkey ; $obj['parent']=$parent_pkey ; }

            if ($options['export_as_is']) $use_tkey=$obj_tkey ;

            //echo 'Опреден код родительского объекта:<strong>'.$obj['parent'].'.'.$use_tkey.'</strong><br>' ;
            $analog_class=array() ;
            $analog_class[]=array(26,9,11,7) ;
            $analog_class[]=array(1,10,210) ;
            $analog_class[]=array(2,200) ;

            // добавляем возможность автоматической конвертации между страница сайта-новость-статья-информация сайта
            if (!isset(_DOT($use_tkey)->list_clss_ext[$obj['clss']]))
            {  //echo '<font color=red>Объект "'.$obj['obj_name'].'" класса "'._CLSS($obj['clss'])->name.'" ('.$obj['clss'].') не поддерживается текущей таблицей</font><br>' ;

               $arr_clss2=array_keys(_DOT($use_tkey)->list_clss) ;
               $news_clss=-1 ;
               // если ненайденный класс присутствует в массиве классов для автозамены
               if (sizeof($analog_class)) foreach($analog_class as $arr_clss1)
               {   //echo 'obj_clss='.$obj['clss'].'<br>' ;
                   //print_r($arr_clss1) ; echo '<br>' ;
                   //print_r($arr_clss2) ; echo '<br>' ;
                   if (array_search($obj['clss'],$arr_clss1)!==false)
                   {  // ищем, какой из классов в этом списке поддерживается текущей таблицей
                       $arr_clss3=array_intersect($arr_clss2,$arr_clss1) ;
                      // $arr_clss1: Array ( [0] => 1 [1] => 10 [2] => 210 )
                      // $arr_clss2: Array ( [0] => 200 [1] => 210 [2] => 3 [3] => 5 )
                      // $arr_clss3: Array ( [1] => 210 )
                      //print_r($arr_clss3) ; echo '<br>' ;
                      if (sizeof($arr_clss3)) { list($t,$news_clss)=each($arr_clss3) ; // берем первый класс
                                                break ;
            }
                   }
               }
               // класс, надейн, делаем автозамену
               if ($news_clss>=0)
               { echo 'Объект <strong>"'.$obj['obj_name'].'"</strong>: автозамена класса: <strong>'.$obj['clss'].'</strong> => <strong>'.$news_clss.'</strong><br>'	;
              $obj['clss']=$news_clss ;
            }
            }

            if (!isset(_DOT($use_tkey)->list_clss_ext[$obj['clss']]))  echo '<font color=red>Объект "'.$obj['obj_name'].'" класса "'._CLSS($obj['clss'])->name.'" ('.$obj['clss'].') не поддерживается текущей таблицей</font><br>' ;
	        // определяем таблицу (относительно таблицу текущего объекта), куда будет сохраняться объект, согласно класса
            //if (isset(_DOT($use_tkey)->list_clss_ext[$obj['clss']]))
	        { if ($obj['parent']) // если родитель найден
	            {  if ($options['debug_import']) { echo 'Итоговый объект: <span class="bold black">'.$obj['obj_name'].'</span> ['.$obj['clss'].']<br>' ; print_r($obj) ; echo '<br>';}
	               $flag=1 ;
                   if ($options['check_unique_name'])
                   { $id_exist=execSQL_value('select pkey from '._DOT($use_tkey)->table_name.' where obj_name="'.$obj['obj_name'].'"') ;
                     if ($id_exist)
                     { $flag=0 ;
                       echo '<span class="bold black">'.$obj['obj_name'].'</span> ['.$obj['clss'].'] - объект уже существует<br>' ;
                       // сохраняем соответствие старого и нового pkey в массиве соответствий
	                   $arr_pkeys[$obj_tkey][$obj_pkey]=array('tkey'=>$use_tkey,'pkey'=>$id_exist);
                     }
                   }

	               if ($flag)
                   { // в зависимости от класса объекта различные варианты создания нового объекта
                     switch ($obj['clss'])
                     { case 3:  if ($options['debug_import'])  { echo 'Сохраняем объект изображение<br>' ; }
                                if ($obj_dir_to_file)
                                {  //if ($options['debug_import']) echo 'Производитм внутреннее копирование изображений<br>' ;
                                   // создаем новый объект "изображение"
                                   $obj_reffer=save_obj_in_table($use_tkey,$obj,array('debug'=>0)) ;
                                   list($new_obj_pkey,$new_obj_tkey)=explode('.',$obj_reffer) ;
                                   if ($options['debug_import']) echo '<span class=green>Объект сохранен в '.$new_obj_tkey.' под кодом '.$new_obj_pkey.'</span><br>';
	                               // сохраняем соответствие старого и нового pkey в массиве соответствий
                                   $arr_pkeys[$obj_tkey][$obj_pkey]['tkey']=$new_obj_tkey ;
	                               $arr_pkeys[$obj_tkey][$obj_pkey]['pkey']=$new_obj_pkey ;
                                   // получаем новое имя для копируемого файла изображения и сохраняем его в объекте
                                   $new_img_fname=$new_obj_pkey.'_'.$obj['file_name'] ;
                                   update_rec_in_table($new_obj_tkey,array('obj_name'=>$new_img_fname,'file_name'=>$new_img_fname),'pkey='.$new_obj_pkey) ;
                                   // формируем список клонов, включая системные - source и small
                                   $list_clones=array_keys(_DOT($new_obj_tkey)->list_clone_img()) ;
                                   $list_clones[]='small' ;
                                   $list_clones[]='source' ;
                                   // производим копирование изображения в каждой из директории клонов
                                   $source_dir=_DIR_TO_ROOT.$obj_dir_to_file ;
                                   foreach($list_clones as $clone_name)
                                   { $clone_source_dir=$source_dir.$clone_name.'/'.$obj['file_name'];
                                     $clone_desct_dir=_DOT($new_obj_tkey)->dir_to_file.$clone_name.'/'.$new_img_fname ;
                                     $res=copy($clone_source_dir,$clone_desct_dir) ;
                                     if (!$res) echo 'Не удалось сделать копию файла изображения из '.$clone_source_dir.' в '.$clone_desct_dir.'<br>' ;
                                     //if ($options['debug_import']) echo 'source_dir='.$clone_source_dir.'<br>'.'desct_dir='.$clone_desct_dir.'<br>' ;
                                   }
                                }
                                else if ($obj_patch_to_file)
                                {  if ($options['debug_import']) echo 'Производитм внешнее копирование изображений из '.$obj_patch_to_file.'<br>' ;
                                   list($new_obj_img_reffer,$result_upload)=obj_upload_image_by_href($obj['parent'].'.'.$use_tkey,$obj_patch_to_file.'source/'.$obj['file_name'],array('view_upload'=>0,'_no_create_obj'=>2));
                                   list($new_obj_pkey,$new_obj_tkey)=explode('.',$new_obj_img_reffer) ;
                                   if ($options['debug_import']) echo 'Объект сохранен в '.$new_obj_tkey.' под кодом '.$new_obj_pkey.'<br>';
	                               $arr_pkeys[$obj_tkey][$obj_pkey]['tkey']=$new_obj_tkey ;
	                               $arr_pkeys[$obj_tkey][$obj_pkey]['pkey']=$new_obj_pkey ; // сохраняем соответствие старого и нового pkey в массиве соответствий
                                   if ($result_upload) echo '<span class=green>Загружено фото с адреса '.$obj_patch_to_file.'source/'.$obj['file_name'].'</span><br>' ;
                                   else                echo '<span class=red>Не удалось загрузить фото с адреса '.$obj_patch_to_file.'source/'.$obj['file_name'].'</span><br>' ;

                                }

                                break ;
                       default: // сохраняем объект в таблице, соответствующей классу объекта
                                $obj_reffer=save_obj_in_table($use_tkey,$obj,array('debug'=>0)) ;
                                list($new_obj_pkey,$new_obj_tkey)=explode('.',$obj_reffer) ;
                                if ($options['debug_import']) echo 'Объект сохранен в '.$new_obj_tkey.' под кодом '.$new_obj_pkey.'<br>';
                                // сохраняем соответствие старого и нового pkey в массиве соответствий
	                            $arr_pkeys[$obj_tkey][$obj_pkey]=array('tkey'=>$new_obj_tkey,'pkey'=>$new_obj_pkey);
                     }
	                //damp_array($arr_pkeys);
	                $all_count++ ;
	                echo $all_count.': Импортирован объект <span class=green>'._CLSS($obj['clss'])->name.'</span> <strong>'.$obj['obj_name'].'</strong> код '.$new_obj_pkey.'.'.$new_obj_tkey.' добавлен в '.$obj['parent'].'.'.$use_tkey.'<br>' ;
                   }
	            }
	           else echo '<font color=red>Объект "'.$obj['obj_name'].'" ('.$obj['pkey'].') - не найден родителький объект.</font><br>';
	        }// else echo '<font color=red>Объект "'.$obj['obj_name'].'" класса "'._CLSS($obj['clss'])->name.'" ('.$obj['clss'].') не поддерживается текущей таблицей</font><br>' ;
	    }
	  }

     return(array($all_count,$objs_xml->length)) ;


 }



?>