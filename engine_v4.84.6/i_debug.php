<?
//*************************************************************************************************************************************/
//
// фукции вывода массивов и объектов на экран в удобной форме
//
//*************************************************************************************************************************************/

  function  print_rbr($arr)
  {
    echo nl2br(print_r($arr,true)) ;
  }

  // вывод двухмерного массива
  function print_2x_arr(&$arr=array(),$options=array())
	{ if (!is_array($options)) { $title=$options ; $options=array() ; }
      if (!isset($options['no_show_index'])) $options['no_show_index']=0 ;
      if (sizeof($arr))
      { ?><table class="left debug tablesorter"><thead><?
        // собираем все ключи всех таблиц
        $sum_titles=array() ;
        foreach ($arr as $finfo) if (sizeof($finfo)) $sum_titles=array_merge($sum_titles,array_keys($finfo)) ; else $sum_titles=array() ;
        // удаляем повторяющеся поля
        $ftitles=array_unique($sum_titles) ;

        if ($title) echo '<tr class=clss_header><td colspan='.(sizeof($ftitles)+1).'>'.$title.'</td><tr>' ;
        echo '<tr class=td_header>' ;
        if (!$options['no_show_index']) echo '<th>&nbsp;</th>' ;
        foreach ($ftitles as $title) echo '<th>'.$title.'</th>' ; ?></tr></thead><tbody><?
        foreach ($arr as $fname=>$finfo)
         { ?><tr>
              <?if (!$options['no_show_index']){?><td><? echo $fname?></td><?}
           	  foreach ($ftitles as $title)
              { $value=$finfo[$title] ;
                //echo '$options[$title]='.$options[$title].'<br>' ;
                if ($title==='c_data' or $title==='r_data') $value=date('d.m.Y H:i:s',$value) ;
                if ($options[$title]==='timedata')         $value=date('d.m.Y H:i:s',$value) ;
                ?><td><? if (is_array($finfo[$title])) print_1x_arr($finfo[$title]) ; else if (!is_object($finfo[$title])) echo $value ; else { echo 'Объект класса ' ; }?></td><? }
          ?></tr><?
         }
        ?></tbody></table><?
      } else echo 'array()<br/>';
	}

 function print_1x_arr($arr=array())
	{ if (sizeof($arr))
      { /*?><table class="left debug"><tr class=td_header><td>key</td><td>value</td></tr><?*/
        ?><table class="left debug"><?

        //trace() ;
        foreach ($arr as $fname=>$finfo)
         { ?><tr><td><? echo $fname?></td><td><? if (is_array($finfo)) print_1x_arr($finfo) ;
                 else if (is_object($finfo)) { if (get_class($finfo)=='stdClass') damp_array($finfo,1,-1) ;
                 else echo 'Объект "'.get_class($finfo).'"'  ;/*else print_r($finfo) ;*/}
                 else
                 {
                     if (strlen($finfo)<=128) echo $finfo ; else {?><div class="short_view"><?echo $finfo?></div><?}
                 }
                      /*print_1x_arr($finfo)*/?></td></tr><?}
        ?></table><?
      } else echo 'array()<br/>';
	}

 function get_damp_array($arr,$mode=1,$show_trace=-1)
 { ob_start() ;
   damp_array($arr,$mode,$show_trace) ;
   $text=ob_get_clean() ;
   return($text) ;
 }

 // $mode = 1: выводить массивы через 1x, =2: выводить массивы через 2x
 // damp_array($arr,1,-1) - вывести массив для любого IP без трассировки
 function damp_array($arr,$mode=1,$show_trace=1)
 { if (!_IS_SYSTEM_IP and _ENGINE_MODE!='admin' and $show_trace!=-1) return ;
   //$show_trace=1 ;
   if (sizeof($arr))
   { ?><style>
         table.debug {table-layout:fixed;border-collapse:collapse;empty-cells:show;margin:10px 0;background:white;}
         table.debug td{border:1px solid gray;padding:5px;}
         table.debug td strong{color:black;}
       </style>
       <? if ($show_trace>0) print_info_sender_function($show_trace) ;
       ?>
       <table class="left debug"><?
     foreach ($arr as $key=>$value) if (!(is_object($value) and $key=='system'))
      { ?><tr><?
        if (!is_array($value) and !is_object($value)) { ?><td><? echo $key ; ?></td><td><?
              if (strlen($value)<=128) echo $value ; else {?><div class="short_view"><?echo $value?></div><?}
              //echo '<br>'.strlen($value) ;
            ?></td><? ; } // если значение не массив
  		else // значение - массив
  		{ list($key1,$value1)=each($value) ; // получаем первый элемент массива
          reset($value) ;
          if (!is_array($value1)) { ?><td><strong><? echo $key ; ?></strong></td><td><? print_1x_arr($value) ; ?></td><? }  // если массив состоит из строк - выводим его
          else
          { list($key2,$value2)=each($value1) ; // получаем первый элемент дочернего массива
          	reset($value1) ;
           	if (!is_array($value2)) { ?><td><strong><? echo $key ; ?></strong></td><td><? if ($mode==1) print_1x_arr($value) ; else print_2x_arr($value) ;?></td><? }
            else
            { ?><td><strong><? echo $key ; ?></strong></td><td><? damp_array($value,$mode,-1) ; ?></td><? }
  		  }
		}
		?></tr><?
	  }
      else
      {
        ?><tr><td><? echo $key ; ?></td><td><? if (is_object($value)) echo 'object "<strong>'.get_class($value).'"</strong>' ; ?></td></tr><? ;

      }
 	 ?></table><?
   }
 }

 function damp_string($str)
 { $arr2=array() ;
   $arr=str_split($str) ;
   if (sizeof($arr)) foreach($arr as $i=>$ch) $arr2[$i]=array($ch,ord($ch)) ;
   print_2x_arr($arr2) ;
 }

 function print_info_sender_function($trace_level=3)
 { global $debug_trace_level ;
   if (!$debug_trace_level) $debug_trace_level=$trace_level ;
   $arr_trace=debug_backtrace() ;
   $all_levels=sizeof($arr_trace) ;
   if ($debug_trace_level=='all') $from_level=$all_levels ;
   else    						  $from_level=($debug_trace_level>$all_levels)? $all_levels:$debug_trace_level+1 ;
   if ($from_level) for ($i=$from_level; $i>0; $i=$i-1) //if (strpos($arr_trace[$i-1]['file'],'i_db.php')===false)
   //echo '<div class=small>function <strong>'.$arr_trace[$i]['function'].'()</strong> '.str_replace(_DIR_TO_ROOT.'/','/',$arr_trace[$i-1]['file']).':'.$arr_trace[$i-1]['line'].'</div>' ;
   echo '<div class=small>function <strong>'.$arr_trace[$i]['function'].'()</strong> '.str_replace(_DIR_TO_ROOT.'/','/',$arr_trace[$i]['file']).':'.$arr_trace[$i]['line'].'</div>' ;
   echo '-------------<br>' ;

   //echo '<div class=small>function <strong>'.$arr_trace[3]['function'].'()</strong> '.str_replace(_DIR_TO_ROOT.'/','/',$arr_trace[2]['file']).':'.$arr_trace[2]['line'].'</div>' ;
   //echo '<div class=small>function <strong>'.$arr_trace[2]['function'].'()</strong> '.str_replace(_DIR_TO_ROOT.'/','/',$arr_trace[1]['file']).':'.$arr_trace[1]['line'].'</div>' ;

 }

 function print_SQL_debug_begin($sql)
 {  global $debug_db,$__debug_sql_cnt ;
    ?><div style="border:1px solid black;margin:5px;padding:5px;BACKGROUND-COLOR: #F9F9F7;color:black;"><?
    if ($debug_db==2) print_info_sender_function() ;
    ?><span class='red bold'><?echo $__debug_sql_cnt+1 ;?>. </span><?
	echo $sql.'<br>' ;
 }

 function print_SQL_debug_end() { ?></div><?}

 function print_error($text)
 { if (!_IS_SYSTEM_IP) return ;
   print_info_sender_function() ;
   //$arr_trace=debug_backtrace() ;
   //echo '<div class=small>Место: '.str_replace(_DIR_TO_ROOT.'/','/',$arr_trace[0]['file']).':'.$arr_trace[0]['line'].' - function <strong>'.$arr_trace[1]['function'].'()</strong></div>' ;
   echo '<div class=alert>'.$text.'</div>' ;
 }

 function print_array(&$arr=array(),$title='Заголовок таблицы')
	{ if (sizeof($arr))
      { ?><div>
        <style>
		.pa_table{BORDER-left: #F3ECDD 1px solid;}
		td{font-family: tahoma,verdana,arial;font-size: 11px;color: Black;}
		.pa_header{FONT-SIZE: 12px;COLOR: #23598c;background: #FFFDE8;font-weight: bold;FONT-FAMILY: Tahoma ;vertical-align: middle;
						 PADDING: 5px; BORDER-TOP: #DDE1E4 1px solid;BORDER-BOTTOM: #DDE1E4 1px solid;BORDER-LEFT: #DDE1E4 0px solid; BORDER-RIGHT: #DDE1E4 1px solid;
						}
        td.pa_data{	BORDER-TOP: white 0px solid;BORDER-BOTTOM: #F3ECDD 1px solid;   BORDER-LEFT: white 1px solid;	BORDER-RIGHT: #F3ECDD 1px solid;	text-align: center;	color: black ;	 background: #f9f9f7 ;	PADDING: 2px;height: 22px;}
        td.pa_l_data{BORDER-TOP: white 0px solid;BORDER-BOTTOM: #F3ECDD 1px solid;   BORDER-LEFT: white 1px solid;	BORDER-RIGHT: #F3ECDD 1px solid;	text-align: left;	color: black ;	 background: #f9f9f7 ;	PADDING: 2px;height: 22px;}
        </style>
        <table class='pa_table' style='valign:top' cellspacing=0 cellpadding=0><?
        $inf=each($arr) ;
        if (is_array($inf[1])) $ftitles=array_keys($inf[1]) ;
        reset($arr) ;
        echo '<tr><td class=pa_header colspan=100>'.$title.'</td><tr>' ;
        if (is_array($inf[1])) { echo '<tr><td class=pa_data>&nbsp;</td>' ; foreach ($ftitles as $title) echo '<td class=pa_data>'.$title.'</td>' ; echo '</tr>' ; }
        foreach ($arr as $fname=>$finfo)
         { echo '<tr><td class=pa_l_data>'.$fname.'</td>' ;
           if (is_array($finfo) and sizeof($ftitles)) foreach ($ftitles as $title) if (!is_array($finfo[$title])) echo '<td class=pa_l_data>'.$finfo[$title].'</td>' ; else { echo '<td class=pa_l_data>' ; print_r($finfo[$title]) ; echo '</td>' ; }
            else echo '<td class=pa_l_data>'.$finfo.'</td>' ;
           echo '</tr>' ;
         }
        ?></table></div><?
      }
      echo '<br>' ;
	}


  function echo_var($var)
	{ if (!_IS_SYSTEM_IP) return ;
      echo $var ;
	}


//*************************************************************************************************************************************/
//
// фукции отладки
//
//*************************************************************************************************************************************/

 function trace($mode=0,$level=0)
	  { //ob_start();
	    if ($mode!=2) echo '<h1>Трассировка вызова функций</h1>' ;
	    $arr=debug_backtrace() ;
	    unset($arr[0]) ;   $result='' ;
        if ($level) $arr=array_slice($arr,0,$level) ;
        switch($mode)
        { case 0: print_2x_arr($arr) ;
                  break ;

          case 1: print_2x_arr($arr) ;
                  echo '<h1>Список подключенных файлов</h1>' ;
	    			print_1x_arr(get_included_files()) ;
                  break ;

          case 2: $result=$arr ;
                  break ;

        }

	    //damp_array(debug_backtrace()) ;
	    if ($mode==1){

	    		  }
	    //$out = ob_get_clean();
	    //return($out) ;


	    global $debug_info ;
	    if (sizeof($debug_info))
        {   echo '<strong>Дамп отладки:</strong><br>' ;
            damp_array($debug_info) ;

	  }
        return($result) ;
	  }

 // показ информации по ошибке
 // $text - текст сообщения
 // сколько уровней показывать
 // какой уровень показывать (будет показан тока один уровень)
 function damp_error($text='',$show_levels=0,$show_count=0)
 	  { echo $text.'<br>' ;
 	    $arr=debug_backtrace() ; //unset($arr[0]) ;
 	    echo '<strong>Дамп программы:</strong><br>' ;
 	    if (!$show_levels) $show_levels=sizeof($arr) ;
 	    if (sizeof($arr)) foreach($arr as $i=>$rec) if ($i and $i<=$show_levels and ($show_count==0 or $show_count==$i))
        if ($rec['function']!='SQL_error_message' and $rec['function']!='trigger_error' and $rec['function']!='userErrorHandler' and $rec['function']!='get_trace_info')
	    {  echo '<hr><strong>Файл:'.$rec['file'].':'.$rec['line'].'</strong>&nbsp;&nbsp;&nbsp;' ;
	       if ($rec['class']) echo '<span style="color:green;">'.$rec['class'].'</span>' ;
	       if ($rec['type']) echo $rec['type'] ;
	       echo '<span style="color:green;font-weight:bold;">'.$rec['function'].'</span><br>' ;
	       //if (sizeof($rec['args']))
	       damp_array($rec['args'],1,-1); '<br><br>' ;
	       //echo nl2br(print_r($rec['args'])) ;
	    }
	    echo '<strong>Дамп отладки:</strong><br>' ;
	    global $debug_info ;
	    if (sizeof($debug_info)) damp_array($debug_info,1,-1) ;

 	  }


  // функция для фиксирования временных интервалов выполения кода
  // $time_before=fixed_time() ; - фиксируем время перед вызов функции в переменной $time_before
  // fixed_time($time_before,'запрос к БД занял - ','') ; - выводим время, прошедшее от момента $time_before,
  //   если $title>'' выводим сообщение "запрос к БД занял - ",
  //   строку после сообщения не переводим
  //   если $time_before=0, возвращаем текущее время
  //   если $time_before>0, возвращаем разницу времен

  function fixed_time($querytime_begin=0,$title='',$br='<br>')
  {  list($usec, $sec) = explode(' ', microtime());
     $querytime_now = ((float)$usec + (float)$sec);
     $querytime_delta = $querytime_now - $querytime_begin;
     if ($title) echo sprintf('<span style="color:green;font-weight:bold">'.$title.' %01.4f сек</span>'.$br,$querytime_delta) ;
     if (!$querytime_begin) return($querytime_now) ; else return($querytime_delta) ;
  }


 function get_formatted_microtime() {
   list($usec, $sec) = explode(' ', microtime());
   return $usec + $sec;
}

 function set_time_point($title)
 { global $__times,$__memory ;
   $__times[$title]=get_formatted_microtime() ;
   if (function_exists('memory_get_usage'))  $__memory[$title]=round(memory_get_usage()/1024) ;
 }

 function show_time_line($mode=1,$text='',$show_from='')
 { global $__times,$__memory,$debug ;
   if (!$text) $text='Время генерации страницы:' ;
   $alt_time=0 ;  $sum_proc=0 ;
   $time_int=100/($__times['end']-$__times['start']) ;
   if ($debug or $mode>=2)
   {
	   ?><br><br>
	     <table cellspacing=2 cellpadding=2 align=left style="border:1px solid green;font-family:arial;font-size:12px;text-align:left;background:white;"><?
	   ?><tr><td> Операция </td><td> Время выполнения </td><td> % </td><td> Суммарное время </td><td> Память </td></tr><?
	   $ff=0 ;
	   if (sizeof($__times)) foreach ($__times as $info=>$time)
	     { $delta=($alt_time)? round($time-$alt_time,6):0 ;
           $proc=round($delta*$time_int,2) ;
           $sum_proc+=$proc ;
	       if (!$alt_time) $start=$time ;
	       $ff=(!$show_from or $ff or $info==$show_from)? 1:0;
           $class=($proc>30)? ' red':'' ;
	       if ($ff)
           { ?><tr><td class="left<?echo $class?>"><?echo $info?></td>
                   <td>+<? echo $delta?> с. </td>
                   <td class="right<?echo $class?>"><?echo printf('%01.1f',$proc)?> % </td>
                   <td><?echo round($time-$start,6)?> с. </td>
                   <td> <?echo $__memory[$info]?> </td>
               </tr>
             <?
           }
	       $alt_time=$time ;
	     }
	   ?></table><div class=clear></div><br><br><?
	   ?><script type=text/javascript>window.document.title=window.document.title+' <?echo round($time-$start,6)?> с.' ;</script><?

   } else {
    		$x1=each($__times) ;
    		echo '<div style="font-family:arial;font-size:12px;text-align:left;">'.$text.' '.round($__times['end']-$__times['start'],6).' c.</div>'  ;
    	  }
 }

 function alert_SQL_inject()
 { $mess=get_script_info() ;
   _send_mail(_EMAIL_ENGINE_ALERT,'Попытка взлома сайта путем SQL иньекции '._BASE_DOMAIN.' c IP '.IP,$mess,array('debug'=>0,'no_reg_events'=>1,'Content-type'=>'text/html')) ;
   if (_IS_SYSTEM_IP) echo '<div class=alert>В условиях запроса присутствуют элементы SQL-иньекции: слова select,from,where</div><br>' ;
 }

 function alert_HOST_replace()
 { //$mess=get_script_info() ;
   //_send_mail(_EMAIL_ENGINE_ALERT,'Попытка замены HTTP_HOST на '._BASE_DOMAIN.' c IP '.IP,$mess,array('debug'=>0,'no_reg_events'=>1,'Content-type'=>'text/html')) ;
   //if (_IS_SYSTEM_IP) echo '<div class=alert>Обнаружена попытка подмены HTTP_HOST</div><br>' ;
 }


function userErrorHandler($errno,$errmsg,$filename,$linenum,$vars='')
{ global $send_mail_by_error ;
  if (!$send_mail_by_error) return ;
  if ($errno==8) return ;
  $errmsg=hide_server_dir($errmsg) ;
  $filename=hide_server_dir($filename) ;
  if (strpos($errmsg,'is deprecated')!==false) return ;
  if (strpos($errmsg,'should be compatible')!==false) return ;
  echo $filename.'('.$linenum.'):'.$errmsg."\n\n"  ;
  $result=false ;
  if (!$GLOBALS['debug_db']) $result=register_error_to_db($errno,$errmsg,$filename,$linenum) ;
  if (!$GLOBALS['debug_db'] and !$result)
  { if (strpos($errmsg,'http://www.12-24.ru/setup/')!==false and strpos($errmsg,'404 Not Found')!==false) $err='<strong>Не найден установочный файл:</strong><br><br>'.$errmsg ;
    else  { $err=$errmsg.'<br><br>'.get_script_info($filename,$linenum,$errno) ;
          $err.=get_trace_info() ;
        }
    _send_mail(_EMAIL_ENGINE_ALERT,'Error on '._BASE_DOMAIN.' from IP '.IP,$err,array('no_reg_events'=>0,'_Content-type'=>'text/plain')) ;
  }
  return(1) ;
}

function register_error_to_db($errno,$errmsg,$filename,$linenum)
{  global $SF_REQUEST_URI ;
   if (!isset($_SESSION['pkey_by_table'][TM_LOG_ERRORS])) return(0) ;
   $errortype = array(1    => "Error",2    => "Warning",4    => "Parsing Error",8    => "Notice",16   => "Core Error",32   => "Core Warning",64   => "Compile Error",128  => "Compile Warning",256  => "User Error",512  => "User Warning",1024 => "User Notice");
   $rec=array('indx'=>1,
              'clss'=>51,
              'IP'=>IP,
              'typ'=>$errortype[$errno],
              'obj_name'=>$errmsg,
              'url'=>$_SERVER['HTTP_HOST'].$SF_REQUEST_URI,
              'script'=>hide_server_dir($_SERVER['SCRIPT_FILENAME'].(($_SERVER['QUERY_STRING'])? '?'.$_SERVER['QUERY_STRING']:'')),
              'file'=>hide_server_dir($filename),
              'line'=>$linenum,
              'server'=>print_r($_SERVER,true),
              'trace'=>get_trace_info()
   ) ;
   $id=adding_rec_to_table(TM_LOG_ERRORS,$rec) ;
   return($id) ;
}

function get_trace_info()
{
   ob_start() ;
   damp_error() ;
   $text=ob_get_clean() ;
   return($text) ;

}

function get_script_info($filename='',$linenum=0,$errno=0)
 { global $SF_REQUEST_URI ;
   $errortype = array(1    => "Error",2    => "Warning",4    => "Parsing Error",8    => "Notice",16   => "Core Error",32   => "Core Warning",64   => "Compile Error",128  => "Compile Warning",256  => "User Error",512  => "User Warning",1024 => "User Notice");
   $dt=date("Y-m-d H:i:s (T)");
   $err='<table>' ;
   $err.='<tr><td><strong>URL:</strong></td><td><a href="http://'.$_SERVER['HTTP_HOST'].$SF_REQUEST_URI.'">http://'.$_SERVER['HTTP_HOST'].$SF_REQUEST_URI.'</a></td></tr>' ;
   $err.='<tr><td><strong>скрипт:</strong></td><td>'.$_SERVER['SCRIPT_FILENAME'].(($_SERVER['QUERY_STRING'])? '?'.$_SERVER['QUERY_STRING']:'').'</td></tr>' ;
   if ($filename) $err.='<tr><td><strong>файл:</strong></td><td>'.$filename.'":строка ("'.$linenum.'")</td></tr>' ;
   $err.='<tr><td><strong>время:</strong></td><td>'.$dt.'</td></tr>' ;
   if ($errno) $err.='<tr><td><strong>тип ошибки:</strong></td><td>'.$errortype[$errno].'</td></tr>' ;
   foreach($_SERVER as $name=>$value) $err.='<tr><td><strong>'.$name.'</strong></td><td>'.$value.'</td></tr>' ;
   $err.='</table>' ;
   return($err) ;
 }
?>