<?php
  include_once(_DIR_TO_ENGINE.'/admin/i_img_working.php') ;


//<button class=button cmd=export_obj_to_xml mode=give_file params="12,33">Далее</button>

 // ---------------------------------------------------------------------------------------------------------------------------------
 // функции генерации элементов списоков и свойств
 // ---------------------------------------------------------------------------------------------------------------------------------

 function generate_datepicker($el_name,$value="")
 { $id='datepicker_'.rand(1,10000) ;
   ?><!--<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.0/themes/base/jquery-ui.css" />
     <script src="http://code.jquery.com/ui/1.10.0/jquery-ui.js"></script>
     <link rel="stylesheet" href="/resources/demos/style.css" />-->
     <input name="<?echo $el_name?>" type="text" class=text id="<?echo $id?>" value="<?echo $value?>">
     <?/*if (!$GLOBALS['is_js_declare']['datepicker'])
        { $GLOBALS['is_js_declare']['datepicker']=1 ;
           ?><script type="text/javascript" src="<?echo _PATH_TO_ADMIN_ADDONS?>/jquery-ui-1.9.2/jquery.ui.datepicker.min.js"></script>
             <script type="text/javascript" src="<?echo _PATH_TO_ADMIN_ADDONS?>/jquery-ui-1.9.2/jquery.ui.datepicker-ru.js"></script>
             <script type="text/javascript" src="<?echo _PATH_TO_ADMIN_ADDONS?>/jquery-ui-1.9.2/jquery-ui-timepicker-addon.js"></script>
             <script type="text/javascript" src="<?echo _PATH_TO_ADMIN_ADDONS?>/jquery-ui-1.9.2/jquery.ui.widget.min.js"></script>
             <script type="text/javascript" src="<?echo _PATH_TO_ADMIN_ADDONS?>/jquery-ui-1.9.2/jquery.ui.tabs.min.js"></script>
           <?
        }*/?>
     <script type="text/javascript">include_datapicker_script() ; $j(function(){$j("#<?echo $id?>" ).datepicker();});</script>
   <?
 }

 function generate_datetimepicker($el_name,$options=array())
 { $id='datepicker_'.rand(1,10000) ;
   /*?><link rel="stylesheet" href="<?echo _PATH_TO_ENGINE?>/AddOns/jquery-ui-timepicker-addon.css"/>
     <script src="<?echo _PATH_TO_ENGINE?>/AddOns/jquery-ui-timepicker-addon.js"></script>*/
   ?><input name="<?echo $el_name?>" type="text" class=text id="<?echo $id?>" <?if ($options['data-required']) echo 'data-required'?> value="<?echo $options['value']?>"><script type="text/javascript">$j(function(){$j("#<?echo $id?>" ).datetimepicker(<?echo $options['js_options']?>);});</script><?
 }

 function generate_select_object_from_array($el_name,$arr,$fname='',$options=array())
 { ?><select name="<?echo $el_name?>"><option></option><?
    if (sizeof($arr)) foreach($arr as $id=>$rec)
    {  if (!is_array($rec)) {?><option value="<?echo $id?>"><?echo $rec?></option><?}
       else                 {?><option value="<?echo $id?>"><?echo $rec[$fname]?></option><?}
    }
   ?></select><?
 }


 // новая версия
 function generate_obj_text($reffer,$options=array())
 { if (!$reffer) return ;
   $obj_rec=select_db_ref_info($reffer,array('no_error_table_not_found'=>1,'debug'=>0)) ;
   return(generate_ref_obj_viewer($obj_rec,$options['only_name'],$options['show_img'])) ;
 }

 function generate_tr_td_field($rec,$fname,$title,$options=array())
 { list($text,$td_attr)=generate_field_text($rec,$fname,$options) ;
   ?><tr reffer="<?echo $rec['_reffer']?>"><td><?echo $title?></td><td <?echo $td_attr?>><?echo $text?></td></tr><?
 }

 // генерация элемента ввода
 function generate_element2($rec,$fname,$type,$options=array())
 { $tkey=$rec['tkey'] ;
   $pkey=$rec['pkey'] ;
   $clss=$rec['clss'] ;
   $size=$options['size'] ;
   return(generate_element($type,$size,$tkey,$clss,$pkey,$fname,$rec[$fname])) ;
 }

 // генерация элемента ввода
 function generate_element($type,$size,$tkey,$clss,$pkey,$fname,$value,$options=array())
 { $element_name="obj[".$tkey."][".$clss."][".$pkey."][".$fname."]" ;
   $text='' ;
   switch ($type)
    { case 'checkbox':  if ($value) $check='checked' ;
    					$text ='<input  name="'.$element_name.'" type="hidden" value=0 />' ;
    					$text.='<input  name="'.$element_name.'" type=checkbox value=1 '.$check.' />' ;
    					break ;
      case 'text':		$text='<input  name="'.$element_name.'" class="text '.$options['class'].'" type=text value="'.$value.'" size='.$size.' />' ;
       					break;
      case 'select':    $text='<select size="1" name="'.$element_name.'">' ;
       					break;
      case 'hidden':	$text ='<input  name="'.$element_name.'" type="hidden" value="'.$value.'" />' ;
    					break ;
   }
   return($text) ;
 }

 // генерация чекбокса для отметки элемента
 function generate_check($tkey,$clss,$pkey,$value='ON',$status='',$_tkey=0,$_clss=0,$_pkey=0,$_value='ON')
 {
   $name='check' ;
   $text='<input name="_'.$name.'['.$tkey.']['.$clss.']['.$pkey.']" type="checkbox"  value="'.$value.'" '.$status.'/>' ;

   $name='check_source' ;
   if ($_tkey) $text.='<input name="_'.$name.'['.$_tkey.']['.$_clss.']['.$_pkey.']" type="checkbox" value="'.$_value.'" class=hidden />' ;

   return($text);
 }


 // 0=>'&nbsp;',1=>'Input big',6=>'Input small',2=>'Memo',3=>'Checkbox',4=>'Drop-down Menu',5=>'Radio button'

 // генерация элементра редактирования для поля
 // $tkey  - код таблицы
 // $rec   - запись (важны поля $tkey,$clss,$pkey,$enabled,$parent,$obj_name,)
 // $fname - название поля
 // options - массив набора параметров
 //  - read_only 	0/1
 //  - edit_element =1: поле ввода
 // 				=2: окошко
 // 				=3: чекбокс
 // 				=10:
 //
 //  - size			- размер поля ввода
 // memo_rows,memo_colls - размеры для мемo

  function generate_field_text($rec,$fname,$options=array())
   {  // системные массивы
     if (!is_array($options)) {echo '$options='.$options.'<br>' ; $options=array() ; }
     $gen_elem='' ;
     $tkey=$rec['tkey'] ;
     $pkey=$rec['pkey'] ;
     $clss=$rec['clss'] ;
     $obj_clss=_CLSS($clss);
     //trace() ;
     //damp_array($options) ;




     if ($options['on_view_before']) if (function_exists($options['on_view_before'])) $options['on_view_before']($rec,$tkey,$pkey,$options,$fname) ;

     //$fname=$fname.'' ;
     //damp_array($options) ;

     if (!$fname) $fname='' ;

     $options['no_htmlspecialchars']=1 ; // введено, т.к. переть текст отображается как есть

     // оставляем поле fname для использования с моделью классов и таблиц - т.к. там прописано все для исходного поля
     // и используем далее  $cur_fname для обращения к полю с учетом текущего языка
     // 8.10.09 заремировано, т.к. в $cur_fname уже передается имя поля с учетом текущего языка
     //$cur_fname=($options['_use_lang_suff'])? $fname.$options['use_lang_suff']:$fname ;
     $cur_fname=$fname ;

     // получаем ОТОБРАЖАЕМОЕ значение поля на основе РЕАЛЬНОГО значения
     // в развиии необходимо переместить внуть get_rec_field все нижеследующие отдачи значений поля
     $value=$obj_clss->get_rec_field_to_view($rec,$cur_fname,$options) ;

     // функция htmlspecialchars применяется для того, чтобы корректно отображать текст с тегами из БД. Если
     // не применять данную функцию, то при выводе текста из полей таблиц в поля редактирования происходит
     // искажение отображения элементов редактирования. Простое экранирование ковычек не помогает.
     //if (!$options['no_htmlspecialchars']) $value=htmlspecialchars($value,ENT_QUOTES) ;

     $class_element=($options['class'])? $options['class']:'' ;

     //echo 'tkey='.$tkey.'<br>';
     //echo 'fname='.$fname.'<br>'; print_r($options) ; echo '<br>' ;
     //damp_array(_DOT($tkey)->field_info).'<br>' ;

     $table_tkey=(isset(_DOT($tkey)->field_info[$fname]))? $tkey:_DOT($tkey)->list_clss_ext[$clss]['ext'] ;
     $element_name="obj[".$table_tkey."][".$clss."][".$pkey."][".$cur_fname."]" ; //echo 'element_name='.$element_name.'<br>' ;
     $element_id="obj_".$table_tkey."_".$clss."_".$pkey."_".$cur_fname ;

     //85.175.46.122
     //85.175.46.130
     if ($options['before_view'] and function_exists($options['before_view'])) $options['before_view']($rec,$tkey,$pkey,$options,$cur_fname) ;

     // раскрываем опции
     $edit_element=$options['edit_element'] ; // echo 'edit_element='.$edit_element.'<br>' ;

     $edit_element_size=$options['size'] ;
     $read_only=$options['read_only'] ; //echo 'read_only='.$read_only.'<br>' ;

     //echo 'fname='.$fname.'<br>'; damp_array($options) ; damp_array($rec) ;

     // поле '__'.$fname будет присутствовать, если все записи прошли предварительную групповую обработку set_reffer_values
     if ($obj_clss->fields[$fname]=='any_object' and !isset($rec['__'.$fname])) $edit_element='any_object' ;
     if ($obj_clss->fields[$fname]=='reffer' and !isset($rec['__'.$fname])) $edit_element='any_object' ;
     if ($fname=='parent' and $obj_clss->fields[$fname]!='int(11)') $edit_element='-' ;    // реализация $_SESSION['descr_clss'][9]['fields']['parent']=array('type'=>'indx_select','array'=>'ARR_news_themes') ;

     // в первую очередь проверяем есть ли заданное функция вывода поля
     if ($options['use_func'])        $options['on_view']=$options['use_func'];
     $td_attr_arr=array() ;

     if (method_exists($obj_clss,'VIEW_FIELD_'.$cur_fname))
     { $method_name='VIEW_FIELD_'.$cur_fname ;
       $res=$obj_clss->$method_name($rec,$options) ;
       if (is_array($res)) list($text,$gen_elem,$dop_td_attr)=array_values($res) ; else $text=$res ;
     }
     elseif ($options['on_view'])
     { // ищем функцию из on_view или в методе класа
       if (method_exists($obj_clss,$options['on_view']))
        { $res=$obj_clss->$options['on_view']($rec,$tkey,$pkey,$options,$cur_fname) ;
          if (is_array($res)) list($text,$gen_elem,$dop_td_attr)=array_values($res) ; else $text=$res ;
        }
       // или в объявленных функциях
       elseif (function_exists($options['on_view']))
        { $res=$options['on_view']($rec,$tkey,$pkey,$options,$cur_fname) ;
         if (is_array($res)) list($text,$gen_elem,$dop_td_attr)=array_values($res) ; else $text=$res ;
       }

     }
     // $options['view'] отличается от $options['on_view'] тем, что первой передается меньшее число параметров - только $rec и $options
     // сделано, так как обычно через $options['on_view'] вызывается функция для заведомо известного поля
     elseif ($options['view'])
     { //if ($fname=='type') damp_array($options) ;
       // ищем функцию из on_view или в методе класа
       if (method_exists($obj_clss,$options['view']))
        { $res=$obj_clss->$options['view']($rec,$options) ;
          if (is_array($res)) list($text,$gen_elem,$dop_td_attr)=array_values($res) ; else $text=$res ;
        }
       // или в объявленных функциях
       else
       { // если в имени команды есть ext - подключаем это расширение
         //list($result,$cmd_exec)=

         include_script_cmd($options['view'],$options['script'],array('cur_page'=>_CUR_PAGE())) ;
         //check_and_include_ext($options['view']) ;

         if (function_exists($options['view']))
         { $res=$options['view']($rec,$options) ;
           if (is_array($res)) list($text,$gen_elem,$dop_td_attr)=array_values($res) ; else $text=$res ;
         }
       }
       // или в расширениях


     }
     elseif ($options['cmd'] and !isset($rec[$fname]) and strpos($options['cmd'],'cur_obj')===false)
     { //echo 'fname='.$fname.'<br>' ;
       $func_name=include_script_cmd($options['cmd'],$options['script'],array()) ;
       //damp_array($options) ;
       if ($func_name)
         { ob_start() ;
           $res=$func_name($rec,$options) ;
           $ob_text=ob_get_clean() ;
           if (is_array($res)) list($text,$gen_elem,$dop_td_attr)=array_values($res) ; else $text=$res ;
           $text.=$ob_text ;
         }
     }
     else
     // во вторую очередь смотрим на имя поля - вывод для этих полей определен именем поля $fname. $edit_element добавлено чтобы уйти от вывода полей тут, если указан $edit_element
     switch ($fname.$edit_element)
     { case 'pkey' : $text=$value ;
     				 break ;
       //case 'clss' : $state_suff=($rec["enabled"])? "on":"off" ;
       //              $text="<img src='".$obj_clss->icons."' width=16 height=16 alt='clss=".$clss."' border='0' />" ;
	//				 if ($options['show_name_class']) $text.=$obj_clss->name ;
    // 				 break ;
       case 'parent' : // вывести название род-кого объекта
                       // переделана 27.08.11 на основе функции get_obj_parents_name
                       $text=get_obj_patch($rec) ;
     				 break ;
       /* полная отмена, вместо этого введены линки
       case 'parent2': // дополнительные подкатегории объекта
       case 'parent3':
       case 'parent4':
                     //$text='Вывод db_indx_multi_select отключен' ;
                     $parent_tkey=_DOT($tkey)->parent_tkey ;
                     if (!_DOT($parent_tkey)->pkey) $parent_tkey=$tkey ;
       				 $tname=_DOT($parent_tkey)->table_name ;
				     //$text="<input name='".$element_name."' type='text' class='text small' value='".$value."' size=".$edit_element_size." />" ;
				     $class_element='text small' ;
				     //if (!$read_only) $gen_elem='onclick="gi(this,\''.$element_name.'\',\''.$class_element.'\','.$value.')"' ;
				     //if (!$read_only) $id='onclick="gi(this,\''.$element_name.'\',\''.$class_element.'\','.$value.')"' ;
                     $gen_elem='gts' ;
				     if ($value)
				       { $parent_name=execSQL_van('select t1.obj_name,(select t2.obj_name from '.$tname.' t2 where t2.pkey=t1.parent and t2.parent!=0) as parent_name from '.$tname.' t1 where t1.pkey='.$rec[$fname]) ;
				     	 $text=($parent_name['parent_name'])? $parent_name['parent_name'].' / '.$parent_name['obj_name']: $parent_name['obj_name'] ;
				       }
                     if (!$read_only) { $gen_elem='gts' ;
                                        $dop_td_attr='as="'.$value.'"' ;
                                      }

                      //$text.=$value ;
				     break;
       */
       case 'c_data':
       case 'r_data': $text=$value ;
                      if (($options['is_edit'] or $options['datepicker']) and !$read_only) $gen_elem='gi' ;
                     break ;
       case 'enabled':
                    if (!$options['indx_select'])
             		{ if ($value) { $img_src="on2.gif" ; $img_alt='+' ;} else  { $img_src="off2.gif" ;$img_alt='-' ;}
                      if ($options['read_only']) $img_src=($value)? 'on3.gif':'off3.gif' ;
                      $text='<img src="'._PATH_TO_ADMIN_IMG.'/'.$img_src.'" width="16" height="16" border="0" alt="'.$img_alt.'">';
                      if (!$options['read_only']) $gen_elem='gc2' ; // gs2 - вывод зеленой лампочки а не галки
       				}
                    break ;
       case '_parent_reffer': $text=generate_obj_text($value,$options) ;
       				break ;
       /*
       case 'ip':   if ($rec['country']) $text=$value.'<br><span class=black>'.$rec['country'].'</span><br><span class=blau>'.$rec['city'] ;
                    elseif (_GEO_IP_METHOD_)
                    { $ip_info=get_IP_city_info($value) ;
       				  $text=$value.'<br><span class=black>'.$ip_info['country'].'</span><br><span class=blau>'.$ip_info['city'] ;
       				} else $text=$value ;
       				break ;
       */
       case 'url_name': $text=$obj_clss->prepare_url_obj($rec) ;
                        if (!$rec['url_name']) update_rec_in_table($tkey,array('url_name'=>$text),'pkey='.$pkey) ;
                        else                   $text=$rec['url_name'] ;
                        $gen_elem='gi' ;
                        break ;
       case 'clss_5_file_size':	    $text=round(filesize($rec['pkey'])/1024).' кБ' ; break ;

       //case 'clss_7_preview':		$text='_info_id('.$rec['pkey'].')' ; break ;
       case 'clss_16_goto':		    $text='<a href="fra_viewer_support.php?tkey='.$rec['tkey'].'&pkey='.$rec['pkey'].'">Перейти</a>' ; break ;
       case 'clss_21_arr_name':     $text='$_SESSION[\''.get_IL_array_name($rec)."']" ;break ;
       case 'clss_23_status':       $text=($rec['r_data']>time())? '+':''; break ;
       case 'clss_28_href_info':	$text=_PATH_TO_SITE.'/bonus_info.php?part_name='.$rec['obj_name'].'&pass='.$rec['password'] ; break ;
       case 'clss_47_data':         $text=date("d.m.y G:i:s",$rec['c_data']).' - '.date("d.m.y G:i:s",$rec['r_data']) ; break ;
       case 'clss_47_proc': 		if ($rec['sum_time_gen']) $text=round($rec['sum_time_gen']/($rec['r_data']-$rec['c_data'])*100,2).' %' ; break ;


       // если для имени поля нет предопределенной функции, производим вывод функции в соотвествии со значением edit_element
       default: $source_fname=($options['source_fname'])? $options['source_fname']:$fname ;
                $edit_element=$obj_clss->get_edit_element_to_field($source_fname,$options) ;
                // вывод поля в зависимости от значения $edit_element
                switch ($edit_element)
			 	       {// генерироемое поле 'memo'
		           		case 'memo':
                                 $view_as_HTML=($obj_clss->use_HTML_editor($rec,$fname))? 1:0 ;  // если редактор не используется, используем nl2br
                                 $text=$value ;
                                 if (!$view_as_HTML) $options['show_html_code']=1 ;
                                 if ($options['view_as_HTML']) $options['show_html_code']=0 ;
                                 if ($options['show_html_code']) $text=htmlspecialchars($text) ;
                                 //if (!$view_as_HTML and stripos($text,'<script')===false) $text=nl2br($text) ;
		           		         if (!$read_only and $options['no_generate_element'])
                                    { $text="<textarea class='memo' name='".$element_name."' id='".$element_id."' type='text' cols=10 rows=5 wrap=on>".$text."</textarea>" ;
                                      if ($view_as_HTML and _CKEDITOR_ and _CKEDITOR_!='none') $text.='<SCRIPT type="text/javascript" src="'._EXT.'/'._CKEDITOR_.'/ckeditor.js" ></SCRIPT><script type="text/javascript">window.addEvent("domready",function(){editor = CKEDITOR.replace("'.$element_id.'")});</script>' ;
                                    }
	 	    	         		 else $gen_elem='gm' ;
	 	    	         		 $td_class='left' ;
	 	    	         		 break ;

		           		// генерироемое поле 'input'
		           		case 'input':
		           			 	 $text=$value ;
	 	                		 if (!$read_only and $options['no_generate_element'] and $value) $text="<input class=text name='".$element_name."' type='text' value='".$value."' size=".$edit_element_size." />" ;
	 	    	         		 else $gen_elem='gi' ;
	 	                		 break ;

		           		// генерироемое поле 'checkbox'
		           		case 'checkbox': // в дальшейшем заменить off3.gif и on3.gif на программируемые значения, например check_0.gif и check_1.gif
                               if ($value) { $img_src=($class_element=='inverse')? "off3.gif":"on3.gif" ;  $img_alt='+' ;}
                               else        { $img_src=($class_element=='inverse')? "on3.gif":"off3.gif" ;  $img_alt='-' ;}
      			               $text='<img src="'._PATH_TO_ADMIN_IMG.'/'.$img_src.'" width="16" height="16" border="0" alt="'.$img_alt.'">';
				 	    	   $gen_elem='gc' ;
	 	                 	   break ;

		           		// генерироемое поле 'input' для ввода даты
		           		// 'data_format' - могут быть следующие варианты: 'd.m.y G:i','d.m.y','d.m.y G:i'
		           		case 'timedata':
		           		case 'data_input':
                                 if (!$read_only) $gen_elem='gi' ;
		                         $text=$value;
	 	                		 break ;
                        case 'serialize':
                                 if ($value) $arr=unserialize($value) ;
                                 //if (is_array($arr)) $text=get_damp_array($arr,1,-1) ; else $text=$value ;
                                 if (is_array($arr)) $text=damp_serial_data($arr) ; else $text=$value ;
                                 break ;
                        // показать значение из массива
                        case 'indx_array_value':
                                 if (!$value) $value=0 ;
                                 global ${$options['indx_array']} ;
                                 $text=${$options['indx_array']}[$value] ;
                                 break ;

                        // масств в виде select
                        case 'indx_select':
                                 $arr_name='' ;
								 if ($options['indx_select']) $arr_name=$options['indx_select'] ;
                                 if ($options['array'])       $arr_name=$options['array'] ;
                                 if (!$arr_name)              $arr_name=_CLSS($rec['clss'])->fields_info[$fname]['array'] ;
                                 //damp_array(_CLSS($rec['clss'])) ;
                                 $list=$_SESSION[$arr_name] ;
                                 $indx_field=($options['indx_field'])? $options['indx_field']:_CLSS($rec['clss'])->fields_info[$fname]['indx_field'] ;
                                 if (!$indx_field) $indx_field='obj_name' ;
                                 if ($arr_name)
                                     if (is_array($list))
                                     { $text=(is_array($list[$value]))? $list[$value][$indx_field]:$list[$value] ;
                                       if (!$read_only) { $gen_elem='gs' ;
                                                          $td_attr_arr['ai']=$arr_name ;
                                                          $td_attr_arr['as']=$value ;
                                                          $td_attr_arr['af']=$indx_field;
								                        }
                                     } else $text='<span class=red>Массив "'.$arr_name.'" не существует</span>' ;
                                 else $text='<span class=red>Не указано имя массива</span>' ;
                                 break ;
                        // массив для мультивыбора
                        case 'multichange':
                                 $arr_name=$options['array'] ;
                                 $indx_field=($options['fname'])? $options['fname']:'obj_name'  ;
                                 $td_class='left' ;
                                 if (mb_substr($arr_name,0,3)=='IL_') $text=_IL($arr_name)->get_multichange_field_view($value,$options) ;
                                 elseif ($value) // переделать потом для простых списков (одномерных массивов)
                                 { $list=$_SESSION[$arr_name] ;
                                   $arr_values=array() ;
                                   $arr_id=explode(' ',trim($value))  ;
                                   if (sizeof($arr_id)) foreach($arr_id as $id) $arr_values[]=$list[$id][$indx_field] ;
                                   if (sizeof($arr_values)) $text=implode('<br>',$arr_values) ; else $text='' ;
                                 }
                                 //$text.='<br>---<br>'.$value ;
								 if (!$read_only) { $gen_elem='gmc' ;
                                                    $td_attr_arr['ai']=$arr_name ;
                                                    $td_attr_arr['as']=trim($value) ;
                                                    $td_attr_arr['af']=$indx_field;

								                  }
                                 break ;

             			//case 'show_obj_field_functions': $text=$show_obj_field_functions[$cur_fname]($rec,$tkey,$pkey,$options,$cur_fname) ; break ;
						case 'urldecode': $text=urldecode($value) ; break ;
			       		case 'href': if ($value and strpos($value,'http://')===false) $value='http://'.$value ;
                                     $text=($value)? '<a href="'.htmlspecialchars(urldecode($value)).'" target=_blank>'.htmlspecialchars(urldecode($value)).'</a>':'' ;
                                     break ;
			       		case 'any_object': if ($value) $text=generate_obj_text($value,$options) ;
                                           else if ($options['select_from_table']) $text=generate_select_object_from_table($tkey,$clss,$pkey,$cur_fname,$options) ;
                                           break;

                        // вычисляемые поля ----------------------------------------------------------------------------------------------------------
                        case 'clss_patch': 	$text=get_obj_patch($rec) ; break ;

                        case 'clss_all_prices': $text=on_view_show_all_prices($rec) ; break ; // на удаление, должна вызываться через func_name

				        case 'parent_clss' :
				                     $text=(!$read_only)? generate_element('text',$edit_element_size,$tkey,$clss,$pkey,$cur_fname,$value).'&nbsp;&nbsp;'.$obj_clss->name:$obj_clss->name.' ('.$value.')' ; break;
				     				 break ;

				        case 'file_upload': if (!$read_only)
				      						  {     $text='<INPUT   name="upload_file['.$tkey.']['.$pkey.']" type=file class=file><br/><br/>' ;
				       				 				$text.= '<input name="upload_file_by_href['.$tkey.']['.$pkey.']" type=text class=text value=""> Ссылка' ;
				       				 		  }
				       				 		 break ;
				        case 'file_preview_upload'
				        				  : if (!$read_only)
				      						  {     $text='<INPUT   name="upload_preview_file['.$tkey.']['.$pkey.']" type=file class=file><br/><br/>' ;
				       				 				$text.= '<input name="upload_preview_file_by_href['.$tkey.']['.$pkey.']" type=text class=text value=""> Ссылка' ;
				       				 		  }
				       				 		break ;
				        case 'file_preview':$file_dir=_DOT($tkey)->dir_to_file.$rec['file_name'] ;
                                            include_extension('get_file_view_text')  ;
                                            $text=get_file_view_text($file_dir,array('flash_width'=>200,'use_preview'=>1)) ;
				        					break ;

				        case 'file_name':	$text=($read_only)? $value:'<a target=_clean href="'._DOT($tkey)->patch_to_file.$value.'">'.$value.'</a>' ;
				       						break ;

				        case 'file_href':	$text='/'._DOT($tkey)->dir_name.$rec['file_name'].'<br><br>'._DOT($tkey)->patch_to_file.$rec['file_name'] ;
				       						break ;

				        case 'show_size':	$text=format_size($value) ;
				        					break ;

                        case 'clss_3_size': $text=get_image_info(_DOT($tkey)->dir_to_file.'source/'.$rec['file_name'],$rec['obj_name']) ; break ;
                        case 'clss_3_image': $text=strip_tags(get_image_info(_DOT($tkey)->dir_to_file.'source/'.$rec['file_name'],$rec['obj_name'])) ;
                                             $text='<a href="'.img_clone($rec,'source').'" id=img_'.$pkey.'  target=_clear onclick="return hs.expand(this)" title="Размер: '.$text.'"><img src="'.img_clone($rec,'small').'" border="0" width=100 height=100/></a>';
                                             break ;
                        case 'clss_3_clone':$text=get_clone_info($tkey,$rec['file_name']) ; break ;
                        case 'clss_3_reffers':$text=get_img_reffers($tkey,$rec['file_name']) ; break ;

                        case 'clss_100_image': //$text=strip_tags(get_image_info(_DOT($tkey)->dir_to_file.'source/'.$rec['file_name'],$rec['obj_name'])) ;
                                               $rec_img=get_img_info_to_reffer($rec['reffer']) ;
                                               if ($rec_img['pkey']) $text='<a href="'.img_clone($rec_img,'source').'" id=img_'.$pkey.'  target=_clear onclick="return hs.expand(this)"><img src="'.img_clone($rec_img,'small').'" border="0" width=100 height=100/></a>';
                                               break ;

				        case 'clss_104_class_name': $text=_CLSS($rec['indx'])->name ; break ;
			       		default: $text=($options['no_htmlspecialchars'])? $value:$value ;
					   }
     }

    if ($options['ue'] and $text) $text.=' '.$options['ue'] ;
    if ($options['format_as_price'] and $text) { $text=_format_price($text) ; $td_class='right' ; }
    if ($options['suff'] and $text) $text.=$options['suff'] ;
    if ($options['pref'] and $text) $text=$options['pref'].$text ;
    if ($options['base_ue'] and $text) $text.=' '.$GLOBALS['base_ue'] ;
    if ($options['wrap']=='nowrap' and $text) $text='<div style="white-space: nowrap;">'.$text.'</div>' ;
    if ($options['nl2br']) $text=nl2br($text) ;

	//$td_attr_arr=array() ;
    // добавить  - для ReadOnly - убрать все заданные функции click
    // описываем класс ячейки - в нем будет передаваться вся информация: наименование поля, класс элемента, имя функции для генерации элемента
    if ($fname)                     $td_attr_arr['fn'][]=$fname ;
    if ($td_class)                  $td_attr_arr['class'][]=$td_class ;
    if ($options['td_class'])       $td_attr_arr['class'][]=$options['td_class'] ;
    if ($class_element)             $td_attr_arr['ec'][]=$class_element ;
    if ($gen_elem and !$read_only)  $td_attr_arr['el'][]=$gen_elem ;

    if ($dop_td_attr)
    { //if (preg_match_all('/(\w+?)=[\"\']([^\'\"]+?)[\"\']/is',$dop_td_attr,$matches,PREG_SET_ORDER))
      //    //foreach($matches as $match) array_push($a,array($match[1],$match[2]));
      $arr=explode(' ',$dop_td_attr) ;
      if (sizeof($arr)) foreach($arr as $val)
      { $arr2=explode('=',$val) ;
        $td_attr_arr[$arr2[0]][]=str_replace(array('"',"'"),'',$arr2[1]) ;
      }
    }
    $arr=array() ;
    if (sizeof($td_attr_arr)) foreach($td_attr_arr as $param=>$arr_values)
    {  if (is_array($arr_values))       $arr[]=$param.'="'.implode(' ',$arr_values).'"' ;
       else if ($param and $arr_values) $arr[]=$param.'="'.$arr_values.'"' ;
    }
    $td_attr=(sizeof($arr))? implode(' ',$arr):'' ;

    if ($options['on_view_after']) if (function_exists($options['on_view_after'])) $options['on_view_after']($rec,$tkey,$pkey,$options,$fname,$text) ;

    return(array($text,$td_attr)) ;
   }

function generate_select_object_from_table($tkey,$clss,$pkey,$fname,$options)
{
   //damp_array($options) ;
   if (!$options['select_from_table']) return ;
   $table_name=_DOT($options['select_from_table'])->table_name ;
   $usl_clss=($options['select_from_clss'])? ' and clss='.$options['select_from_clss']:'' ;
   $list_recs=execSQL('select * from '.$table_name.' where parent!=0 '.$usl_clss);

   $text='<select name=obj['.$tkey.']['.$clss.']['.$pkey.']['.$fname.']><option value=""></option>' ;
   if (sizeof($list_recs)) foreach($list_recs as $rec) $text.='<option value='.$rec['_reffer'].'>'.$rec['obj_name'].'</option>' ;
   $text.='</select>' ;
   return($text) ;
   //damp_array($list_recs) ;
}

 // на удаление. Пока используетмя в модулях - будет болванка, потом удалить
 function show_standart_button($options=array()) {}

 function show_add_image_button($tkey,$pkey) {?><img class="in_text" src="<?_PATH_TO_ENGINE?>/admin/images/add_icon.png" width="16" height="17" alt="" border="0"> Добавить <a after_close_event="show_all_img" menu_id="structure_3" reffer="<?echo $tkey?>_<?echo $pkey?>" href="/admin/image_uploader.php?tkey=<?echo $tkey?>&amp;pkey=<?echo $pkey?>&amp;auto_close=1" id="image_upload_<?echo $pkey?>" onclick="return hs.htmlExpand(this, {objectType: 'iframe', width: 500, headingText: 'Загрузка изображений',wrapperClassName: 'titlebar' } )" class="  ">Фото</a><?}

 function show_ckeditor_hidden_panel($title,$fname,$cnt=0)
 {
    ?><h1><?echo $title?><span class=small><? if ($cnt) echo ' ['.$cnt.' знаков]'?></span></h1><div class=expand_panel>развернуть/свернуть панель</div><div class="panel hidden"><? show_window_ckeditor_v3($GLOBALS['obj_info'],$fname,200) ; ?><br><button class="button" cmd="save_list">Сохранить</button></div><?
 }

 function show_ckeditor_visible_panel($title,$fname,$cnt=0)
 {
    ?><h1><?echo $title?><span class=small><? if ($cnt) echo ' ['.$cnt.' знаков]'?></span></h1><div class="panel"><? show_window_ckeditor_v3($GLOBALS['obj_info'],$fname,200) ; ?><br><button class="button" cmd="save_list">Сохранить</button></div><?
 }



 function _show_window_ckeditor_v3($rec,$fname,$height=300,$use_element_name='')
 { $element_name=($use_element_name)? $use_element_name:"obj[".$rec['tkey']."][".$rec['clss']."][".$rec['pkey']."][".$fname."]" ;
   $element_id=($use_element_name)? $use_element_name:"obj_".$rec['tkey']."_".$rec['clss']."_".$rec['pkey']."_".$fname ;
   $cur_content=($fname)? $rec[$fname]:'' ;
   ?><textarea name="<?echo $element_name?>" class="ckeditor" id=<?echo $element_id?>><?echo $cur_content?></textarea><?
 }

function show_window_ckeditor_v3($rec,$fname,$height=300,$options=array())
 { $style_file=($options['style_file'])? $options['style_file']:'/style.css' ;
   $use_element_name=(!is_array($options))?  $options:$options['use_element_name'] ;
   if (!is_array($options)) $options=array() ;
   $element_name=($use_element_name)? $use_element_name:"obj[".$rec['tkey']."][".$rec['clss']."][".$rec['pkey']."][".$fname."]" ;
   $element_id=($use_element_name)? $use_element_name:"obj_".$rec['tkey']."_".$rec['clss']."_".$rec['pkey']."_".$fname ;
   $cur_content=($fname)? $rec[$fname]:'' ;
   if ($options['content']) $cur_content=$options['content'] ;
   // если отдаем текст в редактор и там вообще нет <тегов> - заменяем переносы строк на абзац
   if (!preg_match('/<(.+?)>/is',$cur_content,$match)) $cur_content=preg_replace('~^(?![\r\n])(.*?)\r?\n?$~m','<p>$1</p>',$cur_content);
   // аналог: for ($result = '',$i = 0,$a = explode("\r\n",$str),$s = sizeof($a); $i < $s && ($result .= (!strlen($a[$i])?$a[$i]:'<p>'.$a[$i].'</p>')."\n") !== FALSE; ++$i);


   ?><textarea name="<?echo $element_name?>" id=<?echo $element_id?>><?echo $cur_content?></textarea>
     <SCRIPT type="text/javascript" src="<? echo _EXT.'/'._CKEDITOR_?>/ckeditor.js" ></SCRIPT>
     <script type="text/javascript">
       window.addEvent('domready', function()
       {config=
        {   height: <?echo $height?>,
            contentsCss : "<?echo $style_file?>",
            <?if ($options['editor_config']) echo $options['editor_config'].','?>
            <?if ($_SESSION['CK_editor_toolbar']) echo $_SESSION['CK_editor_toolbar'].','?>
            EnterMode:'p'
        };
        editor = CKEDITOR.replace( '<?echo $element_id?>', config);
       }) ;
       </script>
      <?
 }

 function show_preview_site()
    { global $obj_info ;
      $clss_info=_DOT($obj_info['tkey'])->list_clss_ext[$obj_info['clss']] ;
      $cnt=preg_match_all('#\#(.*?)\##i',$clss_info['page_pattern'],$arr) ;
      //damp_array($arr) ;
      if ($arr[0][0])
       { $page_href=str_replace($arr[0][0],$obj_info[$arr[1][0]],$clss_info['page_pattern']) ;
         //echo $site_patch.$page_href ;
         ?><div style='margin:5px;'>Адрес: <input class='text' name='adress' type='text' value='<?echo _PATH_TO_SITE.'/'.$page_href ?>' size=100></div><?
         ?><iframe src="<?echo _PATH_TO_SITE.'/'.$page_href ?>" name='fra_prev' id=fra_prev  scrolling=auto frameborder=1 width=100% height=600>&nbsp;</iframe><?
       }
    }


  function panel_obj_images()
  {

  }


//-----------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------
// Журнальные таблицы
//-----------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------

 // показ истории по заданному условию
 function show_history($usl_show,$options=array())
  {  $order=($options['order'])? $options['order']:'c_data' ;
     _CLSS(40)->show_list_items(TM_LOG_EVENTS,$usl_show,array('debug'=>0,'default_order'=>$order,'read_only'=>1,'no_check'=>1,'no_icon_edit'=>1,'no_table_header'=>0,'all_enabled'=>1,'no_htmlspecialchars'=>1,'no_view_field'=>'reffer')) ;
  }

 function show_link()
 { global $obj_info ;
   _CLSS(102)->show_list_items(TM_LINK,'',array('cur_obj'=>$obj_info)) ;
 }

 function show_SEO()
 { global $obj_info ;
   $options['no_extended_info']=1 ;
   include_class(36) ;
   //$system=$_SESSION['list_created_sybsystems'][$obj_info['tkey']] ;
   //if ($system and is_object($_SESSION[$system])) damp_array($_SESSION[$system]) ;
     //damp_array($obj_info) ;
   //$meta_info=execSQL('select * from '.$TM_sitemap.' where url like "%'.$obj_info['url_name'].'" and enabled=1 order by url',2) ;
   $url='' ;
   if ($obj_info['__href'])
   {  $url=str_replace('http://','',$obj_info['__href']) ;
      if (!_USE_SUBDOMAIN)  $url=_MAIN_DOMAIN.$url ;
      $_usl[]='url="'.$url.'"' ;
   }
   $_usl[]='obj_reffer="'.$obj_info['_reffer'].'"' ;
   $_usl[]='enabled=1' ;
   //damp_array($_usl) ;
   $meta_info=execSQL_van('select * from '.TM_SITEMAP.' where  '.implode(' and ',$_usl).' order by url limit 1') ;
   ?><input type=hidden name=cur_sitemap_page_id value=<?echo $meta_info['pkey']?>><?

   //$this->get_url_item($obj_info) ;

   if (!sizeof($meta_info))
   { //echo 'Создаем запись для данной страницы в SEO' ;
     adding_rec_to_table_fast(TM_SITEMAP,array('parent'=>1,'clss'=>36,'obj_reffer'=>$obj_info['_reffer'],'url'=>$url),array('debug'=>0)) ;
     $meta_info=execSQL_van('select * from '.TM_SITEMAP.' where obj_reffer="'.$obj_info['_reffer'].'" and enabled=1 order by url limit 1') ;
   }
   // $SEO_info - все страницы связынные с текущим объектом
   $SEO_info=execSQL('select * from '.TM_SITEMAP.' where obj_reffer="'.$obj_info['_reffer'].'" order by url') ;
   unset($SEO_info[$meta_info['pkey']]) ;

   // показываем свойства главной записи в SEO
   _CLSS($meta_info['clss'])->show_props($meta_info,$options) ;

   ?><button class="button" cmd="sitemap_delete_id_reffer_from_rec" params="<?echo $meta_info['pkey']?>" script="ext/sitemap/map_func.php">Удалить связь этой страницы с текущим объектом</button><?


   //$SEO_info=execSQL('select * from '.TM_SITEMAP.' where url like "lux-time.com.ua/catalog/watch/AudemarsPiguet/%" order by url',2) ;
   if (sizeof($SEO_info))
   { ?><br><br><h1>Другие страницы, связанные с текущим объектом</h1><?
     _CLSS(36)->show_list_items($SEO_info) ;
     //show_standart_button() ; //   show_standart_button - более не используется
     ?><button class="button" cmd="sitemap_delete_check_reffer_from_rec" script="ext/sitemap/map_func.php">Удалить связь отмеченных страниц с текущим объектом</button><?
     ?><button class="button" cmd="sitemap_copy_tags_to_main_page" script="ext/sitemap/map_func.php">Скопировать теги в основную страницу объекта</button><?
   }
 }

 function show_SEO_small()
 { global $obj_info ;
   $options['edit_list']=_CLSS(36)->view['small'] ;
   $options['no_header']=1 ;
   $options['no_extended_info']=1 ;

   $SEO_info=execSQL('select * from '.TM_SITEMAP.' where obj_reffer="'.$obj_info['_reffer'].'" order by url',array('no_indx'=>1)) ;
   if (!sizeof($SEO_info))
   { //echo 'Создаем запись для данной страницы в SEO' ;
     $new_id=adding_rec_to_table_fast(TM_SITEMAP,array('parent'=>1,'clss'=>36,'obj_reffer'=>$obj_info['_reffer']),array('debug'=>0)) ;
     $SEO_info=execSQL('select * from '.TM_SITEMAP.' where pkey="'.$new_id.'"',array('no_indx'=>1)) ;
   }

   // показываем свойства главной записи в SEO
   _CLSS($SEO_info[0]['clss'])->show_props($SEO_info[0],$options) ;

   if (sizeof($SEO_info)>1)
   { ?><br><br><h1>Другие страницы, связанные с текущим объектом</h1><?
     unset($SEO_info[0]) ;
     //print_2x_arr($SEO_info) ;
     _CLSS(36)->show_list_items($SEO_info) ;
   }
 }

 function show_db_info()
 { global $obj_info ;
   damp_array($obj_info,1,-1) ;
 }


 //**************************************************************************************************************************************************
 //
 // функции с генерируемыми данными
 //
 //**************************************************************************************************************************************************

  // возвращает путь текущего объекта
  // переделана 27.08.11 на основе функции get_obj_parents_name
  function get_obj_patch(&$rec)
  {  //  $options['include_root_name'] - включать в итоговый массив имя корня дерева, по умолчанию выключена, пока не научимся определять опцию  $_SESSION['init_options']['XXX']['no_root_dir']=1 ;
     $patch=get_obj_parents_name($rec['_reffer'],array('include_root_name'=>1)) ;
     $arr=array() ;
     if (sizeof($patch)) foreach($patch as $reffer=>$name) $arr[]='<a href="'.get_url_window_object($reffer).'">'.$name.'</a>' ;
     $str=implode(' / ',$arr) ;
     return($str) ;
  }

  // 27.08.11
  // возвращает url окна редактирования объекта
  function get_url_window_object($reffer)
  { global $php_source ;
    list($pkey,$tkey)=explode('.',$reffer) ;
    $url=_CUR_PAGE_DIR._CUR_PAGE_NAME.'?tkey='.$tkey.'&pkey='.$pkey.'&rn='.rand(0,1) ;
    return($url) ;
  }

?>
