$j(document).ready(function()
{

    $j('select.button').change(function()
     { var cmd=$j(this).attr('cmd') ;
       var mode=$j(this).attr('mode') ;
       var pkey=$j(this).attr('pkey') ;
       switch(mode)
       { default:       $j('input#cmd').val(cmd) ; if (pkey!=undefined) $j('input#pkey').val(pkey) ; break ;
       }
       $j('#form').submit() ;
     }) ;

});

    // gallery config object
    var cfg_img_upload = {
        objectType: 'iframe',
        width: 500,
        headingText: 'Загрузка изображений',
        wrapperClassName: 'titlebar',
        transitions: ['expand', 'crossfade'],
        useControls: false
    };

function on_reload_tr_obj(data,status,link)
  { if (status=='success')
      {   var xml = link.responseXML;
          if (xml==undefined) { alert('Нет заголовка XML в sync - обновите файл')  ; return ; }
          var value = get_xml_tag_value(xml,'tr_html');
          var attr = get_xml_tag_attr(xml,'tr_html');
          if (value!=undefined) $j('tr#'+attr.tr_id).html(value) ;
      }
  }

function on_preview_uploaded_obj(data,status,link)
  { if (status=='success')
      {   var xml = link.responseXML;
          if (xml==undefined) { alert('Нет заголовка XML в sync - обновите файл')  ; return ; }
          var elem = xml.getElementsByTagName('obj'); // массив значений массива
          var el_html='<h1>Добавлено:</h1><ul>' ;
          $j.each(elem,function(indx,obj)
              { var obj_attr=get_xml_obj_attr(obj) ; // получаем аттрибуты текущего value
                var title=($j.browser.msie)? obj.text:obj.textContent ;
                el_html+='<li>'+obj_attr.class_name+': '+title+'</li>' ;
              }) ;
          el_html+='</ul>' ;
          var attr = get_xml_tag_attr(xml,'objects');
          $j('div#inner_'+attr.tkey+'_'+attr.id).html(el_html) ;
      }
  }

function on_show_all_img(data,status,link)
  { if (status=='success')
      {   var xml = link.responseXML;
          if (xml==undefined) { alert('Нет заголовка XML в sync - обновите файл')  ; return ; }
          var elem = xml.getElementsByTagName('div_inner_html'); // массив значений массива
          var el_html='' ;
          $j.each(elem,function(indx,obj)
              { var obj_attr=get_xml_obj_attr(obj) ; // получаем аттрибуты текущего value
                el_html+=($j.browser.msie)? obj.text:obj.textContent ;
              }) ;
          //el_html+='</ul>' ;
          //var attr = get_xml_tag_attr(xml,'objects');
          //$j('div#panel_img_inner_'+attr.tkey+'_'+attr.id).html(el_html) ;
          $j('div#panel_img_inner').html(el_html) ;
      }
  }
