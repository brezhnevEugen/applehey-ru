<?
include_once (_DIR_TO_ENGINE.'/admin/c_menu_tabs.php') ;

//---------------------------------------------------------------------------------------------------------------------
//
// функции для сохранения удаленных объектов
//
//---------------------------------------------------------------------------------------------------------------------


function copy_check_obj_to_trash($_check)
{   if (sizeof($_check)) foreach ($_check as $tkey=>$tkey_arr)
	  if (sizeof($tkey_arr)) foreach ($tkey_arr as $clss=>$clss_arr) if (!_CLSS($clss)->no_use_trash)
 		  if (sizeof($clss_arr)) foreach($clss_arr as $pkey=>$parent)
           {   $cur_obj[$tkey][$clss][$pkey]=$parent ;
               include_once(_DIR_ENGINE_EXT.'/XML/export_OBJ_to_XML_doc.php') ;
               $doc=export_OBJ_to_XML_doc($cur_obj,array('copy_file_to_trash'=>1)) ; // получаем XML описание объекта, одновременно перемещая файлы объекта в корзину
               $content=$doc->saveXML() ;
               $obj_info=select_db_obj_info($tkey,$pkey) ;
               $name=($obj_info['obj_name'])? $obj_info['obj_name']:_CLSS($clss)->name.' # '.$pkey ;
               $fname=safe_patch_files(safe_file_names($name)).'.xml' ;
               if (!is_dir(_DIR_TO_DELETED.'/')) create_dir(_DIR_TO_DELETED.'/') ;
               file_put_contents(_DIR_TO_DELETED.'/'.$fname,$content) ;
               //echo $content.'<br>' ;
               echo 'Помещен в мусорную корзину:  <span class=green>'._CLSS($clss)->name.'</span> - <strong>'.$name.'</strong><br>' ;
           }

}

//---------------------------------------------------------------------------------------------------------------------
//
// класс - система доступа админки
//
//---------------------------------------------------------------------------------------------------------------------

class c_admin_account_system 
{
   public $table_name ;
   public $tkey ;
   public $id ;     // идентификатор пользователя по БД (если он внесен в БД)
   public $reffer ; // идентификатор в формате сслыки
   public $info ; //

 // создаем клиента
 function c_admin_account_system($options=array())
 {  $this->table_name=TM_PERSONAL ;
    $this->tkey=get_table_id($this->table_name) ;
 }

 function login()
 {  //echo 'login='.$login.'<br>' ;
    //header('WWW-Authenticate:Basic realm="By Invitation Only"'); // разлогинится
    $login=_CURRENT_USER;

    $this->get_info_from_db_by_login($login,$_SESSION['member']) ;
    if ($_SESSION['member']->id) $this->reload_menu($_SESSION['member']) ;

    _event_reg(array('obj_name'=>'Вход в админку','member'=>$_SESSION['member']->reffer,'reffer'=>$this->reffer_logon_rec)) ; // регистрируем событие 'Вход на сайт'

    if (!isset($_SESSION['top_admin_menu'])) $_SESSION['top_admin_menu']=new c_top_menu() ;

    return($_SESSION['member']->id) ;
 }

 function get_info_from_db_by_login($login,$member_obj)
 {  $db_login=($login=='root')? 'admin':$login ;   // исправляем ошибку постоянного логированая администратора в системе
    $info=execSQL_van('select * from '.$this->table_name.' where login="'.$db_login.'" and enabled=1') ;
    if ($info['pkey'])
    { $member_obj->info=$info ;
      $member_obj->id=$info['pkey'] ;
      $member_obj->name=($login=='root')? 'Конфигуратор':$info['obj_name'] ;
      $member_obj->login=$login ;
      $member_obj->email=$info['email'] ;
      $member_obj->autorize_token=$info['code'] ;
      $member_obj->reffer=$info['pkey'].'.'.$this->tkey ;
    }
 }

 function reboot_member(&$member)
 {
    $this->get_info_from_db_by_login($member->login,$member) ;
    $this->reload_menu($_SESSION['member']) ;
 }

 function logout()
 {  $_SESSION['member']->id='' ;
    $_SESSION['member']->reffer='' ;
    $_SESSION['member']->name='' ;
    $_SESSION['member']->email='' ;
    $_SESSION['member']->autorize_token='' ;
    $_SESSION['member']->info=array() ;

 }

 function reload_menu(&$member_obj)
 {  if (!$member_obj->id) return;
    $member_obj->menu=array() ;
 	$member_obj->menu=$_SESSION['menu_admin'] ;
 	$member_obj->menu_moduls=$_SESSION['menu_admin_site'] ;
 	$member_obj->menu_top=$_SESSION['menu_admin_top'] ;

 	if ($member_obj->login=='admin' or $member_obj->login=='root') return ;

 	$list_items=execSQL('select obj_name from '.$this->table_name.' where parent='.$member_obj->id) ; // разделы меню

 	if (sizeof($member_obj->menu)) foreach($member_obj->menu as $rasd_name=>$rasd_items)
       { if (sizeof($rasd_items)) foreach ($rasd_items as $i=>$item) if (!isset($list_items[$item['href']])) unset($member_obj->menu[$rasd_name][$i]) ;
         if (!sizeof($member_obj->menu[$rasd_name])) unset($member_obj->menu[$rasd_name]) ;
     }

    if (sizeof($member_obj->menu_moduls)) foreach($member_obj->menu_moduls as $rasd_name=>$rasd_items)
       { if (sizeof($rasd_items)) foreach ($rasd_items as $i=>$item) if (!isset($list_items[$item['href']])) unset($member_obj->menu_moduls[$rasd_name][$i]) ;
         if (!sizeof($member_obj->menu_moduls[$rasd_name])) unset($member_obj->menu_moduls[$rasd_name]) ;
     }

    if (sizeof($member_obj->menu_top)) foreach($member_obj->menu_top as $i=>$item)
     if (!isset($list_items[$item['href']])) unset($member_obj->menu_top[$i]) ;

    //damp_array($member_obj) ;
 }

function show_menu($member_obj,$to_member=0)
 { $list_recs=array() ;    //damp_array($member_obj) ;
   if (sizeof($member_obj->menu_moduls)) $list_recs=$member_obj->menu_moduls ;
   if (sizeof($member_obj->menu)) $list_recs=array_merge($list_recs,$member_obj->menu) ;
   if (sizeof($list_recs))
   { include_class(95) ;
     clss_95_list_items($list_recs,array('to_member'=>$to_member)) ;
   }
   else
   {    ?><div class='alert' align=center style='text-align:center;margin:50px;'>Меню не определено. Обновите данную страницу еще раз. Если меню не появилось, обратитесь к администратору сайта.</div><?
        ob_start() ;
        damp_array($_SERVER) ;
        damp_array($_SESSION) ;
        $text=ob_get_clean() ;
        _send_mail(_EMAIL_ENGINE_DEBUG,'Слетели пункты меню в сессии','Не отработала функция init_admin_setting<br>'.$text) ;
        $_SESSION['_flags']=array() ;
   }
 }
}


//---------------------------------------------------------------------------------------------------------------------
//
// класс - корзина в админке
//
//---------------------------------------------------------------------------------------------------------------------


class c_cart_admin
{ public $cnt ;	      // число товаров в корзине
  public $list_check =array() ; // краткая информация по товару в корзине

   // создание корзины
  function c_cart_admin()
  {  $this->cnt=0  ;
     $this->not_delete_obj_from_backet=0 ;
  }

  function destroy() { $list_check =array() ; }

  // добавление товара в корзину
  function add($_check)
  { //damp_array($_check) ;
    //damp_array($this->list_check) ;

    if (sizeof($_check)) foreach ($_check as $tkey=>$tkey_arr)
	  if (sizeof($tkey_arr)) foreach ($tkey_arr as $clss=>$clss_arr)
 		  if (sizeof($clss_arr)) foreach($clss_arr as $pkey=>$parent) $this->list_check[$tkey][$clss][$pkey]=$parent  ;

    $this->update_cnt() ;
  }

  // удаляем из корзины
  function del($_check)
  { //damp_array($_check) ;
    //damp_array($this->list_check) ;

    if (sizeof($_check)) foreach ($_check as $tkey=>$tkey_arr)
	  if (sizeof($tkey_arr)) foreach ($tkey_arr as $clss=>$clss_arr)
 	   {  if (sizeof($clss_arr)) foreach($clss_arr as $pkey=>$parent)
 		  { unset($this->list_check[$tkey][$clss][$pkey])  ;
 		    if (!sizeof($this->list_check[$tkey][$clss])) unset($this->list_check[$tkey][$clss]) ; // очистка пустных массивов
 		  }
 		  if (!sizeof($this->list_check[$tkey])) unset($this->list_check[$tkey]) ; // очистка пустных массивов
 	   }
 	   if (!sizeof($this->list_check)) unset($this->list_check) ; // очистка пустных массивов

 	$this->update_cnt() ;

  }

  function update_cnt()
  {
    $this->cnt=0 ;
 	if (sizeof($this->list_check)) foreach ($this->list_check as $tkey=>$tkey_arr)
	  if (sizeof($tkey_arr)) foreach ($tkey_arr as $clss=>$clss_arr)
 		  if (sizeof($clss_arr)) foreach($clss_arr as $pkey=>$parent) $this->cnt+=1 ;

    //damp_array($this->list_check) ;
  }

  // очищаем корзину
  function erase()
  { $this->list_check = array() ;
	$this->cnt=0 ;
  }


  function save_params()
  {
    if (isset($_POST['_not_delete_obj_from_backet'])) $this->not_delete_obj_from_backet=$_POST['_not_delete_obj_from_backet'] ;
  }
}

 // *****************************************************************************************************************************************************************
 // *****************************************************************************************************************************************************************
 // *****************************************************************************************************************************************************************

 // КОРЗИНА

 // показываем корзину
 function show_cart()
  { //damp_array($cur_page) ;
    if (sizeof(_CUR_PAGE()->obj_info)) { $allow_clss=_CLSS(_CUR_PAGE()->obj_info['clss'])->get_adding_clss(_CUR_PAGE()->obj_info['tkey'],_CUR_PAGE()->obj_info['pkey'],0) ; }
    else                             $allow_clss=_CLSS(0)->get_adding_clss(_CUR_PAGE()->tkey) ;
    $arr_links_clss=_CLSS(100)->get_arr_child_clss() ; // список классов дочерних от "ссылка на объект"
    $arr_allow_clss=array_keys($allow_clss);
    $res_arr=array_intersect($arr_links_clss,$arr_allow_clss) ;
    $copy_as_links=(sizeof($res_arr))? 1:0;
    $copy_as_links=1 ;  // объект всегда можно скопировать как ссылку если прави

 	?><div id=childs_objs><?
 	// по массиву check получаем массивы информации по объекту

    $cnt_view=0 ; $cnt_copy_move=0 ;
 	if (sizeof($_SESSION['cart_admin']->list_check)) foreach ($_SESSION['cart_admin']->list_check as $tkey=>$tkey_arr)
	  if (sizeof($tkey_arr)) foreach ($tkey_arr as $clss=>$clss_arr) if (isset($allow_clss[$clss]) or $copy_as_links)
	   { $pkey_str=implode(',',array_keys($clss_arr))  ;
         $options['read_only']=1 ;
         $options['buttons']=array() ;
         if ($_SESSION['cart_admin']->cnt==1) $options['check_all']=1 ;
         _CLSS($clss)->show_list_items($tkey,'pkey in ('.$pkey_str.')',$options) ;
         $cnt_view+=sizeof($clss_arr)  ;
         if (isset($allow_clss[$clss])) $cnt_copy_move+=sizeof($clss_arr) ;

	   }
    if ($cnt_view)
    { if ($cnt_copy_move){?><button class=button cmd=cart_copy_as_obj>Сделать копию</button><button class=button cmd=cart_move_as_obj>Переместить</button><?}

      if (isset($copy_as_links)){?><button class=button cmd=cart_copy_as_refs>Сделать линк</button><?}

      ?><button class=button cmd=export_obj_to_xml mode=give_file>В XML</button>
        <button class=button cmd=cart_erase mode=before_select_obj_info>Очистить список</button><br><br><strong>Параметры корзины:</strong><br><br>
        <input name="_not_delete_obj_from_backet" type="hidden" value="0"><input name="_not_delete_obj_from_backet" type="checkbox" value="1" <?if ($_SESSION['cart_admin']->not_delete_obj_from_backet) echo 'checked'?>>&nbsp;Не удалять с корзины после операции<br>
      <?
      if ($cnt_copy_move) // опции только для copy/move
      {?>
	   <!--<input name="Name" type="checkbox" value="ON">&nbsp;Без дочерних объектов<br>-->
	   <!--<input name="Name" type="checkbox" value="ON">&nbsp;Создавать копии изображений<br>-->
	   <!--<input name="save_pkey" type="checkbox" value="ON">&nbsp;Сохранить id объектов<br>-->
      <?}
    }
    else {?><div class=alert>Вы не можете вставить или скопировать сюда выбранные объекты</div><?}
    ?></div><?
  }

 // добавить в корзину
 function add_obj_to_cart()
	 { global $_check ;
       //damp_array($_check) ;
       //damp_array($_POST) ;
	   if (!is_object($_SESSION['cart_admin']))  $_SESSION['cart_admin']=new c_cart_admin() ;
       $_SESSION['cart_admin']->save_params() ;
       $_SESSION['cart_admin']->add($_check) ;
	 }

 /*
 function cmd_exe_cart_delete_check()
 {  global $_check ;
    $_SESSION['cart_admin']->save_params() ;
	$_SESSION['cart_admin']->del($_check) ;
 }
 */

 // переносим товар из корзины
 function cart_move_as_obj()
 { global $_check,$tkey,$obj_info ;
   // перемещения объектов через корзину производиться по разному для объектов в этой же таблице и для объектов с другой таблицы
   // перемещение первых происходит за счет изменения parent
   // переменение вторых - через XML DOM
   $_SESSION['cart_admin']->save_params() ;
   $_check_source=$_check ; //damp_array($_check_source) ; echo 'tkey='.$tkey.'<br>';
   // 1. перемещаем объекты, которые находятся внутри текущей таблицы
   if (sizeof($_check)) foreach($_check as $cur_tkey=>$tkey_arr)
     if (sizeof($tkey_arr)) foreach($tkey_arr as $cur_clss=>$clss_arr) if (sizeof($clss_arr))
        if ($cur_tkey==_DOT($tkey)->list_clss[$cur_clss]) // смотрим, нет ли перемещаний объектов для текущего объекта в рамках текущего класса
        {  $str_pkeys=implode(',',array_keys($clss_arr)) ;
           //echo $str_pkeys.'<br>' ; echo _DOT($cur_tkey)->table_name.'<br>' ;
           update_rec_in_table($cur_tkey,array('parent'=>$obj_info['pkey']),'pkey in ('.$str_pkeys.')',array('debug'=>0)) ;
           echo 'Объекты <strong>'.$str_pkeys.'</strong> перемещены в  <span class=green>'._CLSS($obj_info['clss'])->name.'</span> "<strong>'.$obj_info['obj_name'].'</strong>"<br>' ;
	       unset($_check[$cur_tkey][$cur_clss]) ;
	    }
   // удаляем из массива пустые позиции
   if (sizeof($_check)) foreach($_check as $cur_tkey=>$tkey_arr) if (!sizeof($tkey_arr)) unset($_check[$cur_tkey]) ;
   //echo 'check после копирования внутри таблицы' ; damp_array($_check) ; echo '<br>' ;

   // 2. копируем оставшиеся объекты из одной таблицы в другую
   if (sizeof($_check))
   { include_once(_DIR_ENGINE_EXT.'/XML/export_OBJ_to_XML_doc.php') ;
     $doc=export_OBJ_to_XML_doc($_check) ; // создаем DOM-XML объект
     include_once(_DIR_ENGINE_EXT.'/XML/import_OBJ_from_XML.php') ;
   	 import_OBJ_from_XML($doc,array('to_obj_reffer'=>$obj_info['_reffer'],'save_pkey'=>$_POST['save_pkey'])) ; // создаем новые объекты из DOM-модели

     // 2.1. удаляем объекты из исходной таблицы
     cmd_delete_obj($_check) ;
   }

   // 4. очищаем корзину от отработанных объектов
   if (!$_SESSION['cart_admin']->not_delete_obj_from_backet) $_SESSION['cart_admin']->del($_check_source) ;

   // 5. возвращаем id активного пункта меню
   if (sizeof($_check)) list($_tkey,$arr1)=each($_check) ; if (sizeof($arr1)) list($_clss,$arr2)=each($arr1) ;
   if ($_SESSION['cart_admin']->cnt)  return('cart') ;
   else if ($_clss) return('structure_'.$_clss) ;
   else             return('structure') ;

 }

 // копируем товар из корзины
 function cart_copy_as_obj()
 { global $_check,$obj_info ;
   include_once(_DIR_ENGINE_EXT.'/XML/export_OBJ_to_XML_doc.php') ;
   include_once(_DIR_ENGINE_EXT.'/XML/import_OBJ_from_XML.php') ;

   $_SESSION['cart_admin']->save_params() ;
   //damp_array($_check) ;
   $doc=export_OBJ_to_XML_doc($_check,array('use_inner_file_dir'=>1)) ; // создаем DOM-XML объект
   list($cnt,$size)=import_OBJ_from_XML($doc,array('to_obj_reffer'=>$obj_info['_reffer'],'save_pkey'=>$_POST['save_pkey'],'debug_import'=>0)) ; // создаем новые объекты из DOM-модели
   if ($cnt) echo '<div class=green bold>Произведено копирование '.$cnt.' объектов</div>' ;
   if (!$_SESSION['cart_admin']->not_delete_obj_from_backet) $_SESSION['cart_admin']->del($_check) ;
   if (sizeof($_check)) list($_tkey,$arr1)=each($_check) ; if (sizeof($arr1)) list($_clss,$arr2)=each($arr1) ;
   if ($_SESSION['cart_admin']->cnt)  return('ok') ;
   else if ($_clss) return('structure_'.$_clss) ;
   else             return('structure') ;
 }


 // переделано 14.08.13
 // вместо out-таблиц для ссылок используем одну таблицу с линками
 // просто вставляем в эту таблицу данные объекта и объекта, в составе которого он будет находиться
 function cart_copy_as_refs()
 { global $_check,$tkey,$obj_info,$cur_page ;
   $_SESSION['cart_admin']->save_params() ;
   //$use_clss=0 ;
   //if (sizeof($cur_page->obj_info)) $allow_clss=_CLSS($cur_page->obj_info['clss'])->get_adding_clss($cur_page->obj_info['tkey'],$cur_page->obj_info['pkey']) ;
   //else                             $allow_clss=_CLSS(0)->get_adding_clss($cur_page->tkey) ;
   //$arr_links_clss=_CLSS(100)->get_arr_child_clss() ; // список классов дочерних от "ссылка на объект"
   //$arr_allow_clss=array_keys($allow_clss);
   //$res_arr=array_intersect($arr_links_clss,$arr_allow_clss) ;
   //if (sizeof($res_arr)) $use_clss=$res_arr[0] ;
   if (sizeof($_check)) foreach ($_check as $cur_tkey=>$tkey_arr) if (sizeof($tkey_arr)) foreach ($tkey_arr as $cur_clss=>$clss_arr) if (sizeof($clss_arr)) foreach($clss_arr as $cur_pkey=>$parent)
     {  $rec=array('pkey'=>$cur_pkey,'tkey'=>$cur_tkey,'clss'=>$cur_clss) ;
        create_link($rec,$obj_info) ;
	             }
	   if (!$_SESSION['cart_admin']->not_delete_obj_from_backet) $_SESSION['cart_admin']->del($_check) ;
   if ($_SESSION['cart_admin']->cnt)  return('cart') ;
   else return('structure_LINK') ;
 }

 // очистить корзину
 function cart_erase()
 { $_SESSION['cart_admin']->save_params() ;
   $_SESSION['cart_admin']->erase() ;
 }



?>
