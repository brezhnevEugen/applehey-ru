<?php
 include_once(_DIR_TO_ENGINE.'/admin/i_img_working.php') ;

 // создаем скрипты пунктов меню
 function create_menu_item($file_items)
 { global $install_options ;
   // для выполения необходимо или вообще не задавать опции $install_options или задать опцию $install_options['install_script_to_admin']
   if (sizeof($install_options) and !$install_options['install_script_to_admin']) return ;
   echo '<div class="green bold">Создаем скрипты для пунктов меню админки</div>' ;
   create_script_file($file_items,_DIR_TO_ROOT.'/'._ADMIN_.'/','../ini/patch.php') ;
 }

 // создаем скрипты пунктов меню
 function create_script_to_page($file_items,$dir_to='',$patch_dir='')
 { global $install_options ;
   // для выполения необходимо или вообще не задавать опции $install_options или задать опцию $install_options['install_copy_script']
   if (sizeof($install_options) and !$install_options['install_copy_script']) return ;
   echo '<div class="green bold">Создаем входные скрипты модуля</div>' ;
   create_script_file($file_items,$dir_to,$patch_dir) ;
 }

 function create_script_file($file_items,$dir_to='',$patch_dir='')
 { if (!$dir_to) $dir_to=_DIR_TO_ROOT.'/' ;
   if (!$patch_dir) $patch_dir='ini/patch.php' ;

   if (sizeof($file_items)) foreach($file_items as $fname=>$options)
   if ($fname!='.htaccess')
   { $patch_fname=$dir_to.$fname ;
     $_content=array() ;
     echo 'Создаем скрипт: <strong>'.hide_server_dir($patch_fname).'</strong>' ;
     if (!$options['text_content'])
     { $_content[]='<? $options=array() ;' ;
       $_content[]="   include ('".$patch_dir."');" ;
       $_content[]='   include (_DIR_TO_ENGINE."/i_system.php") ;' ;
     }
     if (sizeof($options['include'])) foreach($options['include'] as $inc_file)     $_content[]='   include ('.$inc_file.');' ;
     if (sizeof($options['options'])) foreach($options['options'] as $name=>$value) $_content[]='   $options[\''.$name.'\']='.$value.' ;' ;
   	 if (sizeof($options['text']))    foreach($options['text'] as $text)            $_content[]='   '.$text ;
     else
     if (sizeof($options['options'])) $_content[]='   show_page($options);' ;
   	                             else $_content[]='   show_page();' ;

     if (!$options['text_content']) $_content[]='?>' ;
   	 $content=implode("\n",$_content) ;
     $cnt=file_put_contents($patch_fname,$content) ;
     echo ($cnt)? '<span class=green> - OK</span>':'<span class=red> - ERROR</span>' ;
     echo '<br>' ;
   }
   else
   {  $patch_fname=$dir_to.$fname ;
      echo 'Создаем скрипт: <strong>'.hide_server_dir($patch_fname).'</strong>' ;
      $content=implode("\n",$options['text']) ;
      $cnt=file_put_contents($patch_fname,$content) ;
      echo ($cnt)? '<span class=green> - OK</span>':'<span class=red> - ERROR</span>' ;
      echo '<br>' ;
   }
 }

 //-----------------------------------------------------------------------------------------------------------------------------
 // Инсталяция движка на пустую базу - www.hostname.ru/ini/setup.php
 //-----------------------------------------------------------------------------------------------------------------------------

 // используется тока в режиме инсталяции движка
 function create_table_by_pattern($pattern)
 {  global $install_options ;
    //damp_array($pattern) ; echo '<br>' ;
	if (sizeof($install_options) and !$install_options['install_create_table']) return($_SESSION['list_db_tables'][$pattern['table_name']]) ;

    echo '<h3>Создаем таблицу '.$pattern['table_name'].'</h3>' ;
    if (!isset($_SESSION['list_db_tables'][$pattern['table_name']]))
 	{ if ($pattern['table_name'])
 	  { // создаем запись в DOT по таблице
   		$id=create_DOT_table($pattern) ;
        // создаем таблицу в базе данных
        create_object_table($id,$pattern['table_name'],$pattern['table_title'],$pattern['use_clss'],$pattern['def_recs']) ;

   	    // создаем запись в DOT по поддерживаемым классам
   		create_DOT_clss($id,$pattern['use_clss']) ;
	  }
	  else  { echo '<span class=red>Не указано имя таблицы, pattern=' ; print_r($pattern)  ; echo '</span><br>' ; }
	} else 	{ echo 'Таблица <strong>'.$pattern['table_name'].'</strong> уже существует<br>' ;
			  check_table_clss_list($_SESSION['list_db_tables'][$pattern['table_name']],$pattern['use_clss']) ;
             return($_SESSION['list_db_tables'][$pattern['table_name']]) ;
			}
	//echo '<br>' ;
	return($id) ;
 }

 function check_table_clss_list($tkey,$str_clss)
 { $set_clss=explode(',',$str_clss) ;
   $list_exists_clss=execSQL_line('select indx from obj_descr_obj_tables where clss=104 and parent='.$tkey) ;
   echo 'Проверка поддержки заданных классов<br>' ;
   if (sizeof($set_clss)) foreach($set_clss as $clss)
    //if (!isset(_DOT($tkey)->list_clss[$clss]))
    if (!in_array($clss,$list_exists_clss)) // 7.05.2011 - изменяем проверку по модели на проверку по базе - так как при инсталяции модель не всегда корректа
     { echo '<div class=red>Класс ['.$clss.'] - не поддерживается, будет добавлен</div>' ;
       create_DOT_clss($tkey,$clss) ;
     }
 }



function config_modules()
 {
   global $__functions;
   $func_name='' ;
   ?><h2>Конфигурирование модулей</h2><?
   if (sizeof($__functions['config'])) foreach($__functions['config'] as $func_name) $func_name() ;
 }

// скачиваем с сервера и распаковываем модули AddOns
function install_AddOns($use_AddOns)
{ //damp_array($use_AddOns) ;
  if (sizeof($use_AddOns)) foreach($use_AddOns as $rec) SETUP_get_file('/AddOns/'.$rec,'/AddOns/'.$rec) ;
  include(_DIR_TO_ENGINE.'/AddOns/pclzip.lib.php') ;
  if (sizeof($use_AddOns)) foreach($use_AddOns as $rec) if (file_exists(_DIR_TO_ROOT.'/AddOns/'.$rec) and strpos($rec,'.zip')!==false)
  {  $dir_name=str_replace('.zip','',$rec) ;
     if (file_exists(_DIR_TO_ROOT.'/AddOns/'.$dir_name)) echo 'Плагин <strong>'.$rec.'</strong> уже распакован в <strong>/AddOns/'.$dir_name.'</strong><br>' ;
     else
     {  echo 'Распаковываем модуль <strong>'.$rec.'</strong><br>' ;
        $obj_pclzip = new PclZip(_DIR_TO_ROOT.'/AddOns/'.$rec) ;
        $file_list=$obj_pclzip->extract(_DIR_TO_ROOT.'/AddOns/') ;
        if ($file_list!=0) echo 'Извлечено '.sizeof($file_list).' файлов<br>' ;
        else echo '<div class=alert>Не удалось распаковать архив</div>' ;
     }
     unlink(_DIR_TO_ROOT.'/AddOns/'.$rec) ;
  }

}

function cmd_exe_apply_import_from_xml($file_name,$options=array())
{
    import_setting_from_xml($file_name,$options) ;
}

function import_setting_from_xml($file_name='',$options=array())
{ global $install_options ;
  if (sizeof($install_options) and !$install_options['install_XML_export']) return ;

  $source=file_get_contents($file_name) ;
  $dom = new DomDocument();
  if ($dom->loadXML($source))
  { include_once(_DIR_ENGINE_EXT.'/XML/import_OBJ_from_XML.php') ;
    $options['to_obj_reffer']=$GLOBALS['obj_info']['_reffer'] ;
    list($all_count,$sum_count)=import_OBJ_from_XML($dom,$options) ;
    echo '<strong>Импортировано '.$all_count.' из '.$sum_count.' объектов.</strong><br>' ;
  }
  else echo 'Неверный формат XML' ;
}


 //-----------------------------------------------------------------------------------------------------------------------------
 // РАБОТАЕМ С НАБОРОМ ТАБЛИЦ
 // описание классов
 // 101 - объектная таблицы
 // 106 - индексная таблицы
 // 108 - журнальная таблица
 // 103 - набор таблиц
 // 104 - поддерживаемый класс таблицы
 // 105 - клон изображения
 // 107 - описание поля для редактирования (класс,тип,размер)
 //-----------------------------------------------------------------------------------------------------------------------------

 // создаем запись в DOT для объектной таблицы
 // parent - объект, которому принадлежит таблица
 // table_clss - класс таблицы: 101,106,108
 // table_title - название таблицы (по назначению)
 // table_name - название таблицы (в базе данных)
 // dir - директория файлов таблицы
 function create_DOT_table($options)
 {  if (!$options['table_parent']) $options['table_parent']=1 ;
    if (!$options['table_clss'])   $options['table_clss']=101 ;
    if (!$options['table_title'])  $options['table_title']='Новая таблица' ;
    if (!$options['table_name'])   $options['table_name']='obj_site_'.rand(1,100) ;

    $finfo=array(	'parent'		=>  $options['table_parent'],
					'clss'			=>	$options['table_clss'],
					'obj_name' 		=> 	$options['table_title'],
					'table_name'	=>  $options['table_name'],
					'dir_to_image'	=>  $options['dir']
		  		) ;
	//damp_array($finfo) ;
 	$DOT_id=adding_rec_to_table('obj_descr_obj_tables',$finfo) ;
 	echo 'DOT: '.$DOT_id.' - <strong>\''.$options['table_name'].'\'</strong><br/>' ;
 	return($DOT_id) ;
 }

 // создаем запись в DOT для поддерживаемого класса
 function create_DOT_clss($parent,$clss)
 { //echo '$clss='.$clss.'<br>';
   //echo 'Текущий список поддерживаемых классов: ' ; print_r(_DOT($parent)->list_clss) ; echo '<br>' ;
   $list_exists_clss=execSQL_line('select indx from obj_descr_obj_tables where clss=104 and parent='.$parent) ;
   if (!$clss) { echo 'Не указан код класса для добавления в DOT<br>' ; return ; }
   $list_clss=explode(',',$clss) ; //damp_array($list_clss) ;
   if (sizeof($list_clss)) foreach($list_clss as $clss) if (!in_array($clss,$list_exists_clss))
   	 { $finfo=array('parent'=>$parent,'clss'=>104,'indx'=>$clss,'obj_name'=>_CLSS($clss)->name) ;
   	   //print_r($finfo) ; echo '<br>' ;
       $DOT_id=adding_rec_to_table('obj_descr_obj_tables',$finfo) ;
	   echo 'DOT: '.$DOT_id.' - поддержка класса <strong>'.$clss.'</strong> для DOT '.$parent.'</strong><br/>' ;
   	 }
   return($DOT_id) ;
 }

 // функция объединяет создание записей в DOT, создание таблицы в БД, обновление модели и проверку таблицы
 // title - наименование
 // table_name - название таблицы
 // type - тип таблицы: 101 -объектная, 106 - индексная, 108 - журнал
 // mode - режим таблицы: main, out
 // use_clss - поддерживаемые классы
 // pattern - шаблон таблицы

 function create_DOT_object_table($options)
 {
   // создаем запись в DOT для новой таблицы
   $id=create_DOT_table($options) ;
   // создаем запись в DOT по поддерживаемым классам
   create_DOT_clss($id,$options['use_clss']) ;
   // обновляем модель таблиц - в ней уже будет вся инфоримация по поддерживаемым классам
   create_model_obj_tables(array('use_tkey_table'=>$id,'no_describe_tables'=>1)) ;
   // создаем физическую таблицу в базе данных по описанию в DOT
   create_object_table_by_DOT($id,$options['def_recs']) ;
   // делаем проверку таблицы
   include_extension('table/table_checked') ;
   table_checked($id) ;

   return($id)  ;
 }

 // создать системную таблицу по записи в DOT
 // для таблицы уже должно существовать описание в DOT, tkey - код описания
 // через $def_recs можно передать набор записей, добавляемых по умолчанию в таблицу
 function create_object_table_by_DOT($id,$def_recs=array())
	 {
	   $table_obj=_DOT($id) ; if (!is_object($table_obj)) {echo '<div class=alert>Объект DOT с кодом '.$id.' не существует<br>';}
	   $table_name=$table_obj->table_name ; if (!$table_name) {echo '<div class=alert>Не указано имя таблицы для объекта DOT с кодом '.$id.'<br>';}
  	   $table_title=$table_obj->name ;
	   // собираем в массив все поля объектов, которые должны присутствовать в таблице
	   // !!! конфликтность на одноименные полей различных классов (несоответствие типов) должна проверяться при назначении классов таблице
	   //damp_array($table_obj->list_clss_ext) ;
	   // в таблице должны присутствовать только поля тех классов, которые в list_clss_ext прописаны в поле main для данной таблицы
	   if (sizeof($table_obj->list_clss_ext)) foreach($table_obj->list_clss_ext as $clss=>$clss_info) if ($clss_info['main']==$id) $arr_clss[]=$clss ;
       if (sizeof($arr_clss)) $use_clss=implode(',',$arr_clss) ;

	   create_object_table($id,$table_name,$table_title,$use_clss,$def_recs) ;
	 }



?>
