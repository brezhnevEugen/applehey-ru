$j(document).ready(function ()
{ // назначаем обработчики событий для панели вывода записей списком
  //$j('div.list_item input.fast_search').live('keyup',div_list_item_input_fast_search_keyup) ;
  //$j('table.list tr.td_header td').live('dblclick',table_list_tr_header_td_dblclick) ;
  $j('table.list tr.td_header td').live('click',table_list_tr_header_td_click) ;

  // отработка команд - кнопок и ссылок
  $j('input[type="submit"]').live('click',button_submit_on_click) ;  // для <button .....>
  //$j('button:not(.ajax)').live('click',button_on_click) ;  // для <button .....>
  $j('button[cmd]:not(.v2)').live('click',button_on_click) ; // для <button cmd=.....>,<a cmd=....>,<div cmd=....>,<span cmd=....>
  $j('button[cmd="delete_check_objs"]').live('click',button_on_click) ; // для <button cmd=.....>,<a cmd=....>,<div cmd=....>,<span cmd=....>
  $j('button[func]:not(.v2)').live('click',button_on_click) ; // для <button cmd=.....>,<a cmd=....>,<div cmd=....>,<span cmd=....>
  $j('a[cmd]:not(.v2)').live('click',button_on_click) ; // для <button cmd=.....>,<a cmd=....>,<div cmd=....>,<span cmd=....>
  $j('a[func]:not(.v2)').live('click',button_on_click) ; // для <button cmd=.....>,<a cmd=....>,<div cmd=....>,<span cmd=....>
  $j('a[action="new_window"]').live('click',onclick_to_new_window) ; // для <button cmd=.....>,<a cmd=....>,<div cmd=....>,<span cmd=....>
  //$j('div[cmd]').live('click',button_on_click) ; // div нельзя - так как в них часто передается cmd
  $j('div.cmd[cmd]').live('click',button_on_click) ; // div.cmd можно
  $j('img[cmd]:not(.v2)').live('click',button_on_click) ; // для <button cmd=.....>,<a cmd=....>,<div cmd=....>,<span cmd=....>
  $j('span[cmd]:not(.v2)').live('click',button_on_click) ; // для <button cmd=.....>,<a cmd=....>,<div cmd=....>,<span cmd=....>

  // назначем обработчики для быстрого редактирования ячеек
  $j('td[el="gm"]').live('click',onclick_gm);
  $j('td[el="gi"]').live('click',onclick_gi);
  $j('td[el="gs"]').live('click',onclick_gs);
  $j('td[el="gc"]').live('click',onclick_gc);
  $j('td[el="gc2"]').live('click',onclick_gc2);
  $j('td[el="gmc"]').live('click',onclick_gmc);
  $j('td[el="gts"]').live('click',onclick_gts);
  $j('td.to_edit').live('click',onclick_to_edit);
  $j('div.to_edit').live('click',onclick_to_edit);
  $j('td.to_save').live('click',onclick_to_save);
  $j('table.list tr td input[type="text"]').live('keydown',function(e){if(e.keyCode==13) { $j(this).closest('tr').children('td.to_save').click() ; f_preventDefault(e); return false;}}) ; // поиск по нажатию на enter

  // клик по "отметить все/снять все"
  $j('table input[name="check_all"]').live('click',onclick_check_all) ;
  $j('table td input[name^="_check"]').live('click',onclick_td_check2) ;
  $j('table td:has(input[name^="_check"])').live('click',onclick_td_check) ;

  // при прокрутке фрейма подгружать следующие страницы
  $j(window).scroll(function() { check_next_page_background_upload() ; }) ;
  $j(window).resize(function() { check_next_page_background_upload() ; }) ;
  $j('div.next_page input.view_page').live('click',check_next_page_background_upload) ;

  // показ скрытых панелей
  $j('div.expand_panel').live('click',function() {$j(this).next('div.panel').toggleClass('hidden') ;}) ;
  $j('a.show_hide_panel').live('click',function(){$j(this).parent().next().toggleClass('visible');return(false);});
  $j('div.show_hide_panel').live('click',function(){$j(this).next().toggleClass('visible');});

  // панель быстрого поиска
  $j('div#panel_search img.go_search').live('click',panel_search_go_search_on_click) ; // поиск по нажатию на кнопку поиска
  $j('div#panel_search input[name="text_search"]').live('keydown',function(e){if(e.keyCode==13) { panel_search_go_search_on_click(); f_preventDefault(e); return false;}}) ; // поиск по нажатию на enter
  $j('div#panel_search input[name="target_search"]').live('change',panel_search_go_search_on_click) ; // после смены источника поиска запускам поиск заново
  $j('div#panel_search input[name="orders_status"]').live('change',panel_search_go_search_on_click) ; // после смены источника поиска запускам поиск заново

  // фрейм дерева каталога
  $j('body#tree_objects li').prepend('<div class=expand></div><div class=clss></div>') ; // добавляем иконку узла дерева и иконку класса
  $j('body#tree_objects li').each(tree_fill_count) ; // проставляем кол-во по фактическому наполнению дерева, это будет влиять на показ "+" для раскрытия веток
  $j('body#tree_objects li[open]').addClass('open') ; // добавляем класс "open" для элементов имеющих атрибут "open"
  $j('body#tree_objects li div').live('click',tree_div_expand_click) ; // назначаем обработчик события - клик по любой иконке => раскрытие узла
  $j('body#tree_objects li div').live('touchstart',tree_div_expand_click) ; // назначаем обработчик события - клик по любой иконке => раскрытие узла

  //$j('body#tree_objects li div').on( "touchstart", function(){$j(this).remove();});

  $j('body#tree_objects li').live('click',tree_item_click) ; // назначаем обработчик события - клик по названию элемента => загрузка второго фрейма
  $j('body#tree_objects li').live('touchstart',tree_item_click) ; // назначаем обработчик события - клик по названию элемента => загрузка второго фрейма
  $j('body#tree_objects > ul > li:first-child').addClass('open').addClass('selected') ; // открываем корень дерева и выделяем его
  $j('body#tree_objects div.reload_cur_item').live('click',reload_cur_item) ;

   new jBox('Confirm', {confirmButton: 'Да',cancelButton: 'Нет'});




});

/*********************************************************************************
 *
 * показ inline-редактора для полей
 *
//********************************************************************************/

$j.fn.inline_editor = function()
  { $j('.iedit').each(function(){  var reffer=$j(this).attr('reffer') ;
                                   if (reffer==undefined) { reffer=$j(this).closest('[reffer]').attr('reffer') ;
                                                            $j(this).attr('reffer',reffer) ;
                                                          }
                                   //var div_parent=$j(this).parent() ;
                                   // $j(div_parent).addClass('fed') ;
                                   // $j(div_parent).attr('fname',$j(this).attr('fname')) ;
                                   // $j(div_parent).attr('reffer',$j(this).attr('reffer')) ;
                                   // $j(this).remove() ;
                                  }) ;

    $j('.iedit[fname][reffer]').each(function(){$j(this).attr('contenteditable','true')}) ;

    CKEDITOR.on( 'instanceCreated', function( event )
    {
        var editor = event.editor,
        element = editor.element;
        editor.config.toolbarGroups = [] ;

        editor.config.toolbar = [
            { name: 'document', items: [ 'Source', '-', 'NewPage', 'Preview', '-', 'Templates' ] },
            { name: 'clipboard', items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
            '/',
            { name: 'basicstyles', items: [ 'Bold', 'Italic' ] }
        ];

        // Customize editors for headers and tag list.
        // These editors don't need features like smileys, templates, iframes etc.
        if ( element.getAttribute( 'mode' ) == 'easu' )
        {
            // Customize the editor configurations on "configLoaded" event,
            // which is fired after the configuration file loading and
            // execution. This makes it possible to change the
            // configurations before the editor initialization takes place.
            editor.on( 'configLoaded', function()
            {   // Remove unnecessary plugins to make the editor simpler.
                editor.config.removePlugins = 'colorbutton,find,flash,font,' +
                    'forms,iframe,image,newpage,removeformat,' +
                    'smiley,specialchar,stylescombo,templates';
                alert(1) ;
                // Rearrange the layout of the toolbar.
                editor.config.toolbarGroups = [] ; //
                //    { name: 'undo' },
                //    { name: 'clipboard',	groups: [ 'selection', 'clipboard' ] },
                //    { name: 'about' }
                //];
            });
        }
        /*
        if ( element.is( 'h1', 'h2', 'h3' ) || element.getAttribute( 'id' ) == 'taglist' )
        {
            // Customize the editor configurations on "configLoaded" event,
            // which is fired after the configuration file loading and
            // execution. This makes it possible to change the
            // configurations before the editor initialization takes place.
            editor.on( 'configLoaded', function() {

                // Remove unnecessary plugins to make the editor simpler.
                editor.config.removePlugins = 'colorbutton,find,flash,font,' +
                    'forms,iframe,image,newpage,removeformat,' +
                    'smiley,specialchar,stylescombo,templates';

                // Rearrange the layout of the toolbar.
                editor.config.toolbarGroups = [
                    { name: 'editing',		groups: [ 'basicstyles', 'links' ] },
                    { name: 'undo' },
                    { name: 'clipboard',	groups: [ 'selection', 'clipboard' ] },
                    { name: 'about' }
                ];
            });
        }   */

        editor.on('blur',function( e )
        {
          var params = {} ;
          params['cmd']='set_value';
          var el=editor.container ;
          params['reffer']=$j(el).attr('reffer');
          params['fname']=$j(el).attr('fname');
          params['value']=editor.getData();

          $j.ajax({url:'ajax.php',type:'POST',dataType:'xml',cache:false,data:params});

        });
    });
  };

/*********************************************************************************
 *
 * отработка действий кнопок
 *
//********************************************************************************/

function button_submit_on_click()
{
  var list_item=$j(this).closest('div.list_item') ;
  var list_id=$j(list_item).attr('list_id') ;
  if (list_id!=undefined) $j(this).attr('list_id',list_id) ;

  var form=$j(this).closest('form') ;
  if (form.length>0) convert_tags_to_hiddend_vars(this,form);
}

// подгрузка всех фото объекта для показа в HS
function upload_image_to_hs()
{ var reffer=$j(this).closest('[reffer]').attr('reffer') ;
  $j.ajax({url:'ajax.php',dataType:'xml',type:'POST',cache:false,data:{cmd:upload_image_to_hs,reffer:reffer},success: function (data,status,link) {if (status=='success') mForm_Submit_AJAX_onSuccess(link.responseXML);}}) ;
}

// перезагрузка одной строки таблицы
function reload_tr_rec(reffer)
{ var tr=$j('[reffer="'+reffer+'"]') ; // объект - строка таблицы
  var list_item=$j(tr).closest('div.list_item') ; // рабочий блок
  var data= {cmd:'get_obj_tr_rec',list_id:$j(list_item).attr('list_id'),clss:$j(list_item).attr('clss'),reffer:reffer,no_icon_edit:$j(list_item).attr('no_icon_edit'),no_view_field:$j(list_item).attr('no_view_field'),read_only:$j(list_item).attr('read_only'),no_icon_photo:$j(list_item).attr('no_icon_photo'),no_check:$j(list_item).attr('no_check')} ;
  ajax_send_data(data) ; // вызов ajax.php, success -  mForm_Submit_AJAX_onSuccess
}

// нажатие кнопки
// кнопка должна быть внутри формы
// если формы нет - она будет создана, и кнопка будет помещена внуть этой формы. Новая форма будет работать через ajax
function button_on_click()
 {
     if ($j(this).hasClass('view_page')) { return false ; }
     $j(this).addClass('ok') ;
   // в завистимости от выбранного в mode
   var mode=$j(this).attr('mode') ;
   var cmd=$j(this).attr('cmd') ;
   var func=$j(this).attr('func') ;
   var script=$j(this).attr('script') ;

   $j(this).removeClass('button_green') ;

   // если кнопка ноходиться внутри блока-списка или сразу за ним, перевеодим в переменные также атрибуты блока-списка
   var div_list_item=$j(this).closest('div.list_item') ;
   if (div_list_item.length==0) div_list_item=$j(this).prev('div.list_item') ;

   // подготавливаем данные формы для передачи через AJAX
   // если кнопка не в форме, создаем эту форму - по умолчанию она будет отправлять на ajax  - это неправильно, на ajax должно отправлять v2
   var form=$j(this).closest('form') ;
   if (form.length==0)
   {  if (div_list_item.length==0) { $j(this).wrapInner('<form  method="POST" action="ajax.php"></form>') ; form=$j(this).closest('form') ; } // если блока-списка нет - просто оборачиваем кнопку в форму
      else                         { $j(div_list_item).wrapInner('<form  method="POST" action="ajax.php"></form>') ; form=$j(div_list_item).children('form') ; } // иначе - оборачиваем в форму блок-список
   }


   // переводим все аттрибуты кликнутого элемента в скрытые переменные
   // добавляются после кнопки. По идее надо добавлять именно в форму, в которой находиться таблица
   convert_tags_to_hiddend_vars(this,form);
   //for(var j=0; j<this.attributes.length; j++)
   //{ $j('input[name="'+this.attributes.item(j).nodeName+'"]').remove() ;
   //  $j(form).append('<input type="hidden" class=temp name="'+this.attributes.item(j).nodeName+'" value="'+this.attributes.item(j).nodeValue+'">') ;
   //}
   // сохраняем содержимое радектора в форме
   //if (editor)  { $j('textarea[name="'+editor.name+'"]').val(editor.getData()) ;  }


   if (div_list_item.length!=0)
   { $j(form).append('<input type="hidden" class=temp name="list_id" value="'+$j(div_list_item).attr('list_id')+'">') ;
     $j(form).append('<input type="hidden" class=temp name="read_only" value="'+$j(div_list_item).attr('read_only')+'">') ;
     $j(form).append('<input type="hidden" class=temp name="no_icon_edit" value="'+$j(div_list_item).attr('no_icon_edit')+'">') ;
     $j(form).append('<input type="hidden" class=temp name="no_icon_photo" value="'+$j(div_list_item).attr('no_icon_photo')+'">') ;
     $j(form).append('<input type="hidden" class=temp name="no_view_field" value="'+$j(div_list_item).attr('no_view_field')+'">') ;
     $j(form).append('<input type="hidden" class=temp name="no_check" value="'+$j(div_list_item).attr('no_check')+'">') ;
   }
                                    //IXPfkTSq
   var submit_form=false ;
   switch(cmd)
   { case 'go_url':    document.location=$j(this).attr('url') ; break ;
     case 'save_list': if (mode=='tr_update')
                       { //var div_list=$j(this).closest('div.list_item') ;
                         //if (div_list.length==0) div_list=$j(this).prev('div.list_item') ;
                         //form=$j(div_list).children('form') ;
                         //if (form.length==0) { $j(div_list).wrapInner('<form  method="POST" action="ajax.php"></form>') ; form=$j(div_list).children('form') ; }
                         $j.each(CKEDITOR.instances,function(){this.updateElement()})  ;
                         $j(form).ajaxForm({url:'ajax.php',dataType:'xml',success:function(data){ mForm_Submit_AJAX_onSuccess(data) ;}});
                       }
                       submit_form=true ;
                       break ;
     default:          submit_form=true ;
                       switch(mode)
                          { case 'ajax':
                            case 'modal_window':
                            case 'replace_viewer_main':
                            case 'append_viewer_main':  // если в элементе стоит send=tags
                                                        if ($j(this).attr('send')=='tags')
                                                        { var params=convert_tag_attributes_to_array(this) ; // alert(params) ;
                                                          $j.ajax({url:'ajax.php',type:'POST',dataType:'xml',cache:false,success:function(data){ mForm_Submit_AJAX_onSuccess(data)},data:params});
                                                          submit_form=false ;
                                                        }
                                                        // подготоваливаем форму для отпрвки через AJAX
                                                        // http://www.malsup.com/jquery/form/#options-object
                                                        // единственный недостаток - в случае ошибки не будет правильно сформирован  ответный xml, в результате не будет показан ответ
                                                       else $j(form).ajaxForm({url:'ajax.php',type:'POST',dataType:'xml',success:function(data){ mForm_Submit_AJAX_onSuccess(data) ;}});/*функция mForm_Submit_AJAX_onSuccess разрулит вывод в зависисмости от значения mode*/
                                                       break ;
                            case 'give_file':          $j(form).attr('action','/admin/give_file.php') ; break ;
                            case 'send_form':          break ;  // просто отправляем форму
                            // отработка обычной команды с перегрузкой формы
                            default:                   switch(cmd)
                                                       { case 'delete_check':
                                                         case 'delete_check_objs': $j(form).ajaxForm({url:'ajax.php',dataType:'xml',success:function(data){ mForm_Submit_AJAX_onSuccess(data) ;}});
                                                                                   break ;
                                                         case 'delete_obj':        submit_form=confirm('Вы действительно хотите удалить данный объект?') ; break ;
                                                       }
                          }
   }

   if (submit_form) {  $j(form).submit() ;
                       if ($j(this).attr('disabled_alter_click')==1) $j(this).attr('disabled',true) ;
                    }

   // удаляем все созданные временные скрытые переменные
   //$j('input[type="hidden"].temp').remove() ;

   return(false) ; // блокировка работы <a>
 }


function ajax_save_data(div_list)
{

}

// отправляем данные через AJAX
function ajax_send_data(data)
{
   $j.ajax({url:'ajax.php',dataType:'xml',type:'POST',cache:false,data:data,success: function (data,status,link) {if (status=='success') mForm_Submit_AJAX_onSuccess(link.responseXML);}}) ;
}

/*********************************************************************************
 *
 * старые обработчики действий кнопок - по мере чистки кода будут удалены
 *
//********************************************************************************/

function exe_cmd(cmd,pkey,tkey)
  { document.getElementById('cmd').value=cmd ;
    if (pkey!=undefined) document.getElementById('pkey').value=pkey ;
    if (tkey!=undefined) document.getElementById('tkey').value=tkey ;
    window.document.forms.form.submit() ;
  }
  /*
function give_file(cmd,pkey,tkey)
  {   return ;
      $j('form#form').attr('action','/admin/give_file.php') ;
    document.getElementById('cmd').value=cmd ;

    if (pkey!=undefined) document.getElementById('pkey').value=pkey ;
    if (tkey!=undefined) document.getElementById('tkey').value=tkey ;
    window.document.forms.form.submit() ;
  } */

function exe_cmd_show(cmd,pkey,tkey)
 { document.getElementById('func_show').value=cmd ;
   if (pkey!=undefined) document.getElementById('pkey').value=pkey ;
   if (tkey!=undefined) document.getElementById('tkey').value=tkey ;
   window.document.forms.form.submit() ;
 }

 function exe_cmd_dialog_params(cmd,param)
 { document.getElementById('cmd').value=cmd ;
   if (param!=undefined) document.getElementById('dialog_param').value=param ;
   window.document.forms.form.submit() ;
 }

/*********************************************************************************
 *
 * mForm
 *
//********************************************************************************/

// переменосим параметры из <input type=submit......> в сткрытые переменные перед отправкой формы по AJAX
// вызывается из mForm.Submit.js
function convert_tags_to_vars(input,form)
{  // переводим все аттрибуты кликнутого элемента в скрытые переменные
   //for(var j=0; j<input.attributes.length; j++) $j(form).append('<input type="hidden" name="'+input.attributes.item(j).nodeName+'" value="'+input.attributes.item(j).nodeValue+'">') ;
   // сохраняем содержимое радектора в форме
   //if (editor)  { $j('textarea[name="'+editor.name+'"]').val(editor.getData()) ;  }
   $j.each(CKEDITOR.instances,function(){this.updateElement()})  ;
   //
   //removeEditor() ;
}

// возвращем массив с атрибутами тега
 // просто вернуть .attributes нельзя, так как в IE в этом свойсве хранятся, помимо атрибутов, также и DOM-свойства объекта. Другого места, где находились бы атрибуты тега, в IE нет.
 // поэтому для выделения тегов разбираем  outerHTML тега
 // можно разобрать outerHTML через регулярное выражение /\b((\w+)=(["'])([^"']+)["'])/ig  при условии, что все значения тегов заключены в ковычки
 // но и тут IE отличился -  его outerHTML не заключает в ковычки атрибуты id,name,value: <input id=test class="demo my input" reffer="1111" cmd="222/888" name=a333 value=444s>
 // что сильно усложняет подбор регулярного выражения
 // в итоге был выбран промежуточный вариант - имена тегов получаем из outerHTML, а значения тегов из .attributes
 function convert_tag_attributes_to_array(tag_obj)
 {   var arr_attr= {} ;
     var text=tag_obj.outerHTML ;
     var myRe = /\b((\w+)=)/ig;
      while ((myArray = myRe.exec(text)) != null)
      {   var name = myArray[2] ;
          var attr=tag_obj.attributes.getNamedItem(name) ;
          if (attr!=null) arr_attr[name] = attr.value ;
   }
    return arr_attr ;
}

// переменосим параметры из <input type=submit......> в сткрытые переменные перед отправкой формы по AJAX
// вызывается из mForm.Submit.js
function convert_tags_to_hiddend_vars(input,form)
{  // удаляем старые временные переменные, если форма отправляется повторно
   $j(form).find('input.temp[type="hidden"]').remove() ;
   // переводим все аттрибуты кликнутого элемента в скрытые переменные
   //ВНИМАНИЕ! в IE в input.attributes будут находиться все свойства объекта, а не только атрибуты тега
   var arr_attr=convert_tag_attributes_to_array(input) ;
   for (key in arr_attr)
       $j(form).append('<input type="hidden" class="temp" name="'+key+'" value="'+arr_attr[key]+'">') ;
   {   //$j(form).append('<input type="hidden" class="temp" name="'+key+'" value="'+arr_attr[key]+'">') ;
       //var temp_input = new Element('input', {'type': 'hidden','class': 'temp','name': key,'value':arr_attr[key]});
       //temp_input.inject(form) ;
   }
   //$j(form).reInit=1 ;
   //for(var j=0; j<input.attributes.length; j++) $j(form).add($j('<input type="hidden" class="temp" name="'+input.attributes.item(j).nodeName+'" value="'+input.attributes.item(j).nodeValue+'">').get()) ;
   // сохраняем содержимое радектора в форме
   //if (editor)  { $j('textarea[name="'+editor.name+'"]').val(editor.getData()) ;  }
   //if (CKEDITOR!=undefined && CKEDITOR.length) $j.each(CKEDITOR.instances,function(){this.updateElement()})  ;
   //
   //removeEditor() ;
}


// обрабатываем ответ сервера по AJAX после отравки на AJAX формы с проверкой validate
// в ответе будет параметр "mode" - каким образов выводить ответ AJAX
// вызывается:
// 1. По событию "onSuccess"  на AJAX в mForm.validate
// 2  По событию "success" на AJAX в button.onclick
// 2  По событию "to+save" на AJAX в button.to_save
function mForm_Submit_AJAX_onSuccess(responseXML)
{  if (responseXML!=undefined)
   { var html = get_xml_tag_value(responseXML,'html');
     var title = get_xml_tag_value(responseXML,'title');
     var mode = get_xml_tag_value(responseXML,'mode');
     //var tr_content = get_xml_tag_value(responseXML,'tr_content');
   }
   else
   { var html=responseXML ;
     var title="<div class=alert>Ошибка</div>" ;
     var mode="append_viewer_main" ;
   }

   var data=convert_XML_to_obj(responseXML) ;

   var list_id = get_xml_tag_value(responseXML,'list_id');
   var parent_reffer = get_xml_tag_value(responseXML,'parent_reffer');
   var table_update=0 ;
   var flag_child_reload=0 ;
   if (top.fra_tree!=undefined) var fra_tree=top.fra_tree.document ;

   // создем новое окно
   if (data.new_window)
   {
       parent.window.open(data.new_window,'obj_show_new','status=1,dependent=1,width=800,height=600, resizable=1, scrollbars=yes');
   }


   // обновляем или добавляем переданные строки
   var tr_content = responseXML.getElementsByTagName('tr_content');
   if (tr_content.length && list_id!=undefined && list_id!="") for (var i=0; i<tr_content.length; i++)
    { var attr_XML=tr_content[i].attributes;
      var reffer = attr_XML.getNamedItem("reffer").nodeValue;
      var dat=($j.browser.msie)? tr_content[i].text:tr_content[i].textContent ;
      if ($j('div#'+list_id+' [reffer="'+reffer+'"]').length) { $j('div#'+list_id+' [reffer="'+reffer+'"]').replaceWith(dat) ; table_update=1 ; }
      else if ($j('div#'+list_id+' table.list').length)         { $j('div#'+list_id+' > table.list').append(dat) ; table_update=1 ; }
      //else if ($j('div#'+list_id+' table.list').length)         { $j('div#'+list_id+' > table.list').prepend(dat) ; table_update=1 ; }
      // меняем данные в дереве
      if (fra_tree!=null)
      {   var tree_li=$j(fra_tree).contents().find('li[reffer="'+reffer+'"]') ;
          if (tree_li.length) // если объект из таблицы уже есть в дереве
          {  var name = attr_XML.getNamedItem("name").nodeValue;
             $j(tree_li).children('span.name').html(name) ; // обновляем имя объекта
          }
          else flag_child_reload=1 ; // если объекта нет - добавляем новый узел в дерево путем перезагрузки родительского узла - ставим флаг, перезагрузка будет после обрабротки всех элементов
      }
    }

    if (fra_tree!=null && flag_child_reload==1 && parent_reffer!="")
    { var tree_li=$j(fra_tree).contents().find('li[reffer="'+parent_reffer+'"]') ;
      tree_item_expand(tree_li,1) ; // обновит набор потомков и изменит кол-во в родителе
    }

   // удаляем отмеченные для удаления строки
   var remove_rows = responseXML.getElementsByTagName('remove_rows');
   if (remove_rows.length && list_id!=undefined && list_id!="") for (var i=0; i<remove_rows.length; i++)
    { var attr_XML=remove_rows[i].attributes;
      var reffer = attr_XML.getNamedItem("reffer").nodeValue;
      if ($j('div#'+list_id+' [reffer="'+reffer+'"]').length) { $j('div#'+list_id+' [reffer="'+reffer+'"]').remove() ; table_update=1 ; }
      // удаляем отмеченные узлы из дерева
      var tree_li=$j(fra_tree).contents().find('li[reffer="'+reffer+'"]') ;
      if (tree_li.length)
      { // изменяем кол-во в родительском элементе
        var parent_li=$j(tree_li).closest('ul').prev('li') ;
        var cnt=$j(parent_li).attr('cnt') ;
        cnt-- ;
        if (cnt>0) $j(parent_li).attr('cnt',cnt).children('span.cnt').html('('+cnt+')') ;
        else       $j(parent_li).removeClass('open').removeAttr('cnt').children('span.cnt').html('') ;
        // удаляем элемент
        $j(tree_li).remove() ;
      }

    }

    if (data.show_notife)  show_notife2(responseXML);

     var cmd = get_xml_tag_value(responseXML,'cmd');
     switch (cmd)
     { case 'create_obj':              var clss = get_xml_tag_value(responseXML,'clss');
                                       var no_redirect_to_list_items = get_xml_tag_value(responseXML,'no_redirect_to_list_items');
                                       // если после создания возвращена запись по объекту но не была обновлена таблица со списком записей - перегружаем страницу
                                       if (tr_content.length && !table_update && !no_redirect_to_list_items) document.location='fra_viewer.php?obj_reffer='+parent_reffer+'&menu_id=structure_'+clss ;
                                       if (tr_content.length) show_notife3({content:'Создан новый объект'}) ;
                                       break ;
       case 'create_obj_after_dialog': if (tr_content.length) show_notife3({content:'Создан новый объект'}) ;
                                       break ;
       case 'delete':                  show_notife3({content:'Удалено'}) ;

       /* пока не используется - не удалось нормально настроить HS
       case 'upload_image_to_hs':      $j('tr[reffer="'+parent_reffer+'"] td.img_preview').append(html) ;
                                       var a=$j('tr[reffer="'+parent_reffer+'"] td.img_preview a[cmd]') ;
                                       var group='group_'+get_xml_tag_value(responseXML,'pkey') ;
                                       hs.expand(null,{src:$j(a).attr('href'),slideshowGroup:group}) ;
                                       break ;
       */
     }

   switch (mode)
   { case 'replace_viewer_main': removeEditor() ;
                                 $j('div#viewer_main').empty() ;
                                 //form.reInit();
                                 if (title) title='<h1>'+title+'</h1>' ;
                                 $j('div#viewer_main').html(title+html) ;
                                 break ;

     // показываем рузельт в модельном окне
     case 'modal_window':        //$j('#wrapper').append('<div id="mBox_content">'+html+'</div>') ; // используем промежуточный блок для отработки JS кода
                                 var after_close_window = get_xml_tag_value(responseXML,'after_close_window');
                                 var url = get_xml_tag_value(responseXML,'url');
                                 var mBox_modal_panel=new jBox('Modal',{title: title,
                                     content:html,
                                     overlay: true,closeOnClick:false,closeOnBodyClick:false,overlayStyles: {color: 'black',opacity: 0.4},position:{x: 'center',y: 'center'},
                                                                      onCloseComplete: function(){  removeEditor() ;
                                                                                                    //$j('div#mBox_content').remove();
                                                                                                    if (after_close_window!=undefined) switch (after_close_window)
                                                                                                    { case 'upload_list_recs': break ;
                                                                                                      case 'goto_url': document.location=url ; break ; // пока просто передача url в параметре params
                                                                                                      //case 'update_tr_rec': reload_tr_rec(parent_reffer) ; break ;
                                                                                                    }
                                                                                                 },
                                                                         buttons: [{title:'Закрыть'}]

                                      });
                                  //mBox_modal_panel.addButtons([{title: 'Закрыть',addClass: 'button',event: function(){this.close();}}]) ;
                                  //$j.ajax({url:'/ajax.php',dataType:'xml',cache:false,success:none,data:{cmd:'request_phone_sending',value:$('phone').value.clean()}});
                                  //$j('div#<?echo $form_id?>_ajax_result #cancel').live('click',function(){mBox_ajax_result.close();}) ;
                                  //document.location=document.location ;

                                  //mBox_modal_panel.setContent($j('div#mBox_content').html()) ;
                                  mBox_modal_panel.open();
                                 break ;

     default:                    //removeEditor() ;
                                 //form.reInit();
                                 if (title) title='<h1>'+title+'</h1>' ;
                                 if (list_id!=undefined && list_id!='undefined' && list_id!='')
                                 { $j('div#'+list_id+' div#append_content').remove() ;
                                   $j('div#'+list_id+'').append('<div id="append_content">'+title+html+'</div>') ;
                                 }
                                 else
                                 {
                                 $j('div#viewer_main div#append_content').remove() ;
                                 $j('div#viewer_main').append('<div id="append_content">'+title+html+'</div>') ;
                                 }
                                 // добавляем строку в конец таблицы
                                 // в дальнейшем сделать чтобы передавался id блока таблицы - чтобы строка вставлялась именно в нужную таблицу - если их несколько
                                 //if (tr_content!='') $j('table.list').append(tr_content) ;
                                 break ;

   }

    $j(this.content).find('.v1').prepare_form({}) ;
    $j(this.content).find('.v2').prepare_ajax({}) ;
    $j('.v1').prepare_form({}) ;
    $j('.v2').prepare_ajax({}) ;

}

function on_reload_tr_rec(data,status,link)
  { if (status=='success')
      {   var xml = link.responseXML;
          var value = get_xml_tag_value(xml,'tr_html');
          var attr = get_xml_tag_attr(xml,'tr_html');
          if (value!=undefined) $j('tr#'+attr.tr_id).html(value) ;
      }
  }

 function delete_item_from_tree(reffer)
 {   if (top.fra_tree!=undefined)
     {   var fra_tree=top.fra_tree.document ;
         var tree_li=$j(fra_tree).contents().find('li[reffer="'+reffer+'"]') ;
         if (tree_li.length)
         { // изменяем кол-во в родительском элементе
           var parent_li=$j(tree_li).closest('ul').prev('li') ;
           var cnt=$j(parent_li).attr('cnt') ;
           cnt-- ;
           if (cnt>0) $j(parent_li).attr('cnt',cnt).children('span.cnt').html('('+cnt+')') ;
           else       $j(parent_li).removeClass('open').removeAttr('cnt').children('span.cnt').html('') ;
           // удаляем элемент
           $j(tree_li).remove() ;
         }
     }
 }

/*********************************************************************************
 *
 * CKEditor
 *
//********************************************************************************/


var editor, html = '';
var submit_form = '';
var config = {};

function createEditor(id,config)
{ if ( editor ) return;
  // Create a new editor inside the <div id="editor">, setting its value to html
  //alert(id+' - '+config.skinPath) ;
  //alert(html) ;
  editor = CKEDITOR.replace( id, config, html );
}

function removeEditor()
{ if ( !editor ) return;

  // Retrieve the editor contents. In an Ajax application, this data would be
  // sent to the server or used in any other way.
  //document.getElementById( 'editorcontents' ).innerHTML = html = editor.getData();
  //document.getElementById( 'contents' ).style.display = '';

  // Destroy the editor.
  editor.destroy();
  editor = null;
}


/*********************************************************************************
 * SWFObject v1.5: Flash Player detection and embed - http://blog.deconcept.com/swfobject/
 *
 * SWFObject is (c) 2007 Geoff Stearns and is released under the MIT License:
 * http://www.opensource.org/licenses/mit-license.php
 *
//********************************************************************************/


if(typeof deconcept=="undefined"){var deconcept=new Object();}if(typeof deconcept.util=="undefined"){deconcept.util=new Object();}if(typeof deconcept.SWFObjectUtil=="undefined"){deconcept.SWFObjectUtil=new Object();}deconcept.SWFObject=function(_1,id,w,h,_5,c,_7,_8,_9,_a){if(!document.getElementById){return;}this.DETECT_KEY=_a?_a:"detectflash";this.skipDetect=deconcept.util.getRequestParameter(this.DETECT_KEY);this.params=new Object();this.variables=new Object();this.attributes=new Array();if(_1){this.setAttribute("swf",_1);}if(id){this.setAttribute("id",id);}if(w){this.setAttribute("width",w);}if(h){this.setAttribute("height",h);}if(_5){this.setAttribute("version",new deconcept.PlayerVersion(_5.toString().split(".")));}this.installedVer=deconcept.SWFObjectUtil.getPlayerVersion();if(!window.opera&&document.all&&this.installedVer.major>7){deconcept.SWFObject.doPrepUnload=true;}if(c){this.addParam("bgcolor",c);}var q=_7?_7:"high";this.addParam("quality",q);this.setAttribute("useExpressInstall",false);this.setAttribute("doExpressInstall",false);var _c=(_8)?_8:window.location;this.setAttribute("xiRedirectUrl",_c);this.setAttribute("redirectUrl","");if(_9){this.setAttribute("redirectUrl",_9);}};deconcept.SWFObject.prototype={useExpressInstall:function(_d){this.xiSWFPath=!_d?"expressinstall.swf":_d;this.setAttribute("useExpressInstall",true);},setAttribute:function(_e,_f){this.attributes[_e]=_f;},getAttribute:function(_10){return this.attributes[_10];},addParam:function(_11,_12){this.params[_11]=_12;},getParams:function(){return this.params;},addVariable:function(_13,_14){this.variables[_13]=_14;},getVariable:function(_15){return this.variables[_15];},getVariables:function(){return this.variables;},getVariablePairs:function(){var _16=new Array();var key;var _18=this.getVariables();for(key in _18){_16[_16.length]=key+"="+_18[key];}return _16;},getSWFHTML:function(){var _19="";if(navigator.plugins&&navigator.mimeTypes&&navigator.mimeTypes.length){if(this.getAttribute("doExpressInstall")){this.addVariable("MMplayerType","PlugIn");this.setAttribute("swf",this.xiSWFPath);}_19="<embed type=\"application/x-shockwave-flash\" src=\""+this.getAttribute("swf")+"\" width=\""+this.getAttribute("width")+"\" height=\""+this.getAttribute("height")+"\" style=\""+this.getAttribute("style")+"\"";_19+=" id=\""+this.getAttribute("id")+"\" name=\""+this.getAttribute("id")+"\" ";var _1a=this.getParams();for(var key in _1a){_19+=[key]+"=\""+_1a[key]+"\" ";}var _1c=this.getVariablePairs().join("&");if(_1c.length>0){_19+="flashvars=\""+_1c+"\"";}_19+="/>";}else{if(this.getAttribute("doExpressInstall")){this.addVariable("MMplayerType","ActiveX");this.setAttribute("swf",this.xiSWFPath);}_19="<object id=\""+this.getAttribute("id")+"\" classid=\"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000\" width=\""+this.getAttribute("width")+"\" height=\""+this.getAttribute("height")+"\" style=\""+this.getAttribute("style")+"\">";_19+="<param name=\"movie\" value=\""+this.getAttribute("swf")+"\" />";var _1d=this.getParams();for(var key in _1d){_19+="<param name=\""+key+"\" value=\""+_1d[key]+"\" />";}var _1f=this.getVariablePairs().join("&");if(_1f.length>0){_19+="<param name=\"flashvars\" value=\""+_1f+"\" />";}_19+="</object>";}return _19;},write:function(_20){if(this.getAttribute("useExpressInstall")){var _21=new deconcept.PlayerVersion([6,0,65]);if(this.installedVer.versionIsValid(_21)&&!this.installedVer.versionIsValid(this.getAttribute("version"))){this.setAttribute("doExpressInstall",true);this.addVariable("MMredirectURL",escape(this.getAttribute("xiRedirectUrl")));document.title=document.title.slice(0,47)+" - Flash Player Installation";this.addVariable("MMdoctitle",document.title);}}if(this.skipDetect||this.getAttribute("doExpressInstall")||this.installedVer.versionIsValid(this.getAttribute("version"))){var n=(typeof _20=="string")?document.getElementById(_20):_20;n.innerHTML=this.getSWFHTML();return true;}else{if(this.getAttribute("redirectUrl")!=""){document.location.replace(this.getAttribute("redirectUrl"));}}return false;}};deconcept.SWFObjectUtil.getPlayerVersion=function(){var _23=new deconcept.PlayerVersion([0,0,0]);if(navigator.plugins&&navigator.mimeTypes.length){var x=navigator.plugins["Shockwave Flash"];if(x&&x.description){_23=new deconcept.PlayerVersion(x.description.replace(/([a-zA-Z]|\s)+/,"").replace(/(\s+r|\s+b[0-9]+)/,".").split("."));}}else{if(navigator.userAgent&&navigator.userAgent.indexOf("Windows CE")>=0){var axo=1;var _26=3;while(axo){try{_26++;axo=new ActiveXObject("ShockwaveFlash.ShockwaveFlash."+_26);_23=new deconcept.PlayerVersion([_26,0,0]);}catch(e){axo=null;}}}else{try{var axo=new ActiveXObject("ShockwaveFlash.ShockwaveFlash.7");}catch(e){try{var axo=new ActiveXObject("ShockwaveFlash.ShockwaveFlash.6");_23=new deconcept.PlayerVersion([6,0,21]);axo.AllowScriptAccess="always";}catch(e){if(_23.major==6){return _23;}}try{axo=new ActiveXObject("ShockwaveFlash.ShockwaveFlash");}catch(e){}}if(axo!=null){_23=new deconcept.PlayerVersion(axo.GetVariable("$version").split(" ")[1].split(","));}}}return _23;};deconcept.PlayerVersion=function(_29){this.major=_29[0]!=null?parseInt(_29[0]):0;this.minor=_29[1]!=null?parseInt(_29[1]):0;this.rev=_29[2]!=null?parseInt(_29[2]):0;};deconcept.PlayerVersion.prototype.versionIsValid=function(fv){if(this.major<fv.major){return false;}if(this.major>fv.major){return true;}if(this.minor<fv.minor){return false;}if(this.minor>fv.minor){return true;}if(this.rev<fv.rev){return false;}return true;};deconcept.util={getRequestParameter:function(_2b){var q=document.location.search||document.location.hash;if(_2b==null){return q;}if(q){var _2d=q.substring(1).split("&");for(var i=0;i<_2d.length;i++){if(_2d[i].substring(0,_2d[i].indexOf("="))==_2b){return _2d[i].substring((_2d[i].indexOf("=")+1));}}}return "";}};deconcept.SWFObjectUtil.cleanupSWFs=function(){var _2f=document.getElementsByTagName("OBJECT");for(var i=_2f.length-1;i>=0;i--){_2f[i].style.display="none";for(var x in _2f[i]){if(typeof _2f[i][x]=="function"){_2f[i][x]=function(){};}}}};if(deconcept.SWFObject.doPrepUnload){if(!deconcept.unloadSet){deconcept.SWFObjectUtil.prepUnload=function(){__flash_unloadHandler=function(){};__flash_savedUnloadHandler=function(){};window.attachEvent("onunload",deconcept.SWFObjectUtil.cleanupSWFs);};window.attachEvent("onbeforeunload",deconcept.SWFObjectUtil.prepUnload);deconcept.unloadSet=true;}}if(!document.getElementById&&document.all){document.getElementById=function(id){return document.all[id];};}var getQueryParamValue=deconcept.util.getRequestParameter;var FlashObject=deconcept.SWFObject;var SWFObject=deconcept.SWFObject;

/*********************************************************************************
 *
 * обработка событий при выводе списока объектов - сортировка, фильтрация
 *
//********************************************************************************/


// клик по заголовку стролбца - применить сортировку или изменить её направление  ---------------------------------------------------------------------------------------------------------------------------------
function table_list_tr_header_td_click()
 { var fname=$j(this).attr('fname') ;
   if (fname==undefined) return ;
   var div_list_items=$j(this).parents('div.list_item') ;

   $j.ajax({url:'ajax.php',type:'POST',dataType:'xml',cache:false,
              success             :list_items_sort_change_success,
              data:{cmd           :'list_items_sort_change',
                    tkey          :div_list_items.attr('tkey'),
                    clss          :div_list_items.attr('clss'),
                    usl           :div_list_items.attr('usl'),
                    usl_filter    :div_list_items.attr('usl_filter'),
                    fname         :fname,
                    list_id       :div_list_items.attr('list_id'),
                    order_by      :div_list_items.attr('order_by'),
                    order_mode    :div_list_items.attr('order_mode'),
                    no_check      :div_list_items.attr('no_check'),
                    read_only  :div_list_items.attr('read_only'),
                    no_icon_edit  :div_list_items.attr('no_icon_edit'),
                    no_view_field  :div_list_items.attr('no_view_field'),
                    no_icon_photo :div_list_items.attr('no_icon_photo'),
                    count         :div_list_items.attr('count'),
                    page          :1
                   }
            });
 }

function list_items_sort_change_success(data,status,link)
{ if (status=='success')
    {   var xml = link.responseXML;
        if (xml==undefined)  return ;
        var list_id = get_xml_tag_value(xml,'list_id');
        var order_by = get_xml_tag_value(xml,'order_by');
        var order_mode = get_xml_tag_value(xml,'order_mode');
        var img = get_xml_tag_value(xml,'img');
        var tr_html = get_xml_tag_value(xml,'html');
        $j('div#'+list_id+' table.list').find('img.sort').remove();
        $j('div#'+list_id+' table.list tr.td_header td[fname="'+order_by+'"]').append(img) ;
        $j('div[list_id="'+list_id+'"]').attr('order_by',order_by) ;
        $j('div[list_id="'+list_id+'"]').attr('order_mode',order_mode) ;
        $j('div#'+list_id+' table.list  tr.item').remove() ;
        $j('div#'+list_id+' table.list').append(tr_html) ;
        update_next_page_info(xml) ;
    }
}

// двойной клик по имени слобца - показать поле для фильтрации ---------------------------------------------------------------------------------------------------------------------------------
/*
function table_list_tr_header_td_dblclick()
 { var fname=$j(this).attr('fname') ;
   if (fname==undefined) return ;

   var tkey=$j(this).parents('div.list_item').attr('tkey') ;
   var clss=$j(this).parents('div.list_item').attr('clss') ;
   var usl=$j(this).parents('div.list_item').attr('usl') ;
   var list_id=$j(this).parents('div.list_item').attr('list_id') ;

   $j(this).parents('div.list_item').find('td[is_filter=1]').each(function(){$j(this).html($j(this).attr('title')).attr('is_filter',0)});

   $j.ajax({url:'ajax.php',type:'POST',dataType:'xml',cache:false,success:list_items_fast_search_prepare_field_success,data:{cmd:'list_items_fast_search_prepare_field',tkey:tkey,clss:clss,usl:usl,fname:fname,list_id:list_id}});

 }

function list_items_fast_search_prepare_field_success(data,status,link)
{ if (status=='success')
     {   //$j('div.list_item input.fast_search').removeClass('ajax');
         var xml = link.responseXML;
         if (xml==undefined)  return ;
         var list_id = get_xml_tag_value(xml,'list_id');
         var fname = get_xml_tag_value(xml,'fname');
         var td_html = get_xml_tag_value(xml,'html');
         //alert(tr_html) ;
         var cur_text=$j('div#'+list_id+' table.list tr.td_header td[fname="'+fname+'"]').html() ;
         $j('div#'+list_id+' table.list tr.td_header td[fname="'+fname+'"]').html(td_html).attr('is_filter',1).attr('title',cur_text);
     }
}
*/

// набор текста в окошке быстрого поиска ---------------------------------------------------------------------------------------------------------------------------------
/*
function div_list_item_input_fast_search_keyup()
 { var tkey=$j(this).parents('div.list_item').attr('tkey') ;
   var clss=$j(this).parents('div.list_item').attr('clss') ;
   var usl=$j(this).parents('div.list_item').attr('usl') ;
   var no_check=$j(this).parents('div.list_item').attr('no_check') ;
   var read_only=$j(this).parents('div.list_item').attr('read_only') ;
   var no_icon_edit=$j(this).parents('div.list_item').attr('no_icon_edit') ;
   var no_icon_photo=$j(this).parents('div.list_item').attr('no_icon_photo') ;
   var no_view_field=$j(this).parents('div.list_item').attr('no_view_field') ;
   var list_id=$j(this).parents('div.list_item').attr('list_id') ;
   var fname=$j(this).attr('fname') ;
   var search=$j(this).val() ;
   var order_by=$j(this).parents('div.list_item').attr('order_by') ;
   var order_mode=$j(this).parents('div.list_item').attr('order_mode') ;
   //alert(search) ;
   $j(this).addClass('ajax');
   $j.ajax({url:'ajax.php',type:'POST',dataType:'xml',cache:false,success:input_fast_search_change_success,data:{cmd:'list_items_fast_search',tkey:tkey,clss:clss,usl:usl,search:search,list_id:list_id,order_by:order_by,order_mode:order_mode,fname:fname,no_check:no_check,read_only:read_only,no_icon_edit:no_icon_edit,no_icon_photo:no_icon_photo}});
 }


function input_fast_search_change_success(data,status,link)
{ if (status=='success')
     {   $j('div.list_item input.fast_search').removeClass('ajax');
         var xml = link.responseXML;
         if (xml==undefined)  return ;
         var list_id = get_xml_tag_value(xml,'list_id');
         var tr_html = get_xml_tag_value(xml,'html');
         //alert(tr_html) ;
         $j('div#'+list_id+' table.list tr.item').remove() ;
         $j('div#'+list_id+' table.list').append(tr_html) ;
     }
 }
*/

// поиск через панель быстрого поиска ---------------------------------------------------------------------------------------------------------------------------------

function panel_search_go_search_on_click()
{ var script_url=$j('div#panel_search').attr('script_url') ;
  var cmd=$j('div#panel_search').attr('cmd') ;
  var tkey=$j('div#panel_search').attr('tkey') ;
  var clss=$j('div#panel_search').attr('clss') ;
  var show_van_rec_as_item=$j('div#panel_search').attr('show_van_rec_as_item') ;
  var search=$j('div#panel_search input[name="text_search"]').val() ; if (search=='') return ;
  var target=$j('div#panel_search input[name="target_search"]:checked').val() ;
  var orders_status=$j('div#panel_search input[name="orders_status"]:checked').val() ;
  var div_list_items=$j('div.list_item') ;
  $j.ajax({url:script_url,type:'POST',dataType:'xml',cache:false,
       data:{cmd           :cmd,
             usl           :div_list_items.attr('usl'),
             search_text   :search,
             list_id       :div_list_items.attr('list_id'),
             no_check      :div_list_items.attr('no_check'),
             read_only  :div_list_items.attr('read_only'),
             no_icon_edit  :div_list_items.attr('no_icon_edit'),
             no_icon_photo  :div_list_items.attr('no_icon_photo'),
             no_view_field  :div_list_items.attr('no_view_field'),
     //        place_result_div:$j('div#panel_search').attr('place_result_div'),
             target        :target,
           orders_status        :orders_status,
             target_search  :target,
             tkey           :tkey,
             clss           :clss,
             show_van_rec_as_item :show_van_rec_as_item
            },
       success:function(data,status,link)
            { if (status=='success')
                { var xml = link.responseXML;
                  if (xml==undefined) { alert('Нет удалось получить ответ сервера')  ; return ; }
                  var html = get_xml_tag_value(xml,'html');
       //           var target_div = get_xml_tag_value(xml,'place_result_div');
                  //alert(target_div+' - '+html) ;
         //         $j('div#'+target_div).html(html) ; // target_div = viewer_main по умолчанию
          //        alert($j('div#'+target_div).html()) ;
                  $j('div#viewer_main').html(html) ;
                }
            }
     });
   return(false) ;
}

// создаем элемент memo  ---------------------------------------------------------------------------------------------------------------------------------

function onclick_gm()
 {  $j(this).removeAttr('el').html('').addClass('wait') ;
    $j.ajax({url:'ajax.php',type:'POST',dataType:'xml',cache:false,
                success             :onclick_gm_AJAX_success,
                data:{cmd           :'get_obj_field',
                      reffer        :$j(this).closest('[reffer]').attr('reffer'),
                      fname         :$j(this).attr('fn')
                     }
              });
 }

 function onclick_gm_AJAX_success(data,status,link)
 { if (status=='success')
     {   var xml = link.responseXML;
         // определяем ячейку таблицы
         var reffer = get_xml_tag_value(xml,'reffer');
         var fname = get_xml_tag_value(xml,'fname');
         var td=$j('[reffer="'+reffer+'"] td[fn="'+fname+'"]') ;
         // определяем имя  генерируемого элемента
         var tkey = get_xml_tag_value(xml,'tkey');
         var pkey = get_xml_tag_value(xml,'pkey');
         var clss = get_xml_tag_value(xml,'clss');
         var element_name='obj['+tkey+']['+clss+']['+pkey+']['+fname+']' ;
         var element_id='obj_'+tkey+"_"+clss+"_"+pkey+"_"+fname ;
         // определяем дополнительные параметры
         var el_class=td.attr('ec') ;
         var value = get_xml_tag_value(xml,'html');
         var use_html_editor = get_xml_tag_value(xml,'use_html_editor');

         td.html('<textarea id="'+element_id+'" name="'+element_name+'" class="'+el_class+'" type="text">'+value+'</textarea>')
           .removeClass('wait').removeAttr('fn').removeAttr('ec')
           .children('textarea').width(td.width()-8).height(td.height()-4).focus();

         if (use_html_editor==1)
         {
             include_ckeditor_script() ;
             $j('textarea#'+element_name).editor = CKEDITOR.replace(element_id);

         }

         // заменяем значек редактирования на значек сохранения
         $j('[reffer="'+reffer+'"] td.to_edit').removeClass('to_edit').addClass('to_save') ;
     }
 }

 // создаем элемент input ---------------------------------------------------------------------------------------------------------------------------------
 function onclick_gi()
 {   $j(this).html('').addClass('wait').removeAttr('el') ;
     $j.ajax({url:'ajax.php',type:'POST',dataType:'xml',cache:false,
                 success             :onclick_gi_AJAX_success,
                 data:{cmd           :'get_obj_field',
                       reffer        :$j(this).closest('[reffer]').attr('reffer'),
                       fname         :$j(this).attr('fn')
                      }
               });
 }

function onclick_gi_AJAX_success(data,status,link)
{ if (status=='success')
  {   var xml = link.responseXML;
      // определяем ячейку таблицы
      var reffer = get_xml_tag_value(xml,'reffer');
      var fname = get_xml_tag_value(xml,'fname');
      var td=$j('[reffer="'+reffer+'"] td[fn="'+fname+'"]') ;
      // определяем имя  генерируемого элемента
      var tkey = get_xml_tag_value(xml,'tkey');
      var pkey = get_xml_tag_value(xml,'pkey');
      var clss = get_xml_tag_value(xml,'clss');
      var element_name='obj['+tkey+']['+clss+']['+pkey+']['+fname+']' ;
      var id='el_'+tkey+'_'+pkey+'_'+fname ;
      // определяем дополнительные параметры
      var options=get_xml_tag_attr(xml,'options') ;
      var el_class=td.attr('ec') ;

      var value = get_xml_tag_value(xml,'html');

      td.html('<input name="'+element_name+'" id="'+id+'" class="text '+el_class+'" type="text" value="'+value+'">')
        .removeClass('wait').removeAttr('fn').removeAttr('ec')
        .children('input').width(td.width()-8).focus();

      // дополнительный опции к элементу
      if (options.datepicker==1)
      {  include_datapicker_script() ;
         $j('#'+id).datepicker($j.datepicker.regional["ru"]).datepicker("show"); // добавляем датапикер
      }
       // добавляем датапикер
      if (options.datetimepicker==1)
      {  include_datapicker_script() ;
         $j('#'+id).datetimepicker($j.datepicker.regional["ru"]).datetimepicker("show");
      }

      // заменяем значек редактирования на значек сохранения
      $j('[reffer="'+reffer+'"] td.to_edit').removeClass('to_edit').addClass('to_save') ;
  }
}

 function include_datapicker_script()
 { if ($j.datepicker==undefined)
    { $j("head").append($j('<script type="text/javascript" src="'+_PATH_TO_ADMIN_ADDONS+'/jquery-ui-1.9.2/jquery.ui.datepicker.min.js"></script>'));
      $j("head").append($j('<script type="text/javascript" src="'+_PATH_TO_ADMIN_ADDONS+'/jquery-ui-1.9.2/jquery.ui.datepicker-ru.js"></script>'));
      $j("head").append($j('<script type="text/javascript" src="'+_PATH_TO_ADMIN_ADDONS+'/jquery-ui-1.9.2/jquery-ui-timepicker-addon.js"></script>'));
      $j("head").append($j('<link rel="stylesheet" type="text/css" href="'+_PATH_TO_ADMIN_ADDONS+'/jquery-ui-1.9.2/jquery.ui.datepicker.css"/>'));
      $j("head").append($j('<link rel="stylesheet" type="text/css" href="'+_PATH_TO_ADMIN_ADDONS+'/jquery-ui-1.9.2/jquery-ui-timepicker-addon.css"/>'));
    }
 }

 function include_ckeditor_script()
 { if (CKEDITOR==undefined && _CKEDITOR_!='none' && _CKEDITOR_!="")
    { $j("head").append($j('<script type="text/javascript" src="'+_EXT+'/'+_CKEDITOR_+'/ckeditor.js"></script>'));
    }
 }



 // создаем элемент select ---------------------------------------------------------------------------------------------------------------------------------
 function onclick_gs()
 {  $j(this).removeAttr('el').html('').addClass('wait') ;
    $j.ajax({url:'ajax.php',type:'POST',dataType:'xml',cache:false,
                 success             :onclick_gs_AJAX_success,
                 data:{cmd           :'get_array_values',
                       reffer        :$j(this).closest('[reffer]').attr('reffer'),
                       fname         :$j(this).attr('fn'),
                       arr_id        :$j(this).attr('ai'),
                       arr_field     :$j(this).attr('af')
                      }
               });
 }

 function onclick_gs_AJAX_success(data,status,link)
 { if (status=='success')
    {   var xml = link.responseXML;
        // определяем ячейку таблицы
        var reffer = get_xml_tag_value(xml,'reffer');
        var fname = get_xml_tag_value(xml,'fname');
        var td=$j('[reffer="'+reffer+'"] td[fn="'+fname+'"]') ;
        // определяем имя  генерируемого элемента
        var tkey = get_xml_tag_value(xml,'tkey');
        var pkey = get_xml_tag_value(xml,'pkey');
        var clss = get_xml_tag_value(xml,'clss');
        var element_name='obj['+tkey+']['+clss+']['+pkey+']['+fname+']' ;
        // определяем дополнительные параметры
        var el_arr_sel=td.attr('as') ; // код выбранного элемента
        // собираем select
        var el_sel='' ; var el_html='' ;
        var elem = xml.getElementsByTagName('arr_value'); // массив значений массива
        $j.each(elem,function(indx,obj)
            { var obj_attr=get_xml_obj_attr(obj) ; // получаем аттрибуты текущего value
              if (obj_attr.id==el_arr_sel) el_sel=' selected' ; else el_sel='' ;
              var title=($j.browser.msie)? obj.text:obj.textContent ;
              el_html+='<option value="'+obj_attr.id+'"'+el_sel+'>'+title+'</option>' ;
            }) ;
        td.html('<select size="1" name="'+element_name+'"><option value=""></option>'+el_html+'</select>')
          .removeClass('wait').removeAttr('fn').removeAttr('ec')
          .children('select').width(td.width()-8).focus();

        // заменяем значек редактирования на значек сохранения
        $j('[reffer="'+reffer+'"] td.to_edit').removeClass('to_edit').addClass('to_save') ;
    }
 }

 // создаем элемент select для parent ---------------------------------------------------------------------------------------------------------------------------------
 function onclick_gts()
 {  $j(this).removeAttr('el').html('').addClass('wait') ;
    $j.ajax({url:'ajax.php',type:'POST',dataType:'xml',cache:false,
                 success             :onclick_gs_AJAX_success,
                 data:{cmd           :'get_table_clss_values',
                       reffer        :$j(this).closest('[reffer]').attr('reffer'),
                       fname         :$j(this).attr('fn'),
                       as            :$j(this).attr('as')
                      }
               });
 }

//generate_checkbox   ---------------------------------------------------------------------------------------------------------------------------------
function onclick_gc()
{  var img=$j(this).find('img') ;
   var value ;
   var inverse=$j(this).attr('inverse') ;
   var next_img ;
   if (img.attr('alt')=='+')
    { next_img=(inverse==1)? "img/on3.gif":"img/off3.gif" ;
      img.attr('src',next_img).attr('alt','-') ;
      value=0 ;
    }
   else
    { next_img=(inverse==1)? "img/off3.gif":"img/on3.gif" ;
      img.attr('src',next_img).attr('alt','+') ;
      value=1 ;
    }

   $j.ajax({url:'ajax.php',type:'POST',dataType:'xml',cache:false,
            success             :set_value_AJAX_success,
            data:{cmd           :'set_value',
                  reffer        :$j(this).closest('[reffer]').attr('reffer'),
                  clss          :$j(this).closest('div.list_item').attr('clss'),
                  fname         :$j(this).attr('fn'),
                  value         :value
                 }
          });
}
// для поля "enabled" - отдельные картинки
function onclick_gc2()
{  var img=$j(this).children('img') ;
   var value=0  ;
   var reffer=$j(this).closest('[reffer]').attr('reffer') ;
   if (img.attr('alt')=='+') img.attr('src','img/off2.gif').attr('alt','-') ;
   else                    { img.attr('src','img/on2.gif').attr('alt','+') ; value=1 ; }

   $j.ajax({url:'ajax.php',type:'POST',dataType:'xml',cache:false,
            success             :set_value_AJAX_success,
            data:{cmd           :'set_value',
                  reffer        :reffer,
                  clss          :$j(this).closest('div.list_item').attr('clss'),
                  fname         :$j(this).attr('fn'),
                  value         :value
                 }
          });

   // обновляем статус объекта в дереве - включено/выключено
   var fra_tree=top.fra_tree.document ;
   if (fra_tree!=null)
    {   var tree_li=$j(fra_tree).contents().find('li[reffer="'+reffer+'"]') ;
        if (tree_li.length)
        {  if (value) $j(tree_li).removeAttr('off') ;
           else       $j(tree_li).attr('off',true) ;
        }
    }

}

function set_value_AJAX_success(data,status,link)
{   if (status=='success')
        {   var xml = link.responseXML;
            // результат сохранения
            var update_cnt = get_xml_tag_value(xml,'update_cnt');
            var html = get_xml_tag_value(xml,'html');
            if (update_cnt>0) show_notife3({content:'Сохранено'}) ;
        }
}

// generate nulticheck ---------------------------------------------------------------------------------------------------------------------------------
function onclick_gmc()
 {  $j(this).removeAttr('el').html('').addClass('wait') ;
    $j.ajax({url:'ajax.php',type:'POST',dataType:'xml',cache:false,
                  success             :onclick_gmc_AJAX_success,
                  data:{cmd           :'get_array_values',
                        reffer        :$j(this).closest('[reffer]').attr('reffer'),
                        fname         :$j(this).attr('fn'),
                        arr_id        :$j(this).attr('ai'),
                        arr_field     :$j(this).attr('af')
                       }
                });
 }

 function onclick_gmc_AJAX_success(data,status,link)
 { if (status=='success')
    {   var xml = link.responseXML;
        // определяем ячейку таблицы
        var reffer = get_xml_tag_value(xml,'reffer');
        var fname = get_xml_tag_value(xml,'fname');
        var td=$j('[reffer="'+reffer+'"] td[fn="'+fname+'"]') ;
        // определяем имя  генерируемого элемента
        var tkey = get_xml_tag_value(xml,'tkey');
        var pkey = get_xml_tag_value(xml,'pkey');
        var clss = get_xml_tag_value(xml,'clss');
        var element_name='obj['+tkey+']['+clss+']['+pkey+']['+fname+']' ;
        // определяем дополнительные параметры
        var el_arr_sel=new String(td.attr('as')) ; // выбранные в multicheck коды значений
        var el_arr_sel2=el_arr_sel.split(' ') ;
        // собираем select
        var el_sel='' ; var el_html='' ;
        var elem = xml.getElementsByTagName('arr_value'); // массив значений массива
        $j.each(elem,function(indx,obj)
            { var obj_attr=get_xml_obj_attr(obj) ; // получаем аттрибуты текущего value
              if ($j.inArray(obj_attr.id,el_arr_sel2)!=-1) el_sel=' checked' ; else el_sel='' ;
              var title=($j.browser.msie)? obj.text:obj.textContent ;
              el_html+='<div style="white-space:nowrap;"><input type="checkbox" name="'+element_name+'['+obj_attr.id+']" value="1" '+el_sel+'>&nbsp;'+title+'</div>' ;
            }) ;
        el_html='<div class=left>'+el_html+'</div>' ;

        td.html(el_html).removeClass('wait').removeAttr('fn').removeAttr('ec').children('select').width(td.width()-8).focus();

        // заменяем значек редактирования на значек сохранения
        $j('[reffer="'+reffer+'"] td.to_edit').removeClass('to_edit').addClass('to_save') ;
    }
 }

 function onclick_check_all()
 { if ($j(this).is(':checked')) $j(this).closest('table').find('input[type="checkbox"][name^="_check"]').attr('checked',true)  ;
   else                         $j(this).closest('table').find('input[type="checkbox"][name^="_check"]').removeAttr('checked')  ;
 }

 function onclick_td_check()
{ var input=$j(this).children('input') ;
  if ($j(input).is(':checked')) $j(input).removeAttr('checked')  ;
  else                          $j(input).attr('checked',true)  ;
}

 function onclick_td_check2()
{ var input=$j(this) ;
  if ($j(input).is(':checked')) $j(input).removeAttr('checked')  ;
  else                          $j(input).attr('checked',true)  ;
}

function onclick_to_edit()
{ var reffer=$j(this).attr('reffer') ;
  if (reffer==undefined) reffer=$j(this).closest('tr').attr('reffer') ;
  if ($j(this).attr('link')!=undefined) reffer=$j(this).attr('link') ;
  parent.window.open('fra_viewer.php?obj_reffer='+reffer,'obj_show_'+reffer,'status=1,dependent=1,width=800,height=600, resizable=1, scrollbars=yes');
}

function onclick_to_new_window()
{
    parent.window.open($j(this).attr('href'));
    return(false) ;
}

function onclick_to_save()
{ var tr=$j(this).closest('tr') ;
  var div_list_items=$j(this).closest('div.list_item') ;
  $j(tr).wrapInner('<form  method="POST" action="ajax.php"></form>') ;
  var form=$j(tr).children('form') ;
  $j(form).append('<input type="hidden" name="cmd" value="save_list"><input type="hidden" name="mode" value="tr_update"><input type="hidden" name="list_id" value="'+div_list_items.attr('list_id')+'">') ;
  // if (editor)  { $j('textarea[name="'+editor.name+'"]').val(editor.getData()) ;  }
  // проходим по всем textarea в строке и ищем если ли для них открытые редакторы

  $j.each(CKEDITOR.instances,function(){this.updateElement()})  ;

  $j(form).ajaxForm({url:'ajax.php',dataType:'xml',success:function(data){ mForm_Submit_AJAX_onSuccess(data) ;}});
  $j(form).submit() ;
  $j(form).children('input[name="cmd"]').remove() ;
  $j(form).children('input[name="mode"]').remove() ;
  $j(form).after($j(form).children()) ;
  $j(form).remove() ;
}

// подгрузка страниц при прокрутке экрана ---------------------------------------------------------------------------------------------------------------------------------

function check_next_page_background_upload()
{ var elem=$j('div.list_item input.view_page') ;
  if (elem.attr('class')!=undefined && !elem.hasClass('wait'))
   {  var scrollTop=$j('body').scrollTop() ;
      if (scrollTop==0) scrollTop=document.documentElement.scrollTop ;
      var bottom_height=$j(window).height()-elem.offset().top-elem.outerHeight()+scrollTop ; // вычисляем положение нижнего края элемента относительно нижней части фрейма
      //$j('div.list_item input.view_page').val(bottom_height) ;
      if (bottom_height>-100) next_page_background_upload(elem) ;
   }
}

function next_page_background_upload(elem)
{  var div_list_items=elem.parents('div.list_item') ;
   elem.val(elem.val()+'.          Идет загрузка...').addClass('wait') ;
   $j.ajax({url:'ajax.php',type:'POST',dataType:'xml',cache:false,
            success             :next_page_background_upload_success,
            data:{cmd           :'list_items',
                  tkey          :div_list_items.attr('tkey'),
                  clss          :div_list_items.attr('clss'),
                  usl           :div_list_items.attr('usl'),
                  usl_filter    :div_list_items.attr('usl_filter'),
                  list_id       :div_list_items.attr('list_id'),
                  order_by      :div_list_items.attr('order_by'),
                  order_mode    :div_list_items.attr('order_mode'),
                  no_check      :div_list_items.attr('no_check'),
                  read_only  :div_list_items.attr('read_only'),
                  no_icon_edit  :div_list_items.attr('no_icon_edit'),
                  no_icon_photo  :div_list_items.attr('no_icon_photo'),
                  no_view_field  :div_list_items.attr('no_view_field'),
                  page          :elem.attr('page')
                 }
          });
}

function next_page_background_upload_success(data,status,link)
{ if (status=='success')
    {   var xml = link.responseXML;
        if (xml==undefined)  return ;
        var list_id = get_xml_tag_value(xml,'list_id');
        var tr_html = get_xml_tag_value(xml,'html');
        var info_count_text = get_xml_tag_value(xml,'info_count_text');
        $j('div#'+list_id+' table.list').append(tr_html) ;
        $j('div#'+list_id+' table.list tr.clss_header span.setting').text(info_count_text) ;
        update_next_page_info(xml) ;
        $j('.v1').prepare_form({}) ;
        $j('.v2').prepare_ajax({}) ;

    }
}

// функция должна вызываться после каждой фоновой подгрузки списка записей
function update_next_page_info(xml)
{   var from_pos = get_xml_tag_value(xml,'from_pos');
    var to_pos = get_xml_tag_value(xml,'to_pos');
    var all_pos = get_xml_tag_value(xml,'all_pos');
    var next_page = get_xml_tag_value(xml,'next_page');
    var list_id = get_xml_tag_value(xml,'list_id');

    if (next_page) {  var elem=$j('div.list_item input.view_page') ;
                     if (elem.attr('class')==undefined) $j('div.list_item table.list').after('<div class=next_page><input type="button" class=view_page></div>') ; // если нет элемента для подгрузки страницы - его надо добавить
                     $j('div#'+list_id+' input.view_page').attr('page',next_page).val('еще '+(all_pos-to_pos)+' строк').removeClass('wait') ; // обновляем текст на существующей кнопке
                   }
    else           $j('div#'+list_id+' input.view_page').remove() ; // удаляем кнопку

    hs.onSetClickEvent = function ( sender, e )
    {  // set the onclick for the element, output the group name to the caption for debugging
       e.element.onclick = function (){return hs.expand(this,{slideshowGroup: this.className});} ;
       //e.element.onclick = function (){return hs.expand(this,{slideshowGroup: this.parentNode.className, captionText: this.parentNode.className});} ;
       return false; // return false to prevent the onclick being set once again
    } ;

    hs.isUnobtrusiveAnchor = function(el) {
    	if (el.href && /\.jpg$/.test(el.href)) {
    		el.className = 'highslide'; // for the zoom-in cursor
    		return 'image';
    	}
    }

}

/*********************************************************************************
 *
 * обработчики для дерева каталога
 *
//********************************************************************************/
  // клик для раскрытия узла
  function tree_div_expand_click() { tree_item_expand($j(this).parent(),0) ; return(false); }

  function reload_cur_item() { tree_item_expand($j('li.selected'),1) ; }

  // подгрузка дочерних элементов для элемента дерева li_item
  function tree_item_expand(li_item,always_upload)
  {  if (!li_item.length) return ;
     var li_item_attr=li_item[0].attributes ;
     // получаем параметры вывода дерева
     var tree_root=$j(li_item).closest('ul.tree_root') ;
     var tree_tkey=$j(tree_root).attr('tkey') ; if (tree_tkey==undefined) tree_tkey=0 ;
     var usl_select=$j(tree_root).attr('usl_select') ; if (usl_select==undefined) usl_select='' ;
     var ajax_script=$j(tree_root).attr('on_expand_script');
     var ajax_cmd=$j(tree_root).attr('on_expand_cmd');
     // если ветка еще не подгружена, загружаем через AJAX
     var childs=$j(li_item).next('ul') ;
     if (!childs.length || always_upload)
     { // переводим все аттрибуты кликнутого элемента в скрытые переменные
       var data = new Object() ;
       data['tkey']=tree_tkey ; // будет перекрыт если tkey передеается через атрибуты
       data['usl_select']=usl_select ;
       for(var j=0; j<li_item_attr.length; j++) data[li_item_attr.item(j).nodeName]=li_item_attr.item(j).nodeValue ;
       data['cmd']=ajax_cmd ;
       $j.ajax({url:ajax_script,dataType:'xml',type:'POST',async:false,cache:false,data:data,success:upload_tree_item_success,error:upload_tree_item_error}) ; // ВНИМАНИЕ: этот запрос должен быть синхронным
       $j(li_item).addClass('open');
     }
     else $j(li_item).toggleClass('open');
  }

  // успешная подгрузка ветки
  function upload_tree_item_success(data,status,link)
  { if (status=='success')
      { var xml = link.responseXML;
        var fra_tree=top.fra_tree.document ;
        var html = get_xml_tag_value(xml,'html');
        var reffer = get_xml_tag_value(xml,'reffer');
        if (reffer!=undefined)
        { var parent_item=$j(fra_tree).contents().find('li[reffer="'+reffer+'"]')  ;
          var childs=$j(fra_tree).contents().find('li[reffer="'+reffer+'"] + ul') ;
          if (childs.length) $j(childs).replaceWith(html) ; // если дочерние элементы существуют - заменяем
          else               $j(fra_tree).contents().find('li[reffer="'+reffer+'"]').after(html) ; // иначе добавляем к родителю
          $j(fra_tree).contents().find('li[reffer="'+reffer+'"] + ul > li').prepend('<div class=expand></div><div class=clss></div>') ; // добавляем иконку узла дерева и иконку класса
          $j(fra_tree).contents().find('li[reffer="'+reffer+'"] + ul > li[open]').addClass('open') ; // добавляем класс "open" для элементов имеющих атрибут "open"
          // обновляем имя и количество текущего элемента
          var cur_obj_name = get_xml_tag_value(xml,'cur_obj_name');
          var cur_obj_child_cnt = get_xml_tag_value(xml,'cur_obj_child_cnt');
          $j(parent_item).children('span.name').html(cur_obj_name) ;
          if (cur_obj_child_cnt>0) $j(parent_item).attr('cnt',cur_obj_child_cnt).children('span.cnt').html('('+cur_obj_child_cnt+')') ;
          else                     $j(parent_item).removeClass('open').removeAttr('cnt').children('span.cnt').html('') ;

        }
        var t1  = get_xml_tag_value(xml,'t1');
        var t2  = get_xml_tag_value(xml,'t2');
        if (t1!="" && t2!="")
        { var childs=$j(fra_tree).contents().find('li[t1="'+t1+'"][t2="'+t2+'"] + ul') ;
          if (childs.length) $j(childs).replaceWith(html) ; // если дочерние элементы существуют - заменяем
          else               $j(fra_tree).contents().find('li[t1="'+t1+'"][t2="'+t2+'"]').after(html) ; // иначе добавляем к родителю
          $j(fra_tree).contents().find('li[t1="'+t1+'"][t2="'+t2+'"] + ul > li').prepend('<div class=expand></div><div class=clss></div>') ; // добавляем иконку узла дерева и иконку класса
          $j(fra_tree).contents().find('li[t1="'+t1+'"][t2="'+t2+'"] + ul > li[open]').addClass('open') ; // добавляем класс "open" для элементов имеющих атрибут "open"
        }
      }
  }

  function upload_tree_item_error(data,status,link)
  { alert(data+status+link) ;

  }

  // клик по элементу дерева
  function tree_item_click()
  { $j('li').removeClass('selected') ;
    $j(this).addClass('selected') ;
    var tkey=$j(this).closest('ul.tree_root').attr('tkey') ;
    var table_code=$j(this).closest('ul.tree_root').attr('table_code') ;
    var clss=$j(this).attr('clss') ;
    var on_click_script=$j(this).closest('ul.tree_root').attr('on_click_script') ;
    var on_click_item=$j(this).closest('ul.tree_root').attr('on_click_item_'+clss) ;
    var params='' ;
    if (tkey!=undefined) params+='&tkey='+tkey ;
    if (table_code!=undefined) params+='&table_code='+table_code ;
    var arr_attr=convert_tag_attributes_to_array(this) ;
    for (key in arr_attr) params+='&'+key+'='+arr_attr[key] ;
    for (code in arr_attr) params+='&'+code+'='+arr_attr[code] ;
    //alert(params) ;
    //for(var j=0; j<this.attributes.length; j++) params+='&'+this.attributes.item(j).nodeName+'='+this.attributes.item(j).nodeValue ;
    //if ($j(this).attr('reffer')!=undefined) params+='&obj_reffer='+$j(this).attr('reffer') ;

    var url=(on_click_item!=undefined)? on_click_item:on_click_script;
    if (params) url+='?'+params+"&rn="+Math.random() ;
    parent.fra_view.location=url ;
    if (parent.fra_order!=undefined) parent.fra_order.location='editor_works_viewer.php?'+params+"&rn="+Math.random() ; ;
  }

  function tree_fill_count()
  {  var cnt=$j(this).next('ul').children('li').length ;
     if (cnt>0)
     { $j(this).attr('cnt',cnt) ;
       $j(this).children('span.cnt').text('('+cnt+')') ;
     }
  }


/*********************************************************************************
 *
 * вспомогательные функции
 *
//********************************************************************************/


function f_preventDefault(e){(e.preventDefault)? e.preventDefault():e.returnValue=false ;}

function get_xml_tag_value(xml,tag_name)
{   var elem = xml.getElementsByTagName(tag_name);
    if (elem.length)
    {   if ($j.browser.msie) return(elem[0].text);
        else                 return(elem[0].textContent);
    }
    else return('');
}

// получаем атрибуеты XML TAG объекта
function get_xml_tag_attr(xml,tag_name)
{   var attr_XML=xml.getElementsByTagName(tag_name).item(0).attributes;
    var attr = new Object();
    if (attr_XML.length) for(var j=0; j<attr_XML.length; j++) attr[attr_XML[j].nodeName]=attr_XML[j].nodeValue ;
    return(attr);
}

// получаем атрибуеты XML OBJ объекта
function get_xml_obj_attr(obj)
{   var attr_XML=obj.attributes;
    var attr = new Object();
    for(var j=0; j<attr_XML.length; j++) attr[attr_XML[j].nodeName]=attr_XML[j].nodeValue ;
    return(attr);
}


function show_obj(tkey,pkey,edit,menu_id)
{
   parent.window.open('fra_viewer.php?tkey='+tkey+'&pkey='+pkey+'&edit='+edit+'&menu_id='+menu_id,'obj_show_'+tkey,'status=1,dependent=1,width=800,height=600, resizable=1, scrollbars=yes');
}

function dump(obj, obj_name)
{ var result = '' ;
 for (var i in obj) result += obj_name + '.' + i + ' = ' + obj[i] + '\n' ;
 return result ;
}



