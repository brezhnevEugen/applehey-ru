<?
//-----------------------------------------------------------------------------------------------------------------------------
// фрейм дерева
//-----------------------------------------------------------------------------------------------------------------------------
include_once('c_admin_page.php') ;
class c_fra_tree extends c_admin_page
{

  function site_top_head($options=array())
   { ?><head>
         <META http-equiv=Content-Type content="text/html; charset=utf-8" />
         <script type="text/javascript" src="<?echo _PATH_TO_ADMIN_ADDONS?>/jquery-1.8.3.min.js"></script>
         <SCRIPT type="text/javascript" src="<?echo _PATH_TO_ADMIN_ADDONS?>/jquery.form.js" ></SCRIPT> <?/*используется ajaxForm для отправки форм через ajax*/?>
         <script type="text/javascript" src="<?echo _PATH_TO_ADMIN_ADDONS?>/mBox/jBox-master/Source/jBox.min.js"></script>
         <link rel="stylesheet" type="text/css" href="<?echo _PATH_TO_ADMIN_ADDONS?>/mBox/jBox-master/Source/jBox.css">

         <script type="text/javascript" src="<?echo _PATH_TO_ENGINE?>/admin/script.js"></script>
         <link rel="stylesheet" type="text/css" href="<?echo _PATH_TO_ENGINE?>/admin/style.css">
         <?/*

         <script type="text/javascript" src="/engine_v4.78/AddOns/jquery-1.8.3.min.js"></script>
        <script type="text/javascript" src="/engine_v4.78/AddOns/mootools/mootools-core-1.3.1.js" ></script>
       	<script type="text/javascript" src="/engine_v4.78/AddOns/mootools/mootools-more-1.3.1.1.js" ></script>

        <script type="text/javascript" src="/engine_v4.78/AddOns/jquery.form.js" ></script>
        <script type="text/javascript" src="/engine_v4.78/AddOns/jquery-ui-1.9.2/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/engine_v4.78/AddOns/jquery-ui-1.9.2/jquery.ui.datepicker.min.js"></script>
        <script type="text/javascript" src="/engine_v4.78/AddOns/jquery-ui-1.9.2/jquery.ui.datepicker-ru.js"></script>
        <script type="text/javascript" src="/engine_v4.78/AddOns/jquery-ui-1.9.2/jquery.ui.widget.min.js"></script>
        <script type="text/javascript" src="/engine_v4.78/AddOns/jquery-ui-1.9.2/jquery.ui.tabs.min.js"></script>
        <link rel="stylesheet" type="text/css" href="/engine_v4.78/AddOns/jquery-ui-1.9.2/jquery-ui.css"/>
        <link rel="stylesheet" type="text/css" href="/engine_v4.78/AddOns/jquery-ui-1.9.2/jquery.ui.datepicker.css"/>
        <link rel="stylesheet" type="text/css" href="/engine_v4.78/AddOns/jquery-ui-1.9.2/jquery.ui.tabs.css"/>

        <link rel="stylesheet" type="text/css" href="/engine_v4.78/AddOns/StephanWagner-mBox-0.2.6/Source/assets/mBoxCore.css">
        <link rel="stylesheet" type="text/css" href="/engine_v4.78/AddOns/StephanWagner-mBox-0.2.6/Source/assets/mBoxModal.css">
        <!--<link rel="stylesheet" type="text/css" href="/engine_v4.78/AddOns/StephanWagner-mBox-0.2.6/Source/assets/mBoxNotice.css">-->
        <!--<link rel="stylesheet" type="text/css" href="/engine_v4.78/AddOns/StephanWagner-mBox-0.2.6/Source/assets/mBoxTooltip.css">-->
        <!--<link rel="stylesheet" type="text/css" href="/engine_v4.78/AddOns/StephanWagner-mBox-0.2.6/Source/assets/themes/mBoxTooltip-Black.css">-->
        <!--<link rel="stylesheet" type="text/css" href="/engine_v4.78/AddOns/StephanWagner-mBox-0.2.6/Source/assets/themes/mBoxTooltip-BlackGradient.css">-->
        <link rel="stylesheet" type="text/css" href="/engine_v4.78/AddOns/StephanWagner-mForm-0.2.6/Source/assets/mForm.css">
       	<SCRIPT type="text/javascript" src="/engine_v4.78/AddOns/StephanWagner-mBox-0.2.6/Source/mBox.Core.js" ></SCRIPT>
       	<SCRIPT type="text/javascript" src="/engine_v4.78/AddOns/StephanWagner-mBox-0.2.6/Source/mBox.Modal.js" ></SCRIPT>
       	<!--<SCRIPT type="text/javascript" src="/engine_v4.78/AddOns/StephanWagner-mBox-0.2.6/Source/mBox.Modal.Confirm.js" ></SCRIPT>-->
       	<!--<SCRIPT type="text/javascript" src="/engine_v4.78/AddOns/StephanWagner-mBox-0.2.6/Source/mBox.Notice.js" ></SCRIPT>-->
       	<!--<SCRIPT type="text/javascript" src="/engine_v4.78/AddOns/StephanWagner-mBox-0.2.6/Source/mBox.Tooltip.js" ></SCRIPT>-->
        <SCRIPT type="text/javascript" src="/engine_v4.78/AddOns/StephanWagner-mForm-0.2.6/Source/mForm.Core.js" ></SCRIPT>
       	<SCRIPT type="text/javascript" src="/engine_v4.78/AddOns/StephanWagner-mForm-0.2.6/Source/mForm.Element.js" ></SCRIPT>
       	<SCRIPT type="text/javascript" src="/engine_v4.78/AddOns/StephanWagner-mForm-0.2.6/Source/mForm.Element.Select.js" ></SCRIPT>
        <SCRIPT type="text/javascript" src="/engine_v4.78/AddOns/StephanWagner-mForm-0.2.6/Source/mForm.Submit.js" ></SCRIPT>

        <SCRIPT type="text/javascript" src="/engine_v4.78/AddOns/ckeditor_v4.0.1/ckeditor.js" ></SCRIPT>
         */?>


       <script type="text/javascript">var _PATH_TO_ADMIN='<?echo _PATH_TO_ADMIN?>/';</script>
       </head>
    <?
   }


  function body($options=array())
  { if (!$this->tkey) { echo 'Не задана рабочая таблица.' ; _exit() ; }
    $this->check_reffer() ; // проверяем, откуда происходит вызов скрипта
    ?><body id=tree_objects><div id=tree_menu><?$this->top_menu() ;?></div><?
    $usl_root=($_GET['root_id'])?  'pkey='.$_GET['root_id']:'parent=0' ;
    $this->generate_object_tree($this->tkey,array('usl_root'=>$usl_root,'no_show_cnt'=>0)); // генерим начало дерева
    ?><script type="text/javascript">$j('ul.tree_root > li:first-child').each(tree_item_click)</script><? // имитируем клик по корню
    ?></body><?
  }

  function _top_menu() {?><div class=reload_cur_item></div><button id=go_filter class="button_small go_filter v2" cmd="tree|get_panel_filter">Фильтр</button><?}
  function top_menu() {?><div class=reload_cur_item></div><?}

  // $options:
  //    usl_root            -   условие для определения корня, по умолчнию "parent=0"
  //    on_click_script     -   скрипт, который будет вызван во втором фрейме. Скрипту будут переданы все атрибуты элемента в виде GET параметров
  //    on_expand_script    -   скрипт, который будет вызываться для подгрузки ветвей дерева при разворачивании узлов
  //    on_expand_cmd       -   команда скрипту для для подгрузки
  //    no_show_cnt         -   не показывать кол-во item в ветках
  function generate_object_tree($tkey,$options=array())
  { if (!$options['on_click_script'])  $options['on_click_script']='fra_viewer.php' ;
    if (!$options['on_expand_script']) $options['on_expand_script']='ajax.php' ;
    if (!$options['on_expand_cmd'])    $options['on_expand_cmd']='upload_object_tree_items' ;
    if (!$options['usl_root']) $options['usl_root']='parent=0' ;
    $root=execSQL_van('select pkey,clss,obj_name,enabled,1 as cnt from '._DOT($tkey)->table_name.' where  '.$options['usl_root'].' limit 1') ;
    if (!$root['clss']) echo 'alert("В таблице не найден корень дерева")' ;
    //$_SESSION['tree_filter'][$tkey][10]=array('sort'=>'obj_name desc') ;
    //$_SESSION['tree_filter'][$tkey][200]=array('filter'=>'sales_value>0') ;
    $options['debug']=0 ;
    $options['order_by']=_DEFAULT_TREE_ORDER;
    //$options['use_page_view_sort_setting']=0 ; // используем настройки сортировки редактора объектов, если они таб быти определееы
    $options['limit']='limit 100' ;
    $options['no_image']=1 ; //информация по фото не нужна
    $options['fill_count_childs']=1 ; // получаем кол-во дочерних объектов
    $list_obj=select_db_obj_child_recs($root['_reffer'],$options) ;  $list_clss=array() ;
    ?><ul class="tree_root" table_code="<?echo _DOT($tkey)->table_code?>" on_click_script=<?echo $options['on_click_script']?> on_expand_script=<?echo $options['on_expand_script']?> on_expand_cmd=<?echo $options['on_expand_cmd']?> <?if ($options['no_show_cnt'])  echo 'no_show_cnt'?>>
         <li clss="<?echo $root['clss']?>"  <?if (!$root['parent']) echo 'is_root=1'?> reffer="<?echo $root['_reffer']?>"><span class=name><?echo $root['obj_name']?></span><span class=cnt></span></li>
         <ul><? if (sizeof($list_obj)) foreach ($list_obj as $tkey_arr) if (sizeof($tkey_arr)) foreach ($tkey_arr as $clss=>$recs) { _CLSS($clss)->print_list_recs_to_tree($recs) ; $list_clss[]=$clss ; }?></ul>
      </ul>
    <?
    generate_clss_style_icon($list_clss) ;
  }


  // $options:
  //    usl_select          -   специальное условие выборки параметров (в дополнение к "parent!=0")
  //    on_click_script     -   скрипт который будет вызван во втором фрейме. Скрипту будут переданы все атрибуты элемента в виде GET параметров
  //    on_expand_script    -   скрипт, который будет вызываться для подгрузки ветвей дерева при разворачивании узлов
  //    on_expand_cmd       -   команда скрипту для для подгрузки
  //    no_show_item        -   если передать 1, в дереве не будут показаны items, только tree
  //    no_show_cnt         -   не показывать кол-во item в ветках
  // $this->generate_time_tree('Журнал',$this->tkey,array('on_click_script'=>'log_viewer.php','on_expand_script'=>'ajax.php','on_expand_cmd'=>'upload_time_tree_items'));

  function generate_time_tree($tree_title,$tkey,$options=array())
  {    if (!$options['on_click_script'])  $options['on_click_script']='fra_viewer.php' ;
       if (!$options['on_expand_script']) $options['on_expand_script']='ajax.php' ;
       if (!$options['on_expand_cmd'])    $options['on_expand_cmd']='upload_object_tree_items' ;
       $table_name=_DOT($tkey)->table_name ;
       $usl_select_sql=($options['usl_select'])? ' and '.$options['usl_select']:''  ;
	   $stats=execSQL_van('select min(c_data) as first_data,max(c_data) as last_data from '.$table_name.' where parent!=0 '.$usl_select_sql) ;
	   $first_data=$stats['first_data'] ; //echo 'Первая запись: '.date('F Y',$first_data).'<br>' ;
	   //$last_data=$stats['last_data'] ; //echo 'Последняя запись: '.date('F Y',$last_data).'<br>' ;

	   $now=getdate($first_data) ;
	   $start_year=mktime(0, 0, 0, 1,1,$now['year']); // начало года первой записи

	   $now=getdate() ;
	   $start_day=mktime(0, 0, 0, $now['mon'],$now['mday'],$now['year']);  //echo date('d.m.y H:i:s',$start_day).'<br>' ; // начало дня
	   $end_day=mktime(23, 59, 29, $now['mon'],$now['mday'],$now['year']); // конец дня
	   $temp=($now['wday'])? ($now['wday']-1)*86400:7*86400 ;
	   $start_week=$start_day-$temp ; // echo date('d.m.y H:i:s',$start_week).'<br>' ;// начало недели
	   $end_week=$start_week+7*86400-1 ;  //echo date('d.m.y H:i:s',$end_week).'<br>' ; // конец недели
	   $start_month=mktime(0, 0, 0, $now['mon'],1,$now['year']); // начало месяца
	   //$end_month=mktime(0, 0, 0, $now['mon']+1,1,$now['year'])-1; // конец месяца
	   $start_yar=mktime(0, 0, 0, 1,1,$now['year']); // начало года
	   //$end_yar=mktime(0, 0, 0, 1,1,$now['year']+1)-1; // конец года

	   $stats_day=execSQL_van('select count(pkey) as cnt from '.$table_name.' where parent!=0 '.$usl_select_sql.' and c_data>='.$start_day) ;
	   $stats_week=execSQL_van('select count(pkey) as cnt from '.$table_name.' where parent!=0 '.$usl_select_sql.' and c_data>='.$start_week) ;

       ?><ul class="tree_root" tkey="<?echo $tkey?>" usl_select="<?echo $options['usl_select']?>" on_click_script=<?echo $options['on_click_script']?> on_expand_script=<?echo $options['on_expand_script']?> on_expand_cmd=<?echo $options['on_expand_cmd']?> <?if (sizeof($options['on_click_item'])) foreach($options['on_click_item'] as $clss=>$script_name) echo 'on_click_item_'.$clss.'='.$script_name.' ' ; if ($options['no_show_item']) echo 'no_show_item'?> <?if ($options['no_show_cnt'])  echo 'no_show_cnt'?>>
            <li clss=1 pkey=root><?echo $tree_title?></li>
                <ul>
                    <li clss=1 t1=<?echo $start_day?> t2=<?echo $end_day?> <? if ($stats_day['cnt'])  echo 'cnt="'.$stats_day['cnt'].'"'?> stop_data><span class=name>Сегодня, <? echo get_month_year($start_day,'day month'); if ($stats_day['cnt']) echo '</span><span class=cnt>('.$stats_day['cnt'].')</span>' ; ?></li>
                    <li clss=1 t1=<?echo $start_week?> t2=<?echo $end_week?> <? if ($stats_week['cnt'])  echo 'cnt="'.$stats_week['cnt'].'"'?> stop_data><span class=name>Неделя, <? echo date('d.m',$start_week).' - '.date('d.m',$end_week) ; if ($stats_week['cnt']) echo '</span><span class=cnt>('.$stats_week['cnt'].')</span>' ; ?></li>
       <?
	   // показываем месяцы текущего года
	   while($start_month>=$start_yar) // идем от текущего месяца
	      { $now=getdate($start_month) ;
	      	$end_month=mktime(0, 0, 0, $now['mon']+1,1,$now['year'])-1; // конец текущего месяца
	   		$stats=execSQL_van('select count(pkey) as cnt from '.$table_name.' where parent!=0 '.$usl_select_sql.' and c_data>='.$start_month.' and c_data<='.$end_month) ;
	   		if ($stats['cnt']) {?><li clss=1 t1=<?echo $start_month?> t2=<?echo $end_month?> cnt="<?echo $stats['cnt']?>" stop_data><span class=name><?echo get_month_year($start_month,'month year')?></span><span class=cnt>(<?echo $stats['cnt']?>)</span></li><?}
	      	$start_month=mktime(0, 0, 0, $now['mon']-1,1,$now['year']); // начало предудущего месяца
	      }

        // показываем года
       while($start_yar>=$start_year) // идем от текущего месяца
       { $now=getdate($start_yar) ;
         $end_year=mktime(0, 0, 0,1,1,$now['year']+1)-1; // конец текущего года
	     $stats=execSQL_van('select count(pkey) as cnt from '.$table_name.' where parent!=0 '.$usl_select_sql.' and c_data>='.$start_yar.' and c_data<='.$end_year) ;
	   	 if ($stats['cnt']) {?><li clss=1 t1=<?echo $start_yar?> t2=<?echo $end_year?> cnt="<?echo $stats['cnt']?>"><span class=name><?echo date('Y',$start_yar)?></span><span class=cnt>(<?echo $stats['cnt']?>)</span></li><?}
		 $start_yar=mktime(0, 0, 0, 1,1,$now['year']-1); // начало предудущего года
       }

      ?></ul></ul><?
  }

}
?>