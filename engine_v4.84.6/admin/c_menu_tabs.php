<?

class c_top_menu
{
  // заданы пункты меню и панели меню
  // панель - автоматически формируется для конкретного объекта, например, состав, свойства

  public $active_tabs=array() ; // активные страницы меню для комбинации таблица/класс



  function show($cur_menu,$options=array())
  { global $SF_REQUEST_URI,$debug_admin_menu ;
    if ($debug_admin_menu){?><h2>Опции, переданные в меню</h2><? damp_array($options,1,-1) ; }
    if (is_array($options['menu_set'])) $cur_menu=$options['menu_set'] ;
    $tkey=$options['tkey'] ;
    $pkey=$options['pkey'] ;
    $clss=$options['clss'] ;
    $menu_id='' ;
    $obj_clss=_CLSS($clss) ;

    // если указан класс объекта подключаем скрип с функциями класса - класс уже подключен при  _CLSS($clss) ;
    //if (isset($options['clss'])) if (include_class($options['clss']) and $debug_admin_menu) echo '<div class="green bold">Подключение /engine/class/clss_'.$options['clss'].'.php</div>' ;

    // при показе меню меню очищаем от кнопок верзхную панель
    /*?><script type=text/javascript>if (window.top!=undefined && window.top.fra_top_menu!=undefined) window.top.fra_top_menu.document.getElementById('test_div').innerHTML='' ;</script><?*/

    //print_r($options) ;
    //echo '<br>' ;
    // href - ссылка на скрипт текущей страницы админки, берется из $_SERVER['REQUEST_URI'] или имя скрипта с параметрами будет задано в $options['base_href']
    $href=($options['base_href'])? _PATH_TO_ADMIN.'/'.$options['base_href']:$SF_REQUEST_URI ;
    $href=$this->delete_pars_from_url($href,array('menu_id','no_action','rn')) ; // удаляем из адреса скрипта ряд параметров
    //echo $href ;
    //echo $tkey.'<br>' ;
    //echo $clss.'<br>' ;

    if (isset($_POST['menu_id'])) $menu_id=$_POST['menu_id'] ; // для выбора пунктов меню после отработки событий
    // при клике по пункту меню выбранный пункт меню передается через $_GET['menu_id]
    if (isset($_GET['menu_id']))  $menu_id=$_GET['menu_id'] ;

    //echo 'Создаем меню для класса '.$clss.'<br>' ;
    //echo 'Сохраненный id пункта меню: '.$menu_id.'<br>' ;
    if ($options['active_item']) $menu_id=$options['active_item'] ;
    if (!$menu_id) $menu_id=$this->active_tabs[$tkey][$clss] ; else $this->active_tabs[$tkey][$clss]=$menu_id ;
    //echo 'Активный пункт меню: '.$menu_id.'<br>' ;

    $menu_items=array() ; // массив пунктов меню, они могут быть различных классов, создается каждый раз при показе меню

   	$i=1 ;
    if ($debug_admin_menu){ ?><h2>Переданный список пунктов</h2><? damp_array($cur_menu,1,-1) ; }
    $ext_items_full=get_full_menu_items_to_page($options) ; //damp_array($ext_items_full) ;

    if (!sizeof($ext_items_full)) $ext_items_full=$cur_menu ;
    $ext_items_add=get_ext_menu_items_to_page() ;

    //damp_array($ext_items_add) ;
    if ($debug_admin_menu){ ?><h2>Дополнительный список пунктов</h2><? damp_array($ext_items_add,1,-1) ; }
    $cur_menu=array_merge($ext_items_full,$ext_items_add) ;



    // собираем пункты меню по описанию меню
   	if (sizeof($cur_menu)) foreach($cur_menu as $menu_rec)
   	{

         $cur_type=($menu_rec['type'])? $menu_rec['type']:'item' ;
         $cur_id=$cur_type.'_'.$i ; $i++ ;
         $menu_rec['id']=$cur_id ;
         if ($tkey and !isset($menu_rec['tkey'])) $menu_rec['tkey']=$tkey ;
         if ($pkey and !isset($menu_rec['pkey'])) $menu_rec['pkey']=$pkey ;
         // совместимость со старым форматом
         if ($menu_rec['function'])                  { $menu_rec['cmd']=$menu_rec['function'] ; $menu_rec['function']=''  ; }
         //if ($menu_rec['action'] and isset($clss))   { $menu_rec['cmd']='cmd_show_clss_'.$clss.'_page_'.$menu_rec['action'] ; $menu_rec['action']=''  ; } // более не используется
         if ($menu_rec['action'] and isset($clss))   { $menu_rec['cmd']='panel_action_'.$menu_rec['action'] ; $menu_rec['action']=''  ; }

         if ($cur_type=='item') $menu_items[$menu_rec['id']] = new c_menu_item($menu_rec) ;  // создаем простой пункт меню
         else // создаем специализированные пункты меню - обычно для объектов
         {  // ограничения показа при задании правил наследования
            $show=1 ;
            if ($cur_type=='structure' and isset($obj_clss->parent_to) and !sizeof($obj_clss->parent_to)) $show=0 ;
            if ($cur_type=='create' and isset($obj_clss->parent_to) and !sizeof($obj_clss->parent_to)) $show=0 ;
            if ($show)
	         {  $class_panel='c_menu_panel_'.$cur_type ;
	            $menu_panel = new $class_panel($tkey,$pkey,$clss) ;
	            if (sizeof($menu_panel->panel_items)) $menu_items=array_merge($menu_items,$menu_panel->panel_items) ;
	         }
   		 }
   	}

    // если в описании класса есть указание показывать метатеги - добавляем пунтк меню  "SEO"
    if (isset($clss) and $obj_clss->SEO_tab)  $menu_items['metatags']=new c_menu_item_metatags() ;

    // если в корзине есть что-то, показываем пункт меню для корзины
    if ($_SESSION['cart_admin']->cnt) $menu_items['cart']=new c_menu_item_cart() ;

    // если в линках есть объекты этого объекта - показываем объекты отдельными вкладками
    if ($pkey and $tkey)
    { $links_cnt=execSQL_value('select count(o_id) as cnt from '.TM_LINK.' where (p_id='.$pkey.' and p_tkey='.$tkey.') or (o_id='.$pkey.' and o_tkey='.$tkey.')') ;
      if ($links_cnt) $menu_items['link']=new c_menu_item_link($links_cnt) ;
    }
    //damp_array($links) ;

    //damp_array($menu_items) ;
    // показывем пункты меню
    ?><div id="viewer_menu"><? if (sizeof($menu_items)) foreach($menu_items as $obj_item) $obj_item->show($href) ; ?></div><div class=clear></div><?

    // если не обин из пунктов меню не выбран - выбираем первый в списке
    if (!isset($menu_items[$menu_id])) { reset($menu_items); list($menu_id,$menu_item)=each($menu_items) ; }

    // подствечиваем выбранный пункт меню
    ?><script type="text/javascript">$j('#a_<?echo $menu_id?>').addClass('sel');</script><?

    //для совместимости со старым вариантом меню
    global $menu_action ;
    if (isset($menu_items[$menu_id]->action)) $menu_action=$menu_items[$menu_id]->action ;
    else if (isset($menu_items[$menu_id]->rec['function'])) $menu_action=$menu_items[$menu_id]->rec['function'] ;
    else if (isset($menu_items[$menu_id]->rec['cmd'])) $menu_action=$menu_items[$menu_id]->rec['cmd'] ;

    $menu_items[$menu_id]->rec['id']=$menu_id ;

    // возращаем запись по выбранному пункту меню
    // для вызова функции соответствующей пункту меню, в rec['cmd'] должно быть имя функции, метода или расширения
    if ($debug_admin_menu){ ?><h2>Выбранный пункт меню</h2><? damp_array($menu_items[$menu_id]->rec,1,-1) ; }
    return($menu_items[$menu_id]->rec) ;
  }

  // удаляет из пути скрипта указанные параметры
  function delete_pars_from_url($url,$pars=array())
  { $arr=@parse_url($url) ; //damp_array($arr) ;
  	parse_str($arr['query'],$arr_query) ;
  	if (sizeof($pars)) foreach($pars as $par_name) unset($arr_query[$par_name]);
  	$res_url=$arr['path'] ;
  	if (sizeof($arr_query)) foreach($arr_query as $key=>$value) $arr_query_new[]=$key.'='.$value ;
  	if (sizeof($arr_query_new)) $res_url.='?'.implode('&',$arr_query_new) ;
  	return($res_url) ;
  }

}



// стандартная вкладка меню
class c_menu_item
{  function c_menu_item($rec) // конструктор пункта меню
   { global $patch_to_img  ;
   	 if ($rec['icon'] and strpos($rec['icon'],'<img')!==false) { $this->icon=$rec['icon'] ; $rec['icon']='' ; }
     else if ($rec['icon']) $this->icon='<img src="'.$patch_to_img.$rec['icon'].'" width="16" height="16" alt="" border="0" />' ;
	 else $this->icon='<img src="'._PATH_TO_ADMIN_IMG.'/obj_prop.gif" width="16" height="16" alt="" border="0" />' ;
     $this->rec=$rec ;
   }

  function show($href)
  { echo '<a id=a_'.$this->rec['id'].' href="'.$href.'&amp;menu_id='.$this->rec['id'].'"><div class=clss_icon>'.$this->icon.'</div> '.$this->rec['name'].'</a>' ; }

}

// панель меню - структура
class c_menu_panel_structure
{
   public $panel_items = array() ;

   function c_menu_panel_structure($tkey,$pkey,$clss)
   { $this->id='structure' ;
     if (!$tkey or !$pkey) return ;

     $obj_childs_info=select_obj_childs_clss_cnt($pkey.'.'.$tkey) ; // получаем список классов, которые уже добавлены к текущему объекту
     // пункты меню по существующим дочерним объектам
     if (sizeof($obj_childs_info)) foreach($obj_childs_info as $show_child_clss=>$cnt)
       if ($show_child_clss!=6)
          { $obj_clss=_CLSS($show_child_clss);
            $item_rec['id']=$this->id.'_'.$show_child_clss ; ;
            $item_rec['name']=$obj_clss->name($pkey.'.'.$tkey);
            $item_rec['icon']='<img src="'.$obj_clss->icons.'" alt="" border="0" />' ;

            $this->panel_items[$item_rec['id']]=new c_menu_panel_structure_item($item_rec,$clss,$show_child_clss) ;
          }
     // пункт меню "Добавить"
     $obj_create_info=_CLSS($clss)->get_adding_clss($tkey,$pkey) ; //получаем список классов, которые могут быть добавлены к текущему объекту
     if (sizeof($obj_create_info))
         {  $item_rec['id']=$this->id ; ;
            $item_rec['name']='Добавить' ;
            $item_rec['icon']='<img src="'._PATH_TO_ADMIN_IMG.'/add_icon.png" width="16" height="17" alt="" border="0" />' ;
            $this->panel_items[$item_rec['id']]=new c_menu_panel_structure_item($item_rec,$clss) ;
         }
   }

}

class c_menu_panel_structure_item extends c_menu_item
{
  function c_menu_panel_structure_item($rec,$clss,$show_child_clss=-1) // конструктор пункта меню
   { parent::c_menu_item($rec) ;
     if ($show_child_clss>-1) { $this->rec['show_clss']=$show_child_clss ;
                                $this->rec['cmd']='cur_obj_show_list_children'  ;
                              }
     else                     $this->rec['cmd']='cur_obj_child_create_select' ;
   }
}


// панель меню - свойства
// если в свойствах есть HTML поля - развернуть в виде вкладок
class c_menu_panel_prop
{
  public $panel_items = array() ;

   function c_menu_panel_prop($tkey,$pkey,$clss)
   {
     $item_rec['id']='prop' ;
     $item_rec['name']='Свойства' ;
     $item_rec['icon']='<img src="'._PATH_TO_ADMIN_IMG.'/obj_prop.gif" width="16" height="16" alt="" border="0" />' ;

     $this->panel_items[$item_rec['id']]=new c_menu_panel_prop_item($item_rec,$clss) ;
     $obj_clss=_CLSS($clss) ;

     // добавляем в меню редактируемые HTML или TXT поля
     //damp_array(_DOT($tkey)) ;
	 //if (sizeof(_DOT($tkey)->list_det_edit[$clss])) foreach(_DOT($tkey)->list_det_edit[$clss] as $fname=>$field_info)
	 if (sizeof($obj_clss->view['details']['field'])) foreach($obj_clss->view['details']['field'] as $fname=>$field_info)
       //if (is_array($field_info) and $field_info['use_HTML_editor'])  // Смотрим, не указано ли явно использование HTML редактора)
       if ($obj_clss->use_HTML_editor(array(),$fname))  // Смотрим, не указано ли явно использование HTML редактора)
	   {  //echo 'fname='.$fname.'<br>' ;
          //print_r($field_info);  echo '<br>' ;
          $show_tabs_HTML_editor=$field_info['use_HTML_editor'] ; 	// 1. Смотрим, не указано ли явно использование HTML редактора
		  //echo $fname.'='._DOT($tkey)->list_clss_ext[$clss]['multilang'][$fname].'<br>' ;
		  //if ($fname!='description' and $fname!='keywords' and $obj_clss->fields[$fname]=='text' and $field_info['edit_element']!='input' and $field_info['class']!='small')
		  if ($show_tabs_HTML_editor)
          { if (isset(_DOT($tkey)->list_clss_ext[$clss]['multilang'][$fname]))
            { // вывод нескольких вкладок для мультиязычных полей
              if (sizeof($GLOBALS['lang_arr'])) foreach($GLOBALS['lang_arr'] as $lang_info)
              { $lang_fname=$fname.$lang_info['suff'] ;
                if (isset(_DOT($tkey)->field_info[$lang_fname]))
	            {
                   $item_rec['id']='prop_'.$lang_fname ;
                   $item_rec['name']=(!(is_array($field_info))? $field_info:$field_info["title"]).' ('.$lang_info['name'].')'  ;
                   $item_rec['icon']='<img src="'._PATH_TO_ADMIN_IMG.'/doc_html.gif" width="16" height="16" alt="" border="0" />' ;

                   $this->panel_items[$item_rec['id']]=new c_menu_panel_prop_item($item_rec,$clss,$lang_fname) ;
                } else echo 'В таблице отсутсвует поле '.$lang_fname.'<br>' ;
              } else echo '$GLOBALS[lang_arr] - не задан, вкладка выведена не будет<br>' ;
            }
	       else
	       { // вывод одной вкладки
             $item_rec['id']='prop_'.$fname ;
             $item_rec['name']=!(is_array($field_info))? $field_info:$field_info["title"] ;
             $item_rec['icon']='<img src="'._PATH_TO_ADMIN_IMG.'/doc_html.gif" width="16" height="16" alt="" border="0" />' ;

             $this->panel_items[$item_rec['id']]=new c_menu_panel_prop_item($item_rec,$clss,$fname) ;
	       }
       }

   }
   }

}

class c_menu_panel_prop_item extends c_menu_item
{
  function c_menu_panel_prop_item($rec,$clss,$fname='') // конструктор пункта меню
   { parent::c_menu_item($rec) ;
     if ($fname) $this->rec['fname']=$fname ;
     if ($this->rec['fname'])  $this->rec['cmd']='cur_obj_show_HTML_editor'  ; // функция показа выбранного поля в HTML редакторе
     else                      $this->rec['cmd']='cur_obj_show_props'  ;
   }
}

// вкладка меню - корзина  type = cart
class c_menu_item_cart extends c_menu_item
{
  function c_menu_item_cart()
   { if (sizeof(_CUR_PAGE()->obj_info)) $allow_clss=_CLSS(_CUR_PAGE()->obj_info['clss'])->get_adding_clss(_CUR_PAGE()->obj_info['tkey'],_CUR_PAGE()->obj_info['pkey']) ;
     else                               $allow_clss=_CLSS(0)->get_adding_clss(_CUR_PAGE()->tkey) ;

     $cnt=0 ;
     // проверяем возможность добавления объектов их корзины как ссылки в текущий объект
     $arr_links_clss=_CLSS(100)->get_arr_child_clss() ;
     $arr_allow_clss=array_keys($allow_clss);
     $res_arr=array_intersect($arr_links_clss,$arr_allow_clss) ;
     $copy_as_links=(sizeof($res_arr))? 1:0;
     $copy_as_links=1 ;

     // проверяем возможность добавляния объектов из корзины в текущий объект
     if (sizeof($_SESSION['cart_admin']->list_check)) foreach ($_SESSION['cart_admin']->list_check as $tkey=>$tkey_arr)
       	if (sizeof($tkey_arr)) foreach ($tkey_arr as $clss=>$clss_arr)
             if (isset($allow_clss[$clss]) or $copy_as_links) $cnt+=sizeof($clss_arr) ;


     if ($cnt)
     { $this->rec['id']='cart' ;
       $this->rec['name']='Копировать ['.$cnt.']' ;
       $this->rec['cmd']='show_cart' ;
	   $this->icon='<img src="'._PATH_TO_ADMIN_IMG.'/obj_cart.gif" width="16" height="16" alt="" border="0" />' ;
     }
   }
}

// вкладка меню - метатаги  type = metatags
class c_menu_item_metatags extends c_menu_item
{
  function c_menu_item_metatags()
   { $this->icon='<img src="'._PATH_TO_BASED_CLSS_IMG.'/man.jpg" width="16" height="16" alt="" border="0" />' ;
     $this->rec['id']='metatags' ;
     $this->rec['name']='SEO' ;
     $this->rec['cmd']='show_SEO' ;
   }
}

// вкладка меню - метатаги  type = link
class c_menu_item_link extends c_menu_item
{
  function c_menu_item_link()
   { $this->icon='<img src="'._PATH_TO_BASED_CLSS_IMG.'/man.jpg" width="16" height="16" alt="" border="0" />' ;
     $this->rec['id']='link' ;
     $this->rec['name']='Линки' ;
     $this->rec['cmd']='show_LINK' ;
   }

  function show($href)
  { echo '<a id=a_'.$this->rec['id'].' href="'.$href.'&amp;menu_id='.$this->rec['id'].'&amp;link_tkey='.$this->rec['link_tkey'].'&amp;link_clss='.$this->rec['link_clss'].'"><div class=clss_icon>'.$this->icon.'</div> '.$this->rec['name'].'</a>' ; }

}

function get_ext_menu_items_to_page()
{ parse_str(_CUR_PAGE_QUERY,$in_params) ;
  $menu_item=array() ;
  if (sizeof($_SESSION['ext_panel_func'])) foreach($_SESSION['ext_panel_func'] as $rule)
  {  $i=0 ;
     if (sizeof($rule['frame'])) foreach($rule['frame'] as $param=>$value)
      { if (!is_array($value)) { if ($in_params[$param]==$value) $i++ ; }
        elseif (sizeof($value)) foreach($value as $val) if ($in_params[$param]==$val) { $i++ ; break ; }
      }
     if ($i==sizeof($rule['frame']) and sizeof($rule['menu_item'])) // если выполнены все условия
       $menu_item[]=$rule['menu_item'] ;
  }
  return($menu_item) ;
}

function get_full_menu_items_to_page($options=array())
{ parse_str(_CUR_PAGE_QUERY,$in_params) ;
  $menu_item=array() ;
    //trace() ;
  //damp_array($_SESSION['ext_panel_func']) ;

  if (sizeof($_SESSION['ext_panel_func'])) foreach($_SESSION['ext_panel_func'] as $rule)
  {  $i=0 ;
     if (sizeof($rule['frame'])) foreach($rule['frame'] as $param=>$value) if ($in_params[$param]==$value) $i++ ;
     if ($i==sizeof($rule['frame']) and sizeof($rule['menu_items'])) foreach($rule['menu_items'] as $rule_item) $menu_item[]=$rule_item ; // если выполнены все условия
     $i=0 ;
     if (sizeof($rule['options'])) foreach($rule['options'] as $param=>$value) if ($options[$param]==$value) $i++ ;
     if ($i==sizeof($rule['options']) and sizeof($rule['menu_items'])) foreach($rule['menu_items'] as $rule_item) $menu_item[]=$rule_item ; // если выполнены все условия
  }
  return($menu_item) ;
}



?>