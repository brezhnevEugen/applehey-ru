<? 	// скрипт для вывода изображений, если к исходникам заблокирован доступ
	// может работать как через использование заранее подготовленных клонов, так и через
	// резайс исходного файла на нужный размер
	//  входные параметры
	//   tkey   - код таблицы
	//   pkey	- код фото
	//	 mode	- название клона который подо показать
	//   name	- путь к фото
	//  если при обращении к клонам изображений их нет, они генерируются здесь
 	include_once(_DIR_TO_ENGINE.'/c_objects.php');
 	connect_database() ;
    session_start() ; // подключаем текущую сессиию


 	include_once('i_img_working.php');

 	if ($tkey and $pkey)
 	{ 	if (!sizeof(_DOT($tkey))) return ;
 	  	if (!sizeof(_DOT($tkey)->list_clss_ext[3])) return ;

	 	$img_dir=_DOT($tkey)->dir_to_file ;
	 	$table_name=_DOT($tkey)->table_name ;
	 	$clone_info=$_SESSION['img_pattern'][_DOT($tkey)->img_pattern] ;

	 	$img_info=execSQL_van('select * from '.$table_name.' where pkey="'.$pkey.'"') ;
	 	$fname=$img_info['obj_name'] ;

	 	if (!$fname) return ;

	 	if ($mode!='small' and !isset($clone_info[$mode])) unset($mode) ;

	 	if ($mode and !file_exists($img_dir.$mode.'/'.$fname)) // если надо показать клон, а клона нет
	 	{ // генерим клон
	 	  if ($mode=='small') create_preview_image($img_dir,$fname) ;
	 	   				 else create_clone_image($tkey,$fname,array('clone'=>$mode)) ;
	 	}

	 	working_image($img_dir.$mode.'/'.$fname) ;
    }

    if ($name)
    {
       if ($clone and $tkey) working_image($name,_DOT($tkey)->list_clone_img($clone)) ;
       else if ($name and $mode=='small')
         { $options['resize']=array('width'=>100,'height'=>100,'fields'=>1,'resampled'=>1,'back_color'=>'FFFFFF','quality'=>100);
           working_image($name,$options);
         }
        else working_image($name) ;
    }

?>


