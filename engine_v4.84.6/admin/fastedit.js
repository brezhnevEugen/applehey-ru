function show_obj(tkey,pkey,edit) {parent.window.open('/admin/fra_viewer.php?tkey='+tkey+'&pkey='+pkey+'&edit='+edit,'obj_show_'+tkey,'status=1,location=0,dependent=1,width=800,height=600, resizable=1, scrollbars=yes');}

$j(document).ready(function() {$j(document).fastedit()}) ;

$j.fn.fastedit = function()
  { $j('span.fed').each(function(){ var div_parent=$j(this).parent() ;
                                    $j(div_parent).addClass('fed') ;
                                    $j(div_parent).attr('fname',$j(this).attr('fname')) ;
                                    $j(div_parent).attr('reffer',$j(this).attr('reffer')) ;
                                    $j(this).remove() ;
                                  }) ;
    /*$j('_span.feda').each(function(){ var div_parent=$j(this).parent() ;
                                    $j(div_parent).addClass('fed') ;
                                    $j(div_parent).attr('id','editable') ;
                                    $j(div_parent).attr('fname',$j(this).attr('fname')) ;
                                    $j(div_parent).attr('reffer',$j(this).attr('reffer')) ;
                                    $j(this).remove() ;
                                  }) ;*/
    $j('span.fe').each(function(){ if ($j(this).parent().css('position')!='absolute') $j(this).parent().css({position:'relative'}) ;}) ;
    $j('span.fe').live('click',function()
    { // открываем окно для редактирования объекта
      var newWin = parent.window.open('/admin/fra_viewer.php?obj_reffer='+$j(this).attr('reffer'),'obj_show_'+$j(this).attr('reffer'),'status=1,dependent=1,location=0,width=800,height=600, resizable=1, scrollbars=yes');
      if (newWin!=null) newWin.focus() ;
      else alert('Настройки браузера запрещают открытие дополнительных окон') ;
      return(false) ;
    }) ;

    $j('.fed[fname][reffer]').each(function(){$j(this).attr('contenteditable','true')}) ;

    CKEDITOR.on( 'instanceCreated', function( event )
    {
        var editor = event.editor,
            element = editor.element;
        // Customize editors for headers and tag list.
        // These editors don't need features like smileys, templates, iframes etc.
        if ( element.is( 'h1', 'h2', 'h3' ) || element.getAttribute( 'id' ) == 'taglist' )
        {
            // Customize the editor configurations on "configLoaded" event,
            // which is fired after the configuration file loading and
            // execution. This makes it possible to change the
            // configurations before the editor initialization takes place.
            editor.on( 'configLoaded', function() {

                // Remove unnecessary plugins to make the editor simpler.
                editor.config.removePlugins = 'colorbutton,find,flash,font,' +
                    'forms,iframe,image,newpage,removeformat,' +
                    'smiley,specialchar,stylescombo,templates';

                // Rearrange the layout of the toolbar.
                editor.config.toolbarGroups = [
                    { name: 'editing',		groups: [ 'basicstyles', 'links' ] },
                    { name: 'undo' },
                    { name: 'clipboard',	groups: [ 'selection', 'clipboard' ] },
                    { name: 'about' }
                ];
            });
        }

        editor.on('blur',function( e )
        {
          var params = {} ;
          params['cmd']='ckeditor_v4.4.4_full/save';
          var el=editor.container ;
          params['reffer']=$j(el).attr('reffer');
          params['fname']=$j(el).attr('fname');
          params['value']=editor.getData();

          $j.ajax({url:'/ajax.php',type:'POST',dataType:'xml',cache:false,data:params});

        });
    });
  };