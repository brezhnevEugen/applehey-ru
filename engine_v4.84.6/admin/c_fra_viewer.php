<?
//-----------------------------------------------------------------------------------------------------------------------------
// фрейм объекта
//-----------------------------------------------------------------------------------------------------------------------------

include_once('c_fra_based.php') ;
class c_fra_viewer extends c_fra_based
{ public $obj_info ;

 function body($options=array())
 { if (!$this->table_name or  !$this->tkey) { echo '<div class=alert>Не задана рабочая таблица для fra_viewer.php</div>' ; _exit() ; }
   $this->body_frame($options) ;
 }

 function on_send_header_before($options=array())
 {  global $list_obj_sort_field,$list_obj_sort_mode;
    // значения по текущей сортировке храним в кукисах, а не в сессии - чтобы значения сортировки сохранялись между сессиями
    $list_obj_sort_field=unserialize(stripslashes($_COOKIE['list_obj_sort_field'])) ;
    $list_obj_sort_mode=unserialize(stripslashes($_COOKIE['list_obj_sort_mode'])) ;
    // сохранеям в глобалтных переменных, значения, передеанные через сессию
    if (isset($_POST['_list_obj_sort_field']) and sizeof($_POST['_list_obj_sort_field'])) { foreach($_POST['_list_obj_sort_field'] as $tkey=>$arr_clss) foreach($arr_clss as $clss=>$value) $list_obj_sort_field[$tkey][$clss]=$value ; setcookie('list_obj_sort_field',serialize($list_obj_sort_field)) ;}
    if (isset($_POST['_list_obj_sort_mode']) and sizeof($_POST['_list_obj_sort_mode']))   foreach($_POST['_list_obj_sort_mode']  as $tkey=>$arr_clss) foreach($arr_clss as $clss=>$value) $list_obj_sort_mode[$tkey][$clss]=$value ;
    // сохраняем все значения в кукисах
    //setcookie('list_obj_sort_field',serialize($list_obj_sort_field)) ;
    setcookie('list_obj_sort_mode',serialize($list_obj_sort_mode)) ;
 }

 function select_obj_info()
  { global $tkey,$pkey,$title_page,$obj_info,$cur_table ;
    if ($pkey and $tkey)
    { $this->obj_info=select_db_obj_info($tkey,$pkey,array('get_child_obj'=>3,'debug'=>0,'show_error_is_table_not_found'=>1)) ; // damp_array($this->obj_info) ;
      _CLSS($this->obj_info['clss'])->prepare_public_info($this->obj_info) ;
    }
	if ($this->table_name) { if ($this->table_name==TM_DOT) $cur_table=_DOT($obj_info['pkey']) ; else $cur_table=_DOT($tkey) ; }

	$title_page=$obj_info['obj_name'] ;
    if (is_array($this->obj_info)) $GLOBALS['obj_info']=$this->obj_info ;  // глобалим obj_info - временно

    if (!is_object($_SESSION['top_menu'])) $_SESSION['top_menu']=new c_top_menu() ;
  }

 // $menu_state[tkey][clss] - текущая страница/поле/класс для таблица/класс

  function _show_page_obj_header($obj_info)
  {  $this->panel_page_title($obj_info) ; return ;

     $obj_clss=_CLSS($obj_info["clss"]);
     if (!$obj_info['__name']) $obj_info['__name']=$obj_info['obj_name'] ;
     ?><table class=obj_header><tr><?
       if ($obj_info['pkey']>0)
        {  if ($obj_info['_image_name']) { ?><td class=img><img src="<? echo img_clone($obj_info,'small') ; ?>" width=50 height=50></td><td><?  $br='<br>' ;}
           else                          { ?><td class=icon><div class=clss_icon><img class=clss src="<? echo $obj_clss->icons?>" width="16" height="64" alt="" border="0" /></div><? $br='&nbsp;'; ;}

           $title='<span class=clss_name>'.$obj_clss->name($obj_info['_parent_reffer'])."</span>".$br."'".mb_substr($obj_info['__name'],0,40).((strlen($obj_info['__name'])>40)? '...':'')."'" ;
           $title_func_name='clss_'.$obj_info['clss'].'_frame_title' ;
           if (function_exists($title_func_name)) $title=$title_func_name($obj_info,$title) ;
          echo $title ;?></td><?
        }
        else echo '<td>'.$obj_info['obj_name'].'</td>' ;
       ?></tr></table><?
    }

  // панель вывода название объекта текущей страницы
  function panel_page_title($obj_info)
  {  $obj_clss=_CLSS($obj_info["clss"]);
     if (!$obj_info['__name']) $obj_info['__name']=$obj_info['obj_name'] ;
     if ($obj_info['pkey']>0)
        {  if ($obj_info['_image_name']) $img_src=img_clone($obj_info,'small') ;
           else                          $img_src=$obj_clss->icons ;

           $name='<span class=clss_name>'.$obj_clss->name($obj_info['_parent_reffer'])."</span> '".mb_substr(strip_tags($obj_info['__name']),0,40).((strlen($obj_info['__name'])>40)? '...':'')."'" ;
           $title_func_name='clss_'.$obj_info['clss'].'_frame_title' ;
           if (function_exists($title_func_name)) $name=$title_func_name($obj_info,$name) ;
        }
        else $name=$obj_info['obj_name'] ;

     ?><div id="panel_page_title"><? echo $name ?></div><?
  }


  function show_page_obj_menu($obj_info)
  {  if (!$obj_info['pkey']) return(array()) ;
     // создаем объект top_menu
     $top_menu_name='top_menu_'.$obj_info['tkey'].'_'.$obj_info['clss'] ;
     if (!isset($_SESSION[$top_menu_name])) $_SESSION[$top_menu_name]=new c_top_menu() ;
     // формируем набор пунктом меню
     $obj_clss=_CLSS($obj_info['clss']);
     $menu_set=$obj_clss->get_menu_set_to_table($obj_info['tkey'],$obj_info['pkey']) ;
     //damp_array($menu_set) ;
     $menu_options=array('tkey'=>$obj_info['tkey'],'pkey'=>$obj_info['pkey'],'clss'=>$obj_info['clss']) ;
     // выводим меню
     $menu_item=$_SESSION[$top_menu_name]->show($menu_set,$menu_options) ;  // показываем меню
     return($menu_item) ;
  }

}
?>