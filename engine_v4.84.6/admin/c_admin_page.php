<?php
  include_once (_DIR_TO_ENGINE.'/c_core_HTML.php') ;
  include_once (_DIR_TO_ENGINE.'/admin/i_clss_func.php') ;
  include_once (_DIR_TO_ENGINE.'/admin/c_menu_tabs.php') ;

 // ----------------------------------------------------------------------------------------------------------------------------
 // базовый класс для всех страниц админки
 // ----------------------------------------------------------------------------------------------------------------------------

class c_admin_page extends c_core_HTML
{ public $tkey ;
  public $table_name ;
  public $pkey ;

  public $cmd ;
  public $cmd_file ;
  public $cmd_mode ;
  public $cmd_script ;

  public $start_time ;
  public $end_time ;
  public $cur_menu_item=array() ;
  public $DOCTYPE='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">' ;
  public $HTML_TAG=array('<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">','</html>') ;

 function c_admin_page(&$options=array()) // конструктор
  {  // храним входные переменных в экземпляре страницы
     if (isset($_POST['tkey']))  $this->tkey=$_POST['tkey'] ; elseif (isset($_GET['tkey'])) $this->tkey=$_GET['tkey'] ;
     if (isset($_POST['pkey'])) $this->pkey=$_POST['pkey'] ; elseif (isset($_GET['pkey'])) $this->pkey=$_GET['pkey'] ;
     if ($_GET['obj_reffer']) list($this->pkey,$this->tkey)=explode('.',$_GET['obj_reffer']) ;
     if ($_GET['reffer']) list($this->pkey,$this->tkey)=explode('.',$_GET['reffer']) ;
     if ($options['use_table'])      $this->tkey=$_SESSION['pkey_by_table'][$options['use_table']] ;
     if ($options['use_table_code']) $this->tkey=$_SESSION['pkey_by_table'][$GLOBALS[$options['use_table_code']]] ;
     // определяемся с подсистемой - она в первую очередь будет определять рабочий tkey
     if (isset($this->system_name) and is_object($_SESSION[$this->system_name])) $this->system=&$_SESSION[$this->system_name] ; // указание подсистемы через объявление переменной в классе:  public $system_name='news_system' ;
     if (!isset($this->system) and  $options['system'] and is_object($_SESSION[$options['system']])) { $this->system=&$_SESSION[$options['system']] ; $this->system_name=$this->system->system_name ; } // указание подсистемы через опцию в скрипте фрейма:  $options["system"]="news_system" ;
     if (isset($this->system) and is_object($this->system)) $this->tkey=$this->system->tkey ;
     if (isset($this->tkey) and isset($_SESSION['tables'][$this->tkey])) $this->table_name=$_SESSION['tables'][$this->tkey] ;
     $this->frame_url=_CUR_PAGE_PATH ;
     // временно для совместимости со старым кодом
     $GLOBALS['tkey']=$this->tkey ;
     $GLOBALS['pkey']=$this->pkey ; //  damp_array($this) ;

     $is_html_doc=(!$options['no_html_head'] and !$options['no_html_doc_type'] and ($options['content_type']=='' or $options['content_type']=='text/html'))? 1:0 ;
     if (!$is_html_doc) { echo 'Используйте для не HTML страниц собственный скрипт класса.' ; _exit() ; }
     // вносим в класс параметры, переданные через get  или post
     $this->check_GET_params() ;
     // проверка хоста и при необходимости направление на основной хост или
     $this->check_location($options) ;
     // эта функция должна быть тут на тот случай, что если потребуется устновка куки, это должно быть сделано до отправки первой информации на сайт
     $this->on_send_header_before($options) ;
     // обработка входных данных до определения текущего объекта
     $this->command_execute($options) ; $this->cmd_exec_before_select_obj_info($options) ; set_time_point('cmd_exec_before_select_obj_info - OK') ;
     // получить из базы инфоримацию по ткущему объекту
     $this->select_obj_info() ; set_time_point('select_obj_info - OK') ;
     // еще одна обработка обработка входных данных - после обработки текущего объекта
     $this->cmd_exec() ; $this->cmd_exec_after_select_obj_info($options) ;  set_time_point('cmd_exec_after_select_obj_info - OK') ;
  }

  function init(&$options = Array())
  {

  }

  function check_autorize()
  { if ($_SESSION['member']->login==_CURRENT_USER) return(1) ;
    else  return($_SESSION['admin_account_system']->login()) ;
  }


  function show(&$options=array())
  {  header("Content-type: text/html;charset=UTF-8");
     $this->HTML($options) ;
  }

  // отдаем HTML контент
  // пока оставляем как есть, но в следующей версии необходимо убрать обработку опций  content_type,no_html_doc_type,no_html_head
  // теперь вывод XML страниц только через свой скрипт c_page_XML
  function HTML($options=array())
    {
      echo $this->DOCTYPE ; // <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
      echo $this->HTML_TAG[0] ; // <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

      $this->site_top_head($options) ;  // <html>

      $this->body($options) ;

      echo $this->HTML_TAG[1] ; // </html>
    }


 function check_GET_params($options=array())
 {
   //damp_array($this) ;
   if (isset($_POST['cmd']))  $this->cmd=$_POST['cmd'] ;       elseif (isset($_GET['cmd']))  $this->cmd=$_GET['cmd'] ;          unset($_POST['cmd'],$_GET['cmd']) ;
   // компенсация разбора ext/cmd в из $_POST['cmd] в i_system
   // добавлно, так как в i_system происходит разбор cmd на cmd+ext
   if (isset($_POST['ext']))  $this->cmd=$_POST['ext'].'/'.$this->cmd ;   elseif (isset($_GET['ext']))  $this->cmd=$_GET['ext'].'/'.$this->cmd ;      unset($_POST['ext'],$_GET['ext']) ;
   // другие параметры
   if (isset($_POST['file'])) $this->cmd_file=$_POST['file'] ; elseif (isset($_GET['file'])) $this->cmd_file=$_GET['file'] ;
   if (isset($_POST['mode'])) $this->cmd_mode=$_POST['mode'] ; elseif (isset($_GET['mode'])) $this->cmd_mode=$_GET['mode'] ;
   if (isset($_POST['script'])) $this->cmd_script=$_POST['script'] ; elseif (isset($_GET['script'])) $this->cmd_script=$_GET['script'] ;

   if (isset($_POST['start_time'])) $this->start_time=$_POST['start_time'] ; elseif (isset($_GET['start_time'])) $this->start_time=$_GET['start_time'] ;
   if (isset($_POST['end_time']))   $this->end_time=$_POST['end_time'] ;     elseif (isset($_GET['end_time']))   $this->end_time=$_GET['end_time'] ;
   if (isset($_POST['t1']))         $this->start_time=$_POST['t1'] ;         elseif (isset($_GET['t1']))         $this->start_time=$_GET['t1'] ;
   if (isset($_POST['t2']))         $this->end_time=$_POST['t2'] ;           elseif (isset($_GET['t2']))         $this->end_time=$_GET['t2'] ;
   if (strpos($this->start_time,'.')!==false) $this->start_time=StrToData($this->start_time) ;
   if (strpos($this->end_time,'.')!==false) $this->end_time=StrToData($this->end_time) ;
   $this->start_time=intval($this->start_time)  ;
   $this->end_time=intval($this->end_time)  ;

 }

  function check_location($options=array()) {}
  function on_send_header_before($options=array()) {}
  function command_execute($options=array()) {}
  function cmd_exec_before_select_obj_info($options=array()) {}
  function select_obj_info() {}
  function cmd_exec() {}
  function cmd_exec_after_select_obj_info($options=array()) {}

  function site_top_head($options=array())
  { ?><head>
        <title><? echo $_SESSION['LS_def_page_title'].' ' ; if (isset($this)) $this->page_title($options['title']); ?></title>
        <META http-equiv=Content-Type content="text/html; charset=utf-8" />
        <META NAME="Document-state" CONTENT="Dynamic" />
        <META NAME="revizit-after" CONTENT="5 days" />
        <META name="revisit"  content="5 days" />
        <? if ($_SESSION['favicon']){?><link href="<?echo $_SESSION['favicon']?>" rel="shortcut icon"><? echo "\n\t" ;} ?>
        <script type="text/javascript">
            var _PATH_TO_ADMIN='<?echo _PATH_TO_ADMIN?>/' ;
            var _PATH_TO_ADMIN_ADDONS='<?echo _PATH_TO_ADMIN_ADDONS?>' ;
            var _CKEDITOR_='<?echo _CKEDITOR_?>' ;
            var _EXT='<?echo _EXT?>' ;
            var CKEDITOR ;        </script>

        <script type="text/javascript" src="<?echo _PATH_TO_ADMIN_ADDONS?>/jquery-1.8.3.min.js"></script>
        <script type="text/javascript" src="<?echo _PATH_TO_ADMIN_ADDONS?>/mootools/mootools-core-1.3.1.js" ></script>
       	<script type="text/javascript" src="<?echo _PATH_TO_ADMIN_ADDONS?>/mootools/mootools-more-1.3.1.1.js" ></script>

        <script type="text/javascript" src="<?echo _PATH_TO_ADMIN_ADDONS?>/mBox/jquery.form.js" ></script> <?/*используется ajaxForm для отправки форм через ajax*/?>

        <script type="text/javascript" src="<?echo _PATH_TO_ADMIN_ADDONS?>/jquery-ui-1.11.4/jquery-ui.js"></script>
        <link rel="stylesheet" type="text/css" href="<?echo _PATH_TO_ADMIN_ADDONS?>/jquery-ui-1.11.4/jquery-ui.css"/>

        <script type="text/javascript" src="<?echo _PATH_TO_ADMIN_ADDONS?>/mBox/jBox-master/Source/jBox.min.js"></script>
        <link rel="stylesheet" type="text/css" href="<?echo _PATH_TO_ADMIN_ADDONS?>/mBox/jBox-master/Source/jBox.css">

        <link rel="stylesheet" type="text/css" href="<?echo _PATH_TO_ADMIN_ADDONS?>/mBox/StephanWagner-mForm-0.2.6/assets/mForm.css">
        <SCRIPT type="text/javascript" src="<?echo _PATH_TO_ADMIN_ADDONS?>/mBox/StephanWagner-mForm-0.2.6/mForm.Core.js" ></SCRIPT>
       	<SCRIPT type="text/javascript" src="<?echo _PATH_TO_ADMIN_ADDONS?>/mBox/StephanWagner-mForm-0.2.6/mForm.Element.js" ></SCRIPT>
       	<SCRIPT type="text/javascript" src="<?echo _PATH_TO_ADMIN_ADDONS?>/mBox/StephanWagner-mForm-0.2.6/mForm.Element.Select.js" ></SCRIPT>
        <SCRIPT type="text/javascript" src="<?echo _PATH_TO_ADMIN_ADDONS?>/mBox/StephanWagner-mForm-0.2.6/mForm.Submit.js" ></SCRIPT>

        <script type="text/javascript" src="<?echo _PATH_TO_ADMIN_ADDONS?>/mBox/modal_window.jBox.js"></script>

        <?if (_CKEDITOR_ and _CKEDITOR_!='none'){?>
        <SCRIPT type="text/javascript" src="<?echo _EXT.'/'._CKEDITOR_?>/ckeditor.js" ></SCRIPT>
        <SCRIPT type="text/javascript" src="<?echo _EXT.'/'._CKEDITOR_?>/styles.js" ></SCRIPT>
        <?}?>
        <link rel="stylesheet" type="text/css" href="<?echo _PATH_TO_ADMIN_ADDONS.'/'._HIGHSLIDE_?>/highslide_local.css">
        <link rel="stylesheet" type="text/css" href="<?echo _PATH_TO_ADMIN_ADDONS.'/'._HIGHSLIDE_?>/highslide.css">
        <script type="text/javascript" src="<?echo _PATH_TO_ADMIN_ADDONS.'/'._HIGHSLIDE_?>/highslide.js"></script>
        <script type="text/javascript">hs.graphicsDir = '<?echo _PATH_TO_ADMIN_ADDONS.'/'._HIGHSLIDE_?>/graphics/';</script>

        <? $this->site_top_head_include_addons();
           $this->local_page_head() ;
        ?>

        <link rel="stylesheet" type="text/css" href="<?echo _PATH_TO_ENGINE?>/admin/style.css">
        <script type="text/javascript" src="<?echo _PATH_TO_ENGINE?>/admin/script.js"></script>

      </head>
   <?
  }

  function site_top_head_include_addons() {}
  function local_page_head() {}


 // ----------------------------------------------------------------------------------------------------------------------------
 // структура сайта
 // ----------------------------------------------------------------------------------------------------------------------------


  function body($options=array()) { $this->body_1x($options) ; } // по умолчанию используем страницу без боковых панелей

  function body_frame($options=array())     // вывод страницы для фрейма
  { ?><body class="body_frame <?if ($options['body_class']) echo $options['body_class']?> <?if ($this->body_class) echo $this->body_class?> ">
        <div id=wrapper <?if ($this->obj_info['_reffer']) echo ' reffer='.$this->obj_info['_reffer']?>><?
            $stop_view=$this->check_view_function_by_params($_GET) ;
            if (!$stop_view)
            { ?><div id="block_main"><?
              $this->block_main();
              ?></div><?
            }
     ?></div></body><?
    //damp_array($this);
  }

  function main_panel()
  { $stop_view2=0 ;
    // проверяем, есть ли заданная панель для текущей комбинации параметров фрейма
    $stop_view=$this->check_view_function_by_params($_GET) ;
    // если передана команда - пытаемся выполнить команду
    if ($this->cmd)
    {   list($stop_view2,$cmd_exec)=exec_script_cmd($this->cmd,$this->from,array('debug'=>0,'cur_page'=>$this)) ;
        if (!$cmd_exec) list($stop_view2,$cmd_exec)=exec_script_cmd('cmd_exe_'.$this->cmd,$this->from,array('debug'=>0,'cur_page'=>$this)) ;
        /*
        // внутри класса страницы разрешается использование команд только с префиксом cmd_exe_
        $cmd_exe='cmd_exe_'.$this->cmd ;
        if (method_exists($this,$cmd_exe)) $stop_view2=$this->$cmd_exe() ;
        else // если внутри класса нужного метода нет, ищем функцию. При необходимости подключаем скрипты, согласно комбинациии исходных данных
        { $func_name=$this->include_script_cmd($this->cmd,$this->from,array('debug'=>0)) ;   // вернет имя функции, если она подключена
            if ($func_name) $stop_view2=$func_name() ;
            else
            { // ищем имя функции с префиксом 'cmd_exe_' - для совместимости со старыми версиями
              $func_name=$this->include_script_cmd('cmd_exe_'.$this->cmd,$this->from,array('debug'=>0)) ;   // вернет имя функции, если она подключена
              if ($func_name) $stop_view2=func_name() ;
            }
        }*/
    }
    if (!$stop_view and !$stop_view2)
    {  ?><div id="block_main"><?
        $this->block_main();
       ?></div><?
    }
  }

  function body_1x(&$options=array())
  { global $tkey,$pkey ;
    $form_id=($options['form_id'])? $options['form_id']:'form' ;
    $form_action=($options['form_action'])? $options['form_action']:'' ;
    ?><body class="body_1x">
      <div id=wrapper class="body_1x">
      <form name="form" id="<?echo $form_id?>" method="post" action="<?echo $form_action?>">
		 <input name="tkey" type="hidden" value="<?echo $tkey?>"  id='tkey' />
		 <input name="pkey" type="hidden" value="<?echo $pkey ?>" id='pkey' />
	       <div id=top_menu><? $this->site_top_menu($options); ?></div>
	       <div id=main_panel><? $this->main_panel() ; ?></div>
	       <div class=clear></div>
       </form>
       </div>
       </body>
     <?
  }

  function body_1x_no_menu(&$options=array())
  { global $tkey,$pkey ;
    $form_id=($options['form_id'])? $options['form_id']:'form' ;
    $form_action=($options['form_action'])? $options['form_action']:'' ;
    ?><body class=body_1x_no_menu>
      <div id=wrapper>
      <form name="form" id="<?echo $form_id?>" method="post" action="<?echo $form_action?>">
		 <input name="tkey" type="hidden" value="<?echo $tkey?>"  id='tkey' />
		 <input name="pkey" type="hidden" value="<?echo $pkey ?>" id='pkey' />
         <div id=main_panel><? $this->main_panel() ; ?></div>
	     <div class=clear></div>
       </form>
       </div>

       </body>
     <?
  }

  function body_1x_no_form(&$options=array())
  { ?><body class="body_1x_no_form">
       <div id=wrapper>
	       <div id=top_menu><? $this->site_top_menu($options); ?></div>
           <div id=main_panel><? $this->main_panel() ; ?></div>
	       <div class=clear></div>
       </div>
      </body>
     <?
  }

  function body_2x(&$options=array())
  {?><body class="body_2x">
      <div id=wrapper>
	       <div id=top_menu><? $this->site_top_menu($options); ?></div>
	       <div id=left_panel><? $this->left_panel() ?></div>
           <div id=main_panel><? $this->main_panel() ; ?></div>
	       <div class=clear></div>
       </div>
       </body>
     <?
  }

  function body_3x(&$options=array())
  {?><body>
      <div id=wrapper class="body_3x">
	       <div id=top_menu><? $this->site_top_menu($options); ?></div>
	       <div id=left_panel><? $this->left_panel(); ?></div>
           <div id=main_panel><? $this->main_panel() ; ?></div>
	       <div id=right_panel><? $this->right_panel(); ?></div>
	       <div class=clear></div>
       </div>
       </body>
     <?
  }

 function body_frame_1x($options=array())
  { global $tkey ;
    if ($tkey) 				$_menu_params[]='tkey='.$tkey ;
    if ($options['title']) 	$_menu_params[]='show_title='.$options['title'] ;
    if (sizeof($_menu_params)) $menu_params='?'.implode('&',$_menu_params) ;
    $src_view=($options['fra_view'])? 'SRC="'.$options['fra_view'].'"':'' ;
    // было 89 - стало 27
    ?><FRAMESET rows="59,*" COLS=*>
		    <FRAME id="fra_top_menu" SRC="<?echo _PATH_TO_ADMIN?>/fra_top_menu.php<?echo $menu_params?>" NAME="fra_top_menu" frameBorder=0  NORESIZE SCROLLING=no />
			<FRAME id="fra_view"  NAME="fra_view" <?echo $src_view?>/>
	      <NOFRAMES><body>Ваш браузер не поддерживает фреймы</body></NOFRAMES>
	  </FRAMESET>
    <?
  }

  function body_frame_2x($options=array())
  { global $tkey ;
    if ($tkey) 				$_menu_params[]='tkey='.$tkey ;
    if ($options['title']) 	$_menu_params[]='show_title='.$options['title'] ;
    if ($options['root_id']) 	$_menu_params[]='root_id='.$options['root_id'] ;
    if (sizeof($_menu_params)) $menu_params='?'.implode('&',$_menu_params) ;
    $src_tree=($options['fra_tree'])? 'SRC="'.$options['fra_tree'].'"':'' ;
    // было 89 - стало 27
    ?><FRAMESET rows="59,*" COLS=*>
		    <FRAME id="fra_top_menu" SRC="<?echo _PATH_TO_ADMIN?>/fra_top_menu.php<?echo $menu_params?>" NAME="fra_top_menu" frameBorder=0  NORESIZE SCROLLING=no />
            <FRAMESET ROWS=* COLS="200,*" >
		        <FRAME id="fra_tree"  NAME="fra_tree" <?echo $src_tree?> />
			   	<FRAME id="fra_view"  NAME="fra_view" />
		    </FRAMESET>
	      <NOFRAMES><body>Ваш браузер не поддерживает фреймы</body></NOFRAMES>
	  </FRAMESET>
    <?
  }

  function body_frame_3x($options=array())
  { global $tkey ;
    if ($tkey) 				$_menu_params[]='tkey='.$tkey ;
    if ($options['title']) 	$_menu_params[]='show_title='.$options['title'] ;
    if (sizeof($_menu_params)) $menu_params='?'.implode('&',$_menu_params) ;
    $src_tree=($options['fra_tree'])? 'SRC="'.$options['fra_tree'].'"':'' ;
    // было 89 - стало 27
    ?><FRAMESET rows="59,*" COLS=*>
		    <FRAME id="fra_top_menu" SRC="<?echo _PATH_TO_ADMIN?>/fra_top_menu.php<?echo $menu_params?>" NAME="fra_top_menu" frameBorder=0  NORESIZE SCROLLING=no />
            <FRAMESET ROWS=* COLS="200,*" >
		        <FRAME id="fra_tree"  NAME="fra_tree" <?echo $src_tree?> />
	    		<FRAMESET COLS=* ROWS=50%,*>
			   		<FRAME id="fra_view"  NAME="fra_view" />
		       		<FRAME id="fra_order" NAME="fra_order" />
		    	</FRAMESET>
		    </FRAMESET>
	      <NOFRAMES><body>Ваш браузер не поддерживает фреймы</body></NOFRAMES>
	  </FRAMESET>
    <?
  }

  function body_frame_3x_vert($options=array())
  { global $tkey ;
    if ($tkey) 				$_menu_params[]='tkey='.$tkey ;
    if ($options['title']) 	$_menu_params[]='show_title='.$options['title'] ;
    if (sizeof($_menu_params)) $menu_params='?'.implode('&',$_menu_params) ;
    $src_tree=($options['fra_tree'])? 'SRC="'.$options['fra_tree'].'"':'' ;
    // было 89 - стало 27
    ?><FRAMESET rows="59,*" COLS=*>
		    <FRAME id="fra_top_menu" SRC="<?echo _PATH_TO_ADMIN?>/fra_top_menu.php<?echo $menu_params?>" NAME="fra_top_menu" frameBorder=0  NORESIZE SCROLLING=no />
            <FRAMESET ROWS=* COLS="200,*,*" >
		        <FRAME id="fra_tree"  NAME="fra_tree" <?echo $src_tree?> />
                <FRAME id="fra_view"  NAME="fra_view" />
		        <FRAME id="fra_order" NAME="fra_order" />
		    </FRAMESET>
	      <NOFRAMES><body>Ваш браузер не поддерживает фреймы</body></NOFRAMES>
	  </FRAMESET>
    <?
  }

  // заголовок админа -------------------------------------------------------------------------------------------------------------

 function site_top_menu($options=array())
  { global $no_show_top_panel ;

    if ($no_show_top_panel or !$_SESSION['member']->id) return ;
    if (!$options['no_top_icons'])
    { ?><div class=left_icons><?
      $src=(_ADMIN_LOGO_SRC)? _ADMIN_LOGO_SRC:_PATH_TO_ADMIN_IMG.'/menu/home_gray.jpg' ;
      ?><a href="<?echo _PATH_TO_ADMIN?>/index.php" target='_top'><img src="<?echo $src?>" alt="Главное меню" border=0 /></a><?
      $this->top_menu_icons();
      ?></div><?
    }
    ?><div class='title middle'><? $this->top_menu_title($options['title']) ; ?></div><?
    if (!$options['no_top_icons']) {?><div class=right_icons><?if (_CURRENT_USER=='admin' or _CURRENT_USER=='root') $this->top_menu_setup_icons() ;?></div><?}
    ?><div class=clear></div><?
  }

  // заголовок в верхем меню - стандартный, если не передан специально
  function top_menu_title($use_title='')  { echo $this->generate_title_page($use_title) ; }
  // заголовок окна браузера
  function page_title($title='',$path=array(),$options=array())  	  { echo $this->generate_title_page($title) ; }

  function  generate_title_page($use_title='')
  { global $show_title;
    if ($show_title) $use_title=$show_title ;
    return(_MAIN_DOMAIN.' - '.(($use_title)? $use_title:'панель управления')) ;
  }

  function top_menu_icons()
  {
    $_SESSION['admin_account_system']->reload_menu($_SESSION['member']) ;

  	if (sizeof($_SESSION['member']->menu_top)) foreach($_SESSION['member']->menu_top as $rec_item)
  	{ $img_name=(($rec_item['icon'])? $rec_item['icon']:_PATH_TO_ADMIN_IMG.'/document.gif') ;
	  if ($rec_item['icon_ext']) $img_name=$rec_item['icon_ext'] ;
	  $img="<img src='".$img_name."' width=48 height=48 alt='".$rec_item['name']."' border=0	/>" ;
  	  $target=(isset($rec_item['target']))? $rec_item['target']:"target=_top" ;
  	  $href=(strpos($rec_item['href'],'http://')!==false)? $rec_item['href']:_PATH_TO_ADMIN.'/'.$rec_item['href'] ;
  	  ?><a href='<?echo $href?>' <?echo $target?> <?echo $rec_item['onclick']?>><?echo $img?></a><?
  	}
  }

  function top_menu_setup_icons()
     { ?><a href="<?echo _PATH_TO_ADMIN?>/editor_tables.php" target='_top'><img src="<?echo _PATH_TO_ADMIN_IMG?>/menu/edit_obj_table.gif" width="48" height="48" alt="Редактор объектных таблиц" border=0 /></a>
         <?/*<a href="<?echo _PATH_TO_ADMIN?>/php_pages_editor.php" target='_top'><img src="<?echo _PATH_TO_ADMIN_IMG?>/menu/php_page_editor.png" width="48" height="48" alt="Редактор PHP страниц сайта" border=0 /></a>*/?>
         <a href="<?echo _PATH_TO_ADMIN?>/sys_util.php" target='_top'><img src="<?echo _PATH_TO_ADMIN_IMG?>/menu/edit_operation.gif" width="48" height="48" alt="Служебные операции" border=0 /></a>
       <?
     }



  //
  function check_reffer()
  {
	   /*
	   // проверяем, откуда происходит вызов скрипта
	   $url_arr=parse_url($_SERVER['HTTP_REFERER']) ;
	   //damp_array($url_arr) ;
	   if ($url_arr['host']!=$_SERVER['SERVER_NAME'])
	   { switch (basename($_SERVER['SCRIPT_NAME']))
	     { case 'tree_support_engine.php':  $root_usl='obj_name="'.$url_arr['host'].'"' ;
	     									if (!$url_arr['host']) {echo 'Доступ запрещен' ; exit ;}
	     									break;
	       default: echo 'Доступ запрещен' ; exit ;
	     }
	   }

	   */

	   //echo 'table_name='.$table_name.'<br>' ;
  }

   // функции используются во фреймах модулей
    function get_time_title($start_time=0,$end_time=0)
    { if (!$start_time and !$end_time) return('') ;
      $first_data=getdate($start_time) ;
      $last_data=getdate($end_time) ;
      if ($first_data['mday']==$last_data['mday'] and $first_data['mon']==$last_data['mon'] and $first_data['year']==$last_data['year']) $title=' за '.date('d.m.y',$start_time) ;
      else $title=' от '.date('d.m.y',$start_time).' по '.date('d.m.y',$end_time) ;
      return($title) ;
    }

    function get_time_usl($start_time=0,$end_time=0)
    { $time_usl=array() ; $usl='' ;
      if ($start_time) $time_usl[]=' c_data>='.$start_time ;
      if ($end_time)   $time_usl[]=' c_data<='.$end_time ;
      if (sizeof($time_usl)) $usl=implode(' and ',$time_usl) ;
      return ($usl) ;
    }
       
       
/*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  ПРОВЕРКА GET ПАРАМЕТРОВ СКРИПТА НА ПРЕДМЕТ НАЛИЧИЯ ПРАВИЛ НА ВЫПОЛНЕНИЕ КОМАНДЫ
/*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
// >>> $in_params - входные пепеременны $_GET
// <<< $stop_view=1  - не показывать стандартное содержимое фрейма
//               =0  - показывать стандартное содержисое фрейма
// ПРИМЕР ПРАВИЛ:
//$_SESSION['frame_viewer_menu'][]=array('if'=>array(	'table_code'=>'setting',
//								                        'code'=>'fonts'),
//					                     'then'=>array(	'cmd'=>'import_props_from_list',
//								                        'script'=>'mod/category/i_props.php')) ;
//
//$_SESSION['frame_viewer_menu'][]=array('if'=>array(	'tkey'=>'87',
//								                        'pkey'=>'123'),
//					                     'then'=>array(	'cmd'=>'import_props_from_list',
//								                        'script'=>'mod/category/i_props.php'')) ;

 function check_view_function_by_params($in_params)
 { $stop_view=0 ; $func_name='' ;
   //damp_array($in_params) ;
   //damp_array($_SESSION['frame_viewer_menu']) ;
   if (sizeof($_SESSION['frame_viewer_menu'])) foreach($_SESSION['frame_viewer_menu'] as $rule)
   {  $i=0 ;
      if (sizeof($rule['if'])) foreach($rule['if'] as $param=>$value) if ($in_params[$param]==$value) $i++ ;
      if ($i==sizeof($rule['if']) and $rule['then']['cmd']) // если выполнены все условия
        list($stop_view,$cmd_exec)=exec_script_cmd($rule['then']['cmd'],$rule['then']['script'],array('debug'=>0,'cur_page'=>$this)) ;   // вернет имя функции, если она найдена
   }
   return($stop_view) ;
 }




}
?>