<?php // СТАНДАРТНЫЙ ФАЙЛ НАСТРОЕК САЙТА
 error_reporting(E_ALL^E_NOTICE^E_USER_NOTICE);

 function parse_url_path($url)
  { if (isset($_SERVER['HTTP_HOST'])) $result['_CUR_DOMAIN'] = $_SERVER['HTTP_HOST'] ; // текущий домен страницы - значение HTTP_HOST более правильное чем SERVER_NAME но есть не всегда
    else                              $result['_CUR_DOMAIN'] = $_SERVER['SERVER_NAME'] ; // текущий домен страницы
    //$url=iconv('windows-1251','utf-8',$url) ;
    $url=rawurldecode($url) ;
    $arr=@parse_url($url) ;
    $result['_CUR_SHEME']='http://' ; // далее определять на основе $_SERVER['SERVER_PROTOCOL'] ;
    $result['_CUR_PAGE_PATH']=$url ;
    $arr_path=explode('/',$arr['path']) ;
    $result['_CUR_ROOT_DIR_NAME']=$arr_path[1] ;
    $result['_CUR_PAGE_NAME']=$arr_path[sizeof($arr_path)-1] ;
    $result['_CUR_PAGE_NAME_SOURCE']=$arr_path[sizeof($arr_path)-1] ;
    $result['_CUR_PAGE_IS_PAGINATOR']=0 ;
    // пытаемся определить страницу сайта через $_GET или $_POST
    if ($_POST['page']>1)
    {  $result['_CUR_PAGE_NUMBER']=$_POST['page'];
       $result['_CUR_PAGE_IS_PAGINATOR']=1 ; // помечаем, что текущая страница является страницей пагинатора (при добавлении записи в SEO ей не будут скопированы теги основной страницы)
    }
    if ($_GET['page']>1)
    {  $result['_CUR_PAGE_NUMBER']=$_GET['page'];
       $result['_CUR_PAGE_IS_PAGINATOR']=1 ; // помечаем, что текущая страница является страницей пагинатора (при добавлении записи в SEO ей не будут скопированы теги основной страницы)
    }
    // убираем страницы пагинатора для страниц сайта не в корне
    if (sizeof($arr_path)>2)
    {   //$result['_CUR_PAGE_NAME']=preg_replace('/page_(.+?)\.html/is','',$result['_CUR_PAGE_NAME']) ;
       if (preg_match_all('/page_(.+?)\.html/is',$result['_CUR_PAGE_NAME'],$matches,PREG_SET_ORDER))
        { $result['_CUR_PAGE_NUMBER']=$matches[0][1];
          $result['_CUR_PAGE_NAME']=str_replace($matches[0][0],'',$result['_CUR_PAGE_NAME']) ;
          $result['_CUR_PAGE_IS_PAGINATOR']=1 ; // помечаем, что текущая страница является страницей пагинатора (при добавлении записи в SEO ей не будут скопированы теги основной страницы)
        }
    }
    if (isset($arr['query'])) parse_str($arr['query'],$get_params) ;
    if (!isset($result['_CUR_PAGE_NUMBER']) and isset($get_params['page'])) $result['_CUR_PAGE_NUMBER']=$get_params['page'];
    if (!isset($result['_CUR_PAGE_NUMBER'])) $result['_CUR_PAGE_NUMBER']=1 ;

    //проверяем директорию на предмет LANG DIR
    if ($result['_CUR_ROOT_DIR_NAME'] and isset($GLOBALS['lang_arr'][$result['_CUR_ROOT_DIR_NAME']]))  { $result['_CUR_LANG']=$result['_CUR_ROOT_DIR_NAME'] ; $result['_CUR_LANG_SUFF']=($GLOBALS['lang_arr'][$result['_CUR_LANG']]['suff'])? $GLOBALS['lang_arr'][$result['_CUR_LANG']]['suff']:$result['_CUR_LANG']; }
    else                                                                                               { $result['_CUR_LANG']='' ; $result['_CUR_LANG_SUFF']='' ; }

    unset($arr_path[sizeof($arr_path)-1]) ;
    $result['_CUR_PAGE_DIR']=implode('/',$arr_path).'/' ;
    if (isset($arr['query'])) $result['_CUR_PAGE_QUERY']=$arr['query'] ;
    return($result) ;
  }

    // общие настроки ---------------------------------------------------------------------------------------------------
    if (!isset($use_server_proxi)) $use_server_proxi=0 ;
    $log_pages_for_system_ip=1 ; // ведем журнал страниц для системных IP
    $fast_edit_for_system_ip=0 ; // показываем ссылку быстрого редактирования для системных IP
    $send_mail_by_error=1 ;	// уведомлять разработчика о ошибках на сайте
    $alert_moduls=1 ;			// предупреждения модулей в главном меню (новые заказы, обращения в службу поддержки)
    $log_pages_info=1 ; 		// лог загрузки страниц
    $log_pages_robots=1 ;       // включать в лог загрузки страниц входы роботов
    $log_events_info=1 ;		// вести журнал событий
    $use_trash=0 ;    		// использование мусорной корзины для удаления объектов
    $no_index_robots_php=0;	// запрет для индексации php страниц
    define('_REBOOT_ALL_FOR_SYSTEM_IP',0) ; // постоянная перезагрука движка для системных IP
    $redirect_page_to_main_domains=1 ; // перенапрявлять страницу на основной домен
    $redirect_page_to_main_page=1 ; // перенапрявлять страницу
    define('_CONVERT_OBJ_LIST_TO_LIST_RECS',1) ; // постоянная перезагрука движка для системных IP
    $alert_to_slow_page=0 ;
    $no_access_to_robots=0 ;  // запрет индексации роботами всех страниц сайта (TAG NOINDEX)
    //$reboot_all=1 ;			// принудительно перегружать все данные сайта
    $enabled_fast_edit_to_system_ip=0 ;
    $allow_redirect_to_false_params=1 ; // разреширеть редирект на страницу с разрешенными параметрами запроса (разрешить ограничение парамеров в строке запроса $_GET)
    $allow_filter_GET_params=1 ;
    $save_page_in_sitemap=1 ; // разрешить сохранение страниц в карте сайта
    define('_NO_CHECK_TEMPLATES_FILE',0) ;

    // настроки для отладки ---------------------------------------------------------------------------------------------------
    $debug=0 ;                // режим отладки
    $debug_trace_level=4 ;	// число уровней трассировки кода, 0 - не показывать трассировку, all - все уровни
    $debug_db=0 ;		    // вывод отладочной информации по запросам к БД, 1 - без трассировки, 2 - с трассировкой
    $debug_sql_save_obj=0 ;   // вывод отладочной информации в админке по запросам для сохранения данных
    $debug_mails=0 ;			// вывод отладочной информации по отправляемым письмам (0/1/2)
    $debug_mail_setting=0 ;			// вывод отладочной информации по парамерам отправки почты
    $debug_modul=0 ;			// вывод отладочной информации по подключенным модулям
    $debug_templates=0 ;		// вывод отладочной информации по подключенным шаблонам
    $debug_boot=0 ;			// вывод отладочной информации за старту движка
    $debug_page=0 ;			// вывод отладочной информации по выводимой странице
    $debug_obj_info=0  ;		// вывод отладочной информации по распознованию объекта страницы
    $debug_metatags=0 ;		// вывод отладочной информации по формированию тегов
    $debug_frame_viewer=0 ;
    $debug_admin_menu=0 ;
    $debug_search_panel_function=0 ; // отладка подключаний функций из кнопок
    $time_line_mode=0; 		// фотма трассировки времени (0/1/2/3)
    $debug_trace_files_upload_to_mail=0 ; // трассировка на email информации по загрузке файлов

    define('_DEBUG_CMD_INCLUDE',0) ; // отладка подключения скриптов для выполнения команды кнопок


    // отладка для моудля товары
    $debug_price=0 ;
    $debug_price_convert=0 ;
    $debug_price_format=0 ;


  // подготавливаем защищенные переменные по текущей странице
  $SF_REQUEST_URI=str_replace('http://','',$_SERVER['REQUEST_URI']) ; // = /data/xml/content.xml, защита от URI типа  /http://watch.in.ua/goods_44040.html
  $SF_REQUEST_URI=str_replace(':///','/',$SF_REQUEST_URI) ;       // защита от ошибки типа parse_url(:///catalog/?sex=4): Unable to parse URL
  $SF_REQUEST_URI=str_replace('///','/',$SF_REQUEST_URI) ;       // защита от ошибки типа parse_url(///catalog/?sex=4): Unable to parse URL
  $SF_REQUEST_URI=urldecode($SF_REQUEST_URI) ;
  $SF_REQUEST_URI_arr=@parse_url($SF_REQUEST_URI) ;   // print_r($arr) ;
  $cur_page_path=$SF_REQUEST_URI_arr['path'] ; // в дальшейшем заменить все $url_source на $cur_page_path - $url_source неправильно выделяет имя скрипта, если в строке запроса есть путь
  if (isset($_SERVER['HTTP_HOST'])) $cur_domain = $_SERVER['HTTP_HOST'] ; // текущий домен страницы - значение HTTP_HOST более правильное чем SERVER_NAME но есть не всегда
  else                              $cur_domain = $_SERVER['SERVER_NAME'] ; // текущий домен страницы

  // новые переменные для упорядочивания работы со страницами
  $_CUR_PAGE=parse_url_path($SF_REQUEST_URI) ;  //echo nl2br(print_r($_CUR_PAGE,true)) ;

  define('_USE_SUBDOMAIN',0) ;
  define('_CUR_SHEME',          $_CUR_PAGE['_CUR_SHEME']) ;   // /catalog/page_2.html
  define('_CUR_DOMAIN',         $_CUR_PAGE['_CUR_DOMAIN']) ;   // /catalog/page_2.html
  define('_CUR_SUBDOMAIN',     str_replace(array('.'._BASE_DOMAIN,_BASE_DOMAIN),'',_CUR_DOMAIN))  ;
  define('_CUR_PAGE_PATH',      $_CUR_PAGE['_CUR_PAGE_PATH']) ;   // /catalog/page_2.html?dd=2&hs=2
  define('_CUR_ROOT_DIR_NAME',  $_CUR_PAGE['_CUR_ROOT_DIR_NAME']) ;     // catalog
  define('_CUR_PAGE_DIR',       $_CUR_PAGE['_CUR_PAGE_DIR']) ;     // /catalog/
  define('_CUR_PAGE_NAME',      $_CUR_PAGE['_CUR_PAGE_NAME']) ;                    // page_2.html
  define('_CUR_PAGE_NAME_SOURCE',$_CUR_PAGE['_CUR_PAGE_NAME_SOURCE']) ;             // page_2.html
  define('_CUR_PAGE_IS_PAGINATOR',$_CUR_PAGE['_CUR_PAGE_IS_PAGINATOR']) ;
  define('_CUR_PAGE_QUERY',     $_CUR_PAGE['_CUR_PAGE_QUERY']) ; // dd=2&hs=2
  define('_CUR_PAGE_NUMBER',    $_CUR_PAGE['_CUR_PAGE_NUMBER']) ; // 2
  define('_CUR_LANG',    $_CUR_PAGE['_CUR_LANG']) ; // ua
  define('_CUR_LANG_SUFF', $_CUR_PAGE['_CUR_LANG_SUFF']) ; // ua
  define('_CURRENT_USER',$_SERVER['PHP_AUTH_USER']) ; // $_SERVER['PHP_AUTH_USER'];
  //echo '_CUR_DOMAIN='._CUR_DOMAIN.'<br>_CUR_PAGE_PATH='._CUR_PAGE_PATH.'<br>_CUR_ROOT_DIR_NAME='._CUR_ROOT_DIR_NAME.'<br>_CUR_PAGE_DIR='._CUR_PAGE_DIR.'<br>_CUR_PAGE_NAME='._CUR_PAGE_NAME.'<br>_CUR_PAGE_QUERY='._CUR_PAGE_QUERY.'<br>_CUR_PAGE_NUMBER='._CUR_PAGE_NUMBER.'<br>_CUR_LANG='._CUR_LANG.'<br>_CUR_LANG_SUFF='._CUR_LANG_SUFF.'<br>' ;
  //damp_array($_SERVER,1,-1) ;
  //$system_dir=dirname($_SERVER['PHP_SELF']) ;   // директория текущей подсистемы // более не используется
  define('IP',(($use_server_proxi)? $_SERVER['HTTP_X_FORWARDED_FOR']:$_SERVER['REMOTE_ADDR'])) ;
  define('_IS_SYSTEM_IP',((in_array(IP,$system_IP_list)===true)? 1:0)) ; //echo 'IP='.IP.'<br>' ;
  define('_SERVER_ENGINE_VERS','engine_v'.$engine_vers) ;

  // _BASE_DOMAIN и _MAIN_DOMAIN должны быть прописаны в patch.php при инсталяции сайта - в целях борьбы с подменой HTTP_HOST и SERVER_NAME
  define('_BASE_DOMAIN',str_replace('www.','',$_SERVER['SERVER_NAME'])) ;
  define('_MAIN_DOMAIN','www.'._BASE_DOMAIN) ;
  define('_MOBILE_DOMAIN',_MAIN_DOMAIN) ;

  $domain_arr=explode('.',str_replace(_BASE_DOMAIN,'',_CUR_DOMAIN)) ;
  $subdomain=$domain_arr[0] ;
  if (!isset($index_page)) $index_page='/' ;
  if (!isset($index_page_alias)) $index_page_alias=array('/index.php','/index.html','/index.htm') ; // синонимы главной страницы, с них будет 301 перенаправление на главную страницу
  $redicted_pages=array() ;
  if (sizeof($index_page_alias)) foreach($index_page_alias as $page_name) if ($page_name!=$index_page) $redicted_pages[$page_name]=$index_page ;
  $section_id=0 ;

  //  стандартные имена директорий
  define("_ADMIN_",                 'admin');
  define("_CLASS_",                 'class');
  define("_INIT_",                  'init.php');
  define("_ADDONS_",                'AddOns');
  define('_PUBLIC_',                'public') ;
  define('_DELETED_',               '!deleted') ;
  // директриии плагинов
  define('_PHP_MAILER_',            'PHPMailer_v5.1') ;
  define('_MOOTOOLS_FILEMANAGER_',  'cpojer-mootools-filemanager-751d4ef') ;
  define('_HIGHSLIDE_',             'highslide-4.1.12') ;
  define('_CKEDITOR_',              'ckeditor_v4.4.4_full') ;
  // используемые плагины
  define('_GEO_IP_METHOD_',         'NONE') ; //  ipGeoBase - web-сервис, GeoIP_mod - расширение PHP "GeoIP", GeoIP_db - локальная версия GeoIP в папке /AddOns/Geo/

  // настройки БД
  define('_DB_ALLOW_PROCEDURE',     1) ;
  define('_DB_ALLOW_TRIGGERS',      1) ;

  // внутренние пути к директориям сайта
  // !!!! ВНИМАНИЕ! все пути к директориям НЕ ДОЛЖНЫ заканчиваться слешем.
  define('_DIR_TO_ROOT',        $_SERVER['DOCUMENT_ROOT']);
  define('_DIR_TO_ENGINE',      _DIR_TO_ROOT.'/'._SERVER_ENGINE_VERS);
  define('_DIR_TO_ADMIN',       _DIR_TO_ROOT.'/'._ADMIN_) ;
  define('_DIR_TO_CLASS',       _DIR_TO_ROOT.'/'._CLASS_);
  define('_DIR_TO_TEMPLATES',   _DIR_TO_ROOT.'/'._CLASS_.'/templates') ;
  define('_DIR_TO_MODULES',     _DIR_TO_ROOT.'/'._CLASS_.'/engine_moduls') ;
  define('_DIR_TO_SITE_EXT',    _DIR_TO_ROOT.'/'._CLASS_.'/ext') ;
  define('_DIR_EXT',            _DIR_TO_ROOT.'/'._CLASS_.'/ext') ;
  define('_DIR_TO_PUBLIC',      _DIR_TO_ROOT.'/'._PUBLIC_) ;
  define('_DIR_TO_ADDONS',      _DIR_TO_ROOT.'/'._ADDONS_) ; ;
  define('_DIR_TO_DELETED',     _DIR_TO_ADMIN.'/'._DELETED_) ; ;
  define('_DIR_TO_CLSS',        _DIR_TO_ENGINE.'/class') ;
  define('_DIR_TO_EXTENSIONS',  _DIR_TO_ENGINE.'/extensions') ;
  define('_DIR_ENGINE_EXT',     _DIR_TO_ENGINE.'/ext') ;
  define('_DIR_TO_ADMIN_ADDONS',_DIR_TO_ENGINE.'/'._ADDONS_) ;

  // внешние пути к директориям сайта
  // !!!! ВНИМАНИЕ! все пути к директориям НЕ ДОЛЖНЫ заканчиваться слешем.
  define("_PATH_TO_SITE",        'http://'._MAIN_DOMAIN);
  define("_PATH_TO_EXT",         '/'._CLASS_.'/ext');
  define("_EXT",                 '/'._CLASS_.'/ext');
  define("_PATH_TO_MODULES",     '/'._CLASS_.'/engine_moduls');
  define("_PATH_TO_ENGINE",      '/'._SERVER_ENGINE_VERS);
  define('_PATH_TO_ADMIN',       '/'._ADMIN_) ;
  define('_PATH_TO_PUBLIC',      '/'._PUBLIC_) ;
  define('_PATH_TO_ADDONS',      '/'._ADDONS_) ;
  define('_PATH_TO_DELETED',     _PATH_TO_ADMIN.'/'._DELETED_) ;
  define('_PATH_TO_ADMIN_ADDONS',_PATH_TO_ENGINE.'/'._ADDONS_) ;
  define('_PATH_TO_ADMIN_IMG',_PATH_TO_ENGINE.'/admin/images') ;
  define('_PATH_TO_BASED_CLSS_IMG',_PATH_TO_ENGINE.'/admin/images/clss') ;
  define('_PATH_TO_MODUL_CLSS_IMG',_PATH_TO_ADMIN.'/img/clss') ;

  // настройки для движка
  define('_SUPPORT_EMAIL','5207403@mail.ru') ;
  define('_EMAIL_ENGINE_ALERT','info@12-24.ru') ; // куда будет уходить почта о ошибках, подмене имени сайта и попытках взлома сайта
  define('_EMAIL_ENGINE_STATS','stats@12-24.ru') ; // куда будет уходить почта  по статистике работы движка и контролю изменения кода
  define('_EMAIL_ENGINE_DEBUG','debug@12-24.ru') ; // куда будет уходить почта  по отладкке работы движка
  define('_IP_12_24_ru',       '62.152.56.20') ;  // адрес, с которого будут отрабатываться запросы на контроль версии движка
  define('_PATH_TO_SERVER_SETUP_ALT','http://source.12-24.ru/setup_UTF8') ; // адрес, с которого будут загружаться инсталируемые файлы - старая версия, но пока поддерживаем
  define('_PATH_TO_SERVER_SETUP','http://source.12-24.ru/get_modul.php') ; // адрес, с которого будут загружаться инсталируемые файлы

  // настройки для сайта
  define('_ENGINE_DEMO_MODE',0) ; //взамен $read_only_mode
  define('_INDEX_PAGE_NAME','Главная страница') ;
  define('_DEFAULT_TREE_ORDER','indx') ;
  define('_URL_SPACE','_') ;  // _ , -
  define('_URL_LANG','TRANSLIT') ; // LOCAL,TRANSLIT
  define('_URL_MODE','TREE_NAME') ; // TREE_NAME,TREE_ID
  define('_PAGE_NUMBER_SOURCE','in_name') ;
  define('_CREATE_DIR_PERMS',0775) ;
  define('_CREATE_FILE_PERMS',0664) ;
  define('_SHOW_404_AS_PAGE',1) ; // показыать 404 ошибку в контенте страницы сайта
  define('_DB_CHARSET','utf8') ; // показыать 404 ошибку в контенте страницы сайта
  define('_CONVERT_UPLOADED_GIF_TO_JPG',0) ; // преобразовывать загруженные GIF в JPG
  define('_SITE_CREATE_FULL_MODEL',1) ; // загружать для сайта полную модель классов
  define('_YEAR',date('Y')) ; // текущий год
  define('IMAGE_BIBLIOTHEK','GD') ; // библиотека для обработки изображений   GD/IM
  define('_ASYNC_SEND_MAIL',0) ; // асинхронная отправка писем

  define('SITE_CODE',       'site') ;
  define('ADMIN_CODE',      'admin') ;
  define('TM_DOT',          'obj_descr_obj_tables') ;
  define('TM_SETTING',      'obj_'.SITE_CODE.'_setting') ;
  define('TM_MAILS',        'obj_'.SITE_CODE.'_mails') ;
  define('TM_SITE_MENU',    'obj_'.SITE_CODE.'_site_menu') ;
  define('TM_PAGES',        'obj_'.SITE_CODE.'_pages') ;
  define('TM_SITEMAP',      'obj_'.SITE_CODE.'_sitemap') ;
  define('TM_INFO',         'obj_'.SITE_CODE.'_info') ;
  define('TM_KEYWORDS',     'obj_'.SITE_CODE.'_keywords') ;
  define('TM_BANNERS',      'obj_'.SITE_CODE.'_banners') ;
  define('TM_LIST',         'obj_'.SITE_CODE.'_list') ;
  define('TM_PERSONAL',     'obj_'.SITE_CODE.'_personal') ;
  define('TM_LINK',         'obj_'.SITE_CODE.'_link') ;
  define('TM_MAIL_TURN',    'obj_'.SITE_CODE.'_mail_turn') ;
  define('TM_LOG_PAGES',    'log_'.SITE_CODE.'_pages') ;
  define('TM_LOG_EVENTS',   'log_'.SITE_CODE.'_events') ;
  define('TM_LOG_SEARCH',   'log_'.SITE_CODE.'_search') ;
  define('TM_LOG_BANNERS',  'log_'.SITE_CODE.'_banners') ;
  define('TM_LOG_ERRORS',   'log_'.SITE_CODE.'_errors') ;

  $__functions=array() ;
  $use_modules=array() ;
  $use_ext=array() ;
  $options=array();

  mb_internal_encoding("UTF-8");
  session_set_cookie_params(time()+31536000, '/', '.'._BASE_DOMAIN);  // при обращении к сайту по  IP нарушается работа сессий
  //session_set_cookie_params(time()+31536000, '/');

  // отдельное название для сессии в браузере, чтобы данные админки не смешивались с данными сайта в пределах одного браузера и домена
  define('_ENGINE_MODE',((_CUR_ROOT_DIR_NAME==_ADMIN_)? 'admin':'site')) ;
  session_name(str_replace('.','',_ENGINE_MODE._MAIN_DOMAIN)) ; // разводим сессию сайта и админки


  if (_IS_SYSTEM_IP)
  {  $info_db_error=1 ; 		// выводить на экран информацию о ошибках SQL
  	 $show_errors=1;			// показывать ошибки
  	 $error_report_level=E_ALL^E_NOTICE ;
  } else
  {
  	 $info_db_error=0 ; 		// не выводить на экран информацию о ошибках SQL
  	 $show_errors=0 ;			// не показывать ошибки
  	 $error_report_level=0 ;
  }

  set_include_path(_DIR_TO_CLASS);
  set_include_path(_DIR_TO_TEMPLATES);

  //ini_set('session.cookie_domain','.'._BASE_DOMAIN) ;  // при обращении к сайту по  IP нарушается работа сессий
  ini_set('display_errors',$show_errors) ;
  ini_set('display_startup_errors',$show_errors) ;
  ini_set('track_errors','on') ;
  ini_set('ignore_repeated_errors','on') ;
  ini_set('ignore_repeated_source','on') ;
  ini_set('allow_url_fopen','on') ;
  ini_set('zend.ze1_compatibility_mode','0') ;
  ini_set('allow_call_time_pass_reference','0') ;
  ini_set('register_argc_argv','0') ;
  ini_set('register_long_arrays','0') ;
  ini_set('magic_quotes_gpc','on') ;
  ini_set('magic_quotes_runtime','0') ;
  ini_set('variables_order','EGPCS') ;
  //ini_set('upload_max_filesize','8000') ;
  //ini_set('post_max_size','8M') ;


  error_reporting($error_report_level);

  mb_internal_encoding("UTF-8");

  $GLOBALS['YES']='<span class=green>Да</span>' ;
  $GLOBALS['NO']='<span class=red>Нет</span>' ;

  // фрагменты URL по которым можно определить, что идет попытка атаковать сайт
  $url_attack_string=array('FCKeditor','fckeditor','eval','_SERVER','_CONFIG','module_root_path','passwd','etc','allow_url_include','auto_prepend_file','dsafe_mode','group_concat','phpinfo',' or ',' and ','select','script') ;

  // переменные старых версий, используются только для совместимости. При вервой возможности заменять эти переменные на константы
  $site_patch       =_PATH_TO_SITE.'/' ;        // более не используется
  $patch_to_site    =_PATH_TO_SITE.'/' ;        // более не используется
  $patch_to_site_NS =_PATH_TO_SITE ;            // более не используется
  $patch_to_engine  =_PATH_TO_ENGINE.'/' ;      // более не используется
  $patch_to_admin   =_PATH_TO_ADMIN.'/';        // более не используется
  $admin_patch      =_PATH_TO_ADMIN.'/';        // более не используется
  $admin_dir        =_PATH_TO_ADMIN.'/'  ;      // более не используется, осталась пока тока в модулях, install_modul
  $dir_to_admin		=_DIR_TO_ADMIN.'/' ;        // более не используется
  $dir_to_modules   =_DIR_TO_MODULES.'/' ;      // более не используется
  $site_name        =_MAIN_DOMAIN ;             // более не используется
  $project_name     =_MAIN_DOMAIN ;             // более не используется
  $base_domain      =_BASE_DOMAIN ;             // более не используется
  $main_domain      =_MAIN_DOMAIN ;             // более не используется
  $dir_to_root_NS   =_DIR_TO_ROOT ;             // более не используется
  $dir_to_root	    =_DIR_TO_ROOT.'/' ;         // более не используется
  $dir_to_engine    =_DIR_TO_ENGINE.'/' ;       // более не используется
  $site_TM_kode     =SITE_CODE ;
  $admin_TM_kode    =ADMIN_CODE ;
  $public_TM_kode='total' ;


  // временно оставляем так как в старых админках опции подвязаны на заглобаленные переменные
  $TM_setting=TM_SETTING ;
  $TM_mails=TM_MAILS ;
  $TM_site_menu=TM_SITE_MENU ;
  $TM_pages=TM_PAGES ;
  $TM_sitemap=TM_SITEMAP ;
  $TM_info=TM_INFO ;
  $TM_keywords=TM_KEYWORDS ;
  $TM_banners=TM_BANNERS ;
  $TM_list=TM_LIST ;
  $TM_admin_personal=TM_PERSONAL ;
  $TM_log_pages=TM_LOG_PAGES ;
  $TM_log_events=TM_LOG_EVENTS ;
  $TM_log_search=TM_LOG_SEARCH ;
  $TM_log_banners=TM_LOG_BANNERS ;
  $TM_log_errors=TM_LOG_ERRORS ;
  $TM_log_admin_events=TM_LOG_EVENTS ;


  $dir_to_upload    =_DIR_TO_PUBLIC.'/' ;
  $server_engine_vers=_SERVER_ENGINE_VERS.'/';
  $url_source=basename($cur_page_path) ; // оставлено пока в целях соврестимости со старыми версиями
  $engine_mode=_ENGINE_MODE ;                   // более не используется
  $create_dir_perms=_CREATE_DIR_PERMS ;
  $patch_server_setup=_PATH_TO_SERVER_SETUP ; // пока используется в модулях
  $IP=IP ;
  $is_system_ip=_IS_SYSTEM_IP ;





  $robots=array() ;
  $robots[]="Yandex" ;
  $robots[]="Yahoo" ;
  $robots[]="Googlebot" ;
  $robots[]="Rambler" ;
  $robots[]="msnbot" ;
  $robots[]="CazoodleBot" ;
  $robots[]="Java/" ;
  $robots[]="bot" ;
  $robots[]="ia_archiver" ;
  $robots[]="Indexer" ;
  $robots[]="Slurp" ;
  $robots[]="tele-house.ru" ;
  $robots[]="Spyder" ;
  $robots[]="spider" ;
  $robots[]="crawler" ;
  $robots[]="YandeG" ;
  $robots[]="Wget" ;
  $robots[]="Python" ;
  $robots[]="ovale" ;
  $robots[]="Download Master" ;
  $robots[]="Direct" ;
  $robots[]="contype" ;
  $robots[]="12345" ;
  $robots[]="Jakarta" ;
  $robots[]="findlinks" ;
  $robots[]="YM/1.0" ;
  $robots[]="..." ;
  $robots[]="Mail.Ru" ;
  $robots[]="www.aport.ru" ;

  $robots_IP=array() ;
  //$robots_IP[]="194.67.18.145" ;
  $robots_IP[]="93.174.3.102" ;
  $robots_IP[]="210.97.192.220" ;
  $robots_IP[]="212.158.160.238" ;
  $robots_IP[]="61.140.108.209" ;

?>