<?
function open ($save_path, $session_name)
{ global $sess_save_path, $sess_session_name;
  $sess_save_path = $save_path;
  $sess_session_name = $session_name;
  return(true);
}

function close()
{
  return(true);
}

function read ($id)
{
  global $sess_save_path, $sess_session_name;
  $sess_file = "$sess_save_path/sess_$id";
  if ($fp = @fopen($sess_file, "r"))
  { $sess_data = fread($fp, filesize($sess_file));
    return($sess_data);
  }
  else return(""); // Здесь обязана возвращать "".
}

function write ($id, $sess_data)
{
    global $sess_save_path, $sess_session_name;

    $sess_file = "$sess_save_path/sess_$id";
    if ($fp = @fopen($sess_file, "w"))  return(fwrite($fp, $sess_data));
    else return(false);
}

function destroy ($id)
{
    global $sess_save_path, $sess_session_name;
    $sess_file = "$sess_save_path/sess_$id";
    return(@unlink($sess_file));
}

/*******************************************************************
 * ПРЕДУПРЕЖДЕНИЕ - Вам понадобится реализовать здесь какой-нибудь *
 * вариант утилиты уборки мусора.*
 *******************************************************************/
function gc ($maxlifetime)
{
    return true;
}

?>