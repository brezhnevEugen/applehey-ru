<?php
include_once(_DIR_TO_ENGINE."/admin/c_site.php") ;
include_once(_DIR_TO_ENGINE."/admin/i_clss_func.php") ;
include_once(_DIR_TO_ENGINE.'/admin/c_menu_tabs.php') ;



class c_editor_pages extends c_editor_obj
{
  function body($options=array())
  { global $patch_to_admin ;
    $this->body_frame_3x_vert(array('fra_tree'=>$patch_to_admin.'editor_pages_tree.php','title'=>$options['title'])) ;
  }
}

class c_file_explorer extends c_editor_obj
{
  function body($options=array())
  { global $patch_to_admin ;
    $ck_func=($_GET['CKEditorFuncNum'])? '?CKEditorFuncNum='.$_GET['CKEditorFuncNum']:'' ;
    $this->body_frame_3x_vert(array('fra_tree'=>$patch_to_admin.'file_explorer_tree.php'.$ck_func,'title'=>$options['title'])) ;
  }
}

class c_file_explorer_tree extends c_site
{
  function body($options=array())
  {global $dir_to_root,$br_vers,$patch_to_img,$patch_to_admin ;
   // исключаеим из поиска каталоги, где нельзя размещать информацию
   $no_index=get_no_index_dirs() ;
   $start_dir=($options['mode']=='editor_pages')? $dir_to_root:$dir_to_root.'public/' ;
   $start_dir_id=hide_server_dir($start_dir) ;
   $start_dir_name=basename($start_dir) ;

   $use_filter=1 ;
   // получаем содержимое корневой директории
   $list_dir=get_dir_list($start_dir) ;
   ?><body id=tree>
     <div id=tree_menu></div>
     <?if (strpos($br_vers,'IE6')===false){?><div class=space_div></div><?}?>
     <script type=text/javascript >

      if (clss_icon[1]==undefined) { clss_icon[1]= new Array ; clss_icon[1][0]= new Array ; clss_icon[1][1]= new Array ; }
      if (clss_icon[2]==undefined) { clss_icon[2]= new Array ; clss_icon[2][0]= new Array ; clss_icon[2][1]= new Array ; }
      if (clss_icon[3]==undefined) { clss_icon[3]= new Array ; clss_icon[3][0]= new Array ; clss_icon[3][1]= new Array ; }

      clss_icon[1][1]['open'] ="<? echo $patch_to_img?>clss/folder_on_open.gif" ;
	  clss_icon[1][1]['close']="<? echo $patch_to_img?>clss/folder_on_close.gif" ;
	  clss_icon[1][0]['open'] ="<? echo $patch_to_img?>clss/folder_off_open.gif" ;
	  clss_icon[1][0]['close']="<? echo $patch_to_img?>clss/folder_off_close.gif" ;
      clss_icon[2][1]['open'] ="<? echo $patch_to_img?>clss/item_on.gif" ;
	  clss_icon[2][1]['close']="<? echo $patch_to_img?>clss/item_on.gif" ;
	  clss_icon[2][0]['open'] ="<? echo $patch_to_img?>clss/item_off.gif" ;
	  clss_icon[2][0]['close']="<? echo $patch_to_img?>clss/item_off.gif" ;
      clss_icon[3][1]['open'] ="<? echo $patch_to_img?>clss/item_on_ed.gif" ;
	  clss_icon[3][1]['close']="<? echo $patch_to_img?>clss/item_on_ed.gif" ;
	  clss_icon[3][0]['open'] ="<? echo $patch_to_img?>clss/item_off_ed.gif" ;
	  clss_icon[3][0]['close']="<? echo $patch_to_img?>clss/item_off_ed.gif" ;

      object[1]= new Array ;
      object[1]["<?echo $start_dir_id?>"] = new obj(1,1,"<?echo $start_dir_id?>",null,"<?echo $start_dir_name?>") ;
      object[1]["<?echo $start_dir_id?>"].expand_cmd=6 ;
      object[1]["<?echo $start_dir_id?>"].only_dirs=1 ;
      object[1]["<?echo $start_dir_id?>"].use_filter=<?echo $use_filter?> ;
      <?if (sizeof($list_dir)) foreach($list_dir as $name=>$dir) if (!$use_filter or ($use_filter and !in_array($dir,$no_index))){ echo 'object[1]["'.hide_server_dir($start_dir).'"].add_child(1,"'.hide_server_dir($dir).'","'.$name.'",1,1);';}?>
      object[1]["<?echo $start_dir_id?>"].show() ;
      var rn=Math.random();
      if (parent.fra_view!=null) parent.fra_view.location.href="<?echo $patch_to_admin?>file_explorer_viewer.php?fname="+root.pkey+"&rn="+rn<?if ($_GET['CKEditorFuncNum']) echo '+"&CKEditorFuncNum='.$_GET['CKEditorFuncNum'].'"';?> ;
      function obj_click(tkey,pkey,cmd,evt)
        { var rn=Math.random();
          var obj=object[1][pkey] ;
          if (evt!=undefined) { evt.cancelBubble=true ; evt.returnValue=false ; }

          if (select_obj!=undefined)
          { var class_sel=/select/g ;
            var unsel_class=select_obj.className.replace(class_sel,'') ;
            select_obj.className=unsel_class ;
          }
          select_obj = document.getElementById('obj_'+obj.id);
          //$('div#obj_'+obj.id).addClass('select') ;
          //alert($('div#obj_'+obj.id).name) ;
          select_obj.className=select_obj.className+' select' ;
          select_obj_tree=obj ;
          if (parent.fra_view!=null) parent.fra_view.location.href="<?echo $patch_to_admin?>file_explorer_viewer.php?fname="+pkey+"<?if ($options['mode']=='editor_pages') echo '&mode=editor'?>&rn="+rn<?if ($_GET['CKEditorFuncNum']) echo '+"&CKEditorFuncNum='.$_GET['CKEditorFuncNum'].'"';?> ;
        }

    </script>
    </body>
   <?
  }
}

class c_editor_pages_tree extends c_file_explorer_tree
{
  function body($options=array()) { $options['mode']='editor_pages' ; parent::body($options);}
}


class c_file_explorer_viewer extends c_site
{

 function body(&$options=array()) { $options['body_class']='file_explorer'; $this->body_frame($options) ; }

 function select_obj_info()
 { global $fname,$dir_to_root_NS,$patch_to_site_NS,$obj_info,$TM_pages ;
   //echo 'fname='.$fname.'<br>' ;
   if (is_dir($dir_to_root_NS.$fname)) $obj_info['type']='dir' ;
   else if (is_file($dir_to_root_NS.$fname)) $obj_info['type']='file' ;
   else $obj_info['type']='' ;
   $obj_info['obj_name']=$fname ;
   switch($obj_info['type'])
   { case 'dir':  $obj_info['obj_name']='Директория "'.$patch_to_site_NS.$fname.'"' ;
                  $obj_info['patch']=$patch_to_site_NS.$fname ;
                  $obj_info['dir']=$dir_to_root_NS.$fname ;
                  $obj_info['name']=$fname ;
                  break ;
     case 'file': $obj_info['patch']=$patch_to_site_NS.$fname ;
                  $obj_info['dir']=$dir_to_root_NS.$fname ;
                  $obj_info['file_name']=basename($fname) ;
                  $obj_info['file_type']=mime_content_type($obj_info['dir']) ;
                  $obj_info['size_info']=filesize($obj_info['dir']) ;
                  if ($obj_info['file_type']=='text/html')
                  {  $config=$this->parse_php_file($fname) ;
                     $options=$this->exec_config($config['options']) ;

                     $obj_info['patch_page']=$patch_to_site_NS.$fname ;
                     $class_file=get_name_class_file(dirname($fname),basename($fname),$options) ; // таже функция, что используется в i_system при генерации страницы

                     //$class_name=get_name_class($dir,$fname) ;
                     //echo 'class_file='.$class_file.'<br>' ;
                     if (file_exists($class_file))
                     { $obj_info['page_mode']=2 ; // страница сайта с собственным шаблоном
                       $obj_info['class_file']=$class_file ;
                       $obj_info['config']=$config ;
                       $obj_info['options']=$options ;
                       $obj_info['class_name']=get_name_class(dirname($fname),basename($fname),$options) ;
                       if ($options['system']) $obj_info['system_name']=str_replace('_system','',$options['system']) ;
                     }
                     else
                     {  $db_page_info=select_objs($TM_pages,'*','href="'.$fname.'" and clss=26 and enabled=1',array('no_arr'=>1,'no_slave_table'=>1,'debug'=>0)) ;
                        if ($db_page_info['pkey']) $obj_info['page_mode']=1 ; // страница сайта с информацией в БД
                        else $obj_info['page_mode']=3 ; // текстовой файл
                     }
                    //echo 'page_mode='.$obj_info['page_mode'].'<br>' ;
                  }

                  switch($obj_info['page_mode'])
                  { case 1:     $obj_info['obj_name']='Страница сайта "'.$patch_to_site_NS.$fname.'"' ; break ;
                    case 2:     $obj_info['obj_name']='Страница сайта "'.$patch_to_site_NS.$fname.'"' ; break ;
                    case 3:     $obj_info['obj_name']='Текстовой файл "'.$patch_to_site_NS.$fname.'"' ; break ;
                    default:    $obj_info['obj_name']='Файл "'.$patch_to_site_NS.$fname.'"' ;

                  }
                  break ;
   }
   //damp_array($obj_info);

   global $top_menu ;if (!isset($top_menu)) {$top_menu=new c_top_menu() ; session_register('top_menu') ; }
 }

 function block_main()
 {  global $cmd,$obj_info,$fname,$cur_page,$CKEditorFuncNum,$mode ;
    if ($obj_info['type']=='dir')  $cur_menu_item=$this->show_dir_title() ;
    if ($obj_info['type']=='file') $cur_menu_item=$this->show_file_title() ;

     ?><form id=form name="form" method="post" action='' ENCTYPE="multipart/form-data">
	    <input name="cmd"       type="hidden" id=cmd        value=""/>
        <input name="cur_page"  type="hidden" id=cur_page   value="<?echo $cur_page?>">
        <input name="fname"     type="hidden"               value="<?echo $fname?>">
        <input name="CKEditorFuncNum" type="hidden"         value="<?echo $CKEditorFuncNum?>">
    <?
    //damp_array($obj_info) ;
    $show_next=1 ;
    if (method_exists($this,$cmd)) $show_next=$this->$cmd() ;
           // damp_array($cur_menu_item) ;

    if ($show_next) switch($cmd)
    {  case 'select_file': $this->return_file_patch(); break ;

       // по умолчние выводим данные для соответствующей вкладки меню
       default: switch($cur_menu_item['action'])
                { case 1001: $this->show_dir_info() ; break ;
                  case 2001: $this->show_file_info() ; break ;
                  case 2003: $this->show_file_operation() ; break ;
                  case 2004: $this->show_page_db_source() ; break ;
                  case 2005: $this->show_page_php_source() ; break ;
                  case 2006: $this->show_page_text_source() ; break ;
                  case 2007: $this->show_conf_file() ; break ;
                }
    }
    ?></form>
      <script type="text/javascript">
         $('#fra_order').css('display','none');
         $('div#panel_list_mode > input').click(function(){$('#form').submit();}) ;
         $('div#panel_upload_mode > input').click(function(){$('#cmd').val('show_dialog_upload');$('#form').submit();}) ;
         $('div#panel_filter > select').change(function(){$('#form').submit();}) ;
         $('div#panel_sel_file > a').click(function()
         {   var fname="<?echo $obj_info['name']?>"+$('a.to_file.select').text() ;
             window.parent.opener.CKEDITOR.tools.callFunction('<?echo $CKEditorFuncNum?>',fname,'');
             window.parent.close() ;
             return(false);
         }) ;
         $('a.upload_file_sel').click(function()
         {   var fname=$(this).attr('alt') ;
             //alert(fname) ;
             window.parent.opener.CKEDITOR.tools.callFunction('<?echo $CKEditorFuncNum?>',fname,'');
             window.parent.close() ;
             return(false);
         }) ;
         $('a.to_page').click(function(){$('#cur_page').val($(this).attr('tag'));$('#form').submit();return(false);}) ;
         $('a#to_upload_files').click(function(){$('#cmd').val('show_dialog_upload');$('#form').submit();return(false);}) ;
         $('a#to_create_page').click(function(){$('#cmd').val('show_dialog_create_page');$('#form').submit();return(false);}) ;
         $('a.to_file').click(function(){$('div#panel_sel_file').addClass('show');$('div#panel_sel_file').css('display','block');$('a.to_file.select').removeClass('select');$(this).addClass('select');window.top.fra_order.location=admin_dir+"file_explorer_viewer.php?cmd=<?echo ($mode=='editor')? 'edit&mode=editor':'view';?>&fname=<?echo $obj_info['name']?>"+$(this).text() ;return(false) ;});
         $('a.to_upload_file').click(function(){$('a.to_file.select').removeClass('select');$(this).addClass('select');window.top.fra_order.location=admin_dir+"file_explorer_viewer.php?cmd=view&fname="+$(this).attr('alt') ;return(false) ;});
         $('div.view_icons > div.item').click(function()
           { $('div.view_icons > div.item.select').removeClass('select');
             $(this).addClass('select');
             var file=$(this).children('a').text() ;
             window.top.fra_order.location=admin_dir+"file_explorer_viewer.php?fname=<?echo $obj_info['name']?>"+file ;
             return(false) ;
           });

         // обрабочик нажатия на "добавить поле"
         $('div.list_upd_files > a.to_add').click(function()
           {   var i=1 ;

               var items=$('div.list_upd_files > div.item') ;  //получаем коллекцию
               var last_indx=items.length-1 ;                  // индекс последнего элемента
               $(items).eq(last_indx).clone().insertAfter($(items).eq(last_indx)) ; // дублируем последний эелемент

               var items=$('div.list_upd_files > div.item') ;  //заново получаем коллекцию
               var last_indx=items.length-1 ;                  // индекс последнего элемента
               var num=items.length ;

               $(items).eq(last_indx).children('span.file_num').text(num+'.') ; // пишем номер
               $(items).eq(last_indx).children('input').attr('name','upload_file_by_href['+num+']') ; // пишем новое имя
               $(items).eq(last_indx).children('input').val('') ; // очищаем значение элемента
               return(false);
           });

         // обрабочик нажатия на "удалить поле"
         $('div.list_upd_files > div.item > a.to_del').click(function()
                    { $(this).parent('div.item').remove() ;
                      var i=1 ;
                      $.each($('div.list_upd_files > div.item'),function()
                         { $(this).children('span.file_num').text(i+'.') ;
                           $(this).children('input').attr('name','upload_file_by_href['+i+']') ;
                           i=i+1 ;
                         }) ;
                      return(false);
                    });
      </script>
     <?
     echo '<br><br><br><br>' ;
     //echo 'cmd='.$cmd.'<br>' ;
     if ($cmd) echo (method_exists($this,$cmd))? 'Функция "'.$cmd.'" существует<br>':'Команда не выполнена<br>' ;
     //echo 'mode='.$mode.'<br>' ;
     //echo 'menu_action:'.$menu_action.'<br>' ;

 }

 function show_dir_title()
 {   global $obj_info,$top_menu ;
     $menu_set[1]=array("name" => 'Файлы',"action" => 1001) ;
     $menu_set[2]=array("name" => 'Операции',"action" => 1002) ;
     ?><p class='obj_header'><?echo $obj_info['obj_name'] ;?></p><?
     $cur_menu_item=$top_menu->show($obj_info,array('menu_set'=>$menu_set)) ;
     return($cur_menu_item) ;
 }

 function show_file_title()
 {  global $obj_info,$top_menu,$cur_menu_item ;
    $menu_set[0]=array("name" => 'Просмотр',"action" => 2001) ;
    if ($obj_info['page_mode']==1) $menu_set[1]=array("name" => 'Текст HTML',"action" => 2004) ;
    if ($obj_info['page_mode']==2) $menu_set[1]=array("name" => 'Исходный код php',"action" => 2005) ;
    if ($obj_info['page_mode']==3) $menu_set[1]=array("name" => 'Текст',"action" => 2006) ;
    $menu_set[2]=array("name" => 'Операции',"action" => 2003) ;
    if ($obj_info['page_mode']==2) $menu_set[3]=array("name" => 'Конфигурация страницы',"action" => 2007) ;
    ?><p class='obj_header'><?
	     echo $obj_info['obj_name'] ;
	     if ($obj_info['class_file']) echo '<br><span class="small">'.hide_server_dir($obj_info['class_file']).' ==> '.$obj_info['patch'].'</span>' ;
	     if ($obj_info['template_file']) echo '<br><span class="small">'.hide_server_dir($obj_info['template_file']).' ==> '.$obj_info['patch'].'</span>' ;
	?></p><?
    $cur_menu_item=$top_menu->show($obj_info,array('menu_set'=>$menu_set)) ;
    return($cur_menu_item) ;
 }

 function show_dir_info()
  {   global $list_mode,$filter,$mode,$patch_to_img ;
      if (!isset($list_mode)) session_register('list_mode') ;
      if (!$list_mode) $list_mode=0 ;
      if (isset($_POST['list_mode'])) $list_mode=$_POST['list_mode'] ;
      $arr_check_list_mode[$list_mode]='checked' ;
      if (!isset($filter)) session_register('filter') ;
      if (!$filter) $filter=0 ;
      if (isset($_POST['filter'])) $filter=$_POST['filter'] ;
      $arr_check_filter[$filter]='selected' ;

      ?><div id=panel_list_mode><span class=title>Показать список</span> <input type='radio' value=0 name=list_mode <?echo $arr_check_list_mode[0]?>>Таблица <input type='radio' value=1 name=list_mode <?echo $arr_check_list_mode[1]?>>Иконки <input type='radio' value=2 name=list_mode <?echo $arr_check_list_mode[2]?>>Подробно</div>
        <?/*<div id=panel_filter><span class=title>Применить фильтр</span> <select name=filter><option value=0 <?echo $arr_check_filter[0]?>></option><option value="jpg" <?echo $arr_check_filter['jpg']?>>jpg</option></select></div>*/?>
        <?if ($mode!='editor')
          {?>
            <div id=panel_upload><img src="<?echo $patch_to_img.'upload natural.png'?>"><a href="" id=to_upload_files class=title>Загрузить файл</a></div>
            <div id=panel_sel_file><?if ($_GET['CKEditorFuncNum']){?><img src="<?echo $patch_to_img.'ok_3.gif'?>"><a href="" id=to_select_files class=title>Выбрать файл</a><?}?></div>
         <?} else {?><div id=panel_create><img src="<?echo $patch_to_img.'add_icon.png'?>"><a href="" id=to_create_page class=title>Создать страницу сайта</a></div>
         <?}?>
        <div class=clear></div><hr><?
      $this->show_files() ;
  }

  function show_files()
    { global $cur_page,$obj_info,$mode ;
      $cur_page=$_POST['cur_page'] ;
      if ($cur_page=='') $cur_page=1 ;
      $pattern=($mode=='editor')? array('html','htm','php','js','css','txt',''):array() ;
      $cur_dir_list_files=get_files_list($obj_info['dir'],0,$pattern) ;
      // выводим файлы постранично, не более 30 на страницу
      $num_pages=sizeof($cur_dir_list_files)/30 ;
      if ($cur_page>($num_pages+1)) $cur_page=1 ;
      if ($num_pages>1)
      { ?><span class=title>Страница</span> <?
        for ($i=0; $i<$num_pages; $i++)
         { $class=(($i+1)==$cur_page)? 'class="to_page select"':'class=to_page' ;
           echo '<a href="" '.$class.' tag='.($i+1).'>'.($i+1).'</a>&nbsp;&nbsp;' ;
         }
        // оставляем в массиве тока текущий диспазон страниц
        $start_indx=($cur_page-1)*30+1 ;
        $end_indx=$start_indx+30-1 ; if ($end_indx>sizeof($cur_dir_list_files)) $end_indx=sizeof($cur_dir_list_files) ;
        $res_list_obj=array_slice($cur_dir_list_files,$start_indx,30) ;
        echo 'файлы '.$start_indx.'...'.$end_indx.' из '.sizeof($cur_dir_list_files).'<br><br>' ;
      } else $res_list_obj=$cur_dir_list_files ;

      global $list_mode ;
      switch($list_mode)
      { case 1:  $this->show_files_by_icons($res_list_obj) ;   break ;
        case 2:  $this->show_files_full_info($res_list_obj) ;  break ;
        default: $this->show_files_by_table_cols($res_list_obj) ;
      }

    }

    function show_files_full_info($res_list_obj)
    { global $obj_info,$patch_to_engine ;
      ?><table class=left><?
        if (sizeof($res_list_obj)) foreach($res_list_obj as $fname)
        { $img_type=get_image_type($fname) ;
          if ($img_type) $img_size=getimagesize($fname) ;
          $file_size=filesize($fname) ;
          $mime_type=mime_content_type($fname) ;
          if (strpos($fname,'.php')!==false) $mime_type='application/x-php' ;
          $mime_file=str_replace('/','-',$mime_type).'.png' ;
          $icon='<img src="'.$patch_to_engine.'img/mime/32x32/gnome-mime-'.$mime_file.'">' ;
          ?><tr><?
          ?><td><input type=checkbox name=select_files[<?echo urlencode(basename($fname))?>] value=1></a></td><?
          ?><td><a href="" class=to_file><? echo $icon ?></a></td><?
          ?><td class=width_300><a href="" class=to_file><? echo basename($fname) ?></a></td><?
          ?><td><?echo $mime_type ;?></td><?
          ?><td><?echo $img_type?></td><?
          ?><td><?if ($img_type) echo $img_size[0].'x'.$img_size[1];?></td><?
          ?><td><?echo format_size($file_size)?></td><?
          ?></tr><?
        }
      ?></table><?
      if (!sizeof($res_list_obj)) echo '<div class=alert>Директория пуста</div><br>' ;
    }

    function show_files_by_icons($res_list_obj)
    { global $obj_info,$patch_to_admin,$patch_to_engine ;
      ?><div class=view_icons><?
        if (sizeof($res_list_obj)) foreach($res_list_obj as $fname)
        { $type=get_image_type($fname) ;
          $mime_type=mime_content_type($fname) ;
          if (strpos($fname,'.php')!==false) $mime_type='application/x-php' ;
          $mime_file=str_replace('/','-',$mime_type).'.png' ;
          $icon=($type)? '<img src="'.$patch_to_admin.'getimage.php?name='.$fname.'&mode=small">':'<img src="'.$patch_to_engine.'img/mime/72x72/gnome-mime-'.$mime_file.'">' ;
          ?><div class=item>
                <? echo $icon ?><br>
                <a href="" class=to_file><? echo basename($fname) ?></a>
            </div>
          <?
        } else echo '<div class=alert>Директория пуста</div><br>' ;
      ?><div class=clear></div></div><?
    }

    function show_files_by_table_cols($res_list_obj)
    { global $patch_to_engine ;
      $col=0 ;
      $max_col=3 ;
      $j=0 ;
      ?><table class=left><?
        if (sizeof($res_list_obj)) foreach($res_list_obj as $fname)
        { $mime_type=mime_content_type($fname) ;
          if (strpos($fname,'.php')!==false) $mime_type='application/x-php' ;
          $mime_file=str_replace('/','-',$mime_type).'.png' ;
          $icon='<img src="'.$patch_to_engine.'img/mime/32x32/gnome-mime-'.$mime_file.'">' ;
          if (!$col) {?><tr><?}
          ?><td class=width_300><?echo $icon?><a href="" class=to_file><? echo basename($fname) ?></a></td><?
          $j++ ;
          $col++ ;
          if ($col==$max_col) { echo '</tr>' ; $col=0 ;}
        }
      if ($col)  {?></tr><?}
      ?></table><?
      if (!$j) echo '<div class=alert>Директория пуста</div><br>' ;
    }


 function show_dialog_upload()
 {  global $obj_info,$upload_mode ;
    if (!isset($upload_mode)) session_register('upload_mode') ;
    if (!$upload_mode) $upload_mode=0 ;
    if (isset($_POST['upload_mode'])) $upload_mode=$_POST['upload_mode'] ;
    $arr_check_upl_mode[$upload_mode]='checked' ;

    ?><h1><? echo $obj_info['patch'] ?> - загрузка файла на сервер</h1>
      <div id=panel_upload_mode><span class=title>Откуда будете загружать: </span> <input type='radio' value=0 name=upload_mode <?echo $arr_check_upl_mode[0]?>>Загрузить из моего компьютера <input type='radio' value=1 name=upload_mode <?echo $arr_check_upl_mode[1]?>>Загрузить из Интернета</div>
      <hr>
    <? switch($upload_mode)
         { case 0:  ?><div class=title>Выберите файлы на диске вашего компьютера</div>
                      <div class=list_upd_files>
                           <div class=item><span class=file_num>1.</span><INPUT   name="upload_file[1]" type=file class=file><a class=to_del href="">удалить из списка</a></div>
                           <div class=item><span class=file_num>2.</span><INPUT   name="upload_file[2]" type=file class=file><a class=to_del href="">удалить из списка</a></div>
                           <div class=item><span class=file_num>3.</span><INPUT   name="upload_file[3]" type=file class=file><a class=to_del href="">удалить из списка</a></div>
                           <a class=to_add href="">Добавить еще</a>
                      </div>
                    <?
                   break ;
           case 1:  ?><div class=title>Введите ссылку на файлы в интернете</div>
                      <div class=list_upd_files>
                          <div class=item><span class=file_num>1.</span><input name="upload_file_by_href[1]" type=text class=text value=""><a class=to_del href="">удалить из списка</a></div>
                          <div class=item><span class=file_num>2.</span><input name="upload_file_by_href[2]" type=text class=text value=""><a class=to_del href="">удалить из списка</a></div>
                          <div class=item><span class=file_num>3.</span><input name="upload_file_by_href[3]" type=text class=text value=""><a class=to_del href="">удалить из списка</a></div>
                          <a class=to_add href="">Добавить еще</a>
                      </div>
                   <?
                   break ;
         }
    ?><button onclick="exe_cmd('-')">Отмена</button><button onclick="exe_cmd('show_dialog_upload_step_2')">Загрузить</button><?
 }

 function show_dialog_upload_step_2()
 {
   global $obj_info,$upload_file_by_href,$CKEditorFuncNum ;
   ?><h1>Загружаем файлы на сервер</h1><?
   //damp_array($_FILES) ;

   //загружаем файл на сервер -------------------------------------------------------------------------------------------
   if (sizeof($_FILES['upload_file']['name'])) foreach($_FILES['upload_file']['name'] as $indx=>$name) if ($name)
   {  $finfo=array() ;
      $finfo['name']=$_FILES['upload_file']['name'][$indx] ;
      $finfo['type']=$_FILES['upload_file']['type'][$indx] ;
      $finfo['tmp_name']=$_FILES['upload_file']['tmp_name'][$indx] ;
      $finfo['size']=$_FILES['upload_file']['size'][$indx] ;
      $upload_file_name=upload_file($finfo,$obj_info['dir'],array('debug'=>0)) ;
      echo 'Загружен файл '.basename($upload_file_name).' <a href="" class=to_upload_file alt="'.hide_server_dir($upload_file_name).'">Посмотреть</a>' ;
      if ($CKEditorFuncNum) echo ' <a href="" class=upload_file_sel alt="'.hide_server_dir($upload_file_name).'">Выбрать</a>' ;
      echo '<br>' ;
      //$patch=$patch_to_site_NS.hide_server_dir($upload_file_name) ;
      //$this->show_file_info(array('obj_info'=>array('patch'=>$patch,'dir'=>$upload_file_name,'file_name'=>basename($upload_file_name)),'img_clone'=>250)) ;
   }


   //загружаем файл на сервер по ссылке --------------------------------------------------------------------------------
   //damp_array($upload_file_by_href) ;
   if (sizeof($upload_file_by_href)) foreach($upload_file_by_href as $href_file) if ($href_file)
   {  $upload_file_name=upload_file_by_href($href_file,$obj_info['dir']) ;
      if ($upload_file_name) echo 'Загружен файл '.basename($upload_file_name).' <a href="" class=to_upload_file alt="'.hide_server_dir($upload_file_name).'">Посмотреть</a><br>' ;
      else echo 'Не удалось загрузить файл "'.$href_file.'"<br>' ;
   }
 }

 function show_dialog_mkdir()
  {
    ?><h2>Создать подкаталог</h2>
      <p><input name="new_name_dir" type="text" class=text value=""><br>
      <button onclick="exe_cmd('show_dialog_mkdir_step_2')">Создать подкаталог</button></p>
    <?
  }

 function show_dialog_mkdir_step_2()
   { global $new_name_dir,$abs_cur_dir,$otn_cur_dir,$otn_cur_dir,$list_dir,$abs_root_upload_dir ;
     echo '<br><br>' ;
     if ($new_name_dir)
     { echo 'Создаем подкаталог: <strong>'.$otn_cur_dir.$new_name_dir.'</strong> ' ;
       if (@mkdir($abs_cur_dir.$new_name_dir)) echo '-<span class=green> OK</span><br>' ; else echo ' - <span class=red>директория уже существует или недостаточно прав</span><br>' ;
       unset($_SESSION['list_dir']) ; global $list_dir ; $list_dir=array() ;
       $this->read_dir_list($abs_root_upload_dir,'',$list_dir) ;
       return($otn_cur_dir.$new_name_dir.'/') ;

     } else echo '<div class=alert>Не указано имя создаваемого каталога</div><br>' ;
   }


    function show_file_info($options=array())
    { global $obj_info ;
      $file_patch=($options['obj_info']['patch'])? $options['obj_info']['patch']:$obj_info['patch'] ;
      $file_dir=($options['obj_info']['dir'])? $options['obj_info']['dir']:$obj_info['dir'] ;
      $file_name=($options['obj_info']['file_name'])? $options['obj_info']['file_name']:$obj_info['file_name'] ;
      //$otn_dir=dirname($file_dir) ;
      $file_type=mime_content_type($file_dir) ;
      $size_info=filesize($file_dir) ;
      $info=array() ;
      //$HTML_code='' ;
      $info[]=($file_type)? 'формат файла: <span class=title>'.$file_type.'</span>':'<span class=red> неизвестный формат файла</span><br>' ;
      $info[]='размер файла <span class=title>'.format_size($size_info).'</span>' ;

      switch ($file_type)
      { case 'text/html': $info[]='<div class="black">Открыть в новом окне: <a href="'.$obj_info['patch_page'].'" target="_blank">'.$obj_info['patch_page'].'</a></div><br>' ;
                          $HTML_code=file_get_contents($file_patch) ;
                          break ;
        case 'image/png':
        case 'image/gif':
        case 'image/jpeg':
        case 'image/bmp':
              $size=@getimagesize($file_dir) ;
              //$preview_element=$this->check_preview_image($otn_dir,'preview_'.$file_name) ;
              //if (!$preview_element) $preview_element=$file_name ;
              $HTML_code='<img src="'.safe_web_patch($file_patch).'" width="'.$size[0].'" height="'.$size[1].'" alt="" border="0">' ;
              $info[]='Размер изображения - <span class=title>'.$size[0].'x'.$size[1].'</span>' ;
              break;
       case   'application/x-shockwave-flash':
              $size=@getimagesize($file_dir) ;
              //$preview_element=$this->check_preview_image($otn_dir,'preview_'.$file_name.'.jpg')  ;
              //if (!$preview_element) $preview_element=$this->check_preview_image($otn_dir,'preview_'.$file_name.'.gif') ;
              //if (!$preview_element) $preview_element=$this->check_preview_image($otn_dir,'preview_'.$file_name.'.png')  ;
              //if (!$preview_element) $preview_element=$file_name ;
              $info[]='Размер флешки - <span class=title>'.$size[0].'x'.$size[1].'</span>' ;
              $HTML_code='<embed width="'.$size[0].'" height="'.$size[1].'" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" src="'.$file_patch.'" play="true" loop="true" menu="true"/>' ;
              break;

        case 'video/x-flv':
              $size=@getimagesize($file_dir) ;
              $width=($size[0])? $size[0]:400 ;
              $height=($size[1])? $size[1]:300 ;

              /*
              $preview_file=$this->check_preview_file($otn_dir,'preview_'.$file_name.'.jpg')  ;
              if (!$preview_file) $preview_file=$this->check_preview_file($otn_dir,'preview_'.$file_name.'.gif') ;
              if (!$preview_file)  $preview_file=$this->check_preview_file($otn_dir,'preview_'.$file_name.'.png')  ;
              //if (!$preview_file)  $preview_file=$file_name ;
              //echo 'preview_file='.$preview_file.'<br>' ;
              //echo 'Размер флешки - '.$size[0].'x'.$size[1].'<br>' ;
              if ($preview_file) $video_params['imagePath']=$preview_file ;
              $video_params['videoPath']=$file_patch ;
              $video_params['autoStart']='false' ;
              $video_params['autoHide']='false' ;
              $video_params['autoHideTime']=5 ;
              $video_params['hideLogo']='true' ;
              $video_params['volAudio']=60 ;
              if ($size[0]) $video_params['newWidth']=$size[0] ;
              if ($size[1]) $video_params['newHeight']=$size[1] ;
              $video_params['disableMiddleButton']='false' ;
              $video_params['playSounds']=true ;
              $video_params['soundBarColor']='0xCCCCCC' ;
              $video_params['barColor']='CCCCCC' ;
              $video_params['barShadowColor']='0xCCCCCC' ;
              $video_params['subbarColor']='0xCCCCCC' ;
              $flv_player=$patch_to_upload.'AddOns/flvPlayer.swf' ;
              $embed_str='<embed src="'.$flv_player.'?'.http_build_query($video_params).' width="'.$size[0].'" height="'.$size[1].'" wmode="transparent" allowfullscreen="false" quality="high" bgcolor="#CCCCCC" type="application/x-shockwave-flash"/>' ;
               */
              $HTML_code=generate_flvplayer_code(safe_web_patch($file_patch),$width,$height) ;
              break ;

        default:
              $HTML_code='<a href="'.safe_web_patch($file_patch).'" target=_blank>'.$file_name.'</a>' ;
              break;
        break;
      }

        ?><div id=panel_file_info><? echo implode(', ',$info) ; ?></div><?
        if ($HTML_code){ ?><div id=panel_file_cont><?echo $HTML_code?></div><?}
         /*    <span class=title>HTML код:</span>
             <br><textarea name="Name" class=text wrap="on" style="width:700px;height:70px;"><? echo $HTML_code ?></textarea>
         <?
         */

    }

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 //
 //  функции для разбора и показа скриптов сайта
 //
 ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



  function show_page_db_source()
  {


  }

  function show_page_php_source()
  { global $obj_info,$dir_to_root_NS ;
    if ($obj_info['class_file']) $file=$obj_info['class_file'] ;
      else if ($obj_info['template_file']) $file=$obj_info['template_file'] ;
      else $file=$dir_to_root_NS.$obj_info['patch'] ;
      if ($obj_info['class_file'] or $obj_info['template_file']) echo '<br><span class="green bold">Внимание!</span><span class=black> Вы получили доступ к php-коду страницы сайта. Если Вы не знаете, что это такое, не пытайтесь самостоятельно править код страницы.</span><br><br>' ;
      $cont=file_get_contents($file) ;
      /*
      ?><div class="black">Ссылка на страницу сайта: <a href="<?echo $obj_info['patch_page']?>" target="_blank"><?echo $obj_info['patch_page']?></a></div><br><?
      if ($obj_info['class_file']) {?><div class="black">Адрес скрипта для этой страницы: <span style="text-decoration:underline;"><?echo hide_server_dir($obj_info['class_file'])?></span></div><br><?
                                    ?><div class="black">Имя класса для этой страницы: <span style="text-decoration:underline;"><?echo $obj_info['class_name']?></span></div><br><?
                                   } ;
      if ($obj_info['template_file']){?><div class="black">Адрес шаблона скрипта для этой страницы: <span style="text-decoration:underline;"><?echo hide_server_dir($obj_info['template_file'])?></span></div><br><?
                                      ?><div class="black">Имя шаблона класса для этой страницы: <span style="text-decoration:underline;"><?echo $obj_info['template_name']?></span></div><br><?
                                     } ;
      */
      ?><textarea name=content cols="20" rows=10><?echo htmlspecialchars($cont)?></textarea><br><br>
        <button onclick="exe_cmd('save_text_page')">Сохранить</button>
        <style type="text/css">textarea{width:90%;height:500px;margin-left:20px;word-wrap:normal;}</style>
      <?

  }

  function show_page_text_source()
  {   global $obj_info ;
      $cont=file_get_contents($obj_info['dir']) ;
      ?><textarea name=content cols="20" rows=10><?echo htmlspecialchars($cont)?></textarea><br><br>
        <button onclick="exe_cmd('save_text_page')">Сохранить</button>
        <style type="text/css">textarea{width:90%;height:500px;margin-left:20px;word-wrap:normal;}</style>
      <?
  }

  function save_text_page()
    { global $obj_info,$content ;
      $file=($obj_info['class_file'])? $obj_info['class_file']:$obj_info['dir'] ;
      $cnt=file_put_contents($file,stripslashes($content)) ;
      if ($cnt) echo '<div class=green>Cтраница успешно сохранена</div>' ; else echo '<div class=alert>Не удалось сохранить страницу</div><br>' ;
    }

  function show_file_operation()
    { ?><ul>
          <li>Удалить <a href="#" onclick="exe_cmd('delete_page');return false;">cтраницу сайта</a></li>
        </ul>
      <?
    }


  function delete_page()
    { global $obj_info ;
      $this->safe_del_file($obj_info['dir']) ;
      if ($obj_info['class_file']) $this->safe_del_file($obj_info['class_file']) ;
      ?><script type="text/javascript">window.top.fra_view.location=window.top.fra_view.location;</script><?
      return(1);
    }

  function safe_del_file($file_dir)
  {  global $dir_to_root_NS ;
     $temp_dir=$dir_to_root_NS.'/!deleted/' ;
     $temp_name=str_replace('/','|',hide_server_dir($file_dir)) ;
     if (!is_dir($temp_dir) and !mkdir($temp_dir)) { echo 'Не удалось создать папку для хранения удаленных файлов. Создайте папку самостоятельно и попробуйте еще раз' ; return ; }
     if (!rename($file_dir,$temp_dir.$temp_name)) echo 'Не удалось удалить файл "<strong>'.hide_server_dir($file_dir).'</strong>"' ;
     else echo 'Файл "<strong>'.hide_server_dir($file_dir).'"</strong> - удален<br>' ;
     //echo 'from: '.$file_dir.'<br>' ;
     //echo 'to  : '.$temp_dir.$temp_name.'<br>';
  }


   function show_dialog_create_page()
    { global $use_as_catalog ;
      $arr_parent_class2=$this->get_base_class_to_new_page() ;
      $extensions=array('.html','.php','.css','.js','.txt','') ;
      ?><h1>Создание страницы сайта</h1>
      Укажите имя новой страницы сайта. Можете указать имя на русском языке, имя файла страницы будет автоматически преобразовано в транслит.<br><br>
      <input class="text large" type=text value="test2" name="new_page_name">
      <select name="new_page_ext"><?if (sizeof($extensions)) foreach($extensions as $value) echo '<option value="'.$value.'">'.$value.'</option>'?></select>
      <br><br>
      <input class=new_page_type_radio_check type="radio" name=new_page_type value=0 checked>Создать стандартную страницу сайта <span class="green bold">(рекомендуется)</span><br>
      <? /*<input class=new_page_type_radio_check type="radio" name=new_page_type value=1>Создать страницу на основе шаблона с прямым редактированием<br>*/?>
      <input class=new_page_type_radio_check type="radio" name=new_page_type value=2>Создать нестандартную страницу сайта (требуется знание php)<br>
      <input class=new_page_type_radio_check type="radio" name=new_page_type value=10>Создать текстовой файл на сайте<br>
      <div class="dir_file_parent">Выберите наследуемый шаблон: <select name="base_class"><?
          if (sizeof($arr_parent_class2)) foreach($arr_parent_class2 as $indx=>$parent_class)
           {  list($fname,$class_name)=explode(':',$indx) ;
              echo '<option value="'.$indx.'"'.(($class_name=='site')? 'selected':'').'>'.$fname.': '.$parent_class.' -> ...</option>' ;
           }
        ?></select></div>
        <script type="text/javascript">
           $('input.new_page_type_radio_check').change(function(){ if($(this).val()==0 || $(this).val()==10) $('div.dir_file_parent').css('display','none'); else $('div.dir_file_parent').css('display','block');});
        </script>
        <style type="text/css">
           div.dir_file_parent{display:none;margin:10px 0 10px 20px;}
           input.text{margin-left:20px;width:200px;}
        </style>
        <br>
        <button onclick="exe_cmd('show_dialog_create_page')">Обновить</button>
        <button onclick="exe_cmd('show_dialog_create_page_step_2')">Создать</button>
        <?
        return(0); // 0 - больше не выводить ничего

    }

    function show_dialog_create_page_step_2()
    { global $new_page_name,$new_page_type,$base_class,$obj_info,$new_page_ext;
      $new_page_name=safe_file_names($new_page_name) ;
      /*
      damp_array($obj_info) ;
      echo 'dir='.$obj_info['name'].'<br>';
      echo 'new_page_name='.$new_page_name.'<br>' ;
      echo 'new_page_ext='.$new_page_ext.'<br>' ;
      echo 'new_page_type='.$new_page_type.'<br>' ;
      echo 'base_class='.$base_class.'<br>'       ;
      */
      switch($new_page_type)
      { case 0: // создаем страницу на основе базового шаблона с редактированием через админку
                $this->create_page_from_standart_template($obj_info['name'],$new_page_name.$new_page_ext,$new_page_name) ;
                break ;
        case 2: // cоздаем страницу с собственным шаблоном
                $this->create_page_by_template($obj_info['name'],$new_page_name.$new_page_ext,'Новая страница',$base_class) ;
                break ;
        case 10:// создаем пустую страницу без шаблона
                $this->create_blank_page($obj_info['name'],$new_page_name.$new_page_ext) ;
                break ;
      }

      ?><br><button onclick="exe_cmd('show_dialog_create_page')">Создать еще одну страницу</button><?
      return(0); // 0 - больше не выводить ничего
    }

    function create_page_from_standart_template($dir,$fname,$title)
    { global $TM_pages ;
      echo '<h1>Создаем стандартную страницу сайта с редактированием через админку</h1>' ;
      $id=adding_rec_to_table($TM_pages,array('obj_name'=>$title,'clss'=>26,'href'=>$dir.$fname,'enabled'=>1,'parent'=>1)) ; //echo 'Добавлена запись в БД: '.$id.'<br>' ; echo 'dir='.$dir.'<br>' ; echo 'fname='.$fname.'<br>' ;
      $ini_patch=($dir=='/')? '':'../' ;
      // прописываем стандарный скрипт в корне
      $cont="<? // создано системой автоматического создания шаблонов страниц ".get_month_year(0,'day month year time')."\ninclude ('".$ini_patch."ini/patch.php')  ;\ninclude (\$dir_to_engine.'i_system.php') ;\n\$options=array();\nshow_page(\$options) ;\n?>" ;
      $cnt=create_page($dir,$fname,$cont) ;
      if ($cnt) {?><script type="text/javascript">window.top.fra_order.location=admin_dir+"file_explorer_viewer.php?cmd=view&fname=<?echo $dir.$fname?>" ;</script><?}
    }

    function create_page_by_template($dir,$fname,$title,$base_class)
    { echo '<h1>Создаем PHP страницу с собственным шаблоном</h1>' ;
      $ini_patch=($dir=='/')? '':'../' ;
      list($base_class,$parent_class_name)=explode(':',$base_class) ;
      $extends_class='extends c_'.$parent_class_name ;
      $exitends_include="include(_DIR_TO_CLASS.'".basename($base_class)."') ;" ;
      $class_file=get_name_class_file($dir,$fname) ; // таже функция, что используется в i_system при генерации страницы
      $class_name=get_name_class($dir,$fname) ;
      // прописываем стандарный скрипт в корне
      $cont="<? // создано системой автоматического создания шаблонов страниц ".get_month_year(0,'day month year time')."\ninclude ('".$ini_patch."ini/patch.php')  ;\ninclude (\$dir_to_engine.'i_system.php') ;\n\$options=array();\nshow_page(\$options) ;\n?>" ;
      $cnt=create_page($dir,$fname,$cont) ;
      if ($cnt)
      { $cont="<? // создано системой автоматического создания шаблонов страниц ".get_month_year(0,'day month year time')."\n".$exitends_include."\nclass ".$class_name." ".$extends_class."\n{\n\t// вывод основной информации\n\tfunction block_main()\n\t{\n\t\techo 'Данная система создана системой автоматического создания шаблонов страниц' ;\n\t}\n}\n?>" ;
        $cnt=create_page('/'._CLASS.'/',basename($class_file),$cont,'шаблон страницы сайта') ;
        if ($cnt) {?><script type="text/javascript">window.top.fra_order.location=admin_dir+"file_explorer_viewer.php?cmd=view&fname=<?echo $dir.$fname?>" ;</script><?}
      }
    }

    function create_blank_page($dir,$fname)
    { echo '<h1>Создаем текстовой файл на сайте</h1>' ;
      $cnt=create_page($dir,$fname,' ','текстовой файл') ;
      if ($cnt) {?><script type="text/javascript">window.top.fra_order.location=admin_dir+"file_explorer_viewer.php?cmd=view&fname=<?echo $dir.$fname?>" ;</script><?}
    }

 ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 //
 //  вспомогательные функции для разбора скриптов сайта
 //
 ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

     function parse_php_file($file_name)
    {
         global $dir_to_root_NS ;
         $matches=array() ;
         //echo $dir_to_root_NS.$file_name.'<br>';
         $cont=file_get_contents($dir_to_root_NS.$file_name);
         /*?><div class="blau small"><?echo nl2br($cont) ;?></div><?*/
         // делаем парсинг на include ;
         // \s* - ждем любое количество пробелов
         // (.*?) - ждем любой набор симоволов
         preg_match_all('/include\s*\((.*?)\)\s*;/is',$cont,$matches['include'],PREG_SET_ORDER) ;
         if (sizeof($matches['include'])) foreach($matches['include'] as $rec) $cont=str_replace($rec[0],'',$cont) ;
         // делаем парсинг на $options ;
         // \s* - ждем любое количество пробелов
         // [\"\']? - ждем " или '
         // ([^\'\">]+?) - ждем любой набор симоволов, в которые не входит ' или " или >
         // ([^;]+?) - ждем любой набор симоволов, в которые не входит ;
         preg_match_all('/\$options\s*\[\s*[\"\']?([^\'\"]+?)[\"\']?\s*\]\s*=\s*([^;]+?)\s*;/is',$cont,$matches['options'],PREG_SET_ORDER) ;
         if (sizeof($matches['options'])) foreach($matches['options'] as $rec) $cont=str_replace($rec[0],'',$cont) ;
         // делаем парсинг на объявленные переменные ;
         // \s* - ждем любое количество пробелов
         // [\"\']? - ждем " или '
         // ([^\'\">]+?) - ждем любой набор симоволов, в которые не входит ' или " или >
         // ([^;]+?) - ждем любой набор симоволов, в которые не входит ;
         preg_match_all('/\$(.*?)\s*=\s*(.*?)\s*;/is',$cont,$matches['vars'],PREG_SET_ORDER) ;
         if (sizeof($matches['vars'])) foreach($matches['vars'] as $rec) $cont=str_replace($rec[0],'',$cont) ;
         // делаем парсинг на функцию страницы ;
         // \s* - ждем любое количество пробелов
         // [\"\']? - ждем " или '
         // ([^\'\">]+?) - ждем любой набор симоволов, в которые не входит ' или " или >
         // ([^;]+?) - ждем любой набор симоволов, в которые не входит ;
         preg_match_all('/(\w*?)\((\S*?)\)\s*;/is',$cont,$matches['functions'],PREG_SET_ORDER) ;
         //echo nl2br($cont) ;
         return($matches);
    }

    function exec_config($arr)
    { $options=array() ;
      if (sizeof($arr)) foreach($arr as $rec) eval($rec[0]) ; // выполняем директивы из файла
      compatible_options_names($options) ; // преобразование старых имен опций в новые - i_system
      return($options);
    }

    function get_base_class_to_new_page()
    {   global  $dir_to_site_class ;
        $dir_class=get_files_list($dir_to_site_class) ;
        if (sizeof($dir_class)) foreach($dir_class as $rec)
         { $fname=basename($rec) ;
           //if (strpos($fname,'_')!==0 and $fname!='version_checked.php' and $fname!='mc_ext_moduls.php')
           if (strpos($fname,'c_')===0) // файл должен начинаться с префикса с_
           { //echo $fname.'<br>' ;
             $text=file_get_contents($rec) ;
             if (preg_match_all('/class c_(.+?) extends c_(.+?) /is',$text,$matches,PREG_SET_ORDER)) ;
               $arr_parent_class2[hide_server_dir($rec).':'.$matches[0][1]]='c_'.str_replace('{','',$matches[0][2]).' -> c_'.$matches[0][1]  ;
           } //else echo '<div class=red>'.$fname.'</div>';
         }
         asort($arr_parent_class2);
         return($arr_parent_class2) ;
    }

    function show_conf_file()
    { global $obj_info ;
      $conf=$obj_info['config'] ;
      if (sizeof($conf['include'])) {?><h1>Include</h1><ul><? foreach($conf['include'] as $rec) echo '<li>'.$rec[1].'</li>' ; ?></ul><?}
      if (sizeof($conf['options'])) {?><h1>Options</h1><ul><? foreach($conf['options'] as $rec) echo '<li>'.$rec[1].' = '.$rec[2].'</li>' ; ?></ul><?}
      if (sizeof($conf['vars'])) {?><h1>Vars</h1><ul><? foreach($conf['vars'] as $rec) echo '<li>'.$rec[1].' = '.$rec[2].'</li>' ; ?></ul><?}
      if (sizeof($conf['functions'])) {?><h1>Functions</h1><ul><? foreach($conf['functions'] as $rec) echo '<li>'.$rec[1].'('.$rec[2].')</li>' ; ?></ul><?}
    }


    function show_a_to_edit_page($href)
    {?><br><br>
       Через несколько секунд Вы будут перенаправлены на страницу редактирования новой страницы сайта.<br>
       Если Ващ  браузер не поддерживает автоматического перенаправлния, Вы можете перейти к правке содержания страницы по этой <a href="<?echo $href?>" class="black bold">ссылке</a>
       <br><br>
       <?return;?>
       <noscript><meta http-equiv="Refresh" content="2; URL=<?echo $href?>" /></noscript>
        <script type="text/javascript"><!--
          function exec_refresh()
          { window.status = "Переадресация..." + myvar;
            myvar = myvar + " .";
            var timerID = setTimeout("exec_refresh();", 100);
              if (timeout > 0) {timeout -= 1; }
              else
              { clearTimeout(timerID);
                window.status = "";
                window.location = "<?echo $href?>";
              }
          }
          var myvar = "";
          var timeout = 20;
          exec_refresh();
      //--></script>
     <?
    }

 ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

   /*
   function check_preview_image($otn_dir,$file_name)
   { global $abs_root_upload_patch,$abs_root_upload_dir ;
     $preview_dir=$abs_root_upload_dir.$otn_dir.$file_name ;
     $preview_patch=$abs_root_upload_patch.$otn_dir.$file_name ;
     //echo $preview_dir.'<br>' ;
     if (is_file($preview_dir))
     { $preview_patch=$abs_root_upload_patch.$otn_dir.$file_name ;
       $size_prev=@getimagesize($preview_dir) ;
       $preview_element='<img src="'.$preview_patch.'" width='.$size_prev[0].' height='.$size_prev[1].' alt="" border="0">' ;
     }
     else $preview_element='' ;
     return($preview_element) ;
   }

   function check_preview_file($otn_dir,$file_name)
   { global $abs_root_upload_patch,$abs_root_upload_dir ;
     $preview_dir=$abs_root_upload_dir.$otn_dir.$file_name ;
     $preview_patch=$abs_root_upload_patch.$otn_dir.$file_name ;
     if (is_file($preview_dir)) return($preview_patch) ;
   }




    // возвращает в простом массиве список всех поддиректорий указанной директории
    function read_dir_list($dir,$name,&$list_dir=array(),$level=0)
    { $list_obj=scandir($dir) ;
      $list_dir[]=$name ;
         $level++ ;
         //echo $level.'<br>' ;
         if (sizeof($list_obj)) foreach($list_obj as $i=>$fname) if ($fname!='.' and $fname!='..')
        { $cur_obj=$dir.$fname ;
          if (is_dir($cur_obj)) $this->read_dir_list($cur_obj.'/',$name.$fname.'/',$list_dir,$level) ;
        }
    }

    */










 }



function block_main2()
   { global $select_dir,$list_dir,$abs_cur_dir,$abs_cur_patch,$otn_cur_dir,$abs_root_upload_dir,$abs_root_upload_patch,
              $patch_to_upload,$dir_to_upload	;

     $abs_root_upload_dir=$dir_to_upload ;          // аболютный dir-путь к папке загрузки
     $abs_root_upload_patch=$patch_to_upload ;      // абсолютный patch-путь к папке загрузки

     //session_register('list_dir') ;  $list_dir=array() ;
     //if (!sizeof($list_dir)) { $list_dir=array() ; $this->read_dir_list($abs_root_upload_dir,'',$list_dir) ; }
     //damp_array($list_dir) ;
     //echo 'select_dir='.$select_dir.'<br>' ;
     if (!$select_dir) $select_dir=0 ;

     //$otn_cur_dir=$list_dir[$select_dir] ;							// относительный путь текущей директории
     //$abs_cur_dir=$abs_root_upload_dir.$otn_cur_dir ;               //  абсолютный dir-путь к текущей директории загрузки
     //$abs_cur_patch=$patch_to_upload.$otn_cur_dir ;                   // абсолютный patch-путь к текущей директории загрузки


     //damp_array($list_dir) ;

     ?><br>
       <table class=left>
            <tr><td>Выберите папку для загрузки: <span class="black bold"><?echo $abs_root_upload_patch?></span></td>
              <td><select size="1" name="select_dir" class='text' >
                    <? if (sizeof($list_dir)) foreach($list_dir as $i=>$dir_name){?><option value="<?echo $i?>" <?if ($select_dir==$i) echo 'selected'?>><?echo $dir_name?></option><?}?>
                  </select>
              </td>
              <td><button onclick="exe_cmd('show_files')">Посмотреть файлы</button></td>
            </tr>
            <tr><td>Создать подкаталог:</td><td><input name="new_name_dir" type="text" class=text value=""></td><td><button onclick="exe_cmd('create_dir')">Создать подкаталог</button></td></tr>
         <tr><td>Выберите файл на диске вашего компьютера:</td><td><INPUT   name="upload_file" type=file class=file></td></tr>
         <tr><td>или укажите ссылку на файл в интернете:</td><td><input name="upload_file_by_href" type=text class=text size=100 value=""></td></tr>
         <tr><td>Сгенерировать превью<br><span class=green>только для изображений, если планируется показ через<br>всплывающее окно или панель</span>:</td><td><input name="generate_preview" type="checkbox" value="ON"> размер <input name="preview_width" type="text" class=text style="width:40px;" value="150"> х <input name="preview_height" type="text" class=text style="width:40px;" value="150"> pix</td></tr>
         <tr><td>Загрузить превью<br><span class=green>для всех остальных типов файлов, если планируется показ через<br>всплывающее окно или панель</span>:</td><td><INPUT   name="upload_file_preview" type=file class=file></td></tr>
       </table>
       <button onclick="exe_cmd('upload_file')">Загрузить файл</button>
       <button onclick="exe_cmd('')">В начало</button>
       <br><br>

     <?

     global $cmd,$cur_file ;
     //if (method_exists($this,$cmd)) $this->$cmd() ;
     //$this->show_files() ;
     //return ;
     /*
     //else
       { ?><h2>Рекомендации по загрузке изображений</h2>
              <p><lu><li>Размер изображений - не более 1000х700 pix</li>
                      <li>Размер файла - не более 300 кб</li>
                      <li>Для показа изображения через всплывающее окно или панель необходимо иметь превьюшку изображения.<br>
                          Для этого установить соответствующую галку в поле "Сгенерировать превью", и тогда она будет создана автоматически с заданными Вами размерами<br>
                          Если галка "Сгенерировать превью" не будет отмечена, то в качестве ссылки будет использовано имя файла</li>
                       <li>При генерации превьюшки создается файл на основе загружаемого, но с именем в начало которого добавлено 'prev_' ;</li>
                  </lu>
              </p>
              <h2>Рекомендации по загрузке флешек</h2>
              <p><lu><li>Размер флеш - по ширине той части страницы, куда они загружаются</li>
                      <li>Размер файла - не более 5 Мб</li>
                      <li>Для показа флешки через всплывающее окно или панель необходимо иметь изображение-превьюшку флешки.<br>
                          Файл превьюшки загружается через поле "Загрузить превью" - с вашего комрьютера или адреса в интернете.<br>
                          Если файл для превьюшки не будет отмечен, то в качестве ссылки будет использовано имя файла</li>
                       <li>При загрузки превьюшки она получает имя на основе загружаемой флешки, но в начало будет добавлено 'prev_' ;</li>
                  </lu>
              </p>
            <?
          }

      */
       global $obj_info ;
       ?><script type="text/javascript">
          $('a.to_file').click(function()
          { window.top.fra_order.location=admin_dir+"file_explorer_viewer.php?cmd=view&fname=<?echo $obj_info['name']?>"+$(this).text() ;
            return(false) ;
          });


         </script>
       <?

   }


?>