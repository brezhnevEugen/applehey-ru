<?php
include_once(_DIR_TO_ENGINE."/admin/i_clss_func.php") ;
include_once(_DIR_TO_ENGINE.'/admin/c_menu_tabs.php') ;
include_once(_DIR_TO_ENGINE.'/admin/c_admin_page.php') ;

class c_sys_info extends c_admin_page
{
  //function body() { $this->body_1x() ; }
  function block_main()
  { ?><h1>Настройки движка для сайта</h1><?
    include(_DIR_TO_ROOT.'/ini/patch.php') ;
    ?><table>
    	<tr><td class=left>Перезагрузка всех данных сайта</td><td><?echo ($reboot_all)? 'Вкл':'-'?></td></tr>
    	<tr><td class=left>Перезагрузка настроек сайта</td><td><?echo ($reboot_setting)? 'Вкл':'-'?></td></tr>
    	<tr><td class=left>Перезагрузка модели ОТ</td><td><?echo ($reboot_tables)? 'Вкл':'-'?></td></tr>
    	<tr><td class=left>Перезагрузка системы информации сайта</td><td><?echo ($reboot_info_system)? 'Вкл':'-'?></td></tr>
    	<tr><td class=left>Перезагрузка системы банеров сайта</td><td><?echo ($reboot_ban_system)? 'Вкл':'-'?></td></tr>
        <tr><td colspan=2>&nbsp;</td></tr>
    	<tr><td class=left>Режим отладки</td><td><?echo ($debug)? 'Вкл':'-'?></td></tr>
    	<tr><td class=left>Режим отладки SQL</td><td><?echo ($debug_db)? 'Вкл':'-'?></td></tr>
    	<tr><td class=left>Режим трассировки по времени</td><td><?echo ($time_line_mode)? 'Вкл':'-'?></td></tr>
        <tr><td class=left>Показывать ошибки SQL</td><td><?echo ($info_db_error)? 'Вкл':'-'?></td></tr>
        <tr><td class=left>display_errors</td><td><?echo (ini_get('display_errors'))? 'Вкл':'-'?></td></tr>
        <tr><td class=left>display_startup_errors</td><td><?echo (ini_get('display_startup_errors'))? 'Вкл':'-'?></td></tr>
        <tr><td class=left>track_errors</td><td><?echo (ini_get('track_errors'))? 'Вкл':'-'?></td></tr>
        <tr><td class=left>error_reporting</td><td><?echo error_reporting();?></td></tr>
        <tr><td colspan=2>&nbsp;</td></tr>
    	<tr><td class=left>Ведение лога загруженных страниц</td><td><?echo ($log_system_info)? 'Вкл':'-'?></td></tr>
    	<tr><td class=left>Ведение лога входов на сайт</td><td><?echo ($log_logons)? 'Вкл':'-'?></td></tr>
        <tr><td colspan=2>&nbsp;</td></tr>
    	<tr><td class=left>Имя директории для загрузки файлов</td><td><?echo _PUBLIC_?></td></tr>
      </table>

    <h1>Настройки движка для админки</h1><?
    include(_DIR_TO_ENGINE.'/i_admin.php') ;
    ?><table>
    	<tr><td class=left>Режим отладки</td><td><?echo ($debug)? 'Вкл':'-'?></td></tr>
    	<tr><td class=left>Режим отладки SQL</td><td><?echo ($debug_db)? 'Вкл':'-'?></td></tr>
    	<tr><td class=left>Режим трассировки по времени</td><td><?echo ($time_line_mode)? 'Вкл':'-'?></td></tr>
        <tr><td class=left>Показывать ошибки SQL</td><td><?echo ($info_db_error)? 'Вкл':'-'?></td></tr>
        <tr><td class=left>display_errors</td><td><?echo (ini_get('display_errors'))? 'Вкл':'-'?></td></tr>
        <tr><td class=left>display_startup_errors</td><td><?echo (ini_get('display_startup_errors'))? 'Вкл':'-'?></td></tr>
        <tr><td class=left>track_errors</td><td><?echo (ini_get('track_errors'))? 'Вкл':'-'?></td></tr>
        <tr><td class=left>error_reporting</td><td><?echo error_reporting();?></td></tr>
        <tr><td colspan=2>&nbsp;</td></tr>
    	<tr><td class=left>Ведение лога загруженных страниц</td><td><?echo ($log_system_info)? 'Вкл':'-'?></td></tr>
    	<tr><td class=left>Ведение лога входов на сайт</td><td><?echo ($log_logons)? 'Вкл':'-'?></td></tr>
        <tr><td colspan=2>&nbsp;</td></tr>
      </table>
    <?
  }
}

function show_system_info()
 {

// определяем параметры, значения которых критичны
  ?><h1>Переменные patch.php</h1><?

/*
  echo   'IP='.IP.'<br>'.
  		 'server_engine_vers='._SERVER_ENGINE_VERS.'/'.'<br>'.
  		 '<br>'.
  		 'project_name='._MAIN_DOMAIN.'<br>'.
  		 'site_name='._MAIN_DOMAIN.'<br>'.
  		 '<br>'.
  		 'dir_to_root='._DIR_TO_ROOT.'/'.'<br>'.
  		 'dir_to_engine='._DIR_TO_ENGINE.'/'.'<br>'.
  		 'dir_to_class='.$dir_to_class.'<br>'.
  		 '<br>'.
  		 'patch_to_site='.$patch_to_site.'<br>'.
  		 'patch_to_admin='._PATH_TO_ADMIN.'/'.'<br>'.
  		 'patch_to_engine='._PATH_TO_ENGINE.'/'.'<br>'.
  		 '<br>'.
  		 'template_dir='.$template_dir.'<br>'.
  		 'template_file='.$template_file;
  */
  global $TM_artikle ;
  ?><h1>Имена таблиц БД</h1><?
  damp_array(get_defined_vars()) ;



  ?><h1>Конфигурация php</h1><?
  $params=array('register_globals'=>1,
  				'short_open_tag'=>1,
  				'allow_call_time_pass_reference'=>0,
  				'safe_mode'=>0,
  				'track_var'=>1,
  				'register_long_arrays'=>0,
  				'variables_order'=>"EGPCS",
  				'register_argc_argv'=>0,
  				'post_max_size'=>'8M',
  				'magic_quotes_gpc'=>1,
  				'magic_quotes_runtime'=>0,
  				'display_errors'=>0,
  				'display_startup_errors'=>0,
  				//'error_prepend_string'=>'<font color=ff0000>',
  				//'error_append_string'=>'</font>',
  				'memory_limit'=>'8M',
  				'report_memleaks'=>1,
  				'file_uploads'=>1,
  				'upload_max_filesize'=>'2M',
  				'allow_url_fopen'=>1,
  				'define_syslog_variables'=>0,
  				'session.auto_start'=>0,
  				'session.use_cookies'=>1,
  				'session.use_only_cookies'=>1,
  				'session.gc_probability'=>1,
  				'session.gc_divisor'=>100,
  				'session.gc_maxlifetime'=>1440,
  				'session.bug_compat_42'=>0,
  				'session.bug_compat_warm'=>1,
  				'session.use_trans_sid'=>0,
  				'zend.ze1_compatibility_mode'=>0,

  ) ;

  $cur_value=ini_get_all() ;
  ?><table>
      <tr class=header>
          <td>Название параметра</td>
          <td>Глобальное значение</td>
          <td>Текущее значение</td>
          <td>Требуемое значение</td>
          <td>Доступ</td>
      </tr><?
  if (sizeof($cur_value)) foreach($cur_value as $name=>$rec)
   {if (isset($params[$name])) { switch ($rec['access'])
                                 { case 1: $show_access='ini_set()' ; break;
                                   case 2: $show_access='.htaccess, httpd.conf, php.ini' ; break;
                                   case 4: $show_access='httpd.conf, php.ini ' ; break;
                                   case 6:
                                   case 7: $show_access='ini_set(), .htaccess, httpd.conf, php.ini' ; break;
                                   default: $show_access=$rec['access'] ;
                                 }
   								 $color=(($params[$name]==$rec['local_value'])? 'class=back_green':'class=back_red2') ;
   							   }
     else { $color='' ; $show_access='' ; }
    ?><tr><td class=left <?echo $color?>><?echo $name?></td>
          <td <?echo $color?>><?echo $rec['global_value']?></td>
          <td <?echo $color?>><?echo $rec['local_value']?></td>
          <td <?echo $color?>><?if (isset($params[$name])) echo $params[$name]?></td>
          <td><? echo  $show_access ;?></td>
      </tr><?}
  ?></table><?


 }

?>