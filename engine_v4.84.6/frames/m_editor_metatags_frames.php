<?php
include_once(_DIR_TO_ENGINE."/admin/i_clss_func.php") ;
include_once(_DIR_TO_ENGINE.'/admin/c_menu_tabs.php') ;
//damp_array(get_declared_classes()) ;
// системные утилиты
class c_editor_metatags extends c_editor_obj
{
  function body($options=array()) { $this->body_frame_2x(array('fra_tree'=>_PATH_TO_ADMIN.'/editor_metatags_tree.php','title'=>$options['title'])) ; }
}


class c_editor_metatags_tree extends c_fra_tree
{

  function body($options=array())
  {?><body id=tree_objects>
      <ul class="tree_root" on_click_script="editor_metatags_viewer.php">
         <li clss=1 cmd="stats_info">Редактор метатегов</li>
         <ul>
             <li clss=1>Страницы сайта</li>
             <ul>
                 <!--<li clss=1 cmd=show_van_pages>Отдельные страницы</li>-->
                 <?if ($_SESSION['TM_goods']){?><li clss=1 cmd=show_pages_goods>Товары</li><?}?>
                 <?if ($_SESSION['TM_artikle']){?><li clss=1 cmd=show_pages_art>Статьи</li><?}?>
                 <?if ($_SESSION['TM_news']){?><li clss=1 cmd=show_pages_news>Новости</li><?}?>
             </ul>

             <li clss=1>Мусорные URL</li>
             <ul>
                 <li cmd=show_trash_pages>Все страницы</li>
                 <li cmd=show_trash_pages_param>Все страницы с параметрами</li>
                 <!--<li cmd=show_trash_pages_200>200</li>-->
                 <li cmd=show_trash_pages_301>301</li>
                 <li cmd=show_trash_pages_404>404</li>
                 <li cmd=show_trash_pages_attack>Дятло-хакеры</li>
             </ul>
             <li clss=1>Операции</li>
             <ul>
                 <li cmd=set_redirect_301>Задать 301 редиректы</li>
             </ul>
             <li cmd=show_robots>robots.txt</li>
             <li cmd=show_meta_info>Метаданные HEADER</li>
         </ul>
      </ul>
      </body>
      <script type="text/javascript">$j('ul.tree_root > li:first-child').each(tree_item_click)</script><? // имитируем клик по корню
  }
}

class c_editor_metatags_viewer extends c_fra_based
{

   function stats_info()
   {   echo '</form>' ;
       ?><h1>Информация по доменам</h1><?
       $res=$this->get_arr_host_name_from_site_map() ;
       damp_array($res,1,-1) ;
       if (sizeof($res))
       { ?><form><?
         ?>Изменение домена <select name=from_text><?
         foreach($res as $name=>$cnt) {?><option value="<?echo $name?>"><?echo $name?> [<?echo $cnt?>]</option><?}
         ?></select> на <input class=text type=text size=20 name=to_text>
         <input type="submit" class="v1" cmd=replace_hostname_in_sitemap value="Заменить"><br><br></form>
         <form>
         Изменение фрагмента URL <input class=text type=text name=from_text size="20"> на <input class=text type=text size=20 name=to_text>
         <input type="submit" class="v1" cmd=replace_str_in_urls value="Заменить"><br><br></form>
       <?
       } else echo 'В sitemap не найдено не одной записи' ;

      ?><form><h2>Удаление страниц из карты сайта</h2>
        фрагмент url: <input class=text type=text size=20 name=del_url><button cmd=delete_from_sitemap>Удалить</button><br><br></form>
      <?
        return ;
      ?>
       <form><h2>Дубликаты URL</h2>
      <? $res=array() ;
         $list=execSQL('select url,count(pkey) as cnt from '.TM_SITEMAP.' where clss=36 and url!="" and not url is null group by url order by cnt desc,url') ;
         if (sizeof($list)) foreach($list as $rec) if ($rec['cnt']>1)
         { $list2=execSQL_Line('select pkey as id from '.TM_SITEMAP.' where clss=36 and url="'.$rec['url'].'" order by c_data desc',array('no_indx'=>1,'debug'=>0)) ;
           if (sizeof($list2)) foreach($list2 as $id) $res[]=$id ;
         }
         $str_id=implode(',',$res) ;
         if (sizeof($res)) _CLSS(36)->show_list_items(TM_SITEMAP,'pkey in ('.$str_id.')',array('default_order'=>'url')) ;
         else echo 'Дубликатов страниц не обнаружено<br><br>' ;
         ?></form><?
       echo '<form>' ;
   }

   function get_arr_host_name_from_site_map()
    {
        $list_url=execSQL('select pkey,url from '.TM_SITEMAP.' where clss=36  and url!="" and not url is null order by url') ;
        $res=array() ;
        if (sizeof($list_url)) foreach($list_url as $rec)
        { $arr=explode('/',$rec['url']) ;
          $res[$arr[0]]++ ;
        }
        return($res) ;
    }

    function replace_hostname_in_sitemap()
      { ?><h1>Заменяем адрес хоста для всех страниц сайта</h1><?
        echo 'Меняем <strong>'.$_POST['from_text'].'</strong> на <strong>'.$_POST['to_text'].'</strong><br>' ;
        if ($_POST['from_text'] and $_POST['to_text'])
        { $sql='update '.TM_SITEMAP.' set url=replace(url,"'.$_POST['from_text'].'","'.$_POST['to_text'].'")' ;
          execSQL_update($sql,array('log_result'=>1)) ;
        }
        $this->stats_info() ;
      }      

     function replace_str_in_urls()
      { ?><h1>Заменяем фрагмент URL для всех страниц сайта</h1><?
        echo 'Меняем <strong>'.$_POST['from_text'].'</strong> на <strong>'.$_POST['to_text'].'</strong><br>' ;
        if ($_POST['from_text'])
        { $sql='update '.TM_SITEMAP.' set url=replace(url,"'.$_POST['from_text'].'","'.$_POST['to_text'].'")' ;
          execSQL_update($sql,array('log_result'=>1)) ;
        }
        $this->stats_info() ;
      }      
      

    function delete_from_sitemap()
      { if ($_POST['del_url'])
        { ?><h1>Удаляем страницы из sitemap</h1><?
          echo 'Удаляем страницы с url, содержащими <strong>'.$_POST['del_url'].'</strong><br>' ;
          $sql='delete from '.TM_SITEMAP.' where url like "%'.$_POST['del_url'].'%"' ;
          execSQL_update($sql,array('log_result'=>1)) ;
        }
        $this->stats_info() ;
      }

    function get_usl_attack($not='')
    {  global $url_attack_string ; // i_base.php
       $arr=array() ;
       if (sizeof($url_attack_string)) foreach($url_attack_string as $str) $arr[]='url like \'%'.$str.'%\'' ;
       $usl_attack=(sizeof($arr))?  ' and '.$not.' ('.implode(' or ',$arr).')':'' ;
       return($usl_attack) ;
    }

    function show_robots()
    {  $text=file_get_contents(_DIR_TO_ROOT.'/robots.txt') ;
       ?><h1>Файл robox.txt</h1>
         <textarea name=robotx_cont rows="40" cols="100"><?echo $text?></textarea><br><br>
         <button cmd=save_robots_content>Сохранить</button>
        <?
    }

    function save_robots_content()
    { if ($_POST['robotx_cont'])
        { if (file_put_contents(_DIR_TO_ROOT.'/robots.txt',$_POST['robotx_cont'])) echo '<div class=green>Файл успешно сохранен</div>' ;
          else                                                                     echo '<div class=red>Не удалось сохранить файл, проверьте права доступа к /robots.txt</div>' ;
        }
      $this->show_robots() ;
    }


    function show_meta_info()
    {
       ?><h1>Метаданные в HEADER страницы</h1><?
       include_once(_DIR_TO_CLASS.'/c_page.php') ;
       $page=new c_page() ;
       if (sizeof($page->HEAD['meta']))
       { ?><h2>Данные, заданные в c_page:</h2><?
         print_2x_arr($page->HEAD['meta']) ;
         ?><div class="info">Внимание! Для изменения этих данных Вам необходимо отредактировать /class/c_page.php</div><?

       }
       //damp_array($page) ;


    }

    function show_list_urls($usl)
    { $list_URL=execSQL('select pkey as id,url,status,script,class from '.TM_SITEMAP.' where '.$usl.' order by url') ;
      if (sizeof($list_URL)) foreach($list_URL as $id=>$rec)
      {  if (isset($rec['url'])) $list_URL[$id]['url']='<a href="http://'.$rec['url'].'" target=_blank>'.$rec['url'].'</a>' ;

      }
      echo 'Найдено: <strong>'.sizeof($list_URL).'</strong> URL <br>' ;
      if (sizeof($list_URL)) print_2x_arr($list_URL,array('no_show_index'=>1)) ;
    }


/******************************************************************************************************************************************************************************************

  МУСОРНЫE URL

******************************************************************************************************************************************************************************************/
    function show_trash_pages()
    { ?><h1>Мусорные URL</h1><?
      $this->show_list_urls('status!=200 and obj_reffer="" or obj_reffer is null '.$this->get_usl_attack('not')) ;
    }

    function show_trash_pages_param()
    { ?><h1>Мусорные URL c параметрами</h1><button script="ext/sitemap/map_func.php" cmd="sitemap_delete_pages_mit_params">Удалить эти записи из карты сайта</button><?
      $this->show_list_urls('url like "%?%" and  (obj_reffer="" or obj_reffer is null) '.$this->get_usl_attack('not')) ;
    }

    function show_trash_pages_200()
    { ?><h1>Мусорные URL, статус 200</h1><?
      $this->show_list_urls('status=200 and  (obj_reffer="" or obj_reffer is null) '.$this->get_usl_attack('not')) ;
    }

    function show_trash_pages_301()
    { ?><h1>Мусорные URL, статус 301</h1><?
      $this->show_list_urls('status=301 and  (obj_reffer="" or obj_reffer is null) '.$this->get_usl_attack('not')) ;
    }

    function show_trash_pages_404()
    { ?><h1>Мусорные URL, статус 404</h1><?
      $this->show_list_urls('status=404 and  (obj_reffer="" or obj_reffer is null) '.$this->get_usl_attack('not')) ;
    }

    function show_trash_pages_attack()
    { ?><h1>Мусорные URL, попытка атаки через параметры URL</h1><?
      $this->show_list_urls('url like "%?%" and  (obj_reffer="" or obj_reffer is null) '.$this->get_usl_attack()) ;
    }


/******************************************************************************************************************************************************************************************

  URL СТРАНИЦ ОБЪЕКТОВ САЙТА

******************************************************************************************************************************************************************************************/


    function show_all_pages_SEO()
    {
      _CLSS(36)->show_list_items(TM_SITEMAP,"title!='' or description!='' or keywords!='' or h1!='' or SEO_text1!='' or SEO_text2!='' or SEO_text3!='' or SEO_text4!='' or SEO_text5!='' or SEO_text6!='' or SEO_text7!='' or SEO_text8!='' or SEO_text9!='' or SEO_text10!=''",array('buttons'=>array('delete'))) ;
    }

    function show_all_pages_not_SEO()
    {
      _CLSS(36)->show_list_items(TM_SITEMAP,"(title='' or title is null) and (description='' or description is null) and (keywords='' or keywords is null)  and  (h1='' or h1 is null) and (SEO_text1='' or SEO_text1 is null) and (SEO_text2='' or SEO_text2 is null) and (SEO_text3='' or SEO_text3 is null) and (SEO_text4='' or SEO_text4 is null) and (SEO_text5='' or SEO_text5 is null) and (SEO_text6='' or SEO_text6 is null) and (SEO_text7='' or SEO_text7 is null) and (SEO_text8='' or SEO_text8 is null) and (SEO_text9='' or SEO_text9 is null) and (SEO_text10='' or SEO_text10 is null)",array('buttons'=>array('delete')));
    }

    function show_all_pages_subdomain()
    {
      _CLSS(36)->show_list_items(TM_SITEMAP,"url like '%."._BASE_DOMAIN."%' and url not like '%"._MAIN_DOMAIN."%'",array('buttons'=>array('delete'))) ;
    }


   function show_van_pages()
   { // собираем страницы в статусе 200, но не привязанные к объекту
     ?><h1>Страницы сайта, созданные через собственный скрипт</h1><?
     $list_URL=execSQL('select pkey as id,url,status,script,class from '.TM_SITEMAP.' where status=200 and  (obj_reffer="" or obj_reffer is null) '.$this->get_usl_attack('not').' order by url') ;
     if (sizeof($list_URL)) foreach($list_URL as $id=>$rec)
     {  if (isset($rec['url'])) $list_URL[$id]['url']='<a href="http://'.$rec['url'].'" target=_blank>'.$rec['url'].'</a>' ;

     }
     echo 'Найдено: <strong>'.sizeof($list_URL).'</strong> URL <br>' ;
     if (sizeof($list_URL)) print_2x_arr($list_URL,array('no_show_index'=>1)) ;

     $this->system_export_pages_to_sitemap2($_SESSION['pages_system']) ;
   }

function system_export_pages_to_sitemap2($system)
{ //damp_array($system) ;
  if (!$system->no_root_dir) $arr_id[]=1;
  $list_pages=execSQL('select pkey,parent,obj_name,href,url_name from '.$system->table_name.' where '.$system->usl_show_items.' order by parent,indx',2) ;
  $system->prepare_public_info_for_arr($list_pages) ;
  damp_array($list_pages) ;

  if (sizeof($arr_id)) foreach($arr_id as $section_id)
  { $res_arr=array() ;
    ?><h1>Страницы сайта, созданные через админку</h1><?
    $list_URL=execSQL('select * from '.TM_SITEMAP.' where obj_reffer like "%.'.$system->tkey.'" order by url',2) ;

    // добавляем в список страницы товаров
    if (sizeof($list_pages)) foreach($list_pages as $rec_obj) $this->append_page_by_obj($rec_obj,$list_URL,$res_arr) ;
//damp_array($res_arr) ;

    if (sizeof($res_arr))
    { foreach($res_arr as $id=>$rec)
      { if (isset($rec['url'])) $res_arr[$id]['url']='<a href="http://'.$rec['url'].'" target=_blank>'.$rec['url'].'</a>' ;
        if (isset($rec['Имя'])) $res_arr[$id]['Имя']='<div reffer="'.$rec['obj_reffer'].'" class=to_edit>'.$rec['Имя'].'</div>' ;
      }
      print_2x_arr($res_arr,array('no_show_index'=>1)) ;
    }
  }

}



   function show_pages_goods()
   {
     $this->system_export_pages_to_sitemap($_SESSION['goods_system']) ;
   }

   function show_pages_news()
   {
     $this->system_export_pages_to_sitemap($_SESSION['news_system']) ;
   }

   function show_pages_art()
   {
     $this->system_export_pages_to_sitemap($_SESSION['art_system']) ;
   }


 //-----------------------------------------------------------------------------------------------------------------------------
 // goods_system
 //-----------------------------------------------------------------------------------------------------------------------------
   function system_export_pages_to_sitemap($system,$options=array())
   { //damp_array($system) ;
     if (!$system->no_root_dir) $arr_id[]=($system->tree['root']->pkey)? $system->tree['root']->pkey:1;
     else                       $arr_id=$system->tree['root']->list_obj ;
     $list_pages_goods=execSQL('select pkey,parent,obj_name,url_name from '.$system->table_name.' where '.$system->usl_show_items.' order by parent,indx') ;
     $system->prepare_public_info_for_arr($list_pages_goods) ;
     $list_pages=group_by_field('parent',$list_pages_goods) ;
     //damp_array($list_pages) ;
     $list_URL=execSQL('select * from '.TM_SITEMAP.' where URL like "%'._BASE_DOMAIN.'%" and (obj_reffer!="" or not obj_reffer is null) order by url') ;

     if (sizeof($arr_id)) foreach($arr_id as $section_id)
     { $res_arr=array() ;  //damp_array($system->tree[$section_id]) ;
       ?><h1><?echo $system->tree[$section_id]->name?></h1><?
       $GLOBALS['urls_of_sections']=array() ;
       if (isset($system->tree[$section_id])) $system->tree[$section_id]->exec_func_recursive('get_url_of_obj',array('params_is_obj'=>1)) ;
       // собираем в один массив URL разделов и товаров
       if (sizeof($GLOBALS['urls_of_sections'])) foreach($GLOBALS['urls_of_sections'] as $subsection_id=>$rec_section)
           {  // добавляем в список страницу раздела
              $this->append_page_by_obj($rec_section,$list_URL,$res_arr) ;

             // добавляем в список страницы товаров
             if (sizeof($list_pages[$subsection_id])) foreach($list_pages[$subsection_id] as $rec_obj) $this->append_page_by_obj($rec_obj,$list_URL,$res_arr) ;
           }
      if (sizeof($res_arr))
      {  _CLSS(36)->show_buttons(array('buttons'=>array('save','delete'))) ; ?><br><br><?
         _CLSS(36)->show_list_items($res_arr,array('button'=>array('delete'))) ;
      }
     }

   }

    // служебная функция - выбор из списка $list_URL записей по текущему объекту $rec_obj и занесение их в $res_arr
    function append_page_by_obj(&$rec_obj,&$list_URL,&$res_arr)
     { $cnt=0 ;
       $obj_href=str_replace(array('http://','https://'),'',$rec_obj['__href']) ;
       if (!_USE_SUBDOMAIN)  $obj_href=_MAIN_DOMAIN.$obj_href ;
       //  составляем список всеъ страниц связанных с текущим объектом по reffer и href
       if (sizeof($list_URL)) foreach($list_URL as $rec_URL)
       { //echo $rec_URL['url'].'<br>' ;
         if ($rec_URL['obj_reffer']==$rec_obj['_reffer'] and $rec_URL['url']==$obj_href)
         { $res_arr[]=$rec_URL ;
           $cnt++ ;
         }
       }
       //echo '<strong>cnt='.$cnt.'</strong><br>' ;
       // если ни один URL не связан, создаем запись в карте сайта и тоже добавляем в список
       if (!$cnt)
       { $reffer_URL=adding_rec_to_table(TM_SITEMAP,array('clss'=>36,'url'=>$obj_href,'obj_reffer'=>$rec_obj['_reffer']),array('return_reffer'=>1)) ;
         echo 'Добавлена новая запись для URL '.$obj_href.'<br>' ;
         $res_arr[]=select_db_ref_info($reffer_URL) ;
       }

       //  составляем список всеъ страниц связанных с текущим объектом только по reffer
       if (sizeof($list_URL)) foreach($list_URL as $rec_URL)
         if ($rec_URL['obj_reffer']==$rec_obj['_reffer'] and $rec_URL['url']!=$obj_href)
         { $rec_URL['obj_reffer']='' ;
           $res_arr[]=$rec_URL ;
         }

     }

    function set_redirect_301()
    {   echo '</form>' ;
        ?><h1>Задание 301 редиректов</h1>
           Вы можете задать URL списком, каждый URL должен начинаться с новой строки. Разделять URL запятой <strong>НЕ НАДО!</strong><br><br>
          <form method="post">
          <table>
              <tr>
                  <td>Откуда</td>
                  <td>Куда</td>
              </tr>
              <tr>
                  <td><textarea name="URL_from" cols="100" rows="10"></textarea></td>
                  <td><textarea name="URL_to" cols="100" rows="10"></textarea><br></td>
              </tr>
          </table>
          <input type="submit" cmd=set_redirect_301_by_list value="Задать редиректы"><br><br>
         </form>
        <?
        echo '<form>' ;
    }

    function set_redirect_301_by_list()
    {
      //damp_array($_POST) ;
      $arr_from=explode("\n",$_POST['URL_from']) ;
      $arr_to=explode("\n",$_POST['URL_to']) ;
      ?><h1>Задание 301 редиректов</h1><?
      if (sizeof($arr_from)) foreach($arr_from as $i=>$url_from) if (trim($url_from))
      { $url_from=trim(str_replace(array('http://','https://'),'',$url_from)) ;
        $url_to=trim($arr_to[$i]) ;
        $rec=execSQL_van('select * from '.TM_SITEMAP.' where url like "'.$url_from.'"') ;
        if ($rec['pkey']) { update_rec_in_table(TM_SITEMAP,array('r301to'=>$url_to,'status'=>301),'pkey='.$rec['pkey']) ;
                            echo $url_from.' => '.$url_to.' - <span class=green>OK</span><br>' ;
                          }
        else              { adding_rec_to_table(TM_SITEMAP,array('clss'=>36,'url'=>$url_from,'r301to'=>$url_to,'status'=>301)) ;
                            echo $url_from.' => '.$url_to.' - <span class=green>Добавлено</span><br>' ;
                          }
      }

      //damp_array($_POST['URL_to']) ;
    }

}

function get_url_of_obj($obj)
{
  $GLOBALS['urls_of_sections'][$obj->pkey]=array('_reffer'=>$obj->reffer,'__href'=>$obj->href,'__name'=>$obj->name) ;
}



?>