<?php
include_once(_DIR_TO_ENGINE."/admin/i_clss_func.php") ;
include_once(_DIR_TO_ENGINE.'/admin/c_menu_tabs.php') ;

// системные утилиты
class c_sys_util extends c_editor_obj
{
  function body($options=array()) { $this->body_frame_2x(array('fra_tree'=>_PATH_TO_ADMIN.'/sys_util_tree.php','title'=>$options['title'])) ; }
}


class c_sys_util_tree extends c_fra_tree
{

  function body($options=array())
  {?><body id=tree_objects>
      <ul class="tree_root" on_click_script="sys_util_viewer.php">
         <li clss=1>Служебные операции</li>
         <ul>
             <li clss=1>Информация</li>
                <ul>
                 <li cmd=get_phpinfo>Информация PHP</li>
                 <li cmd=get_image_support>Типы поддерживаемых типов изображений</li>
                 <li cmd=103>Настройки сервера</li>
                 <li cmd=104>Таблицы</li>
                 <li cmd=info_geoIP>GeoIP</li>
                </ul>
             <li clss=1>Диагностика движка</li>
                <ul>
                 <li cmd=201>Переменные сессии</li>
                 <li cmd=202>Выделение памяти сценарию</li>
                 <li cmd=203>Определения расхода ресурсов</li>
                 <li cmd=204>Диагностика создания модели классов</li>
                 <li cmd=show_all_clss>Объявленные классы</li>
                 <li cmd=206>Показать модель классов</li>
                 <li cmd=207>Показать модель объектных таблиц</li>
                 <li cmd=208>Показать системные объекты</li>
                 <li cmd=test_send_mail script="ext/mail">Проверка почты</li>
                 <li cmd=test_exec_send_stats>Проверка расчета минимальной статистики</li>
                 <li cmd=show_obj_clss_info script="ext/info">Показать объект класса</li>
                 <li cmd=show_obj_list_info script="ext/info">Показать объект списка</li>
                 <li cmd=show_session_list>Списки в сессии</li>
                </ul>
             <li clss=1>Обслуживание БД</li>
                <ul>
                 <li cmd=delete_alt_tables>Очистить базу от удаленных таблиц</li>
                 <li cmd=check_obj_table>Проверить все таблицы</li>
                 <li cmd=analiz_table>Анализ таблицы</li>
                 <li cmd=db_procedure script="ext/db/procedure.php">Процедуры БД</li>

                </ul>
             <li clss=1>Обновления</li>
                 <ul>
                  <li cmd=db_convert_win1251_to_utf8>Преобразовать в БД win1251 -> uft8</li>
                  <li cmd=move_metatags_to_sitemap>Скопировать метатеги в sitemap</li>
                  <li cmd=merge_log_events>Объединить журналы событий</li>
                  <li cmd=set_subscripton_status>Установить для всех email флаг "Рассылка"</li>
                  <li cmd=update_parent_to_clss_123>Перенести массивы свойств в свойства</li>
                  <li cmd=update_move_shiping_to_service>Оформить доставку в заказах как услугу</li>
                  <li cmd=create_account_by_order>Создать клиентов в личном кабинете по заказам</li>
                  <li cmd=create_links_to_obj_reffer>Создать линки для obj_reffer</li>
                 </ul>

             <li clss=1>Утилиты</li>
                <ul>
                    <li cmd=sitemap>Работа с sitemap</li>
                    <li cmd=check_clone  script="ext/image">Тест генерации клонов изображений</li>
                </ul>
             <li clss=1>Импорт</li>
                 <ul>
                     <li cmd=import_get_tables_fields_array>Получить массив с описанием таблиц</li>
                     <li cmd=import_from_drupal>Импорт Drupal</li>
                 </ul>
             <li clss=1>SETUP</li>
                <ul>
                <li cmd=info_db_tables>Информация по таблицам БД</li>
                <li cmd=create_based_tables>Создать базовые таблицы</li>
                <li cmd=install_modules script="ext/setup">Инсталлировать модули</li>
                </ul>
             <li clss=1>Управление журналами</li>
             <li clss=1>Проверки</li>
                 <ul>
                 <li cmd=test_upload_object_tree_items>upload_object_tree_items</li>
                 <li cmd=test_check_frames>Проверка доступа между фреймами</li>
                 <li cmd=test_safe_url_name>Проверка формирования url name</li>
                 <li cmd=test_translit>Проверка перевода в транслит</li>
                 <li cmd=test_checked_robots>Проверка распознования робота по UserAgent</li>
                 <li cmd=test_buttons>Проверка работы кнопок</li>
                 <li cmd=test_templates script="ext/test">Проверка работы шаблонов</li>
                 <li cmd=test_include_script_cmd script="ext/test">Проверка include_script_cmd</li>
                 </ul>

      </ul>
      </body>
     <?
      /*
	  object[1][300].add_child(2,302,"Очистить базу от удаленных таблиц") ;
	  object[1][300].add_child(2,304,"Оптимизация таблиц") ;
	  object[1][300].add_child(2,305,"Выполнить скрипт") ;
	  object[1][300].add_child(2,306,"Преобразовать CHARACTER SET таблицы") ;


      //object[1][400].add_child(2,402,"Проверить системные таблицы") ;
      object[1][400].add_child(2,403,"Импорт изображений из старых версий сайта") ;
      object[1][400].add_child(2,404,"Преобразование статуса заказа из parent") ;
      //object[1][400].add_child(2,405,"Удалить разовые UID из журнала ресурсов") ;
      object[1][400].add_child(2,406,"Преобразование журнала событий сайта в новый формат") ;
      object[1][400].add_child(2,407,"Преобразование журнала событий админки в новый формат") ;
      object[1][400].add_child(2,408,"Операции по обновлению таблиц") ;
      object[1][400].add_child(2,409,"Перенести информацию из индексных таблиц в таблицу списков") ;
      object[1][400].add_child(2,410,"Скопировать сообщения службы поддержки на форум") ;
      object[1][400].add_child(2,411,"Скопировать сообщения отзывов на форум") ;
      object[1][400].add_child(2,412,"Таблица отзывов v3.48 -> v4.37") ;
      object[1][400].add_child(2,413,"Преобразовать /pages/... в файлы") ;


	  object[1][500].add_child(2,501,"Импорт стилей сайта для редактора") ;
	  object[1][500].add_child(2,502,"Распределить клиентов в папки по алфавиту") ;
	  object[1][500].add_child(2,503,"Импорт старых заказов") ;
	  //object[1][500].add_child(2,505,"Синхронизация клонов изображений") ;
	  //object[1][500].add_child(2,506,"Анализ фото товарной таблицы") ;
	  //object[1][500].add_child(2,507,"Проверка изображений по именам в БД") ;

	  object[1][500].add_child(2,511,"Назначить права доступа (644) все фоткам товара") ;
	  //object[1][500].add_child(2,512,"Удалить клон 160") ;
	  object[1][500].add_child(2,513,"Проверить функцию выделения поисковых запросов") ;
	  object[1][500].add_child(2,514,"Преобразование старых HTML полей") ;
	  //object[1][500].add_child(2,515,"Удаление неиспользуемых изображений с диска") ;

	  object[1][600].add_child(2,'import_db_Joomla',"Импорт данных из Joomla") ;

	  //object[1][800].add_child(2,801,"Удалить статистику переходов") ;
	  //object[1][800].add_child(2,802,"Удалить статистику ресурсов") ;
	  //object[1][800].add_child(2,803,"Удалить статистику TOP страниц сайта") ;
	  //object[1][800].add_child(2,804,"Удалить статистику поисковых запросов") ;
	  //object[1][800].add_child(2,805,"Показать таблицу статистики") ;
	  //object[1][800].add_child(2,813,"Расчет статистики") ;
	  //object[1][800].add_child(2,814,"Удалить записи из журнала старее 2-х дней") ;
      //object[1][800].add_child(2,806,"Удалить устаревшие записи журнала входов сайта") ;
      //object[1][800].add_child(2,807,"Удалить устаревшие записи журнала входов админки") ;
      //object[1][800].add_child(2,808,"Удалить устаревшие записи журнала ресурсов сайта") ;
      //object[1][800].add_child(2,809,"Удалить устаревшие записи журнала ресурсов админки") ;
      //object[1][800].add_child(2,810,"Показать данные по датам индексации") ;
      //object[1][800].add_child(2,811,"Показать данные по партнерским переходам") ;
      //object[1][800].add_child(2,812,"Показать данные по витрынным переходам") ;
      object[1][800].add_child(2,815,"Показать данные по журналу страниц") ;


      object[1][900].add_child(2,905,"Проверка преобразования пути файла для загрузки") ;
      object[1][900].add_child(2,902,"Проверка правильного назначения прав на загружаемые файлы") ;
      object[1][900].add_child(2,'check_permission',"Проверка прав доступа к директориям и файлам сервера") ;
      object[1][900].add_child(2,903,"Проверка и исправление прав доступа к директория /public/") ;
      object[1][900].add_child(2,904,"Проверка и исправление недопустимых символов в именах файлов в БД") ;
      object[1][900].add_child(2,906,"Проверка defines_vers") ;




      object[1][900].add_child(2,'test_class_systems',"Проверка новой модели классов") ;
      object[1][900].add_child(2,'test_submit_form',"Проверка отправки формы") ;


	  */
  }


}

//**************************************************************************************************************************************
//**************************************************************************************************************************************
//**************************************************************************************************************************************
//**************************************************************************************************************************************

class c_sys_util_viewer extends c_fra_based
{

 function _block_main() // выводит список разделов
	 {
	   ?><form name="form" method="post" action="" ENCTYPE="multipart/form-data">
		  		<input name="cmd"  type="hidden" value=""   id='cmd' />
		  		<input name="func_dialog"  type="hidden" value=""   id='func_dialog' />
       <?

       // отрабатываем команду
	   $func_dialog='' ;   $cmd='' ;
       global $cmd,$pkey,$func_dialog ;

	   if ($cmd and method_exists($this,$cmd)) { $this->$cmd() ; unset($pkey) ; unset($cmd) ; }
	   if ($func_dialog and function_exists($func_dialog)) { $func_dialog() ; unset($pkey) ; }

	   switch ($pkey)
	    { case 103: $func_name='get_server_info' ; 				break ;  // Настройка сервера
	      case 104: $func_name='get_db_recs_info' ; 			break ;  // Настройка сервера

	      case 201: $func_name='get_session_vars' ; 			break ;  // Переменные сессии
	      case 202: $func_name='get_memory_info' ; 				break ;  // Выделение памяти сценарию
	      //case 203: $func_name='' ; break ;  // Определения расхода ресурсов
	      case 204: $func_name='check_create_class_model' ; 	break ;  // Диагностика создания модели классов
	      case 206: $func_name='show_descr_obj_clss' ; 			break ;  // Показать модель классов
	      case 207: $func_name='show_descr_obj_table' ; 		break ;  // Показать модель объектных таблиц
	      case 208: $func_name='show_sys_obj' ; 				break ;  // Показать системные объекты
	      case 210: $func_name='read_trace' ; 				    break ;  // читаем файл трассировки


	      //case 304: $func_name='' ; break ;  // Оптимизация таблиц
	      case 305: $func_name='execSQLscript' ; 				break ;  // Проверка таблиц
	      case 306: $func_name='convert_char_set' ; 			break ;  //


          case 400: $func_name='info_400' ; 					break ;  // Проставить типы таблиц в поле use_clss
	      //case 402: $func_name='check_system_tables' ; 			break ;  // Проверить наличие и создать отсутствующие системные таблицы
	      case 404: $func_name='convert_orders' ; 				break ;  // Проверить наличие и создать отсутствующие системные таблицы
	      //case 405: $func_name='delete_robots_uid' ; 			break ;  // удалить uid роботов из журнала ресурсов
	      case 406: $func_name='convert_events_site' ; 			break ;  //
          case 407: $func_name='convert_events_admin' ; 		break ;  //
          case 408: $func_name='update_tables' ; 				break ;  //
          case 409: $func_name='export_index_tables_to_van_table' ; break ;  //
          case 412: $func_name='convert_TM_responses_from_3_48_to_4_37' ; break ;  //
          case 413: $func_name='convert_pages_to_files' ; break ;  //

	      case 501: $func_name='import_site_css' ; 				break ;  // Импорт стилей сайта для редактора
	      case 502: $func_name='raspred_user_by_first_letter' ; break ;  // Распределить клиентов в папки по алфавиту
	      case 503: $func_name='import_alt_orders' ; 			break ;  // Импорт старых заказов
	      //case 505: $func_name='sync_clone' ;	 				break ;
	      case 506: $func_name='search_delete_images' ; 		break ;
	      case 507: $func_name='check_name_images' ; 			break ;
	      case 510: $func_name='check_clone' ; 					break ;
	      case 511: $func_name='set_644_access' ; 				break ;
	      case 512: $func_name='del_160_images' ; 				break ;
	      case 513: $func_name='test_func_1' ; 					break ;
	      case 514: $func_name='test_func_2' ; 					break ;
	      case 515: $func_name='delete_not_used_images' ; 		break ;

	      case 801: $func_name='delete_stats_reffers' ; 			break ;
	      case 802: $func_name='delete_stats_debug' ; 			break ;
	      case 803: $func_name='delete_stats_top_pages' ; 				break ;
	      case 804: $func_name='delete_stats_49' ; 				break ;
	      case 805: $func_name='show_table_stats' ; 				break ;
	      case 806: $func_name='clear_alt_log_logons_site' ; 				break ;
	      case 807: $func_name='clear_alt_log_logons_admin' ; 				break ;
	      case 808: $func_name='clear_alt_log_debug_site' ; 				break ;
	      case 809: $func_name='clear_alt_log_debug_admin' ; 				break ;
	      case 810: $func_name='show_ya_data' ; 							break ;
	      case 811: $func_name='show_part_info' ; 							break ;
	      case 812: $func_name='show_vitrina_info' ; 							break ;
	      case 813: $func_name='exec_generate_sum_stats' ; 							break ;
	      case 814: $func_name='exec_delete_alt_recs_from_log' ; 							break ;
	      case 815: $func_name='func_815' ; 							break ;


	      case 902: $func_name='_902_check_file_perms' ; 							break ;
          case 903: $func_name='_903_change_dir_perms' ; 							break ;
          case 904: $func_name='_0904_change_file_names' ; 							break ;
          case 905: $func_name='_0905_check_safe_patch_files' ; 					break ;
          case 906: $func_name='_0906_check_defines_vers' ; 					break ;
          default: $func_name=$pkey ;
	    }
       if ($func_name and file_exists(_DIR_TO_ENGINE.'/extensions/'.$func_name.'.php'))
       { include(_DIR_TO_ENGINE.'/extensions/'.$func_name.'.php') ;
         if ($cmd and function_exists($cmd)) { $func_name=$cmd() ; $cmd='' ; }
       }
       if ($cmd and function_exists($cmd)) $func_name=$cmd() ;
       if ($func_name and method_exists($this,$func_name))  $this->$func_name() ;
       else if ($func_name and function_exists($func_name))      $func_name() ;
       else if ($func_name) echo 'Функция <strong>'.$func_name.'</strong> не найдена.' ;


	   ?></form><?
	 }


 function test_exec_send_stats()
 { ?><h1>Проверка расчета минимальной статистики</h1><?
   include_once(_DIR_TO_ENGINE.'/admin/i_stats.php') ;
   exec_send_site_stats_informations(1) ;

 }




 //
function read_trace()
{ $files=get_dir_files_list(_DIR_TO_ROOT.'/trace/') ;
  //damp_array($files) ;
  ?><select name=track_file><option></option><?
  if (sizeof($files['files'])) foreach($files['files'] as $fname) {?><option value="/trace/<?echo $fname?>">/trace/<?echo $fname?></option><?}
  ?></select><?
  ?><button onclick="exe_cmd('read_trace_exec')">Прочитать</button><br><?
}


function read_trace_exec()
{ global $track_file ;
    ?><style type="text/css">
                   table{table-layout:auto;border-collapse:collapse;}
                   table td{padding:4px;border-bottom:1px solid #CDCDCD;}
                   table tr.level_1 td{padding-left:10px;}
                   table tr.level_2 td{padding-left:20px;}
                   table tr.level_3 td{padding-left:30px;}
                   table tr.level_4 td{padding-left:40px;}
                   table tr.level_5 td{padding-left:50px;}
                   table tr.level_6 td{padding-left:60px;}
                   table tr.level_7 td{padding-left:60px;}
                   table tr.level_8 td{padding-left:80px;}
                   table tr.level_9 td{padding-left:90px;}
                   table tr.level_10 td{padding-left:100px;}
                   table tr.level_11 td{padding-left:110px;}
                   table tr.level_12 td{padding-left:120px;}
                   .hidden{display:none;}
         </style>
       <?
       set_time_limit(0) ;
       $cont=file(_DIR_TO_ROOT.$track_file) ;
       $last_time=0 ;
       ?><h1><?echo $track_file?></h1><?
       echo 'в TRACE '.sizeof($cont).' строк<br><br>' ;
       $res_arr=array() ;
       for ($i=1;$i<=sizeof($cont)-4;$i++)
       { //echo $cont[$i].'<br>' ;
         $arr=explode(" ",$cont[$i]) ;
         $arr2=array() ;
         $k=0 ; // номер элемента
         $s=0 ; // число пробелов
         foreach($arr as $j=>$value)
         { if (!$value) $s++ ;
           else {
                  if ($k==0) { $arr2['st']=$value ; $arr2['dt']=round($value-$last_time,4) ; $arr2['dlit']='' ; $last_time=$value ; }
                  if ($k==1)   $arr2['t_code']=$value ;
                  if ($k==2)  $arr2['t_corr']=$value ;
                  if ($k==3) $arr2['level']=$s/2-1 ;
                  if ($k==4) $arr2['func']=str_replace('/home/roman/data/www/watch.clss.ru','',$value) ;
                  if ($k==5) $arr2['script']=str_replace('/home/roman/data/www/watch.clss.ru','',$value) ;
                  $k++ ;
                  $s=0 ;
                }

         }
         $res_arr[]=$arr2 ;
         //print_r($arr2) ; echo '<br>' ; return ;
       }
       // рассчитываем времена выполнения по уровням
       $summ_arr=array() ;
       $last_level=0 ;
       foreach($res_arr as $i=>$rec)
       { if ($last_level>$rec['level'])
         {  //echo '<strong>Возврат на предыдущий уровень</strong><br>' ;
            //echo '>>>с уровня '.$last_level.' ==> '.$rec['level'].'<br>' ;
            //echo '>>>Вычисляем значения для уровней от j='.($last_level-1).' до j<='.($rec['level']).'<br>' ;
            for($j=$last_level-1;$j>=$rec['level'];$j--)
            {   //echo '>>> id level cur ='.$i.'<br>' ;
                //echo '>>> id level prev='.$summ_arr[$j].'<br>' ;
                //echo '>>> t_code cur ='.$rec['st'].'<br>'    ;
                //echo '>>> t_code prev='.$res_arr[$summ_arr[$j]]['st'].'<br>'    ;
                //echo '>>> delta      ='.($rec['st']-$res_arr[$summ_arr[$j]]['st']).'<br>'    ;

                $res_arr[$summ_arr[$j]]['dlit']=round($rec['st']-$res_arr[$summ_arr[$j]]['st'],4) ;
            }

         }
         $summ_arr[$rec['level']]=$i ;
         $last_level=$rec['level'] ;
       }
       //damp_array($summ_arr,1,-1);
       if ($last_level>1) for($j=$last_level-1;$j>=1;$j--)
       { //echo '>>> id level prev='.$summ_arr[$j].'<br>' ;
         //echo '>>> t_code cur ='.$rec['st'].'<br>'    ;
         //echo '>>> t_code prev='.$res_arr[$summ_arr[$j]]['st'].'<br>'    ;
         //echo '>>> delta      ='.($rec['st']-$res_arr[$summ_arr[$j]]['st']).'<br>'    ;
         $res_arr[$summ_arr[$j]]['dlit']=round($rec['st']-$res_arr[$summ_arr[$j]]['st'],4) ;

       }
       // делаем вывод, скрывая вложенные уровни
       $summ_arr=array() ;
       ?><table id=trace><?
       foreach($res_arr as $i=>$rec)
       { $parent_id=$summ_arr[$rec['level']-1] ;
         ?><tr class="<?if (isset($parent_id)) echo 'parent_'.$parent_id?> <?if ($rec['level']>1) echo 'hidden'?> level_<?echo $rec['level']?>" id="parent_<?echo $i?>">
           <?/*<td><?echo $i?></td>
               <td><?echo $rec['st']?></td>
               <td><?echo $rec['t_code']?></td>
               <td><?echo $rec['t_corr']?></td>
               <td class=level_<?echo $rec['level']?>><?echo $rec['level']?></td>*/?>
               <td><?echo $rec['dlit']?></td>
               <?/*<td><?echo $rec['dt']?></td>*/?>
               <td><?echo $rec['script']?></td>
               <td><?echo $rec['func']?></td>
               <? if (!$rec['dlit'] and $rec['dt']>0) echo '<td>'.$rec['dt'].'</td>'?>
          <tr><?
         $summ_arr[$rec['level']]=$i ;

       }

       ?></table>
         <script type="text/javascript">
             $j('table#trace tr').click(function()
              { //alert(1) ;
                $j('tr.'+$j(this).attr('id')).toggleClass('hidden')
              });
         </script>

        <?
}


  // преобразовываем страницы из базы данных в скрипты со ссылкой на страницы
  function convert_pages_to_files()
  { $pages=execSQL('select pkey,obj_name,href from '.TM_PAGES.' where clss=26') ;
    //damp_array($pages) ;
    $dir='/' ;
    $ini_patch=($dir=='/')? '':'../' ;
    if (sizeof($pages)) foreach($pages as $rec)
    {   $fname=($rec['href'])? $rec['href'].'.html':'page_'.$rec['pkey'].'.html' ;
        $cont="<? // создано системой автоматического создания шаблонов страниц ".get_month_year(0,'day month year time')."\ninclude ('".$ini_patch."ini/patch.php')  ;\ninclude (_DIR_TO_ENGINE.'/i_system.php') ;\n\$options=array();\nshow_page(\$options) ;\n?>" ;
        $fdir=_DIR_TO_ROOT.$dir.$fname ;
        $fpatch=_PATH_TO_SITE.$dir.$fname ;
        //if (file_exists($fdir)) { echo '<div class=alert>Файл '.$fpatch.' уже существует. Удалите файл самостоятельно либо выберите другое имя файла</div>' ; return ; }
        $cnt=file_put_contents($fdir,$cont) ;
        echo ($cnt)? 'Создан скрипт '.$dir.$fname.'<br>':'<div class=red>Создан скрипт '.$dir.$fname.'</div><br>' ;
        execSQL_update('update '.TM_PAGES.' set href="'.$dir.$fname.'" where pkey='.$rec['pkey']) ;
    }
  }

 /*таблица отзывов - смена названий полей*/
 function convert_TM_responses_from_3_48_to_4_37()
 { ?><h1>Таблица отзывов - новый формат (для версии 4.37)</h1>
    1. Поле "member" копируется в поле "account_reffer"<br>
 	2. Поле "obj_ref" копируется в поле "obj_reffer"<br>
 	3. Поле "obj_ref_name" копируется в поле "obj_reffer_name"<br>
 	4. Поле "obj_name" копируется в поле "autor_name"<br>
 	5. Поле "obj_name" = "Отзыв № 222"<br>
    <button onclick="exe_cmd('convert_TM_responses_from_3_48_to_4_37_exec')">Преобразовать</button><br>
   <?
 }

 function convert_TM_responses_from_3_48_to_4_37_exec()
 {  $sql='update '.$_SESSION['TM_responses'].' set account_reffer=member where account_reffer="" or account_reffer is null' ;
    echo $sql.'<br>' ;
    if (!$result = mysql_query($sql)) SQL_error_message($sql,'',1) ;
    $sql='update '.$_SESSION['TM_responses'].' set obj_reffer=obj_ref where obj_reffer="" or obj_reffer is null' ;
    echo $sql.'<br>' ;
    if (!$result = mysql_query($sql)) SQL_error_message($sql,'',1) ;
    $sql='update '.$_SESSION['TM_responses'].' set obj_reffer_name=obj_ref_name where obj_reffer_name="" or obj_reffer_name is null' ;
    echo $sql.'<br>' ;
    if (!$result = mysql_query($sql)) SQL_error_message($sql,'',1) ;
    $sql='update '.$_SESSION['TM_responses'].' set autor_name=obj_name where autor_name="" or autor_name is null' ;
    echo $sql.'<br>' ;
    if (!$result = mysql_query($sql)) SQL_error_message($sql,'',1) ;
 }

 function update_tables()
 {?><h1>Таблица заказов - новый формат (с версии 4.14)</h1>
    <button onclick="exe_cmd('update_tables_1_view_1')">Просмотр таблицы журнала событий сайта</button><br><br>
 	1. Поле "member" копируется в поле "member_reffer"<br>
 	2. Поле "partner_reffer" копируется в поле "referer_host"<br>
 	3. Поле "to_member" копируется в поле "name"<br>
 	3. Поле "to_phone" копируется в поле "phone"<br>
 	3. Поле "to_contact" копируется в поле "email"<br>


 	Поля с названиями  member и partner_reffer более не поддерживаются в целях стандартизации названий.<br><br>
 	<button onclick="exe_cmd('update_tables_1')">Преобразовать</button><br>

 	<h1>Журнал событий сайта - новый формат (с версии 4.14)</h1>
    <button onclick="exe_cmd('update_tables_2_view_1')">Просмотр таблицы журнала событий сайта</button><br><br>
 	1. События прописыватся не кодами, а текстом: в поле "obj_name" сохраняется значение $list_events_type[type]  <button onclick="exe_cmd('update_tables_2_1')">Выполнить</button> <br>
 	2. Поле "reffer" копируется в поле "member" <button onclick="exe_cmd('update_tables_2_2')">Выполнить</button><br>
 	3. Удаляем записи по отправке e-mail (type 9) <button onclick="exe_cmd('update_tables_2_3')">Выполнить</button><br>

 	<br>
 	<h1>Журнал событий админки - новый формат (с версии 4.14)</h1>
    <button onclick="exe_cmd('update_tables_3_view_1')">Просмотр таблицы журнала событий сайта</button><br><br>
 	1. События прописыватся не кодами, а текстом: в поле "obj_name" сохраняется значение $list_events_type[type]  <button onclick="exe_cmd('update_tables_3_1')">Выполнить</button> <br>
 	2. Поле "init" копируется в поле "member" <button onclick="exe_cmd('update_tables_3_2')">Выполнить</button><br>
 	3. Удаляем записи по отправке e-mail (type 9) <button onclick="exe_cmd('update_tables_3_3')">Выполнить</button><br>

 	<?
 }

 function update_tables_1()
 { global $TM_orders ;
   ?><h1>Таблица заказов - новый формат (с версии 4.14)</h1><?
   $sql='update '.$TM_orders.' set account_reffer=member where account_reffer="" or account_reffer is null' ;
   if (!$result = mysql_query($sql)) SQL_error_message($sql,'',1) ;
   $sql='update '.$TM_orders.' set referer_host=partner_reffer where referer_host="" or referer_host is null' ;
   if (!$result = mysql_query($sql)) SQL_error_message($sql,'',1) ;
   $sql='update '.$TM_orders.' set name=to_member where name="" or name is null' ;
   if (!$result = mysql_query($sql)) SQL_error_message($sql,'',1) ;
   $sql='update '.$TM_orders.' set phone=to_phone where phone="" or phone is null' ;
   if (!$result = mysql_query($sql)) SQL_error_message($sql,'',1) ;
   $sql='update '.$TM_orders.' set email=to_contact where email="" or email is null' ;
   if (!$result = mysql_query($sql)) SQL_error_message($sql,'',1) ;
   $sql='update '.$TM_orders.' set adres=to_adress where adres="" or adres is null' ;
   if (!$result = mysql_query($sql)) SQL_error_message($sql,'',1) ;
   $sql='update '.$TM_orders.' set rekvezit=info where rekvezit="" or rekvezit is null' ;
   if (!$result = mysql_query($sql)) SQL_error_message($sql,'',1) ;


   $list=execSQL('select * from '.$TM_orders.' order by rand() limit 100 ',2) ;
 }

 function update_tables_1_view_1()
 { global $TM_orders ;
   $list=execSQL('select * from '.$TM_orders.' where member!="" order by c_data desc limit 100 ',2) ;
 }

 function update_tables_2_view_1()
 { $list=execSQL('select * from '.TM_LOG_EVENTS.' order by c_data desc limit 100 ',2) ;
 }

 function update_tables_3_view_1()
 { $list=execSQL('select * from '.TM_LOG_EVENTS.' order by c_data desc limit 100 ',2) ;
 }


 function update_tables_2_2()
 { $sql='update '.TM_LOG_EVENTS.' set init=reffer where reffer=""' ;
   if (!$result = mysql_query($sql)) SQL_error_message($sql,'',1) ;
   $list=execSQL('select * from '.TM_LOG_EVENTS.' order by rand() limit 100 ',2) ;
 }

 function update_tables_3_2()
 { $sql='update '.TM_LOG_EVENTS.' set reffer=init where reffer=""' ;
   if (!$result = mysql_query($sql)) SQL_error_message($sql,'',1) ;
   $list=execSQL('select * from '.TM_LOG_EVENTS.' order by rand() limit 100 ',2) ;
 }


 function update_tables_2_3()
 { $sql='delete from '.TM_LOG_EVENTS.' where type=9' ;
   if (!$result = mysql_query($sql)) SQL_error_message($sql,'',1) ;
   $list=execSQL('select * from '.TM_LOG_EVENTS.' order by rand() limit 100 ',2) ;
 }



 function update_tables_3_3()
 { $sql='delete from '.TM_LOG_EVENTS.' where type=9' ;
   if (!$result = mysql_query($sql)) SQL_error_message($sql,'',1) ;
   $list=execSQL('select * from '.TM_LOG_EVENTS.' order by rand() limit 100 ',2) ;
 }




  function convert_events_admin()
 { $this->convert_events(TM_LOG_EVENTS) ;
 }

 function convert_events($table_name)
 { ?><h1>Преобразование журналов в новый вид</h1><?

   //$sql='update '.$table_name.' set member=init' ;
   //if (!$result = mysql_query($sql)) SQL_error_message($sql,'',1) ;

   $sql='update '.$table_name.' set links=member where (reffer="" or reffer is null)' ;
   echo $sql.'<br>' ;
   if (!$result = mysql_query($sql)) SQL_error_message($sql,'',1) ;

   $sql='update '.$table_name.' set links=CONCAT(member,",",reffer) where reffer!=""' ;
   echo $sql.'<br>' ;
   if (!$result = mysql_query($sql)) SQL_error_message($sql,'',1) ;

   $list_events_type=array('1'=>'Создание заказа',
    						'2'=>'Изменение статуса заказа',
    						'3'=>'Создание проводки',
    						'4'=>'Добавление комментария к заказу',
    						'5'=>'Редактирование заказа',
    						'6'=>'Оплата заказа',
    						'7'=>'Добавление товара в заказ',
    						'8'=>'Удаление товара из заказа',
    						'9'=>'Отправка e-mail',
    						'10'=>'Автоочистка журнала входов',
    						'11'=>'Автоочистка журнала ресурсов',
    						'12'=>'Создание в БД таблицы',
    						'13'=>'Не удалось отправить e-mail',
    						'14'=>'Ввод ошибочного пароля',
    						'15'=>'Создание отзыва',
    						//'16'=>'',
    						'17'=>'Авторизация на сайте через суппорт',
    						'18'=>'Авторизация на сайте через дисконтную карту',
    						'19'=>'Авторизация в админе',
    						'20'=>'Авторизация на сайте через админ',
    						'21'=>'Создание таблицы',
    						'22'=>'Удаление таблицы',
    						'23'=>'Создание объекта',
    						'24'=>'Удаление объекта',
    						'25'=>'Открытие существующей темы поддержки',
    						'26'=>'Создание темы поддержки',
    						'27'=>'Добавление нового сообщения в тему',
    						'28'=>'Создание нового аккаунта',
    						'29'=>'Создание нового аккаунта через поддержку',
    						'30'=>'Генерация пароля на аккаунт',
    						'31'=>'Открытие темы поддержки для ответа',
    						'32'=>'Добавление ответа в тему поддержки',
    						'33'=>'Списание бонусов для партнера сайта',
    						'35'=>'Удаление заказа из БД'
    					)  ;

   if (sizeof($list_events_type)) foreach($list_events_type as $id=>$env_name)
    { $sql='update '.$table_name.' set obj_name="'.$env_name.'" where type='.$id ;
   	  echo $sql.'<br>' ;
   	  if (!$result = mysql_query($sql)) SQL_error_message($sql,'',1) ;
    }

   $list=execSQL('select * from '.$table_name.' limit 100 ',2) ;
 }

 // собираем данных из индексных таблиц в одну таблицу TM_list
 function export_index_tables_to_van_table()
 { ?><h1>Cобираем данных из индексных таблиц в одну таблицу списков</h1>
   <p>Начиная с версии 4.31 индексные таблицы более не поддерживаются. Вместе них введена единая таблица - obj_site_list</p>
   <p>Утилита перенесет данные из существующих индексных таблиц в новую таблицу</p>
   <p>Внимание! Если имеются расщиренные в init.php классы индексных записей, проверьте что obj_site_list поддерживает аналогичные классы - иначе отсутствующие поля будут потеряны.
      Аналогично проверьте поддержку мультиязычности.
   </p>
   <br>
   <input type="hidden" value=1 name=export_index_tables_to_van_table>
   <button onclick="exe_cmd('')">Выполнить</button><br><br>
   <?
   global $export_index_tables_to_van_table ; if (!$export_index_tables_to_van_table) return ;
   $list_index_tables=execSQL('select * from obj_descr_obj_tables where clss=106') ;
   if (sizeof($list_index_tables)) foreach($list_index_tables as $table_info)
   { // создаем новый список
     $list_id=adding_rec_to_table(TM_LIST,array('parent'=>1,'clss'=>21,'obj_name'=>$table_info['obj_name'],'enabled'=>1)) ;
     echo 'Создан список: <strong>'.$table_info['obj_name'].'</strong><br>' ;
     $list_values=execSQL('select * from '.$table_info['table_name'].' order by pkey') ;
     if (sizeof($list_values)) foreach($list_values as $rec)
     { $rec['id']=$rec['pkey'] ;
       $rec['parent']=$list_id ;
       $rec['clss']=20 ;
       if (!isset($rec['enabled'])) $rec['enabled']=1 ;
       if (!$rec['obj_name'] and $rec['value']) $rec['obj_name']=$rec['value'] ;
       unset($rec['pkey']) ;
       adding_rec_to_table(TM_LIST,$rec) ;
       echo 'Добавлена запись "'.$rec['obj_name'].'"<br>' ;
     }


   } else { echo '<div class=alert>Индексных таблиц не обнаружено</div>' ; return ; }



 }

/*************************************************************************************************************************************/
/*************************************************************************************************************************************/
/*************************************************************************************************************************************/
 function show_ya_data()
 { ?><h1>Каталог товаров</h1><?
   $list=execSQL('select pkey,clss,obj_name,c_data,r_data,ya_data from obj_site_goods where not ya_data is null') ;
   damp_array($list) ;

   $list=execSQL('select pkey,clss,obj_name,c_data,r_data,ya_data from obj_site_news where not ya_data is null') ;
   damp_array($list) ;

   $list=execSQL('select pkey,clss,obj_name,c_data,r_data,ya_data from obj_site_artikle where not ya_data is null') ;
   damp_array($list) ;


 }

 function show_part_info()
 { ?><h1>Партнерские переходы по сайту</h1><?
   $list=execSQL('select * from log_eu_pages where not partner_uid is null or partner_uid=""',2) ;
 }

 function show_vitrina_info()
 { ?><h1>Витринные переходы по сайту</h1><?
   $list=execSQL('select * from log_eu_pages where url like "%vitrina.php%"',2) ;
   $sql='delete from log_eu_pages where url like "%vitrina.php%"' ;
   //if (!$result = mysql_query($sql)) SQL_error_message($sql,'',1) ; //else echo 'Результат: '.$result.'<br>' ;

 }



 function test_func_2()
 {
 	global $TM_goods ;
 	$info=execSQL('select * from '.$TM_goods.' where manual!=""') ;
 	//echo $info['manual'] ;
 	if (sizeof($info)) foreach($info as $pkey=>$rec)
 	{ $new_manual=addslashes(htmlspecialchars_decode($rec['manual'])) ;
 	  echo 'Обработано: #'.$pkey.' - '.$rec['obj_name'].'<br>' ;
      $sql='update '.$TM_goods.' set manual="'.$new_manual.'" where pkey='.$pkey ;
      if (!$result = mysql_query($sql)) SQL_error_message($sql,'',1) ; //else echo 'Результат: '.$result.'<br>' ;
    }
 }






 function test_func_1()
 { global $TM_site_logons ;
   $list=execSQL('select * from '.$TM_site_logons.' order by c_data desc limit 100') ;
   //damp_array($list) ;
   ?><table><?
   if (sizeof($list)) foreach($list as $rec)
    { $word=$this->check_searchengine($rec['referer']) ;
      ?><tr><td class=left><?echo $rec['referer']?></td><td class=left><?echo $word?></td></tr><?

    }
   ?></table><?

 }



	//Проверка запроса поисковика
	function check_searchengine($test='')
	{   $referer=($test)? $test:$_SERVER['HTTP_REFERER'] ;
    	if($referer)
    	{   $from = urldecode( $referer );
			if (strlen($from) > 60) { $fromurl = mb_substr($from, 0, 60)."..."; } else {$fromurl = $from; }
			$fromurl = str_replace('http://', '', $fromurl);
			$fromurl = str_replace('www.', '', $fromurl);
			$se = '';
			$start = 0;
			$filter = '';
			$word = get_se_query($from, $se, $start, $filter);
		    /*
		    if (!empty($se) && !empty($word)) {
	    		$query = "SELECT * FROM tagcloud_word WHERE word = '$word' LIMIT 1;";
		    	$result = $this->dbc->query($query);
		    	if( $result && $result->num_rows()>0 ){
		    		$row = $result->fetch_array(patDBC_TYPEASSOC);
		    		$tag_id = $row['tag_id'];
		    		return $tag_id;
		    	}
		    }
		    */
		    return($word) ;
		}
		return false;
	}

 function exec_generate_sum_stats()
 {

     include_once(_DIR_TO_MODULES.'/stats/m_stats_admin.php') ;
     generate_sum_stats(1) ;
 }

 function exec_delete_alt_recs_from_log()
 { include_once(_DIR_TO_MODULES.'/stats/m_stats_admin.php') ;
   $logs_recs=execSQL_van('select min(c_data)  as min_data,max(c_data) as max_data from '.TM_LOG_PAGES) ; // первая и последняя запись в журнале
   //list($data_from,$data_last)=get_time_logs_intervals(TM_LOG_PAGES,48) ;
   echo 'Первая запись в журнале '.date('d.m.y H:i:s',$logs_recs['min_data']).'<br>' ;
   echo 'Последняя запись в журнале '.date('d.m.y H:i:s',$logs_recs['max_data']).'<br>' ;
   $now=time() ;
   $start_delete	= mktime(0, 0, 0, date("m",$now), date("d",$now)-1, date("Y",$now)); // время начала вчерашнего дня
   echo 'Удаляем все записи от '.date('d.m.y H:i:s',$start_delete).'<br>' ;
   $count_rec=execSQL_van('select count(pkey) as cnt from '.TM_LOG_PAGES) ;
   echo 'Записей в журнале до очистки: '.$count_rec['cnt'].'<br>' ;
   execSQL('delete from '.TM_LOG_PAGES.' where c_data<'.$start_delete) ;
   $count_rec=execSQL_van('select count(pkey) as cnt from '.TM_LOG_PAGES) ;
   echo 'Записей в журнале после очистки: '.$count_rec['cnt'].'<br>' ;


 }


 function clear_alt_log_logons_site()
 {  include_once(_DIR_TO_ENGINE.'/admin/i_stats.php') ;
 	clear_alt_log_logons(1) ;
 }

 function clear_alt_log_debug_site()
 {  include_once(_DIR_TO_ENGINE.'/admin/i_stats.php') ;
 	clear_alt_log_debug(1) ;
 }


 function show_table_stats()
 { global $TM_stats ;
   $list=execSQL('select * from '.$TM_stats.' order by c_data',2)	 ;
 }

 function delete_stats_49()
 {	include_once(_DIR_TO_ENGINE.'/admin/i_stats.php') ;
 	erase_stats(49) ;
 }

 function delete_stats_reffers()
 {	include_once(_DIR_TO_ENGINE.'/admin/i_stats.php') ;
 	erase_stats(48) ;
 }

 function delete_stats_debug()
 {	include_once(_DIR_TO_ENGINE.'/admin/i_stats.php') ;
 	erase_stats(47) ;
 }

 function delete_stats_top_pages()
 {	include_once(_DIR_TO_ENGINE.'/admin/i_stats.php') ;
 	erase_stats(46) ;
 }


 function convert_char_set()
 {

    $field_info=execSQL("SHOW CREATE TABLE res_2008_05_04_09_46_53__obj_smw_info") ;
    damp_array($field_info) ;
    $list=execSQL('select * from res_2008_05_04_09_46_53__obj_smw_info order by c_data desc') ;
    damp_array($list) ;


 }

 function convert_events_site()
 { $this->convert_events(TM_LOG_EVENTS) ;
 }



 function convert_orders()
 { ?><h1>Конвертация parent в статус заказа</h1><?
   global $TM_orders ;
   $list_parent_status=execSQL('select distinct(t1.parent),t2.obj_name from '.$TM_orders.' t1,'.$TM_orders.' t2 where t1.parent>0 and t2.pkey=t1.parent') ;
   ?><table><tr class=clss_header><td>Старое состояние</td><td>Новое состояние</td></tr><?
   if (sizeof($list_parent_status)) foreach($list_parent_status as $i=>$parent_info)
   { ?><tr><td class=left><? echo $parent_info['obj_name'].' (parent='.$parent_info['parent'].')' ; ?></td>
           <td class=left><input name="conv_status[<?echo $i?>][alt]" type="hidden" value="<?echo $parent_info['parent']?>">
            <select size="1" name="conv_status[<?echo $i?>][new]">
              <? if (sizeof($_SESSION['list_status_orders'])) foreach($_SESSION['list_status_orders'] as $status=>$name) {?><option value="<?echo $status?>"><?echo $name?></option><?}?>
            </select>
           </td>
       </tr>
     <?
   }
   ?></table>
     <button onclick="exe_cmd('convert_orders_step2')">Конвертировать</button>
   <?
 }

 function convert_orders_step2()
 { global $conv_status,$TM_orders ;
   //damp_array($conv_status) ;
   $sql='update '.$TM_orders.' set status=0 where status is null' ;
   echo $sql.'<br>' ;
   if (!$result = mysql_query($sql)) SQL_error_message($sql,'',1) ; else echo 'Результат: '.$result.'<br>' ;

   if (sizeof($conv_status)) foreach($conv_status as $rec)
   { $sql='update '.$TM_orders.' set status='.$rec['new'].' where parent='.$rec['alt'] ;
     echo $sql.'<br>' ;
     if (!$result = mysql_query($sql)) SQL_error_message($sql,'',1) ; else echo 'Результат: '.$result.'<br>' ;
   }



 }


 function set_644_access()
 { global $TM_goods,$TM_goods_image ;
   include_once('i_img_working.php');
   $tkey=$_SESSION['pkey_by_table'][$TM_goods] ;
   $tkey_image=$_SESSION['pkey_by_table'][$TM_goods_image] ;

   $dir=_DOT($tkey_image)->dir_to_file ;
   $patch=_DOT($tkey_image)->patch_to_file ;

   $dir_list_images=get_list_img_for_dir($dir.'source/') ;
   //damp_array($dir_list_images) ;
   if (sizeof($dir_list_images)) foreach($dir_list_images as $fname)
   { $acc=mb_substr(sprintf('%o',fileperms($dir.'source/'.$fname)), -4) ;
     echo $fname.' - '.$acc ;
     if ($acc!='0777')
     { if (chmod($dir.'/'.$fname,0644)) echo ' - атрибуты изменены' ;
       else echo ' - не удалось изменить атрибуты' ;
     }

     echo '<br>' ;


   }

 }





// ИНФОРМАЦИЯ

 function info_400()
 { ?>
    <h1>Импорт изображений из старых версий движка</h1>
    В старых версиях движка таблицы с дочерними объектами изображение не было, а фото просто находились в папке изобрадений,
    и именовались по коду товара.<br>
    Утилита создает дочерние объекты "фото" в соответствии с фото, найденными в папке /source подкаталога заданного в свойствах
    таблицы, как каталог изображений, и переименовает фото в соотвествии со стандартом


   <?
 }

 //-----------------------------------------------------------------------------------------------------------------------------
 //-----------------------------------------------------------------------------------------------------------------------------
 //-----------------------------------------------------------------------------------------------------------------------------
 // служебные операции с таблицами
 //-----------------------------------------------------------------------------------------------------------------------------
 //-----------------------------------------------------------------------------------------------------------------------------
 //-----------------------------------------------------------------------------------------------------------------------------



 function check_table($table_name,$status=0)
 {  if ($status)
 	  { if ($_SESSION['pkey_by_table'][$table_name]) echo 'Таблица <strong>"'.$table_name.'"</strong> более не используется. <br>' ;
 	  }
 	else
 	  {
 	  	if (!$_SESSION['pkey_by_table'][$table_name]) echo 'Таблица <strong>"'.$table_name.'"</strong> не найдена.<br>' ;
 	  }

 }


function execSQLscript()
 { global $SQLscript ;
   ?><textarea name="SQLscript" rows=10 cols=200 wrap="off"><?echo $SQLscript?></textarea><br>
 	 <button onclick="exe_cmd('runSQLscript')">Выполнить скрипт</button>
   <?
 }

 function runSQLscript()
 { global $SQLscript ;
   echo $SQLscript.'<br><br>' ;

   $list=execSQL($SQLscript) ;
   print_array($list,$SQLscript) ;
   //if (!$result = mysql_query($SQLscript)) SQL_error_message($SQLscript,'',1) ; else echo 'Результат: '.$result.'<br>' ;
   $this->execSQLscript() ;
   //update obj_smw_goods G set G.meh_info=(select TM.value from indx_smw_tm TM where TM.pkey=G.meh) where G.meh>2 and G.clss=8
 }

 function show_sys_obj() // Показать системные объекты
 { ?><h1>Объект регистрации событий events</h1><?
   damp_array($_SESSION['events_system']) ;
   ?><h1>Объект - буфер обмена</h1><?
   damp_array($_SESSION['cart_admin']) ;
   ?><h1>Объект - текущий система авторизации</h1><?
   damp_array($_SESSION['admin_account_system']) ;
   ?><h1>Таблица кодов ОТ</h1><?
   damp_array($_SESSION['pkey_by_table'],3) ;
 }



 function get_memory_info()
 {
 	echo 'Выделяемая память под скрипт: '.round(memory_get_usage()/1024).' Кб<br>';
 	echo 'Максимально выделяемая память под скрипт: '.round(memory_get_peak_usage(true)/1024).' Кб<br>';
 	echo 'Расход ресурсов:' ;
 	//damp_array(getrusage(1));
 	//ini_set('session.gc_divisor',100) ;
 	//
 	global $reboot_setting,$descr_obj_vars ;
 	echo '$reboot_setting='.$reboot_setting.'<br>' ;

 	echo 'Размер _SESSION:'.sizeof($_SESSION).'<br>' ;
 	echo 'Размер descr_obj_vars:'.sizeof($descr_obj_vars).'<br>' ;
 	echo 'Размер descr_obj_tables:'.sizeof($_SESSION['descr_obj_tables']).'<br>' ;
 	damp_array($_SESSION) ;
 	//ini_set('apache.child_terminate',1) ;
    //damp_array(ini_get_all()) ;
 	//apache_child_terminate() ;



 }


 function get_session_vars()
 {  damp_array($_SESSION) ; }

 function test_create_model_clss()
 {
 	create_class_model(1) ;
 }


 // удаляем таблицы с префиксом "delete_"
 function delete_alt_tables()
  {
 	?><h1>Окончательно удаляем удаленные таблицы</h1><?


  }

 // проверка объектной таблицы на наличие всех необходимых полей согласно полей класса
 // $clss_info - массив информации по предполагаемым классам объектам
 // формат array ( код класса=pkey класса, .... )
 function check_obj_table()
  { $cnt_sql_cmd=0 ;
    include_extension('table/table_checked') ;
    ?><h1>Проверка структуры объектных таблиц</h1><?
    ?><table class=table_checked><?
   	if (sizeof($_SESSION['descr_obj_tables'])) foreach ($_SESSION['descr_obj_tables'] as $obj) $cnt_sql_cmd+=table_checked($obj->pkey,array('no_update_descr_tables'=>1,'no_show_button'=>1,'show_in_outer_table'=>1)) ;
    if ($cnt_sql_cmd) {?><tr><td colspan=2><button class=button cmd=exec_sql_list_cmd mode=dialog>Выполнить выбранные операции</button></td><?}
   	?></table><?
  }




 //-----------------------------------------------------------------------------------------------------------------------------
 //-----------------------------------------------------------------------------------------------------------------------------
 //-----------------------------------------------------------------------------------------------------------------------------
 // служебные операции - информация по внутренним ресурсам
 //-----------------------------------------------------------------------------------------------------------------------------
 //-----------------------------------------------------------------------------------------------------------------------------
 //-----------------------------------------------------------------------------------------------------------------------------

 // Показать описание модели классов
 function show_descr_obj_clss()
 { ?><h1>Модель классов</h1><?
   damp_array($_SESSION['class_system']->tree_objects);
 }

 // Показать описание модели объектных таблиц
 function show_descr_obj_table()
 { ?><h1>Модель объектных таблиц</h1><?
   damp_array($_SESSION['descr_obj_tables']);
 }





 function get_db_recs_info()
 { $info=execSQL('show table status') ;

   ?><table>
     <tr class=clss_header>
       <td>Название</td>
       <td>Наименование</td>
       <td>Кол-во записей</td>
       <td>Размер</td>
       <td>Макс.размер</td>
       <td>Длина индекса</td>
       <td>Не используется</td>
       <td>След.значение индекса</td>
     </tr>
   <?
   if (sizeof($info)) foreach($info as $rec)
   { $tkey=$_SESSION['list_db_tables'][$rec['Name']] ;
     if (_DOT($tkey)->pkey) $name=_DOT($tkey)->name ; else $name='-' ;
     ?><tr>
        <td class=left><?echo $name?></td>
        <td class=left><?echo $rec['Name']?></td>
        <td><?echo $rec['Rows']?></td>
        <td class=left><?echo format_size($rec['Data_length'])?></td>
        <td><?echo format_size($rec['Max_data_length'])?></td>
        <td class=left><?echo format_size($rec['Index_length'])?></td>
        <td class=left><?echo format_size($rec['Data_free'])?></td>
        <td class=left><?echo $rec['Auto_increment']?></td>


      </tr><?
   }
   ?></table><?


 }


 function get_server_info()
 {  include_once(_DIR_TO_ROOT."/ini/index.php") ;
 	show_system_info() ;
 }

 // информация по настройке PHP
 function get_phpinfo()  { ?><div style='font-size:14px;'>
 <style type="text/css"><!--
 td, th { border: 1px solid #000000; font-size: 100%; vertical-align: baseline;}
//--></style>




 <? echo phpinfo() ; ?></div><?}
 /*
 function func_test_1()
 { $list_obj_main=execSQL('select G1.*,G2.* from obj_spl_gds G1,obj_spl_gds_ext_125 G2 where G1.clss=125 and G2.pkey=G1.pkey',2) ;
   $tkey=$_SESSION['pkey_by_table']['obj_spl_gds'] ;
   if (sizeof($list_obj_main)) foreach($list_obj_main as $obj)
     { $obj['parent']=$obj['pkey'] ;
       unset($obj['pkey']) ;
       unset($obj['temp']) ;
       unset($obj['add_name']) ;
       unset($obj['sales_type']) ;
       unset($obj['sales_value']) ;
       unset($obj['tkn_typ']) ;
       unset($obj['tkn_plt']) ;
       unset($obj['naznach']) ;
       unset($obj['sostav']) ;


       if (!isset($obj['yandex'])) unset($obj['yandex']) ;
       if (!isset($obj['color'])) unset($obj['color']) ;
       if (!isset($obj['postav'])) unset($obj['postav']) ;
       if (!isset($obj['gender'])) unset($obj['gender']) ;
       if (!isset($obj['fs'])) unset($obj['fs']) ;
       if (!isset($obj['tiposize'])) unset($obj['tiposize']) ;


       $obj['clss']=134 ;
       save_obj_in_table($tkey,$obj) ; // сохраняем объект в таблице, соответствующей классу объекта
     }
 }

 */

 function import_site_css()
 { global $FCKeditor_xmlstyle_fname,$FCKeditor_source_css ;
   if (!$FCKeditor_xmlstyle_fname) $FCKeditor_xmlstyle_fname='fckstyle.xml' ;

   echo '<strong>Импорт стилей редактора из файла </strong>'._PATH_TO_SITE.'/'.$FCKeditor_source_css.'<br>' ;
   $file_css=file_get_contents(_PATH_TO_SITE.'/'.$FCKeditor_source_css) ;
   // получаем все описания стилей
   preg_match_all('#^(.*?){#im',$file_css,$rules) ;
   //echo '<strong>Классы</strong>' ; damp_array($rules[1]) ;

   $imp = new DOMImplementation;
   //$dtd = $imp->createDocumentType('','',''); // Creates a DOMDocumentType instance
   $doc = new DomDocument(); // старый вариант
   //$doc = $imp->createDocument("","",$dtd); // Creates a DOMDocument instance

   $doc->encoding = 'windows-1251';
   $doc->formatOutput = true;
   $styles = add_element($doc,$doc,'Styles') ;

   if (sizeof($rules[1])) foreach($rules[1] as $name)
   { $info=explode('.',$name) ;
     $tag=($info[0])? $info[0]:'span' ;
     $class=$info[1] ;

     if ($class) { $style=add_element($doc,$styles,'Style','',array('name'=>$name,'element'=>$tag)) ;
                   add_element($doc,$style,'Attribute','',array('name'=>'class','value'=>$class)) ;
                   echo 'Импортирован: <strong>'.$name.'</strong><br>' ;
                 }

   }

   $xml=$doc->saveXML() ;
   file_put_contents(_DIR_TO_ROOT.'/'.$FCKeditor_xmlstyle_fname,$xml) ;
   echo '<strong>Сохранено в '._DIR_TO_ROOT.'/'.$FCKeditor_xmlstyle_fname.'</strong><br>' ;
   ?><iframe src="<?echo _PATH_TO_SITE.'/'.$FCKeditor_xmlstyle_fname ?>" name='fra_xml' scrolling=auto frameborder=1 width=100% height=600>&nbsp;</iframe><?

 }

 function raspred_user_by_first_letter()
 {
   global $TM_users ;
   $list_buk=execSQL('select obj_name,pkey from '.$TM_users.' where clss=1 and parent=4') ;
   $list_users=execSQL('select * from '.$TM_users.' where clss=87 and parent=4') ;
   if (sizeof($list_users)) foreach($list_users as $rec)
   { $name=ltrim($rec['obj_name']) ;
     $first_buk=mb_strtoupper($name[0]) ;
     echo $rec['pkey'].' - '.$rec['obj_name'].' - '.$first_buk.'<br>' ;
     if (isset($list_buk[$first_buk])) $arr[$list_buk[$first_buk]['pkey']][]=$rec['pkey'] ;
   }
   //damp_array($arr) ;
   if (sizeof($arr)) foreach($arr as $new_parent=>$arr_pkeys)
   { $pkeys=implode(',',$arr_pkeys) ;
     echo $new_parent.': '.$pkeys.'<br>' ;
     $sql='update '.$TM_users.' set parent='.$new_parent.' where pkey in ('.$pkeys.')' ;
     if (!$result = mysql_query($sql)) SQL_error_message($sql,'Не удалось обновить запись в таблице',1) ;
   } else echo 'Все клиенты распределены' ;
 }

 function check_create_class_model()
 {  create_class_model(1) ; }


 function test_JS()
 {
 	create_model('arr_obj','c_obj','1,101','obj_descr_obj_tables','indx,obj_name') ;   // данные параметры должны стыковаться с параметрами в c_editor_descr_tables
 	global $arr_obj ;
 	?><script><?
 	if (isset($arr_obj['root'])) $arr_obj['root']->convert_php_obj_to_js() ;
   	?>obj_root.show_as_select('sel_name',597) ;
   	  </script><?
 }


 function dates_select($set_mode=0) // выводим меню по датам
 {  global $mode,$tkey,$pkey,$tree_orders ;
    $now=getdate() ;
    if ($set_mode) $mode=$set_mode ;

    // считаем число заказов за сегодня
    $start_time=mktime(0, 0, 0, $now['mon'],$now['mday'],$now['year']); // начало дня
    $usl='c_data>='.$start_time.' and clss=82 ' ;
    $sql='select count(pkey) as cnt from '._DOT($tkey)->table_name.' where '.$usl ;
    $count_1=execSQL_van($sql) ;
    // считаем число заказов за неделю
    $start_time=time()-(($now['wday'])? ($now['wday']-1)*86400:7*86400) ;// начало недели
    $usl='c_data>='.$start_time.' and clss=82 ' ;
    $sql='select count(pkey) as cnt from '._DOT($tkey)->table_name.' where '.$usl ;
    $count_2=execSQL_van($sql) ;
    // считаем число заказов за месяц
    $start_time=mktime(0, 0, 0, $now['mon'],1,$now['year']); // начало месяца
    $usl='c_data>='.$start_time.' and clss=82 ' ;
    $sql='select count(pkey) as cnt from '._DOT($tkey)->table_name.' where '.$usl ;
    $count_3=execSQL_van($sql) ;

    switch ($mode)
    { case 1: $class1='class=sel' ; $title='Заказы за сегодня' ; break;
      case 2: $class2='class=sel' ; $title='Заказы за неделю' ; break ;
      case 3: $class3='class=sel' ; $title='Заказы за месяц' ; break;
    }

    $base_href='viewer_orders.php?tkey='.$tkey.'&pkey='.$pkey ;

    echo '<h2 class=orders><a href="'.$base_href.'&mode=1" '.$class1.'>За сегодня ('.$count_1['cnt'].')</a></h2>' ;
    echo '<h2 class=orders><a href="'.$base_href.'&mode=2" '.$class2.'>За неделю ('.$count_2['cnt'].')</a></h2>' ;
    echo '<h2 class=orders><a href="'.$base_href.'&mode=3" '.$class3.'>За месяц ('.$count_3['cnt'].')</a></h2>' ;
    ?><div class=clear></div><?
    ?><h1><?echo $title?></h1><?
    $tree_orders[$pkey]->show_orders($mode) ;


 }

 function del_clone()
 { global $clone,$tkey,$TM_goods,$TM_goods_image ;
   if (!$clone) { echo 'Не указано имя клона' ; return ;}
   ?><h1>Удаляем все клоны изображений</h1>
     Будут удалены все изображения клона "<?echo $clone?>"<br><br><br>
   <?
   $tkey=$_SESSION['pkey_by_table'][$TM_goods] ;
   $tkey_image=$_SESSION['pkey_by_table'][$TM_goods_image] ;
   $dir=_DOT($tkey_image)->dir_to_file ;
   $dir_list=get_list_img_for_dir($dir.$clone.'/') ;
   echo 'До удаления в '.$dir.$clone.'/'.sizeof($dir_list).' изображений<br>' ;
   if (sizeof($dir_list)) foreach($dir_list as $fname) { if (unlink($dir.$clone.'/'.$fname)) echo $fname.' - удален<br>' ; else echo $fname.' - не получилось удалить<br>' ; } ;
   $dir_list=get_list_img_for_dir($dir.$clone.'/') ;
   echo 'После удаления в '.$dir.$clone.'/'.sizeof($dir_list).' изображений<br>' ;
 }

 // создаем клоны изображений для товарной таблицы
 /*
 function sync_clone()
 { global $tkey,$TM_goods,$TM_goods_image ;
   //$list=execSQL('select * from '.$TM_goods_image.' where pkey>=1118 and pkey<=1122',2) ;
   ?><h1>Синхронизация клонов фото для товарной таблицы</h1><?
   $tkey=$_SESSION['pkey_by_table'][$TM_goods] ;
   $tkey_image=$_SESSION['pkey_by_table'][$TM_goods_image] ;

   $dir=_DOT($tkey_image)->dir_to_file ;


   // получаем массив исходников
   include_once(_DIR_TO_ENGINE.'/admin/i_img_working.php') ;
   $dir_list_images=get_list_img_for_dir($dir.'source/') ;
   $dir_list_small=get_list_img_for_dir($dir.'small/') ;


   echo $dir.'source/ : '.sizeof($dir_list_images).' изображений<br>' ;
   echo $dir.'small/  : '.sizeof($dir_list_small).' изображений &nbsp;&nbsp;&nbsp;<a href="?cmd=del_clone&clone=small">Удалить клоны</a><br>' ;

   if (sizeof(_DOT($tkey_image)->list_clone_img())) foreach(_DOT($tkey_image)->list_clone_img() as $indx_clone=>$clone_info)
   {
     $dir_list_clones=get_list_img_for_dir($dir.$indx_clone.'/') ;
     echo $dir.$indx_clone.'/ : '.sizeof($dir_list_clones).' изображений<br>' ;
   }

   include(_DIR_TO_ENGINE.'/admin/i_sect_command.php') ;
   global $use_clone_to_create ;

   if (sizeof(_DOT($tkey_image)->list_clone_img())) foreach(_DOT($tkey_image())->list_clone_img() as $use_clone_to_create=>$clone_info) cmd_exe_create_clone_photo() ;
   $use_clone_to_create='small' ;
   cmd_exe_create_clone_photo() ;
 }

 function get_images_exist_info($table_name)
 { $tkey_image=$_SESSION['pkey_by_table'][$table_name] ;
   $dir=_DOT($tkey_image)->dir_to_file ;
   $db_list_images=execSQL('select obj_name,pkey,parent from '._DOT($tkey_image)->table_name.' where clss=3 and obj_name>""') ;
   $dir_list_images=get_list_img_for_dir($dir.'source/') ;

   echo 'Всего в базе '.sizeof($db_list_images).' изображений<br>' ;
   echo 'Закачано в /source : '.sizeof($dir_list_images).' изображений<br>' ;

   // идем по списку изображений в каталоге, если изображение с таким именем присутствуе в базе, удаляем его из списка базы
   if (sizeof($dir_list_images)) foreach($dir_list_images as $i=>$fname)
    if (sizeof($db_list_images[$fname]))
      { unset($db_list_images[$fname]) ;
        unset($dir_list_images[$i]) ;
      }
   return(array($db_list_images,$dir_list_images)) ;
 }
 */

 function search_delete_images()
 { global $TM_goods_image ;
   include_once(_DIR_TO_ENGINE.'/admin/i_img_working.php');
   //$tkey=$_SESSION['pkey_by_table'][$TM_goods] ;


   //$patch=_DOT($tkey_image)->patch_to_file ;

   //echo 'dir='.$dir.'<br>' ;

   ?><h1>Анализ фото товарной таблицы</h1><?
   list($db_list_images,$dir_list_images)=$this->get_images_exist_info($TM_goods_image) ;

   ?><h1>Объекты "фото" для которых указано имя файла, но сам файл отсутствует (<?echo sizeof($db_list_images)?>)</h1><?
   damp_array($db_list_images) ;
   /*?><button onclick="exe_cmd('exec_add_photo')">Выполнить добавление </button><?*/

   ?><h1>Изображения, которые не внесены в базу (<?echo sizeof($dir_list_images)?>)</h1><?
   damp_array($dir_list_images) ;
   ?><button onclick="exe_cmd('exec_delete_not_used_photo')">Выполнить удаление этих фото с диска</button><?

   return ;
   //damp_array($dir_list_images) ;


   $db_list_images=execSQL('select obj_name,pkey,parent from '._DOT($tkey_image)->table_name.' where clss=3 and obj_name>""') ;
   // идем по списку изображений в базе, если изображение с таким именем присутствуе в каталоге, удаляем его из списка каталога
   if (sizeof($db_list_images)) foreach($db_list_images as $rec)
    //if (in_array($dir_list_images[$rec['obj_name']]))
   //  { $i=array_search($rec['obj_name'],$dir_list_images) ;
   //    unset($dir_list_images[$i]) ;
   //  }  ;

    /*

    if (sizeof($db_list_images[$fname]))
     { unset($db_list_images[$fname]) ;
       unset($dir_list_images[$i]) ;
     }  ;
      */

   return ;

   ?><h1>Фото в /source для которых отсутствуют объекты фото (<?echo sizeof($dir_list_images)?>)</h1><?
   ?><button onclick="exe_cmd('exec_delete_photo')">Выполнить удаление </button>&nbsp;&nbsp;&nbsp;&nbsp;
     <button onclick="exe_cmd('exec_add_photo')">Выполнить добавление </button>&nbsp;&nbsp;&nbsp;&nbsp;
     <br><br><?
   // сначала собираем все коды объектов
   ?><table>
        <tr><td>Файл</td>
      		<td>Размер</td>
      		<td>Удалить</td>
      		<td>Картинка</td>
      		<td>Возможный товар</td>
      		<td>Фото в составе товара</td>
      		<td>Добавить к товару</td>
      	</tr>
   <?
   if (sizeof($dir_list_images)) foreach($dir_list_images as $i=>$fname)
   { ?><tr><?
   	 $obj_id=mb_substr($fname,0,strpos($fname,'_')) ;
   	 $obj_info=execSQL_van('select * from '.$TM_goods.' where pkey="'.$obj_id.'"') ;
   	 if (sizeof($obj_info)) $obj_img_info=execSQL('select * from '.$TM_goods_image.' where parent="'.$obj_info['pkey'].'"') ;
   	  else unset($obj_img_info) ;
   	 $size_info=get_image_info($dir.'source/'.$fname,'source ') ;
   	 $checked_add=($obj_info['pkey'] and !sizeof($obj_img_info))? 'checked':'' ;
   	 $checked_del=(!$obj_info['pkey'] and !sizeof($obj_img_info) and $fname!='def_img.jpg')? 'checked':'' ;

  	 $small_image_src=$patch.'small/'.$fname ;
     $big_image_src=$patch.'source/'.$fname ;
     //echo $big_image_src.'<br>' ;
     $text_img='<a href="'.$big_image_src.'"  target=_clear><img src="'.$small_image_src.'" border="0" /></a>';

   	 ?><td><? echo $fname ?></td>
   	   <td><? echo $size_info ?></td>
   	   <td><input name="image_to_del[<?echo $i?>]" type="checkbox" value="<?echo $fname?>" <?echo $checked_del?>></td>
   	   <td><? echo $text_img ?></td>
   	   <td><? if ($obj_info['pkey']) echo $obj_info['obj_name'].'('.$obj_info['pkey'].'['.$obj_info['clss'].']' ;  ?></td>
   	   <td><? if (sizeof($obj_img_info)) foreach ($obj_img_info as $rec_img)  {?><img src="<? echo $patch.'small/'.$rec_img['obj_name'] ?>" border="0"><br><?echo $rec_img['obj_name']?><br><?}?></td>
   	   <td><input name="image_to_add[<?echo $obj_info['pkey']?>]" type="checkbox" value="<?echo $fname?>" <?echo $checked_add?>></td>
   	 </tr><?
   }
   ?></table><?

   ?><h1>Объекты "фото" для которых не указано имя файла(<?echo sizeof($db_list_space)?>)</h1><?
   damp_array($db_list_space,1) ;

 }

 function exec_delete_not_used_photo()
 { global $TM_goods_image ;
   $tkey_image=$_SESSION['pkey_by_table'][$TM_goods_image] ;
   $dir=_DOT($tkey_image)->dir_to_file ;
   ?><h1>Удаляем фото, которые не внесены в базу</h1><?
   list($db_list_images,$dir_list_images)=$this->get_images_exist_info($TM_goods_image) ;
   echo '<div class="red bold">Будет удалено '.sizeof($dir_list_images).' изображений</div>' ;
   if (sizeof($dir_list_images)) foreach($dir_list_images as $fname)
   { //echo $fname.'<br>' ;
     if (unlink($dir.'source/'.$fname)) echo $fname.' - удален<br>' ; else echo $fname.' - не получилось удалить<br>' ;
   } ;



 }

 function exec_delete_photo()
 { global $image_to_del ;
   global $tkey,$TM_goods,$TM_goods_image ;
   $tkey=$_SESSION['pkey_by_table'][$TM_goods] ;
   $tkey_image=$_SESSION['pkey_by_table'][$TM_goods_image] ;

   $dir=_DOT($tkey_image)->dir_to_file ;
   $patch=_DOT($tkey_image)->patch_to_file ;


   if (sizeof($image_to_del))
   { ?><h1>Удаляем отмеченные фото (<?echo sizeof($image_to_del)?>)</h1><?
     foreach($image_to_del as $fname) { if (unlink($dir.'source/'.$fname)) echo $fname.' - удален<br>' ; else echo $fname.' - не получилось удалить<br>' ; } ;
   } else echo 'Ни одного объекта не выбрано' ;
 }

 function exec_add_photo()
 { global $image_to_add ;
   global $tkey,$TM_goods,$TM_goods_image ;
   $tkey=$_SESSION['pkey_by_table'][$TM_goods] ;
   $tkey_image=$_SESSION['pkey_by_table'][$TM_goods_image] ;

   $dir=_DOT($tkey_image)->dir_to_file ;
   $patch=_DOT($tkey_image)->patch_to_file ;

   if (sizeof($image_to_add))
   { ?><h1>Добавляем отмеченные фото в состав объектов (<?echo sizeof($image_to_add)?>)</h1><?
     foreach($image_to_add as $pkey=>$fname)
     { $next_indx=execSQL_van('select max(indx) as max_indx from '.$TM_goods_image.' where parent='.$pkey.' and clss=3') ;
       echo 'Добавляем '.$fname.' к '.$pkey.'<br>' ;
       $finfo=array('clss'			=>	3,
      				'parent'		=>	$pkey,
     				'obj_name' 		=> 	$fname,
     				'enabled'		=>	1,
    				'indx'			=>	$next_indx['max_indx']+1
    			   ) ;
       //damp_array($finfo) ;
       adding_rec_to_table($tkey_image,$finfo) ;
     }
   } else echo 'Ни одного объекта не выбрано' ;
 }

 function check_name_images()
 { global $tkey,$TM_goods,$TM_goods_image ;
   ?><h1>Проверяем соответствие имен изображений именам родительских объектов</h1>
   Сейчас проверяется соотвествие имен дочерних изображений объект их кодам.<br>
   При добавлении изображения в состав объекта его имя формируется как "код объекта_наименование файла".<br>
   Соотвествеено, если код объекта не входит в имя изображения то возможно несоответствие между фото и родительским объектом.<br>
   Однако, необходимо помнить, что после копирования/перемещения объекта его код изменится, а имена дочерних фоток отстанутся без изменений.<br>
   Проверке не подлежат разделы каталога, т.к им может произвольно назначатся фото любого дочернего объекта<br><br><br>
   <?

   $tkey=$_SESSION['pkey_by_table'][$TM_goods] ;
   $tkey_image=$_SESSION['pkey_by_table'][$TM_goods_image] ;
   $list=execSQL('select goods.pkey,goods.obj_name,img.pkey as img_pkey,img.obj_name as img_name from '.$TM_goods_image.' img,'.$TM_goods.' goods where goods.pkey=img.parent and img.obj_name not like CONCAT(img.parent,"_%") and goods.clss!=1 and goods.clss!=10 order by goods.pkey') ;

   $dir=_DOT($tkey_image)->dir_to_file ;
   $patch=_DOT($tkey_image)->patch_to_file ;

   ?><table>
        <tr><td>Код товара</td>
            <td>Название товара</td>
      		<td>Название фото</td>
      		<td>Картинка</td>
      		<td>Удалить из товара</td>
      	</tr>
   <?
   if (sizeof($list)) foreach($list as $rec)
   { ?><tr>
       <td><? echo $rec['pkey'] ?></td>
   	   <td><? echo $rec['obj_name'] ?></td>
   	   <td><? echo $rec['img_name'] ?></td>
   	   <td><img src="<? echo $patch.'small/'.$rec['img_name'] ?>" border="0"></td>
   	   <td><input name="image_to_del[<?echo $rec['pkey']?>]" type="checkbox" value="<?echo $rec['img_pkey']?>" ></td>
   	 </tr><?
   }
   ?></table><?



 }


//************************************************************************************************************************************
// SETUP
//*************************************************************************************************************************************/

 function info_db_tables()
 { ?><h1>Информация по таблицам БД</h1><?
   $_SESSION['list_db_tables']=get_db_info() ;
   if (!sizeof($_SESSION['list_db_tables'])) echo 'В БД не обнаружено таблиц' ; else damp_array($_SESSION['list_db_tables']) ;
 }

function create_based_tables()
 { include_once(_DIR_TO_ENGINE.'/i_descr_tables.php') ;
   create_sys_table() ;
    echo '<span class="red bold">Перегрузите админку для принятия изменений</span>' ;
 }


//************************************************************************************************************************************
// SETUP
//*************************************************************************************************************************************/


 function _902_check_file_perms()
 {  ?><h1>Проверка правильного назначения прав на загружаемые файлы</h1><?
	echo 'Проверяем владельца и группу для директорий созданных через SSH или FTP<br>' ;
    $SSH_file_info=get_file_perms_info(_DIR_TO_ENGINE.'/') ;
    damp_array($SSH_file_info) ;

	//echo '<span class=black>'._DIR_TO_PUBLIC.'</span><br>' ;
	//damp_array(get_file_perms_info(_DIR_TO_PUBLIC.'/')) ;

	$check_file_name='test.txt' ; $check_dir_name='test' ;

	echo 'Пытаемся создать директорию - ' ;
    $fdir=_DIR_TO_PUBLIC.'/'.$check_dir_name ;
    $fpatch=_PATH_TO_PUBLIC.'/'.$check_dir_name ;
    if (create_dir($fdir))
      		    { echo '<a target=_blank href="'.$fpatch.'" class=green>'.$fpatch.'</a>  - директория успешно создана<br>' ;
                  $PHP_dir_info=get_file_perms_info($fdir) ;
                  damp_array($PHP_dir_info) ;
      			  if (_CREATE_DIR_PERMS and $PHP_dir_info['perms']!=_CREATE_DIR_PERMS)
                  { if (chmod($fdir,_CREATE_DIR_PERMS)) echo 'Автоизменение прав: <span class=black>'.$fdir.'</span> - права доступа успешно изменены на <span class="black bold">'.decoct(_CREATE_DIR_PERMS).'</span><br>' ;
            	  	else echo 'Автоизменение прав: <span class=red>'.$fdir.'</span> - не удалось изменить права доступа на '.decoct(_CREATE_DIR_PERMS).'<br>' ;
            	  }
                }
      else      echo $fpatch.' - <span class=red>директория уже существует или недостаточно прав</span><br>' ;

	echo '<br>Пытаемся создать файл - ' ;
    $fdir=_DIR_TO_PUBLIC.'/'.$check_file_name ;
    $fpatch=_PATH_TO_PUBLIC.'/'.$check_file_name ;
    $cnt=file_put_contents($fdir,'тестовый файл, можно удалять') ;
      if ($cnt) { echo '<a target=_blank href="'.$fpatch.'" class=green>'.$fpatch.'</a>  - файл успешно создан<br>' ;
                  $PHP_file_info=get_file_perms_info($fdir) ;
                  damp_array($PHP_file_info) ;

      			  if (_CREATE_FILE_PERMS and $PHP_file_info['perms']!=_CREATE_FILE_PERMS)
                  { if (chmod($fdir,_CREATE_FILE_PERMS)) echo 'Автоизменение прав: <span class=black>'.$fdir.'</span> - права доступа успешно изменены на <span class="black bold">'.decoct(_CREATE_FILE_PERMS).'</span><br>' ;
            	  	else echo 'Автоизменение прав: <span class=red>'.$fdir.'</span> - не удалось изменить права доступа на '.decoct(_CREATE_FILE_PERMS).'<br>' ;
            	  }
                }
      else      echo $fpatch.' - <span class=red>не удалось создат файл</span><br><br>' ;

      ?><h2>Результаты проверки</h2>
		<ul><? if ($SSH_file_info['owner']==$PHP_dir_info['owner']) {?><li>SSH и PHP работают под одним пользователем, для совместного доступа директории должны иметь права доступа 0755, файлы - 0644 </li><?}
			   	else if ($SSH_file_info['group']==$PHP_dir_info['group']) {?><li>SSH и PHP находятся в одной группе но работаю под разными пользователями, для совместного доступа директории должны иметь права доступа 0775, файлы - 0664</li><?}
			   	else {?><li>SSH и PHP находятся в разных группах - это неправильная настройка хостинга - срочно обратитесь в службу поддержки!!!</li><?}

               if ($SSH_file_info['owner']!=$PHP_dir_info['owner'] and $SSH_file_info['group']==$PHP_dir_info['group'] and $PHP_dir_info['perms']!='0775') {?><li>Вновь создаваемым средствами PHP директориям по умолчанию присвается недостаточно прав для их редактировния под SSH.<br>Установите в админке настройки: <span class="black bold">_CREATE_DIR_PERMS='0775' ;</span></li><?}

               if ($SSH_file_info['owner']!=$PHP_file_info['owner'] and $SSH_file_info['group']==$PHP_file_info['group'] and $PHP_file_info['perms']!='0664') {?><li>Вновь создаваемым средствами PHP директориям по умолчанию присвается недостаточно прав для их редактировния под SSH.<br>Установите в админке настройки: <span class="black bold">_CREATE_FILE_PERMS='0664' ;</span></li><?}

            ?>
        </ul>
        <h2>Помни!</h2>
        <ul><li>Права доступа к файлу может изменять только владелец файла</li>
            <li>Владельца файла или директории может изменять только администратор хостинга с правами root</li>
        	<li>Переименовывать файл, изменять его содержимое производится согласно правам доступа:<br>0644 - Чтение - Все, Запись - толко Владелец<br>0664 - Чтение - Все, Запись - Владелец + Группа<br></li>
        	<li>Переименовывать файл по SSH можно если user SSH входит в одну группу с владельцем директории и на директории стоят права 0775</li>
        	<li>Редактировать файл по SSH можно если user SSH входит в одну группу с владельцем файла и на файле стоят права 0664</li>
        </ul>
      <?

    echo 'Удаляем временную директорию: - ' ;
    if (rmdir(_DIR_TO_PUBLIC.'/'.$check_dir_name)) echo 'директория успешно удалена<br>' ; else echo ' не удалось удалить директорию<br>' ;
    echo 'Удаляем временный файл: - ' ;
    if (unlink(_DIR_TO_PUBLIC.'/'.$check_file_name)) echo 'файл успешно удален<br>' ; else echo ' не удалось удалить файл<br>' ;
 }





 function _903_change_dir_perms()
 { ?><h1>Проверка и исправление прав доступа к подразделам /public/</h1>
     Директориям будет назначено права: <?echo decoct(_CREATE_DIR_PERMS)?><br><br>
   <?
   read_dir_list(_DIR_TO_PUBLIC.'/','',$list_dir) ;
   //$list_dir=array_slice($list_dir,0,10);
   //damp_array($list_dir) ;
   ?><table><?
   if (sizeof($list_dir)) foreach($list_dir as $dir_name)
   {  $file_info=get_file_perms_info(_DIR_TO_PUBLIC.'/'.$dir_name) ;
      $color='' ;
   	  if ($file_info['perms']!=decoct(_CREATE_DIR_PERMS)) $color=(chmod(_DIR_TO_PUBLIC.'/'.$dir_name,_CREATE_DIR_PERMS))? 'green':'red' ;
      ?><tr><td class=left><? echo _PATH_TO_PUBLIC.'/'.$dir_name?></td>
   	       <td><? echo $file_info['perms']?></td>
   	       <td><? echo $file_info['owner']?></td>
   	       <td><? echo $file_info['group']?></td>
   	       <td><? if ($color) echo '<span class=" '.$color.' bold">'.decoct(_CREATE_DIR_PERMS).'</span>'?></td>
       </tr>
     <?
}
   ?></table><?
 }

 function _0904_change_file_names()
 { ?><h1>Проверка доступности изображений</h1><?
   global $sel_tkey,$cmd ;
   ?>Выберите таблицу: <select size="1" name="sel_tkey"><option value=""></option><?
   if (sizeof($_SESSION['descr_obj_tables'])) foreach($_SESSION['descr_obj_tables'] as $table_obj) if ($table_obj->list_clss[3]==$table_obj->pkey or $table_obj->list_clss[5]==$table_obj->pkey) echo '<option value="'.$table_obj->pkey.'" '.(($sel_tkey==$table_obj->pkey)? 'selected':'').'>'.$table_obj->name.' ('.$table_obj->table_name.')</option>' ;
   ?></select> <button onclick="exe_cmd('')">Далее</button><?
   if (!$sel_tkey) return ;
   ?><button onclick="exe_cmd('delete_error')">Устранить найденные ошибки</button><?
   //echo 'cmd='.$cmd.'<br>' ;
   ?><br><br><?
   $parent_tkey=_DOT($sel_tkey)->parent ;
   //damp_array(_DOT($sel_tkey));

   $dir_to_file=_DOT($sel_tkey)->dir_to_file ;
   $table_name=_DOT($sel_tkey)->table_name ;
   $parent_table_name=_DOT($parent_tkey)->table_name ;
   $fname_name=(_DOT($sel_tkey)->list_clss[3]==$sel_tkey)? 'obj_name':'file_name' ;
   $list_recs=execSQL('select T1.pkey,T2.parent,T1.'.$fname_name.' as file_name,T2.obj_name as parent_name,T2.pkey as parent_pkey from '.$table_name.' T1 left join '.$parent_table_name.' T2 on T2.pkey=T1.parent')  ;
   echo 'Всего в базе '.sizeof($list_recs).' записей<br><br>' ;
   $i=0 ;
   ?><table><?
   if (sizeof($list_recs)) foreach($list_recs as $rec) //if ()
   {  $info=array(); $res_1=array() ; $res_2=array() ; $file_info=array() ; $sql='' ; $new_file_name='' ;  $new_file_dir='' ;
   	  $sub_dir=(_DOT($sel_tkey)->list_clss[3]==$sel_tkey)? 'source/':'' ;
   	  $file_dir=$dir_to_file.$sub_dir.$rec['file_name'] ;

      if (preg_match('/\s/i',$rec['file_name'])) 	{ $info[]='Имя файла содержит пробелы' ;
                                                      $new_file_name=safe_file_names($rec['file_name']) ;
                                                      $new_file_dir=$dir_to_file.$sub_dir.$new_file_name ;
      												  $sql='update '.$table_name.' set '.$fname_name.'="'.$new_file_name.'" where pkey='.$rec['pkey'] ;
      												  $res_1[]='Имя файла будет изменено' ;
      												  $res_2[]='Изменено имя файла' ;

      												}

      if (preg_match('/%20/i',$rec['file_name'])) 	{ $info[]='Имя файла содержит пробелы' ;
      												  $new_file_name=safe_file_names(str_replace('%20',' ',$rec['file_name'])) ;
      												  $new_file_dir=$dir_to_file.$sub_dir.$new_file_name ;
      												  $sql='update '.$table_name.' set '.$fname_name.'="'.$new_file_name.'" where pkey='.$rec['pkey'] ;
      												  $res_1[]='Имя файла будет изменено' ;
      												  $res_2[]='Изменено имя файла' ;
      												}
      if (preg_match("/[А-я]/",$rec['file_name'])) 	{ $info[]='Имя файла содержит русские буквы';
                                                      $new_file_name=safe_file_names($rec['file_name']) ;
                                                      $new_file_dir=$dir_to_file.$sub_dir.$new_file_name ;
      												  $sql='update '.$table_name.' set '.$fname_name.'="'.$new_file_name.'" where pkey='.$rec['pkey'] ;
      												  $res_1[]='Имя файла будет изменено' ;
      												  $res_2[]='Изменено имя файла' ;
      												}

      if (!$rec['parent_pkey'])						{ $info[]='Для этого объекта нет родителя';
													  $sql='delete from '.$table_name.' where pkey='.$rec['pkey'] ;
      												  $res_1[]='Запись по объекту будет удалена с базы ' ;
      												  $res_2[]='Запись по объекту удалена с базы' ;
      												}


      if (!$rec['file_name'])                       { $info[]='Не указано имя файла';
      												  $sql='delete from '.$table_name.' where pkey='.$rec['pkey'] ;
      												  $res_1[]='Запись по объекту будет удалена с базы ' ;
      												  $res_2[]='Запись по объекту удалена с базы' ;
      												}

      else { if (file_exists($file_dir)) 			$file_info=get_file_perms_info($file_dir) ;
         	 else 									{
         	 										  if (!sizeof($info)) {	$sql='update '.$table_name.' set enabled=0 where pkey='.$rec['pkey'] ;
         	 										  						$res_1[]='Запись по объекту будет отключена' ;
         	 										  						$res_2[]='Запись по объекту отключена' ;

         	 										  					  }
         	 										  $info[]='Файл отстутсвует на сервере' ;
         	 										}
       	   }

      $res_1=array_unique($res_1) ;
      $res_2=array_unique($res_2) ;

   	  if (sizeof($info))
      { $i++ ;
      	?><tr><td class=left><? echo $rec['parent_pkey']?></td>
      		<td class=left><a onclick="show_obj(<?echo $parent_tkey?>,<?echo $rec['parent_pkey']?>,1);return(false);" class="a_ref" href=""><? echo $rec['parent_name']?></a></td>
      		<td class=left><? echo $rec['file_name']?></td>
   	       	<td><? echo $file_info['perms']?></td>
   	      	<td><? echo $file_info['owner']?></td>
   	       	<td><? echo $file_info['group']?></td>
   	       	<td class="red left"><? echo implode('<br>',$info) ;?></td>
   	       	<td class="green left"><?
   	       	                          if ($cmd=='delete_error')
	   	       	                      {  echo implode('<br>',$res_2) ;
	   	       	                         if (file_exists($file_dir) and $new_file_dir)
	   	       	                          { echo '<br>Переименование файла - ' ;
	   	       	                            if (rename($file_dir,$new_file_dir)) { 	echo 'OK<br>операция - БД - ' ;
	   	       	                          											if (!$result = mysql_query($sql)) SQL_error_message($sql,'',1) ;
	   	       	                          											else echo 'OK' ;
	   	       	                          										 }
	   	       	                            else echo 'file error' ;
	   	       	                            $sql='' ; // защита от
	   	       	                          }

	   	       	                          if ($sql) 							{  echo '<br>операция БД - ' ;
	   	       	                          										   if (!$result = mysql_query($sql)) SQL_error_message($sql,'',1) ;
	   	       	                          										   else echo 'OK' ;
	   	       	                          										 }
	   	       	                      } else echo implode('<br>',$res_1) ;
   	       						   ?>
   	         </td>

       </tr>
     <?}

   }
   ?></table><?
   echo 'Обнаружено '.$i.' ошибочных записей<br>' ;


   //$list_recs=execSQL('select * from obj_site_goods_image where obj_name like "% %"',2)  ;
   //$list_recs=execSQL('select * from obj_site_goods_files where file_name like "% %"',2)  ;


 }

 function _0905_check_safe_patch_files()
 { global $check_name ;
   ?><h1>Проверка преобразования пути файла для загрузки</h1>
     проверяем работу функции "safe_web_patch" и "safe_file_names"<br><br>
     Примеры имен файлов:<br>
     <ul>
  		<li>http://www.clss.ru/%D0%9A%D0%BB%D0%B0%D1%81%D1%81%D0%BD%D0%B0%D1%8F%20%D0%BA%D1%83%D1%85%D0%BD%D1%8F.jpg</li>
  		<li>http://www.clss.ru/studia%20273.jpg</li>
  		<li>http://www.clss.ru/super%20%D0%BA%D1%83%D1%85%D0%BD%D1%8F%203.jpg</li>
  		<li>http://www.clss.ru/Классная%20кухня.jpg</li>
     </ul>
     Введите ссылку на файл для проверки:
     <input name="check_name" type="text" value="<?echo $check_name?>" class=text size=100> <button onclick="exe_cmd('')">Проверить</button><br><br>
    <?
   if (!$check_name) return ;
   echo 'Ссылка для скачания файла: <span class="black">"'.$check_name.'"</span><br><br>';
   $file=file_get_contents(safe_web_patch($check_name)) ;
   if (strlen($file)) echo 'Файл <span class="green bold">доступен</span> для скачавания по ссылке, размер файла:'.strlen($file).'<br><br>' ;
   else echo 'Файл <span class="red bold">недоступен</span> для скачавания по ссылке<br><br>';
   echo 'Файл будет сохранен на сервере как "<span class="green bold">'.safe_file_names(basename($check_name)).'</span>"' ;




 }


 function _0906_check_defines_vers()
 { global $check_text
   ?><h1>Проверяем работу функции defines_vers</h1>
   Введите заголовок браузера ($_SERVER['HTTP_USER_AGENT']): <input type="text" size=100 name=check_text class=text value="<?echo $check_text?>"><button onclick="exe_cmd('')">Проверить</button><br><br>
   <?
   if ($check_text) { $br_vers=defines_vers($check_text,1) ;
                      echo 'Определенный браузер: '.$br_vers.'<br>' ;
                    }


 }


 function func_815()
 { ?><h1>Информация по журналу страниц</h1><?
   $info=execSQL_van('select min(c_data) as time1,max(c_data) as time2,count(pkey) as cnt from '.TM_LOG_PAGES) ;
   echo 'Первая запись в журнале: '.date('d.m.Y H:i:s',$info['time1']).'<br>' ;
   echo 'Последняя запись в журнале: '.date('d.m.Y H:i:s',$info['time2']).'<br>' ;
   echo 'Число записей в журнале: '.$info['cnt'].'<br>' ;

 }


 function merge_log_events()
 {   ?><h1>Объединяем журналы событий</h1><?
     ?><h2>До объединения</h2><?
     $info=execSQL_van('select min(c_data) as time1,max(c_data) as time2,count(pkey) as cnt from '.TM_LOG_EVENTS) ;
     echo 'Первая запись в журнале: '.date('d.m.Y H:i:s',$info['time1']).'<br>' ;
     echo 'Последняя запись в журнале: '.date('d.m.Y H:i:s',$info['time2']).'<br>' ;
     echo 'Число записей в журнале: '.$info['cnt'].'<br>' ;
     ?><h2>Объдиняем</h2><?
     $info=execSQL_update('insert into '.TM_LOG_EVENTS.' (c_data,reffer,comment,ip,obj_name,member,links,init,clss,enabled,indx,parent,r_data)  select c_data,reffer,comment,ip,obj_name,member,links,init,clss,enabled,indx,parent,r_data from '.TM_LOG_EVENTS) ;
     print_r($info) ;
     ?><h2>До объединения</h2><?
     $info=execSQL_van('select min(c_data) as time1,max(c_data) as time2,count(pkey) as cnt from '.TM_LOG_EVENTS) ;
     echo 'Первая запись в журнале: '.date('d.m.Y H:i:s',$info['time1']).'<br>' ;
     echo 'Последняя запись в журнале: '.date('d.m.Y H:i:s',$info['time2']).'<br>' ;
     echo 'Число записей в журнале: '.$info['cnt'].'<br>' ;

 }


 function test_buttons()
 {   ?><h1>Проверяем работу кнопок</h1>
       <h2>Отработка команды в текущем классе страницы. Будет вызван метод <strong>"test_buttons_1"</strong> из класса страницы</h2>
       <textarea class="view_code"><button cmd="test_buttons_1" xxx="1" yyyy="2">Проверить</button></textarea><br>
       <button cmd="test_buttons_1" xxx="1" yyyy="2">Проверить</button>

       <h2>Отработка команды в классе CLSS. Будет вызван метод <strong>"show_this"</strong> класса 1</h2>
       <textarea class="view_code"><button cmd="show_this" clss="1">Проверить</button></textarea><br>
       <button cmd="show_this" clss="1" debug="1">Проверить</button>

       <h2>Вызов функции из расширения двидка. Будет вызвана функция <strong>"demo_panel"</strong> из скрипта /engine/ext/demo/demo_panel.php</h2>
       <textarea class="view_code"><button cmd="demo_panel" from="ext/demo">Проверить</button></textarea><br>
       <button cmd="demo_panel" from="ext/demo">Проверить</button>

       <h2>Вызов функции из расширения двидка. Будет вызвана функция <strong>"demo_panel2"</strong> из скрипта /engine/ext/demo/i_demo.php</h2>
       <textarea class="view_code"><button cmd="demo_panel2" from="ext/demo/i_demo.php">Проверить</button></textarea><br>
       <button cmd="demo_panel2" from="ext/demo/i_demo.php">Проверить</button>

       <h2>Вызов функции из модуля сайта. Будет вызвана функция <strong>"demo_panel3"</strong> из скрипта /class/engine_modules/demo/demo_panel.php</h2>
       <textarea class="view_code"><button cmd="demo_panel3" from="mod/demo">Проверить</button></textarea><br>
       <button cmd="demo_panel3"  from="mod/demo">Проверить</button>

       <h2>Вызов функции из модуля сайта. Будет вызвана функция <strong>"demo_panel4"</strong> из скрипта /class/engine_modules/demo/i_demo.php</h2>
       <textarea class="view_code"><button cmd="demo_panel4" from="mod/demo/i_demo.php">Проверить</button></textarea><br>
       <button cmd="demo_panel4" from="mod/demo/i_demo.php">Проверить</button>

       <h2>Вызов функции из расширения сайта. Будет вызвана функция <strong>"demo_panel5"</strong> из скрипта /class/ext/demo/demo_panel.php</h2>
       <textarea class="view_code"><button cmd="demo/demo_panel5">Проверить</button></textarea><br>
       <button cmd="demo/demo_panel5">Проверить</button>

       <h2>Вызов функции из расширения сайта. Будет вызвана функция <strong>"demo_panel6"</strong> из скрипта /class/ext/demo/i_demo.php</h2>
       <textarea class="view_code"><button cmd="demo/demo_panel6" from="i_demo.php">Проверить</button></textarea><br>
       <button cmd="demo/demo_panel6" from="i_demo.php">Проверить</button>
     <?
 }

 function test_buttons_1($options=array())
 {  ?><div class="info">Вызван метод <strong>"test_buttons_1"</strong> из класса текущей страницы</div>
      Переданные параметры:<?damp_array($options,1,-1)?><br><br>
      Данные POST:<?damp_array($_POST,1,-1)?><br><br>
      <button cmd="test_buttons">Вернуться в начало</button>
    <?
 }

  function test_panel()
  {
     echo 'function test_panel()' ;
  }

}



?>