/**
 * FancyUpload Showcase
 *
 * @license		MIT License
 * @author		Harald Kirschner <mail [at] digitarald [dot] de>
 * @copyright	Authors
 */

window.addEvent('domready', function()
{ // wait for the content
    var reffer=$('reffer').get('value') ;
    var mode=$('mode').get('value') ;
    var clss=$('clss').get('value') ;
    var up = new FancyUpload2($('upload-status'), $('upload-list'),
    {   // options object
		// we console.log infos, remove that in production!!
		verbose: true,
        appendCookieData:true,
		//data:{member_id:$('member_id').get('value'),trace_to_email:$('trace_to_email').get('checked')},
		// url is read from the form, so you just have to change one place
		url: $('form-upload').action,
        data: {reffer:reffer,mode:mode,clss:clss},

		// path to the SWF file
        path: _PATH_TO_ADMIN_ADDONS+'/fancyupload-v3.0.1/source/Swiff.Uploader.swf',
		
		// remove that line to select all files, or edit it, add more items
		//typeFilter: {'Images (*.jpg, *.jpeg, *.gif, *.png)': '*.jpg; *.jpeg; *.gif; *.png'},
		//typeFilter: {'Excel (*.xls, *.xlsx)': '*.xls; *.xlsx; *.gif; *.png'},

		// this is our browse button, *target* is overlayed with the Flash movie
		target: 'upload-browse',
		
		// graceful degradation, onLoad is only called if all went well with Flash
		onLoad: function()
        {   $('panel_fancyupload').removeClass('hide'); // we show the actual UI
			$('upload-status').removeClass('hide'); // we show the actual UI
			if ($('upload-fallback')!=null) $('upload-fallback').destroy(); // ... and hide the plain form
			// We relay the interactions with the overlayed flash to the link
			this.target.addEvents({click: function() {return false;},
				                   mouseenter: function() {this.addClass('hover');},
				                   mouseleave: function() {this.removeClass('hover');this.blur();},
				                   mousedown: function() {this.focus();}
			});

			// Interactions for the 2 other buttons
            // start upload
			//$('upload-upload').addEvent('click', function() { up.start(); return false;});
		},
        onStart:function()
        {
        },
		
		// Edit the following lines, it is your custom event handling
        onSelect: function() {
       		this.status.removeClass('status-browsing');
            up.start();
       	},

		
		/**
		 * Is called when files were not added, "files" is an array of invalid File classes.
		 * 
		 * This example creates a list of error elements directly in the file list, which
		 * hide on click.
		 */ 
		onSelectFail: function(files)
        {
            files.each(function(file)
              {	new Element('li',
                 {	'class': 'validation-error',
					html: file.validationErrorMessage || file.validationError,
					title: MooTools.lang.get('FancyUpload', 'removeTitle'),
					events: {click: function() {this.destroy();}
                 }
			  }).inject(this.list, 'top');
			}, this);
		},
		
		/**
		 * This one was directly in FancyUpload2 before, the event makes it
		 * easier for you, to add your own response handling (you probably want
		 * to send something else than JSON or different items).
		 */
		onFileSuccess: function(file, response) {
			var json = new Hash(JSON.decode(response, true) || {});

			if (json.get('status') == '1')
            {
				file.element.addClass('file-success');
				file.info.set('html', '<strong>Файл успешно загружен:</strong> ' + json.get('width') + ' x ' + json.get('height') + 'px, <em>' + json.get('mime') + '</em>)'+'<br>'+json.get('trace'));
                //$j.ajax({url:'ajax.php',dataType:'html',cache:false,success:update_obj_viewer_page_success,data:{cmd:'update_obj_viewer_page',reffer:reffer}});
                //document.getElementsByTagName('html').parentNode.getElementById('wrapper').innerHTML('aaa') ;
                //alert('wrapper:'+) ;
                //top.document.location='/admin/fra_viewer.php?reffer=9.56' ;
				//file.info.set('html', json.get('trace'));
                //$j('div#panel_fancyupload #upload-list li.file.file-success').css('background-image','url("'+json.get('img_url')+'") no-repeat;')  ;
			} else
            {
				file.element.addClass('file-failed');
				file.info.set('html', '<strong>Ошибка загрузки:</strong> ' + (json.get('error') ? (json.get('error') + ' #' + json.get('code')) : response));
			}
		},
		
		/**
		 * onFail is called when the Flash movie got bashed by some browser plugin
		 * like Adblock or Flashblock.
		 */
		onFail: function(error) {
			switch (error) {
				case 'hidden': // works after enabling the movie and clicking refresh
					alert('To enable the embedded uploader, unblock it in your browser and refresh (see Adblock).');
					break;
				case 'blocked': // This no *full* fail, it works after the user clicks the button
					alert('To enable the embedded uploader, enable the blocked Flash movie (see Flashblock).');
					break;
				case 'empty': // Oh oh, wrong path
					alert('A required file was not found, please be patient and we fix this.');
					break;
				case 'flash': // no flash 9+ :(
					alert('To enable the embedded uploader, install the latest Adobe Flash plugin.')
			}
		}
		
	});
});