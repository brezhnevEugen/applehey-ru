<?php
$options=array() ;
include_once($_SERVER['DOCUMENT_ROOT']."/ini/patch.php")  ;
include_once(_DIR_TO_ENGINE."/i_system.php") ;
include_once(_DIR_TO_ENGINE."/c_HELP.php") ;
class c_page_man extends c_HELP
{
  public $h1='Расширение mBox' ;
  public $title='Расширение mBox' ;
  public $path=array('/'=>'Главная') ;



  function block_main()
  {	$this->page_title() ;
    ?><h3>Параметры элементов</h3>
     <ul><li><strong>Элементом</strong> может кнопка, div или любой другой элемент.</li>
         <li>Элемент может как находиться внутри <strong>формы</strong> так и вне её</li>
         <li>В AJAX-запрос будут переданы все атрибуты элемента (div,button,span,img)</li>
     </ul>
     <p>Теги элемента:</p>
         <table class=params>
             <tr><td>class</td><td><ul><li>v1 - открытие модального окна</li>
                                       <li>v2 - выполнение AJAX-запроса</li>
                                   </ul></td>
             </tr>
             <tr><td>event</td><td><ul><li>click</li>
                                       <li>keyup</li>
                                    </ul>
                                    Если параметр <strong>event</strong> не указан, будет использовано событие событие <strong>click</strong>
                                </td>
             </tr>
             <tr><td>ext</td><td>Название расширения, в котором находится принимающий AJAX-запрос скрипт <strong>ajax.php</strong>. Если тег не указан, запрос будет отправлен в <strong>/class/c_ajax.php</strong></td>
             <tr><td>cmd</td><td>Команда для выполения скриптом. Будет вызван метод класса <strong>c_page_ajax</strong> c именем <strong>AJAX_...cmd...</strong> </td>
             <tr><td>validate</td><td><ul><li>form - к форме будет применен плагин mForm.Submit, производящий валидацию формы по определенным для элементам правилам</li>
                                      </ul>
                                  </td>
             </tr>
         </table>
    <h3>Содержимое ajax.php</h3>
    <table class=params>
         <tr><td>Действие после отработки AJAX-запроса</td>
             <td>$this->add_element('success','modal_window') ;<br>$this->add_element('success','.....') ;
                 Название функции JS которой будет вызвана после выполения AJAX запроса и которой будут переданы результаты запроса.
                                     <ul><li>modal_window - результат будет показан в модальном окне</li>
                     <li>show_notife - результат будет показан как всплывающее уведомление</li>
                                          <li>func_name - имя функции JS которой будут переданы результаты запроса</li>
                                     </ul>
                               </td>
             </tr>
        <tr><td>Перезагрузить страницу</td><td>$this->add_element('update_page',1) ;</td></tr>
        <tr><td>Перейти по заданному url</td><td>$this->add_element('go_url','.....') ;</td></tr>
        <tr><td>Закрыть предыдущее модальное окно</td><td>if ($result['type']!='error') $this->add_element('close_mBox_window','panel_id') ;<br><br><strong>panel_id</strong>- название шаблона, по которому было создано модальное окно</td></tr>

         <tr><td colspan=2><strong>ВЫВОД РЕЗУЛЬТАТА AJAX В МОДАЛЬНОЕ ОКНО</strong></td></tr>
         <tr><td>Создать модальное окно</td><td>$this->add_element('success','modal_window') ;</td></tr>
         <tr><td>Заголовок модального окна</td><td>$this->add_element('title','.....') ;</td></tr>
         <tr><td>Не показывать кнопку "Закрыть"</td><td>$this->add_element('no_close_button',1) ;</td></tr>
         <tr><td>Перезагрузить страницу после закрытия модального окна</td><td>$this->add_element('after_close_update_page',1) ;</td></tr>
         <tr><td>Перейти по заданному url после закрытия модального окна</td><td>$this->add_element('after_close_go_url','.....') ;</td></tr>
     </table>
    <h2>Кпопка для открытия модального окна из скрытой панели на странице</h2>
    <p>Панель, которая будет отображена в модальном окне, должна находиться в скрытом блоке</p>
     <div>
        <p><button class=v1 event=click panel_id=demo_modal_panel title="ПРОСТОЕ МОДАЛЬНОЕ ОКНО" modal_width=200>Открыть окно</button></p>
        <div class=hidden><div id=demo_modal_panel>Содержимое панели</div></div>
     </div>
     <textarea class=source_code></textarea>

    <h2>Кнопка вне формы для выполнения AJAX-запроса с проверкой валидности формы перед отправкой, результат отображается в модальном окне</h2>
    <p>В AJAX-запрос будут переданы все атрибуты кликнутого элемента (div,button,span,img)</p>
    <p>Для передачи резудьтатов в модальное окно у элемента должен быть установлен атрибут <strong>success=ajax_to_modal_window</strong></p>
    <p><button class="v2" ext=mBox cmd=demo_ajax_request success=ajax_to_modal_window>Выполнить запрос</button></p>
    <textarea class=source_code></textarea>

    <h2>Кнопка в форме для выполнения AJAX-запроса, результат отображается в модальном окне</h2>
    <p>В AJAX-запрос будут переданы данные формы в которой находиться элемент и все атрибуты кликнутого элемента</p>
    <p>Для передачи резудьтатов в модальное окно у элемента должен быть установлен атрибут <strong>success=ajax_to_modal_window</strong></p>
    <form>
        <input type=text name=data_1 value="Проверка">
        <label><input type=checkbox name=data_3 value="1" checked> test check</label>
        <br><br>
        <button class="v2" ext=mBox cmd=demo_ajax_request success=ajax_to_modal_window>Выполнить запрос</button>
    </form>
    <textarea class=source_code></textarea>

    <h2>Кнопка вне формы для выполнения AJAX-запроса, результат передается в JS-функцию</h2>
    <p><button class="v2" ext=mBox cmd=demo_ajax_request success=demo_ajax_request_success1>Выполнить запрос</button>
       <script type="text/javascript">
           function demo_ajax_request_success1(data) {alert_array(data)}
       </script>
    </p>
    <textarea class=source_code></textarea>

    <h2>Кнопка в форме для выполнения AJAX-запроса, результат передается в JS-функцию</h2>
    <form><input type=text name=data_1 value="Привет">
       <label><input type=checkbox name=data_3 value="1" checked> test check</label>
       <br><br>
       <button class="v2" ext=mBox cmd=demo_ajax_request success=demo_ajax_request_success2>Выполнить запрос</button>
       <script type="text/javascript">
           function demo_ajax_request_success2(data) {$j('div#ajax_result').html(data.html)}
       </script>
       <div id=ajax_result></div>
    </form>
    <textarea class=source_code></textarea>

    <h2>Проверка данных при вводе в текстовое поле через AJAX-запрос, результат передается в JS-функцию</h2>
    <form><input type=text name=data_1 class="v2" event="keyup" ext=mBox cmd=demo_ajax_check success=demo_ajax_request_success3><span id=ajax_result3></span>
       <script type="text/javascript">
           function demo_ajax_request_success3(data) {$j('span#ajax_result3').html('Длина текста - '+data.len)}
       </script>
    </form>
    <textarea class=source_code></textarea>

    <h2>Кнопка для AJAX-запроса, результат содержит JS-код</h2>
    <p>Необходимо иметь в виду, что JS-код будет отработан в следующих случаях:</p>
        <ul><li>Создание модального окна</li>
            <li>Замещение блока по ID</li>
            <li>Вывод JS-кода в тег "JSCODE"</li>
        </ul>
    <div id=test_demo_ajax_request_JS_code class=demo_1>Текст в блоке будет обновлен через JS-код после AJAX-запроса. Этим будет показано корректность выполнения JS кода, находящегося в ответе AJAX</div>
    <p><button class="v2" ext=mBox cmd=demo_ajax_request_JS_code>Выполнить запрос</button></p>
    <textarea class=source_code></textarea>

    <?

  }


}

$options['class_file']='this' ;
show_page($options) ;
?>