<?exit() ;
session_start();?>
<head>
    <title>Инсталяция движка</title>
    <META http-equiv=Content-Type content="text/html; charset=utf-8" />
    <META NAME="Document-state" CONTENT="Dynamic" />
    <META NAME="revizit-after" CONTENT="5 days" />
    <META name="revisit"  content="5 days" />
    <style type="text/css">
      body{font-family:arial;font-size:12px;color:#666666;}
      h1{font-size:16px;color:black;font-family:arial;border-bottom:2px solid green;}
      h2{font-size:15px;color:#555555;font-family:arial;}
      h3{font-size:13px;color:#006633;font-family:arial;}
      span.ok{color:green;font-weight:bold;}
      span.info{color:#6495ed;font-weight:normal;}
      span.error{color:red;font-weight:bold;}
      .green{color:green;}
      .right{text-align:right;}
      table{margin-left:20px;}  
      table td{padding:5px}
      input.text.big{width:400px;}
    </style>
 </head>
 <body>
<? // удаление всех директорий на сайте кроме движка
   // rm -r AddOns admin class images index.php ini public script.js search.php style.css .htaccess catalog news art support orders
   $patch=$_SERVER['SCRIPT_FILENAME'] ;
   $project=$_SERVER['SERVER_NAME'] ;
   $base_dir=$_SERVER['DOCUMENT_ROOT']  ;
   $list_dir=SETUP_get_dir_list($base_dir.'/');
   if (sizeof($list_dir)) foreach($list_dir as $dir_name)
   { preg_match_all('/engine_v(.*?)\//is',$dir_name,$matches,PREG_SET_ORDER) ;
     if (sizeof($matches) and $matches[0][1]) $engine_vers=$matches[0][1] ;
   }
   if (!$engine_vers) { echo 'Не уладось определить версию движка - его директория имеет некоррекное название<br>' ; return ; }
   $engine_dir='/engine_v'.$engine_vers ;
   $server_engine_vers=($engine_vers)? 'engine_v'.$engine_vers.'/':'engine/'  ;
   $document_root_slash='/' ;
   $dir_to_root=$_SERVER['DOCUMENT_ROOT'].$document_root_slash ;
   $dir_to_root_NS=$_SERVER['DOCUMENT_ROOT'] ;
   $dir_to_engine=$dir_to_root.$server_engine_vers ;
   $dir_to_modules=$dir_to_root.'class/engine_moduls/' ;

   if (file_exists($dir_to_root.'ini/patch.php')) include($dir_to_root.'ini/patch.php') ;
   include_once($dir_to_engine.'i_base.php') ;
   include($dir_to_engine.'i_descr_tables.php') ;
   include($dir_to_engine.'admin/i_setup.php') ;


   ini_set('display_errors',1) ;
   ini_set('display_startup_errors',1) ;
   error_reporting(E_ALL^E_NOTICE);

   define('_ENGINE_MODE','admin') ;
   $admin_dir='/admin/' ;
   $reboot_all=1 ;
   include($dir_to_engine.'i_system.php') ;
   include($dir_to_engine.'admin/c_site.php') ;

   // описание подключаемых модулей
   $list_modules['account']=array('m_account_admin.php','m_account.php') ;
   //$list_modules['catalog']=array('m_catalog_admin.php','m_catalog.php') ;
   $list_modules['forum']=array('m_forum_admin.php','m_forum.php') ;
   $list_modules['galerey']=array('m_galerey_admin.php','m_galerey.php') ;
   $list_modules['goods']=array('m_goods_admin.php','m_goods.php') ;
   $list_modules['makros']=array('m_makros_admin.php','m_makros.php')  ;
   $list_modules['news']=array('m_news_admin.php','m_news.php')  ;
   $list_modules['art']=array('m_art_admin.php','m_art.php')  ;
   $list_modules['orders']=array('m_orders_admin.php','m_orders_frames.php','m_orders.php')  ;
   //$list_modules['pars_price']=array('m_pars_price_admin.php','m_pars_price_frames.php','m_pars_price.php') ;
   //$list_modules['partners']=array('m_partners_admin.php','m_partners.php') ;
   $list_modules['poll']=array('m_poll_admin.php','m_poll.php') ;
   $list_modules['responses']=array('m_responses_admin.php','m_responses_frames.php','m_responses.php') ;
   $list_modules['robox']=array('m_robox_admin.php','m_robox.php') ;
   $list_modules['stats']=array('m_stats.php','m_stats_admin.php','m_stats_frames.php') ;
   $list_modules['support']=array('m_support_admin.php','m_support_frames.php','m_support.php') ;
   //$list_modules['tags_gloud']=array('m_tags_gloud_admin.php','m_tags_gloud.php') ;
   //$list_modules['yandex-market']=array('m_market_frames.php') ;



   ?><h1>Инсталяция сайта <?echo $project?> на движке 12-24:Управление сайтом (vers.<?echo $engine_vers?>)</h1><?
   if (!isset($step)) $step=0  ;
   switch($step)
   { case 0: SETUP_show_promt_info() ; break;
     case 1: SETUP_check_db_connect() ; break;
     case 2: SETUP_copy_files() ; break;
     case 3: SETUP_declare_moduls() ; break;
     case 4: SETUP_write_modules_to_patch() ; SETUP_create_sys_table() ; break ;
     case 5: SETUP_install_modules() ; break;
     case 6: SETUP_select_AddOns() ; break;
     case 7: SETUP_install_AddOns() ; break;
     case 8: SETUP_write_setting() ; break;
     case 9: SETUP_apply_write_setting() ; SETUP_finish() ; break;
   }

?></body><?

return ;


function SETUP_show_promt_info()
{  global $dir_to_root,$project ;
   ?><p>Сайт <strong><?echo $project?></strong> будет создан в директории <strong><?echo $dir_to_root?></strong></p><?
   ?><h2>Шаг 1: Проверка наличия прав для создания директорий и файлов</h2><?
   $check_dir_name='test2' ;
   $check_file_name='test.html' ;
   $res=0 ;
   ?><form action="setup.php" method="post">
      <table>
         <tr><td>Проверка создания директории</td><td><?SETUP_create_dir($check_dir_name);?></td><td><?if (is_dir($dir_to_root.$check_dir_name)) { echo  '<span class="ok">OK</span>' ; $res++ ; } else echo '<span class="error">ERROR - '.$dir_to_root.$check_dir_name.'</span>';?></td></tr>
         <tr><td>Создание файла</td><td><?SETUP_create_file($check_file_name,'test');?></td><td><?if (file_get_contents($dir_to_root.$check_file_name)=='test') { echo  '<span class="ok">OK</span>' ; $res++ ; } else echo '<span class="error">ERROR</span>';?></td></tr>
         <tr><td>Получение файла с сервера</td><td><?__SETUP_get_file('/root/style.css','/check_file.php');?></td><td><?if (is_file($dir_to_root.'check_file.php')) { echo  '<span class="ok">OK</span>' ; $res++ ;} else echo '<span class="error">ERROR</span>';?></td></tr>
         <?if ($res==3){?><tr><td colspan="3" class="right"><br><input name="step" value="1" type="hidden"><input type="submit" value="Далее"></td></tr><?}
         else          {?><tr><td colspan="3" class="right"><br><input name="step" value="0" type="hidden"><input type="submit" value="Повторить проверку"></td></tr><?}
         ?>
      </table><br>
   </form>
   <?
   rmdir($dir_to_root.$check_dir_name) ;
   unlink($dir_to_root.$check_file_name) ;
   unlink($dir_to_root.'check_file.php') ;
}

function SETUP_check_db_connect()
{ global $db_host,$db_name,$db_username,$db_password ;
  if (!$db_host) $db_host='localhost' ;
  if (!$db_username) $db_username='user' ;
  if (!$db_password) $db_password='user' ;
  $res=0 ;
  ?><h2>Шаг 2: Ввод данных для доступа к БД</h2>
     <form action="setup.php" method="post">
      <table>
         <tr><td>Сервер базы данных</td><td><input name="db_host" class="text" value="<?echo $db_host?>"></td></tr>
         <tr><td>Имя базы данных</td><td><input name="db_name" class="text" value="<?echo $db_name?>"></td></tr>
         <tr><td>Имя пользователя</td><td><input name="db_username" class="text" value="<?echo $db_username?>"></td></tr>
         <tr><td>Пароль</td><td><input name="db_password" class="text" value="<?echo $db_password?>"></td></tr>
         <tr><td></td><td></td></tr>
         <tr><td>Проверка подключения к БД</td><td><?if ($db_host and $db_name and $db_username and $db_password) $res=SETUP_connect_database(); else echo '---' ;?></td></tr>
         <?if ($res==1){?><tr><td colspan="2" class="right"><br><input name="step" value="2" type="hidden"><input type="submit" value="Далее"></td></tr><?}
         else          {?><tr><td colspan="2" class="right"><br><input name="step" value="1" type="hidden"><input type="submit" value="Проверить"></td></tr><?}
         ?>
      </table>
   </form>
   <?
}

function SETUP_copy_files()
{   global $db_host,$db_name,$db_username,$db_password,$engine_vers,$dir_to_root_NS,$admin_dir ;
    $cont_patch=file_get_contents(_PATH_TO_SERVER_SETUP_ALT.'/ini/patch.php') ;
    $cont_patch=str_replace('**db_host**',$db_host,$cont_patch) ;
    $cont_patch=str_replace('**db_name**',$db_name,$cont_patch) ;
    $cont_patch=str_replace('**db_user**',$db_username,$cont_patch) ;
    $cont_patch=str_replace('**db_pass**',$db_password,$cont_patch) ;
    $cont_patch=str_replace('**engine_vers**',$engine_vers,$cont_patch) ;
    $cont_patch=str_replace('**TM_code**','site',$cont_patch) ;
    $cont_patch=str_replace('**DRS**','/',$cont_patch) ;
    $cont_patch=str_replace('**IP**',$_SERVER['REMOTE_ADDR'],$cont_patch) ;
    $cont_htaccess=file_get_contents(_PATH_TO_SERVER_SETUP_ALT.'/admin/htaccess.txt') ;
    $cont_htaccess=str_replace('**htpasswd**',$dir_to_root_NS.'/admin/.htpasswd',$cont_htaccess) ;
    $res=1 ;
    ?><h2>Шаг 3: Создаем директории сайта и копируем необходимые файлы</h2>
       <form action="setup.php" method="post">
        <table>
           <tr><td>Каталог админки</td><td>     <?SETUP_create_dir('/admin') ;?></td></tr>
           <tr><td>Каталог конфигурации</td><td><?SETUP_create_dir('/ini') ;?></td></tr>
           <tr><td>Каталог шаблонов страниц сайта</td><td><?SETUP_create_dir('/class') ;?></td></tr>
           <tr><td>Каталог шаблонов списков сайта</td><td><?SETUP_create_dir('/class/templates') ;?></td></tr>
           <tr><td>Каталог изображений для страниц сайта</td><td><?SETUP_create_dir('/images') ;?></td></tr>
           <tr><td>Каталог для загрузки файлов</td><td><?SETUP_create_dir('/public') ;?></td></tr>
           <tr><td>Каталог плагинов</td><td><?SETUP_create_dir('/AddOns') ;?></td></tr>
           <tr><td>Каталог модулей</td><td><?SETUP_create_dir('/class/engine_moduls') ;?></td></tr>
           <tr><td>Файл конфигурации сервера для админки</td><td><?SETUP_create_file($admin_dir.'.htaccess',$cont_htaccess) ;?></td></tr>
           <tr><td>Файл паролей для админки</td><td><?__SETUP_get_file($admin_dir.'htpasswd.txt',$admin_dir.'.htpasswd');?></td></tr>
           <tr><td>Главный шаблон сайта</td><td><?__SETUP_get_file('/class/c_page.php','/class/c_page.php') ;?></td></tr>
           <tr><td>Шаблон главной страницы сайта</td><td><?__SETUP_get_file('/class/c_index.php','/class/c_index.php') ;?></td></tr>
           <tr><td>Шаблон карты сайта</td><td><?__SETUP_get_file('/class/c_sitemap.php','/class/c_sitemap.php') ;?></td></tr>
           <tr><td>Шаблон страницы поиска по сайту</td><td><?__SETUP_get_file('/class/c_search.php','/class/c_search.php') ;?></td></tr>
           <tr><td>Шаблон панелей страниц</td><td><?__SETUP_get_file('/templates_other/panels.php','/class/templates/panels.php') ;?></td></tr>
           <tr><td>Шаблон панелей страниц</td><td><?__SETUP_get_file('/templates_other/page_path.php','/class/templates/page_path.php') ;?></td></tr>
           <tr><td>Скрипт для расширений классов сайта</td><td><?__SETUP_get_file('/class/mc_ext_moduls.php','/class/mc_ext_moduls.php') ;?></td></tr>
           <tr><td>Стартовые настройки сайта</td><td><?SETUP_create_file('/ini/patch.php',$cont_patch) ;?></td></tr>
           <tr><td>Конфигурация сайта</td><td><?__SETUP_get_file('/ini/init.php','/ini/init.php') ;?></td></tr>
           <tr><td>Файл конфигурации сервера для сайта</td><td><?__SETUP_get_file('/root/htaccess.txt','/.htaccess') ;?></td></tr>
           <tr><td>Файл стилей CSS сайта</td><td><?__SETUP_get_file('/root/style.css','/style.css') ;?></td></tr>
           <tr><td>Файл скриптов JS сайта</td><td><?__SETUP_get_file('/root/script.js','/script.js') ;?></td></tr>
           <tr><td>Главная страница сайта</td><td><?__SETUP_get_file('/root/index.php','/index.php') ;?></td></tr>
           <tr><td>Карта сайта</td><td><?__SETUP_get_file('/root/sitemap.php','/sitemap.php') ;?></td></tr>
           <tr><td>Cтраница поиска по сайту</td><td><?__SETUP_get_file('/root/search.php','/search.php') ;?></td></tr>
           <tr><td>Cтраница быстрого редактирования</td><td><?__SETUP_get_file('/root/fastedit.php','/fastedit.php') ;?></td></tr>
           <tr><td>Скрипт для генерации капчи</td><td><?__SETUP_get_file('/images/img_check_code.php','/images/img_check_code.php') ;?></td></tr>
           <tr><td>Шрифт для генерации капчи</td><td><?__SETUP_get_file('/images/check_font.ttf','/images/check_font.ttf') ;?></td></tr>
           <tr><td></td><td></td></tr>
           <?if ($res==1){?><tr><td colspan="2" class="right"><br><input name="step" value="3" type="hidden"><input type="submit" value="Далее"></td></tr><?}
           else          {?><tr><td colspan="2" class="right"><br><input name="step" value="2" type="hidden"><input type="submit" value="Повторить"></td></tr><?}
           ?>
        </table>
     </form>
     <?
}

function SETUP_declare_moduls()
{ global $dir_to_engine,$list_modules ;
  $res=0 ;
  ?><h2>Шаг 4: Выбор модулей сайта</h2>
    <p>Отметьте необходимые Вам модули и нажмите "далее"</p>
     <form action="setup.php" method="post">
      <table>
         <?if (sizeof($list_modules)) foreach($list_modules as $modul_name=>$modul_info)
           {?><tr><td><input type="checkbox" name='use_modul[]' value="<?echo $modul_name?>"></td><td><?echo $modul_name?></td></tr><?}
         ?>
         <tr><td></td><td></td></tr>
         <tr><td colspan="2" class="right"><br><input name="step" value="4" type="hidden"><input type="submit" value="Далее"></td></tr>
      </table>
   </form>
   <?
}

function SETUP_write_modules_to_patch()
{ global $dir_to_root,$use_modul,$list_modules ;
  SETUP_create_dir('/class/engine_moduls') ;
  if (sizeof($use_modul)) foreach($use_modul as $modul_name)
  {   SETUP_create_dir('/class/engine_moduls/'.$modul_name) ;
      // пока пишем вручную, потом это будет скачиваться
      if (sizeof($list_modules[$modul_name])) foreach($list_modules[$modul_name] as $file_name) __SETUP_get_file('/'.$modul_name.'/'.$file_name,'/class/engine_moduls/'.$modul_name.'/'.$file_name) ;
      $str[]='$use_modules[]=\''.$modul_name.'\';'  ;
  }

  if (sizeof($str)) $text=implode("\n  ",$str) ; else $text='' ;
  $cont=file_get_contents($dir_to_root.'ini/patch.php') ;
  $cont=str_replace('//**modules_declare**',$text,$cont) ;
  file_put_contents($dir_to_root.'ini/patch.php',$cont) ;
}

function SETUP_create_sys_table()
{ global $dir_to_engine ;
  ?><h2>Шаг 5: Создаем базовые таблицы</h2><?
  connect_database() ;   // законектиться к базе
  $options=array('install_mode'=>1) ;
  session_boot($options) ;
  create_sys_table() ;
  ?><form action="setup.php" method="post"><input name="step" value="5" type="hidden"><input type="submit" value="Далее"></form><?
}

function SETUP_install_modules()
{ ?><h2>Шаг 6: Инсталируем модули движка</h2><?
  connect_database() ;   // законектиться к базе
  $options=array('install_mode'=>1) ;
  session_boot($options) ;
  // damp_array(_DOT(25)) ;
  include_extension('install_modules') ;
  install_modules_apply() ;
  ?><form action="setup.php" method="post"><input name="step" value="6" type="hidden"><input type="submit" value="Далее"></form><?
}

function SETUP_select_AddOns()
{ ?><h2>Шаг 7: Выбираем внешние плагины</h2><?
  connect_database() ;   // законектиться к базе
  $options=array('install_mode'=>1) ;
  session_boot($options) ;
  //install_modules() ;
  ?><form action="setup.php" method="post"><?
  $cont=file_get_contents(_PATH_TO_SERVER_SETUP_ALT.'/AddOns/') ;
  $arr=explode("\n",strip_tags($cont)) ;
  //damp_array($arr,1,0) ;
  ?><table><?
  if (sizeof($arr)) foreach($arr as $i=>$rec) if (strpos($rec,'Index of /setup/AddOns')===false and strpos($rec,'Parent Directory')==false and trim($rec)!='')
  {?><tr><td><input type="checkbox" name='use_AddOns[<?echo $i?>]' value="<?echo trim($rec)?>"></td><td><?echo trim($rec)?></td></tr><?}
  ?></table><?
  ?><input name="step" value="7" type="hidden"><input type="submit" value="Далее"></form><?
}

function SETUP_install_AddOns()
{ global $use_AddOns ;
  ?><h2>Шаг 8: Инсталируем внешние плагины</h2><?
  connect_database() ;   // законектиться к базе
  $options=array('install_mode'=>1) ;
  session_boot($options) ;
  //damp_array($use_AddOns) ;
  install_AddOns($use_AddOns) ;
  ?><form action="setup.php" method="post"><input name="step" value="8" type="hidden"><input type="submit" value="Далее"></form><?
}

function SETUP_write_setting()
{ connect_database() ;   // законектиться к базе
  $options=array('install_mode'=>1) ;
  session_boot($options) ;
  $vars=execSQL('select * from '.TM_SETTING.' where clss=99') ;
  ?><h2>Шаг 9: Задание информации о сайте</h2>
   <form action="setup.php" method="post">
        <table>
        <?if (sizeof($vars)) foreach($vars as $name=>$rec){?><tr><td><?echo $rec['obj_name']?></td><td><input name="vars[<?echo $rec['pkey']?>]" class="big text" value="<?echo addslashes(htmlspecialchars($rec['value']))?>"></td></tr><?}?>
        <tr><td colspan="2" class="right"><input name="step" value="9" type="hidden"><input type="submit" value="Сохранить"></td></tr>
        </table>
     </form>
  <?
}

function SETUP_apply_write_setting()
{ connect_database() ;   // законектиться к базе
  $options=array('install_mode'=>1) ;
  session_boot($options) ;
  global $vars ;
  if (sizeof($vars)) foreach($vars as $pkey=>$value) update_rec_in_table(TM_SETTING,array('value'=>$value),'pkey='.$pkey) ;
}

function SETUP_finish()
{  global $admin_dir ;
   ?><h2>Ваш сайт готов к работе!</h2>
   <p>Вы можете <a href='/index.php'>перейти на сайт</a> или <a href='<?echo $admin_dir?>'>Перейти в панель управления сайтом</a> (логин - "admin", пароль не требуется)</p>
<?
}

function SETUP_create_dir($dir_name)
{ global $dir_to_root ;
  $abs_dir_name=$dir_to_root.$dir_name ;
  if (is_dir($abs_dir_name)) echo 'Директория <strong>'.$dir_name.'</strong> уже существует<br>' ;
  else if (mkdir($abs_dir_name,_CREATE_DIR_PERMS)) echo 'Создана директория <strong>'.$dir_name.'</strong><br>' ; else echo 'Не удалось создать директорию <strong>'.$dir_name.'</strong><br>' ;
}

function SETUP_create_file($file_name,$cont)
{ global $dir_to_root ;
  $abs_file_name=$dir_to_root.$file_name ;
  if (is_file($abs_file_name)) echo 'Файл <strong>'.$file_name.'</strong> уже существует<br>' ;
  else if (file_put_contents($abs_file_name,$cont)) echo 'Создан файл <strong>'.$file_name.'</strong><br>' ; else echo 'Не удалось создать файл <strong>'.$file_name.'</strong><br>' ;
}

function SETUP_connect_database($reset_link=0)
{ global $db_host,$db_name,$db_username,$db_password,$Link,$database ;
     if ($reset_link) mysql_close() ; //else if ($Link) return ;
     $database = @mysql_connect($db_host, $db_username, $db_password);
     @mysql_query('SET CHARSET utf8');
     @mysql_query('SET NAMES utf8');

     if (!$database) { echo "<span class=error>Невозможно подключиться к серверу MySQL</span>"; return ; }
     if (!($Link = @mysql_select_db($db_name))) { echo "<span class=error>Не найдена БД '$db_name'</span>"; return ; }

     echo '<span class=OK>OK</span>' ;
     return(1);
}

// получение файла из инсталяционного каталога сервера _PATH_TO_SERVER_SETUP_ALT
// $dir_dest - адрес файла : /root/index.php
// $dir_descr - имя файла назначения, с относителбным путем отн.корня сайта : /index.php
function __SETUP_get_file($dir_source,$dir_dest,$copy_css_js=0)
{ global $dir_to_root_NS,$install_options ;
  if (sizeof($install_options) and !$install_options['install_copy_script'] and !$install_options['install_copy_css_js']) return ;
  $cont=@file_get_contents(_PATH_TO_SERVER_SETUP_ALT.$dir_source) ;
  //list($cont,$error)=get_page_by_sock('www.12-24.ru','/setup'.$dir_source,array('IP'=>$engine_server_IP,'___charset'=>'utf-8')) ;
  //$cont=http_request('GET', 'www.12-24.ru', 80, '/setup'.$dir_source);
  //echo 'error='.$error.'<br>' ;
  //echo $dir_source.'='.strlen($cont).'<br>'.$cont.'<br>' ;
  $abs_dir_desct=$dir_to_root_NS.$dir_dest ;
  $abs_dir=$dir_to_root_NS.dirname($dir_dest) ;
  if (!strlen($cont)){echo '<span class=error>Не удалось получить файл</span> <strong>'.$dir_source.'</strong><br>' ; return ; }
  if ($abs_dir and !is_dir($abs_dir)) {echo '<span class=error>Директория назначения </span><strong>/'.$abs_dir.'</strong> не существует<br>' ; return ; }
  if (file_exists($abs_dir_desct)){echo '<span class=info>Файл <strong>'.$dir_dest.'</strong></span> уже существует<br>' ; return ; }
  if (!file_put_contents($abs_dir_desct,$cont)) { echo '<span class=error>Не удалось получить файл </span><strong>'.$dir_source.'</strong> в <strong>'.$abs_dir_desct.'</strong><br>' ; return(0) ;}
  echo 'Файл <strong>'.basename($dir_dest).'</strong> успешно скопирован в <strong>'.$dir_dest.'</strong><br>' ;
  if (!$copy_css_js) return(1) ;
  $dir_source=str_replace('.php','.css',$dir_source) ;
  __setup_append_file($dir_source,'/style.css') ;
  $dir_source=str_replace('.css','.js',$dir_source) ;
  __setup_append_file($dir_source,'/script.js') ;
  return(1) ;
}

// получение файла модуля из депозитария модулей
// $dir_dest - адрес файла : /root/index.php
// $dir_descr - имя файла назначения, с относителбным путем отн.корня сайта : /index.php
function SETUP_get_modul($dir_source,$dir_dest)
{ global $dir_to_root_NS ;
  //if (sizeof($install_options) and !$install_options['install_copy_script'] and !$install_options['install_copy_css_js']) return ;
  $cont=@file_get_contents(_PATH_TO_SERVER_SETUP_ALT.$dir_source) ;
  //list($cont,$error)=get_page_by_sock('www.12-24.ru','/setup'.$dir_source,array('IP'=>$engine_server_IP,'___charset'=>'utf-8')) ;
  //$cont=http_request('GET', 'www.12-24.ru', 80, '/setup'.$dir_source);
  //echo 'error='.$error.'<br>' ;
  //echo $dir_source.'='.strlen($cont).'<br>'.$cont.'<br>' ;
  $abs_dir_desct=$dir_to_root_NS.$dir_dest ;
  $abs_dir=$dir_to_root_NS.dirname($dir_dest) ;
  if (!strlen($cont)){echo '<span class=error>Не удалось получить файл</span> <strong>'.$dir_source.'</strong><br>' ; return ; }
  if ($abs_dir and !is_dir($abs_dir)) {echo '<span class=error>Директория назначения </span><strong>/'.$abs_dir.'</strong> не существует<br>' ; return ; }
  if (file_exists($abs_dir_desct)){echo '<span class=info>Файл <strong>'.$dir_dest.'</strong></span> уже существует<br>' ; return ; }
  if (!file_put_contents($abs_dir_desct,$cont)) { echo '<span class=error>Не удалось получить файл </span><strong>'.$dir_source.'</strong> в <strong>'.$abs_dir_desct.'</strong><br>' ; return(0) ;}
  echo 'Файл <strong>'.basename($dir_dest).'</strong> успешно скопирован в <strong>'.$dir_dest.'</strong><br>' ;
  if (!$copy_css_js) return(1) ;
  $dir_source=str_replace('.php','.css',$dir_source) ;
  __setup_append_file($dir_source,'/style.css') ;
  $dir_source=str_replace('.css','.js',$dir_source) ;
  __setup_append_file($dir_source,'/script.js') ;
  return(1) ;
}

function __setup_append_file($dir_source,$dir_dest)
{ global $dir_to_root_NS ;
  $cont=@file_get_contents(_PATH_TO_SERVER_SETUP_ALT.$dir_source) ;
  if (!$cont){/*echo 'Файл <strong>'.$dir_source.'</strong> не существует<br>' ;*/ return ; }
  $abs_dir_desct=$dir_to_root_NS.$dir_dest ;
  $abs_dir=$dir_to_root_NS.dirname($dir_dest) ;
  $cont2=@file_get_contents($abs_dir_desct) ;
  $header="\n/*Добавлено автоматически из файла ".basename($dir_source)."*/\n" ;
  if ($abs_dir and !is_dir($abs_dir)) {echo 'Директория назначения <strong>/'.$abs_dir.'</strong> не существует<br>' ; return ; }
  if (file_put_contents($abs_dir_desct,$cont2.$header.$cont)) { echo 'Файл <strong>'.basename($dir_source).'</strong> успешно добавлен в <strong>'.$dir_dest.'</strong><br>' ; return(1) ;}
  else echo 'Не удалось изменить файл <strong>'.$dir_source.'</strong> в <strong>'.$abs_dir_desct.'</strong><br>' ;
}

function SETUP_get_dir_list($dir)
{ $list=array() ;
  $list_files=scandir($dir) ;
    foreach($list_files as $file_name) if ($file_name!='.' and $file_name!='..' and $file_name!='__MACOSX' and is_dir($dir.$file_name)) $list[$file_name]=$dir.$file_name.'/' ;
  return($list) ;
}




?>