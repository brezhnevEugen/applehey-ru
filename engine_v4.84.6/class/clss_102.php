<?
include_once('clss_0.php') ;
class clss_102 extends clss_0
{

 // $options['cur_obj] - запись по текущему объекту
 function show_list_items($table_id,$usl,$options=array())
 { $cur_pkey=$options['cur_obj']['pkey'] ;
   $cur_tkey=$options['cur_obj']['tkey'] ;

   $options['cur_obj_pkey']=$cur_pkey ;
   $options['cur_obj_tkey']=$cur_tkey ;

   $res_arr=get_arr_links($cur_pkey,$cur_tkey,array('all'=>1,'table_id'=>$table_id)) ; // берем все ликнеи, включая выключенные
   //damp_array($res_arr) ;
   $arr_ids=array() ;
   if (sizeof($res_arr))
   { foreach($res_arr as $rec) $arr_ids[$rec['link_clss']][]=$rec['id'] ;
     //damp_array($arr_ids) ;
     $i=1 ;
     foreach($arr_ids as $clss=>$ids)
     { $usl_ids='pkey in ('.implode(',',$ids).')' ;
       $options['title']='Линки на '._CLSS($clss)->name  ;
       $options['buttons']=($i==sizeof($arr_ids))? array('save','delete'=>array('title'=>'Удалить линк','cmd'=>'delete_check_objs','data-confirm'=>"Вы действительно хотите удалить выбранные объекты?")):array()  ;
       parent::show_list_items($table_id,$usl_ids,$options) ;
       ?><?
       $i++ ;
     }
   }
 }

 // свойства объекта-ссылки
 function show_props($obj_info,$options=array()) // необходимо показать объект функцией того класса, к которому принадлежит объект
  { $ref_info=select_db_obj_info($obj_info['o_tkey'],$obj_info['o_id'],array('no_child_obj'=>1)) ;
	if ($ref_info['pkey'])  _CLSS($ref_info['clss'])->show_props($ref_info,$options) ;
	else echo 'Объект не существует' ;
  }

 // выделено в функцию чтобы различные классы могли сами изменять содержимое значка редактирования
 function get_edit_td($rec,$options=array())
 { $edit_td=(!$options['read_only'] and !$options['no_icon_edit'] and !$options['_small'])? '<td class=to_edit link="'.$rec['o_id'].'.'.$rec['o_tkey'].'"></td>':'' ;
   return($edit_td) ;
 }

 function VIEW_FIELD_obj_name($rec,$options=array())
 { global $cur_link_obj ;
   //damp_array($rec) ;
   //damp_array($options) ;
   $pkey=0 ; $tkey=0 ;$clss=0 ; $obj_name='' ;
   if ($rec['o_id']==$options['cur_obj_pkey'] and $rec['o_tkey']==$options['cur_obj_tkey']) { $pkey=$rec['p_id'] ; $tkey=$rec['p_tkey'] ;  $clss=$rec['p_clss'] ;}
   if ($rec['p_id']==$options['cur_obj_pkey'] and $rec['p_tkey']==$options['cur_obj_tkey']) { $pkey=$rec['o_id'] ; $tkey=$rec['o_tkey'] ;  $clss=$rec['o_clss'] ;}
   if ($pkey and $clss)
   {  $cur_link_obj=execSQL_van('select * from '._DOT($tkey)->table_name.' where pkey='.$pkey) ;
      //$obj_name=_CLSS($cur_link_obj['clss']->name)->name($cur_link_obj['_parent_reffer']) ;  // сложная функция, большая нагрузка
      $obj_name='<a href="fra_viewer.php?obj_reffer='.$pkey.'.'.$tkey.'">'.$cur_link_obj['obj_name'].'</a>' ;

   }
   return($obj_name) ;
 }

 function VIEW_FIELD_path($rec,$options=array())
 { global $cur_link_obj ;
   $text=get_obj_patch($cur_link_obj) ;
   return($text) ;
 }

}

?>