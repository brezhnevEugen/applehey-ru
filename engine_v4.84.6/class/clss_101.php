<? include_once(_DIR_TO_ENGINE.'/admin/i_setup.php') ;

class clss_101 extends clss_0
{
  function obj_create_dialog()
   {  $form_id='form_obj_create_dialog_clss_'.$this->clss ;
      $set_table_base='obj_'.SITE_CODE.'_' ;
       ?><form method="POST" action="ajax.php" id=<?echo $form_id?>><table>
               <tr class=clss_header><td colspan=3>Создание объектной таблицы</td></tr>
               <tr class=td_header>
                     <td>Наименование</td>
                     <td>Имя таблицы</td>
                     <td>Список поддерживаемых классов<br>(через запятую)</td>
               </tr>
               <tr> <td><input name="set_table_title" id=set_table_title type=text class=text  value="" size=30></td>
                     <td><?echo $set_table_base?><input name="set_table_suff" id=set_table_suff type=text class=text  value="" size=30></td>
                     <td><input name="set_table_clss" id=set_table_clss type=text class=text  value="" size=30></td>
                </tr>
             </table>
             <input name="set_table_base" type="hidden" value="<?echo $set_table_base?>">
             <!--<input name="inc_img" type="checkbox" value="1"> Добавить поддержку фото &nbsp;&nbsp;&nbsp;&nbsp;
             <input name="inc_file" type="checkbox" value="1"> Добавить поддержку файлов<br><br>-->
             <button class=button_green mode=append_viewer_main cmd=create_obj_after_dialog clss="101" parent_reffer="<?echo $_POST['parent_reffer']?>">Создать</button>
             <br/>
         </form>
       <?
       return(0) ; // далеее не производить вывод по пункту меню
   }

 // выполнение создания объекта по результатам диалога
  function create_obj_after_dialog()
  { // создание объектной таблицы
    // set_table_title: заголовок таблицы
    // set_table_base: базовая часть имени таблицы
    // set_table_suff: изменяемая часть имени таблицы
    // set_table_clss: классы, поддерживаемые аблицей
    $_POST['set_table_suff']=safe_file_names($_POST['set_table_suff']) ;
    if (!$_POST['set_table_clss']) $_POST['set_table_clss']='1' ;
    $def_recs[]=array('parent'=>0,'clss'=>1,'enabled'=>1,'obj_name'=>'Корень каталога','indx'=>1);
    list($parent_id,$tkey)=explode('.',$_POST['parent_reffer']) ;
    $new_table_options=array(   'table_parent'  =>$parent_id,
                                'table_clss'    =>101,
                                'table_title'   =>$_POST['set_table_title'],
                                'table_name'    =>$_POST['set_table_base'].$_POST['set_table_suff'],
                                'use_clss'      =>$_POST['set_table_clss'],
                                'def_recs'      =>$def_recs
                             ) ;
    $new_table_id=create_DOT_object_table($new_table_options) ;
    if ($new_table_id) echo '<span class=green>Создание таблицы завершено</span>' ;
    if ($_POST['inc_img'])
    {

    }
    return('ok') ;
  }

 // реакция на событие удаления объекта - у каждого класса должно выражаться по своему
 function on_delete_event($obj_info,$options=array())
  { if (!$options['debug']) delete_object_table($obj_info['pkey']) ;
    parent::on_delete_event($obj_info,$options) ;
  }

 function db_damp()
 { $cur_table=_DOT(_CUR_PAGE()->obj_info['pkey']) ;
   ?><h2>Дамп таблицы <?echo $cur_table->table_name?> (последние 400 строк)</h2><?
   $damp=execSQL('select * from '.$cur_table->table_name.' order by pkey desc limit 400',array('no_indx'=>1)) ;
   print_2x_arr($damp) ;
 }

// показываем реальные поля базы данных
 function db_info()
 { global $pkey,$obj_info ;
    ?><h2>Описание создания таблицы</h2><?
    $field_info=execSQL("SHOW CREATE TABLE ".$obj_info['table_name']) ;
    echo nl2br($field_info[$obj_info['table_name']]['Create Table']) ;

    $field_info=execSQL("DESCRIBE ".$obj_info['table_name']) ;
    //damp_array($field_info) ;

    ?><h2>Описание полей таблицы</h2><?

    ?><table>
        <tr class=clss_header>
	            <td class="table_ff" align="left">Поле таблицы</td>
	    		<td class="table_ff" align="center">Тип поля</td>
	    		<td class="table_ff" align="left">Код, имя класса, назначение поля</td>
	    </tr>
	<?
    if (sizeof($field_info)) foreach ($field_info as $fname=>$finfo)
    {
	    ?><tr>
	            <td><? echo $fname ; ?></td>
	    		<td><? echo $finfo['Type'] ; ?></td>
	    		<td>
	    		   <table><?
	    		   // смотрим описания полей для тех объектов, которые состоят в списке поддерживаемых классов
	    		   if (sizeof(_DOT($pkey)->list_clss)) foreach (_DOT($pkey)->list_clss as $pkey_clss=>$tkey_clss) if ($tkey_clss==$pkey)
	    		   { $obj_clss=_CLSS($pkey_clss);
                     if (isset($obj_clss->fields[$fname]))
	    		      { ?><tr>
	    		           <td>(<? echo $obj_clss->clss?>) <? echo $obj_clss->name ; ?></td>
	    		           <td><? echo $obj_clss->fields[$fname] ; ?></td>
	    		         </tr><?
	    		      }
	    		   }
	    		   ?></table>
	          </td>
	    </tr>
	 <?}?>
    </table><?
  }
}


 // состав объектной таблицы
 function clss_101_sostav()
 { global $pkey ;
   upload_use_clss_info($pkey,1) ;     // обновляем информацию по составу классов
   //upload_use_img_clone_info($pkey) ;  // обновляем информацию по составу клонов изображений
   check_obj_table_image_dir($pkey) ;  // проверяем папки изображений
   check_obj_table_file_dir($pkey) ;   // проверяем папки файлов

   _CLSS(101)->show_list_children($GLOBALS['obj_info']) ;  // показ состава
 }

 //-----------------------------------------------------------------------------------------------------------------------------
 // параметры объектной таблицы
 //-----------------------------------------------------------------------------------------------------------------------------

 function clss_101_show_params($cur_tkey=0)
 { global $pkey,$tkey,$obj_info,$cur_table ;
    // перегружаем данные по клонам - перегружаем движок
    reboot_engine() ;

    if (!$cur_tkey or is_array($cur_tkey)) $cur_tkey=$pkey ;
    $cur_table=$_SESSION['descr_obj_tables'][$cur_tkey] ;

	check_obj_table_image_dir($cur_tkey) ;
	check_obj_table_file_dir($cur_tkey) ;

    $show_img_pars=0 ; $show_file_pars=0 ;

    if (sizeof($cur_table->list_clss)) foreach($cur_table->list_clss as $clss=>$clss_tkey)
     { //echo 'clss='.$clss.'<br>' ;
       $obj_clss=_CLSS($clss);
       if ($obj_clss->has_parent_clss(3) and $cur_table->list_clss[$clss]==$cur_tkey) $show_img_pars=1 ;
       if ($obj_clss->has_parent_clss(5) and $cur_table->list_clss[$clss]==$cur_tkey) $show_file_pars=1 ;
     }


    $table_name=$cur_table->table_name ;
    $cnt_foto_obj=execSQL_value('select count(pkey) as cnt from '.$table_name.' where clss=3') ;
    $cnt_foto_obj_space=execSQL_value('select count(pkey) as cnt from '.$table_name.' where clss=3 and obj_name="" or obj_name is null') ;
    $arr_foto_obj_dubl=execSQL_row('select obj_name,count(pkey) as cnt from '.$table_name.' where clss=3 group by obj_name having cnt>1') ;
    //$cnt_foto_obj_dubl=array_sum($arr_foto_obj_dubl) ;
    $cnt_foto_obj_dubl=sizeof($arr_foto_obj_dubl) ;



    $update_parent_enabled_mode=$cur_table->setting['update_parent_enabled'];   if (!$update_parent_enabled_mode) $update_parent_enabled_mode='none' ;
    $set_image_name=$cur_table->setting['set_image_name'];                      if (!$set_image_name) $set_image_name='subselect' ;

    ?><table class=left>
	           <tr class=clss_header><td colspan=2 >Параметры</td></tr>
	           <tr class=td_header>
  				    <td>Наименование</td>
	     			<td>Значение</td>
	           </tr>
               <?if ($cur_table->mode=='main'){?>
               <tr>
                 <td>update parent_enabled</td>
                 <td><input type=radio name=update_parent_enabled_mode value=none <?if ($update_parent_enabled_mode) echo 'checked'?>>None<br>
                     <input type=radio name=update_parent_enabled_mode value=php <?if ($update_parent_enabled_mode=='php') echo 'checked'?>>PHP function<br>
                     <input type=radio name=update_parent_enabled_mode value=sql <?if ($update_parent_enabled_mode=='sql') echo 'checked'?>>MySQL procedure<br>
                 </td>
               </tr><?}?>
               <?if ($cur_table->list_clss[3]){?>
               <tr>
                 <td>set _image_name</td>
                 <td><input type=radio name=set_image_name value=subselect <?if ($set_image_name=='subselect') echo 'checked'?>>subselect<br>
                     <input type=radio name=set_image_name value=sql <?if ($set_image_name=='sql') echo 'checked'?>>MySQL procedure<br>
                 </td>
               </tr>
	           <?}

               if ($show_img_pars)
	           {
                 $dir_name=($cur_table->dir_name)? $cur_table->dir_name:$cur_table->patch_to_file ;
                 $cnt_source=sizeof(scandir($cur_table->dir_to_file.'source/'))-2 ;


                 ?><tr><td>Каталог изображений (относительной HTTP путь)</td><td><?echo _PATH_TO_SITE.' '.generate_element('text',60,$tkey,$obj_info['clss'],$cur_tkey,'dir_to_image',$dir_name) ;?></td></tr>
		           <tr><td>Набор клонов</td><td><select size="1" name="obj[<?echo $tkey?>][<?echo $obj_info['clss']?>][<?echo $cur_tkey?>][img_pattern]"><option value=""></option><?if (sizeof($_SESSION['img_pattern'])) foreach($_SESSION['img_pattern'] as $name=>$arr) if ($name!='no_photo'){?><option value="<? echo $name?>" <?if ($cur_table->img_pattern==$name) echo 'selected'?>><?echo $name?></option><?}?></select>
		           			<? /*if ($cur_table->img_pattern) { echo '<br><br>' ;damp_array($cur_table->list_clone_img()) ; } */?>
		           	   </td>
		           </tr>
		           <tr><td>Путь к фото</td><td><?echo $cur_table->dir_to_file?></td></tr>
		           <tr><td>Адрес к фото</td><td><?echo $cur_table->patch_to_file?></td></tr>
		           <tr><td>Создано объектов фото<br><br>
                           <span class="green bold"><? echo $cnt_foto_obj?></span> объектов фото<br>
                           <span class="green bold"><? echo $cnt_foto_obj-$cnt_foto_obj_dubl-$cnt_foto_obj_space?></span> изображений
                        </td><td>
                           <? if ($cnt_foto_obj_dubl) echo '<strong>'.$cnt_foto_obj_dubl.'</strong> объектов фото имеют дублирующиеся названия изображений<br>' ;
		                      if ($cnt_foto_obj_space) echo 'Из них пустых объектов фото: <strong>'.$cnt_foto_obj_space.'</strong><br>' ;
		                   ?>
		           		</td>
		           </tr>
		           <? $arr_clone=array('source','small') ;
                      if (sizeof($cur_table->list_clone_img())) $arr_clone=array_merge($arr_clone,array_keys($cur_table->list_clone_img())) ;

                   if ($cur_table->dir_name) foreach($arr_clone as $dir)
                   { $cnt_clone=($dir=='source')? $cnt_source:sizeof(scandir($cur_table->dir_to_file.$dir.'/'))-2 ;
                     $safe_exist=file_exists($cur_table->dir_to_file.$dir.'/.htaccess') ;

    				 ?><tr><td>Клон <strong><? echo $dir ?></strong><br><br><span class="green bold"><? echo $cnt_clone?></span> изображения</td><td><?
                       if ($dir!='source' and $dir!='small') {?><h2>Параметры клона:</h2><? damp_array($cur_table->list_clone_img($dir),1,-1) ; }
                       ?><h2>Операции:</h2><?
    				   if ($dir=='source')
                          { $cnt_img=$cnt_foto_obj-$cnt_foto_obj_dubl ;
                            if ($cnt_img>$cnt_source) echo '<a class=green mode=replace_viewer_main func=delete_obj_not_found_file script="ext/image/delete_obj_not_found_file" send=tags  table_id="'.$obj_info['pkey'].'">Рекомендуется удалить объекты фото, файлы которых не существуют</a><br>' ;
                            if ($cnt_img<$cnt_source) echo '<a class=green mode=replace_viewer_main func=delete_not_use_img script="ext/image/delete_not_use_img" send=tags table_id="'.$obj_info['pkey'].'" clone="'.$dir.'">Рекомендуется удалить файлы, не связанные ни с одним объектом фото</a><br>' ;
                            if ($safe_exist) echo 'Директория защишена паролем <a class=green mode=append_viewer_main cmd=clear_safe_dir table_id="'.$obj_info['pkey'].'" clone="'.$dir.'">Выключить защиту</a><br>' ;
                            else echo 'Директория не защишена <a class=green mode=append_viewer_main cmd=set_safe_dir table_id="'.$obj_info['pkey'].'" clone="'.$dir.'">Включить защиту</a><br>' ;
                          }
                       else
                         { if ($cnt_clone>$cnt_source) echo '<a class=green mode=append_viewer_main cmd=synhronize_clone_folder table_id="'.$obj_info['pkey'].'" clone="'.$dir.'">Рекомендуется удалить неиспользуемые файлы клонов</a><br>' ;
                           if ($cnt_clone<$cnt_source) echo '<a class=green mode=append_viewer_main func=create_clone_photo script="ext/image/create_clone_photo" send=tags table_id="'.$obj_info['pkey'].'" clone="'.$dir.'">Рекомендуется создать отсутствующие клоны изображений</a><br>' ;
                           if ($cnt_foto_obj) echo '<a class="green" mode=replace_viewer_main func=delete_all_img_of_clone script="ext/image/delete_all_img_of_clone" send=tags table_id="'.$obj_info['pkey'].'" clone="'.$dir.'">Удалить все изображения клона</a><br>' ;
                           echo '<br><a action=new_window href="http://'._MAIN_DOMAIN.'/admin/sh_ext.php?ext=create_clone_photo&script=ext/image&table_id='.$obj_info['pkey'].'&clone='.$dir.'">Cоздать отсутствующие клоны изображений</a>' ;
                         }


    				   ?><br></td></tr><?
                    }
		       }
	           if ($show_file_pars)
	           { $dir_name=($cur_table->dir_name)? $cur_table->dir_name:$cur_table->patch_to_file ;
                 ?><tr><td>Каталог к фото (относительно корня сайта)</td><td><?echo _DIR_TO_ROOT.'/'.generate_element('text',60,$tkey,$obj_info['clss'],$cur_tkey,'dir_to_image',$dir_name) ;?></td></tr>
		           <tr><td>Путь к фото</td><td><?echo $cur_table->dir_to_file?></td></tr>
		           <tr><td>Адрес к фото</td><td><?echo $cur_table->patch_to_file?></td></tr>
		         <?}?>
	  </table>
      <button class=button cmd=clss_101_save_list_params>Сохранить</button>
      <?
 }

 function clss_101_save_list_params()
 { global $pkey ;
     //damp_array($_POST) ;
     $table_obj=$_SESSION['descr_obj_tables'][$pkey] ;
     // переводим update_parent_enabled на процедуру MySQL
     switch($_POST['update_parent_enabled_mode'])
     { case 'none': delete_descr_table_setting($pkey,'update_parent_enabled') ;
                    break ;
       case 'php':  add_field_to_table($pkey,'_enabled','int(1)',1) ;
                    upload_descr_table($pkey) ;
                    update_descr_table_setting($pkey,'update_parent_enabled','php') ;
                    break ;
       case 'sql':  if ($table_obj->setting['update_parent_enabled']!='sql')
                    { create_procedure_update_parent_enabled($table_obj->table_name) ;
                      add_field_to_table($pkey,'_enabled','int(1)',1) ;
                      upload_descr_table($pkey) ;
                      // если процедура была успешно добавлена - сключаем режим
                      if (isset($table_obj->proc_info['update_parent_enabled'])) update_descr_table_setting($pkey,'update_parent_enabled','sql') ;
                    }
                    break ;
     }
     switch($_POST['set_image_name'])
     { case 'none': delete_descr_table_setting($pkey,'set_image_name') ;
                    break ;
       case 'subselect':  update_descr_table_setting($pkey,'set_image_name','subselect') ;
                    break ;
       case 'sql':  $img_table_obj=$_SESSION['descr_obj_tables'][$table_obj->list_clss[3]] ;
                    if ($table_obj->setting['set_image_name']!='sql')
                    {   add_field_to_table($pkey,'_image_name','varchar(255)') ;
                        create_procedure_update_image_name($table_obj->table_name,array('img_table_name'=>$img_table_obj->table_name)) ;
                        create_trigger($img_table_obj->table_name,'AFTER','INSERT',array('update_image_name'=>1,'main_table_name'=>$table_obj->table_name)) ;
                        create_trigger($img_table_obj->table_name,'AFTER','UPDATE',array('update_image_name'=>1,'main_table_name'=>$table_obj->table_name)) ;
                        create_trigger($img_table_obj->table_name,'AFTER','DELETE',array('update_image_name'=>1,'main_table_name'=>$table_obj->table_name)) ;
                        upload_descr_table($pkey) ;
                        if (isset($table_obj->proc_info['update_image_name'])) update_descr_table_setting($pkey,'set_image_name','sql') ;
                    }
                    break ;
     }

     save_list_obj($_POST['obj']) ;
 	 //$rec=execSQL_van("SELECT * from obj_descr_obj_tables where pkey=$pkey order by indx,obj_name") ;
 	 //$_SESSION['descr_obj_tables'][$pkey]->save_info($rec) ;
 	 create_model_obj_tables() ;
     return('ok') ;
 }

 function update_descr_table_setting($pkey,$setting_name,$setting_value)
 { $cur_setting=execSQL_value('select setting from obj_descr_obj_tables where pkey='.$pkey) ;
   if ($cur_setting) $cur_setting_arr=unserialize($cur_setting) ; else $cur_setting_arr=array() ;
   $cur_setting_arr[$setting_name]=$setting_value ;
   //damp_array($cur_setting_arr) ;
   $res_setting=serialize($cur_setting_arr) ;
   update_rec_in_table('obj_descr_obj_tables',array('setting'=>$res_setting),'pkey='.$pkey) ;
   $_SESSION['descr_obj_tables'][$pkey]->setting=$cur_setting_arr ;
 }

 function delete_descr_table_setting($pkey,$setting_name)
 { $cur_setting=execSQL_value('select setting from obj_descr_obj_tables where pkey='.$pkey) ;
   if ($cur_setting) $cur_setting_arr=unserialize($cur_setting) ; else $cur_setting_arr=array() ;
   unset($cur_setting_arr[$setting_name]);
   $res_setting=serialize($cur_setting_arr) ;
   update_rec_in_table('obj_descr_obj_tables',array('setting'=>$res_setting),'pkey='.$pkey) ;
   $_SESSION['descr_obj_tables'][$pkey]->setting=$cur_setting_arr ;
 }

 //-----------------------------------------------------------------------------------------------------------------------------
 // поддержка мультиязычности
 //-----------------------------------------------------------------------------------------------------------------------------

 function clss_101_multilang()
 { global $pkey ;
   if (sizeof($GLOBALS['lang_arr'])<=1) { echo 'Поддержка мультиязычности отключена. Смотрите настройки init.php, массив lang_arr<br>' ; return ; }
   ?><h1>Поддержка языков в системе</h1><?
   ?><table class=left><tr class=clss_header><td>Наименование</td><td>Суффикс для полей таблицы</td><tr><?
   foreach($GLOBALS['lang_arr'] as $lang_info) {?><tr><td><?echo $lang_info['name']?></td><td><?echo $lang_info['suff']?></td><tr><?}
   ?></table><?
   //damp_array(_DOT($pkey)) ;

   ?><h1>Поддержка языков в текущей таблице</h1><?
   ?><table valign=top><?
   if (sizeof(_DOT($pkey)->list_clss_ext)) foreach(_DOT($pkey)->list_clss_ext as $clss=>$clss_info)
       { $obj_clss=_CLSS($clss);
         if (sizeof($clss_info['multilang'])) foreach($clss_info['multilang'] as $fname=>$value)
           { $title=$obj_clss->view['list']['field'][$fname]['title'] ;
             if (!$title) $title=$obj_clss->view['details']['field'][$fname]['title'] ;
             if (!$title) $title=$fname ; else $title.=' ('.$fname.')' ;
             ?><tr><td><?echo $obj_clss->name?> (<?echo $clss?>)</td><td><?echo $title?></td></tr><?
           }
       }
   ?></table><?
   ?><br><strong class=red>При изменении поддержки мультиязычности необходимо выполнить "Проверку таблицы"</strong><?
 }

 // возщает select со списоком полей класса, доступных для мультиязыности - для поля класса 105
 function get_clss_text_fields(&$rec,$tkey,$pkey,$options,$fname)
  { $parent_rec=execSQL_van('select indx from obj_descr_obj_tables where clss=104 and pkey='.$rec['parent']) ;
    $clss=$parent_rec['indx'] ;
    $obj_clss=_CLSS($clss);
    $element_name="obj[".$rec['tkey']."][".$rec['clss']."][".$rec['pkey']."][obj_name]" ;
    $text='<select size="1" name="'.$element_name.'"><option value=""></option>' ;
    if (sizeof($obj_clss->fields)) foreach($obj_clss->fields as $fname=>$ftype)
        if (strpos($ftype,'varchar')!==false or strpos($ftype,'text')!==false)
        {   $title=$obj_clss->view['details']['field'][$fname]['title'] ;
            if (!$title) $title=$obj_clss->view['list']['field'][$fname]['title'] ;
            if (!$title) $title=$fname ; else $title.=' ('.$fname.')' ;
            $selected=($rec['obj_name']==$fname)? 'selected':'' ;
        	$text.='<option value="'.$fname.'" '.$selected.'>'.$title.'</option>' ;
        }
    $text.='</select>' ;
    return($text);
  }


 //-----------------------------------------------------------------------------------------------------------------------------
 // поддерживаемые классы
 //-----------------------------------------------------------------------------------------------------------------------------

 function clss_101_used_classes()
 { global $pkey ;
   	upload_use_clss_info($pkey) ;
   	//получаем список классов реальных объектов, находящихся в самой таблице
    $list_clss_info=execSQL('select clss,count(pkey) as cnt,'.$pkey.' as tkey from '._DOT($pkey)->table_name.' group by clss') ;
    //и в дополнительных таблицах
    if (sizeof(_DOT($pkey)->list_OUT)) foreach (_DOT($pkey)->list_OUT as $out_tkey=>$out_table_name)
    { $_arr=execSQL('select clss,count(pkey) as cnt,'.$out_tkey.' as tkey from '.$out_table_name.' group by clss') ;
      if (is_array($_arr)) $list_clss_info=array_merge($list_clss_info,$_arr) ;
    }

    // выводим информацию по классам согласно данным в descr_obj_tables
    ?><br><table cellspacing=0 cellpadding=0>
	           <tr><td class='grp_clss_header' colspan=6 >Поддерживаемые классы</td></tr>
	           <tr>
	     			<td class="cell_header">Код класса (clss)</td>
	     			<td class="cell_header">Иконка</td>
	     			<td class="cell_header">Имя класса</td>
	     			<td class="cell_header">Тип</td>
	     			<td class="cell_header">Дополнительная таблица </td>
	     			<td class="cell_header">Кол-во объектов класса в таблице</td>
	           </tr>
	      <? if (sizeof($list_clss_info)) foreach($list_clss_info as $cinfo)
	          { $obj_clss=_CLSS($cinfo['clss']);
                $src=$obj_clss->icons ;
	            ?> <tr>
	                <td class="td_data_on" ><? echo $cinfo['clss'] ?></td>
	                <td class="td_data_on" ><img src="<?echo $src?>" width="16" height="16" alt="" border="0"></td>
	                <td class="tdl_data_on"><? echo $obj_clss->name?></td>
	                <td class="tdl_data_on"><? echo (isset(_DOT($cinfo['tkey'])->list_clss[$cinfo['clss']]))? 'Назначен':'Найден&nbsp;&nbsp;&nbsp;<input name="add_to_sostav['.$cinfo['clss'].']" type="checkbox" value="1"> добавить в состав класса' ?></td>
	                <td class="tdl_data_on" ><? if ($cinfo['tkey']!=$pkey) echo _DOT($cinfo['tkey'])->table_name?>&nbsp;</td>
	                <td class="td_data_on" ><?echo $cinfo['cnt']?></td>
	               </tr><?
	          } ?>
	           <tr><td class='td_space' colspan=6 >&nbsp;</td></tr>
	           <tr><td class='grp_clss_header' colspan=6 >Сохранение классов по таблицам</td></tr>
	           <tr>
	     			<td class="cell_header">Код класса (clss)</td>
	     			<td class="cell_header">Иконка</td>
	     			<td class="cell_header">Имя класса</td>
	     			<td class="cell_header">Тип таблицы</td>
	     			<td class="cell_header" colspan=2>Имя таблицы</td>
	           </tr>
	      <? if (sizeof(_DOT($pkey)->list_clss)) foreach(_DOT($pkey)->list_clss as $clss=>$clss_tkey)
	          { $obj_clss=_CLSS($clss);
                $src=$obj_clss->icons ;
	            ?> <tr>
	                <td class="td_data_on" ><? echo $clss ?></td>
	                <td class="td_data_on" ><img src="<?echo $src?>" width="16" height="16" alt="" border="0"></td>
	                <td class="tdl_data_on"><? echo $obj_clss->name?></td>
	                <td class="tdl_data_on"><? echo ($clss_tkey==$pkey)? 'Основная':'Дополнительная' ?></td>
	                <td class="tdl_data_on" colspan=2><? echo _DOT($clss_tkey)->table_name?>&nbsp;</td>
	               </tr><?
	          } ?>

	     </table>
	<?
 }

 function cmd_exe_add_check_class()
 { global $add_to_sostav,$pkey ;
   if (sizeof($add_to_sostav)) foreach ($add_to_sostav as $clss=>$value) adding_rec_to_table('obj_descr_obj_tables',array('parent'=>$pkey,'clss'=>104,'enabled'=>1,'indx'=>$clss)) ;
   include_extension('table/table_checked') ;
   table_checked($pkey) ;
 }

 //-----------------------------------------------------------------------------------------------------------------------------
 // Модель таблицы
 //-----------------------------------------------------------------------------------------------------------------------------

 // показываем информацию
 function clss_101_model()
 { global $pkey ;
   ?><button class=button cmd=clss_101_reboot_model_tables>Перегрузить модель</button><br><br><?
   damp_array(_DOT($pkey),1,-1) ;
 }

 function clss_101_reboot_model_tables()
 { create_model_obj_tables() ;
   include_once(_DIR_TO_ROOT.'/ini/'._INIT_) ;
   init_local_setting() ;
   if (_ENGINE_MODE=='admin' and function_exists('init_admin_setting'))  init_admin_setting() ;  // грузим переменные адмики

   return('ok') ;
 }



//-----------------------------------------------------------------------------------------------------------------------------
// РАБОТАЕМ С ОБЪЕКТНОЙ ТАБЛИЦЕЙ - ОПЕРАЦИИ
//-----------------------------------------------------------------------------------------------------------------------------

// показываем список операций
function clss_101_operation()
{ global $pkey,$obj_info ;
   ?><table class=left>
    <tr class=clss_header><td colspan=2 >Операции</td></tr>
    <tr>
      <td>Создать пункт меню для прямого доступа (root id:<input  class='text' size=3 name="root_id" type=text value="0">)</td><td class="td_data_on"><button class=button cmd=clss_101_create_admin_menu_item>OK</button></td>
    </tr>
       <?if (_DOT($pkey)->list_clss_ext[3]) {?>
    <tr>
      <td>Добавить файлы изображений как объект 'фото' (clss=3)</td><td><button onclick="exe_cmd('assign_photo_to_obj')">OK</button></td>
    </tr>
    <tr>
      <td>Создать превьюшки фоток</td><td class="td_data_on"><button onclick="exe_cmd('create_small_priview_foto')">OK</button></td>
    </tr>
    <tr>
      <td>Заполнить file_name из obj_name</td><td class="td_data_on"><button cmd=clss_101_fill_file_name">OK</button></td>
    </tr>
       <?}?>
    <tr>
      <td>Собрать потерянные объекты</td><td class="td_data_on"><button  class=button cmd=found_not_parent_obj>OK</button></td>
    </tr>
    <tr>

    <tr>
      <td>Переименовать <input  class='text' name="_new_db_table_name" type=text class=text  value="<?echo $obj_info['table_name']?>"></td><td class="td_data_on"><button class=button cmd=clss_101_rename_db_table>OK</button></td>
    </tr>
    <tr>
      <td>Создать резервную копию</td><td class="td_data_on"><button class=button cmd=clss_101_create_res_copy_db_table>OK</button></td>
    </tr>

    <tr>
      <td>Преобразовать все объекты класса <input class='text' name="_alt_clss" type=text size=3 value=""> в объекты класса <input  class='text' name="_new_clss" type=text size=3 value=""></td><td class="td_data_on"><button class=button cmd=clss_101_convert_clss>OK</button></td>
    </tr>
    <tr>
      <td>Преобразовать все объекты id= <input class='text' name="_obj_ids" type=text size=10 value=""> в объекты класса <input  class='text' name="_new_clss2" type=text size=3 value=""></td><td class="td_data_on"><button class=button cmd=clss_101_convert_clss_to_id>OK</button></td>
    </tr>
    <tr>
      <td>Удалить все объекты класса <input class='text' name="_del_clss" type=text class=text  size=3 value=""></td><td class="td_data_on"><button class=button cmd=clss_101_delete_clss>OK</button></td>
    </tr>
    <tr>
      <td>Экспорт таблицы в xml-файл</td><td><button onclick="exe_cmd('export_table_to_xml')">OK</button></td>
    </tr>
    <?if (_DOT($pkey)->clss==108) {?><tr><td>Очистить журнал</td><td><button class=button cmd=jurnal_clear script="ext/table">OK</button></td></tr><?}?>
    <?if (_DOT($pkey)->clss==108) {?><tr><td>Обновить поле clss,enabled в журнале</td><td><button class=button cmd=jurnal_update_clss_enabled>OK</button></td></tr><?}?>
    <?if (_DOT($pkey)->field_info['url_name']) {?><tr><td>Заполнить поле url_name</td><td><button class=button cmd=clss_101_fill_url_name>OK</button></td></tr><?}?>
    <?if (_DOT($pkey)->field_info['_enabled']) {?><tr><td>Заполнить поле _enabled</td><td><button class=button cmd=clss_101_fill_parent_enabled>OK</button></td></tr><?}?>
    <?if (_DOT($pkey)->field_info['_image_name']) {?><tr><td>Заполнить поле _image_name</td><td><button class=button cmd=clss_101_fill_image_name>OK</button></td></tr><?}?>
    <?if (_DOT($pkey)->field_info['price_site']) {?><tr><td>Заполнить поле price_site</td><td><button class=button table_reffer="<?echo $_GET['reffer']?>" cmd=update_price_site script="mod/goods/i_goods_root_operation_actions.php">OK</button></td></tr><?}?>
   </table><?
   //echo _DOT($pkey)->mode ;
   //damp_array($_GET) ;
}


function clss_101_fill_image_name()
{ global $pkey ;
  ?><h1>Заполняем (обновляем) поле "_image_name"</h1><?
  $main_table_name=_DOT($pkey)->table_name ;
  $img_table_name=_DOT(_DOT($pkey)->list_clss[3])->table_name ;
  execSQL_update('update '.$main_table_name.' set _image_name=""') ;
  //$list_images=execSQL('select parent,file_name from '.$img_table_name.' where clss=3 and enabled=1 and not (file_name="" or file_name is null) group by parent order by parent,indx,pkey  ') ;
  $sql='select t1.parent,(select t2.file_name from '.$img_table_name.' t2 where t2.parent=t1.parent and  t2.clss=3 and t2.enabled=1 and not (t2.file_name="" or t2.file_name is null)  order by t2.indx limit 1) as file_name from '.$img_table_name.' t1 '.
        'group by t1.parent order by parent' ;
  $list_images=execSQL_row($sql) ;
  // потерянные объекты
  //$list_images=execSQL('select pkey,file_name from '.$img_table_name.' t1 where (select t2.pkey from '.$main_table_name.' t2 where t2.pkey=t1.parent) is null',2) ;
  if (sizeof($list_images)) foreach($list_images as $parent=>$fname)
  { execSQL_update('update '.$main_table_name.' set _image_name="'.$fname.'" where pkey='.$parent) ;
    echo '# '.$parent.' - '.$fname.'<br>' ;
  }

}

function clss_101_fill_file_name()
{ global $pkey ;
  $main_table_name=_DOT($pkey)->table_name ;
  ?><h1>Заполняем (обновляем) поле "_file_name"</h1><?
  execSQL_update('update '.$main_table_name.' set file_name=obj_name where file_name is null',array('debug'=>2)) ;
}

function clss_101_fill_parent_enabled()
{ global $pkey ;
  ?><h1>Заполняем (обновляем) поле "parent_enabled"</h1><?
  $list_id=execSQL_line('select distinct(t1.parent) as id,(select t2.enabled from '._DOT($pkey)->table_name.' t2 where t2.pkey=t1.parent) as status from '._DOT($pkey)->table_name.' t1 having status=1',1) ;
  $str_id=implode(',',$list_id) ;
  execSQL_update('update '._DOT($pkey)->table_name.' set _enabled=0',array('debug'=>1)) ;
  execSQL_update('update '._DOT($pkey)->table_name.' set _enabled=1 where parent in ('.$str_id.') or parent=0 or parent is null',array('debug'=>1)) ;
  return('ok') ;
}

function clss_101_fill_url_name()
{  global $obj_info ;
   ?><h1>Заполняем поле "url_name"</h1><?
   $list=execSQL('select pkey,obj_name,clss from '.$obj_info['table_name'].' order by pkey') ;
   ?><table><?
   if (sizeof($list)) foreach($list as $rec)
   { $url_name=_CLSS($rec['clss'])->prepare_url_obj($rec) ;
     execSQL_update('update '.$obj_info['table_name'].' set url_name="'.$url_name.'" where pkey='.$rec['pkey']) ;
     ?><tr><td><?echo $rec['id']?></td><td class=left><?echo $rec['obj_name']?></td><td class=left><?echo $url_name?></td></tr><?
   }
   ?></table><?
   echo 'Всего обновлено '.sizeof($list).' записей<br>' ;
}

// создаем пункт меню для прямого доступа к таблице
 function clss_101_create_admin_menu_item()
 {  global $obj_info ; $rec_root=array() ;
    if ($_POST['root_id']) $rec_root=execSQL_van('select * from '.$obj_info['table_name'].' where pkey='.$_POST['root_id']) ;
    ?><h1>Cоздаем пункт меню для прямого доступа к таблице</h1><?
    $name_root=($rec_root['pkey'])? $rec_root['obj_name']:$obj_info['obj_name'] ;
    //damp_array($obj_info) ;
    $_name=explode('_',$obj_info['table_name']) ;
    unset($_name[0],$_name[1]) ;
    $script_name=($rec_root['pkey'])? 'editor_'.implode('_',$_name).'_'.safe_file_names($name_root).'.php':'editor_'.implode('_',$_name).'.php' ;
    $options['include'][]			='_DIR_TO_ENGINE."/admin/c_site.php"' ;
    $options['options']['use_table']='"'.$obj_info['table_name'].'"' ;
    $options['options']['title']		='"'.$name_root.'"' ;
    $options['options']['use_class']	='"c_editor_obj"' ;
    if ($rec_root['pkey']) $options['options']['root_id']=$rec_root['pkey'] ;
    //echo 'script_name='.$script_name.'<br>' ;
    $file_items[$script_name]=$options ;
    //damp_array($file_items) ;
    create_menu_item($file_items) ;

    ?><br>
    <div class="black bold">Вставте этот код в файл /ini/init.php, функция init_admin_setting():</div>
    <br>
    <textarea name="Name" rows=7 cols=200 wrap="on" class=text>
	//----------------------------------------------------------------------------------------------------------------------
	// описание пунктов меню админа сайта
	//-----------------------------------------------------------------------------------------------------------------------

	$_SESSION['menu_admin_site']['Модули'][]=array('name'=>'<?echo $name_root?>','href'=>'<?echo $script_name?>') ;
    </textarea><br><br>
    <div class="black bold">После сохранения изменений перезагрузите админку</div>
	<?
 }



  // создаем резервную копию таблицы БД, передаем код таблицы, имя таблицы или используется obj_info
 function clss_101_create_res_copy_db_table()
 { global $obj_info ;
   create_res_copy_db_table($obj_info['pkey']) ;
 }


 function clss_101_delete_clss()
 { global $pkey,$_del_clss ;
   $sql='delete from '._DOT($pkey)->table_name.' where clss='.$_del_clss ;
     echo $sql.'<br>' ;
     mysql_query($sql) or die("Invalid query: " . mysql_error()."-".$sql);
   }


 function clss_101_convert_clss()
 { global $pkey,$_alt_clss,$_new_clss ;
   { $sql='update '._DOT($pkey)->table_name.' set clss='.$_new_clss.' where clss='.$_alt_clss ;
     echo $sql.'<br>' ;
     mysql_query($sql) or die("Invalid query: " . mysql_error()."-".$sql);
   }

 }

 function clss_101_convert_clss_to_id()
 { global $pkey,$_obj_ids,$_new_clss2 ;
   { $sql='update '._DOT($pkey)->table_name.' set clss='.$_new_clss2.' where pkey in ('.$_obj_ids.')' ;
     echo $sql.'<br>' ;
     mysql_query($sql) or die("Invalid query: " . mysql_error()."-".$sql);
   }
 }

 function clss_101_rename_db_table()
 { global $pkey,$obj_info,$table_name,$_new_db_table_name ;
   if ($_new_db_table_name) // имя должно быть не пустое
   { $inf=execSQL_van("show tables like '$_new_db_table_name'",0,1) ;
     if (!sizeof($inf))
     { $sql='rename table '.$obj_info['table_name'].' to '.$_new_db_table_name ;
         if (!$result = mysql_query($sql)) echo "Invalid query: " . mysql_error()."-".$sql;
          else { update_rec_in_table($table_name,array ('table_name'=>$_new_db_table_name),'pkey='.$pkey) ;
                 echo 'Переименование выполнено успешно<br>' ;
          	   }
     } else echo "Имя '$_new_db_table_name' уже существует в базе.<br>" ;
   }
 }

// показываем список операций с разделом объектных таблиц
 function _table_catalog_operation()
 { ?><table>
	  <tr class=clss_header><td colspan=2>Операции</td></tr>
	    <tr>
	      <td>Показать список удаленных таблиц</td><td><button onclick="exe_cmd_show('delete_tables')">OK</button></td>
	    </tr>
	    <tr>
	      <td>Очистить БД от удаленных таблиц</td><td><button onclick="exe_cmd_show('erase_from_delete_tables')">OK</button></td>
	    </tr>
    </table>
   <?
}

 // Показать список удаленных таблиц
 function cmd_show_delete_tables()
 {
   ?><h1>Cписок удаленных таблиц</h1><?
   $list=execSQL('show tables like "delete_%"') ;
   if (sizeof($list)) foreach($list as $table_name=>$table_info) echo $table_name.'<br/>' ;
    else echo 'Удаленных таблиц в базе не найдено<br/>' ;
 }

 // очищаем базу данных от удаленных таблиц
 function cmd_show_erase_from_delete_tables()
 {
 	?><h1>Очистка базы от удаленных таблиц</h1><?
	 $list=execSQL('show tables like "delete_%"') ;
	   if (sizeof($list)) foreach($list as $table_name=>$table_info)
	    { $sql='drop table '.$table_name ;
	      //echo $sql.'<br>' ;
	      if (!$result = mysql_query($sql)) echo "Ошибка : ".mysql_error()."-".$sql.'<br>' ;
	        else echo $table_name.' - удалена <br/>' ;
	    }
	    else echo 'Удаленных таблиц в базе не найдено<br/>' ;
 }


?>