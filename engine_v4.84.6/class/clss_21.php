<?  // список
include_once('clss_0.php') ;
class clss_21 extends clss_0
{

  function obj_create_dialog()
     { $form_id='form_obj_create_dialog_clss_'.$this->clss ;
       ?><form method="POST" action="ajax.php" id=<?echo $form_id?>>
             <table class="debug left">
                <tr><td>Название списка</td><td><input type=text class="text big" name=new_obj[obj_name]></td></tr>
                <tr><td>Имя массива в PHP</td><td>IL_<input type=text class="text" name=new_obj[arr_name]></td></tr>
                <tr><td>Элементы списка<br>новый элемент - с новой строки</td><td><textarea style="width:500px;height:150px;" class="text big" name=new_obj[list_items]></textarea></td></tr>
             </table>
             <button class=button_green mode=append_viewer_main cmd=create_obj_after_dialog clss="<?echo $this->clss?>" parent_reffer="<?echo $_POST['parent_reffer']?>">Добавить</button>
             и <? show_select_action('after_clss_'.$_POST['clss'].'_create_action',array('go_list'=>'перейти к спискам','go_this'=>'перейти к созданному списку','go_create'=>'создать еще один список')) ;?>
         </form>
       <?
       return(0) ;
     }

  // создаем объект нужного класса
  function obj_create(&$finfo,$options=array())
  { if ($finfo['arr_name']) $finfo['arr_name']=safe_file_names($finfo['arr_name']) ;
    if (isset($finfo['list_items'])) $list_items=$finfo['list_items'] ;
    if (isset($options['list_items'])) $list_items=$options['list_items'] ;
    unset($finfo['list_items']) ;
    // вызываем базовую функцию создания объекта
    $obj_info=parent::obj_create($finfo,$options) ;
    echo 'Создан список "<span class=green>'.$obj_info['obj_name'].'</span>"<br>' ;
    // добавляем элементы списка - если они переданы
    if ($list_items)
    { $arr=explode("\n",$list_items) ;
       if (sizeof($arr)) foreach($arr as $name)
       {  $name=trim($name) ;
          if ($name) { $item_info=_CLSS(20)->obj_create(array('obj_name'=>$name),array('parent_reffer'=>$obj_info['_reffer'],'debug'=>0,'no_save_to_session'=>1)) ;
                       echo 'В список добавлено "<span class=green>'.$item_info['obj_name'].'</span>"<br>' ;
                     }
       }
       echo '<br><div class="green bold">Было добавлено '.sizeof($arr).' элементов</div><br>' ;
    }
    // сохраняем массив в сессии - чтобы можно было с ним сразу работать
    $arr_name=get_IL_array_name($obj_info) ;
    $_SESSION[$arr_name]=array() ;
    $_SESSION['index_list_names'][$obj_info['pkey']]=$arr_name ;
    // создаем класс и перезагружаем данные списка в сессии
    _IL($obj_info['pkey'])->reload_list() ;
    return($obj_info) ;
  }

  // при удалении списка, также список из сессии и массива index_list_names
  function on_delete_event($obj_info,$options=array())
  { $arr_name=$_SESSION['index_list_names'][$obj_info['pkey']] ;
    unset($_SESSION[$arr_name]) ;
    unset($_SESSION['index_list_names'][$obj_info['pkey']]) ;
    parent::on_delete_event($obj_info,$options) ;
  }


  // -----------------------------------------------------------------------------------------------------------------------------------------------------------------------
  //
  //  ВЫВОД ОБЪЕКТОВ КЛАССА В VIEWER - показ списка дочерних объектов
  //
  // -----------------------------------------------------------------------------------------------------------------------------------------------------------------------
  // вывод дочерних объектов объекта
  //  перед показом списка обновляем массив в памяти
  function show_list_children($obj_rec,$options=array())
  {  // получаем имя массива
     // создаем класс и перезагружаем данные списка в сессии
     _IL($obj_rec['pkey'])->reload_list() ;
     parent::show_list_children($obj_rec,$options) ;
  }


  // -----------------------------------------------------------------------------------------------------------------------------------------------------------------------
  //
  //  eникальные методы класса "список"
  //
  // -----------------------------------------------------------------------------------------------------------------------------------------------------------------------


  function show_list_operation()
  {
    ?><table>
         <tr><td class="left">Оптимизировать дубликаты значений</td><td class='center'><button class=button cmd=remove_dublikate_item>Далее</button></td></tr>
     </table>
   <?
  }

  function remove_dublikate_item()
  {   global $obj_info ;
      ?><h2>Ищем дубликаты элементов списка</h2><?
      //damp_array($obj_info) ;
      $recs=execSQL('select pkey,id,obj_name from '._DOT($obj_info['tkey'])->table_name.' where parent='.$obj_info['pkey'].' order by id') ;
      $dubl_names=execSQL_row('select obj_name,count(pkey) as cnt from '._DOT($obj_info['tkey'])->table_name.' where parent='.$obj_info['pkey'].' group by obj_name having cnt>1 order by id',2) ;
      if (sizeof($recs)) foreach($recs as $rec)
      {   $rec['obj_name']=strtolower(str_replace(array('ё'),array('е'),$rec['obj_name'])) ;
          /*if ($rec['id']==66)
            { damp_string($rec['obj_name']) ;
              echo 'str='.($dubl_names['серо-зеленый']).'<br>' ;
              echo 'val='.($rec['obj_name']).'<br>' ;
              echo 'val='.($dubl_names[$rec['obj_name']]).'<br>' ;

            }*/
          if (isset($dubl_names[$rec['obj_name']]))
          {   if (!isset($dubl_names2[$rec['obj_name']])) { $dubl_names2[$rec['obj_name']]=$rec['id'] ; /*echo '<strong>'.$rec['id'].' => '.$rec['id'].'</strong><br>' ; */ }
              else
              { $main_id=$dubl_names2[$rec['obj_name']] ;
                echo 'Преобразовываем '.$rec['id'].' => '.$main_id.'<br>' ;
                execSQL_update('update '.TM_SORTAMENT.' set color='.$main_id.' where color='.$rec['id'],array('debug'=>0)) ;
                execSQL_update('delete from '._DOT($obj_info['tkey'])->table_name.' where parent='.$obj_info['pkey'].' and id='.$rec['id'],array('debug'=>0)) ;
              }
          }
      }
      damp_array($dubl_names2,1,-1) ;


  }
}

?>