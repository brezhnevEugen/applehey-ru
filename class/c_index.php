<?php
include('c_page.php') ;
class c_page_index extends c_page
{ public $body_class='index' ;

  function set_head_tags()
    { parent::set_head_tags() ;
      $this->HEAD['js'][]='http://code.jquery.com/ui/1.10.3/jquery-ui.js' ;
      $this->HEAD['js'][]=_PATH_TO_EXT.'/Camera-master/scripts/camera.min.js' ;
      $this->HEAD['js'][]=_PATH_TO_EXT.'/Camera-master/scripts/camera.js' ;
      $this->HEAD['css'][]=_PATH_TO_EXT.'/Camera-master/css/camera.css' ;
    }

  function block_main()
  { include_once(_DIR_EXT.'/Camera-master/list_banners_camera.php');?>
    <section class = "camera_container">
      <?list_banners_camera($_SESSION['ban_system']->banners[75],$options=array());?>
    </section>

    <section class = "well cntr">
      <div class = "container">
        <h2>Where We Put Customers First</h2>
        <h4 class = "color4">Get Peace-of-Mind with a Company You Can Trust</h4>
        <p>Dolore cuml dia sed in lacus ut eniascet inger es sitet amet eismod ictor ut ligulate ameti dapib ticdu nt mtsent dolor lte comme. Mes cumldi <br>sed inertio. Lacus ut eniascet ingerto aliiqt sit amet eism odictor ipsum commete. Ut ligulate ameti dapibus ticdu nt mtsent justo dolor los <br>ltiissim commete pulvinar ac loreestibul umsed ante. Donec sagittieismo purus </p>
        <div class = "row">
          <div class = "col-md-6 col-sm-6 col-xs-12  wow fadeIn right mobile-cntr">
            <h4>Shop DIY Kits &amp; Tools</h4>
            <a href = "index.html#" class = "btn"><i class = "fa-shopping-cart"></i>Shop Now</a>
          </div>
          <div class = "col-md-6 col-sm-6 col-xs-12  wow fadeIn left mobile-cntr">
            <h4>Find a Store Near You</h4>
            <a href = "index.html#" class = "btn mod1"><i class = " fa-map-marker"></i>Locations</a>
          </div>
        </div>
      </div>
    </section>

    <section class = "well2 cntr">
      <div class = "container">
        <div class = "row">
          <div class = "col-md-4 col-sm-4 col-xs-12  wow fadeIn">
            <div class = "icon1 fa-thumbs-o-up"></div>
            <h3>180 Day <br>Warranty</h3>
            <h5 class = "color4"><a href = "index.html#">Mes cuml dia sed in lacus <br>es sitet amet eismodi ut.</a></h5>
          </div>
          <div class = "col-md-4 col-sm-4 col-xs-12  wow fadeIn">
            <div class = "icon1 fa-mobile"></div>
            <h3>Quality <br>Parts</h3>
            <h5 class = "color4"><a href = "index.html#">Mes cuml dia sed in lacus <br>es sitet amet eismodi ut.</a></h5>
          </div>
          <div class = "col-md-4 col-sm-4 col-xs-12  wow fadeIn">
            <div class = "icon1 fa-wrench"></div>
            <h3>Tech <br>Savvy</h3>
            <h5 class = "color4"><a href = "index.html#">Mes cuml dia sed in lacus <br>es sitet amet eismodi ut.</a></h5>
          </div>
        </div>
      </div>
    </section>

    <section class = "well3 bg4">
      <div class = "container">
        <h2>Services We Offer</h2>
        <div class = "row">
          <div class = "col-md-6 col-sm-12 col-xs-12  wow fadeIn">
            <h5>The skilled technicians at our company provide <br>the highest quality cell phone repair services</h5>
            <p>Dolore cuml dia sed in lacus ut eniascet inger es sitet amet eismod ictor ut ligulate ameti dapib ticdu nt mtsent dolor lte comme. Mes cumldi sed inertio. Lacus ut eniascet ingerto aliiqt sit amet eism odictor ipsum commete. Ut ligulate ameti dapibus ticdzu nt mts.</p>
          </div>
          <div class = "col-md-3 col-sm-6 col-xs-12  wow fadeIn">
            <ul class = "marked-list">
              <li><a href = "index.html#">Repair</a></li>
              <li><a href = "index.html#">Refurbishing</a></li>
              <li><a href = "index.html#">Unlocking</a></li>
            </ul>
          </div>
          <div class = "col-md-3 col-sm-6 col-xs-12  wow fadeIn">
            <ul class = "marked-list">
              <li><a href = "index.html#">Custom Software</a></li>
              <li><a href = "index.html#">Branding</a></li>
              <li><a href = "index.html#">Flashing</a></li>
            </ul>
          </div>
        </div>
      </div>
    </section>

    <section class = "well4 cntr">
      <div class = "container">
        <div class = "icon2 fa-comments-o"></div>
        <h2>What People Say</h2>
        <div class = "owl-carousel">
          <div class = "item">
            <blockquote>
              <h5>Thank you for your prompt response <br>and the help that you gave me.</h5>
              <p>
                <q>What a great organisation you are!! I am surprised pleasurably for your prompt answer. <br>You rendered an invaluable service! Thank you very much! </q>
              </p>
              <cite>Lara Onamuska <br><a href = "index.html#">https://demolink.org</a></cite>
            </blockquote>
          </div>
          <div class = "item">
            <blockquote>
              <h5>What a great organisation you are!! I am surprised.</h5>
              <p>
                <q>Thank you for your prompt response and the help that you gave me. <br>Surprised pleasurably for your prompt answer. You rendered an invaluable service! </q>
              </p>
              <cite>Liza Smith <br><a href = "index.html#">https://demolink.org</a></cite>
            </blockquote>
          </div>
        </div>
      </div>
    </section>

    <section class = "parallax well5 wow fadeIn" data-url = "images/parallax1.jpg" data-mobile = "true" data-speed = "1">
      <div class = "container">
        <h2>Providing Fast &amp; Efficient Repair <br>at a Reasonable Cost</h2>
        <h5>We warranty all of our repair work.</h5>
        <p>Mes cuml dia sed in lacus ut eniascet ipsu ingerto aliiqt es site amet eismod ictor uten ligulate ameti dapibu ticdute numtsen lusto dolor ltissim comes cuml dia sed inertio lacusueni ascet dolingert aliiqt sit amet. Eism com odictor ut ligulate cot ameti dapibu ipsam voluptatem. Nemo enim ipsam voluptatem voluptas sit asper natur aut odit aut fugit sed quia consequuntur magni dolores eos qui ratione.</p>
        <p>Dia sed in lacus ut eniascet ipsu ingerto aliiqt es site amet eismod ictor uten ligulate ameti dapibu ticdute numtsen lusto dolor ltissim comes cuml dia sed inertio lacusueni ascet dolingert aliiqt sit amet. Eism com odictor ut ligulate cot ameti dapibu ipsam voluptatem. </p>
        <a href = "index.html#" class = "btn"><i class = "fa-file-text"></i>More Info</a>
      </div>
    </section>

    <section class = "bg1 section1 well6 wow fadeIn">
      <div class = "container">
        <div class = "row">
          <div class = "col-lg-6 col-md-6 col-sm-6 col-xs-12  wow fadeIn mobile-cntr">
            <h3 class = "mod1">Get in Touch <br>With Us</h3>
            <h5 class = "color5">Our company is your one stop solution for all needs.</h5>
            <dl class = "dlist1">
              <dt>Telephone:</dt>
              <dd>
                <a href = "callto:#"> +1 800 603 6035 </a>
              </dd>
              <dt>E-mail:</dt>
              <dd>
                <a href = "../cdn-cgi/l/email-protection.html#1330"> <span class = "__cf_email__" data-cfemail = "315c50585d7155545c5e5d585f5a1f5e4356">[email&#160;protected]</span>
                  <script data-cfhash = 'f9e31' type = "text/javascript">/* <![CDATA[ */
                    !function (t,e,r,n,c,a,p)
                    { try
                      { t = document.currentScript || function () {for(t = document.getElementsByTagName('script'), e = t.length; e--;)if (t[e].getAttribute('data-cfhash'))return t[e]}();
                        if (t && (c = t.previousSibling))
                        { p = t.parentNode;
                          if (a = c.getAttribute('data-cfemail'))
                          { for(e = '', r = '0x'+a.substr(0,2)|0, n = 2; a.length-n; n += 2)e += '%'+('0'+('0x'+a.substr(n,2)^r).toString(16)).slice(-2);
                            p.replaceChild(document.createTextNode(decodeURIComponent(e)),c)
                          }
                          p.removeChild(t)
                        }
                      }
                      catch(u){}
                    }()
                    /* ]]> */
                  </script>
                </a>
              </dd>
            </dl>
          </div>
          <div class = "col-lg-6 col-md-6 col-sm-6 col-xs-12  wow fadeIn  mobile-cntr">
            <h3 class = "mod1"><i class = "fa-envelope-o icon3 mod1"></i> <span>Subscribe to <br>our Newsletter</span></h3>
            <div class = "subsribe_mailform">
              <form method = "post" action = "bat/rd-mailform.php.html" class = "mailform">
                <input type = "hidden" name = "form-type" value = "subscribe">
                <fieldset class = "row">
                  <label class = "col-lg-8 col-md-12 col-sm-12 col-xs-12">
                    <input type = "text" name = "email" placeholder = "YOUR EMAIL ADDRESS" data-constraints = "@Email @NotEmpty">
                  </label>
                  <div class = "mfControls col-lg-4 col-md-12 col-sm-12 col-xs-12">
                    <button type = "submit" class = "btn mod1"> Subscribe</button>
                  </div>
                </fieldset>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>
    <?
  }







  function panel_main_banner()
  { global $ban_system ;
    ?><div id="panel_main_banner"><?
      $ban_system->show_list_items(48,'Jssor.Slider.FullPack/panel_banner_rotator_2') ;    //отключено для ускорения загруки, будет подгружено по ajax после загрузки всей страницы
      //_banner(3) ;
    ?></div><?
  }

  function panel_banner_section()
  { global $ban_system ;
    $ban_system->show_list_items(27,'list_banners_img_a_name',array('order'=>'indx','count'=>4,'id'=>'panel_banner_section','view_mode'=>'icon','clone'=>175)) ;
  }

  /*function panel_last_news()
  { global $news_system ;
    ?><div id="panel_last_news" class="round_bottom"><div class=title>Новости</div><?
    $news_system->show_last_items('news/list_news_data_a_name_annot',array('order'=>'c_data desc','count'=>2)) ;
    ?></div><?
  } */

  function panel_responses_slider()
  { global $responses_system ;
    ?><br><br><h2>Отзывы наших клиентов</h2><?
      $responses_system->show_list_items('','responses/list_responses_messages',array('debug'=>0,'panel_select_pages'=>0,'order_by'=>'c_data desc','id'=>'panel_responses_slider')) ;
      ?><script type="text/javascript">$j(document).ready(function(){$j('div#panel_responses_slider').carusel_item({interval:10000}) ;})</script><?
  }


}
?>
