<?php
include_once(_DIR_TO_ENGINE."/admin/c_site.php") ;
include_once(_DIR_TO_ENGINE."/admin/i_clss_func.php") ;
include_once(_DIR_TO_ENGINE.'/admin/c_menu_tabs.php') ;

// редактор заказов
class c_editor_accounts extends c_editor_obj
{
  //function body($options=array()) { $this->body_frame_2x(array('fra_tree'=>_PATH_TO_ADMIN.'/editor_accounts_tree.php','title'=>$options['title'])) ; }
}


class c_editor_accounts_tree extends c_fra_tree
{ public $system_name='account_system' ;

  function _body($options=array())
 {?><body id=tree_objects><div id=tree_menu><?$this->top_menu() ;?></div><?
     $this->generate_object_tree('Клиенты',$this->tkey,array('on_click_script'=>'editor_account_viewer.php'));
  ?></body><?
  }

 function body($options=array())
 { if (!$this->tkey) { echo 'Не задана рабочая таблица.' ; _exit() ; }
   $this->check_reffer() ; // проверяем, откуда происходит вызов скрипта
   ?><body id=tree_objects><div id=tree_menu><?$this->top_menu() ;?></div><?
   $usl_root=($_GET['root_id'])?  'pkey='.$_GET['root_id']:'parent=0' ;
   $this->generate_object_tree($this->tkey,array('usl_root'=>$usl_root,'no_show_cnt'=>0,'on_click_script'=>'editor_accounts_viewer.php')); // генерим начало дерева
   ?><script type="text/javascript">$j('ul.tree_root > li:first-child').each(tree_item_click)</script><? // имитируем клик по корню
   ?></body><?
 }

}

//-----------------------------------------------------------------------------------------------------------------------------
// ЗАКАЗЫ
//-----------------------------------------------------------------------------------------------------------------------------

class c_editor_accounts_viewer extends c_fra_based
{ public $system_name='account_system' ;
  public $top_menu_name='top_menu_list_account' ;

 function body($options=array()) {$this->body_frame($options) ; }

// заголовок фрейма
 function show_page_obj_header($options=array())
 { global $obj_info ;
   $obj_info=get_obj_info($_GET['reffer']) ;
   ?><p class='obj_header'><?echo $obj_info['obj_name']?></p><?
 }

 // меню фрейма
 function show_page_obj_menu($obj_info)
    {  // создаем объект top_menu
       //damp_array($_GET) ;
       if (!is_object($_SESSION[$this->top_menu_name])) $_SESSION[$this->top_menu_name]=new c_top_menu() ;
       // формируем набор пунктом меню
       $menu_set=array() ;
       // пункты меню для корня дерева
       if ($_GET['pkey']=='root') $menu_set[]=array("name" => 'Поиск по клиентам','cmd' =>'show_list_items','mode'=>'last_items','count'=>20) ;
       // или если выбран один из временных интервалов
       else
       { //$arr_status=$_SESSION['account_system']->get_count_by_status($time_usl) ;
         /// !!! внимание - переделать, чтобы были вкладки "ТИПЫ ОПЛАТЫ"
         //$arr_oplata=$_SESSION['account_system']->get_count_by_field('payments_method',$time_usl) ;
         //damp_array($arr_oplata) ;
         //if (sizeof($arr_status)) foreach($arr_status as $status=>$cnt) {$menu_set[]=array("name" => $_SESSION['list_status_account'][$status].'('.$cnt.')','cmd' =>'show_list_items','status'=>$status) ; }
         //if ($arr_oplata['robox']) $menu_set[]=array("name" => 'Robox ('.$arr_oplata['robox'].')',"cmd" =>'show_list_items','robox'=>1) ;
         //$menu_set[]=array("name" => 'Все ('.array_sum($arr_status).')',"cmd" =>'show_list_items') ;
       }
       // выводим меню
       $this->cur_menu_item=$_SESSION[$this->top_menu_name]->show($menu_set) ;  // показываем меню
       return($this->cur_menu_item) ;
    }

  function show_list_items()
  {


      $options=array() ;
    if ($this->start_time or $this->end_time)  $_usl[]=$this->get_time_usl($this->start_time,$this->end_time) ;
    if (isset($this->cur_menu_item['status'])) $_usl[]='status='.$this->cur_menu_item['status'] ;
    if (isset($this->cur_menu_item['robox']))  $_usl[]='payments_method="robox"' ;
    $usl=implode(' and ',$_usl) ;
    $title='Заказы '.$this->get_time_title($this->start_time,$this->end_time) ;
    if (isset($this->cur_menu_item['status'])) $title.=' в состоянии "'.$_SESSION['list_status_account'][$this->cur_menu_item['status']].'"' ;
    if (isset($this->cur_menu_item['robox'])) $title.=' оплаченные через Robox' ;
    if ($this->cur_menu_item['mode']=='last_items')
    { $options['count']=$this->cur_menu_item['count'] ;
      $options['order']='c_data desc' ;
      $title.=' - последние '.$this->cur_menu_item['count'] ;
    }
    $options['default_order']='c_data desc' ;
    $options['title']=$title ;
    $options['buttons']=array('save','delete') ;
    _CLSS(82)->show_list_items($this->tkey,$usl,$options) ;
  }

  function panel_fast_search()
    {   global $obj_info ;
        echo '</form>' ;
        ?><div id="panel_fast_search"><form>
            <input type="hidden" name="parent" value="<?echo $obj_info['pkey']?>">
            <strong>Имя, логин, email или адрес</strong>&nbsp;&nbsp;&nbsp;
            <input type=text name=text_search class="text">
            <label><input type=radio name=mem_type value="87" checked> Частное лицо</label>
            <label><input type=radio name=mem_type value="88"> Организация</label>
            <br>
            <br>
            <strong>Число заказов:</strong>
            <select name="count_orders">
                <option></option>
                <option value="1">1 заказ</option>
                <option value="2">2 заказа</option>
                <option value="3">3 заказа</option>
                <option value="4">4 заказа</option>
                <option value="5">5 заказа</option>
                <option value="6">6 заказа</option>
                <option value="7">7 заказа</option>
                <option value="8">8 заказа</option>
                <option value="9">9 заказа</option>
                <option value="10">10 и более заказов</option>
            </select>&nbsp;&nbsp;&nbsp;
            <strong>Сумма заказов:</strong>
            <select name="summ_orders">
                <option></option>
                <option value=".10">10 тыс</option>
                <option value="10.50">10..50 тыс</option>
                <option value="50.100">50..100 тыс</option>
                <option value="100.200">100..200 тыс</option>
                <option value="200.300">200..300 тыс</option>
                <option value="300.">более 300 тыс</option>
            </select>&nbsp;&nbsp;&nbsp;
            <strong><input type=checkbox name=use_order_time value="1"> Дата покупки:</strong>
            <? $time_info=execSQL_van('select min(c_data) as t1,max(c_data) as t2 from obj_site_orders where clss=82')?>
            <? generate_datepicker('start_time',date('d.m.Y',time())) ?>...
            <? generate_datepicker('end_time',date('d.m.Y',time())) ?>
            <br><br>
            <label><input type=checkbox name=subscript value="yes"> Подписанные на рассылку</label>
            <label><input type=checkbox name=subscript value="no"> Неподписанные на рассылку</label>
            <br><br>
            <button class="v2 button_green" cmd="account-get_account">Искать</button>
        </div></form><br><br>
        <div id="panel_result_search"></div>
     <?
    }
}


?>