<?
include_once(_DIR_TO_ENGINE."/admin/c_site.php") ;
include_once(_DIR_TO_ENGINE."/admin/i_clss_func.php") ;
include_once(_DIR_TO_ENGINE.'/admin/c_menu_tabs.php') ;

// редактор службы поддержки
class c_editor_responses extends c_editor_obj
{
  function body($options=array()) { $this->body_frame_2x(array('fra_tree'=>_PATH_TO_ADMIN.'/editor_responses_tree.php','title'=>$options['title'])) ; }
}

// дерево тикетов
class c_editor_responses_tree extends c_fra_tree
{ public $system_name='responses_system' ;
  function body($options=array())
 {?><body id=tree_objects><div id=tree_menu><?$this->top_menu() ;?></div><?
     $options=array() ;
     $options['usl_select']='clss=27' ;
     $options['on_click_script']='editor_responses_viewer.php' ;
     $options['on_click_item']=array(27=>'fra_viewer.php') ;

     $this->generate_time_tree('Служба поддержки',$this->tkey,$options);
  ?></body><?
 }

}

class c_editor_responses_viewer extends c_fra_based
{  public $system_name='responses_system' ;
   public $top_menu_name='top_menu_list_responses' ;

 function body(&$options=array()) {$this->body_frame($options) ; }

// заголовок фрейма
 function show_page_obj_header()
 { $title=($_GET['pkey']=='root')? 'Служба поддержка: все тикеты':'Служба поддержка: тикеты '.$this->get_time_title($this->start_time,$this->end_time) ;
   ?><p class='obj_header'><?echo $title?></p><?
 }
    
 // меню фрейма
 function show_page_obj_menu($obj_info)
    {  global $responses_system ;
      // создаем объект top_menu
       if (!is_object($_SESSION[$this->top_menu_name])) $_SESSION[$this->top_menu_name]=new c_top_menu() ;
       // формируем набор пунктом меню
       $menu_set=array() ;
       // пункты меню для корня дерева
       if ($_GET['pkey']=='root')
       { $menu_set[]=array("name" => 'На модерации',"cmd" =>'show_list_items','status'=>0) ;
         $menu_set[]=array("name" => 'Опубликованные',"cmd" =>'show_list_items','status'=>1) ;
         $menu_set[]=array("name" => 'Поиск по отзывам',"cmd" =>'show_list_items','no_show_list'=>1) ;
       }
       // или если выбран один из временных интервалов
       else
       { $time_usl=$this->get_time_usl($this->start_time,$this->end_time) ;
         $arr_status=$_SESSION['responses_system']->get_count_by_status($time_usl) ;
         if (sizeof($arr_status)) foreach($arr_status as $status=>$cnt) {$menu_set[]=array("name" => $_SESSION['list_responses_status'][$status].'('.$cnt.')','cmd' =>'show_list_items','status'=>$status) ; }
         $menu_set[]=array("name" => 'Все ('.array_sum($arr_status).')',"cmd" =>'show_list_items') ;
       }
       // выводим меню
       $this->cur_menu_item=$_SESSION[$this->top_menu_name]->show($menu_set) ;  // показываем меню
       return($this->cur_menu_item) ;
    }    

    function show_list_items()
    { $options=array() ;
      if ($this->start_time or $this->end_time) $_usl[]=$this->get_time_usl($this->start_time,$this->end_time) ;
      if (isset($this->cur_menu_item['status'])) $_usl[]='enabled='.$this->cur_menu_item['status'] ;
      $usl=implode(' and ',$_usl) ;
      $title='Отзывы '.$this->get_time_title($this->start_time,$this->end_time) ;
      if (isset($this->cur_menu_item['status'])) $title.=' в статусе "'.$_SESSION['list_responses_status'][$this->cur_menu_item['status']].'"' ;
      if ($this->cur_menu_item['mode']=='last_items')
      { $options['count']=$this->cur_menu_item['count'] ;
        $options['order']='c_data desc' ;
        $title.=' - последние '.$this->cur_menu_item['count'] ;
      }
      $options['default_order']='c_data desc' ;
      $options['title']=$title ;
      $options['buttons']=array('save','delete') ;
      if (!$this->cur_menu_item['no_show_list']) _CLSS(27)->show_list_items($this->tkey,$usl,$options) ;
    }

    function panel_fast_search()
       { ?><div id=panel_search script_url="<?echo _PATH_TO_ADMIN?>/editor_responses_ajax.php" cmd="get_list_tiket">
                <strong>ПОИСК:</strong>
                <input type=text name=text_search class="text"><img src="<?echo _PATH_TO_ENGINE?>/admin/images/find_icon.gif" class=go_search>
                <input type=radio name=target_search value=autor checked>По автору
                <input type=radio name=target_search value=content>По содержанию
                <!--<input type=radio name=target_search value=goods>По товару-->
           </div>
         <?
       }

}
?>