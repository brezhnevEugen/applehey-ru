<?php
$__functions['init'][]='_responses_site_vars' ;
$__functions['boot_site'][]='_responses_site_boot' ;

function _responses_site_vars() //
{	$_SESSION['TM_responses']= 'obj_'.SITE_CODE.'_responses' ;
    $_SESSION['init_options']['responses']=array() ;
    $_SESSION['init_options']['responses']['use_capcha_code']=1 ;
    $_SESSION['init_options']['responses']['debug']=0 ;
    $_SESSION['init_options']['responses']['usl_show_items']='enabled=1 and clss=27' ;
    $_SESSION['init_options']['responses']['system_title']='Отзывы на сайте' ;
    $_SESSION['init_options']['responses']['root_dir']='responses' ;
    $_SESSION['init_options']['responses']['moderation']='pre' ;// pre - премодерация, after - постмодерация сообщний
    $_SESSION['init_options']['responses']['notice_message_autor']='1' ;// уведомлять клиента о размещении им сообщения на сайта
}


function _responses_site_boot($options)
{
    create_system_modul_obj('responses',$options) ; // создание объекта responses
}

class c_responses_system extends c_system
{
  public $moderation='pre' ;
  public $notice_message_autor=1 ;
  public $no_fs_info=1 ;

  function c_responses_system($create_options=array())
  {  // устанавливаем значения опций по умолчанию, если они не были заданы ранее
     if (!$create_options['table_name']) 			$create_options['table_name']=$_SESSION['TM_responses'] ;
     if (!$create_options['root_dir'])				$create_options['root_dir']='responses' ;
     if (!$create_options['usl_show_items'])        $create_options['usl_show_items']='clss=27 and enabled=1' ;
     if (!$create_options['system_title'])          $create_options['system_title']='Отзывы на сайте' ;
     if (!$create_options['usl_show_items'])        $create_options['system_title']='enabled=1' ;
  	 parent::c_system($create_options) ;
  	 $this->use_capcha_code=1 ;  // создание сообщений только через капча-код
  	 $this->use_autorize_to_otwet=0 ; // модерация сообщений без авторизации на сайте
  	 $this->moderation=($create_options['moderation'])? $create_options['moderation']:'pre' ;
  	 $this->notice_message_autor=$create_options['notice_message_autor'] ;
  }

  // готовим информацию по объекту - эта функция должны быть перекрыта в кажом классе
  function prepare_public_info(&$rec,$options=array())
  {
  	$rec['__name']='Сообщение от '.$rec['obj_name'].' '.date("d.m.y",$rec['c_data']) ;
  	$rec['__data']=date("d.m.y",$rec['c_data']) ;
  	$rec['__href']='/'.$this->root_dir.'/'.$rec['pkey'].'.html'; ;
    $rec['__moderator_url']='http://'._MAIN_DOMAIN.'/'.$this->root_dir.'/message.php?uid='.$rec['edit_code']; ;
  	if ($rec['obj_reffer'])  $rec['obj_reffer_info']=select_db_ref_info($rec['obj_reffer'],array('no_text_fields'=>1)) ;
  }



 // добавляем новое сообщение в систему отзывов
 // rec : запись по новому отзыву
 //     -   value   - текст отзыва
 //     -   name, email, phone - данные по автору
 //     -   _reffer, _reffer_name, _reffer_href, _reffer_parent - данные по объекту отзыва
 // options : параметры создания отзыва
 //     -   debug           - отладка
 //     -   use_mail_code   - код письма

 function create_message($rec=array(),$options=array())
 { global $member ;
   $debug=$options['debug'] ;
   // проверяем правильность проверочного кода
   if ($this->use_capcha_code) if (!capcha_code_checkit()) return(prepare_result('error_check_code')) ;
     
   // обязательные поля
   $rec['edit_code']=md5(uniqid(rand(),true)) ;
   $rec['value']=strip_tags($rec['value']) ;
   if ($rec['_text_mess'] and !$rec['value']) $rec['value']=$rec['_text_mess'] ; // совместимость со старым именем поля
   // поля по умолчанию
   if (!$rec['parent']) $rec['parent']=1 ;
   if (!$rec['clss'])   $rec['clss']=27 ;
   if (!$rec['c_data']) $rec['c_data']=time() ;
   if (!$rec['enabled']) $rec['enabled']=($this->moderation=='after')? 1:0;
   if (!$rec['account_reffer']) $rec['account_reffer']=$member->reffer ;
   // совместимость со старыми полями - в будушем убрать
   if (!$rec['autor_email'])        $rec['autor_email']=$rec['email'] ;
   if (!$rec['autor_name'])         $rec['autor_name']=$rec['name'] ;
   if (!$rec['autor_phone'])        $rec['autor_phone']=$rec['phone'] ;
   if (!$rec['obj_reffer'])         $rec['obj_reffer']=$rec['_reffer'] ; // объект на котором был задан вопрос
   if (!$rec['obj_reffer_name'])    $rec['obj_reffer_name']=$rec['_reffer_name'] ;   // название объекта
   if (!$rec['obj_reffer_href'])    $rec['obj_reffer_href']=$rec['_reffer_href'] ; // с какой страницы был сделал запрос
   if (!$rec['obj_reffer_parent'])  $rec['obj_reffer_parent']=$rec['_reffer_parent'] ; // раздел, с товара которого был задан вопрос

   // если заполнены не все поля, показать ошибку
   if (!$member->id and ($rec['autor_name']=='')) return(prepare_result('error_field')) ;
   if ($rec['value']=='') return(prepare_result('error_field')) ;
     
   //if (isset($rec['use_klient_reffer'])) $klient_reffer=$rec['use_klient_reffer'] ; else $klient_reffer=($member->id)? $member->reffer:'' ;
   //$name=($arr_form_values['city'])? $arr_form_values['name'].', '.$arr_form_values['city']:$arr_form_values['name'] ;
   if ($debug) damp_array($rec) ;
   $rec['pkey']=adding_rec_to_table($this->tkey,$rec) ; // создаем новую ветку
   if (!$rec['pkey']) return(prepare_result('db_error')) ;
   update_rec_in_table($this->tkey,array('obj_name'=>'Отзыв № '.$rec['pkey']),'pkey='.$rec['pkey']) ;

   $rec['_reffer']=$rec['pkey'].'.'.$this->tkey ;
   $this->prepare_public_info($rec) ;

   // регистрируем событие 'Создание сообщения в системе отзывов'
   $evt_id=_event_reg('Создание сообщения в системе отзывов','',$rec['_reffer'],$rec['account_reffer']) ;
   // отправляем уведомление о создании тикета в системе отзывов
   $this->send_mail_new_message($rec,array('use_event_id'=>$evt_id,'use_mail_code'=>$options['use_mail_code'])) ;

   $result['code']='create_success' ;
   $result['type']='success' ;
   $this->prepare_public_info($rec) ;
   $result['rec']=$rec ;

   return($result) ;
 }



 function add_comment($obj_rec,$data,$options=array())
 {  global $member ;
    if ($this->use_autorize_to_otwet and !$member->id) return(prepare_result('required_autorize')) ;
    $arr_fields=array() ;
    if ($obj_rec['value']!=$data['value'])        $arr_fields['value']=$data['value'] ;
    if ($obj_rec['comment']!=$data['comment'])    $arr_fields['comment']=$data['comment'] ;
    if ($obj_rec['enabled']!=$data['enabled'])    $arr_fields['enabled']=($data['enabled'])? 1:0 ;
    if (sizeof($arr_fields)) { $res=update_rec_in_table($this->tkey,$arr_fields,'pkey='.$obj_rec['pkey'],array('debug'=>0)) ;
                               $result=($res)? prepare_result('create_comment_success'):prepare_result('db_error') ;
                               if ($res)
                               { if ($arr_fields['comment'])
                                  { $evt_id=_event_reg('Добавление комментария  к отзыву','',$obj_rec['_reffer'],$obj_rec['account_reffer']) ;
                                    $obj_rec['comment']=$arr_fields['comment'] ;
                                    $this->send_mail_change_comment($obj_rec,array('use_event_id'=>$evt_id)) ;
                                  }
                                 if (isset($arr_fields['enabled']))
                                  { $ent_name=($arr_fields['enabled'])? 'Публикация отзыва на сайте':'Удаление отзыва с сайта' ;
                                    $evt_id=_event_reg($ent_name,'',$obj_rec['_reffer'],$obj_rec['account_reffer']) ;
                                    $obj_rec['enabled']=$arr_fields['enabled'] ;
                                    $this->send_mail_change_status($obj_rec,array('use_event_id'=>$evt_id)) ;
                                  }
                               }
                             }
    else $result=prepare_result('no_change_success') ;
    return ($result);
 }

 // возвращает количество отзывов по коду объекта
 function get_responses_count_by_reffer($reffer)
 {   $req=execSQL_van('select count(pkey) as cnt from '.$this->table_name.' where clss=27 and enabled=1 and obj_reffer="'.$reffer.'"') ;
     return($req['cnt']) ;
 }

 function get_count($usl_select='')
 { if ($usl_select) $usl_select='and '.$usl_select ;
   $cnt=execSQL_value('select count(pkey) as cnt from '.$this->table_name.' where clss=27 '.$usl_select) ;
   return($cnt) ;
 }


 function get_responses_count($usl_select='')
 { if ($usl_select) $usl_select='and '.$usl_select ;
   $cnt=execSQL_value('select count(pkey) as cnt from '.$this->table_name.' where clss=27 and enabled=1 '.$usl_select) ;
   return($cnt) ;
 }

    function get_count_by_status($usl_select='')
    { $sql='select enabled,count(pkey) as cnt from '.$this->table_name.' where clss=27 ' ;
      if ($usl_select!="") $sql.=' and '.$usl_select ;
      $sql.=' group by enabled' ;
      $cnt_arr=execSQL_row($sql) ;
      return($cnt_arr) ;
    }

 function get_rating($add_usl='')
 {
 	$usl='clss=27 and enabled=1 and rating>0' ;
 	if ($add_usl) $usl.=' and '.$add_usl ;
 	$req=execSQL_van('select count(pkey) as cnt,sum(rating) as sum from '.$this->table_name.' where '.$usl) ;
    $res['count']=$req['cnt'] ;
    $res['rating']=($req['cnt'])? round($req['sum']/$req['cnt'],1):0 ;

 	return($res) ;
 }

 function get_message_info_by_uid($uid)
    {
        $mess=execSQL_van('select * from '.$this->table_name.' where clss=27 and edit_code="'.$uid.'"') ;
        if ($mess['pkey']) { $result=prepare_result('open_success') ; $this->prepare_public_info($mess) ; }
        else               $result=prepare_result('message_not_found') ;
        return(array($mess,$result)) ;
    }


 function send_mail_new_message($message,$options=array())
 { global $mail_system,$LS_mail_bottom_podpis,$LS_responses_system_email_alert;
   $params['site_name']=_MAIN_DOMAIN ;
   $params['name']=$message['autor_name'] ;
   $params['email']=$message['autor_email'] ;
   $params['data']=date("d.m.Y H:i",$message['c_data']) ;
   $params['to']=($message['obj_reffer_name'])? 'на "<a href="'.$message['obj_reffer_href'].'">'.$message['obj_reffer_name'].'</a>".':'.' ;
   $params['message']=$message['value'] ;
   $params['message_href']=$message['__moderator_url'] ;
   $params['mail_bottom_podpis']=$LS_mail_bottom_podpis ;
   $mail_system->send_mail_to_pattern($LS_responses_system_email_alert,'responses_system_message_create_alert',$params,$options) ;
   if ($this->notice_message_autor and $message['autor_email']) $mail_system->send_mail_to_pattern($message['autor_email'],'responses_system_message_create_alert_user',$params,$options) ;
 }

 function send_mail_change_comment($message,$options=array())
 { global $site_name,$mail_system,$LS_mail_bottom_podpis;
   $params['site_name']=$site_name ;
   $params['name']=$message['autor_name'] ;
   $params['email']=$message['autor_email'] ;
   $params['data']=date("d.m.Y H:i",$message['c_data']) ;
   $params['data_change']=date("d.m.Y H:i",$message['r_data']) ;
   $params['to']=($message['obj_reffer_name'])? 'на "<a href="'.$message['obj_reffer_href'].'">'.$message['obj_reffer_name'].'</a>".':'.' ;
   $params['message']=$message['value'] ;
   $params['comment']=$message['comment'] ;
   $params['mail_bottom_podpis']=$LS_mail_bottom_podpis ;
   if ($message['autor_email']) $mail_system->send_mail_to_pattern($message['autor_email'],'responses_system_info_change_mess_comment',$params,$options) ;
 }

 function send_mail_change_status($message,$options=array())
 { global $site_name,$mail_system,$LS_mail_bottom_podpis;
   $params['site_name']=$site_name ;
   $params['name']=$message['autor_name'] ;
   $params['email']=$message['autor_email'] ;
   $params['data']=date("d.m.Y H:i",$message['c_data']) ;
   $params['data_change']=date("d.m.Y H:i",$message['r_data']) ;
   $params['to']=($message['obj_reffer_name'])? 'на "<a href="'.$message['obj_reffer_href'].'">'.$message['obj_reffer_name'].'</a>".':'.' ;
   $params['message']=$message['value'] ;
   $params['new_status']=$message['enabled']? 'Опубликовано':'Неопубликовано' ;
   $params['mail_bottom_podpis']=$LS_mail_bottom_podpis ;
   if ($message['autor_email']) $mail_system->send_mail_to_pattern($message['autor_email'],'responses_system_info_change_mess_status',$params,$options) ;
 }



}


?>