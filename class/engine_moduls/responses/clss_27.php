<?
include_once(_DIR_TO_ENGINE.'/class/clss_0.php') ;
class clss_27 extends clss_0
{

 function show_responses_links()
 { global $obj_info,$responses_system ;
   $responses_system->prepare_public_info($obj_info) ;
   ?><h2>Прямые ссылки на отзыв:</h2>
     <p>Ссылка для <span class="green bold">МОДЕРАЦИИ</span> отзывы через сайт: <a href="<?echo $obj_info['__moderator_url']?>" target=_blank><?echo $obj_info['__moderator_url']?></a></p>
   <?
 }

 function show_responses_info($rec)
 {  global $responses_system ;
    $responses_autor=($rec['account_reffer'])? generate_obj_text($rec['account_reffer'],array('read_only'=>1)):'<span class="bold green">'.$rec['autor_name'].'</span> ('.$rec['autor_phone'].' '.$rec['autor_email'].')' ;
    $all_count=$responses_system->get_count('and autor_email="'.$rec['autor_email'].'"') ;
    if (!$rec['status']) $rec['status']=0 ;
    echo 'Состояние: '.(($rec['enabled'])? '<span class="green">Опубликован</span>':'<span class="red">На модерации</span>').'&nbsp;&nbsp;&nbsp;' ;
    echo 'Клиент: '.$responses_autor.'&nbsp;&nbsp;&nbsp;' ;
    echo 'Всего отзывов автора: <span class="green bold size18">'.$all_count.'</span><br/><br/>' ;
    if ($rec['href']) {?>Отзыв открыт со страницы сайта: <a target=_blank href=<?echo _PATH_TO_SITE.'/'.$rec['href']?>><?echo _PATH_TO_SITE.'/'.$rec['href']?></a><br><br><?}
    if ($rec['obj_reffer'])
    { //list($text,$onclick,$td_class)=generate_field_text($responses_system->tkey,$rec,'obj_reffer',array('only_text'=>1)) ;
      $obj_rec=select_db_ref_info($rec['obj_reffer']) ;
      echo generate_ref_obj_viewer($obj_rec).'<br/><br/>' ;
    }
 }


 function responses_add_message()
 { global $responses_system,$obj_info,$member ;
   //damp_array($_POST) ;
   if ($_POST['mess'])
   {   echo 'Добавляем новое сообщение' ;
       $obj_info['open_mode']=1 ; // почеаем, что это ответ
       $info=array() ;
       $info['value']=$_POST['mess'] ;
       $info['name']=$member->name ;
       $info['email']=$member->info['email'] ; if (!$info['email']) $info['email']=$_SESSION['LS_responses_system_email_alert'] ;
       $info['phone']=$member->info['phone'] ;
       $info['debug_mail']=1 ;
       ob_start() ;
       $res=$responses_system->add_message($obj_info,$info) ; //damp_array($res) ;
       $text=ob_get_clean() ;
       echo ($res['message_id']=='create_otwet_success')? '<span class="green bold"> - OK</span><br>':'<span class="red bold"> - ERROR ('.$res['message_id'].')</span><br>' ;
       echo $text ;
   }

   return('ok') ; // будет показан начальный экран
 }

 function responses_delete()
 {
   echo 'Удаляем тикет' ;
 }

 function responses_close()
 { global $responses_system,$obj_info ;
   $responses_system->close_responses($obj_info) ;
   return('ok') ; // // будет показан начальный экран
 }

 function show_responses_history()
   { global $obj_info ;
     show_history('links like "%'.$obj_info['_reffer'].'%" or reffer="'.$obj_info['_reffer'].'"') ;
   }

 function show_other_responses_autor()
 { global $obj_info,$responses_system ;
   _CLSS(27)->show_list_items($responses_system->tkey,'autor_email="'.$obj_info['autor_email'].'" and pkey!='.$obj_info['pkey'],array('order'=>'c_data desc')) ;
 }

 function on_change_event_enabled($tkey,$pkey,$fvalue)
 {  global $responses_system ;
    $message=select_db_obj_info($tkey,$pkey) ;
    damp_array($message) ;
    if ($message['enabled']==$fvalue) return(0) ; // возвращаем, что значение не изменилось
    $ent_name=($fvalue)? 'Публикация отзыва на сайте':'Удаление отзыва с сайта' ;
    $evt_id=_event_reg($ent_name,'',$message['_reffer'],$message['account_reffer']) ;
    $message['enabled']=$fvalue ;
    $responses_system->send_mail_change_status($message,array('use_event_id'=>$evt_id)) ;
    return(1);
 }

 function on_change_event_comment($tkey,$pkey,$fvalue)
 {  global $responses_system ;
    $message=select_db_obj_info($tkey,$pkey) ;
//    damp_array($message) ;
    if ($message['comment']==$fvalue) return(0) ; // возвращаем, что значение не изменилось
    $evt_id=_event_reg('Добавление комментария  к отзыву','',$message['_reffer'],$message['account_reffer']) ;
    $message['comment']=$fvalue ;
    $responses_system->send_mail_change_comment($message,array('use_event_id'=>$evt_id)) ;
    return(1);
 }
}


?>