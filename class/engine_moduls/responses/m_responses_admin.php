<?php
$__functions['init'][]		='_responses_admin_vars' ;
$__functions['alert'][]		='_responses_admin_alert' ;
$__functions['install'][]	='_responses_install_modul' ;
$__functions['boot_admin'][]='_responses_admin_boot' ;

include_once('clss_27.php') ;

function _responses_admin_vars() //
{   //-----------------------------------------------------------------------------------------------------------------------------
	// описание классов модуля
	//-----------------------------------------------------------------------------------------------------------------------------
    $_SESSION['list_responses_status']=array('0'=>'На модерации','1'=>'Опубликован','2'=>'Закрыт') ;
 	// сообщение системы отзывов
 	// в старой версии:
 	// текст сообщения - value => obj_name
 	// обхект сообщения - obj_ref => obj_reffer
 	// имя объекта сообщения - obj_ref_name => obj_reffer_name
 	// аккаунт автора -  member => account_reffer
    $_SESSION['descr_clss'][27]=array() ;
	$_SESSION['descr_clss'][27]['name']='Сообщение' ;
	$_SESSION['descr_clss'][27]['parent']=0 ;
	$_SESSION['descr_clss'][27]['fields']=array('account_reffer'=>'any_object',
                                                'autor_email'=>'varchar(100)',
                                                'autor_name'=>'varchar(100)',
                                                'autor_phone'=>'varchar(32)',
                                                'value'=>'text',
                                                'comment'=>'text',
                                                'obj_reffer'=>'any_object',
                                                'obj_reffer_name'=>'text',
                                                'obj_reffer_href'=>'varchar(255)',
                                                'obj_reffer_parent'=>'int(11)',
                                                'edit_code'=>'varchar(32)',
                                                'rating'=>'int(11)') ;
	$_SESSION['descr_clss'][27]['parent_to']=array() ;
    $_SESSION['descr_clss'][27]['icons']=_PATH_TO_BASED_CLSS_IMG.'/7.png' ;
    
    $_SESSION['descr_clss'][27]['menu'][]=array("name" => "Отзыв",	                "type" => 'prop') ;
    $_SESSION['descr_clss'][27]['menu'][]=array("name" => "История отзыва",			"cmd" => 'show_responses_history') ;
    $_SESSION['descr_clss'][27]['menu'][]=array("name" => "Ссылки",			        "cmd" => 'show_responses_links') ;
    $_SESSION['descr_clss'][27]['menu'][]=array("name" => "Другие отзывы автора",	"cmd" => 'show_other_responses_autor') ;
    

    $_SESSION['descr_clss'][27]['list']['field']['c_data']=array('title'=>'Создан','edit_element'=>'data_input','data_format'=>'d.m.y','datepicker'=>1) ;
	$_SESSION['descr_clss'][27]['list']['field']['enabled']=array('title'=>'Разместить на сайте') ;
    $_SESSION['descr_clss'][27]['list']['field'][]=array('title'=>'Автор','on_view'=>'clss_27_show_autor','td_class'=>'left') ;
    $_SESSION['descr_clss'][27]['list']['field']['value']=array('title'=>'Содержание отзыва','td_class'=>'left') ;
    //$_SESSION['descr_clss'][27]['list']['field']['is_HTML']=array('title'=>'HTML')  ;
    $_SESSION['descr_clss'][27]['list']['field']['comment']=array('title'=>'Комментарий модератора','td_class'=>'left') ;
   	$_SESSION['descr_clss'][27]['list']['field'][]=array('title'=>'На что','on_view'=>'clss_27_show_reffer','td_class'=>'left') ;
   	    	$_SESSION['descr_clss'][27]['list']['field']['rating']=array('title'=>'Оценка автора') ;

    $_SESSION['descr_clss'][27]['details']['field']['c_data']=array('title'=>'Создан','edit_element'=>'data_input','data_format'=>'d.m.y','datepicker'=>1) ;
    $_SESSION['descr_clss'][27]['details']['field']['enabled']=array('title'=>'Разместить на сайте') ;
    $_SESSION['descr_clss'][27]['details']['field']['rating']=array('title'=>'Оценка автора') ;    
    $_SESSION['descr_clss'][27]['details']['field']['autor_name']=array('title'=>'Автор') ;
    $_SESSION['descr_clss'][27]['details']['field']['autor_email']=array('title'=>'Email автора') ;
    $_SESSION['descr_clss'][27]['details']['field']['autor_phone']=array('title'=>'Телефон автора') ;
    //$_SESSION['descr_clss'][27]['details']['field'][]=array('title'=>'Автор','on_view'=>'clss_27_show_autor','td_class'=>'left') ;
    $_SESSION['descr_clss'][27]['details']['field'][]=array('title'=>'На что','on_view'=>'clss_27_show_reffer','td_class'=>'left') ;
    $_SESSION['descr_clss'][27]['details']['field']['value']=array('title'=>'Содержание отзыва') ;
    //$_SESSION['descr_clss'][27]['details']['field']['is_HTML']=array('title'=>'HTML')  ;
    $_SESSION['descr_clss'][27]['details']['field']['comment']=array('title'=>'Комментарий модератора','td_class'=>'left') ;
    $_SESSION['descr_clss'][27]['details']['field'][]=array('title'=>'Ссылка для редактирования через сайт','on_view'=>'clss_27_show_edit_code') ;

	//----------------------------------------------------------------------------------------------------------------------
	// описание пунктов меню админа сайта
	//-----------------------------------------------------------------------------------------------------------------------
    $_SESSION['menu_admin_site']['Модули']['responses']=array('name'=>'Отзывы','href'=>'editor_responses.php','icon'=>'menu/speech_balloon.jpg') ;

}

function _responses_admin_boot($options)
{
	create_system_modul_obj('responses',$options) ;
}


function _responses_install_modul($DOT_root)
{   echo '<h2>Инсталируем модуль <strong>Отзывы</strong></h2>' ;

    $file_items=array() ;
    // editor_responses.php
    $file_items['editor_responses.php']['include'][]='$dir_to_modules."responses/m_responses_frames.php"' ;
    $file_items['editor_responses.php']['options']['use_table_code']='"TM_responses"' ;
    $file_items['editor_responses.php']['options']['title']='"Редактор отзывов"' ;
    $file_items['editor_responses_tree.php']['include'][]='$dir_to_modules."responses/m_responses_frames.php"' ;
    $file_items['editor_responses_viewer.php']['include'][]='$dir_to_modules."responses/m_responses_frames.php"' ;
    create_menu_item($file_items) ;

	// таблица отзывов
	$pattern=array() ;

	$pattern['table_title']				= 	'Отзывы' ;
	$pattern['use_clss']				= 	'1,27' ;
	$pattern['def_recs'][]				=	array ('parent'=>0,	'clss'=>1,	'enabled'=>1,	'indx'=>0,	'obj_name'=>'Система отзывов') ;
   	$pattern['table_name']				= 	$_SESSION['TM_responses']  ;
	$pattern['table_clss']				= 	101 ;
	$pattern['table_parent']			= 	$DOT_root;
   	create_table_by_pattern($pattern) ;

    SETUP_get_img('/responses/admin_img_menu/speech_balloon.jpg',_DIR_TO_ADMIN.'img/menu/speech_balloon.jpg') ;

    //create_dir('/responses/') ;
    //SETUP_get_file('/responses/system_dir/htaccess.txt','/responses/.htaccess');
    //SETUP_get_file('/responses/system_dir/index.php','/responses/index.php');
    //SETUP_get_file('/responses/system_dir/create.php','/responses/create.php');
    //SETUP_get_file('/responses/system_dir/create_comment.php','/responses/create_comment.php');

    create_dir('/responses/') ;
    //$file_items=array() ;
    //$file_items['index.php']['options']['system']='"request_system"' ;
    //$file_items['index.php']['options']['ext']='_DIR_TO_MODULES."/responses/"' ;
    //$file_items['create.php']['options']['system']='"request_system"' ;
    //$file_items['create.php']['options']['use_include_patch']='_DIR_TO_MODULES."/galerey/"' ;
    //$file_items['.htaccess']['text'][]='RewriteEngine On' ;
    //$file_items['.htaccess']['text'][]='RewriteRule ^(.*)/(.*).html$ index.php[L]' ;
    //$file_items['.htaccess']['text'][]='RewriteRule ^(.*).html$ index.php [L]' ;
    //$file_items['.htaccess']['text'][]='RewriteRule ^(.*)/$ index.php [L]' ;
    //create_script_to_page($file_items,_DIR_TO_ROOT.'/request/','../ini/patch.php') ;


    import_setting_from_xml(_PATH_TO_SERVER_SETUP.'?m=responses&s=setting',array('parent_tkey'=>$_SESSION['pkey_by_table'][TM_SETTING],'parent_pkey'=>1,'check_unique_name'=>1)) ;
    import_setting_from_xml(_PATH_TO_SERVER_SETUP.'?m=responses&s=mails',array('parent_tkey'=>$_SESSION['pkey_by_table'][TM_MAILS],'parent_pkey'=>1,'check_unique_name'=>1)) ;

}

function _responses_admin_alert()
{ $res=execSQL_van('select count(pkey) as cnt from '.$_SESSION['TM_responses'].' where clss=27 and enabled=0') ;
  if ($res['cnt']) echo "<div>У Вас <span class='red bold'>".$res['cnt']."</span> новых отзывов в <a href='"._PATH_TO_ADMIN."/editor_responses.php'>службе отзывов</a></div><br>";
}


function clss_27_show_autor($rec,$tkey,$pkey,$options,$cur_fname)
{ $text='' ;
  if ($rec['autor_name']) $text.=$rec['autor_name'].'<br>' ;
  if ($rec['autor_email']) $text.=$rec['autor_email'] ;
  return($text) ;
}

function clss_27_show_wopros($rec,$tkey,$pkey,$options,$cur_fname)
{ $text='' ;
  if ($rec['value']) $text.='<strong>'.$rec['value'].'</strong><br>' ;
  if ($rec['comment']) $text.='<div class=comment>'.$rec['comment'].'</div>' ;
  return($text) ;
}

function clss_27_show_reffer($rec,$tkey,$pkey,$options,$cur_fname)
{ global $goods_system  ;
  $arr=array() ;
  if ($rec['obj_reffer']) $arr[]=generate_obj_text($rec['obj_reffer'],$options).'<br>' ;
  if ($rec['obj_reffer_parent']) $arr[]=generate_obj_text($rec['obj_reffer_parent'].'.'.$goods_system->tkey,$options).'<br>'  ;
  if ($rec['obj_reffer_href']) $arr[]='<strong>URL:</strong><a href="'.$rec['obj_reffer_href'].'" target=_blank>'.$rec['obj_reffer_href'].'</a>' ;
  return(implode('<br>',$arr)) ;
}

function clss_27_show_edit_code($rec,$tkey,$pkey,$options,$cur_fname)
{ $url=_MAIN_DOMAIN.'/responses/message.php?uid='.$rec['edit_code'];
  $href='<a href="http://'.$url.'" target=_blank>'.$url.'</a>' ;
  return($href) ;
}

//-----------------------------------------------------------------------------------------------------------------------------
// Функции - обработчики событий
//-----------------------------------------------------------------------------------------------------------------------------

 function on_change_event_clss_27_comment($tkey,$pkey,$fvalue)
 {  global $responses_system,$events_system ;
    $message=select_db_obj_info($tkey,$pkey) ;
    if ($message['comment']==$fvalue) return(0) ; // возвращаем, что значение не изменилось
    $evt_id=$events_system->reg('Добавление комментария  к отзыву','',$message['_reffer'],$message['account_reffer']) ;
    $message['comment']=$fvalue ;
    $responses_system->send_mail_change_comment($message,array('use_event_id'=>$evt_id)) ;
    return(1);
 }

 function on_change_event_clss_27_enabled($tkey,$pkey,$fvalue)
 {  global $responses_system,$events_system ;
    $message=select_db_obj_info($tkey,$pkey) ;
    if ($message['enabled']==$fvalue) return(0) ; // возвращаем, что значение не изменилось
    $ent_name=($fvalue)? 'Публикация отзыва на сайте':'Удаление отзыва с сайта' ;
    $evt_id=$events_system->reg($ent_name,'',$message['_reffer'],$message['account_reffer']) ;
    $message['enabled']=$fvalue ;
    $responses_system->send_mail_change_status($message,array('use_event_id'=>$evt_id)) ;
    return(1);
 }
?>