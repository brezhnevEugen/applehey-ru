<?php

$__functions['init'][]		='_art_site_vars' ;
$__functions['boot_site'][]	='_art_site_boot' ;

// функции быстрого доступа
function _show_rasdel_art($id,$func_name,$options=array()) { $_SESSION['art_system']->show_list_section($id,$func_name,$options) ;}
function _ART_SYSTEM() {return($_SESSION['art_system']);}


function _art_site_vars() //
{   $_SESSION['TM_artikle']='obj_'.SITE_CODE.'_artikle' ; $GLOBALS['TM_artikle']=&$_SESSION['TM_artikle'] ;

    // описание системы  -------------------------------------------------------------------------------------------------------------------
	$_SESSION['init_options']['art']['patch_mode']	='TREE_NAME' ;
	$_SESSION['init_options']['art']['root_dir']	='art' ;
    $_SESSION['init_options']['art']['tree_fields']='pkey,parent,clss,indx,enabled,href,obj_name,c_data,intro' ;
    $_SESSION['init_options']['art']['no_use_value_to_annot']=0 ;
    $_SESSION['init_options']['art']['annot_strip_tags']=1 ;
    $_SESSION['init_options']['art']['tree']['get_count_by_clss']=0 ;
    //$_SESSION['init_options']['art']['tags_list_id']=2 ; // код списка тегов в списках сайта
	$_SESSION['init_options']['art']['pages']['size']=array('10'=>'По 10 статей','20'=>'По 20 статей','all'=>'Все') ;
	$_SESSION['init_options']['art']['pages']['sort_type']=array() ;
    $_SESSION['init_options']['art']['pages']['sort_type'][1]=array('name'=>'По порядку',	'order_by'=>'indx',			'diapazon_title'=>'Позиции:') ;
	$_SESSION['init_options']['art']['pages']['sort_type'][2]=array('name'=>'По дате',	    'order_by'=>'c_data desc',	'diapazon_title'=>'Дата:') ;
	$_SESSION['init_options']['art']['pages']['sort_type'][3]=array('name'=>'По наименованию','order_by'=>'obj_name',	'diapazon_title'=>'Наименования:') ;

}


function _art_site_boot($options)
{
	//create_system_modul_obj('art',array('debug'=>'show_model','tree'=>array('debug'=>0))) ;  // создание объекта art_system
	create_system_modul_obj('art',$options) ;  // создание объекта art_system
}

class c_art_system  extends c_system_catalog
{ var $max_annot_length ;
  var $table_values ;
  var $table_parents ;
  var $tags_list_id ;


  function	c_art_system($create_options=array())
  { if (!$create_options['patch_mode']) 			$create_options['patch_mode']='TREE_NAME' ;
    if (!$create_options['table_name']) 			$create_options['table_name']=$_SESSION['TM_artikle'] ;
    if (!$create_options['usl_show_items']) 		$create_options['usl_show_items']='clss in (11,3,4,5) and enabled=1' ;
    if (!$create_options['root_dir'])				$create_options['root_dir']='art' ;
    if (!$create_options['order_by'])	            $create_options['order_by']='indx' ;
    parent::c_system_catalog($create_options) ;
    $this->max_annot_length=($create_options['max_annot_length'])? $create_options['max_annot_length']:450 ;
    if (_DOT($this->table_name.'_values')!=null)    $this->table_values=$this->table_name.'_values' ;
    if (_DOT($this->table_name.'_parents')!=null)   $this->table_parents=$this->table_name.'_parents' ;
    if ($create_options['tags_list_id'])	        $this->tags_list_id=$create_options['tags_list_id'];
  }

  // готовим информацию по статьи
  // 6.10.2010 - переделан алгоритм формирования аннотации
  // если аннотация не прописана, текст аннотации формируется на основе основного текста
  // $options['annot_strip_tags'] - удалить мета теги из текста аннотации
  // $options['annot_count']      - максимальный размер аннотации, генерируемой на основе основного текста

  function prepare_public_info(&$rec,$options=array())
  { // для показа полной аннотации значение $options['annot_count']=0
    $max_annot_length=(isset($options['annot_count']))? $options['annot_count']:$this->max_annot_length ;
    // ВНИМАНИЕ! если устанавливается значение для 'max_annot_length' то опция 'annot_strip_tags' автоматически устанавливается в 1 - чтобы не обрезать закрывающие теги в тексте статьи
    if ($max_annot_length) $options['annot_strip_tags']=1 ;
    //damp_array($rec) ;
    if ($rec['intro'] and !$rec['annot']) $rec['annot']=$rec['intro'] ;
  	$rec['__href']=$this->get_patch_item($rec) ;
    $rec['__name']=$rec['obj_name'] ;
  	$rec['__data']=date("d.m.y",$rec['c_data']) ;
    $arr_data=getdate($rec['c_data']) ;
    $rec['__data_1']=$arr_data['mday'].' '.$_SESSION['list_mon_short_names'][$arr_data['mon']].' '.$arr_data['year'] ;
    $rec['__level']=$this->tree[$rec['parent']]->level+1 ;

    if ($this->table_values)  $rec['tags']=execSQL_row('select pkey,value_id as id from '.$this->table_values.' where parent='.$rec['pkey'].' and list_id='.$this->tags_list_id) ;
    if ($this->table_parents) $rec['sections']=execSQL_row('select pkey,section_id as id from '.$this->table_parents.' where parent='.$rec['pkey']) ;

    //damp_array($rec) ;
    /*
    if (!$rec['value'])
    { $files=execSQL_van('select * from obj_site_artikle_files where parent='.$rec['pkey']) ;
      if ($files['pkey']) $rec['__href']='/public/art/files/'.$files['file_name'] ;
    }*/
    // если к статье не подцелены фото, извекаем фото из текста статьи
    if (!$rec['_img_name']) $rec['_img_url']=get_first_img_of_content($rec['value']) ;

    // если не заполнен текст статьи, берем его из аннотации
    if (!trim($rec['value'])) $rec['value']=$rec['annot'];
    if ($rec['value'] or $rec['annot'])
      {   $annot=ltrim($rec['annot'],'&nbsp;') ;
          $annot=($options['annot_strip_tags'])? strip_tags($annot):$annot ;
          $value=ltrim($rec['value'],'&nbsp;') ;
          if ($annot) $rec['__annot']=$annot ;
          else { if (!$options['no_use_value_to_annot']) // 19.11.2010 - добавлена опция запрета использования текста новости для аннатации
                 { $rec['__annot']=($options['annot_strip_tags'])? strip_tags($value):$value ;
        // если указана максимальная длина аннотации, сокращаем анотации до необходимой длины
                   if ($max_annot_length and strlen($rec['__annot'])>$max_annot_length) $rec['__annot']=mb_substr($rec['__annot'],0,$max_annot_length).' ...' ;

                 }
               }

        if (is_object($_SESSION['makros_system'])) $_SESSION['makros_system']->exec($value,$rec) ;
        $rec['__value']=$value ;
    }
  }

  function get_array_search_usl($text)
  { $_usl=array() ;
    $_usl[]=$this->prepare_search_usl('obj_name','like%%',$text) ;
    $_usl[]=$this->prepare_search_usl('value','like%%',$text) ;
    $_usl[]=$this->prepare_search_usl('annot','like%%',$text) ;
    $_usl[]=$this->prepare_search_usl('pkey','=',$text) ;
    return($_usl) ;
  }

  // возвращает id статей, содержащих указанные в $str_tags_id теги
  // id отсортированы в обратном порядке по pkey
  // $str_tags_id - коды тегов, через запятую: 1,15,22,17
  // $options['count'] - ограничение числа выдаваемых статей
  function get_art_by_tags($str_tags_id,$options=array())
  {  //
     $sql='select distinct(parent) as id from '.$this->table_values.' where list_id='.$this->tags_list_id.' and value_id in ('.$str_tags_id.') order by id desc' ;
     if ($options['count']) $sql.=' limit '.$options['count'] ;
     $arr=execSQL_line($sql) ;
     return($arr) ;
  }
}
///art/russia/topnews/priznaniya-razrushiteley-sssr:-m_4632.html
///art/v-rossii/vazhno/priznaniya-razrushiteley-sssr:-m_4632.html

include_once(_DIR_TO_ENGINE.'/class/clss_0.php') ;
class clss_11 extends clss_0
{
    // подготовка url для страницы объекта
    function prepare_url_obj($rec)
    {   //$url_name=substr(safe_text_to_url($rec['obj_name'],$rec['tkey']),0,32).'_'.$rec['pkey'] ;
        $url_name=substr(safe_text_to_url($rec['obj_name'],$rec['tkey']),0,48) ;
        return($url_name) ;
    }

  // возвращает текст html кнопки для создания объекта
  // часть классов создают новые объекты через модальное окно - у них метод будет переопределен
  function get_create_button_html($parent_reffer)
    { $text='<button mode=replace_viewer_main cmd=create_obj clss="'.$this->clss.'" parent_reffer="'.$parent_reffer.'">Создать еще '.$this->name($parent_reffer).'</button>'  ;
      return($text) ;
    }

  // возвращает текст html ссылки для создания объекта
  function get_create_a_html($parent_reffer)
    { $text='<a href="#" class=button mode=replace_viewer_main  cmd=create_obj clss="'.$this->clss.'" parent_reffer="'.$parent_reffer.'">'.$this->name($parent_reffer).'</a>'  ;
      return($text) ;
    }

    // в качестве $options передается $_POST
  function _obj_create_dialog($options=array())
     { $form_id='form_obj_create_dialog_clss_'.$this->clss ;
       include_once(_DIR_TO_ENGINE.'/admin/i_clss_func.php') ;
       ?><form method="POST" action="ajax.php" id=<?echo $form_id?>>
         <h2>Название статьи</h2><input type=text class="text big" name=new_obj[obj_name] data-required>
         <? if (isset($_SESSION['descr_clss'][9]['details']['field']['annot']))  { ?><h2>Аннотация</h2><textarea class=big name=new_obj[annot]></textarea><br><?/* show_window_ckeditor_v3(array(),'',100,'new_obj[annot]') ; */}?>
         <h2>Содержание</h2><textarea class=big name=new_obj[value]></textarea><br>
         <?/* show_window_ckeditor_v3(array(),'',400,'new_obj[value]') ; */?>
         <input type=submit id=submit  class=button_green mode=append_viewer_main cmd=create_obj_after_dialog clss="<?echo $this->clss?>" parent_reffer="<?echo $_POST['parent_reffer']?>" value="Создать статью">
         и <? show_select_action('after_clss_'.$_POST['clss'].'_create_action',array('go_list'=>'перейти к списку статей','go_this'=>'перейти к созданной статье','go_create'=>'создать еще одну статью')) ;?>
         <script type="text/javascript">new mForm.Submit({form:'<?echo $form_id?>',validateOnBlur:true,blinkErrors:true,shakeSubmitButton: true,submitButton:$('submit'),ajax: true, showLoader: true, responseSuccess:'any',onSuccess:function(responseText, responseXML){if (responseXML!=null) mForm_Submit_AJAX_onSuccess(responseXML);}});</script>
        </form>
       <?
       return(0) ;
     }
}


?>