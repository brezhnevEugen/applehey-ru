<?php
$__functions['init'][]		='_art_admin_vars' ;
$__functions['install'][]	='_art_install_modul' ;
$__functions['boot_admin'][]='_art_admin_boot' ;


function _art_admin_vars() //
{
	// статьи
	$_SESSION['descr_clss'][11]['name']='Инфо' ;
	$_SESSION['descr_clss'][11]['parent']=0 ;

	$_SESSION['descr_clss'][11]['fields']=array('value'=>'mediumtext','annot'=>'text','top'=>'int(1)','url_name'=>'text') ;
    $_SESSION['descr_clss'][11]['fields']['c_data']=array('type'=>'timedata','datepicker'=>1,'data_format'=>'d.m.y') ;
	$_SESSION['descr_clss'][11]['fields']['is_comment']='int(1)' ; // разрешить комментарии к статье
	$_SESSION['descr_clss'][11]['fields']['is_ping']='int(1)' ; // разрешить Трекбек к статье // http://wpcafe.org/hacks/trekbeki-trackbacks-v-wordpress-chto-eto-takoe/
    $_SESSION['descr_clss'][11]['fields']['source']=array('type'=>'varchar(255)') ;
    $_SESSION['descr_clss'][11]['fields']['source_url']=array('type'=>'varchar(255)') ;

	$_SESSION['descr_clss'][11]['child_to']=array(1) ;
	$_SESSION['descr_clss'][11]['parent_to']=array(3,5) ;
	$_SESSION['descr_clss'][11]['SEO_tab']=1 ;
    //$_SESSION['descr_clss'][11]['no_show_in_tree']=1 ; // не показывать объекты класса в дереве

    $_SESSION['descr_clss'][11]['icons']=_PATH_TO_BASED_CLSS_IMG.'/7.png' ;

	$_SESSION['descr_clss'][11]['list']['field']['pkey']			='Код' ;
	$_SESSION['descr_clss'][11]['list']['field']['enabled']			='Сост.' ;
	$_SESSION['descr_clss'][11]['list']['field']['top']				='ТОР' ;
	$_SESSION['descr_clss'][11]['list']['field']['c_data']			='Создана' ;
	$_SESSION['descr_clss'][11]['list']['field']['indx']			='Позиция' ;
	$_SESSION['descr_clss'][11]['list']['field']['obj_name']		=array('title'=>'Наименование','class'=>'small','td_class'=>'justify') ;
    $_SESSION['descr_clss'][11]['list']['field']['annot']		    =array('title'=>'Аннотация','class'=>'big','td_class'=>'justify') ;
    $_SESSION['descr_clss'][11]['list']['field']['source']			=array('title'=>'Источник','td_class'=>'justify') ;
   	$_SESSION['descr_clss'][11]['list']['field']['source_url']	    =array('title'=>'URL источника','class'=>'big','td_class'=>'justify') ;


	$_SESSION['descr_clss'][11]['details']=$_SESSION['descr_clss'][11]['list'] ;
	$_SESSION['descr_clss'][11]['details']['field']['annot']		=array('title'=>'Аннотация','class'=>'big','td_class'=>'justify','use_HTML_editor'=>1) ;
	$_SESSION['descr_clss'][11]['details']['field']['value']		=array('title'=>'Текст','class'=>'big','use_HTML_editor'=>1) ;

	// статьи
	$_SESSION['descr_clss'][24]['name']='Версия статьи' ;
	$_SESSION['descr_clss'][24]['parent']=11 ;
	$_SESSION['descr_clss'][24]['fields']['comment']='text' ;
	$_SESSION['descr_clss'][24]['fields']['use_parent_attachent']=array('type'=>'int(1)','default'=>1) ;
    $_SESSION['descr_clss'][24]['parent_to']=array(3,5) ;
    $_SESSION['descr_clss'][24]['table_code']='versions' ;

    $_SESSION['descr_clss'][24]['icons']=_PATH_TO_BASED_CLSS_IMG.'/7.png' ;

	$_SESSION['descr_clss'][24]['list']=$_SESSION['descr_clss'][11]['list'] ;
	$_SESSION['descr_clss'][24]['list']['field']['comment']			    ='Комментарий к версии.' ;

	$_SESSION['descr_clss'][24]['details']=$_SESSION['descr_clss'][11]['details'] ;
    $_SESSION['descr_clss'][24]['details']['field']['comment']			    ='Комментарий к версии.' ;
    $_SESSION['descr_clss'][24]['details']['field']['use_parent_attachent']=array('title'=>'Использовать изображения<br> и файлы рабочей версии') ;

	//----------------------------------------------------------------------------------------------------------------------
	// описание пунктов меню админа сайта
	//-----------------------------------------------------------------------------------------------------------------------

	$_SESSION['menu_admin_site']['Модули']['art']=array('name'=>'Информационный','href'=>'editor_artikle.php','icon'=>'menu/newspaper.jpg') ;
}

function _art_admin_boot($options)
{   $options['no_create_model']=1 ;
	create_system_modul_obj('art',$options) ;
}


function _art_install_modul($DOT_root)
{   echo '<h2>Инсталируем модуль <strong>Каталог статей</strong></h2>' ;

    $file_items=array() ;
    $file_items['editor_artikle.php']['include'][]='_DIR_TO_ENGINE."/admin/c_site.php"' ;
    $file_items['editor_artikle.php']['options']['system']='"art_system"' ;
    $file_items['editor_artikle.php']['options']['title']='"Каталог статей"' ;
    $file_items['editor_artikle.php']['options']['use_class']='"c_editor_obj"' ;
    create_menu_item($file_items) ;

    $pattern=array() ;
	$pattern['table_title']				= 	'Статьи' ;
	$pattern['use_clss']				= 	'1,11' ;
	$pattern['def_recs'][]				=	array ('parent'=>0,	'clss'=>1,	'enabled'=>1,	'indx'=>0,	'obj_name'=>'Статьи') ;
   	$pattern['table_name']				= 	$_SESSION['TM_artikle'] ;
	$pattern['table_clss']				= 	101 ;
	$pattern['table_parent']			= 	$DOT_root;
   	create_table_by_pattern($pattern) ;

    SETUP_get_img('/art/admin_img_menu/newspaper.jpg',_PATH_TO_ADMIN.'/img/menu/newspaper.jpg') ;

    create_dir('/art/') ;
    SETUP_get_file('/art/system_dir/htaccess.txt','/art/.htaccess');
    SETUP_get_file('/art/system_dir/index.php','/art/index.php');
    SETUP_get_file('/art/class/c_art.php','/class/c_art.php');
    $list_templates_file=analize_class_file_to_templates('/class/c_art.php') ;
    SETUP_get_templates('/art/',$list_templates_file) ;
}



?>