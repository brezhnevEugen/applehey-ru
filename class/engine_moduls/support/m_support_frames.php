<?php
include_once(_DIR_TO_ENGINE."/admin/c_site.php") ;
include_once(_DIR_TO_ENGINE."/admin/i_clss_func.php") ;
include_once(_DIR_TO_ENGINE.'/admin/c_menu_tabs.php') ;

// редактор службы поддержки
class c_editor_support extends c_editor_obj
{
  function body($options=array()) { $this->body_frame_2x(array('fra_tree'=>_PATH_TO_ADMIN.'/editor_support_tree.php','title'=>$options['title'])) ; }
}

// дерево тикетов
class c_editor_support_tree extends c_fra_tree
{ public $system_name='support_system' ;
  function body($options=array())
 {?><body id=tree_objects><div id=tree_menu><?$this->top_menu() ;?></div><?
     $options=array() ;
     $options['usl_select']='clss=16' ;
     $options['on_click_script']='editor_support_viewer.php' ;
     $options['on_click_item']=array(16=>'fra_viewer.php') ;

     $this->generate_time_tree('Служба поддержки',$this->tkey,$options);
  ?></body><?
 }

}

class c_editor_support_viewer extends c_fra_based
{  public $system_name='support_system' ;
   public $top_menu_name='top_menu_list_support' ;

 function check_GET_params()
 { global $support_system ;
   if ($_GET['reffer'])
   {  list($id,$tkey)=explode('.',$_GET['reffer']) ;
      $tiket=$support_system->get_tiket_info_by_id($id) ;
      //damp_array($tiket) ;
      header("HTTP/1.1 301 Moved Permanently");
      header("Location: ".$tiket['__moderator_url']);
      _exit() ;
   } else parent::check_GET_params() ;
 }

 function body(&$options=array()) {$this->body_frame($options) ; }

// заголовок фрейма
 function show_page_obj_header()
 { $title=($_GET['pkey']=='root')? 'Служба поддержка: все тикеты':'Служба поддержка: тикеты '.$this->get_time_title($this->start_time,$this->end_time) ;
   ?><p class='obj_header'><?echo $title?></p><?
 }
    
 // меню фрейма
 function show_page_obj_menu($obj_info)
    {  global $support_system ;
      // создаем объект top_menu
       if (!is_object($_SESSION[$this->top_menu_name])) $_SESSION[$this->top_menu_name]=new c_top_menu() ;
       // формируем набор пунктом меню
       $menu_set=array() ;
       // пункты меню для корня дерева
       if ($_GET['pkey']=='root')
       { $menu_set[]=array("name" => 'Неотвеченные тикеты',"cmd" =>'show_list_items','status'=>0) ;
         $menu_set[]=array("name" => 'Отвеченные тикеты',"cmd" =>'show_list_items','status'=>1) ;
         $menu_set[]=array("name" => 'Поиск по тикетам',"cmd" =>'show_list_items','no_show_list'=>1) ;
       }
       // или если выбран один из временных интервалов
       else
       { $time_usl=$this->get_time_usl($this->start_time,$this->end_time) ;
         $arr_status=$_SESSION['support_system']->get_count_by_status($time_usl) ;
         if (sizeof($arr_status)) foreach($arr_status as $status=>$cnt) {$menu_set[]=array("name" => $_SESSION['list_support_status'][$status].'('.$cnt.')','cmd' =>'show_list_items','status'=>$status) ; }
         $menu_set[]=array("name" => 'Все ('.array_sum($arr_status).')',"cmd" =>'show_list_items') ;
       }
       // выводим меню
       $this->cur_menu_item=$_SESSION[$this->top_menu_name]->show($menu_set) ;  // показываем меню
       return($this->cur_menu_item) ;
    }    

    function show_list_items()
    { $options=array() ; $usl='' ; $_usl=array() ;
      if ($this->start_time or $this->end_time) $_usl[]=$this->get_time_usl($this->start_time,$this->end_time) ;
      if (isset($this->cur_menu_item['status'])) $_usl[]='status='.$this->cur_menu_item['status'] ;
      if (sizeof($_usl)) $usl=implode(' and ',$_usl) ;
      $title='Тикеты '.$this->get_time_title($this->start_time,$this->end_time) ;
      if (isset($this->cur_menu_item['status'])) $title.=' в статусе "'.$_SESSION['list_support_status'][$this->cur_menu_item['status']].'"' ;
      if ($this->cur_menu_item['mode']=='last_items')
      { $options['count']=$this->cur_menu_item['count'] ;
        $options['order']='c_data desc' ;
        $title.=' - последние '.$this->cur_menu_item['count'] ;
      }
      $options['default_order']='c_data desc' ;
      $options['title']=$title ;
      $options['buttons']=array('save','delete') ;
      if (!$this->cur_menu_item['no_show_list'] and $usl) _CLSS(16)->show_list_items($this->tkey,$usl,$options) ;
    }

    function panel_fast_search()
       { ?><div id=panel_search script_url="<?echo _PATH_TO_ADMIN?>/editor_support_ajax.php" cmd="get_list_tiket">
                <strong>ПОИСК:</strong>
                <input type=text name=text_search class="text"><img src="<?echo _PATH_TO_ENGINE?>/admin/images/find_icon.gif" class=go_search>
                <input type=radio name=target_search value=autor checked>По автору
                <input type=radio name=target_search value=content>По содержанию
                <!--<input type=radio name=target_search value=goods>По товару-->
           </div>
         <?
       }

}

// старый фрейм службы поддержки
class c_editor_support_viewer2 extends c_fra_viewer
{



 function block_main()
    {   global $obj_info,$object_menu,$search_text,$table_name,$status_usl,$time_usl,$tkey,$status_title,$title ;

        if ($obj_info['clss']==16) // если текущий объект - тикет, то выводим соответсвующий заголовк
           {if (sizeof($obj_info['obj_clss_15'])) $cnt_text=' ('.sizeof($obj_info['obj_clss_15']).')' ;
	      	?><p class='obj_header'>Тикет № <? echo $obj_info['pkey'];?> от <? echo date("d.m.y G:i",$obj_info['c_data']);?></p><?
	      	$menu_set[1]=array("name" => 'Сообщения',"action" => 2001) ;
			//$menu_set[2]=array("name" => 'Журнал',"action" => 2002) ;
            $menu_res=$object_menu->show($obj_info,array('menu_set'=>$menu_set)) ;


	    ?><div id="viewer_main">Быстрый поиск <input name="search_text" type="text" value="<?echo $search_text?>" class=text> <input type="submit" value="Искать"><br><?
	    if ($search_text) { ?><br><?
     						$list_tikets=execSQL_line('select pkey from '.$table_name.' where '.$status_usl.$time_usl.' and clss=16 and (obj_name like "%'.$search_text.'%" or autor_name like "%'.$search_text.'%" or autor_phone like "%'.$search_text.'%" or autor_email like "%'.$search_text.'%")') ;
     						//print_r($list_tikets) ;
     						echo '<span class=green>Найдено тем (в имени автора или телефона или email):</span><span class=black> '.sizeof($list_tikets).'</span><br>' ;
     						$pkey_all_tikets=execSQL_line('select pkey from '.$table_name.' where clss=16 and '.$status_usl.$time_usl) ;
     						if (sizeof($pkey_all_tikets))
     						{ $list_pkeys_all=implode(',',$pkey_all_tikets) ; // echo 'list_pkeys_all='.$list_pkeys_all.'<br>' ;
     						  $list_messages=execSQL('select pkey,parent from '.$table_name.' where clss=15 and parent in ('.$list_pkeys_all.') and obj_name like "%'.$search_text.'%"') ;
     						  if (sizeof($list_messages)) foreach($list_messages as $rec) if (!in_array($rec['parent'],$list_tikets)) $list_tikets[]=$rec['parent'];
     						  //print_r($list_tikets) ;
     						}
     						echo '<span class=green>Найдено сообщений (в тексте сообщения):</span><span class=black> '.sizeof($list_messages).'</span><br>' ;
     						if (sizeof($list_tikets)) $search_usl=' and pkey in ('.implode(',',$list_tikets).')' ;
	    				  }  else $search_usl='' ;

        //$this->show_all_tiket($status_usl.$time_usl.$search_usl) ;

        $list_tiket=execSQL('select *,'.$tkey.' as tkey from '.$_SESSION['TM_support'].' where clss=16 and '.$status_usl.$time_usl.$search_usl.' order by c_data desc') ;
		//set_reffer_values($list_tiket,16) ;
		_CLSS(16)->show_list_items($list_tiket,array('no_check'=>0,'no_page_panel'=>1,'title'=>'Тикеты '.$status_title.$title)) ;
        //damp_array($list_tiket) ;
        if (sizeof($list_tiket))
            ?><button class=button cmd=save_list>Сохранить</button><button onclick="exe_cmd('reply_uvedomlen')">Повторить уведомление менеджеру</button><?}

           ?></div><?

    }






}


