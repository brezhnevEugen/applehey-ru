<?php
$__functions['init'][]='_support_site_vars' ;
$__functions['boot_site'][]='_support_site_boot' ;

function _support_site_vars()
{
	$_SESSION['TM_support']= 'obj_'.SITE_CODE.'_support' ;
    $_SESSION['TM_support_files']='obj_'.SITE_CODE.'_support_files' ;
    $_SESSION['list_support_status']=array('0'=>'Не отвечен','1'=>'Отвечен','2'=>'Закрыт') ;

    // описание системы службы поддержки  -------------------------------------------------------------------------------------------------------------------
    $_SESSION['init_options']['support']=array() ;
    $_SESSION['init_options']['support']['use_capcha_code']=1 ;
    $_SESSION['init_options']['support']['debug']=0 ;
    $_SESSION['init_options']['support']['usl_show_items']='clss in (16,15)' ;
    $_SESSION['init_options']['support']['system_title']='Служба поддержки' ;
    $_SESSION['init_options']['support']['system_href']='/support/' ;
    $_SESSION['init_options']['support']['moderator_name']='Администрация сайта' ;
    $_SESSION['init_options']['support']['moderator_email']=$_SESSION['LS_support_system_email_alert'] ;
}


function _support_site_boot($options)
{
	create_system_modul_obj('support',$options) ; // создание объекта support_system
}


class c_support_system extends c_system
{  public $moderator_name ;
   public $moderator_email ;

 function c_support_system($create_options)
  {  $this->table_name=($create_options['table_name'])? $create_options['table_name']:$_SESSION['TM_support'] ;
     $this->tikets_parent=1 ; // все суппорты - в корень
     $this->use_capcha_code=$create_options['use_capcha_code'] ;
     $this->moderator_name=($create_options['moderator_name'])? $create_options['moderator_name']:'Администрация сайта' ;
     $this->moderator_email=($create_options['moderator_email'])? $create_options['moderator_email']:$_SESSION['LS_support_system_email_alert'] ;
     $this->tkey_files=$_SESSION['pkey_by_table'][$_SESSION['TM_support_files']] ;

     set_time_point('Создание объекта <strong>support_system</strong>') ;
     parent::c_system($create_options) ;
  }

 //-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 //
 // функции для работы с БД
 //
 //-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    function get_count($usl_select='')
    { $cnt=execSQL_value('select count(pkey) as cnt from '.$this->table_name.' where clss=16 '.$usl_select,0,0) ;
      return($cnt) ;
    }

    function get_count_by_status($usl_select='')
    { $sql='select status,count(pkey) as cnt from '.$this->table_name.' where clss=16 ' ;
      if ($usl_select!="") $sql.=' and '.$usl_select ;
      $sql.=' group by status' ;
      $cnt_arr=execSQL_row($sql) ;
      return($cnt_arr) ;
    }

    // приводим попдсистему к стандартному виду
    function get_items($usl,$options=array())
    { $list_tikets=parent::get_items($usl,$options) ;
      //damp_array($list_tikets) ;
      if (sizeof($list_tikets))
      { $tiket_pkeys=implode(',',array_keys($list_tikets)) ;
        //echo '$tiket_pkeys='.$tiket_pkeys.'<br>' ;
        $list_messages=select_objs($this->tkey,'','clss=15 and parent in ('.$tiket_pkeys.') and enabled=1',array('group_by'=>'parent','order_by'=>'c_data')) ;
        //damp_array($list_messages) ;
        if (sizeof($list_messages[$this->tkey])) foreach($list_messages[$this->tkey] as  $tiket_pkey=>$tiket_messages) $list_tikets[$tiket_pkey]['messages']=$tiket_messages ;
      }
      return($list_tikets) ;
     }

    function get_tiket_info_by_uid($uid)
    {   $tiket=execSQL_van('select * from '.$this->table_name.' where clss=16 and (uid="'.$uid.'" or edit_code="'.$uid.'")') ;
        if ($tiket['pkey'])
        { $tiket['messages']=$this->get_list_mess($tiket['pkey']) ;
          $tiket['open_mode']=($tiket['uid']==$uid)? 0:1 ; // 0 - открыт автором, 1 - отвечающим
          $tiket['open_uid']=($tiket['uid']==$uid)? $tiket['uid']:$tiket['edit_code'] ; // uid использованный для открытия тикета
          $this->prepare_public_info($tiket) ;
        }
        return ($tiket)  ;
    }

    function get_tiket_info_by_id($id)
    {  $tiket=execSQL_van('select * from '.$this->table_name.' where clss=16 and pkey="'.$id.'"') ;
        if ($tiket['pkey'])
        { $tiket['messages']=$this->get_list_mess($tiket['pkey']) ;
          $this->prepare_public_info($tiket) ;
        }
        return ($tiket)  ;
    }


    function get_list_mess($id)
    {   $mess_list=execSQL('select * from '.$this->table_name.' where clss=15 and parent="'.$id.'" order by c_data') ; // выбираем все сообщения темы
        return($mess_list) ;
    }

    function get_list_tiket($usl,$options=array())
     { $tikets=execSQL('select * from '.$this->table_name.' where clss=16 and '.$usl) ;
       if (sizeof($tikets)) foreach($tikets as $id=>$tiket) $this->prepare_public_info($tikets[$id],array('no_get_messages'=>1,'no_reffer_info'=>1)) ;
       return($tikets) ;
     }


  // готовим информацию по объекту - эта функция должны быть перекрыта в кажом классе
  function prepare_public_info(&$rec,$options=array())
  { // готовим информацию по тикету
    if ($rec['clss']==16)
    {   $arr_autor_info=array() ;  $autor_info_db=array() ;
        $rec['__theme']=$rec['obj_name'] ;
        $rec['obj_name']='Тикет № '.$rec['pkey'].' от '.date("d.m.y G:i",$rec['c_data']) ;
        $rec['__name']=$rec['obj_name'] ;
        $rec['__data']=date("d.m.y h:i",$rec['c_data']) ;
        if (is_array($rec['autor_info'])) $autor_info_db=$rec['autor_info'] ;
        elseif (strlen($rec['autor_info'])) $autor_info_db=unserialize($rec['autor_info']) ; ;
        if ($rec['autor_name']) $arr_autor_info['ФИО']=$rec['autor_name'] ;
        if ($rec['autor_email']) $arr_autor_info['Email']=$rec['autor_email'] ;
        if ($rec['autor_phone']) $arr_autor_info['Телефон']=$rec['autor_phone'] ;
        if (sizeof($autor_info_db)) $arr_autor_info=array_merge($arr_autor_info,$autor_info_db) ;
        $rec['__autor_info']=damp_serial_data($arr_autor_info);
        $rec['__autor_url']='http://'._MAIN_DOMAIN.'/support/tiket.php?id='.$rec['uid']; ;
        $rec['__autor_url_']='/support/tiket.php?id='.$rec['uid']; ;
        $rec['__moderator_url']='http://'._MAIN_DOMAIN.'/support/tiket.php?mid='.$rec['edit_code']; ;
        $rec['__moderator_url_']='/support/tiket.php?mid='.$rec['edit_code']; ;

        if (!$rec['otvet_email']) $rec['otvet_email']=$_SESSION['LS_support_system_email_alert'] ; // на случай если не указан email администратора

        if ($rec['obj_reffer'])  { $rec['obj_reffer_info']=select_db_ref_info($rec['obj_reffer'],array('no_text_fields'=>1)) ;
                                   unset($rec['obj_reffer_info']['manual']) ;
                                   unset($rec['obj_reffer_info']['intro']) ;
                                 }
    }
    // готовим информацию по сообщению
    if ($rec['clss']==15)
    { $rec['_list_files']=execSQL('select * from '.$_SESSION['TM_support_files'].' where parent='.$rec['pkey']) ;

    }
  }

 //-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 //
 // функции вызываемые при обработке страниц подсистемы
 //
 //-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 function select_obj_info($_CUR_PAGE_DIR,$_CUR_PAGE_NAME='',$options=array())
 {
   if ($_GET['id'])    $rec=$this->get_tiket_info_by_uid($_GET['id']) ; // получаем информацию по текущему тикету
   if ($_GET['mid'])   $rec=$this->get_tiket_info_by_uid($_GET['mid']) ; // получаем информацию по текущему тикету

   if (($_GET['mid'] or $_GET['id']) and !$rec['pkey'])
   { $rec=prepare_result('theme_not_found') ; // одна система
     $rec['__open_result']=prepare_result('theme_not_found') ; //  вторая система
   }
   else
   { $result['http_code']=200 ;
     $result['cur_obj']=$rec ;
     return($result) ;
   }
   // в дальнейшем передалить систему авториризованного доступа к тикетам
   /*  if ($support_system->use_autorize_to_otwet and $obj_info['open_mode']==1 and !$member->id)
        {	?>Для совершения ответа на вопрос необходимо авторизоваться на сайте<?
        		account_panel_login_promp() ;
         	return ;
        }
   */
 }

 //-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 //
 // функции создания тикета и все связанное с этим
 //
 //-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------


 // создаем новую ветку для текущего пользователя
 function create_tiket($data,$options=array())
 { global $member ;
   $this->save_form_values_to_session($options) ;
   $debug=$options['debug'] ;
   // если заполнены не все поля, показать ошибку
   if (!$member->id and ($data['name']=='' or $data['email']=='')) return(prepare_result('error_field')) ;
   // проверяем правильность проверочного кода
   if ($this->use_capcha_code) if (!capcha_code_checkit()) return(prepare_result('error_check_code')) ;
   // содаем тикет
   if (isset($options['use_klient_reffer'])) $klient_reffer=$options['use_klient_reffer'] ; else $klient_reffer=($member->id)? $member->reffer:'' ;
   // задаем email, на который будет отправлятся уведомление по тикету. Это может быть как заданный в настройках email для уведомлений, так и  email менеджера
   $otvet_email=($data['otvet_email'])? $data['otvet_email']:$this->moderator_email ;
   // создаем новый тикет
   $tiket=array(	'parent'		=>	$this->tikets_parent, // код папки новых веток
			        'clss'			=>	16, // 16 - ветка
     				'obj_name'		=>	($data['theme'])? $data['theme']:'Вопрос в службу поддержки',
     				'c_data'		=>	time(),
    				'account_reffer'=> 	$klient_reffer,
    				'uid'			=>  md5(uniqid(rand(),true)),
    				'autor_email'	=>  $data['email'],
    				'autor_name'	=>  $data['name'],
    				'autor_phone'	=>  $data['phone'],
    				'autor_info'	=>  $data['autor_info'], // расширенная информация по автору тикета
    				'otvet_email'	=>  $otvet_email, // email для кого тикет. В дальшейнем используется для отправки уведомлений о новых вопросах клиета
    				'status'		=>	0,     // =0 - новый, не отвечен
    				'obj_reffer'	=>	$data['_reffer'], // объект на котором был задан вопрос
                    'obj_reffer_name'=>	$data['_reffer_name'], // название объекта
                    'obj_reffer_href'=>	$data['_reffer_href'], // с какой страницы был сделал запрос
                    'obj_reffer_parent'=>$data['_reffer_parent'], // раздел, с товара которого был задан вопрос
                    'edit_code'		 =>  md5(uniqid(rand(),true))
   				) ;

   if ($debug) damp_array($tiket) ;
   $tiket['pkey']=adding_rec_to_table($this->tkey,$tiket) ; // создаем новую ветку
   $tiket['_reffer']=$tiket['pkey'].'.'.$this->tkey ;
   $tiket['message']=strip_tags($data['value']) ;
   if (!$tiket['pkey']) return(prepare_result('error_create_tiket')) ;

   //echo 'Была создана новая ветка '.$this->reffer.'<br>' ;
   // регистрируем событие 'Открытие существующей темы'
   $evt_id=_event_reg('Создание тикета в службе поддержки','',$tiket['_reffer'],$tiket['account_reffer']) ;

   // добавляем в ветку сообщение
 	$finfo=array(	'clss'			=>	15,
      				'parent'		=>	$tiket['pkey'],
     				'obj_name' 		=> 	$tiket['message'],
                    'autor_email'	=>  $data['email'],
             	    'autor_name'	=>  $data['name'],
             		'autor_phone'	=>  $data['phone'],
     				'status'		=>	0
    			  ) ;

    if ($options['use_autor_reffer']) 	$finfo['account_reffer']=$options['use_autor_reffer'] ;
    if ($options['use_message_status']) $finfo['status']=$options['use_message_status'] ;

    if ($debug) damp_array($finfo) ;
    //damp_array($finfo) ;

    $mess_id=adding_rec_to_table($this->tkey,$finfo) ;

    //загружаем файлы есть есть. Файлы прикрепляем к сообщению.
    if (sizeof($data['upload_files']))  foreach($data['upload_files'] as $rec_file) if (!$rec_file['error'])  $this->tiket_append_file($mess_id.'.'.$this->tkey,$rec_file) ;

   $result['code']='create_tiket_success' ;
   $result['type']='success' ;
   $this->prepare_public_info($tiket) ;
   $result['rec']=$tiket ;

   $options['use_event_id']=$evt_id ;
   $this->send_mail_support($tiket,$options) ;

   return($result) ;
 }

 function tiket_append_file($obj_reffer,$rec)
    {  include_once(_DIR_TO_ENGINE.'/admin/i_admin_system.php') ;
       $obj_rec=_CLSS(5)->obj_create(array(),array('parent_reffer'=>$obj_reffer)) ;
       $result=obj_upload_file($obj_rec['_reffer'],$rec,array('view_upload'=>0,'debug'=>0,'no_comment'=>1)) ;
       return($result) ;
    }


 function close_tiket(&$tiket_rec)
 { $sql='update '.$this->table_name.' set status=2 where pkey='.$tiket_rec['pkey'] ;
   execSQL_update($sql) ;
   _event_reg('Закрытие тикета','',$tiket_rec['_reffer']) ;
   $tiket_rec['status']=2 ;
 }

 // продолжает существующую тему
 // внимание - поле autor для сообщения заполняется только для персонала сайта
 // указываем uid тикета, для которого необходимо добавить сообщение
 // функция возвращает массив - описание тикета
 function add_message($uid,$rec=array(),$tiket_files=array())
 { global $member ;
   if (is_array($uid)) { $rec=$uid ;  $uid=$rec['uid'] ; } // новый формат вызова функции: $result=$support_system->add_message($_POST) ;
   // открываем тикет
   $tiket_rec=$this->get_tiket_info_by_uid($uid) ;
   if (!$tiket_rec['pkey']) return(prepare_result('tiket_not_found')) ;
   if (!$rec['value']) return(prepare_result('error_field')) ; // если заполнены не все поля, показать ошибку

   // после открытия тикета  $tiket_rec['open_mode']:  0 - открыт автором, 1 - отвечающим
   // если не переданы поля name,email,phone то необходимо эти поля заполнить автоматически
   // если открыт автором, то берем данные из тикета
   // если открыт отвечающим - пишем "Администрация сайта" ($this->admin_podpis) ;
   $name=$rec['name'] ;     if (!$name)     $name=($tiket_rec['open_mode'])? $this->moderator_name:$tiket_rec['autor_name'] ;
   $email=$rec['email'] ;   if (!$email)    $email=($tiket_rec['open_mode'])? $this->moderator_email:$tiket_rec['autor_email'] ;
   $phone=$rec['phone'] ;   if (!$phone)    $phone=($tiket_rec['open_mode'])? '':$tiket_rec['autor_phone'] ;

   $finfo=array('parent'        =>	$tiket_rec['pkey'],
				'enabled'		=>	1,
				'clss'			=>	15, // 15 - сообщение
	     		'obj_name'		=>	$rec['value'],
                'autor_email'	=>  $email,
	            'autor_name'	=>  $name,
		        'autor_phone'	=>  $phone,
	     		'status'		=>	$tiket_rec['open_mode'] // 0 - вопрос, 1 - ответ
	   	        ) ;

   if ($member->id)	  $finfo['account_reffer']=$member->reffer ;

   $mess_id=adding_rec_to_table($this->tkey,$finfo) ; // продолжаем новую ветку
   $mess_reffer=$mess_id.'.'.$this->tkey ;

   // обновляем статус тикета
   update_rec_in_table($this->tkey,array('status'=>$tiket_rec['open_mode'],'public'=>$rec['public']),'pkey='.$tiket_rec['pkey']) ;

   //загружаем файлы есть есть
   include_once(_DIR_TO_ENGINE.'/admin/i_admin_system.php') ;
   if (sizeof($tiket_files))  foreach($tiket_files as $i=>$tmp_name) if ($tmp_name)
   {   $obj_file_ref=_CLSS(5)->obj_create(array('parent'=>$mess_id,'clss'=>5,'enabled'=>1),array('tkey'=>$this->tkey_files)) ;
       $file_info=array('name'=>$_FILES['tiket_files']['name'][$i],'type'=>$_FILES['tiket_files']['type'][$i],'tmp_name'=>$_FILES['tiket_files']['tmp_name'][$i],'error'=>$_FILES['tiket_files']['error'][$i],'size'=>$_FILES['tiket_files']['size'][$i]) ;
       obj_upload_file($obj_file_ref[1].'.'.$obj_file_ref[0],$file_info) ;
   }

   $evt_id=_event_reg('Добавление нового сообщения в тикет','',$tiket_rec['_reffer'],$mess_reffer) ;

   // регистрируем событие 'Добавление нового сообщения в тему'
   if (!$tiket_rec['open_mode']) $this->send_mail_support_new_message($tiket_rec,$rec['value'],array('use_event_id'=>$evt_id,'debug_mail'=>$rec['debug_mail'])) ;
   else                          $this->send_mail_support_new_otvet($tiket_rec,$rec['value'],array('use_event_id'=>$evt_id,'debug_mail'=>$rec['debug_mail'])) ;

   $result['code']=($tiket_rec['open_mode'])? 'create_otwet_success':'create_wopros_success' ;
   $result['type']='success' ;
   $result['rec']=$tiket_rec ;
   return($result) ;
 }



 // создание тикета - отравляем два письма - уведомление клиенту и менежеру
 function send_mail_support($tiket,$options=array())
 { $params=array() ; $arr_autor_info=array() ;
   //damp_array($tiket) ;
   $params['site_name']=$_SESSION['LS_public_site_name'] ;
   $params['theme']=$tiket['__theme'] ;
   $params['name']=$tiket['autor_name'] ;
   if ($tiket['obj_reffer_name']) $arr_autor_info['Запрос по']='<a href="'.$tiket['obj_reffer_href'].'">'.$tiket['obj_reffer_name'].'</a>' ;
   $params['autor_info']=$tiket['__autor_info'] ; // подготавливается в prepare_public_info
   $params['support_id']=$tiket['pkey'] ;
   $params['support_data']=date("d.m.Y H:i",$tiket['c_data']) ;
   $params['support_href']=$tiket['__autor_url'] ;
   $params['to']=($tiket['obj_reffer_name'])? 'на "<a href="http://'._MAIN_DOMAIN.$tiket['obj_reffer_href'].'">'.$tiket['obj_reffer_name'].'</a>"':' ' ;
   $params['mail_bottom_podpis']=$_SESSION['LS_mail_bottom_podpis'] ;
   // если установлена эта опция отправляем тока одно письмо у указанным кодом
   if ($options['use_mail_code'])
   {  $to=($options['mail_to'])? $options['mail_to']:$tiket['autor_email'] ;
      //$options['debug_mail']=1 ;
   	 _send_mail_to_pattern($to,$options['use_mail_code'],$params,$options) ;
     return ;
   }

   // отправляем письмо клиенту о созданию тикета
   _send_mail_to_pattern($tiket['autor_email'],'support_system_tiket_create_to_member',$params,$options) ;

   // отправляем уведомление менеджеру
   $params['support_href']=$tiket['__moderator_url'] ;
   $params['text_mess']=nl2br($tiket['message']) ;
   _send_mail_to_pattern($tiket['otvet_email'],'support_system_tiket_create_alert',$params,$options) ;
 }

 // отправка сообщения менеджерам о добавлении нового сообщения в тикет
 function send_mail_support_new_message($tiket,$message,$options=array())
 { $params['site_name']=$_SESSION['LS_public_site_name'] ;
   $params['theme']=$tiket['__theme'] ;
   $params['name']=$tiket['autor_name'] ;
   $params['autor_info']=$tiket['__autor_info'] ;
   $params['support_id']=$tiket['pkey'] ;
   $params['support_data']=date("d.m.Y H:i",$tiket['c_data']) ;
   $params['support_href']=$tiket['__moderator_url'] ; ;
   $params['text_mess']=nl2br($message) ;
   $params['to']=($tiket['obj_reffer_name'])? 'на "<a href="http://'._MAIN_DOMAIN.$tiket['obj_reffer_href'].'">'.$tiket['obj_reffer_name'].'</a>"':' ' ;
   $params['mail_bottom_podpis']=$_SESSION['LS_mail_bottom_podpis'] ;
   _send_mail_to_pattern($tiket['otvet_email'],'support_system_tiket_create_alert',$params,$options) ;
 }

 // отправка сообщения клиенту о добавлении нового ответа в тикет
 function send_mail_support_new_otvet($tiket,$message,$options=array())
 { $params['site_name']=$_SESSION['LS_public_site_name'] ;
   $params['theme']=$tiket['__theme'] ;
   $params['name']=$tiket['autor_name'] ;
   $params['autor_info']=$tiket['__autor_info'] ;
   $params['support_id']=$tiket['pkey'] ;
   $params['support_data']=date("d.m.Y H:i",$tiket['c_data']) ;
   $params['support_href']=$tiket['__autor_url'] ;
   $params['text_mess']=nl2br($message) ;
   $params['to']=($tiket['obj_reffer_name'])? 'на "<a href="http://'._MAIN_DOMAIN.$tiket['obj_reffer_href'].'">'.$tiket['obj_reffer_name'].'</a>"':' ' ;
   $params['mail_bottom_podpis']=$_SESSION['LS_mail_bottom_podpis'] ;
   _send_mail_to_pattern($tiket['autor_email'],'support_system_tiket_otvet_to_member',$params,$options) ;
 }

}

?>