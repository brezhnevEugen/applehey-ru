<?
include_once(_DIR_TO_ENGINE.'/class/clss_0.php') ;
class clss_16 extends clss_0
{
    function show_props($obj_rec,$options=array())
    {  _CUR_PAGE()->obj_info=$obj_rec ;
       $this->show_order_info() ;
    }

 // показ тикета
 function show_tiket()
 {  global $support_system,$obj_info ;
    $support_system->prepare_public_info($obj_info) ;
    //damp_array($obj_info) ;

    ?><h1>Тикет <?echo $obj_info['pkey']?> от <?echo date("d.m.y G:i",$obj_info['c_data'])?></h1>
      <div class=tiket_body>
      <input name="_check[<?echo $support_system->tkey?>][16][<?echo $obj_info['pkey']?>]" value="1" type="hidden">
    <?

    $list_messages=$support_system->get_list_mess($obj_info['pkey']) ;
    $this->show_tiket_info($obj_info,sizeof($list_messages)) ;

	?><div class=list_tiket_messages><?
    if (sizeof($list_messages)) foreach($list_messages as $message)
    { $support_system->prepare_public_info($message) ;
      if ($message['status'] and !$message['autor_name']) $message['autor_name']=_BASE_DOMAIN ;
      //$mess_autor=($message['status'])?  $_SERVER['SERVER_NAME'].' - '.generate_obj_text($message['account_reffer'],array('read_only'=>1)) ;
      ?><div class="item <?if ($message['status']) echo 'otvet'?>">
            <div class=title><? echo date("d.m.y G:i",$message['c_data']).' от <strong>'.$message['autor_name'].'</strong>' ; ?></div>
            <div class=mess><?echo nl2br($message['obj_name'])?></div>
            <? if (sizeof($message['_list_files']))
                {  ?><div class="files"><div><strong>Загруженные файлы:</strong></div><?
                    print_template($message['_list_files'],'support/list_files_ul_a_name') ;
                   ?></div><?
                }
        ?>

        </div>
      <?

    }

    ?></div><?
    if ($obj_info['status']<2) {?><h2>ВАШ ОТВЕТ:</h2><textarea class=mess wrap='on' name='mess' type='text'></textarea><?}
    ?> <div class=buttons>
         <?/*if (!$obj_info['public']){?><button cmd=tiket_publication_on class=float_left>Опубликовать тикет</button><?}
           else                     {?><button cmd=tiket_publication_off class=float_left>Отменить публикацию</button><?}
           */
           if ($obj_info['status']<2){?><button cmd=tiket_add_message class=float_left>Добавить ответ</button><?
                                      ?><button cmd=tiket_close class=float_right>Закрыть тикет</button><?}

           ?><button cmd=delete_obj reffer="<?echo $obj_info['_reffer']?>" class=float_right>Удалить тикет</button><?

         ?><div class=clear></div><br><br>
           <button cmd=tiket_send_alert_manager class=float_left>Отправить повторное уведомление менеджеру</button>
           <div class=clear></div>
          </div>
      </div>
      <style type="text/css">
         div.tiket_body{margin-left:20px;}
         textarea.mess{width:600px;height:100px;margin-left:20px;}
         div.buttons{width:600px;margin:15px 0 15px 20px;}
         div.list_tiket_messages{}
         div.list_tiket_messages div.item{}
         div.list_tiket_messages div.item div.title{font-family:Tahoma,Arial,sans-serif;font-size:11px;}
         div.list_tiket_messages div.item div.mess{width:580px;border:1px solid #CECECE;margin:5px 25px;padding:10px;}
         div.list_tiket_messages div.item div.files{border-left:3px solid #235383;margin:5px 25px;padding:0 10px;}
         div.list_tiket_messages div.item.otvet div.mess{background:#FFFACD;}
      </style>
    <?
 }

 function show_tiket_links()
 { global $obj_info,$support_system ;
   $support_system->prepare_public_info($obj_info) ;
   ?><h2>Прямые ссылки на тикет:</h2>
     <p>Ссылка для <span class="green bold">АВТОРА</span> тикета, для добавления нового <span class="green bold">ВОПРОСА</span>: <a href="<?echo $obj_info['__autor_url']?>" target=_blank><?echo $obj_info['__autor_url']?></a></p>
     <br>
     <br>
     <p>Ссылка для <span class="green bold">ОТВЕТА</span> на тикет через сайт: <a href="<?echo $obj_info['__moderator_url']?>" target=_blank><?echo $obj_info['__moderator_url']?></a></p>


  <?
 }

 function show_tiket_info($rec,$count)
 {  global $list_support_status,$support_system ;
    $tiket_autor=($rec['account_reffer'])? generate_obj_text($rec['account_reffer'],array('read_only'=>1)):'<span class="bold green">'.$rec['autor_name'].'</span> ('.$rec['autor_phone'].' '.$rec['autor_email'].')' ;
    $all_count=$support_system->get_count('and autor_email="'.$rec['autor_email'].'"') ;
    if (!$rec['status']) $rec['status']=0 ;
    echo 'Состояние: <span class="red bold">'.(($rec['enabled'])? $list_support_status[$rec['status']]:'Только чтение').'</span>' ;
    if ($rec['public']) echo ', <span class="green bold">Опубликован</span>' ;
    echo '&nbsp;&nbsp;&nbsp;' ;
    echo 'Клиент: '.$tiket_autor.'&nbsp;&nbsp;&nbsp;' ;
    echo 'Всего сообщений в тикете: <span class="red bold size18">'.$count.'</span>&nbsp;&nbsp;&nbsp;' ;
    echo 'Всего тикетов автора: <span class="green bold size18">'.$all_count.'</span><br/><br/>' ;
    //echo 'Тема: <span class="black bold">'.$rec['obj_name'].'</span><br/><br/>' ;
    if ($rec['href']) {?>Тикет открыт со страницы сайта: <a target=_blank href=<?echo _PATH_TO_SITE.'/'.$rec['href']?>><?echo _PATH_TO_SITE.'/'.$rec['href']?></a><br><br><?}
    if ($rec['obj_reffer'])
    { //list($text,$onclick,$td_class)=generate_field_text($support_system->tkey,$rec,'obj_reffer',array('only_text'=>1)) ;
      $obj_rec=select_db_ref_info($rec['obj_reffer']) ;
      echo generate_ref_obj_viewer($obj_rec).'<br/><br/>' ;
    }
    echo get_serial_data($rec['autor_info']) ;
 }


 function tiket_add_message()
 { global $support_system,$obj_info,$member ;
   //damp_array($_POST) ;
   if ($_POST['mess'])
   {   echo 'Добавляем новое сообщение' ;
       $obj_info['open_mode']=1 ; // почеаем, что это ответ
       $info=array() ;
       $info['value']=$_POST['mess'] ;
       $info['name']=$member->name ;
       $info['email']=$member->info['email'] ; if (!$info['email']) $info['email']=$_SESSION['LS_support_system_email_alert'] ;
       $info['phone']=$member->info['phone'] ;
       $info['debug_mail']=1 ;
       ob_start() ;
       $res=$support_system->add_message($obj_info['edit_code'],$info) ;// damp_array($res) ;
       $text=ob_get_clean() ;
       echo ($res['type']=='success')? '<span class="green bold"> - OK</span><br>':'<span class="red bold"> - ERROR ('.$res['code'].')</span><br>' ;
       echo $text ;
   }

   return('ok') ; // будет показан начальный экран
 }

 function tiket_publication_on()
 { global $support_system,$obj_info ;
   echo 'Публикуем тикет' ;
   update_rec_in_table($support_system->tkey,array('public'=>1),'pkey='.$obj_info['pkey']) ;
   $obj_info['public']=1 ;
   return('ok') ; // будет показан начальный экран
 }

 function tiket_publication_off()
 { global $support_system,$obj_info ;
   echo 'Отменяем публикацию тикета' ;
   update_rec_in_table($support_system->tkey,array('public'=>0),'pkey='.$obj_info['pkey']) ;
   $obj_info['public']=0 ;
   return('ok') ; // будет показан начальный экран
 }



 function tiket_delete()
 {
   echo 'Удаляем тикет' ;
 }

 function tiket_close()
 { global $support_system,$obj_info ;
   $support_system->close_tiket($obj_info) ;
   return('ok') ; // // будет показан начальный экран
 }

 function tiket_send_alert_manager()
 {
   global $support_system,$obj_info ;
   //damp_array($obj_info) ;
   $support_system->prepare_public_info($obj_info) ;
   $support_system->send_mail_support($obj_info,array('mail_to'=>$_SESSION['LS_support_system_email_alert'],'use_mail_code'=>'support_system_tiket_create_alert','reg_events'=>1)) ;
   ?><div class="green">Уведомление менеджеру успешно отправлено</div><?
   return('ok') ; // // будет показан начальный экран
 }

 function show_tiket_history()
   { global $obj_info ;
     show_history('links like "%'.$obj_info['_reffer'].'%" or reffer="'.$obj_info['_reffer'].'"') ;
   }

 function show_other_tiket_autor()
 { global $obj_info,$support_system ;
   _CLSS(16)->show_list_items($support_system->tkey,'autor_email="'.$obj_info['autor_email'].'" and pkey!='.$obj_info['pkey'],array('order'=>'c_data desc')) ;

 }
}


?>