<?php
include_once (_DIR_TO_ENGINE.'/c_page_XML_AJAX.php') ;
include_once (_DIR_TO_ENGINE.'/admin/i_XML_import_export.php') ;
include_once (_DIR_TO_ENGINE.'/admin/i_admin_system.php') ;
class c_editor_support_ajax extends c_page_XML_AJAX
{
   function get_list_tiket($doc,$xml)
   { $options=array() ; $usl='' ; $arr_fields=array() ;  $_usl_filter=array() ;
     $title='Заказы ' ;
     if ($_POST['search_text'] and $_POST['target'])
     {   $arr_words=explode(' ',trim($_POST['search_text'])) ; // разбираем на пробелы
         switch($_POST['target'])
          { case 'autor': $arr_fields=array('autor_email','autor_name','autor_phone') ; $title.=' - имя, телефон, или почта содержит "'.$_POST['search_text'].'"' ; break ;
            case 'content':  $arr_fields=array('obj_name') ; $title.=' - вопрос содержит "'.$_POST['search_text'].'"' ; break ;
          }
         if (sizeof($arr_words)) foreach($arr_words as $word)
         { $_usl2=array() ;
           if (sizeof($arr_fields)) foreach($arr_fields as $fname) $_usl2[]="UCASE(".$fname.") rlike UCASE('[[:<:]]".mb_strtoupper($word)."')" ;
           if (sizeof($_usl2)) $_usl_filter[]='('.implode(' or ',$_usl2).')' ;
         }


     }
     if ($_POST['usl'])                  $usl=stripslashes($_POST['usl']) ;
     if ($_POST['show_van_rec_as_item']) $options['show_van_rec_as_item']=$_POST['show_van_rec_as_item'] ;
     if (sizeof($_usl_filter))           $options['usl_filter']=implode(' and ',$_usl_filter) ;
     $options['default_order']='c_data desc' ;
     $options['title']=$title ;
     // получаем и показывем строки
     $pages_info=_CLSS(16)->show_list_items($_SESSION['support_system']->tkey,$usl,$options) ;
      // выводим информацию по отображенным строкам
      add_element($doc,$xml,'list_id',$_POST['list_id']) ;
      add_element($doc,$xml,'from_pos',$pages_info['from']) ;
      add_element($doc,$xml,'to_pos',$pages_info['to']) ;
      add_element($doc,$xml,'all_pos',$pages_info['all']) ;
      add_element($doc,$xml,'next_page',$pages_info['next_page']) ;
      add_element($doc,$xml,'info_count_text','1...'.$pages_info['to'].' из '.$pages_info['all'])  ;
   }



}



?>