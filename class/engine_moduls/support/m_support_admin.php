<?php
$__functions['init'][] 			='_support_admin_vars' ;
$__functions['boot_admin'][] 	='_support_admin_boot' ;
$__functions['alert'][]			='_support_admin_alert' ;
$__functions['install'][]		='_support_install_modul' ;

include_once('clss_16.php') ;

function _support_admin_vars() //
{

    //-----------------------------------------------------------------------------------------------------------------------------
	// описание переменных модуля
	//-----------------------------------------------------------------------------------------------------------------------------

    $_SESSION['arr_tree_clss_names'][16]=array('Тикет: ','pkey',' : ','obj_name') ;

	//-----------------------------------------------------------------------------------------------------------------------------
	// описание классов модуля
	//-----------------------------------------------------------------------------------------------------------------------------

	//тикеты службы поддержки
    $_SESSION['descr_clss'][16]['name']='Тикет службы поддержки' ;
    $_SESSION['descr_clss'][16]['parent']=0 ;
    // поля - автор тикета
    $_SESSION['descr_clss'][16]['fields']['account_reffer']='any_object' ;
    $_SESSION['descr_clss'][16]['fields']['autor_email']='varchar(100)' ;
    $_SESSION['descr_clss'][16]['fields']['autor_name']='varchar(100)' ;
    $_SESSION['descr_clss'][16]['fields']['autor_phone']='varchar(32)' ;
    $_SESSION['descr_clss'][16]['fields']['autor_info']='serialize' ;

    $_SESSION['descr_clss'][16]['fields']['otvet_email']='varchar(100)' ;

    // поля для доступа к тикету для его редактирования
    $_SESSION['descr_clss'][16]['fields']['uid']='varchar(32)' ;         // для доступа автора
    $_SESSION['descr_clss'][16]['fields']['edit_code']='varchar(32)' ;   // для доступа отвечающего
    $_SESSION['descr_clss'][16]['fields']['act_code']='varchar(32)' ;
    // поля, отвечающие за связь тикета с объектом
    $_SESSION['descr_clss'][16]['fields']['obj_reffer']='any_object' ;
    $_SESSION['descr_clss'][16]['fields']['obj_reffer_name']='text' ;
    $_SESSION['descr_clss'][16]['fields']['obj_reffer_href']='varchar(255)' ;
    $_SESSION['descr_clss'][16]['fields']['obj_reffer_parent']='int(11)' ;
    // дополнительные поля тикета
    $_SESSION['descr_clss'][16]['fields']['public']='int(1)' ; // публицаия тикета на сайте
    $_SESSION['descr_clss'][16]['fields']['status']=array('type'=>'indx_select','array'=>'list_support_status','read_only'=>1) ; // статус тикета

    $_SESSION['descr_clss'][16]['menu'][]=array("name" => "Тикет",	                "cmd" => 'show_tiket') ;
    $_SESSION['descr_clss'][16]['menu'][]=array("name" => "История тикета",			"cmd" => 'show_tiket_history') ;
    $_SESSION['descr_clss'][16]['menu'][]=array("name" => "Ссылки",			        "cmd" => 'show_tiket_links') ;
    $_SESSION['descr_clss'][16]['menu'][]=array("name" => "Другие тикеты автора",	"cmd" => 'show_other_tiket_autor') ;
    //$_SESSION['descr_clss'][0]['menu'][]=array("name" => "Состав заказа",			"cmd" => 'show_order_goods') ;


	//$_SESSION['descr_clss'][16]['list']['field']['pkey']					=array('title'=>'id') ;
    $_SESSION['descr_clss'][16]['list']['field']['c_data']					=array('title'=>'Дата','type'=>'time','nowrap'=>1) ;
    $_SESSION['descr_clss'][16]['list']['field']['status']					=array('title'=>'Статус') ;
    $_SESSION['descr_clss'][16]['list']['field'][]				            =array('title'=>'Действие','td_class'=>'left','use_func'=>'cmd_show_clss_16_edit') ;
	//$_SESSION['descr_clss'][16]['list']['field']['public']					=array('title'=>'Публ.') ;
    $_SESSION['descr_clss'][16]['list']['field']['obj_name']					=array('title'=>'Тема') ;
    $_SESSION['descr_clss'][16]['list']['field'][]			            	=array('title'=>'Автор','td_class'=>'left','use_func'=>'cmd_show_clss_16_autor_info') ;
    $_SESSION['descr_clss'][16]['list']['field']['tiket_info']				=array('title'=>'Сообщения','td_class'=>'left','use_func'=>'cmd_show_clss_16_tiket_info') ;

    $_SESSION['descr_clss'][16]['list']['field']['obj_reffer']				=array('title'=>'На что','show_img'=>1) ;


	//$_SESSION['descr_clss'][16]['list']['field']['clss_16_goto']			=array('title'=>'Переход к тикету') ;
    $_SESSION['descr_clss'][16]['list']['options']=array('no_icon_edit'=>1) ;


	//----------------------------------------------------------------------------------------------------------------------
	// описание пунктов меню админа сайта
	//-----------------------------------------------------------------------------------------------------------------------
	$_SESSION['menu_admin_site']['Модули']['support']=array('name'=>'Служба поддержки','href'=>'editor_support.php','icon'=>_PATH_TO_MODULES.'/support/images/broadcast_help.jpg') ;

	//----------------------------------------------------------------------------------------------------------------------
	// описание меню фрейма объекта
	//-----------------------------------------------------------------------------------------------------------------------

    //$_SESSION['menu_set']['any'][16][]=array("name" => "Просмотр",				"cmd" => 'view','def'=>1) ; // заказ
    //$_SESSION['menu_set']['any'][16][]=array("name" => "История тикета",		"cmd" => 'history') ;
    //$_SESSION['menu_set']['any'][16][]=array("name" => "Ссылки",		        "cmd" => 'links') ;

    // встраиваем служба поддержки в заказы
    //$_SESSION['menu_set']['any'][82][]=array("name" => "Открыть тикет",			"action" => 'open_tiket') ;
    //$_SESSION['menu_set']['any'][82][]=array("name" => "Тикеты клиента",		"action" => 'show_tikets') ;

    // встраиваем служба поддержки в партнерку
    // кстати, эти записи по идее должны быть в модуле "партнерка"
    //$_SESSION['menu_set']['any'][91][]=array("name" => "Открыть тикет",			"action" => 'open_tiket') ;
    //$_SESSION['menu_set']['any'][91][]=array("name" => "Тикеты клиента",		"action" => 'show_tikets') ;

}

function _support_admin_boot($options)
{
	create_system_modul_obj('support',$options) ; // создание объекта support_system - его функии используются для доступа к тикетам
}

function _support_install_modul($DOT_root)
{   echo '<h2>Инсталируем модуль <strong>Служба поддержки</strong></h2>' ;

    $file_items=array() ;
    // editor_support.php
    $file_items['editor_support.php']['include'][]					='_DIR_TO_MODULES."/support/m_support_frames.php"' ;
    $file_items['editor_support.php']['options']['use_table_code']	='"TM_support"' ;
    $file_items['editor_support.php']['options']['title']			='"Служба клиенткой поддержки"' ;
    $file_items['editor_support_tree.php']['include'][]				='_DIR_TO_MODULES."/support/m_support_frames.php"' ;
    $file_items['editor_support_viewer.php']['include'][]			='_DIR_TO_MODULES."/support/m_support_frames.php"' ;
    $file_items['editor_support_ajax.php']['include'][]		        ='_DIR_TO_MODULES."/support/m_support_ajax.php"' ;
    create_menu_item($file_items) ;

	// служба поддержки
	$pattern['table_title']	= 	'Служба поддержки' ;
	$pattern['use_clss']	=	'1,15,16' ;
	$pattern['def_recs'][]	=	array ('parent'=>0,	'clss'=>1,	'indx'=>0,	'obj_name'=>'Служба поддержки') ;
	$pattern['def_recs'][]	=	array ('parent'=>1,	'clss'=>1,	'indx'=>1,	'obj_name'=>'Новые') ;
	$pattern['def_recs'][]	=	array ('parent'=>1,	'clss'=>1,	'indx'=>2,	'obj_name'=>'Открытые') ;
	$pattern['def_recs'][]	=	array ('parent'=>1,	'clss'=>1,	'indx'=>3,	'obj_name'=>'Закрытые') ;
	$pattern['def_recs'][]	=	array ('parent'=>1,	'clss'=>1,	'indx'=>4,	'obj_name'=>'На удаление') ;

   	$pattern['table_name']				= 	$_SESSION['TM_support'] ;
	$pattern['table_clss']				= 	101 ;
	$pattern['table_parent']			= 	$DOT_root;
   	create_table_by_pattern($pattern) ;

    SETUP_get_img('/support/admin_img_menu/broadcast_help.jpg',_PATH_TO_ADMIN.'/img/menu/broadcast_help.jpg') ;

    create_dir('/support/') ;
    SETUP_get_file('/support/system_dir/index.php','/support/index.php');
    SETUP_get_file('/support/system_dir/create.php','/support/create.php');
    SETUP_get_file('/support/system_dir/tiket.php','/support/tiket.php');
    SETUP_get_file('/support/class/c_support.php','/class/c_support.php');
    SETUP_get_file('/support/class/c_support_create.php','/class/c_support_create.php');
    SETUP_get_file('/support/class/c_support_tiket.php','/class/c_support_tiket.php');
    SETUP_get_file('/support/templates/form_support.php','/class/templates/form_support.php');

    import_setting_from_xml(_PATH_TO_SERVER_SETUP.'?m=support&s=setting',array('parent_tkey'=>$_SESSION['pkey_by_table'][TM_SETTING],'parent_pkey'=>1,'check_unique_name'=>1)) ;
    import_setting_from_xml(_PATH_TO_SERVER_SETUP.'?m=support&s=mails',array('parent_tkey'=>$_SESSION['pkey_by_table'][TM_MAILS],'parent_pkey'=>1,'check_unique_name'=>1)) ;

}

function _support_admin_alert()
{ $res=execSQL_van('select count(pkey) as cnt from '.$_SESSION['TM_support'].' where clss=16 and status=0') ;
  if ($res['cnt'])
  { echo "<div>У Вас <span class='red bold'>".$res['cnt']."</span> неотвеченных сообщений в <a href='"._PATH_TO_ADMIN."/editor_support.php'>службе поддержки</a></div><br>";
  }
}


//-----------------------------------------------------------------------------------------------------------------------------
// Функции вывода списков и свойств
//-----------------------------------------------------------------------------------------------------------------------------

 // показ всех тикетов
 function show_list_tiket($usl_select='')
 {  global $support_system ;

 	$list_obj=execSQL('select *,'.$support_system->tkey.' as tkey from '.$support_system->table_name.' where '.$usl_select.' and clss=16 order by c_data desc');

    if (sizeof($list_obj)) foreach($list_obj as $rec)
     {  //damp_array($rec);
       $list_messages=execSQL('select * from '.$support_system->table_name.' where parent='.$rec['pkey'].' and clss=15 order by c_data asc') ;

       ?><h1>Тикет <?echo $rec['pkey']?> от <?echo date("d.m.y G:i",$rec['c_data'])?></h1><?
       ?><div style="margin-left:20px;"><?
       echo '<span class="black bold">Редактировать</span>: '.generate_obj_text($rec['_reffer']).'<br><br>';

       show_tiket_info($rec,sizeof($list_messages)) ;

       list($pkey,$message)=each($list_messages) ;
       show_tiket_message($rec,$message,'Первое сообщение тикета: ') ;

       if (sizeof($list_messages)>2) echo '<br><div class=support_message_header>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>еще <span class="red size18">'.(sizeof($list_messages)-2).'</span> сообщений</strong>...................................................................................................................................................................................................................................................</div><br>' ;
       if (sizeof($list_messages)>1)
       { $message=end($list_messages) ;
         show_tiket_message($rec,$message,'Последнее сообщение тикета: ') ;
       }

	   ?></div><?
    } else echo '<h1>Тикеты</h1>Ничего не найдено' ;
 }

 function cmd_exe_reply_uvedomlen()
 {  global $_check ;
 	if (!sizeof($_check)) return ;
 	//damp_array($_check) ;
 	echo 'Отправляем повторвное уведомление о отмеченных тикетах<br>' ;
 	$arr=get_objs_info_by_check($_check) ;
 	if (sizeof($arr)) foreach($arr as $tiket)
 	{ // получаем последний вопрос тикета тикета
 	  $last_mess=execSQL_van('select * from '.$_SESSION['TM_support'].' where status=0 and clss=15 and parent='.$tiket['pkey'].' order by c_data desc limit 1') ;
 	  send_mail_support_reply_tiket($tiket,$last_mess['obj_name'],array('theme'=>'Уведомление о неотвеченном тикете # '.$tiket['pkey'],'debug'=>1)) ;
 	}
 }

 // получаем информацию о объектах, выбранных в $_check - все в общий список
 function get_objs_info_by_check($_check)
 {
    $res_arr=array() ;
 	if (sizeof($_check)) foreach($_check as $tkey=>$recs_table)
 	 if (sizeof($recs_table)) foreach($recs_table as $clss=>$recs_clss)
 	  { $pkeys=implode(',',array_keys($recs_clss)) ;
 	    $_res_arr=execSQL('select * from '._DOT($tkey)->table_name.' where pkey in ('.$pkeys.')') ;
 	    if (sizeof($_res_arr)) $res_arr=array_merge($res_arr,$_res_arr) ;
 	  }
    return($res_arr) ;
 }

 // отправка повторного сообщения менеджерам о неотвеченном тикете
 function send_mail_support_reply_tiket($tiket,$message,$options=array())
 { $this->prepare_public_info($tiket) ;
   $params['site_name']=$_SESSION['LS_public_site_name'] ;
   $params['name']=$tiket['autor_name'] ;
   $params['support_id']=$tiket['pkey'] ;
   $params['support_data']=date("d.m.Y H:i",$tiket['c_data']) ;
   $params['support_href']=$tiket['__moderator_url'] ;
   $params['text_mess']=$message ;
   $params['mail_bottom_podpis']=$_SESSION['LS_mail_bottom_podpis'] ;
   _send_mail_to_pattern($_SESSION['LS_support_system_email_alert'],'support_system_tiket_create_alert',$params,$options) ;
 }

 function cmd_show_clss_16_autor_info(&$rec)
 {
 	$res='<strong>'.$rec['autor_name'].'</strong>' ;
    if ($rec['autor_phone']) $res.='<br>'.$rec['autor_phone'] ;
    if ($rec['autor_email']) $res.='<br>'.$rec['autor_email'].'<br>' ;
    $res.=get_serial_data($rec['autor_info']) ;
    return($res) ;
 }


 function cmd_show_clss_16_tiket_info(&$rec)
 { $res='' ;
   $list=execSQL('select * from '.$_SESSION['TM_support'].' where clss=15 and parent='.$rec['pkey']) ;
   if (sizeof($list)) foreach($list as $rec_15)
   { //echo 'account_reffer='.$rec_15['account_reffer'].'<br>' ;
   	 $obj_reffer_info=($rec_15['account_reffer'])? select_db_ref_info($rec_15['account_reffer']):'' ;
     $obj_reffer_text=($rec_15['account_reffer'])? ' ('.$obj_reffer_info['obj_name'].')':'' ;
     if ($rec_15['status']) 	$res.='<div style="margin:5px 0 10px 0;"><strong class=green>Ответ</strong> '.$obj_reffer_text.'<br>'.nl2br($rec_15['obj_name']).'</div>' ;
     else 					    $res.='<strong>Вопрос</strong><br>'.nl2br($rec_15['obj_name']).'<br>' ;
   }
   return($res) ;
   //damp_array($list_recs) ;


 }

 function cmd_show_clss_16_edit(&$rec)
{ $text='<a href="fra_viewer.php?obj_reffer='.$rec['_reffer'].'">Редактировать</a>' ;
  return($text) ;
}

//-----------------------------------------------------------------------------------------------------------------------------
// Открытие тикета в заказах
//-----------------------------------------------------------------------------------------------------------------------------

 function cmd_show_clss_91_page_open_tiket()
 { global $obj_info ;
   page_open_tiket('',$obj_info['_reffer'],$obj_info['obj_name']);
 }

 function cmd_show_clss_82_page_open_tiket()
 { global $obj_info ;
   page_open_tiket('Заказ '.$obj_info['obj_name'],$obj_info['account_reffer'],$obj_info['name']);
 }

 function page_open_tiket($thema,$klient_reffer,$klient_name)
 { global $obj_info ;

   ?>   <h1>Открытие нового тикета с клиентом</h1>
        <input name="arr_form_values[member_form][name]" type="hidden" value="<?echo $klient_name?>">
        <input name="arr_form_values[member_form][phone]" type="hidden" value="<?echo $obj_info['phone']?>">
        <input name="arr_form_values[member_form][email]" type="hidden" value="<?echo $obj_info['email']?>">
        <input name="_klient_reffer" type="hidden" value="<?echo $klient_reffer?>">
		<div class="green bold">Тема тикета:</div>
        <input name="_support_theme" type="text" class=text value="<?echo $thema?>" size=123>
	 	<div class="green bold">Текст Вашего сообщения:</div>
	    <textarea name="_support_text" rows=7 cols=120 wrap="on">Уважаемый(ая) <?echo $obj_info['name']?>!



C уважением, администрация сайта <?echo _MAIN_DOMAIN?>.
	    </textarea><br><br>
	 	<div><button id=upload_button class=button cmd=cmd_dialog_open_tiket mode=dialog>Создать тикет</button></div>
	<?
	//damp_array($obj_info);
 }

 function cmd_dialog_open_tiket()
 {  global $_klient_reffer,$member ;
 	echo '<h1>Создаем новый тикет</h1>' ;
    $support=new c_support('new_theme',array('use_klient_reffer'=>$_klient_reffer,'use_autor_reffer'=>$member->reffer,'use_message_status'=>1,'use_mail_code'=>'support_system_tiket_create_admin_to_member','debug'=>0))  ;  // создаем новую ветку сообщений
    if ($support->reffer)
    { echo 'Создан тикет № '.$support->id.'<br>' ;
      echo 'Клиенту отправлено уведомление о открытии тикета <br>' ;
    }
    else echo 'Не удалось открыть тикет<br>' ;
    //damp_array($support) ;
 }

 function cmd_show_clss_82_page_show_tikets()
 { global $obj_info ;
   show_tiket('autor_email="'.$obj_info['email'].'"') ;
 }

 function cmd_show_clss_91_page_show_tikets()
 { global $obj_info ;
   show_tiket('autor_email="'.$obj_info['email'].'"') ;
 }


?>
