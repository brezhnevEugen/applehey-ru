<?php
$__functions['init'][]		='_stats_admin_vars' ;
$__functions['install'][]	='_stats_install_modul' ;

$__functions['stats'][]	='generate_stats_reffers' ;
$__functions['stats'][]	='generate_stats_TOP_pages' ;
$__functions['stats'][]	='generate_stats_debug' ;
$__functions['stats'][]	='generate_stats_logons' ;
$__functions['stats'][]	='generate_stats_users' ;


function _stats_admin_vars() //
{   global $menu_admin_site,$site_TM_kode ;
    $_SESSION['TM_stats']='obj_'.$site_TM_kode.'_stats' ;
	//----------------------------------------------------------------------------------------------------------------------
	// описание пунктов меню админа сайта
	//-----------------------------------------------------------------------------------------------------------------------
  $_SESSION['menu_admin_site']['Модули']['stats']=array('name'=>'Статистика','href'=>'viewer_stats.php','icon'=>'menu/statistic.jpg') ;

	// статистика входов на сайт клиентов
	$_SESSION['descr_clss'][43]['name']='Статистика входов на сайт' ;
	$_SESSION['descr_clss'][43]['parent']=0 ;
	$_SESSION['descr_clss'][43]['fields']=array('cnt'=>'int(11)','is_robot'=>'int(1)') ;

	$_SESSION['descr_clss'][43]['list']['field']['pkey']			='Код' ;
	$_SESSION['descr_clss'][43]['list']['field']['c_data']=array('title'=>'Дата','class'=>'left','wrap'=>'nowrap') ;
	$_SESSION['descr_clss'][43]['list']['field']['obj_name']=array('title'=>'User Agent','td_class'=>'left') ;
	$_SESSION['descr_clss'][43]['list']['field']['cnt']=array('title'=>'Число входов') ;
	$_SESSION['descr_clss'][43]['list']['field']['is_robot']=array('title'=>'Робот') ;

	// Статистика ТОР страниц сайта
	$_SESSION['descr_clss'][46]['name']='Статистика ТОР страниц сайта' ;
	$_SESSION['descr_clss'][46]['parent']=0 ;
	$_SESSION['descr_clss'][46]['fields']=array('page_name'=>'text','reffer'=>'text') ;

	$_SESSION['descr_clss'][46]['list']['field']['pkey']			='Код' ;
	$_SESSION['descr_clss'][46]['list']['field']['c_data']=array('title'=>'От') ;
	$_SESSION['descr_clss'][46]['list']['field']['r_data']=array('title'=>'До') ;
	$_SESSION['descr_clss'][46]['list']['field']['page_name']=array('title'=>'Страница','td_class'=>'left') ;
    $_SESSION['descr_clss'][46]['list']['field']['obj_name']=array('title'=>'Объект','td_class'=>'left') ;
    $_SESSION['descr_clss'][46]['list']['field']['reffer']=array('title'=>'Код объекта') ;

	$_SESSION['descr_clss'][46]['list']['field']['indx']=array('title'=>'Количество') ;

	// Статистика использования ресурсов
	$_SESSION['descr_clss'][47]['name']='Статистика использования ресурсов' ;
	$_SESSION['descr_clss'][47]['parent']=0 ;
	$_SESSION['descr_clss'][47]['fields']=array('users_cnt'=>'int(11)','ip_cnt'=>'int(11)','cnt'=>'int(11)','page_cnt_users'=>'int(11)','page_cnt_robots'=>'int(11)','sum_time_gen'=>'float','avg_time_gen'=>'float','avg_mem_size'=>'float','sum_sql_cnt'=>'int(11)','avg_sql_cnt'=>'float','sum_sql_time'=>'float','avg_sql_time'=>'float','cnt_init'=>'int(11)') ;

	$_SESSION['descr_clss'][47]['list']['field']['clss_47_data']=array('title'=>'Интервал') ;
	$_SESSION['descr_clss'][47]['list']['field']['users_cnt']=array('title'=>'Уникальных UID') ;
	$_SESSION['descr_clss'][47]['list']['field']['ip_cnt']=array('title'=>'Уникальных IP') ;
	$_SESSION['descr_clss'][47]['list']['field']['cnt']=array('title'=>'Загружено страниц всего') ;
	$_SESSION['descr_clss'][47]['list']['field']['page_cnt_users']=array('title'=>'Загружено страниц пользователями') ;
	$_SESSION['descr_clss'][47]['list']['field']['page_cnt_robots']=array('title'=>'Загружено страниц роботами') ;
	$_SESSION['descr_clss'][47]['list']['field']['cnt_init']=array('title'=>'init engine') ;
	$_SESSION['descr_clss'][47]['list']['field']['sum_time_gen']=array('title'=>'Суммарное время генерации','ue'=>'c.') ;
	$_SESSION['descr_clss'][47]['list']['field']['clss_47_proc']=array('title'=>'Процент загрузки сервера') ;
	$_SESSION['descr_clss'][47]['list']['field']['avg_time_gen']=array('title'=>'Среднее время генерации','ue'=>'c.') ;
	$_SESSION['descr_clss'][47]['list']['field']['avg_mem_size']=array('title'=>'Среднее использование памяти','ue'=>'кБ') ;
	$_SESSION['descr_clss'][47]['list']['field']['sum_sql_cnt']=array('title'=>'Суммарное число запросов к БД') ;
	$_SESSION['descr_clss'][47]['list']['field']['avg_sql_cnt']=array('title'=>'Среднее число запросов на 1 страницу') ;
	$_SESSION['descr_clss'][47]['list']['field']['sum_sql_time']=array('title'=>'Суммарное время выполнения запросов к БД','ue'=>'c.') ;
	$_SESSION['descr_clss'][47]['list']['field']['avg_sql_time']=array('title'=>'Среднее время выполнения 1 запроса к БД','ue'=>'c.') ;

	// Статистика переходов
	$_SESSION['descr_clss'][48]['name']='Статистика переходов' ;
	$_SESSION['descr_clss'][48]['parent']=0 ;

	$_SESSION['descr_clss'][48]['list']['field']['pkey']			='Код' ;
	$_SESSION['descr_clss'][48]['list']['field']['c_data']=array('title'=>'От') ;
	$_SESSION['descr_clss'][48]['list']['field']['r_data']=array('title'=>'До') ;
	$_SESSION['descr_clss'][48]['list']['field']['obj_name']=array('title'=>'Откуда','td_class'=>'left width_300') ;
	$_SESSION['descr_clss'][48]['list']['field']['indx']=array('title'=>'Количество') ;

	// Статистика запросов
	$_SESSION['descr_clss'][49]['name']='Статистика запросов' ;
	$_SESSION['descr_clss'][49]['parent']=0 ;

	$_SESSION['descr_clss'][49]['list']['field']['pkey']			='Код' ;
	$_SESSION['descr_clss'][49]['list']['field']['c_data']=array('title'=>'От') ;
	$_SESSION['descr_clss'][49]['list']['field']['r_data']=array('title'=>'До') ;
	$_SESSION['descr_clss'][49]['list']['field']['obj_name']=array('title'=>'Текст запроса','td_class'=>'left width_300') ;
	$_SESSION['descr_clss'][49]['list']['field']['indx']=array('title'=>'Количество') ;

    // журнал показанных страниц
	$_SESSION['descr_clss'][50]['name']='Запись журнала показанных страниц' ;
	$_SESSION['descr_clss'][50]['parent']=-1 ;
	$_SESSION['descr_clss'][50]['fields']=array('pkey'=>'int(11)','c_data'=>'timedata','ip'=>'varchar(15)','url'=>'text','obj_name'=>'text','reffer'=>'text','session_id'=>'varchar(50)','url_from'=>'text','search_text'=>'text','user_agent'=>'text','time_gen'=>'float','mem_size'=>'float','sql_cnt'=>'int(11)','sql_time'=>'float','use_cache'=>'int(1)','page_keywords'=>'text','page_description'=>'text','is_robot'=>'int(1)','is_reboot'=>'int(1)') ;
	$_SESSION['descr_clss'][50]['table_code']='pages' ;
    $_SESSION['descr_clss'][50]['table_name']='Журнал страниц' ;

}

function _stats_install_modul($DOT_root)
{   global $dir_to_modules,$admin_dir ;
    //include_once($dir_to_modules.'setup/m_setup_admin.php') ;
	echo '<h2>Инсталируем модуль <strong>Статистика</strong></h2>' ;

    $file_items=array() ;
    // viewer_stats.php
    $file_items['viewer_stats.php']['include'][]					='$dir_to_modules."stats/m_stats_frames.php"' ;
    $file_items['viewer_stats.php']['options']['title']				='"Статистика"' ;
    $file_items['viewer_stats_tree.php']['include'][]				='$dir_to_modules."stats/m_stats_frames.php"' ;
    $file_items['viewer_stats_viewer.php']['include'][]				='$dir_to_modules."stats/m_stats_frames.php"' ;
    create_menu_item($file_items) ;

    global $TM_stats ;
    $pattern=array() ;
    // статистика
    $pattern['table_title']	        	= 	'Статистика сайта' ;
    $pattern['use_clss']			    =   '1,43,46,47,48,49' ;
    $pattern['def_recs'][]		        =	array ('parent'=>0,	'clss'=>1,	'enabled'=>1,	'indx'=>0,	'obj_name'=>'Статистика') ;
    $pattern['def_recs'][]		        =	array ('parent'=>1,	'clss'=>1,	'enabled'=>1,	'indx'=>1,	'obj_name'=>'Переходы');
    $pattern['def_recs'][]		        =	array ('parent'=>1,	'clss'=>1,	'enabled'=>1,	'indx'=>2,	'obj_name'=>'Запросы') ;
    $pattern['table_name']				= 	$TM_stats ;
    $pattern['table_clss']				= 	101 ;
    $pattern['table_parent']			= 	$DOT_root;
    create_table_by_pattern($pattern) ;

    SETUP_get_img('/stats/admin_img_menu/statistic.jpg',$admin_dir.'img/menu/statistic.jpg') ;
}