<?php
$__functions['init'][]        ='_stats_site_vars' ;
// 7.08.11 - в данный модуль из модуля /engine/admin/i_stats.pho перенесены все функции, связанные с расчетом статистики и внесением её в БД

$__functions['stats'][]	='generate_stats_reffers' ;
$__functions['stats'][]	='generate_stats_TOP_pages' ;
$__functions['stats'][]	='generate_stats_debug' ;
$__functions['stats'][]	='generate_stats_logons' ;
$__functions['stats'][]	='generate_stats_users' ;

function _stats_site_vars() //
{ global $site_TM_kode ;
  $_SESSION['TM_stats']='obj_'.$site_TM_kode.'_stats' ; $GLOBALS['TM_stats']=&$_SESSION['TM_stats'] ;
}

/**************************************************************************************************************************************

   СТАТИСТИКА ПЕРЕХОДОВ

***************************************************************************************************************************************/



  function generate_stats_reffers($data_from,$data_to,$debug=0)
   { global $TM_log_pages;
     if (!$data_from) return ;
     if ($debug) {?><h2>Формируем статистику по переходам [48]</h2><?}

        if ($debug==2) echo 'Обрабатываем интервал: '.date('d.m.y H:i:s ',$data_from).'-'.date('d.m.y H:i:s ',$data_to).'<br>' ;
        $i=0 ;

        list($stats,$min_time,$max_time)=get_stats_reffer($TM_log_pages,$data_from,$data_to) ;

        $j=0 ;
        if (sizeof($stats))
         { //if ($debug) echo 'Получены данные в &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: '.date('d.m.y H:i:s ',$min_time).'-'.date('d.m.y H:i:s ',$max_time).'<br>' ;
           arsort($stats) ; //damp_array($stats) ;
           foreach($stats as $ref=>$cnt)
	       { // просто тупо добавляем записи в базу
	          { $finfo=array(	'clss'			=>	48,
			      				'parent'		=>	2,
			     				'obj_name' 		=> 	$ref,
			     				'enabled'		=>	1,
			    				'indx'			=>	$cnt,
			    				'c_data'		=>	$data_from,
			    				'r_data'		=>	$data_to
			    			  ) ;
		        //print_r($finfo) ; echo '<br>' ;
		        adding_rec_to_table($_SESSION['TM_stats'],$finfo) ;
		        $i++ ;
		      }
		      $j++ ; if ($j>=30) break ; // максимум 30 записей
           }

	    } else if ($debug)  echo '<span class=red>Записей в журнале в этом диапазоне нет</span><br>' ;

	 if ($debug) echo 'Добавлены <span class="bold green">'.$i.'</span> записей<br>' ;
	 return ($i) ;
   }

function get_stats_reffer($table_name,$from_time,$to_time)
 { global $base_domain ;
   $daily_recs=execSQL('select c_data,url_from from '.$table_name.' where c_data>='.$from_time.' and c_data<='.$to_time.' and url_from>"" and url_from not like "%'.$base_domain.'%" order by c_data') ;
   //echo 'Всего записей: '.sizeof($daily_recs).'<br>' ;
   $min_time=0 ; $max_time=0 ;
   if (sizeof($daily_recs)) foreach($daily_recs as $rec)
   { $url_info=parse_url($rec['url_from']) ;
     //print_r($url_info) ; echo '<br>' ;
     if (!$min_time) $min_time=$rec['c_data'] ;
     if ($max_time<$rec['c_data']) $max_time=$rec['c_data'] ;
     if (isset($host_list[$url_info['host']])) $host_list[$url_info['host']]++ ; else $host_list[$url_info['host']]=1 ;
   }
   //damp_array($host_list) ;
   return(array($host_list,$min_time,$max_time)) ;
 }



/**************************************************************************************************************************************

   СТАТИСТИКА ТОР страниц сайта

***************************************************************************************************************************************/

   function generate_stats_TOP_pages($data_from,$data_to,$debug=0)
   { global $TM_log_pages;
     if (!$data_from) return ;
     if ($debug) {?><h2>Формируем статистику ТОР страниц сайта [46]</h2><?}

     if ($debug==2) echo 'Обрабатываем интервал: '.date('d.m.y H:i:s ',$data_from).'-'.date('d.m.y H:i:s ',$data_to).'<br>' ;
     $i=0 ;
     // получаем и сохраняем статистику ТОP страниц сайта
     list($stats,$min_time,$max_time)=get_stats_top_pages($TM_log_pages,$data_from,$data_to) ;

     $j=0 ;
     if (sizeof($stats))
        {   //if ($debug) echo 'Получены данные в &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: '.date('d.m.y H:i:s ',$min_time).'-'.date('d.m.y H:i:s ',$max_time).'<br>' ;
        	arsort($stats) ;
	        foreach($stats as $ref=>$ref_info)
	    	{ // просто тупо добавляем записи в базу
	          { $finfo=array(	'clss'			=>	46,
			      				'parent'		=>	2,
			     				'obj_name'		=> 	$ref_info['obj_name'],
			     				'page_name' 	=> 	$ref,
			     				'reffer' 		=> 	$ref_info['reffer'],
			    				'indx'			=>	$ref_info['cnt'],
			    				'c_data'		=>	$data_from,
			    				'r_data'		=>	$data_to
			    			  ) ;
		        //print_r($finfo) ; echo '<br>'  ;
		        // добавляем описание нового поля в таблицу описаний
		        adding_rec_to_table($_SESSION['TM_stats'],$finfo) ;
		        $i++ ;
		      }
		      $j++ ; if ($j>=30) break ; // максимум 30 записей
	        }
	    } else if ($debug)  echo '<span class=red>Записей в журнале в этом диапазоне нет</span><br>' ;

	 if ($debug) echo 'Добавлены <span class="bold green">'.$i.'</span> записей<br>' ;
	 return ($i) ;
   }

  function get_stats_top_pages($table_name,$from_time,$to_time)
   {
     $daily_recs=execSQL('select * from '.$table_name.' where c_data>='.$from_time.' and c_data<='.$to_time.' and (is_robot=0 or is_robot is null) order by c_data') ;
     //echo 'Всего записей: '.sizeof($daily_recs).'<br>' ;
     $min_time=0 ;  $max_time=0 ;
     if (sizeof($daily_recs)) foreach($daily_recs as $rec)
     { $rec_url=str_replace('http://','',$rec['url']) ;
       $url_info=parse_url($rec_url) ;
       $url=$url_info['path'] ;
       if (strpos($url,'.php')===false)
       {   //print_r($url_info) ; echo '<br>' ;
	       if (!$min_time) $min_time=$rec['c_data'] ;
	       if ($max_time<$rec['c_data']) $max_time=$rec['c_data'] ;
	       if (isset($host_list[$url]['cnt'])) $host_list[$url]['cnt']++ ; else { $host_list[$url]=array('cnt'=>1,'obj_name'=>$rec['obj_name'],'reffer'=>$rec['reffer']) ;}
	   }
     }
     //damp_array($host_list) ;
     return(array($host_list,$min_time,$max_time)) ;
   }


/**************************************************************************************************************************************

   СТАТИСТИКА нагрузки на сервер

***************************************************************************************************************************************/


   function generate_stats_debug($data_from,$data_to,$debug=0)
   { global $TM_log_pages;
     if (!$data_from) return ;
     if ($debug) {?><h2>Формируем статистику нагрузки на сервер [47]</h2><?}

     if ($debug==2) echo 'Обрабатываем интервал: '.date('d.m.y H:i:s ',$data_from).'-'.date('d.m.y H:i:s ',$data_to).'<br>' ;
     $i=0 ;

	 // получаем и сохраняем статистику  по ресурсам
	 $stats=array() ;
	 $stats=execSQL_van('select count(pkey) as cnt,round(sum(time_gen)) as sum_time_gen,round(avg(time_gen),4) as avg_time_gen,round(avg(mem_size)) as avg_mem_size,sum(sql_cnt) as sum_sql_cnt,round(avg(sql_cnt)) as avg_sql_cnt,round(sum(sql_time)) as sum_sql_time,round(avg(sql_time),4) as avg_sql_time,count(distinct(session_id)) as users_cnt,count(distinct(ip)) as ip_cnt, min(c_data) as c_data,max(c_data) as r_data from '.$TM_log_pages.' where c_data>='.$data_from.' and c_data<='.$data_to) ;
	 $stats2=execSQL_van('select count(distinct(session_id)) as cnt from '.$TM_log_pages.' where c_data>='.$data_from.' and c_data<='.$data_to) ;
	 $stats3=execSQL('select is_robot,count(pkey) as cnt from '.$TM_log_pages.' where c_data>='.$data_from.' and c_data<='.$data_to.' group by is_robot') ;

	 $stats['clss']=47 ;
	 $stats['parent']=1 ;
	 $stats['c_data']=$data_from ;
	 $stats['r_data']=$data_to ;
	 $stats['cnt_init']=$stats2['cnt'] ;
	 $stats['page_cnt_users']=$stats3[0]['cnt'] ;
	 $stats['page_cnt_robots']=$stats3[1]['cnt'] ;
     unset($stats['tkey']) ;
	 //print_r($stats) ; echo '<br>' ;
	 if ($stats['cnt']) adding_rec_to_table($_SESSION['TM_stats'],$stats) ;
	 $i++ ;
	 if ($debug) echo 'Добавлены <span class="bold green">'.$i.'</span> записей<br>' ;
	 return ($i) ;
   }

/**************************************************************************************************************************************

   СТАТИСТИКА входов на сайт

***************************************************************************************************************************************/


   function generate_stats_logons($data_from,$data_to,$debug=0)
   { global $TM_log_pages;
     if (!$data_from) return ;
     if ($debug) {?><h2>Формируем статистику заходов на сайт [43]</h2><?}

     if ($debug==2) echo 'Обрабатываем интервал: '.date('d.m.y H:i:s ',$data_from).'-'.date('d.m.y H:i:s ',$data_to).'<br>' ;
     $i=0 ;

     $res_stats_user=array() ;
     $res_stats_robots=array() ;

     // получаем и обрабатываем статистику по входам клиентов
     $stats=execSQL('select session_id,user_agent,count(pkey) as cnt from '.$TM_log_pages.' where c_data>='.$data_from.' and c_data<='.$data_to.' and is_robot=0 group by session_id order by user_agent',0,1) ;
     if (sizeof($stats)) foreach($stats as $rec)
     { $br_vers=defines_vers($rec['user_agent'],1) ;
       if ($br_vers!='OTHER') $res_stats_user[$br_vers]+=1 ;//$rec['cnt'] ;
       else 				  $res_stats_user[$rec['user_agent']]+=1 ; //$rec['cnt'] ;
     }

	 if (sizeof($res_stats_user)) foreach($res_stats_user as $user_agent=>$cnt)
			 { $finfo=array(	'clss'			=>	43,
			      				'parent'		=>	2,
			     				'obj_name'		=> 	$user_agent,
			     				'cnt' 			=> 	$cnt,
			     				'is_robot' 		=> 	0,
			    				'c_data'		=>	$data_from,
			    				'r_data'		=>	$data_to
			    			  ) ;
		       //print_r($finfo) ; echo '<br>'  ;
		        // добавляем описание нового поля в таблицу описаний
		       adding_rec_to_table($_SESSION['TM_stats'],$finfo) ;
		       $i++ ;
    		 }

     // получаем и обрабатываем статистику по входам роботов
     $stats=execSQL('select user_agent,count(pkey) as cnt from '.$TM_log_pages.' where c_data>='.$data_from.' and c_data<='.$data_to.' and is_robot>0 group by user_agent order by user_agent',0) ;
     if (sizeof($stats)) foreach($stats as $rec)
       { $res_stats_robots[$rec['user_agent']]=$rec['cnt'] ;
         //else         $res_stats_robots['Другие']++ ;
       }

            //damp_array($res_stats_user) ;
            //damp_array($res_stats_robots) ;
            //echo sizeof($res_stats_user).'<br>' ;
            //echo sizeof($res_stats_robots).'<br>' ;

	 if (sizeof($res_stats_robots)) foreach($res_stats_robots as $user_agent=>$cnt)
			 { $finfo=array(	'clss'			=>	43,
			      				'parent'		=>	2,
			     				'obj_name'		=> 	($user_agent)? $user_agent:'Пустое поле',
			     				'cnt' 			=> 	$cnt,
			     				'is_robot' 		=> 	1,
			    				'c_data'		=>	$data_from,
			    				'r_data'		=>	$data_to
			    			  ) ;
		       //print_r($finfo) ; echo '<br>'  ;
		        // добавляем описание нового поля в таблицу описаний
		       adding_rec_to_table($_SESSION['TM_stats'],$finfo) ;
		       $i++ ;
    		 }

     if ($debug) echo 'Добавлены <span class="bold green">'.$i.'</span> записей<br>' ;
     return ($i) ;
   }

/**************************************************************************************************************************************

   СТАТИСТИКА качества клиентов

***************************************************************************************************************************************/


   function generate_stats_users($data_from,$data_to,$debug=0)
   { global $TM_log_pages;
     if (!$data_from) return ;
     $res=array() ;
     if ($debug) {?><h2>Формируем статистику качества посетителей [42]</h2><?}

     if ($debug==2) echo 'Обрабатываем интервал: '.date('d.m.y H:i:s ',$data_from).'-'.date('d.m.y H:i:s ',$data_to).'<br>' ;
     $i=0 ;

     // добавдяем статистику по случайным и реальным клиентам
     $active_users=0 ; $passive_users=0 ;
     $list= execSQL('select session_id,count(pkey) as cnt,user_agent from '.$TM_log_pages.' where is_robot=0 and c_data>='.$data_from.' and c_data<='.$data_to.' group by session_id') ;
     if (sizeof($list)) foreach($list as $rec) if ($rec['cnt']>1)  $active_users++ ; else $passive_users++ ;
     $res['all_users']=$res['active_users']+$res['passive_users'] ;
	 $finfo=array(	'clss'			=>	42,
   					'parent'		=>	2,
   					'cnt' 			=> 	$passive_users,
  					'cnt_init' 		=> 	$active_users,
 					'c_data'		=>	$data_from,
 					'r_data'		=>	$data_to
 			  	  ) ;
	 // добавляем описание нового поля в таблицу описаний
	 adding_rec_to_table($_SESSION['TM_stats'],$finfo) ;
	 $i++ ;

     if ($debug) echo 'Добавлены <span class="bold green">'.$i.'</span> записей<br>' ;
     return ($i) ;
   }



