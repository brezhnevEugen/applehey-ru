<?php
include_once(_DIR_TO_ENGINE."/admin/c_site.php") ;
include_once(_DIR_TO_ENGINE."/admin/i_clss_func.php") ;
include_once(_DIR_TO_ENGINE."/admin/c_menu_tabs.php") ;
include_once(_DIR_TO_ENGINE."/admin/i_stats.php") ;

class c_viewer_stats extends c_editor_obj
{
  function body($options=array()) {$this->body_frame_2x(array('fra_tree'=>_PATH_TO_ADMIN.'/viewer_stats_tree.php','title'=>$options['title'])) ; }
}


class c_viewer_stats_tree extends c_fra_tree
{

  function body($options=array())
  { ?>
    <body id=tree_objects>
     <div id=tree_menu></div>
        <ul class="tree_root" on_click_script="viewer_stats_viewer.php">
            <li clss=1 modes=1>Статистика</li>
            <ul>
            <li clss=1 modes=20>Заходы на сайт</li>
            <ul>
<!--                <li modes=21>Сегодня</li>-->
                <li modes=22>За месяц</li>
                <li modes=23>Еженедельно</li>
                <li modes=24>Ежемесячно</li>
                <li modes=26>Показать статистику входов</li>
                <li modes=27>Показать статистику посетителей</li>
            </ul>
            <li clss=1 modes=10>Переходы на сайт</li>
            <ul>
                <li modes=11>Сегодня</li>
                <li modes=16>За последние 24 часа</li>
                <li modes=12>За неделю</li>
                <li modes=13>За месяц</li>
                <li modes=14>За три месяца</li>
                <li modes=15>За год</li>
                <li modes=102>Показать статистику переходов</li>
            </ul>
            <li clss=1 modes=50>Тор страниц сайта</li>
            <ul>
                <li modes=51>Сегодня</li>
                <li modes=52>За неделю</li>
                <li modes=53>За месяц</li>
                <li modes=54>За три месяца</li>
                <li modes=55>За год</li>
                <li modes=502>Показать статистику TOP страниц</li>
            </ul>
            <li clss=1 modes=40>Использование ресурсов сервера</li>
            <ul>
               <li modes=41>Сегодня</li>
               <li modes=42>За месяц</li>
               <li modes=43>Еженедельно</li>
               <li modes=44>Ежемесячно</li>
               <li modes=402>Показать статистику ресурсов</li>
            </ul>
            <li clss=1>Операции</li>
            <ul>
               <li modes=716>Проверка распознования роботов по USER_AGENT</li>
               <li modes=720>Проверка распознования роботов по анализу SESSION_ID и IP</li>
               <li modes=719>роверка подсчета входов на сайт и активных клиентов</li>
               <li modes=717>Удалить записи последнего дня из статистики</li>
               <li modes=718>Обновить статистику</li>
               <li modes=719>Информация по журналам</li>
            </ul></ul>
        </ul>
    </body><?
  }


}

class c_viewer_stats_viewer extends c_site
{

 function body($options=array()) {$this->body_frame($options=array()) ; }

 function block_main()
    { global $modes ;
	  switch ($modes)
	    { case 10:
	      case 11:  $this->show_stats_reffers(0) ; break ;
	      case 12:  $this->show_stats_reffers(2) ; break ;
	      case 13:  $this->show_stats_reffers(3) ; break;
	      case 14:  $this->show_stats_reffers(4) ; break;
	      case 15:  $this->show_stats_reffers(5) ; break;
	      case 16:  $this->show_stats_reffers(1) ; break ;

	      case 1:
	      case 20:
//	      case 21:  $this->show_stats_logons(1) ; 		break;
	      case 22:  $this->show_stats_logons(2) ; 		break;
	      case 23:  $this->show_stats_logons(3) ; 		break;
	      case 24:  $this->show_stats_logons(4) ; 		break;
	      case 25:  $this->show_stats_logons(5) ; 		break;
	      case 26:  $this->show_stats('Статистика входов',43) ; break ;
	      case 27:  $this->show_stats('Статистика посетителей',42) ; break ;

	      case 40:
	      case 41:  $this->show_stats_debug(1) ; 		break;
	      case 42:  $this->show_stats_debug(2) ; 		break;
	      case 43:  $this->show_stats_debug(3) ; 		break;
	      case 44:  $this->show_stats_debug(4) ; 		break;
	      case 45:  $this->show_stats_debug(5) ; 		break;
	      case 50:
	      case 51:  $this->show_stats_top_pages(1) ; 		break;
	      case 52:  $this->show_stats_top_pages(2) ; 		break;
	      case 53:  $this->show_stats_top_pages(3) ; 		break;
	      case 54:  $this->show_stats_top_pages(4) ; 		break;
	      case 55:  $this->show_stats_top_pages(5) ; 		break;

	      case 101: generate_stats_reffers(1) ; break ;
	      case 102: $this->show_stats('Статистика переходов',48) ; break ;
		  case 103: erase_stats(48) ; break ;

	      case 401: generate_stats_debug(1) ; break ;
	      case 402: $this->show_stats('Статистика ресурсов',47) ; break ;
          case 403: erase_stats(47) ; break ;

	      case 502: $this->show_stats('Статистика ТОР страниц сайта',46) ; break ;
          case 503: erase_stats(46) ; break ;

          case 710: $this->show_log_info();         	break;
          case 714: check_robots_recs(1) ; 				break;
          case 715: delete_logs_recs(1) ; 				break;
          case 716: check_user_agent() ; 				break;
          case 717: delete_last_day_from_stats(); 		break;
          case 718: generate_sum_stats(1); 				break;
          case 719: get_users_active_info(10,2); 				break;
          case 720: check_robots_by_session_id() ; 		break;
          default: echo 'Нераспознанный код операции:'.$modes.'<br>' ;


	    }
    }


  function get_time_stats_intervals($mode)
  {
     $now=getdate() ;
     $prev_day_end	= mktime(0, 0, 0, $now['mon'], $now['mday'], $now['year'])-1; // время окончания вчерашнего дня

     $end_time=time() ;
     switch ($mode)
     { case 0: 	$name='за сегодня' ;
     		   	$start_time=mktime(0,0,0,$now['mon'],$now['mday'],$now['year']); // сутки назад
     		   	break ;
       case 1: 	$name='за последние 24 часа' ;
     		   	$start_time=mktime($now['hours'],$now['minutes'],$now['seconds'],$now['mon'],$now['mday']-1,$now['year']); // сутки назад
     		   	break ;
       case 2: 	$name='за неделю' ;
                $start_time=mktime(0,0,0,$now['mon'],$now['mday']-7,$now['year']); // неделю назад
                $end_time=$prev_day_end ;
       			break ;
       case 3: 	$name='за месяц' ;
                $start_time=mktime(0,0,0,$now['mon']-1,$now['mday'],$now['year']); // месяц назад
                $end_time=$prev_day_end ;
       			break ;
       case 4:  $name='за три месяца' ;
       			$start_time=mktime(0,0,0,$now['mon']-3,$now['mday'],$now['year']); // три месяца назад
       			$end_time=$prev_day_end ;
       			break ;
       case 5:  $name='за год' ;
       			$start_time=mktime(0,0,0,$now['mon'],1,$now['year']-1); // год назад
       			$end_time=$prev_day_end ;
       			break ;
     }
    return(array($name,$start_time,$end_time)) ;
  }


/**************************************************************************************************************************************

   прямой просмотр журналов и статистики

***************************************************************************************************************************************/


   function show_stats($title,$clss,$limit=100)
   {
   	 global $TM_stats ;
   	 ?><h1><?echo $title?></h1><?
   	 $count_rec=execSQL_van('select count(pkey) as cnt from '.$TM_stats.' where clss='.$clss) ;
	 echo 'Записей в журнале: '.$count_rec['cnt'].', показано последние 100 записей<br><br>' ;
	 $list=execSQL('select * from '.$TM_stats.' where clss='.$clss.' order by c_data desc limit '.$limit) ;
     $this->show_based_obj_list($list,array('read_only'=>1,'clss'=>$clss,'no_check'=>1,'title'=>$title,'all_enabled'=>1,'no_htmlspecialchars'=>1)) ;
   }

  function show_based_obj_list(&$arr_rec,$options=array())
   { global $descr_obj_tables;
     //damp_array($options) ;
     //trace() ;
     if (!sizeof($arr_rec)) { if (!$options['no_show_not_found']) echo '<p class=alert>Ничего не найдено</p>' ; return ; }
     // берем tkey и clss из первой записи
     reset($arr_rec); list($first_pkey,$first_rec)=each($arr_rec) ; prev($arr_rec) ; //print_r($first_rec) ;
     $tkey=$first_rec['tkey'] ;
     $clss=($options['clss'])? $options['clss']:$first_rec['clss'] ; //echo 'clss='.$clss ; echo '<br>' ;
     // tckb
     //damp_array($arr_rec) ;

     // если в init.php задан шаблон вывода объектов текущего класса, то использовать его
     // список полей для быстрого редактирования будет или задан для таблицы или взять с класса по умолчанию
     $list_block=($options['small'])? 'small':'list' ;
     $edit_list=_CLSS($clss)->view[$list_block]['field'] ; //damp_array($edit_list); echo '<br>' ;
     $options['space_value']='1111' ; // массив не должен быть пустым - иначе php может зависнуть
     if (is_array(_CLSS($clss)->view[$list_block]['options'])) $options=array_merge($options,_CLSS($clss)->view[$list_block]['options']) ;
     $options['edit_by_click']=1 ;
     //$options['debug']=2 ;
     //echo 'clss='.$clss ; //echo '<br>' ; exit ;
     //print_r($arr_rec);  echo '<br>' ;
     //print_r($options); echo '<br>' ;

     //$full_size=sizeof($arr_rec) ; //echo '$full_size='.$full_size.'<br>' ;

     //$start_indx=0 ; $cur_size=sizeof($arr_rec) ;

     // если у объектов таблицы есть фото в дочерних элементах - получаем информацию о количесте фото для каждого выводимого элемента
     if ($descr_obj_tables[$tkey]->list_clss[3]) update_obj_all_image($descr_obj_tables[$tkey]->list_clss[3],$arr_rec) ;

     //damp_array($options) ;
     /* отключено 1.02.13 - переделан вывод для CLSS 100
     if (sizeof($options['list_reffers_obj']))
     {list($_id,$_rec)=each($options['list_reffers_obj']) ;
       if ($descr_obj_tables[$_rec['tkey']]->list_clss[3]) update_obj_all_image($descr_obj_tables[$_rec['tkey']]->list_clss[3],$options['list_reffers_obj']) ;
     } */
     //damp_array($options) ;
     $title=($options['title'])? $options['title']:_CLSS($clss)->name($options['cur_obj_rec']['_reffer']) ;
     /*?><h2><?echo $title?></h2><?*/

     if (!$options['no_table_colgroup_tr'] and !$options['only_tr_content'] )
     { ?><table class=list><?
       $this->print_table_colgroup($tkey,$clss,$edit_list,$options) ;
     }

     if (!$options['no_header'] and !$options['only_tr_content']) // выводим заголовок таблицы
     {

        $header_options['colspan']=100 ; //sizeof($edit_list)+3 ;
        $header_options['icon_clss']=$clss ;
        $header_options['page_info']=$options['info_count_text'] ;

        if (!$options['no_table_header']) $this->print_table_header($title,$header_options) ;

  	      ?><tr class=td_header><?
       $this->print_td_header($tkey,$clss,$edit_list,$options) ;

  		      // если передан списко ссылающихся обхектов, то выводим их заголовки тоже
                /* отключено 1.02.13 - переделан вывод для CLSS 100
  			  if (sizeof($options['list_reffers_obj']))
                { //damp_array($options['list_reffers_obj']);
                  //list($id,$obj_ref)=each($options['list_reffers_obj']) ;
                  //damp_array($options) ;
                  $RF_edit_list=_CLSS($options['list_reffers_obj_clss'])->view['list']['field'] ;
                  print_td_header($options['list_reffers_obj_tkey'],$options['list_reffers_obj_clss'],$RF_edit_list,array('read_only'=>1,'no_check'=>1)) ;
                } */

  	      ?></tr><?
     }

     // выводим содержимое полей
     //print_r($arr_rec) ; echo '<br>' ;
     //echo 'tkey='.$tkey ; echo '<br>' ;

      //damp_array(get_included_files()) ;

      if (sizeof($arr_rec)) foreach ($arr_rec as $rec)
      {   $pkey=$rec['pkey'] ;
          $tr_attr=array() ;
          // формируемм переменные для строки
          $tr_attr[]=(!$rec["enabled"])? 'class="item off"':'class="item on"' ;
          // формируем обрабочик собитытия onclikc если такой задан
          $tr_attr[]=($options['onclick_func_name'])? 'onclick="'.$options['onclick_func_name'].'(\''.$tkey.'\',\''.$pkey.'\');"':'' ;
          // учет ссылкок
          //$tr_attr[]=(!sizeof($rec['__reffer_from']))? 'tr_'.$tkey.'_'.$pkey:'tr_'.$rec['__reffer_from']['tkey'].'_'.$rec['__reffer_from']['pkey'] ;
          //$tr_attr[]='id="'.$tkey.'_'.$rec['clss'].'_'.$pkey.'"';
          $tr_attr[]='reffer="'.$rec['_reffer'].'"';
          $tr_attr_str=' '.implode(' ',$tr_attr) ;
          //print_r($tr_attr) ;
          if (!$options['no_table_colgroup_tr']) {?><tr<?echo $tr_attr_str?>><?}

            //print_r($rec) ;
            //print_r($edit_list) ; echo '<br>' ;
        $this->print_obj_rec_in_table($rec,$edit_list,$options) ;

  		  // если передан списко ссылающихся обхектов, то выводим их далее в режиме - тока чтение
            /* отключено 1.02.13 - переделан вывод для CLSS 100
  		  if (sizeof($options['list_reffers_obj']) and $rec['reffer']) foreach ($options['list_reffers_obj'] as $ref_pkey=>$ref_rec)
              { $RF_edit_list=_CLSS($ref_rec['clss'])->view['list']['field'] ;
                if ($rec['reffer']==$ref_rec['_reffer']) print_obj_rec_in_table($ref_rec,$RF_edit_list,array('read_only'=>1,'no_check'=>1)) ;
              } */

  	    if (!$options['no_table_colgroup_tr']) {?></tr><?}

  	    if ($rec['__reffer_from']['clss']==120)
  	    {   global $orders_system ;
              ?><tr><td colspan=<? echo sizeof($edit_list)+3 ?> class=left><?
              if ($rec['__reffer_from']['price_info']) $cart_price_info=unserialize($rec['__reffer_from']['price_info']) ;
              $cart_goods_info=array_merge($rec['__reffer_from'],$cart_price_info) ;
              $price_info=$orders_system->prepare_price_info($cart_goods_info) ;
              //damp_array($price_info,1,-1) ;


              echo '<strong>Артикул: </strong> '.$rec['art'].'&nbsp;&nbsp;&nbsp;&nbsp;<strong>Склад: </strong> '.$rec['stock'].' шт. <strong> Наименование:</strong> '.$rec['__reffer_from']['obj_name'].'<br>' ; ;
  	        echo '<strong>Цена товара: </strong>'.$price_info['price'].'&nbsp;&nbsp;&nbsp;&nbsp;';
              if ($price_info['sales_text'])  echo strip_tags($price_info['sales_title']).' '.$price_info['sales_text'] ;
  	        echo '<strong>Количество: </strong>'.$price_info['cnt'].' шт.&nbsp;&nbsp;&nbsp;&nbsp;<br>' ;
  	        echo '<strong>Итого:</strong> '.$price_info['sum_price'].'' ;


            ?></td></tr><?
  	    }

  	}
      if (!$options['no_table_colgroup_tr']  and !$options['only_tr_content']) {?></table><?}
      //trace() ;
   }



/**************************************************************************************************************************************

   СТАТИСТИКА ПЕРЕХОДОВ

***************************************************************************************************************************************/

   function show_stats_reffers($mode)
   { global $TM_stats,$TM_log_pages,$patch_to_site;

     list($name,$start_time,$end_time)=$this->get_time_stats_intervals($mode) ;

	 ?><h1>Статистика переходов на сайт <?echo $name?></h1><?
     if (!$mode or $mode==1)
     {
       list($stats,$min_time,$max_time)=get_stats_reffer($TM_log_pages,$start_time,$end_time) ;
       arsort($stats) ;
       if (sizeof($stats)) foreach($stats as $ref=>$cnt) $list[]=array('obj_name'=>$ref,'cnt'=>$cnt) ;
     }
	 else $list=execSQL('select obj_name,sum(indx) as cnt from '.$TM_stats.' where clss=48 and c_data>='.$start_time.' and r_data<='.$end_time.' group by obj_name order by cnt desc',0,1) ;

	 ?><table><tr class=clss_header><td colspan=3>Интервал обзора: <? echo date('d.m.y H:i:s',$start_time).' - '.date('d.m.y H:i:s',$end_time)?></td></tr>
	 		  <tr class=td_header><td>№</td><td>Адрес ресурса</td><td>Число заходов</td></tr><?
     if (sizeof($list)) foreach($list as $i=>$info)
      {?><tr><td><?echo $i+1?></td>
             <td class=left><?echo $info['obj_name']?></td>
             <td><?echo $info['cnt']?></td>
         </tr><?
      }
	 ?></table><?
   }

/**************************************************************************************************************************************

   СТАТИСТИКА ВХОДОВ

***************************************************************************************************************************************/

   function show_stats_logons($mode)
   { global $TM_stats,$TM_site_logons,$TM_site_debug ;
     $now=getdate() ;
     $end_time=time() ;
     switch ($mode)
     { case 1: 	$name='ежечастно, последние 24 часа' ;
     		   	$start_time=mktime($now['hours'],0,0,$now['mon'],$now['mday']-1,$now['year']); // сутки назад
     		   	break ;
       case 2: 	$name='ежедневно, в течении месяца' ;
                $start_time=mktime(0,0,0,$now['mon']-1,$now['mday'],$now['year']); // месяц назад
       			break ;
       case 3:  $name='еженедельно, в течении трех месяцев' ;
       			$start_time=mktime(0,0,0,$now['mon']-3,$now['mday'],$now['year']); // три месяца назад
       			break ;
       case 4:  $name='ежемесячно, в течении года' ;
       			$start_time=mktime(0,0,0,$now['mon'],1,$now['year']-1); // год назад
       			break ;
     }


     $res_list=execSQL('select obj_name,is_robot,sum(cnt) as sum_cnt from '.$TM_stats.' where clss=43 and c_data>='.$start_time.' and r_data<='.$end_time.' group by obj_name order by is_robot,sum_cnt desc') ;
     $tag_arr=array() ;
     $sum_real=0 ;
     $sum_all=0 ;
     //return ;

	 ?><h1>Статистика заходов на сайт <?echo $name?></h1><?
	 $cur_time=$start_time ;
	 while($cur_time<=$end_time)
	 {    // определяем текущий временной интервал
		  $now=getdate($cur_time) ;
	      switch ($mode)
	      { case 1: $cur_time2=mktime($now['hours'],59,59,$now['mon'],$now['mday'],$now['year']); break ;
	      	case 2: $cur_time2=mktime(23,59,59,$now['mon'],$now['mday'],$now['year']); break ;
	      	case 3: $cur_time2=mktime(23,59,59,$now['mon'],$now['mday']+6,$now['year']); break ;
	      	case 4: $cur_time2=mktime(23,59,59,$now['mon']+1,0,$now['year']); break ;
	      }

     	  if ($mode==1) { $list= execSQL_van('select count(c_data) as sum from '.$TM_site_logons.' where c_data>='.$cur_time.' and c_data<='.$cur_time2) ;
                          $stats=execSQL_van('select count(pkey) as all_pages,count(distinct(session_id)) as cnt_users,count(session_id) as show_pages from '.$TM_site_debug.'  where c_data>='.$cur_time.' and c_data<='.$cur_time2) ;

     	  				}
     	           else { $cur_list=execSQL('select obj_name,is_robot,sum(cnt) as sum_cnt from '.$TM_stats.' where clss=43 and c_data>='.$cur_time.' and r_data<='.$cur_time2.' group by obj_name order by is_robot,sum_cnt desc') ;

                          if (sizeof($cur_list)) { foreach($cur_list as $rec) { $res_list[$rec['obj_name']][$cur_time]=$rec['sum_cnt'] ;
                          														$sum_info[$rec['is_robot']][$cur_time]+=$rec['sum_cnt'] ;
                          														$sum_info_res[$rec['is_robot']]+=$rec['sum_cnt'] ;
                          													  }
                                                   // получаем статистику по качеству клиентов
                                                   $_user_info=execSQL_van('select sum(cnt) as sum_cnt,sum(cnt_init) as sum_cnt_init from '.$TM_stats.' where clss=42 and c_data>='.$cur_time.' and r_data<='.$cur_time2.' ') ;
                                                   $sum=$_user_info['sum_cnt']+$_user_info['sum_cnt_init'] ; $sum_all+=$sum ;
                                                   $real=$_user_info['sum_cnt_init'] ; $sum_real+=$real ;
                                                   if ($sum) $user_info[$cur_time]=round($real*100/$sum) ;

                                                   $tag_arr[]=$cur_time ;
                                                 }

     	            	  //$list= execSQL_van('select sum(indx) as sum from '.$TM_stats.' where clss=48 and c_data>='.$cur_time.' and r_data<='.$cur_time2) ;
                          //$stats=execSQL_van('select sum(users_cnt) as cnt_users,sum(page_cnt_users) as show_pages,sum(page_cnt_robots) as show_pages_robots from '.$TM_stats.' where clss=47 and c_data>='.$cur_time.' and r_data<='.$cur_time2) ;
     	                }
         /*
	      switch ($mode)
	      { case 1: $res_arr[date('d.m.y H:i:s',$cur_time).' - '.date('d.m.y H:i:s',$cur_time2)]=array('cnt_logons'=>$list['sum'],'show_pages'=>$stats['show_pages'],'cnt_users'=>$stats['cnt_users'],'cnt_robots'=>$stats['all_pages']-$stats['show_pages']) ; break ;
	      	case 2: $res_arr[date('d.m.y H:i:s',$cur_time).' - '.date('d.m.y H:i:s',$cur_time2)]=array('cnt_logons'=>$list['sum'],'show_pages'=>$stats['show_pages'],'cnt_users'=>$stats['cnt_users'],'cnt_robots'=>$stats['show_pages_robots']) ; break ;
	      	case 3: $res_arr[date('d.m.y H:i:s',$cur_time).' - '.date('d.m.y H:i:s',$cur_time2)]=array('cnt_logons'=>$list['sum'],'show_pages'=>$stats['show_pages'],'cnt_users'=>$stats['cnt_users'],'cnt_robots'=>$stats['show_pages_robots']) ; break ;
	      	case 4: $res_arr[date('d.m.y H:i:s',$cur_time).' - '.date('d.m.y H:i:s',$cur_time2)]=array('cnt_logons'=>$list['sum'],'show_pages'=>$stats['show_pages'],'cnt_users'=>$stats['cnt_users'],'cnt_robots'=>$stats['show_pages_robots']) ; break ;
	      }
         */

	      switch ($mode)
	      { case 1: $cur_time=mktime($now['hours']+1,0,0,$now['mon'],$now['mday'],$now['year']); break ;
	      	case 2: $cur_time=mktime(0,0,0,$now['mon'],$now['mday']+1,$now['year']); break ;
	      	case 3: $cur_time=mktime(0,0,0,$now['mon'],$now['mday']+7,$now['year']); break ;
	      	case 4: $cur_time=mktime(0,0,0,$now['mon']+1,1,$now['year']); break ;
	      }


	 }

	 //print_2x_arr($res_list) ;
	 //if (is_array($res_arr)) $res_arr=array_reverse($res_arr) ;
	 ?><table><tr class=clss_header><td colspan=<?echo (sizeof($tag_arr)+2)?>>Интервал обзора: <? echo date('d.m.y H:i:s',$start_time).' - '.date('d.m.y H:i:s',$end_time)?></td></tr>
	 		  <tr class=td_header><td>User Agent</td>
	 		  					  <td>Общее число заходов</td>
	 		  					  <? if (sizeof($tag_arr)) foreach($tag_arr as $data) {?><td><?echo date('d.m.y',$data)?></td><?}?>

	 		  </tr>
	 <?

	 ?><tr class=clss_header><td>Посетители</td><td><?echo $sum_info_res[0]?></td><? if (sizeof($tag_arr)) foreach($tag_arr as $data) {?><td><?echo $sum_info[0][$data]?></td><?}?></tr><?
	 ?><tr><td class="center width_300"><strong><a href="http://ru.wikipedia.org/wiki/Целевая аудитория" target=_CLEAN>Целевая аудитория</a></strong></td>
       	      <td><?echo round($sum_real*100/$sum_all)?>%</td>
	 		  <? if (sizeof($tag_arr)) foreach($tag_arr as $data) {?><td class=black><?echo $user_info[$data]?>%</td><?}?>
      </tr><?
     if (sizeof($res_list)) foreach($res_list as $rec) if (!$rec['is_robot'])
       {?><tr><td class="left width_300"><?echo $rec['obj_name']?></td>
       	      <td><?echo $rec['sum_cnt']?></td>
	 		  <? if (sizeof($tag_arr)) foreach($tag_arr as $data) {?><td><?echo $rec[$data]?></td><?}?>
          </tr><?}

	 ?><tr class=clss_header><td>Роботы</td><td><?echo $sum_info_res[1]?></td><? if (sizeof($tag_arr)) foreach($tag_arr as $data) {?><td><?echo $sum_info[1][$data]?></td><?}?></tr><?
     if (sizeof($res_list)) foreach($res_list as $rec) if ($rec['is_robot'])
       {?><tr><td class=left><?echo robot_name($rec['obj_name'])?></td>
       	      <td><?echo $rec['sum_cnt']?></td>
	 		  <? if (sizeof($tag_arr)) foreach($tag_arr as $data) {?><td><?echo $rec[$data]?></td><?}?>
          </tr><?}



	 ?></table><?
   }

/**************************************************************************************************************************************

   СТАТИСТИКА ИСПОЛЬЗОВАНИЯ РЕСУРСОВ

***************************************************************************************************************************************/

   function show_stats_debug($mode)
   { global $TM_stats,$TM_log_pages ;
     $now=getdate() ;
     $end_time=time() ;
     switch ($mode)
     { case 1: 	$name='ежечастно, в течении суток' ;
     		   	$start_time=mktime($now['hours'],0,0,$now['mon'],$now['mday']-1,$now['year']); // сутки назад
     		   	break ;
       case 2: 	$name='ежедневно, в течении месяца' ;
                $start_time=mktime(0,0,0,$now['mon']-1,$now['mday'],$now['year']); // месяц назад
       			break ;
       case 3:  $name='еженедельно, в течении трех месяцев' ;
       			$start_time=mktime(0,0,0,$now['mon']-3,$now['mday'],$now['year']); // три месяца назад
       			break ;
       case 4:  $name='ежемесячно, в течении года' ;
       			$start_time=mktime(0,0,0,$now['mon'],1,$now['year']-1); // год назад
       			break ;
     }
	 ?><h1>Статистика использования ресурсов <?echo $name?></h1><?
	 $cur_time=$start_time ;
	 while($cur_time<=$end_time)
	 {    // определяем текущий временной интервал
		  $now=getdate($cur_time) ;
	      switch ($mode)
	      { case 1: $cur_time2=mktime($now['hours'],59,59,$now['mon'],$now['mday'],$now['year']); break ;
	      	case 2: $cur_time2=mktime(23,59,59,$now['mon'],$now['mday'],$now['year']); break ;
	      	case 3: $cur_time2=mktime(23,59,59,$now['mon'],$now['mday']+6,$now['year']); break ;
	      	case 4: $cur_time2=mktime(23,59,59,$now['mon']+1,0,$now['year']); break ;
	      }

     	  if ($mode==1)
     	  { $stats[$cur_time]=execSQL_van('select count(pkey) as cnt,round(sum(time_gen)) as sum_time_gen,round(avg(time_gen),4) as avg_time_gen,round(avg(mem_size)) as avg_mem_size,sum(sql_cnt) as sum_sql_cnt,round(avg(sql_cnt)) as avg_sql_cnt,round(sum(sql_time)) as sum_sql_time,round(avg(sql_time),4) as avg_sql_time,count(distinct(session_id)) as users_cnt,count(distinct(ip)) as ip_cnt,min(c_data) as c_data,max(c_data) as r_data from '.$TM_log_pages.' where c_data>='.$cur_time.' and c_data<='.$cur_time2) ;
     	    $stats2=execSQL_van('select count(distinct(session_id)) as cnt from '.$TM_log_pages.' where c_data>='.$cur_time.' and c_data<='.$cur_time2) ;
            $stats3=execSQL('select is_robot,count(pkey) as cnt from '.$TM_log_pages.' where c_data>='.$cur_time.' and c_data<='.$cur_time2.' group by is_robot') ;
            $stats[$cur_time]['cnt_init']=$stats2['cnt'] ;
            $stats[$cur_time]['page_cnt_users']=$stats3[0]['cnt'] ;
			$stats[$cur_time]['page_cnt_robots']=$stats3[1]['cnt'] ;
          } else $stats[$cur_time]=execSQL_van('select count(pkey),sum(cnt) as cnt,sum(cnt_init) as cnt_init,sum(sum_time_gen) as sum_time_gen,round(avg(avg_time_gen),4) as avg_time_gen,round(avg(avg_mem_size)) as avg_mem_size,sum(sum_sql_cnt) as sum_sql_cnt,round(avg(avg_sql_cnt)) as avg_sql_cnt,sum(sum_sql_time) as sum_sql_time,round(avg(avg_sql_time),4) as avg_sql_time,sum(users_cnt) as users_cnt,sum(ip_cnt) as ip_cnt,sum(page_cnt_users) as page_cnt_users,sum(page_cnt_robots) as page_cnt_robots, min(c_data) as c_data, max(r_data) as r_data  from '.$TM_stats.' where clss=47 and c_data>='.$cur_time.' and r_data<='.$cur_time2) ;

          if (!$stats[$cur_time]['c_data']) $stats[$cur_time]['c_data']=$cur_time ;
          if (!$stats[$cur_time]['r_data']) $stats[$cur_time]['r_data']=$cur_time2 ;

	      switch ($mode)
	      { case 1: $cur_time=mktime($now['hours']+1,0,0,$now['mon'],$now['mday'],$now['year']); break ;
	      	case 2: $cur_time=mktime(0,0,0,$now['mon'],$now['mday']+1,$now['year']); break ;
	      	case 3: $cur_time=mktime(0,0,0,$now['mon'],$now['mday']+7,$now['year']); break ;
	      	case 4: $cur_time=mktime(0,0,0,$now['mon']+1,1,$now['year']); break ;
	      }
	 }
	 $stats=array_reverse($stats) ;
     $this->show_based_obj_list($stats,array('read_only'=>1,'clss'=>47,'no_check'=>1,'title'=>'Статистика использования ресурсов','all_enabled'=>1)) ;

   }


/**************************************************************************************************************************************

   СТАТИСТИКА ТОР страниц сайта

***************************************************************************************************************************************/

// статистика покаызвается интегрированно за день, неделю, месяц, год

   function show_stats_top_pages($mode)
   { global $TM_stats,$TM_log_pages,$patch_to_site;

     list($name,$start_time,$end_time)=$this->get_time_stats_intervals($mode) ;

	 ?><h1>Статистика ТОР страниц сайта <?echo $name?></h1><?
     if ($mode==1)
     {
       list($stats,$min_time,$max_time)=get_stats_top_pages($TM_log_pages,$start_time,$end_time) ;
       arsort($stats) ;
       if (sizeof($stats)) foreach($stats as $ref=>$ref_info) $list[]=array('obj_name'=>$ref_info['obj_name'],'page_name'=>$ref,'cnt'=>$ref_info['cnt'],'reffer'=>$ref_info['reffer']) ;
     }
	 else $list=execSQL('select page_name,obj_name,reffer,sum(indx) as cnt from '.$TM_stats.' where clss=46 and c_data>='.$start_time.' and r_data<='.$end_time.' group by page_name order by cnt desc',0,1) ;

	 ?><table><tr class=clss_header><td colspan=4>Интервал обзора: <? echo date('d.m.y H:i:s',$start_time).' - '.date('d.m.y H:i:s',$end_time)?></td></tr>
	 		  <tr class=td_header><td>№</td><td>Страница</td><td>Число показов</td><td>Заголовок страницы</td></tr><?
     if (sizeof($list)) foreach($list as $i=>$info)
      {?><tr><td><?echo $i+1?></td>
             <td class=left><a href='<?echo $patch_to_site.mb_substr($info['page_name'],1)?>' target=_clean><?echo $info['page_name']?></a></td>
             <td><?echo $info['cnt']?></td>
             <td class=left><?echo $info['obj_name']?></td>
         </tr><?
      }
	 ?></table><?
   }



/**************************************************************************************************************************************

   Информация по журналам

***************************************************************************************************************************************/

 function get_log_info($table_name,$usl='')
 { // получаем информацию по числу записей в журнале
   $usl_sql=($usl)? ' where '.$usl:'' ;
   $info=execSQL_van('select count(pkey) as cnt,min(c_data)  as min_data,max(c_data) as max_data  from '.$table_name.' '.$usl_sql) ;
   return(array('cnt'=>$info['cnt'],'first_data'=>date('d.m.y H:i:s ',$info['min_data']),'last_data'=>date('d.m.y H:i:s ',$info['max_data']))) ;
 }

 function show_log_info()
 { global $log_pages_info,$TM_log_pages,$TM_stats ;
 	?><h1>Информация по журналам</h1>
 	  <h2>Состояние протоколирования</h2>
	  Ведение журнала загрузки страниц - <?echo ($log_pages_info)? 'Включено':'отключено';?><br>
	  <h2>Информация по журналам</h2>
	  <table><tr><td>Журнал</td><td>Кол-во<br>записей</td><td>Дата первой записи</td><td>Дата последней записи</td></tr>
	  <? $rec_info=$this->get_log_info($TM_log_pages);?>
	  <tr><td class=left>Журнал страниц сайта</td><td class=left><?echo $rec_info['cnt']?></td><td><?echo $rec_info['first_data']?></td><td><?echo $rec_info['last_data']?></td></tr>
	  <? $rec_info=$this->get_log_info($TM_stats,' clss!=1');?>
	  <tr><td class=left>Всего статистика</td><td class=left><?echo $rec_info['cnt']?></td><td><?echo $rec_info['first_data']?></td><td><?echo $rec_info['last_data']?></td></tr>
	  <? $rec_info=$this->get_log_info($TM_stats,' clss=42');?>
	  <tr><td class=left>Статистика по посетителям [42]</td><td class=left><?echo $rec_info['cnt']?></td><td><?echo $rec_info['first_data']?></td><td><?echo $rec_info['last_data']?></td></tr>
	  <? $rec_info=$this->get_log_info($TM_stats,' clss=43');?>
	  <tr><td class=left>Статистика по входам [43]</td><td class=left><?echo $rec_info['cnt']?></td><td><?echo $rec_info['first_data']?></td><td><?echo $rec_info['last_data']?></td></tr>
	  <? $rec_info=$this->get_log_info($TM_stats,' clss=46');?>
	  <tr><td class=left>Статистика ТОР страниц [46]</td><td class=left><?echo $rec_info['cnt']?></td><td><?echo $rec_info['first_data']?></td><td><?echo $rec_info['last_data']?></td></tr>
	  <? $rec_info=$this->get_log_info($TM_stats,' clss=47');?>
	  <tr><td class=left>Статистика ресурсов [47]</td><td class=left><?echo $rec_info['cnt']?></td><td><?echo $rec_info['first_data']?></td><td><?echo $rec_info['last_data']?></td></tr>
	  <? $rec_info=$this->get_log_info($TM_stats,' clss=48');?>
	  <tr><td class=left>Статистика по переходам [48]</td><td class=left><?echo $rec_info['cnt']?></td><td><?echo $rec_info['first_data']?></td><td><?echo $rec_info['last_data']?></td></tr>
      </table>
 	<?
 }

// -------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------
//  функции из старой версии движки 4.71 для нормальной работы модуля
// -------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------

  function print_table_colgroup($tkey,$clss,$edit_list,$options=array())
     { global $descr_obj_tables,$lang_arr ;

 	  if (!$options['no_check']) 														$cols[]='<col id=check>' ;
       if (!$options['read_only'] and !$options['no_icon_edit'] and !$options['_small']) 	$cols[]='<col id=edit>' ;    // временно отключено для  small
       if (!$options['no_icon_photo'] and $clss!=3 and isset($descr_obj_tables[$tkey]->list_clss[3])) 					$cols[]='<col id=img>' ;

 	   if (sizeof($edit_list)) foreach ($edit_list as $fname=>$edit_info)
 	   {

 	     //$width='' ; //$width=$edit_list[$fname]['width'] ;
 	     //if (!$width)
              $width='auto' ;

 	     $i=0 ;
 	     if ($descr_obj_tables[$tkey]->list_clss_ext[$clss]['multilang'][$fname] )
            { if (sizeof($lang_arr)) foreach($lang_arr as $lang_code=>$lang_info)
              {  $lang_fname=($lang_code)? $fname.'_'.$lang_code:$fname ;
                 if (isset($descr_obj_tables[$tkey]->field_info[$lang_fname])) {$cols[]='<col style="'.$width.';">' ; $i++ ;}
              }
            }

          if (!$i) $cols[]='<col style="'.$width.';">' ; // если ничего не было показано - обычный вывод поля
 	   }
 	  echo '<colgroup>'.implode('',$cols).'</colgroup>' ;
    }



 // заголовок таблицы с типом класса
 function print_table_header($title,$options=array())
  { $obj_clss=_CLSS($options['icon_clss']);
    if ($options['icon_clss']) $icon_text='<div class=clss_icon><img alt="" src='.$obj_clss->icons.' width="16" height="64" alt="" border="0" /></div> ' ;
    if ($options['colspan']) $colspan_text='colspan='.$options['colspan'] ;
    /*?><tr class=clss_header><td <?echo $colspan_text?>><span class=fast_search><input class="fast_search text" type=text></span><span class=setting><?echo $options['page_info']?></span></td></tr><?*/
    ?><tr class=clss_header><td <?echo $colspan_text?>><span class=title><? echo $icon_text.$title?></span><span class=setting><?echo $options['page_info']?></span></td></tr><?
 }

  // показывает заголовки для ячеек таблицы для конкетного класса
  function print_td_header($tkey,$clss,$edit_list,$options=array())
    { global $descr_obj_tables,$lang_arr ;
     //trace() ;
     //echo '$tkey='.$tkey.'<br>' ;
     //echo '$clss='.$clss.'<br>' ;
        $edit_td='' ;   $image_td='' ;

      //if (!$options['no_check']) $check_td='<td><input name="check_all" type="checkbox" value="ON" /></td>' ;

      if (!$options['no_check'])
          { //if (!sizeof($first_rec['__reffer_from']))
              $check_td='<td><input name="check_all" type="checkbox" value="ON" /></td>' ;
            //else
            //  $check_td='<td><input name="check_all" type="checkbox" value="ON"/></td>' ;
          } else $check_td='' ;

      if (!$options['read_only'] and !$options['no_icon_edit'] and !$options['_small']) $edit_td ='<td>Правка</td>' ;
      if (!$options['no_icon_photo'] and $clss!=3 and isset($descr_obj_tables[$tkey]->list_clss[3])) $image_td='<td>Фото</td>' ;

	   // выводим заголовки полей
	   echo $check_td.$edit_td.$image_td ;
	   if (sizeof($edit_list)) foreach ($edit_list as $fname=>$edit_info) if (!isset($options['no_view_field'][$fname]))
	   { if (!is_array($edit_info)) if (isset($_SESSION['pattern_clss_fields'][$edit_info])) $edit_info=$_SESSION['pattern_clss_fields'][$edit_info] ; else $edit_info=array('title'=>$edit_info) ;
         // делаем проверку на мультиязыность, если + , то выводим доп. колонку
	     // условие +:
	     // у таблицы включена мультиязчность, текущее поле имеет тип "строка", поле с соответствующем языком есть в базе
	     $i=0 ;
	     if ($descr_obj_tables[$tkey]->list_clss_ext[$clss]['multilang'][$fname] )
           { if (sizeof($lang_arr)) foreach($lang_arr as $lang_code=>$lang_info)
             {  $lang_fname=($lang_code)? $fname.'_'.$lang_code:$fname ;
                if (isset($descr_obj_tables[$tkey]->field_info[$lang_fname])) {?><td><?echo $edit_info['title'].' ('.$lang_info['name'].')' ; $i++ ;?></td><?}
             }
           }

         if ($fname and $options['order_by']==$fname) $sort_img_src=($options['order_mode']=='asc')? _PATH_TO_ENGINE.'/admin/images/sort_down.png':_PATH_TO_ENGINE.'/admin/images/sort_up.png' ;
         else                              $sort_img_src='' ;
         if ($sort_img_src)                $img='<img class=sort src="'.$sort_img_src.'">' ;
         else                              $img='' ;

         if (!$i) {?><td <?if ($fname and !$options['no_ext_func']) echo 'fname="'.$fname.'"'?>><?echo $edit_info['title']?><?echo $img?></td><?} // если ничего не было показано - обычный вывод поля
	   }
   }

 // показывает запись в виде строки таблицы (<td>...<td><td>...<td><td>...<td><td>...<td>)
 function print_obj_rec_in_table(&$rec,$edit_list=array(),$options=array())
    { global $patch_to_img,$descr_obj_tables,$lang_arr ;
        $tkey=$rec["tkey"] ;
        $pkey=$rec["pkey"] ;
    	$clss=($options['clss'])? $options['clss']:$rec['clss'] ; $rec['clss']=$clss ;

        $obj_clss=_CLSS($clss) ;

        $obj_clss->before_view_rec($rec,$options) ;


   		// если в init.php задан шаблон вывода объектов текущего класса, то использовать его
   		// список полей для быстрого редактирования будет или задан для таблицы или взять с класса по умолчанию
   		if (!sizeof($edit_list)) // если список полей не передали ищем сами
   		{ $list_block=($options['small'])? 'small':'list' ;
   		  $edit_list=$obj_clss->view[$list_block]['field'] ; //damp_array($edit_list); echo '<br>' ;
   		  if (is_array($obj_clss->view[$list_block]['options'])) $options=array_merge($options,$obj_clss->view[$list_block]['options']) ;
   		}

        // если объект - ссылка - добавляем соответвующую иконку
        if (sizeof($rec['__reffer_from'])) $icon_short='<img src="'.$patch_to_img.'short2.gif" width="16" height="16" alt="Текущая запись - ссылка" border="0" />' ;
         else $icon_short='' ;

        // если разрешено выделение объектов выводим колонку чекбокса
        $check_status=($options['check_all'])? 'checked':'' ;
        if (!$options['no_check'])
          { if (!sizeof($rec['__reffer_from'])) $check_box=generate_check($rec["tkey"],$rec["clss"],$rec['pkey'],$rec["parent"],$check_status) ;
            else                                $check_box=generate_check($rec['__reffer_from']['tkey'],$rec['__reffer_from']['clss'],$rec['__reffer_from']['pkey'],$rec['__reffer_from']['parent'],$check_status,$rec['tkey'],$rec["clss"],$rec['pkey'],$rec["parent"],$check_status) ;
            $check_td='<td>'.$icon_short.$check_box.'</td>' ;
          }
        else $check_td='' ;

        $edit_td=(!$options['read_only'] and !$options['no_icon_edit'] and !$options['_small'])? '<td class=to_edit></td>':'' ;

        // если таблица поддерживает фото, то значок для добавления фото объекта
        $icon_edit_img=(!$options['read_only'])? '<img cmd=panel_obj_img_upload mode=modal_window parent_reffer="'.$rec['_reffer'].'" clss=3 after_close_window="update_tr_rec" src="'.$patch_to_img.'tree_close.gif" alt="Редактировать фото" border="0" />':'' ;

        // показ иконки изображения объекта
        // будет показано фото, кроме объектов "фото" и наследуемых от них - у них показ фото отдельным "edit_element"
        if (!$options['no_icon_photo'] and $clss!=3 and isset($descr_obj_tables[$tkey]->list_clss[3]))
        { if ($rec["_image_name"])
               { $image_td='<td class=img_preview>' ;
                 // пока показываем только первое фото
                 $image_td.='<a href="'.img_clone($rec,'source').'" class=group_'.$rec['pkey'].' rel="highslide"><img src="'.img_clone($rec,'small').'"></a>' ;
                 if (!$options['no_edit_img'])
                 { $image_td.=$icon_edit_img ;
                   $image_td.='<div class=cnt_img><a href="" onclick="show_obj('.$rec["tkey"].','.$rec["pkey"].',1,\'structure_3\');return(false);">'.sizeof($rec['_all_images']).' шт.</a></div>' ;
                 }
                 $image_td.='</td>' ;
               }
          else { $image_td='<td class=img_preview>' ;
                 if (!is_array($obj_clss->parent_to) or array_search(3,$obj_clss->parent_to)!==false) $image_td.=$icon_edit_img ;
                 $image_td.='</td>' ;
               }
        } else   $image_td='' ;

          echo $check_td ; // ячека с выбором объекта
          echo $edit_td ;  // ячейка с редактированием
          echo $image_td ; // ячейка с фото

	      // идем по списку полей редактирования для данного класса данной таблицы
	      //print_r($edit_list) ;

          //print_r($descr_obj_tables[$tkey]->list_clss_ext[$clss]['multilang']) ;

	      if (sizeof($edit_list)) foreach ($edit_list as $fname=>$edit_info) if (!isset($options['no_view_field'][$fname]))
			{ //if (!is_array($edit_info)) if (isset($_SESSION['pattern_clss_fields'][$edit_info])) $edit_info=$_SESSION['pattern_clss_fields'][$edit_info] ; else $edit_info=array('title'=>$edit_info) ;
			  //print_r($edit_info) ; echo '<br>' ;
			  //echo 'fname='.$fname.'<br>' ;


			  $td_class=($edit_info['td_class'])? 'class="'.$edit_info['td_class'].' gi"':'class=gi' ;
			  //$td_id='id=td_'.$rec['pkey'].'_'.$fname ;
			  //$td_onclick=(isset($descr_obj_tables[$tkey]->list_connect[$clss][$fname]))? 'onclick=generate_select(this,"'.$fname.'","'.$rec[$fname].'","'.$edit_info['class'].'")':'' ;

			  if ($options['read_only']) 			$edit_info['read_only']=1 ;
			  if ($options['no_htmlspecialchars']) 	$edit_info['no_htmlspecialchars']=1 ;

			  	// делаем проверку на мультиязыность, если + , то выводим доп. колонку
				// условие +:
				// у таблицы включена мультиязчность, текущее поле имеет тип "строка", поле с соответствующем языком есть в базе
			  $i=0 ;
			  if ($descr_obj_tables[$tkey]->list_clss_ext[$clss]['multilang'][$fname])
				  { //echo 'multilang='.$descr_obj_tables[$tkey]->list_clss_ext[$clss]['multilang'][$fname].'<br>';
				     if (sizeof($lang_arr)) foreach($lang_arr as $lang_code=>$lang_info)
                     { $lang_fname=($lang_code)? $fname.'_'.$lang_code:$fname ;
                       if (isset($descr_obj_tables[$tkey]->field_info[$lang_fname]))
				       { $edit_info['use_lang_suff']='_'.$lang_code ;
				         list($text,$td_attr)=generate_field_text($rec,$lang_fname,$edit_info) ;
				       	 ?><td <?echo $td_attr?>><?echo $text?></td><?
				       	 $i++ ;
				       }
                     }
				  }

              // если ничего не было показано - обычный вывод поля
			  if (!$i)
			  { list($text,$td_attr)=generate_field_text($rec,$fname,$edit_info) ; //print_r($res) ;
			  	?><td <?echo $td_attr?>><?echo $text?></td><?
			  }
			}


	    }

}

?>