<?php
$__functions['init'][]		='_request_admin_vars' ;
$__functions['alert'][]		='_request_admin_alert' ;
$__functions['install'][]	='_request_install_modul' ;
$__functions['boot_admin'][]='_request_admin_boot' ;

function _request_admin_vars() //
{
	//-----------------------------------------------------------------------------------------------------------------------------
	// описание переменных модуля
	//-----------------------------------------------------------------------------------------------------------------------------

    $_SESSION['ARR_list_request_status']=array('0'=>'Не обработан','1'=>'Обработан');

	//-----------------------------------------------------------------------------------------------------------------------------
	// описание классов модуля
	//-----------------------------------------------------------------------------------------------------------------------------
	$_SESSION['descr_clss'][31]['name']='Заявка' ;
	$_SESSION['descr_clss'][31]['parent']=0 ;
	$_SESSION['descr_clss'][31]['fields']=array('code'=>'varchar(32)',
                                                'account_reffer'=>'any_object',
                                                'autor_email'=>'text',
                                                'autor_name'=>'text',
                                                'autor_phone'=>'text',
                                                'value'=>'serialize', // массив данных запроса, если в запросе идет более чем имя, email, телефон
                                                'message'=>'text', // сообщение запроса
                                                'comment'=>'text',
                                                'obj_reffer'=>'any_object',
                                                'obj_reffer_name'=>'text',
                                                'obj_reffer_href'=>'varchar(255)',
                                                'edit_code'=>'varchar(32)',
                                                'status'=>array('type'=>'indx_select','array'=>'ARR_list_request_status','default'=>0)) ;
	$_SESSION['descr_clss'][31]['parent_to']=array(5) ;

    $_SESSION['descr_clss'][31]['icons']=_PATH_TO_BASED_CLSS_IMG.'/7.png' ;

	$_SESSION['descr_clss'][31]['list']['field']['c_data']=array('title'=>'Создано','data_format'=>'d.m.Y H:i') ;
	$_SESSION['descr_clss'][31]['list']['field']['pkey']=array('title'=>'Номер заявки') ;
	$_SESSION['descr_clss'][31]['list']['field']['status']=array('title'=>'Состояние') ;
	$_SESSION['descr_clss'][31]['list']['field']['obj_name']=array('title'=>'Тема') ;
    $_SESSION['descr_clss'][31]['list']['field'][]=array('title'=>'Автор','on_view'=>'clss_31_show_autor','td_class'=>'left') ;
    $_SESSION['descr_clss'][31]['list']['field'][]=array('title'=>'На что','on_view'=>'clss_31_show_reffer','td_class'=>'left') ;
    $_SESSION['descr_clss'][31]['list']['field']['value']=array('title'=>'Данные запроса') ;
    $_SESSION['descr_clss'][31]['list']['field']['message']=array('title'=>'Текст запроса') ;
    $_SESSION['descr_clss'][31]['list']['field']['comment']=array('title'=>'Комментарий администора') ;

    $_SESSION['descr_clss'][31]['details']['field']['c_data']=array('title'=>'Создано','data_format'=>'d.m.Y H:i') ;
    $_SESSION['descr_clss'][31]['details']['field']['pkey']=array('title'=>'Номер заявки') ;
    //$_SESSION['descr_clss'][31]['details']['field']['enabled']=array('title'=>'Запрос обработан') ;
    $_SESSION['descr_clss'][31]['details']['field']['obj_name']=array('title'=>'Тема') ;
    $_SESSION['descr_clss'][31]['details']['field']['autor_name']=array('title'=>'Автор') ;
    $_SESSION['descr_clss'][31]['details']['field']['autor_email']=array('title'=>'Контактный email') ;
    $_SESSION['descr_clss'][31]['details']['field']['autor_phone']=array('title'=>'Контактный телефон') ;
	$_SESSION['descr_clss'][31]['details']['field']['account_reffer']=array('title'=>'Аккаунт автора') ;
    $_SESSION['descr_clss'][31]['details']['field']['value']=array('title'=>'Данные запроса') ;
    $_SESSION['descr_clss'][31]['details']['field']['message']=array('title'=>'Текст запроса') ;
    $_SESSION['descr_clss'][31]['details']['field']['comment']=array('title'=>'Комментарий администратора') ;
	$_SESSION['descr_clss'][31]['details']['field']['obj_reffer']=array('title'=>'Объект вопроса','on_view'=>'clss_31_show_reffer') ;


	//----------------------------------------------------------------------------------------------------------------------
	// описание пунктов меню админа сайта
	//-----------------------------------------------------------------------------------------------------------------------
    $_SESSION['menu_admin_site']['Модули']['request']=array('name'=>'Запросы услуг','href'=>'editor_request.php','icon'=>_PATH_TO_MODULES.'/request/images/serviceRequestSummaryIcon.png') ;

}

function _request_admin_boot($options)
{
	create_system_modul_obj('request') ;
}


function _request_install_modul($DOT_root)
{   echo '<h2>Инсталируем модуль <strong>Запросы</strong></h2>' ;

    $file_items=array() ;
    // editor_request.php
    $file_items['editor_request.php']['include'][]='_DIR_TO_MODULES."/request/m_request_frames.php"' ;
    $file_items['editor_request.php']['options']['use_table_code']='"TM_request"' ;
    $file_items['editor_request.php']['options']['title']='"Редактор запросов"' ;
    $file_items['editor_request_tree.php']['include'][]='_DIR_TO_MODULES."/request/m_request_frames.php"' ;
    $file_items['editor_request_viewer.php']['include'][]='_DIR_TO_MODULES."/request/m_request_frames.php"' ;
    create_menu_item($file_items) ;

	// таблица запросов
	$pattern=array() ;

	$pattern['table_title']				= 	'Запросы' ;
	$pattern['use_clss']				= 	'1,31' ;
	$pattern['def_recs'][]				=	array ('parent'=>0,	'clss'=>1,	'enabled'=>1,	'indx'=>0,	'obj_name'=>'Система Запросов') ;
   	$pattern['table_name']				= 	$_SESSION['TM_request']  ;
	$pattern['table_clss']				= 	101 ;
	$pattern['table_parent']			= 	$DOT_root;
   	$id_DOT_ord=create_table_by_pattern($pattern) ;

    SETUP_get_img('/request/admin_img_menu/speech_balloon.jpg',_PATH_TO_ADMIN.'/img/menu/speech_balloon.jpg') ;

    create_dir('/request/') ;

    $file_items=array() ;
    $file_items['index.php']['options']['system']='"request_system"' ;
    $file_items['index.php']['options']['use_include_patch']='_DIR_TO_MODULES."/galerey/"' ;
    //$file_items['create.php']['options']['system']='"request_system"' ;
    //$file_items['create.php']['options']['use_include_patch']='_DIR_TO_MODULES."/galerey/"' ;
    $file_items['.htaccess']['text'][]='RewriteEngine On' ;
    $file_items['.htaccess']['text'][]='RewriteRule ^(.*)/(.*).html$ index.php[L]' ;
    $file_items['.htaccess']['text'][]='RewriteRule ^(.*).html$ index.php [L]' ;
    $file_items['.htaccess']['text'][]='RewriteRule ^(.*)/$ index.php [L]' ;
    create_script_to_page($file_items,_DIR_TO_ROOT.'/request/','../ini/patch.php') ;

    import_setting_from_xml(_PATH_TO_SERVER_SETUP.'?m=request&s=setting',array('parent_tkey'=>$_SESSION['pkey_by_table'][TM_SETTING],'parent_pkey'=>1,'check_unique_name'=>1)) ;
    import_setting_from_xml(_PATH_TO_SERVER_SETUP.'?m=request&s=mails',array('parent_tkey'=>$_SESSION['pkey_by_table'][TM_MAILS],'parent_pkey'=>1,'check_unique_name'=>1)) ;


}

function clss_31_show_autor($rec,$tkey,$pkey,$options,$cur_fname)
{ $text=array() ;
  if ($rec['autor_name']) $text[]=$rec['autor_name'] ;
  if ($rec['autor_email']) $text[]=$rec['autor_email'] ;
  if ($rec['autor_phone']) $text[]=$rec['autor_phone'] ;
  return(implode('<br>',$text)) ;
}

function clss_31_show_reffer($rec,$tkey,$pkey,$options,$cur_fname)
{ $arr=array() ;
  if ($rec['obj_reffer']) $arr[]=generate_obj_text($rec['obj_reffer'],$options).'<br>' ;
  if ($rec['obj_reffer_parent']) $arr[]=generate_obj_text($rec['obj_reffer_parent'].'.'.$_SESSION['goods_system']->tkey,$options).'<br>'  ;
  if ($rec['obj_reffer_href']) $arr[]='<strong>URL:</strong><a href="'.$rec['obj_reffer_href'].'" target=_blank>'.$rec['obj_reffer_href'].'</a>' ;
  return(implode('<br>',$arr)) ;
}

function _request_admin_alert()
{ $res=execSQL_van('select count(pkey) as cnt from '.$_SESSION['TM_request'].' where clss=31 and enabled=0') ;
  if ($res['cnt']) echo "<div>У Вас <span class='red bold'>".$res['cnt']."</span> новых сообщений в <a href='"._PATH_TO_ADMIN."/editor_request.php'>службе запросов</a></div><br>";
}

?>