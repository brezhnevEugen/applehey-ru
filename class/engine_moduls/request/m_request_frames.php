<?php
include_once(_DIR_TO_ENGINE."/admin/c_site.php") ;
include_once(_DIR_TO_ENGINE."/admin/i_clss_func.php") ;
include_once(_DIR_TO_ENGINE.'/admin/c_menu_tabs.php') ;

// редактор запросов
class c_editor_request extends c_editor_obj
{
  function body($options=array()) {  $this->body_frame_2x(array('fra_tree'=>_PATH_TO_ADMIN.'/editor_request_tree.php','title'=>$options['title'])) ; }
}

// дерево тикетов
class c_editor_request_tree extends c_fra_tree
{ public $system_name='request_system' ;

 function body($options=array())
 {?><body id=tree_objects><div id=tree_menu><?$this->top_menu() ;?></div><?
     $this->generate_time_tree('Запросы',$this->tkey,array('usl_select'=>'clss=31','on_click_script'=>'editor_request_viewer.php','on_click_item'=>array(31=>'fra_viewer.php')));
  ?></body><?
 }

}

class c_editor_request_viewer extends c_fra_based
{ public $system_name='request_system' ;
  public $top_menu_name='top_menu_list_orders' ;

  function body(&$options=array()) {$this->body_frame($options) ; }

// заголовок фрейма
 function show_page_obj_header()
 { $title=($_GET['pkey']=='root')? 'Запросы':'Запросы '.$this->get_time_title($this->start_time,$this->end_time) ;
   ?><p class='obj_header'><?echo $title?></p><?
 }
    
 // меню фрейма
 function show_page_obj_menu($obj_info)
    {  global $request_system ;
      // создаем объект top_menu
        if (!is_object($_SESSION[$this->top_menu_name])) $_SESSION[$this->top_menu_name]=new c_top_menu() ;
       // формируем набор пунктом меню
       $menu_set=array() ;
       if ($this->pkey=='root')  // пункты меню для корня дерева
       { $menu_set[]=array("name" => 'Последние запросы на сайт',"cmd" =>'show_list_items','mode'=>'last_items','count'=>20) ;
       }
       else // выбран один из временных интервалов
       { $time_usl=$this->get_time_usl($this->start_time,$this->end_time) ;
         $cnt=$_SESSION['request_system']->get_count($time_usl) ;
         $menu_set[]=array("name" => 'Запросы ('.$cnt.')',"cmd" =>'show_list_items') ;
       }
       // выводим меню
       $this->cur_menu_item=$_SESSION[$this->top_menu_name]->show($menu_set) ;  // показываем меню
       return($this->cur_menu_item) ;
    }    
    

  function show_list_items()
  { $options=array() ;
    $_usl[]=$this->get_time_usl($this->start_time,$this->end_time) ;
    if (isset($this->cur_menu_item['status'])) $_usl[]='status='.$this->cur_menu_item['status'] ;
    $usl=implode(' and ',$_usl) ;
    $title='Запросы '.$this->get_time_title($this->start_time,$this->end_time) ;
    if (isset($this->cur_menu_item['status'])) $title.=' в состоянии "'.$_SESSION['list_status_orders'][$this->cur_menu_item['status']].'"' ;
    if ($this->cur_menu_item['mode']=='last_items')
    { $options['count']=$this->cur_menu_item['count'] ;
      $options['order']='c_data desc' ;
      $title.=' - последние '.$this->cur_menu_item['count'] ;
    }
    $options['default_order']='c_data desc' ;
    $options['title']=$title ;
    $options['buttons']=array('save','delete') ;
    _CLSS(31)->show_list_items($this->tkey,$usl,$options) ;
  }

}




