<?php

$__functions['init'][]='_request_site_vars' ;
$__functions['boot_site'][]='_request_site_boot' ;

function _request_site_vars() //
{	$_SESSION['TM_request']= 'obj_'.SITE_CODE.'_request' ;
    $_SESSION['TM_request_files']= 'obj_'.SITE_CODE.'_request_files' ;
    $_SESSION['init_options']['request']=array() ;
    $_SESSION['init_options']['request']['use_capcha_code']=0 ;
    $_SESSION['init_options']['request']['debug']=0 ;
    $_SESSION['init_options']['request']['usl_show_items']='enabled=1' ;
    $_SESSION['init_options']['request']['system_title']='Запросы на сайте' ;
}


function _request_site_boot($options)
{
    create_system_modul_obj('request',$options) ; // создание объекта request
}

class c_request_system extends c_system
{
  function c_request_system($create_options=array())
  {  // устанавливаем значения опций по умолчанию, если они не были заданы ранее
     if (!$create_options['table_name']) 			$create_options['table_name']=$_SESSION['TM_request'] ;
     if (!$create_options['root_dir'])				$create_options['root_dir']='request' ;
     if (!$create_options['usl_show_items'])        $create_options['usl_show_items']='clss=31 and enabled=1' ;
     if (!$create_options['system_title'])          $create_options['system_title']='Запросы на сайте' ;
     if (!$create_options['usl_show_items'])        $create_options['system_title']='enabled=1' ;
  	 parent::c_system($create_options) ;
  	 $this->use_capcha_code=$create_options['use_capcha_code'] ;  // создание сообщений только через капча-код
  	 $this->use_autorize_to_otwet=0 ; // модерация сообщений без авторизации на сайте
    $this->tkey_files=$_SESSION['pkey_by_table'][$_SESSION['TM_request_files']] ;
  }

  function get_request_count($usl_select='') {return($this->get_count($usl_select));}
  function get_count($usl_select='')
       { $cnt=execSQL_value('select count(pkey) as cnt from '.$this->table_name.' where clss=31 and '.$usl_select,0,0) ;
         return($cnt) ;
       }

    
  // готовим информацию по объекту - эта функция должны быть перекрыта в кажом классе
  function prepare_public_info(&$rec,$options=array())
  { $rec['__name']='Запрос от '.$rec['obj_name'].' '.date("d.m.y",$rec['c_data']) ;
  	$rec['__data']=date("d.m.y",$rec['c_data']) ;
  	
    if ($rec['obj_reffer'])  { $rec['obj_reffer_info']=select_db_ref_info($rec['obj_reffer'],array('no_text_fields'=>1)) ;
                               unset($rec['obj_reffer_info']['manual']) ;
                               unset($rec['obj_reffer_info']['intro']) ;
                             }

      
    $request_info_db=array() ;  $arr_request_info=array() ;
    if (is_array($rec['value'])) $request_info_db=$rec['value'] ;
    elseif (strlen($rec['value'])) $request_info_db=unserialize($rec['value']) ;
    if ($rec['autor_name']) $arr_request_info['ФИО']=$rec['autor_name'] ;
    if ($rec['autor_email']) $arr_request_info['Email']=$rec['autor_email'] ;
    if ($rec['autor_phone']) $arr_request_info['Телефон']=$rec['autor_phone'] ;
    if ($rec['comment'])        $arr_request_info['Текст запроса']=$rec['comment'] ; // на тот случай, если все таки в $rec['value'] просто текст сообщения
    
    if ($rec['obj_reffer'])  { $rec['obj_reffer_info']=select_db_ref_info($rec['obj_reffer'],array('no_text_fields'=>1)) ;
                               unset($rec['obj_reffer_info']['manual']) ;
                               unset($rec['obj_reffer_info']['intro']) ;
    if ($rec['obj_reffer']) $arr_request_info['Запрос на']='<a href="'.$rec['obj_reffer_href'].'">'.$rec['obj_reffer_name'].'</a>' ;  
                             }

    if (sizeof($request_info_db)) $arr_request_info=array_merge($arr_request_info,$request_info_db) ;
    $rec['__request_info']=damp_serial_data($arr_request_info);
  }    
    
 // добавляем запрос
 function create_message($send_data,$options=array())
 { $debug=$options['debug'] ;
   // проверяем правильность проверочного кода
   if (!$options['no_check_code'] and $this->use_capcha_code) if (!capcha_code_checkit()) return(prepare_result('error_check_code')) ;
   // создаем новый сопрос
   $message=array(	'parent'		=>	1, 
			        'clss'			=>	31,
     				'c_data'		=>	time(),
     				'enabled'		=>	1,
     				'obj_name'		=>	$send_data['theme'],
     				'code'		    =>	$send_data['code'],
    				'autor_email'	=>  $send_data['autor_email'],
    				'autor_phone'	=>  $send_data['autor_phone'],
                    'autor_name'	=>  $send_data['autor_name'],
                    'obj_reffer'	=>	$send_data['obj_reffer'], // объект на котором был задан вопрос
    				'obj_reffer_name'=>	$send_data['obj_reffer_name'], // название объекта
    				'obj_reffer_href'=>	$send_data['obj_reffer_href'], // с какой страницы был сделал запрос
    				'value'	        =>  $send_data['value'], // массив параметров запроса
    				'message'	    =>  $send_data['message'], // текст запроса
                    'edit_code'		 =>  md5(uniqid(rand(),true))
   				) ;

   if ($debug) damp_array($message) ;
   $message['pkey']=adding_rec_to_table($this->tkey,$message) ; // создаем запрос
   if (!$message['pkey']) return(prepare_result('db_error')) ;
   $message['_reffer']=$message['pkey'].'.'.$this->tkey ;
   $this->prepare_public_info($message) ;

   //загружаем файлы есть есть
   include_once(_DIR_TO_ENGINE.'/admin/i_admin_system.php') ;
   if (sizeof($_FILES['upload_files']['name']))  foreach($_FILES['upload_files']['name'] as $i=>$name) if ($name)
     {  $obj_file_ref=adding_rec_to_table($_SESSION['TM_request_files'],array('parent'=>$message['pkey'],'clss'=>5,'enabled'=>1),array('return_reffer'=>1)) ;
        $message['files'][]=obj_upload_file($obj_file_ref,array('name'=>$_FILES['upload_files']['name'][$i],'type'=>$_FILES['upload_files']['type'][$i],'tmp_name'=>$_FILES['upload_files']['tmp_name'][$i],'error'=>$_FILES['upload_files']['error'][$i],'size'=>$_FILES['upload_files']['size'][$i]),array('debug'=>0)) ;
     }

   // регистрируем событие 'Создание сообщения в системе запросов'
   $evt_id=_event_reg('Создание сообщения в системе запросов','',$message['_reffer'],$message['account_reffer']) ;
   // отправляем уведомление
   $options['use_event_id']=$evt_id ;
   $this->send_mail_new_message($message,$options) ;

   $result['code']='create_success' ;
   $result['type']='success' ;
   $result['rec']=$message ;

   return($result) ;
 }

 function send_mail_new_message($rec,$options=array())
 { $params['theme']=$rec['obj_name'] ;
   $params['site_name']=_MAIN_DOMAIN ;
   $params['name']=$rec['autor_name'] ;
   $params['email']=$rec['autor_email'] ;
   $params['phone']=$rec['autor_phone'] ;
   $params['request_info']=$rec['__request_info'] ;
   $params['data']=date("d.m.Y H:i",$rec['c_data']) ;
   $params['to']=($rec['obj_reffer_name'])? '"<a href="'.$rec['obj_reffer_href'].'">'.$rec['obj_reffer_name'].'</a>"':' ' ;
   $params['obj_href']=($rec['obj_reffer_name'])? '"<a href="'.$rec['obj_reffer_href'].'">'.$rec['obj_reffer_href'].'</a>"':' ' ;
   $params['message']=$rec['message'] ;  // если в  value был массив (serialize) то его данные будут в $rec['__request_info'], а само поле value - очищено
   if (sizeof($rec['files']))
   { $params['message'].='<br><br><strong>К запросу были приложены файлы:</strong><ul>' ;
     foreach($rec['files'] as $fdir) $params['message'].='<li><a href="http://'._MAIN_DOMAIN.hide_server_dir($fdir).'">'.basename($fdir).'</a></li>' ;
     $params['message'].='</ul>' ;
   }
   // отправляем уведомление менеджеру
   $event_mail_to=($options['event_mail_to'])? $options['event_mail_to']:$_SESSION['LS_request_system_email_alert'] ;
   $mail_template=($options['mail_template'])? $options['mail_template']:'request_system_message_create_alert' ;
   _send_mail_to_pattern($event_mail_to,$mail_template,$params,$options) ;
   // если указан код шаблона - отправляем уведомление клиенту
   if ($options['mail_template_to_member'] and  $rec['autor_email']) _send_mail_to_pattern($rec['autor_email'],$options['mail_template_to_member'],$params,$options) ;
 }



}


?>