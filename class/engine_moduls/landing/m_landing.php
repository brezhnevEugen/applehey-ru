<?php

$__functions['init'][]		='_landing_site_vars' ;
$__functions['boot_site'][]	='_landing_site_boot' ;

// функции быстрого доступа
function _show_rasdel_landing($id,$func_name,$options=array()) { $_SESSION['landing_system']->show_list_section($id,$func_name,$options) ;}
function _landing_SYSTEM() {return($_SESSION['landing_system']);}


function _landing_site_vars() //
{   $_SESSION['TM_landing']='obj_'.SITE_CODE.'_landing' ; $GLOBALS['TM_landing']=&$_SESSION['TM_landing'] ;

    // описание системы  -------------------------------------------------------------------------------------------------------------------
	//$_SESSION['init_options']['landing']['debug']=2 ;
	$_SESSION['init_options']['landing']['tree']['debug']=0 ;
	$_SESSION['init_options']['landing']['tree']['clss']='1,70' ;
	$_SESSION['init_options']['landing']['tree']['fields']='*' ;
	$_SESSION['init_options']['landing']['tree']['get_count_by_clss']=0 ;
    //$_SESSION['init_options']['landing']['tags_list_id']=2 ; // код списка тегов в списках сайта
	$_SESSION['init_options']['landing']['pages']=array() ;
    $_SESSION['init_options']['landing']['tree']['include_space_section']=1 ;
}


function _landing_site_boot($options)
{
	//create_system_modul_obj('landing',array('debug'=>'show_model','tree'=>array('debug'=>0))) ;  // создание объекта landing_system
	create_system_modul_obj('landing',$options) ;  // создание объекта landing_system
}

class c_landing_system  extends c_system_catalog
{ var $max_annot_length ;
  var $table_values ;
  var $table_parents ;
  var $tags_list_id ;


  function	c_landing_system($create_options=array())
  { if (!$create_options['patch_mode']) 			$create_options['patch_mode']='TREE_NAME' ;
    if (!$create_options['table_name']) 			$create_options['table_name']=$_SESSION['TM_landing'] ;
    if (!$create_options['usl_show_items']) 		$create_options['usl_show_items']='clss in (7,3) and enabled=1' ;
    if (!$create_options['root_dir'])				$create_options['root_dir']='landing' ;
    if (!$create_options['order_by'])	            $create_options['order_by']='indx' ;
    parent::c_system_catalog($create_options) ;
    $this->max_annot_length=($create_options['max_annot_length'])? $create_options['max_annot_length']:450 ;
    if (_DOT($this->table_name.'_values')!=null)    $this->table_values=$this->table_name.'_values' ;
    if (_DOT($this->table_name.'_parents')!=null)   $this->table_parents=$this->table_name.'_parents' ;
    if ($create_options['tags_list_id'])	        $this->tags_list_id=$create_options['tags_list_id'];
  }

  // готовим информацию по статьи
  // 6.10.2010 - переделан алгоритм формирования аннотации
  // если аннотация не прописана, текст аннотации формируется на основе основного текста
  // $options['annot_strip_tags'] - удалить мета теги из текста аннотации
  // $options['annot_count']      - максимальный размер аннотации, генерируемой на основе основного текста

  function prepare_public_info(&$rec,$options=array())
  { // для показа полной аннотации значение $options['annot_count']=0
    $max_annot_length=(isset($options['annot_count']))? $options['annot_count']:$this->max_annot_length ;
    // ВНИМАНИЕ! если устанавливается значение для 'max_annot_length' то опция 'annot_strip_tags' автоматически устанавливается в 1 - чтобы не обрезать закрывающие теги в тексте статьи
    if ($max_annot_length) $options['annot_strip_tags']=1 ;
    //damp_array($rec) ;
    if ($rec['intro'] and !$rec['annot']) $rec['annot']=$rec['intro'] ;
  	$rec['__href']=$this->get_patch_item($rec) ;
    $rec['__name']=$rec['obj_name'] ;
  	$rec['__data']=date("d.m.y",$rec['c_data']) ;
    $arr_data=getdate($rec['c_data']) ;
    $rec['__data_1']=$arr_data['mday'].' '.$_SESSION['list_mon_short_names'][$arr_data['mon']].' '.$arr_data['year'] ;
    $rec['__level']=$this->tree[$rec['parent']]->level+1 ;


    //damp_array($rec) ;
    /*
    if (!$rec['value'])
    { $files=execSQL_van('select * from obj_site_landingikle_files where parent='.$rec['pkey']) ;
      if ($files['pkey']) $rec['__href']='/public/landing/files/'.$files['file_name'] ;
    }*/
    // если к статье не подцелены фото, извекаем фото из текста статьи
    if (!$rec['_img_name']) $rec['_img_url']=get_first_img_of_content($rec['value']) ;

    // если не заполнен текст статьи, берем его из аннотации
    if (!trim($rec['value'])) $rec['value']=$rec['annot'];
    if ($rec['value'] or $rec['annot'])
      {   $annot=ltrim($rec['annot'],'&nbsp;') ;
          $annot=($options['annot_strip_tags'])? strip_tags($annot):$annot ;
          $value=ltrim($rec['value'],'&nbsp;') ;
          if ($annot) $rec['__annot']=$annot ;
          else { if (!$options['no_use_value_to_annot']) // 19.11.2010 - добавлена опция запрета использования текста новости для аннатации
                 { $rec['__annot']=($options['annot_strip_tags'])? strip_tags($value):$value ;
        // если указана максимальная длина аннотации, сокращаем анотации до необходимой длины
                   if ($max_annot_length and strlen($rec['__annot'])>$max_annot_length) $rec['__annot']=mb_substr($rec['__annot'],0,$max_annot_length).' ...' ;

                 }
               }

        if (is_object($_SESSION['makros_system'])) $_SESSION['makros_system']->exec($value,$rec) ;
        $rec['__value']=$value ;
    }
  }

  function show_blocks()
  {
    $this->recs=select_db_recs('obj_site_landing','enabled=1 and parent>0',array('order_by'=>'pkey')) ;
    //damp_array($this->recs)  ;
    $data=array() ; $recs=array() ;
    if (sizeof($this->recs)) foreach($this->recs as $i=>$rec)
    {  $section_code=$this->tree[$rec['parent']]->rec['code'] ;
       if ($rec['clss']==71) { $data[$section_code][$rec['code']]=$rec['value'] ; $recs[$section_code][$rec['code']]=$rec ; }
       if ($rec['clss']==70) { $data[$section_code][$rec['code']]=$rec['pkey'] ;  $recs[$section_code][$rec['code']]=$rec ; }
    }

    if (sizeof($this->tree['root']->list_obj)) foreach($this->tree['root']->list_obj as $section_id)
      { $template_code=$this->tree[$section_id]->rec['code'] ;
        if (method_exists(_CUR_PAGE(),'block_'.$template_code))
        {
            _CUR_PAGE()->{'block_'.$template_code}($data[$template_code],$recs[$template_code]) ;

        }
      }

  }

  function get_array_search_usl($text)
  { $_usl=array() ;
    $_usl[]=$this->prepare_search_usl('obj_name','like%%',$text) ;
    $_usl[]=$this->prepare_search_usl('value','like%%',$text) ;
    $_usl[]=$this->prepare_search_usl('annot','like%%',$text) ;
    $_usl[]=$this->prepare_search_usl('pkey','=',$text) ;
    return($_usl) ;
  }

  // возвращает id статей, содержащих указанные в $str_tags_id теги
  // id отсортированы в обратном порядке по pkey
  // $str_tags_id - коды тегов, через запятую: 1,15,22,17
  // $options['count'] - ограничение числа выдаваемых статей
  function get_landing_by_tags($str_tags_id,$options=array())
  {  //
     $sql='select distinct(parent) as id from '.$this->table_values.' where list_id='.$this->tags_list_id.' and value_id in ('.$str_tags_id.') order by id desc' ;
     if ($options['count']) $sql.=' limit '.$options['count'] ;
     $arr=execSQL_line($sql) ;
     return($arr) ;
  }
}


?>