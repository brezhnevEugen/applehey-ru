<?php
$__functions['init'][]		='_landing_admin_vars' ;
$__functions['install'][]	='_landing_install_modul' ;
$__functions['boot_admin'][]='_landing_admin_boot' ;


function _landing_admin_vars() //
{
    // 70 - ШАБЛОН
	$_SESSION['descr_clss'][70]['name']='Раздел страницы' ;
	$_SESSION['descr_clss'][70]['parent']=0 ;
    $_SESSION['descr_clss'][70]['fields']['code']='varchar(32)' ;
    $_SESSION['descr_clss'][70]['menu'][]=array("name" => "Добавить", "type" => 'structure') ;
    $_SESSION['descr_clss'][70]['menu'][]=array("name" => "Свойства", "type" => 'prop') ;
    $_SESSION['descr_clss'][70]['icons']=_PATH_TO_BASED_CLSS_IMG.'/1.png' ;

	$_SESSION['descr_clss'][70]['list']['field']['pkey']				='Код' ;
	$_SESSION['descr_clss'][70]['list']['field']['enabled']			='Сост.' ;
	$_SESSION['descr_clss'][70]['list']['field']['indx']				='Позиция'  ;
	$_SESSION['descr_clss'][70]['list']['field']['obj_name']			=array('title'=>'Наименование','edit_element'=>'input','class'=>'big','td_class'=>'left') ;
    $_SESSION['descr_clss'][70]['list']['field']['code']				='ШАБЛОН'  ;

	$_SESSION['descr_clss'][70]['details']['field']['pkey']			='Код' ;
	$_SESSION['descr_clss'][70]['details']['field']['enabled']		='Сост.' ;
 	$_SESSION['descr_clss'][70]['details']['field']['parent']		='Род.раздел' ;
	$_SESSION['descr_clss'][70]['details']['field'][]				=array('title'=>'Путь в каталоге','edit_element'=>'clss_patch') ;
	$_SESSION['descr_clss'][70]['details']['field']['c_data']		='Создан' ;
	$_SESSION['descr_clss'][70]['details']['field']['r_data']		='Изменен' ;
	$_SESSION['descr_clss'][70]['details']['field']['indx']			='Позиция'  ;
	$_SESSION['descr_clss'][70]['details']['field']['obj_name']		=array('title'=>'Наименование','edit_element'=>'input','class'=>'big','td_class'=>'left') ;
	$_SESSION['descr_clss'][70]['details']['field']['code']		    =array('title'=>'Код') ;


    // 71 - Информация
    $_SESSION['descr_clss'][71]=array() ;
	$_SESSION['descr_clss'][71]['name']='Информация' ;
	$_SESSION['descr_clss'][71]['parent']=0 ;
	$_SESSION['descr_clss'][71]['fields']=array('value'=>'text','code'=>'varchar(32)','is_HTML'=>'int(1)') ;
	$_SESSION['descr_clss'][71]['parent_to']=array(3) ;

    $_SESSION['descr_clss'][71]['icons']=_PATH_TO_BASED_CLSS_IMG.'/7.png' ;

	$_SESSION['descr_clss'][71]['list']['field']['enabled']					='Сост.' ;
	$_SESSION['descr_clss'][71]['list']['field']['pkey']					='ID' ;
	$_SESSION['descr_clss'][71]['list']['field']['code']					='Код блока<br>в шаблоне' ;
    $_SESSION['descr_clss'][71]['list']['field']['indx']						='Позиция' ;
	$_SESSION['descr_clss'][71]['list']['field']['obj_name']				='Наименование' ;
	$_SESSION['descr_clss'][71]['list']['field']['is_HTML']	                =array('title'=>'HTML')  ;
    $_SESSION['descr_clss'][71]['list']['field']['value']					=array('title'=>'Содержание','td_class'=>'justify','class'=>'big','use_HTML_editor'=>1) ;

	$_SESSION['descr_clss'][71]['details']=$_SESSION['descr_clss'][7]['list'] ;
	$_SESSION['descr_clss'][71]['details']['field']['value']				=array('title'=>'Текст','td_class'=>'justify','class'=>'big','no_generate_element'=>1) ;



	//----------------------------------------------------------------------------------------------------------------------
	// описание пунктов меню админа сайта
	//-----------------------------------------------------------------------------------------------------------------------



}                        

function _landing_admin_boot($options)
{   $options['no_create_model']=1 ;
	create_system_modul_obj('landing',$options) ;
}


function _landing_install_modul($DOT_root)
{   echo '<h2>Инсталируем модуль <strong>Главная страница</strong></h2>' ;

    $file_items=array() ;
    $file_items['editor_landing.php']['include'][]='_DIR_TO_ENGINE."/admin/c_site.php"' ;
    $file_items['editor_landing.php']['options']['system']='"landing_system"' ;
    $file_items['editor_landing.php']['options']['title']='"Лендлинг сайт"' ;
    $file_items['editor_landing.php']['options']['use_class']='"c_editor_obj"' ;
    create_menu_item($file_items) ;

    $pattern=array() ;
	$pattern['table_title']				= 	'Лендинг сайт' ;
	$pattern['use_clss']				= 	'70,71' ;
	$pattern['def_recs'][]				=	array ('parent'=>0,	'clss'=>1,	'enabled'=>1,	'indx'=>0,	'obj_name'=>'Главная страница') ;
   	$pattern['table_name']				= 	$_SESSION['TM_landing'] ;
	$pattern['table_clss']				= 	101 ;
	$pattern['table_parent']			= 	$DOT_root;
   	$id=create_table_by_pattern($pattern) ;

    $pattern=array() ;
	$pattern['table_title']				= 	'Изображения' ;
	$pattern['use_clss']				= 	'3' ;
   	$pattern['table_name']				= 	$_SESSION['TM_landing'].'_image' ;
	$pattern['table_clss']				= 	101 ;
	$pattern['table_parent']			= 	$id;
	$pattern['dir_to_image']			= 	'public/index/';
   	create_table_by_pattern($pattern) ;

    SETUP_get_img('/landing/admin_img_menu/newspaper.jpg',_PATH_TO_ADMIN.'/img/menu/newspaper.jpg') ;
}



?>