<?
$__functions['boot_site'][]	='_goods_of_day_site_boot' ;

// парамерт options будет передан из session_boot($options) <= show_page($options)
function _goods_of_day_site_boot($options=array())
{   $options['debug']=0 ;
	create_system_modul_obj('goods_of_day',$options) ; // создаем подсистему "goods"
}

class c_goods_of_day_system
{
    //помечаем товар дня в каталоге товара
    function checked_goods()
    { $data_now=time() ;
      $cur_day = mktime(0,0,0, date("m",$data_now), date("d",$data_now), date("Y",$data_now)); // время начала дня
      // сбрасываем все флаги у всего товара
      execSQL_update('update obj_site_goods set goods_of_day=0,sales_of_day=0 where clss in (2,200)') ;
      // получаем все товары, которые сегодя "Товар дня"
      $list_recs=execSQL('select id,day,sales from obj_site_goods_out_225 where day='.$cur_day.' and clss=225 and enabled=1',array('no_indx'=>1)) ;
      print_2x_arr($list_recs,array('day'=>'timedata')) ;
      // выставляем флаги у новых товаров дня
      if (sizeof($list_recs)) foreach($list_recs as $rec) execSQL_update('update obj_site_goods set goods_of_day=1,sales_of_day='.$rec['sales'].' where pkey='.$rec['id']) ;
      // ставим в конец очереди товары акция по котопым окончилась и у которых стоит флаг автопродления
      $list_recs=execSQL('select pkey,reffer from obj_site_goods_out_225 where day<'.$cur_day.' and flag=1 and clss=225',array('no_indx'=>1)) ; print_2x_arr($list_recs,array('day'=>'timedata')) ;
      if (sizeof($list_recs)) foreach($list_recs as $rec)
      { list($free_day,$parent_level_1)=$this->get_first_free_day_to_goods($rec['reffer']) ;
        update_rec_in_table('obj_site_goods_out_225',array('day'=>$free_day),'pkey='.$rec['pkey']) ;
        echo 'Товар '.$rec['reffer'].' поставлен в очередь на '.date('d.m.Y',$free_day).'<br>' ;
      }
      // удаляем из очереди товары, которые не были проведены
      execSQL_update('delete from obj_site_goods_out_225 where day<'.$cur_day.' and (flag=0 or flag IS NULL) and clss=225') ;
      _event_reg('Обновление очереди товаров дня','Обновлена позиция в очереди для <strong>'.sizeof($list_recs).'</strong> товаров') ;
    }

    // возвращаем id сегодняшнего товара дня для указанного раздела, если раздел не указан, выбираем его случайным образом
    function get_goods_id_of_day($section_id=0)
    { global $goods_system ;
      if ($section_id) $goods_id=execSQL_value('select pkey from obj_site_goods where parent in ('.$goods_system->tree[$section_id]->get_list_child().') and '.$goods_system->usl_show_items.' and goods_of_day=1 and sales_of_day>0 order by rand() limit 1') ;
      else             $goods_id=execSQL_value('select pkey from obj_site_goods where '.$goods_system->usl_show_items.' and goods_of_day=1 and sales_of_day>0 order by rand() limit 1') ;
      return($goods_id) ;
    }

    // проверяем, вляется ли товар id товаров дня, если да, то возвращаем инфу
    // проверяем по модели - товары дня уже списаны в модель
    function check_goods_of_day(&$rec,&$options=array())
    { if ($rec['goods_of_day'] and $rec['sales_of_day'])
      { $rec['_sales_goods_of_day']=$rec['sales_of_day'] ;
        $rec['sales_type']=3 ; // считать скидку по методе с процентами
        $rec['sales_value']=$rec['sales_of_day']+10 ;
        $rec['price']=$rec['price']+$rec['price']*0.1;
        $rec['__price_used_sales_text']='<strong>Товар дня</strong><br>'.$rec['sales_of_day'].'%' ; // внимание! запись НЕ должна быть перетерта в goods_system->exec_price
        $options['use_sales']=1 ;
      }
    }

    // получить все товары дня для всех подразделов
    // в корень каталога вписать случайный товар
    /*
    function save_all_goods_of_day_if_tree_goods()
    { global $goods_system ;
      $data_now=time() ;
      $cur_day = mktime(0,0,0, date("m",$data_now), date("d",$data_now), date("Y",$data_now)); // время начала дня
      $list_recs=execSQL('select * from obj_site_goods_out_225 where day='.$cur_day.' and clss=225',array('no_indx'=>1)) ;
      if (sizeof($list_recs))
      { foreach($list_recs as $rec) if ($rec['sales'] and isset($goods_system->tree[$rec['parent']])) $goods_system->tree[$rec['parent']]->goods_of_day=array('id'=>$rec['id'],'sales'=>$rec['sales']) ;
        // в кореень каталога прописываем один случайный товар из списка (но можно и сделать потом галку)
        if (!sizeof($goods_system->tree['root']->goods_of_day))
        { $rand_i=rand(0,sizeof($list_recs)-1) ;
          $random_rec=$list_recs[$rand_i] ;
          $goods_system->tree['root']->goods_of_day=array('id'=>$random_rec['id'],'sales'=>$random_rec['sales']) ;
        }
      }
    }
    */

 // получаем первый свободный день в очереди для товара reffer
 function get_first_free_day_to_goods($reffer)
 {    // находим первый свободный день
      $last_day=0 ;  $free_day=0 ;  $cur_day=time() ;
      $cur_day=mktime(0,0,0, date("m",$cur_day), date("d",$cur_day), date("Y",$cur_day)); // время начала прошлого дня
      $prev_day=mktime(0,0,0, date("m",$cur_day), date("d",$cur_day)-1, date("Y",$cur_day)); // время начала прошлого дня
      $parents_ids=get_obj_parents_ids($reffer) ;
      $parent_level_1=$parents_ids[1] ;
      $days=execSQL_line('select day from  obj_site_goods_out_225  where day>='.$cur_day.' and clss=225 and parent='.$parent_level_1.' order by day') ;
      if (sizeof($days)) foreach($days as $cur_day)
      { if (!$last_day) $last_day=$prev_day ;
        //else
        { // добавляем сутки к последнему дню
          $next_day = mktime(0,0,0, date("m",$last_day), date("d",$last_day)+1, date("Y",$last_day)); // время начала текущего дня
          // если пропущен день
          //echo '<hr>' ;
          //echo '$last_day='.$last_day.' = '.date('d.m.Y',$last_day).'<br>' ;
          //echo '$next_day='.$next_day.' = '.date('d.m.Y',$next_day).'<br>' ;
          //echo '$cur_day ='.$cur_day.' = '.date('d.m.Y',$cur_day).'<br>' ;
          if ($next_day<$cur_day) { $free_day=$next_day ; break ; }
          $last_day=$next_day ;
        }
      } else $cur_day=$prev_day ;
      //echo '<hr>' ;
      //echo '$free_day='.$free_day.' = '.date('d.m.Y',$free_day).'<br>' ;

      if (!$free_day and $cur_day) $free_day=mktime(0,0,0, date("m",$cur_day), date("d",$cur_day)+1, date("Y",$cur_day)); // время начала следующего дня
      if (!$free_day) $free_day=time() ;
      return(array($free_day,$parent_level_1)) ;
 }

 // проверяем время в записи по товару дня, и если товар дня активен - обновляем запись по товару
 function update_status_goods_of_day($rec)
 {  $data_now=time() ;
   $t1 = mktime(0,0,0, date("m",$data_now), date("d",$data_now), date("Y",$data_now)); // время начала дня
   $t2 = mktime(23,59,59, date("m",$data_now), date("d",$data_now), date("Y",$data_now)); // время конца дня
   list($_pkey,$_tkey)=explode('.',$rec['reffer']) ;
   $status=($rec['day']>=$t1 and $rec['day']<=$t2 and $rec['enabled']==1)? 1:0 ;

   update_rec_in_table($_tkey,array('goods_of_day'=>$status,'sales_of_day'=>$rec['sales']),'pkey='.$_pkey,array('debug'=>0)) ;
 }

 }

?>