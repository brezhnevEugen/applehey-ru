<?
$__functions['init'][]		='_goods_of_day_admin_vars' ;
$__functions['boot_admin'][]='_goods_of_day_admin_boot' ;

function _goods_of_day_admin_vars() //
{

  //-----------------------------------------------------------------------------------------------------------------------------
  // описание классов модуля
  //-----------------------------------------------------------------------------------------------------------------------------
  global $descr_clss ;
  if (sizeof($_SESSION['descr_clss'][2]))
  { $_SESSION['descr_clss'][2]['fields']['goods_of_day']='int(1)' ;
    $_SESSION['descr_clss'][2]['fields']['sales_of_day']='float(10,2)' ;
    $_SESSION['descr_clss'][2]['menu'][]=array("name" => "Товар для", "function" => 'clss_2_list_goods_of_the_day') ;
  }
  if (sizeof($_SESSION['descr_clss'][200]))
  { $_SESSION['descr_clss'][200]['fields']['goods_of_day']='int(1)' ;
    $_SESSION['descr_clss'][200]['fields']['sales_of_day']='float(10,2)' ;
    $_SESSION['descr_clss'][200]['menu'][]=array("name" => "Товар для", "function" => 'clss_2_list_goods_of_the_day') ;
  }

  //товар дня
  $_SESSION['descr_clss'][225]=array() ;
  $_SESSION['descr_clss'][225]['name']='Товар дня' ;
  $_SESSION['descr_clss'][225]['parent']=0 ;
  $_SESSION['descr_clss'][225]['fields']=array('id'=>'int(11)','sales'=>'float(10,2)','sales_real'=>'float(10,2)','day'=>'timedata','reffer'=>'any_object','flag'=>'int(1)') ;
  $_SESSION['descr_clss'][225]['default']['flag']=1 ;
  $_SESSION['descr_clss'][225]['list_items']['buttons']=array('save','delete') ;
  $_SESSION['descr_clss'][225]['on_change_event']['day']='clss_225_on_change_event_day' ;
  $_SESSION['descr_clss'][225]['on_change_event']['sales']='clss_225_on_change_event_sales' ;
  $_SESSION['descr_clss'][225]['on_change_event']['enabled']='clss_225_on_change_event_enabled' ;
  $_SESSION['descr_clss'][225]['obj_delete']='clss_225_obj_delete' ;
  $_SESSION['descr_clss'][225]['no_show_in_tree']=1 ;

  $_SESSION['descr_clss'][225]['list']['field']['enabled']		='Состояние' ;
  $_SESSION['descr_clss'][225]['list']['field']['day']			=array('title'=>'Дата') ;
  $_SESSION['descr_clss'][225]['list']['field']['reffer']		=array('title'=>'Наименование','class'=>'small','td_class'=>'left') ;
  $_SESSION['descr_clss'][225]['list']['field']['sales']	    =array('title'=>'Скидка') ;
  //$_SESSION['descr_clss'][225]['list']['field']['sales_real']	=array('title'=>'Реальная скидка') ;
  $_SESSION['descr_clss'][225]['list']['field']['flag']	    	=array('title'=>'Повтор') ;
  $_SESSION['descr_clss'][225]['list']['options']=array('no_icon_edit'=>1) ;

  $_SESSION['descr_clss'][225]['details']=$descr_clss[222]['list'] ;

}

function _goods_of_day_admin_boot($options)
{
  create_system_modul_obj('goods_of_day',$options) ;
}


function clss_2_list_goods_of_the_day()
{ global $obj_info ;
  $parents_ids=get_obj_parents_ids($obj_info['_reffer']) ;
  $parent_level_1=$parents_ids[1] ;
  $list_recs=execSQL('select * from obj_site_goods_out_225 where parent='.$parent_level_1.' order by day') ;
  //cmd_show_array_objects(225,$list_recs,array()) ;
  _CLSS(225)->show_list_items($list_recs) ;
  ?><button class=button cmd=add_goods_of_the_day>Добавить текущий товар в первое свободное место в очереди "Товаров дня"</button>
    <button class=button cmd=save_list>Сохранить</button>
    <button class=button cmd=delete_check>Удалить</button>
  <?
}

function add_goods_of_the_day()
{ //echo 'Добавляем текущий товар в очередь товаров дня' ;
  global $obj_info,$goods_of_day_system ;
  list($free_day,$parent_level_1)=$goods_of_day_system->get_first_free_day_to_goods($obj_info['_reffer']) ;
  $rec_goods_of_day=array('clss'=>225,'parent'=>$parent_level_1,'id'=>$obj_info['pkey'],'reffer'=>$obj_info['_reffer'],'day'=>$free_day,'sales'=>5) ;
  adding_rec_to_table('obj_site_goods_out_225',$rec_goods_of_day,array('debug'=>0)) ;
  $goods_of_day_system->update_status_goods_of_day($rec_goods_of_day) ;
  return('ok') ;
}


// изменение даты в товаре дня
function clss_225_on_change_event_day($tkey,$pkey,&$fvalue)
{ global $goods_of_day_system ;
  // проверем - если дата сегодняшсяяя, установить в товаре соотвеитсвущие флаги
  $rec_goods_of_day=select_db_obj_info($tkey,$pkey,array('no_child_obj'=>1)) ;
  $rec_goods_of_day['day']=StrToData($fvalue) ;
  $goods_of_day_system->update_status_goods_of_day($rec_goods_of_day) ;
  return(1) ;
}

// изменение даты в товаре дня
function clss_225_on_change_event_sales($tkey,$pkey,&$fvalue)
{ global $goods_of_day_system ;
  // проверем - если дата сегодняшсяяя, установить в товаре соотвеитсвущие флаги и скидку
  $rec_goods_of_day=select_db_obj_info($tkey,$pkey,array('no_child_obj'=>1)) ;
  $rec_goods_of_day['sales']=$fvalue ;
  $goods_of_day_system->update_status_goods_of_day($rec_goods_of_day) ;
  return(1) ;
}

// изменение даты в товаре дня
function clss_225_on_change_event_enabled($tkey,$pkey,&$fvalue)
{ global $goods_of_day_system ;
  // проверем - если дата сегодняшсяяя, установить в товаре соотвеитсвущие флаги и скидку
  $rec_goods_of_day=select_db_obj_info($tkey,$pkey,array('no_child_obj'=>1)) ;
  $rec_goods_of_day['enabled']=$fvalue ;
  $goods_of_day_system->update_status_goods_of_day($rec_goods_of_day) ;
  return(1) ;
}

function clss_225_obj_delete($tkey,$pkeys,$debug=0)
{ $list_reffers=execSQL_line('select reffer from obj_site_goods_out_225 where pkey in ('.$pkeys.')') ;
  $list_reffers=array_unique($list_reffers) ;
  if (sizeof($list_reffers))
  { foreach($list_reffers as $reffer) list($list_id[],$_tkey)=explode('.',$reffer) ;
    $sql='update obj_site_goods set goods_of_day=0,sales_of_day=0 where pkey in ('.implode(',',$list_id).')' ;
    execSQL_update($sql) ;
  }
  clss_0_obj_delete($tkey,$pkeys,$debug) ;
}




?>