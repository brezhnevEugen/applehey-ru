<?php

$__functions['init'][]='_mailer_site_vars' ;
$__functions['boot_site'][]='_mailer_site_boot' ;


function _mailer_site_vars() //
{	$_SESSION['TM_mailer']='obj_'.SITE_CODE.'_mailer' ;
    $_SESSION['TM_mailer_zadan']='obj_'.SITE_CODE.'_mailer_zadan' ;
    $_SESSION['list_status_mailer']=array('Остановлена','Рассылается','Разослана') ;

    $_SESSION['init_options']['mailer']=array() ;
    $_SESSION['init_options']['mailer']['debug']=0 ;
    $_SESSION['init_options']['mailer']['usl_show_items']='enabled=1' ;
    $_SESSION['init_options']['mailer']['system_title']='Система рассылки' ;

    if (!$_SESSION['LS_mailer_max_send_cnt'])  $_SESSION['LS_mailer_max_send_cnt']=3 ;

}


function _mailer_site_boot($options)
{
    create_system_modul_obj('mailer',$options) ; // создание объекта mailer
}

class c_mailer_system
{
  function c_mailer_system()
  {  global $site_TM_kode,$pkey_by_table ;
     $this->table_mailer='obj_'.$site_TM_kode.'_mailer' ;
     $this->table_zadan='obj_'.$site_TM_kode.'_mailer_zadan' ;
     $this->tkey=$pkey_by_table[$this->table_mailer] ;
  }

  // добавляем задание в очередь - это функция будет вызываться mail_system
  function add_mail_to_turn($zadan,$to,$from_email,$theme,$text,$options)
  { global  $events_system ;
    $to=str_replace(array(',',';',' '),'',$to) ;
    $from_email=str_replace(array(',',';',' '),'',$from_email) ;
    $turn_id=adding_rec_to_table_fast($this->table_zadan,array('parent'=>$zadan,'clss'=>34,'indx'=>0,'mail_to'=>$to,'mail_from'=>$from_email,'obj_name'=>$theme,'value'=>$text,'options'=>serialize($options)),array('debug'=>0)) ;
    $comment='От '.$from_email.' к '.$to.' с темой "'.$theme.'"' ;
    $res_text='Поставнока в очередь на отправку письма' ;
    if ($options['use_event_id'])  $events_system->add_event_comment($options['use_event_id'],$res_text.' , id='.$turn_id.','.$comment) ;
    if ($options['reg_events']) $events_system->reg($res_text,$comment) ;
    return('set_to_turn') ;
  }

  function exec_turn()
  {   global $mail_system ;
      if (!$_SESSION['LS_mailer_email_max_count']) $_SESSION['LS_mailer_email_max_count']=20 ;
      if (!$_SESSION['LS_mailer_max_send_cnt'])    $_SESSION['LS_mailer_max_send_cnt']=3 ;
      if ($_GET['debug'])                          $_SESSION['LS_mailer_email_max_count']=1 ;
      $comment='' ;

      $activa_mailers=execSQL('select pkey,obj_name from '.$this->table_mailer.' where enabled=1 and status=1 and clss=33') ;
      if ($_GET['debug']) { echo 'Активные рассылки' ; print_2x_arr($activa_mailers) ; echo '<br>' ; }
      $current_mailer=$activa_mailers[array_rand($activa_mailers)] ;
      if ($_GET['debug']) { echo 'Выбранная рассылка' ; damp_array($current_mailer,1,-1) ; echo '<br>' ; }
      if ($current_mailer['pkey'])
      {  // получаем неотправленные письма, число попыток отправок которых менее 6
         $cnt_mail_to_send=execSQL_value('select count(pkey) from '.$this->table_zadan.' where clss=34 and enabled=1 and indx<'.$_SESSION['LS_mailer_max_send_cnt'].' and parent='.$current_mailer['pkey']) ;
          if ($cnt_mail_to_send)
      {   $cnt_send=0 ; $cnt_non_send=0 ;
              if ($_GET['debug']) echo 'В очереди на отправку <strong>'.$cnt_mail_to_send.'</strong> писем<br>' ;
              for($i=1;$i<=$_SESSION['LS_mailer_email_max_count'];$i++)
              {   $rec=execSQL_van('select * from '.$this->table_zadan.' where clss=34 and enabled=1 and indx<'.$_SESSION['LS_mailer_max_send_cnt'].' and parent='.$current_mailer['pkey'].' order by r_data limit 1') ; //r_data - чтобы письма, которые не получилось отправить, попадали в конец очереди
              if (sizeof($rec))
              { if ($rec['options']) $options=unserialize($rec['options']) ; else $options=array() ;
                //$options['check_only']=1 ;
                $options['debug']=($_GET['debug'])? 1:0  ;
                $options['use_turn']=0 ;
                $res=$mail_system->send_mail($rec['mail_to'],$rec['obj_name'],$rec['value'],$options) ;
                    if ($res=='ok') { update_rec_in_table($this->table_zadan,array('enabled'=>0,'value'=>'','indx'=>$rec['indx']+1,'error'=>0,'send_result'=>'ok'),'pkey='.$rec['pkey']) ; $cnt_send++ ;
                                      execSQL_update('update '.$this->table_mailer.' set posted=posted+1 where pkey='.$current_mailer['pkey']) ;// учеличивам в рассылке число получивших
                                }
                        else        { execSQL_update('update '.$this->table_mailer.' set error=error+1 where pkey='.$current_mailer['pkey']) ;// учеличивам в рассылке число ошибок
                                      update_rec_in_table($this->table_zadan,array('indx'=>$rec['indx']+1,'error'=>1,'send_result'=>strip_tags($res)),'pkey='.$rec['pkey']) ;  // обновиться r_data, письмо встанет в конец очереди
                                      $cnt_non_send++ ;
                                      $comment.=$rec['mail_to'].' :<span class=red>'.strip_tags($res).'</span><br>' ;
                                    }
              }
          }
              $mailer_info=$this->get_mailer_stats($current_mailer['pkey']) ;
              update_rec_in_table($this->table_mailer,array('posted'=>$mailer_info[0],'error'=>$mailer_info[2]),'pkey='.$current_mailer['pkey']) ;
          $event_text='<strong>'.$cnt_mail_to_send.'</strong> => <strong>'.$mailer_info[1].'</strong> ('.$cnt_send.'/'.$cnt_non_send.')' ;
              if ($comment) $event_text.='<br>'.$comment ;
              _event_reg('Запуск крона - отправка почты',$event_text,$current_mailer['_reffer']) ;
      } else echo 'Очередь рассылки пуста' ;

      // проверяем число писем, которые осталось отправить, переводим в статус "Разослано" если были отправлены все письма
      $mailer_info=$this->get_mailer_stats($current_mailer['pkey']) ;
          //ob_start() ; print_r($mailer_info) ; $text=ob_get_clean()  ; _event_reg('Статус рассылк',$text,$current_mailer['_reffer']) ;

      if ($_GET['debug']) { echo 'Текущая рассылка:' ; damp_array($mailer_info,1,-1) ; echo '<br>' ; }
      if (!$mailer_info[1])
      { _event_reg('Окончание рассылки','Отправлено <strong>'.$mailer_info[0].'</strong> писем, не удалось отправить - <strong>'.$mailer_info[2].'</strong>',$current_mailer['_reffer']) ;
            update_rec_in_table($this->table_mailer,array('status'=>2,'posted'=>$mailer_info[0],'error'=>$mailer_info[2]),'pkey='.$current_mailer['pkey']) ;
        //execSQL_update('delete from '.$this->table_zadan.' where parent='.$current_mailer['pkey']) ; // очищаем очередь рассылки - эта информация уже избыточна
      }
      }
  }

  // оправляем рассылку id на email
  function send($id,$email)
  { global $mail_system ;
    $obj_info=select_db_obj_info($this->table_mailer,$id) ;
    $html='<html><head></head><body><style type="text/css">'.file_get_contents(_DIR_TO_ROOT.'/style_emails.css').'</style>'.$obj_info['value'].'</body></html>' ;
    //$html='<html><head></head><body><style type="text/css">'.file_get_contents(_DIR_TO_ROOT.'/style_emails.css').'</style>'.$obj_info['value'].'</body></html>' ;
    //$html='<html><head></head><body>'.$html.'</body></html>' ;
    //$html=$obj_info['value'] ;
    $mail_system->send_mail($email,$obj_info['obj_name'],$html.$this->add_mail_bottom($email),array('debug'=>1)) ;
    //$mail_system->send_mail($email,$obj_info['obj_name'],$html,array('debug'=>1)) ;
    //$mail_system->send_mail($email,$obj_info['obj_name'],$html.'<!---[RL 55KIE7F]-->',array('debug'=>1)) ;
  }

  function start($id)
  {  global $events_system ;
     update_rec_in_table($this->table_mailer,array('status'=>1),'pkey='.$id) ;
     $stats=$this->get_mailer_stats($id) ;
     if (!$stats[1]) $stats[1]='нет' ;
     $events_system->reg('Запуск рассылки','в очереди <strong>'.$stats[1].'</strong> писем',$id.'.'.$this->tkey) ;
     return($stats[1]) ;
  }

  function stop($id)
  {  global $events_system ;
     $res=update_rec_in_table($this->table_mailer,array('status'=>0),'pkey='.$id.' and status!=0') ;
     if ($res['update']) $events_system->reg('Остановка рассылки','',$id.'.'.$this->tkey) ;
     //$stats=$this->get_mailer_stats($id) ;
     //return($stats[1]) ;
  }

  function reload($id)
  {  global $events_system ;
     $obj_info=select_db_obj_info($this->table_mailer,$id) ;
     //$list_emails=$this->get_list_emails($id,array('only_email'=>1)) ;
     $list_emails=$this->get_list_emails($id,array('status'=>1,'only_email'=>1)) ;
     if (sizeof($list_emails))
        { $cnt=$this->delete_email_from_mailer($id,implode(',',$list_emails)) ;
          echo '<div class="green">Удалено из очереди <strong>'.$cnt.'</strong> писем.</div>' ;
          $this->add_email_to_mailer($obj_info,'',implode(',',array_keys($list_emails))) ;
          echo '<div class="green">Поставлено в очередь <strong>'.$cnt.'</strong> писем.</div>' ;
        }
      $events_system->reg('Пересоздание неотправленной','Заново в очередь <strong>'.$cnt.'</strong> писем.',$obj_info['_reffer']) ;
  }

  function reload_full($id)
  {  global $events_system ;
     $obj_info=select_db_obj_info($this->table_mailer,$id) ;
     $list_emails=$this->get_list_emails($id,array('only_email'=>1)) ;
     //print_r($list_emails) ;
     if (sizeof($list_emails))
        { $cnt=$this->delete_email_from_mailer($id,implode(',',$list_emails)) ;
          echo '<div class="green">Удалено из очереди <strong>'.$cnt.'</strong> писем.</div>' ;
          $this->add_email_to_mailer($obj_info,'',implode(',',array_keys($list_emails))) ;
          echo '<div class="green">Поставлено в очередь <strong>'.$cnt.'</strong> писем.</div>' ;
        }
      $events_system->reg('Пересоздание рассылки','Заново в очередь <strong>'.$cnt.'</strong> писем.',$obj_info['_reffer']) ;
  }

  // возвращает статистикe по письмам рассылке
  // если указан код рассылки, возвращаем массив, arr[0] - число отправленных, arr[1] - число неотправленных
  // если код рассылки не указан, возвразаем массив, arr[id] - число неотправленных по каждой рассылке отдельно
  function get_mailer_stats($pkey)
  { $stats[0]=execSQL_value('select count(pkey) as cnt from '.$this->table_zadan.' where parent="'.$pkey.'" and enabled=0') ; //  все письма в статусе "отправлено"
    $stats[1]=execSQL_value('select count(pkey) as cnt from '.$this->table_zadan.' where parent="'.$pkey.'" and enabled=1 and indx<'.$_SESSION['LS_mailer_max_send_cnt']) ; // все неотправленные письма
    $stats[2]=execSQL_value('select count(pkey) as cnt from '.$this->table_zadan.' where parent="'.$pkey.'" and error=1') ; // все письма с ошибокой отправки
    return($stats) ;
               }

  // возвращает статистикe по письмам рассылке
  // если указан код рассылки, возвращаем массив, arr[0] - число отправленных, arr[1] - число неотправленных
  // если код рассылки не указан, возвразаем массив, arr[id] - число неотправленных по каждой рассылке отдельно
  function get_all_mailer_stats()
  { // получаем число неотправленных писем в рассылках, находящихся в статусе "рассылается"
    $stats=execSQL_row('select parent,count(pkey) as cnt from '.$this->table_zadan.' where enabled=1 and (parent in (select pkey from '.$this->table_mailer.' where enabled=1 and status=1 and clss=33)) group by parent') ;
    return($stats) ;
  }

  // возвращаем статистику по состояниям рассылок
  // возвращаем массив,
  // arr[0] - остановлена
  // arr[1] - рассылается
  // arr[2] - разослана
  function get_mailer_count($usl_select='')
  {
    $stats=execSQL_row('select status,count(pkey) as cnt from '.$this->table_mailer.' where clss=33 '.$usl_select.' group by status',0,0) ;
    return($stats) ;
  }

  function get_count_by_status($usl_select='')
    { $sql='select status,count(pkey) as cnt from '.$this->table_mailer.' where clss=33 ' ;
      if ($usl_select) $sql.=' and '.$usl_select ;
      $sql.=' group by status' ;
      $cnt_arr=execSQL_row($sql) ;
      return($cnt_arr) ;
    }


  // удаляем все неотправленные адресаты из рассылки или только указанные
  function delete_email_from_mailer($pkey,$mailer_ids='')
  {
    if ($mailer_ids) $cnt=execSQL_update('delete from '.$this->table_zadan.' where clss=34 and parent='.$pkey.' and pkey in ('.$mailer_ids.')') ;
    else             $cnt=execSQL_update('delete from '.$this->table_zadan.' where clss=34 and parent='.$pkey.' ') ;
    $this->update_mailer_stats($pkey) ;
    return($cnt) ;
  }

  // удаляем флаг что письмо не удалось отправить
  function clear_error_flag($pkey,$mailer_ids='')
  {
    if ($mailer_ids) $cnt=execSQL_update('update '.$this->table_zadan.' set indx=0, enabled=1, error=0, send_result="" where clss=34 and parent='.$pkey.' and error=1 and pkey in ('.$mailer_ids.')') ;
    else             $cnt=execSQL_update('update '.$this->table_zadan.' set indx=0, enabled=1, error=0, send_result="" where clss=34 and parent='.$pkey.' and error=1') ;
    $this->update_mailer_stats($pkey) ;
    return($cnt) ;
  }

  // сбрасываем флаг отправки у всех писем в очереди
  function restock_email_from_mailer($pkey,$mailer_ids='')
  {
    if ($mailer_ids) $cnt=execSQL_update('update '.$this->table_zadan.' set enabled=1,r_data=NULL where clss=34 and parent='.$pkey.' and pkey in ('.$mailer_ids.')',array('debug'=>0)) ;
    else             $cnt=execSQL_update('update '.$this->table_zadan.' set enabled=1,r_data=NULL where clss=34 and parent='.$pkey.' ',array('debug'=>0)) ;
    $this->update_mailer_stats($pkey) ;
    return($cnt) ;
  }

  // возвращает всех адресатов из очереди, сортировка по коду
  function get_list_emails($pkey,$options=array())
  { $usl_status=(isset($options['status']))? ' and enabled="'.$options['status'].'"':'' ;
    if ($options['only_email'])
    { $list_emails=execSQL_row('select pkey as id,mail_to from '.$this->table_zadan.' where parent='.$pkey.$usl_status.' order by pkey',$options) ;
      if (sizeof($list_emails)) foreach($list_emails as $id=>$email) $list_emails[$id]=trim($email) ;
      $list_emails=array_flip($list_emails) ;
    }
    else  $list_emails=execSQL('select * from '.$this->table_zadan.' where parent='.$pkey.$usl_status.' order by pkey',$options) ;

    return($list_emails) ;
  }

  // различные варианты получения email для подписки. Эта функция должна уточняться для каждого сайта в mc_ext_moduls.php
  function show_submitter_variants()
  {   global $TM_account,$orders_system,$account_system ;
      if (is_object($orders_system))
      { $cnt_emails=$orders_system->get_subscript_count() ;
        ?><input type="checkbox" name="source_emails[]" value='orders_system'>Добавить подписчиков из системы заказов [ <strong><?echo $cnt_emails?> получателей</strong> ]<br><?
      }
      if ($TM_account)
      { $cnt_emails=$account_system->get_subscript_count() ;
        //$cnt_emails=execSQL_value('select count(pkey) from '.$TM_account.' where email!="" and subscript=1 and parent!=5') ;
        ?><input type="checkbox" name="source_emails[]" value='account_system'>Добавить подписчиков из системы аккаунтов [ <strong><?echo $cnt_emails?> получателей</strong> ]<br><?
      }
  }

  function add_mail_bottom($email)
  {   global $main_domain ;
      $hash=md5($email) ;
      $html='<br><br>Это сообщение было отправлено службой рассылки сайта '.$main_domain.'<br>Вы можете отказаться от рассылки, для этого воспользуйтесь ссылкой: <a href="http://'._MAIN_DOMAIN.'/mailer/unsubscribed.php?id='.$hash.'">Отказатьcя от рассылки</a>' ;
      return($html) ;
  }

  function unsubscribed($hash)
  {  global $account_system,$orders_system ;
     $_SESSION['account_system']->unsubscribed($hash) ;
     $_SESSION['orders_system']->unsubscribed($hash) ;
     return(1) ;
     //echo 'Описываем от рассылки :'.$hash.'<br>' ;
  }


  // оправка письма в очередь рассылкки через mail_system
  function add_email_to_mailer($obj_info,$source_emails=array(),$list_emails="")
    { global $mail_system,$dir_to_root,$events_system ;
      $_list_sender=array() ; $list_sender=array() ;

      if (sizeof($source_emails)) foreach($source_emails as $system_name) if (is_object($_SESSION[$system_name]) and method_exists($_SESSION[$system_name],'get_subscript_emails')) $_list_sender[$system_name]=$_SESSION[$system_name]->get_subscript_emails() ;

      if (sizeof($_list_sender)) foreach($_list_sender as $arr) $list_sender=array_merge($list_sender,$arr) ;

      // добавляем в массив адреса из поля
      if ($list_emails)
      { $send_to_mail_emails=str_replace(array(',',';',"\n"),' ',$list_emails) ;
        //$send_to_mail_emails=str_replace(';',' ',$send_to_mail_emails) ;
        $arr=explode(' ',$send_to_mail_emails) ;
        if (sizeof($arr)) foreach($arr as $email)
        {  $email=trim($email) ;
           if ($email) $list_sender[]=$email ;
        }
      }

      echo 'Обрабатываем <strong>'.sizeof($list_sender).'</strong> записи по аккаунту<br>' ;

      $list_sender2=array() ;
      // чистим email от указания в одной строке нескольких email
        if (sizeof($list_sender)) foreach($list_sender as $i=>$email)
        { $arr=explode(',',$email) ; // print_r($arr) ; echo '<br>' ;
          if (sizeof($arr)==1)
          { $arr=explode(';',$email) ; //print_r($arr) ; echo '<br>' ;
            if (sizeof($arr)==1) $list_sender2[]=$email ;
            else                 $list_sender2=array_merge($list_sender2,$arr) ;
          } else                 $list_sender2=array_merge($list_sender2,$arr) ;
        }
      unset($list_sender) ; $list_sender=array() ;

      // чистим email
      if (sizeof($list_sender2)) foreach($list_sender2 as $email) if ($email)
      {  $clean=$this->clean_email($email) ;
         if ($clean) $list_sender[]=$clean ;
      }

      // удаляем из списка повторы
      $list_sender=array_unique($list_sender) ;
      //damp_array($list_sender2) ;  echo sizeof($list_sender2).'<br>' ;
      //damp_array($list_sender) ;   echo sizeof($list_sender).'<br>' ;

      echo 'Выделено <strong>'.sizeof($list_sender).'</strong> уникальных email<br>' ;

      // проверяем уже находящиеся в очереди email - повторно в очередь их не ставим
      $list_exist_emails=$this->get_list_emails($obj_info['pkey'],array('only_email'=>1)) ;
      //damp_array($list_exist_emails) ;
      $list_to_send=array() ; $cnt_exist=0 ;
      if (sizeof($list_sender)) foreach($list_sender as $email)
        if (!isset($list_exist_emails[$email])) $list_to_send[]=$email ;  // если адресат уже в очереди - больше его email в очередь не ставим
        else $cnt_exist++ ;

      if ($cnt_exist) echo 'В списке email обнаружено <strong>'.$cnt_exist.'</strong> email, которые уже присутствуют в рассылке<br>' ;

      $html='<style type="text/css">'.file_get_contents(_DIR_TO_ROOT.'/style_emails.css').'</style>'.$obj_info['value'] ;

      $cnt=0 ;
      global $IL_vyiberi_menedzhera,$account_system  ;
      if (sizeof($list_to_send)) foreach($list_to_send as $email) if ($email)
           { $options=array('debug'=>0,'use_turn'=>$obj_info['pkey']) ;
             //$account_info=$account_system->get_account_info_by_email($email) ;
             //if ($account_info['id_man']) $options['from_email']=$IL_vyiberi_menedzhera[$account_info['id_man']]['email'] ;
             $mail_system->send_mail($email,$obj_info['obj_name'],$html.$this->add_mail_bottom($email),$options) ;
             $cnt++ ;
           }
      $events_system->reg('Добавление email в список рассылки','Поставлено в очередь <strong>'.$cnt.'</strong> писем',$obj_info['_reffer']) ;

      $this->update_mailer_stats($obj_info['pkey']) ;

      return(sizeof($list_to_send)) ;

    }

   function update_mailer_stats($id)
   { $stats=$this->get_mailer_stats($id) ;
     update_rec_in_table($this->table_mailer,array('recipients'=>array_sum($stats),'posted'=>$stats[0],'error'=>$stats[2]),'pkey='.$id) ;
   }

   function clean_email($email)
   { $email=trim($email) ;
     $email=str_replace(array(' ','"',"'"),'',$email) ; // очищаем адрес от мусора
     $email=str_replace(array('.ru.','@.',".@"),array('.ru','@','@'),$email) ; // очищаем адрес от мусора
     if (strpos($email,'@')!==false and strpos($email,'.')!==false) return($email) ;
     else return ('') ;
   }

   function get_mail_info($pkey)
   {  $rec=select_db_obj_info($this->tkey,$pkey,array('debug'=>0)) ;
      return($rec) ;
   }

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// подписка и рассылка - функции долдны быть едины для всех дочерних подсистем
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


function unsubscribed2($hash)
{ if (!sizeof($this->subscriptions)) return ;
  $_table_name=$this->subscriptions['table_name'] ;
  $_email=$this->subscriptions['fnames']['email'] ;
  $_subscript=$this->subscriptions['fnames']['subscript'] ;

  $emails=$this->get_subscript_emails() ;
  if (sizeof($emails)) foreach($emails as $email) if (md5($email)==$hash)
  { update_rec_in_table($_table_name,array($_subscript=>0),$_email.'="'.$email.'"') ;
    _event_reg('Отказ от подписки',$email) ;
  }
}

// получаем все email из всех подсистем, что учавствуют в рассылке
function get_subscript_emails()
{ $_sql=array() ; $emails=array() ;
  if (sizeof($_SESSION['list_created_sybsystems'])) foreach($_SESSION['list_created_sybsystems'] as $system_code) if (sizeof($_SESSION[$system_code]->subscriptions))
  { $_table_name=$_SESSION[$system_code]->subscriptions['table_name'] ;
    $_email=$_SESSION[$system_code]->subscriptions['fnames']['email'] ;
    $_subscript=$_SESSION[$system_code]->subscriptions['fnames']['subscript'] ;
    $_sql[]='select distinct('.$_email.') from '.$_table_name.' where '.$_subscript.'=1 and '.$_email.'!=""' ;
  }
  if (sizeof($_sql)) $emails=execSQL_line(implode(' union ',$_sql)) ;
  return($emails)  ;
}

function get_subscript_count()
{ $emails_cnt=array() ; $_sql=array() ;
  if (sizeof($_SESSION['list_created_sybsystems'])) foreach($_SESSION['list_created_sybsystems'] as $system_code) if (sizeof($_SESSION[$system_code]->subscriptions))
  { $_table_name=$_SESSION[$system_code]->subscriptions['table_name'] ;
    $_email=$_SESSION[$system_code]->subscriptions['fnames']['email'] ;
    $_subscript=$_SESSION[$system_code]->subscriptions['fnames']['subscript'] ;
    $_name=$_SESSION[$system_code]->system_title ;
    $_sql[]='select "'.$_name.'" as system_name,count(distinct('.$_email.')) as cnt from '.$_table_name.' where '.$_subscript.'=1 and '.$_email.'!=""' ;
  }
  if (sizeof($_sql)) $emails_cnt=execSQL_row(implode(' union ',$_sql)) ;
  return($emails_cnt)  ;
}



}


?>