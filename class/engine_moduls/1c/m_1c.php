<?php

$__functions['init'][]		='_1c_site_vars' ;
$__functions['boot_site'][]	='_1c_site_boot' ;

define('TM_SORTAMENT','obj_site_goods_sortament') ;
define('TM_SORTAMENT_STOCK','obj_site_goods_sortament_stock') ;

function _1C() {return($_SESSION['1c_system']);$_SESSION['1c_system']=new c_1c_system();return($_SESSION['1c_system']);} ;

// функции быстрого доступа
function _show_last_1c($func_name,$options=array()) { $_SESSION['1c_system']->show_last_items($func_name,$options) ;}


function _1c_site_vars()
{    // описание системы  -------------------------------------------------------------------------------------------------------------------
	$_SESSION['init_options']['1c']['debug']=0 ;
    $_SESSION['init_options']['1c']['patch_mode']	='TREE_NAME' ;
    $_SESSION['init_options']['1c']['root_dir']	='1c' ;
    $_SESSION['init_options']['1c']['tree']['debug']=0 ;
    $_SESSION['init_options']['1c']['tree']['order_by']	='c_data desc' ;
    $_SESSION['init_options']['1c']['tree']['get_count_by_clss']=0 ;
    $_SESSION['init_options']['1c']['pages']['size']=array('10'=>'По 10 новостей','20'=>'По 20 новостей','all'=>'Все') ;
    $_SESSION['init_options']['1c']['pages']['sort_type']=array() ;
    $_SESSION['init_options']['1c']['pages']['sort_type'][1]=array('name'=>'По дате',	'order_by'=>'c_data desc',	'diapazon_title'=>'Дата:') ;
    $_SESSION['init_options']['1c']['pages']['sort_type'][2]=array('name'=>'По порядку',	'order_by'=>'indx',			'diapazon_title'=>'Позиции:') ;
    $_SESSION['init_options']['1c']['pages']['sort_type'][3]=array('name'=>'По наименованию','order_by'=>'obj_name',	'diapazon_title'=>'Наименования:') ;
}


function _1c_site_boot($options)
{
	create_system_modul_obj('1c',$options) ; // создание объекта 1c_system
}

//-----------------------------------------------------------------------------------------------------------------------------------------------
//
// система новостей
//
//-----------------------------------------------------------------------------------------------------------------------------------------------


class c_1c_system extends c_system
{ public $root_reffer ;
  function	c_1c_system($create_options=array())
  { if (!$create_options['table_name']) 			$create_options['table_name']=TM_SORTAMENT ;
    if (!$create_options['usl_show_items']) 		$create_options['usl_show_items']='clss=65 and enabled=1' ;
    if (!$create_options['root_dir'])				$create_options['root_dir']='1c' ;
    if (!$create_options['order_by'])	            $create_options['order_by']='indx' ;
    parent::c_system($create_options) ;
    $this->root_reffer='1.'.$this->tkey ;
  }

  function get_array_search_usl($text)
  { $_usl=array() ;
    $_usl[]=$this->prepare_search_usl('obj_name','like%%',$text) ;
    $_usl[]=$this->prepare_search_usl('value','like%%',$text) ;
    $_usl[]=$this->prepare_search_usl('annot','like%%',$text) ;
    $_usl[]=$this->prepare_search_usl('pkey','=',$text) ;
    return($_usl) ;
  }

  function get_count($usl_select='')
    { $sql='select count(pkey) as cnt from '.$this->table_name.' where clss=9 ' ;
      if ($usl_select) $sql.=' and '.$usl_select ;
      $cnt=execSQL_value($sql) ;
      return($cnt) ;
    }

  function get_img_to_sortament($sortament_id)
  {

  }

  function get_sortament_rec($sortament_reffer)
  { $rec=get_obj_info($sortament_reffer) ;
    //$this->prepare_public_info($rec) ; // вызывается автоматически в get_obj_info
    return($rec) ;
  }

  function get_sortament_info($rec,$options=array())
  { $arr=array() ; $text='' ;
    if ($rec['color']) $arr[]='Цвет: <strong>'.$_SESSION['IL_colors'][$rec['color']]['obj_name'].'</strong> ' ;
    if ($rec['color_name']) $arr[]='Цвет: <strong>'.$rec['color_name'].'</strong> ' ;
    if ($rec['size']) $arr[]='Размер: <strong>'.$rec['size'].'</strong>' ;
    if ($options['show_code']) $arr[]='Штрих-код: <strong>'.$rec['code'].'</strong>' ;
    if ($options['show_barcode']) $arr[]='<img src="http://'._MAIN_DOMAIN.'/orders/barcode'.$rec['code'].'.png">' ;
    if (sizeof($arr)) $text=implode($options['delimiter'],$arr) ;
    return($text) ;
  }

  function get_first_sortament_to_goods($reffer)
  { list($pkey,$tkey)=explode('.',$reffer) ;
    $rec=execSQL_van('select * from '.$this->table_name.' where goods_id='.$pkey.' order by indx limit 1') ;
    if ($rec['pkey']) $this->prepare_public_info($rec) ;
    return($rec) ;
  }

  function prepare_public_info(&$rec)
  { //if ($rec['img_id'])
     //trace() ;
    if ($rec['color'])
      {   $tkey_goods_image=_DOT(TM_GOODS_IMAGES)->pkey ;
          $img_id=execSQL_value('select pkey from '.TM_GOODS_IMAGES.' where parent='.$rec['parent'].' and color='.$rec['color'].' order by indx limit 1') ;
          //$img_rec=get_obj_info($rec['img_id'].'.'.$tkey_goods_image) ;
          $img_rec=get_obj_info($img_id.'.'.$tkey_goods_image) ;  // damp_array($img_rec) ;
          if ($img_rec['pkey'])
          { $rec['_image_name']=$img_rec['file_name'] ;
            $rec['_image_tkey']=$img_rec['tkey'] ;
          }
      }
  }



}

?>