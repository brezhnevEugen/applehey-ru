<?php
include_once (_DIR_TO_ENGINE.'/c_page_XML_AJAX.php') ;
include_once (_DIR_TO_ENGINE.'/admin/i_admin_system.php') ;
include_once ('i_1c.php') ;
class c_page_ajax extends c_page_XML_AJAX
{
   function set_img_to_sortament()
   { list($pkey,$tkey)=explode('.',$_POST['sort_reffer']) ;
     //execSQL_update('update obj_site_1c set img_id=0 where img_id='.$_POST['img_id']) ;
     // получаем название цвета, на который кидается рисунок
     $sort_rec=execSQL_van('select * from '.TM_SORTAMENT.' where pkey='.$pkey)  ;
     // привязыаем рисунок ко всему сортаменту
     execSQL_update('update obj_site_1c set img_id='.$_POST['img_id'].' where color_name="'.$sort_rec['color_name'].'" and goods_id='.$sort_rec['goods_id']) ;

     // теперь надо вернуть эту обновленную строку
     include_once('i_1c.php') ;
     ob_start() ;
     $rec_sort=get_obj_info($_POST['sort_reffer']) ; // получем запись сортамента
     show_sortament_image($rec_sort) ;
     ?><script type="text/javascript">$j(document).ready(function(){$j('div.item.grag_img').draggable({revert:'invalid',scope:'test'});}) ;</script><?

     $text=ob_get_clean()  ;
     $this->add_element('sort_img_'.$pkey,$text)  ;
   }

   function clear_img_to_sortament()
   { execSQL_update('update obj_site_1c set img_id=0 where img_id='.$_POST['img_id']) ;
     echo '   ' ;
     // теперь надо вернуть эту обновленную строку

   }


   function get_list_orders($doc,$xml)
   { $options=array() ; $usl='' ; $arr_fields=array() ;  $_usl_filter=array() ;
     $title='Заказы ' ;
     if ($_POST['search_text'] and $_POST['target'])
     {   $arr_words=explode(' ',trim($_POST['search_text'])) ; // разбираем на пробелы
         switch($_POST['target'])
          { case 'number': $arr_fields=array('obj_name') ; $title.=' - номер или дата содержит "'.$_POST['search_text'].'"' ; break ;
            case 'client': $arr_fields=array('name','phone','email') ; $title.=' - ФИО, телефон, или почта содержит "'.$_POST['search_text'].'"' ; break ;
            case 'adres':  $arr_fields=array('adres') ; $title.=' - адрес содержит "'.$_POST['search_text'].'"' ; break ;
            case 'rekv':   $arr_fields=array('rekvezit') ; $title.=' - реквизиты содержат "'.$_POST['search_text'].'"' ;  break ;
            case 'goods':  //$arr_fields=array('name','phone','email') ; break ;
          }
         if (sizeof($arr_words)) foreach($arr_words as $word)
         { $_usl2=array() ;
           if (sizeof($arr_fields)) foreach($arr_fields as $fname) $_usl2[]="UCASE(".$fname.") rlike UCASE('[[:<:]]".mb_strtoupper($word)."')" ;
           if (sizeof($_usl2)) $_usl_filter[]='('.implode(' or ',$_usl2).')' ;
         }


     }
     if ($_POST['usl'])                  $usl=stripslashes($_POST['usl']) ;
     if ($_POST['show_van_rec_as_item']) $options['show_van_rec_as_item']=$_POST['show_van_rec_as_item'] ;
     if (sizeof($_usl_filter))           $options['usl_filter']=implode(' and ',$_usl_filter) ;
     $options['default_order']='c_data desc' ;
     $options['title']=$title ;
     // получаем и показывем строки
     $pages_info=_CLSS(82)->show_list_items($_SESSION['orders_system']->tkey,$usl,$options) ;
      // выводим информацию по отображенным строкам
      add_element($doc,$xml,'list_id',$_POST['list_id']) ;
      add_element($doc,$xml,'from_pos',$pages_info['from']) ;
      add_element($doc,$xml,'to_pos',$pages_info['to']) ;
      add_element($doc,$xml,'all_pos',$pages_info['all']) ;
      add_element($doc,$xml,'next_page',$pages_info['next_page']) ;
      add_element($doc,$xml,'info_count_text','1...'.$pages_info['to'].' из '.$pages_info['all'])  ;
   }
}
?>