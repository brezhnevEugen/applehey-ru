<?
 define('_ID_PARENT_NEW_GOODS',3936) ;
 define('_ADDING_NEW_GOODS',1) ;
 define('_ADDING_NEW_SORTAMENT',1) ;
 define('_UPDATE_EXIST_GOODS',0) ;
 define('_UPDATE_EXIST_SORTAMENT',1) ;

 function unzip_arhive($arh_name,$desct_dir)
 {
     $zip = new ZipArchive;
      if ($zip->open($arh_name) === TRUE)
      {   $zip->extractTo($desct_dir);
          $zip->close();
          echo 'Архив '.basename($arh_name).' успешно распакован<br>';
      }
      else { echo 'Ошибка извлечения архива '.basename($arh_name).'<br>'; return ; }

 }



/*=====================================================================================================================================================================================================================================
 *
 * ГРУППИРУЕМ ТОВАР с одинаковыми артикулами
 *
 *=====================================================================================================================================================================================================================================*/


function replace_colors($table_name)
{ $arr_colors=array(   'ТЕМНОСИНИЙ'=>'темно-синий',
                        "бел"=>"белый",
                        "бел."=>"белый",
                        "беж"=>"бежевый",
                        "беж."=>"бежевый",
                        "красн"=>"красный",
                        "красн."=>"красный",
                        "роз"=>"розовый",
                        "син"=>"синий",
                        "без покр"=>"без покраски",
                        "без покрас"=>"без покраски",
                        "гол"=>"голубой",
                        "желт"=>"желтый",
                        "зел"=>"зеленый",
                        "зол"=>"золотой",
                        "красная"=>"красный",
                        "малин"=>"малиновый",
                        "оранж"=>"оранжевый",
                        "розовое"=>"розовый",
                        "розовый."=>"розовый",
                        "сер"=>"серебро",
                        "серебр"=>"серебро",
                        "фиолет"=>"фиолетовый",
                        "тем-"=>"тёмно-",
                        "св-"=>"светло-",
                        "чер"=>"черный"
   ) ;
   if (sizeof($arr_colors)) foreach($arr_colors as $from=>$to) execSQL_update('update '.$table_name.' set color_name=REPLACE(color_name,"'.$from.'","'.$to.'")') ;
}


/*=====================================================================================================================================================================================================================================
 *
 * СОЕДИНЯЕМ ПОЗИЦИИ из 1С с товарами сайта - по артикулу.
 *
 *=====================================================================================================================================================================================================================================*/


function upload_1c_XML_offer()
 {
    $import_xml_file=_DIR_TO_ROOT.'/1c/1cobmen/work/import.xml' ;
    if (file_exists($import_xml_file))
    {
      // импорт новых товаров
      link_1c_goods_to_site($import_xml_file) ;

    }
    else {?><div class="alert">Не удалось найти распакованный файл выгрузки /1c/1cobmen/work/<?echo basename($import_xml_file)?></div><?}
 }



function link_1c_goods_to_site($import_xml_file)
{ //ob_start() ;


  $xml=simplexml_load_file($import_xml_file) ;
  ?><h2>Импорт описаний товаров</h2><?

  $arr_attr=$xml->Каталог->attributes() ;
  $mode=($arr_attr['СодержитТолькоИзменения']=='true')? 1:0 ; // 1 - 'Только измемения' ; 0 -  'Все данные' ;
  if ($mode) echo '<div class="green bold">Выгрузка содержит только изменения</div>' ; else echo '<div class="green bold">Выгрузка содержит  все данные</div>' ;
  echo '<br>' ;


  import_sortament_from_XML($xml->Каталог->Товары,$mode) ;
  //$text=ob_get_flush()  ;
  //_event_reg('Импорт из 1с',$text) ;

}

 function import_sortament_from_XML(&$xml,$mode)
 {   $sort_exist=0 ; $sort_adding=0 ; $cnt_no_art=0 ; $cnt_no_code=0 ;
     $arr_sortament_exist=execSQL_row('select code,pkey from '.TM_SORTAMENT.' where clss=65') ; // все сортаменты должны быть с штрихкодом, без вариантов
     $cnt_sortament_exist_before=execSQL_value('select count(pkey) from '.TM_SORTAMENT.' where clss=65') ;
     $cnt_unique_art_before=execSQL_value('select count(distinct(art)) from '.TM_SORTAMENT.' where clss=65') ;
     $cnt_sortament_enabled_defore=execSQL_row('select enabled,count(pkey) from '.TM_SORTAMENT.' where clss=65 group by enabled') ; // все сортаменты должны быть с штрихкодом, без вариантов



     echo 'В выгрузке 1С <strong>'.sizeof($xml->Товар).'</strong> позиций с описаниями товаров<br><br>' ;
     if (!$mode) execSQL_update('update '.TM_SORTAMENT.' set temp=0 where clss=65') ;
     foreach ($xml->Товар as $goods) // проходим по списку сортаментов, группируем их в товары
     { $arr_attr=$goods->attributes() ;
       //print_r($arr_attr) ; echo '<br>' ;
       if ($arr_attr['Статус']!='Удален')
         { $rec_sortament=array();
           $art=(string)$goods->Артикул ;
           $name=(string)$goods->Наименование ;
           if ($art)
           {   // формируем запись для сортамента товара. Пока ничего не вычислям, просто переносим все как есть из XML
               $rec_sortament['art']=(string)$goods->Артикул ;      // один артикул на весь сортамент одного товара
               $rec_sortament['code']=(string)$goods->Штрихкод ;      // отдельный штрих-код для каждого сортамента
               $rec_sortament['1c_id']=(string)$goods->Ид ;      // отдельный код 1С для каждого сортамента
               $rec_sortament['obj_name']=(string)$goods->Наименование ;      // отдельный код для каждого сортамента
               if ($rec_sortament['code'])
                 { $rec_sortament['clss']=65 ;
                   $rec_sortament['temp']=1 ;
                   // если записи по сортаменту еще нет в базе - добавляем
                   if (isset($arr_sortament_exist[$rec_sortament['code']]))
                   { $sort_id=$arr_sortament_exist[$rec_sortament['code']] ;
                     if (_UPDATE_EXIST_SORTAMENT) update_rec_in_table(TM_SORTAMENT,$rec_sortament,'pkey='.$sort_id) ;    // либо обновляем все данные сортамента
                     else                         update_rec_in_table(TM_SORTAMENT,array('temp'=>1),'pkey='.$sort_id) ;  // либо только ставим флаг что сортамент есть
                     $sort_exist++ ;
                     echo '<div class="gray">Сортамент <strong>'.$name.'</strong> ['.(string)$goods->Штрихкод.'] - обновлена.</div>' ;
                   }
                   else if (_ADDING_NEW_SORTAMENT) { adding_rec_to_table(TM_SORTAMENT,$rec_sortament) ; $sort_adding++ ;
                                                     echo '<div class="green">Сортамент <strong>'.$name.'</strong> ['.(string)$goods->Штрихкод.'] - Добавлена.</div>' ;
                                                   }
                 }
                 else
                 {
                     echo '<div class="red">Сортамент <strong>'.$name.'</strong> ['.(string)$goods->Штрихкод.'] - отсутствует шрих-код, пропущена.</div>' ;
                     $cnt_no_code++ ;
                 }
           }
           else
           {
             echo '<div class="red">Сортамент <strong>'.$name.'</strong> ['.(string)$goods->Штрихкод.'] - отсутствует артикль, пропущена.</div>' ;
             $cnt_no_art++ ;
           }

         }
     }
     if (!$mode) execSQL_update('update '.TM_SORTAMENT.' set enabled=temp where clss=65') ;
     $cnt_sortament_exist_after=execSQL_value('select count(pkey) from '.TM_SORTAMENT.' where clss=65') ;
     $cnt_sortament_enabled_after=execSQL_row('select enabled,count(pkey) from '.TM_SORTAMENT.' where clss=65 group by enabled') ; // все сортаменты должны быть с штрихкодом, без вариантов
     $cnt_unique_art_after=execSQL_value('select count(distinct(art)) from '.TM_SORTAMENT.' where clss=65') ;

     $table_stats['Всего позиций сортамента']=array($cnt_sortament_exist_before,$cnt_sortament_exist_after) ;
     $table_stats['Включенных позиций сортамента']=array($cnt_sortament_enabled_defore[1],$cnt_sortament_enabled_after[1]) ;
     $table_stats['Отключенных позиций сортамента']=array($cnt_sortament_enabled_defore[0],$cnt_sortament_enabled_after[0]) ;
     $table_stats['Уникальных артикулов']=array($cnt_unique_art_before,$cnt_unique_art_after) ;
     ?><table><tr><th></th><th>До импорта</th><th>После импорта</th><th></th></tr><?
     if (sizeof($table_stats)) foreach($table_stats as $title=>$rec)
        {?><tr><td class="left"><?echo $title?></td><td><?echo $rec[0]?></td><td><?echo $rec[1]?></td><td><?echo (($rec[0]<$rec[1])? '+':'').((($rec[1]-$rec[0])!=0)? $rec[1]-$rec[0]:'-')?></td></tr><?}
     ?></table><?

     echo 'Найдено существующих  '.$sort_exist.' сортаментов<br>' ;                                                           //Пропущено 4 сортаментов без артикля
     echo 'Добавлено новых '.$sort_adding.' сортаментов<br>' ;                                                       //Пропущено 40 сортаментов без штрих-кода
     echo 'Пропущено '.$cnt_no_art.' сортаментов без артикля<br>' ;                                                           //Пропущено 4 сортаментов без артикля
     echo 'Пропущено '.$cnt_no_code.' сортаментов без штрих-кода<br>' ;                                                       //Пропущено 40 сортаментов без штрих-кода

 }


 function import_price_from_XML()
 { ?><h2>Импортируем цены и склад из offers.xml</h2><?
   set_time_limit(0) ;
   $xml=simplexml_load_file(_DIR_TO_ROOT.'/1c/1cobmen/work/offers.xml') ;

   $arr_attr=$xml->ПакетПредложений->attributes() ;
   $mode=($arr_attr['СодержитТолькоИзменения']=='true')? 1:0 ; // 1 - 'Только измемения' ; 0 -  'Все данные' ;
   if ($mode) echo '<div class="green bold">Выгрузка содержит только изменения</div>' ; else echo '<div class="green bold">Выгрузка содержит  все данные</div>' ;
   echo '<br>' ;



   $i=1 ;
   // очищаем все остатки
   if (!$mode) execSQL_update('delete from '.TM_SORTAMENT_STOCK.' where clss=66') ;
   $arr_sortament_exist=execSQL('select code,pkey,price,stock from '.TM_SORTAMENT.' where clss=65') ; // все сортаменты должны быть с штрихкодом, без вариантов
   $cnt_update=0 ; $cnt_notfound=0 ;
   echo 'В выгрузке 1С <strong>'.sizeof($xml->ПакетПредложений->Предложения->Предложение).'</strong> позиций с ценами товаров<br><br>' ;
   ?><table class="debug"><tr><th>№</th><th>id сортамента</th><th>Код 1С</th><th>Штрих-код</th><th>Цена розн., руб.</th><th>Склад</th><th>Остатки</th><th>Результат</th></tr><?
   foreach ($xml->ПакетПредложений->Предложения->Предложение as $offer=>$price_info)
   { $arr_attr=$price_info->attributes() ;
     if ($arr_attr['Статус']!='Удален')
     {   $code_1c=(string)$price_info->Ид ;
         $code=(string)$price_info->Штрихкод ;
         $sortament_rec=$arr_sortament_exist[$code] ;
         $sortament_id=$sortament_rec['pkey'] ;
         $stock=(int)$price_info->Количество;
         $price=(int)$price_info->Цены->Цена[0]->ЦенаЗаЕдиницу;

         $comment='' ;
         if ($sortament_id)
         { if ($price!=$sortament_rec['price'] or $stock!=$sortament_rec['stock'])
           { $_SESSION['goods_system']->exec_price($sortament_rec) ;
             execSQL_update('update '.TM_SORTAMENT.' set price="'.$price.'", stock="'.$stock.'" where pkey="'.$sortament_id.'"') ;
             if ($price!=$sortament_rec['price']) $comment.='Цена разничная ' ;
             if ($stock!=$sortament_rec['stock']) $comment.='Склад ' ;
             $cnt_update++ ;
           }
           $arr=array() ;
           if (isset($price_info->Остатки))
           { if ($mode) execSQL_update('delete from '.TM_SORTAMENT_STOCK.' where clss=66 and parent='.$sortament_id) ; // в режиме только изменения удаляем остатки только измененных товаров
             if (sizeof($price_info->Остатки->Остаток)) foreach($price_info->Остатки->Остаток as $ostatok)
             {  execSQL_update('insert into  '.TM_SORTAMENT_STOCK.' (parent,clss,enabled,obj_name,cnt,c_data,r_data) values ('.$sortament_id.',66,1,"'.$ostatok->Склад.'","'.$ostatok->Количество.'",'.time().','.time().')') ;
                $arr[]=$ostatok->Склад.': '.$ostatok->Количество ;
             }
           }

         } else $cnt_notfound++ ;

         if ($comment or !$sortament_id)
         {?><tr><?
         ?><td class="left"><?echo $i ;?></td><?
         ?><td class="left"><?echo $sortament_id ;?></td><?
         ?><td class="left"><?echo $code_1c ;?></td><?
         ?><td><?echo $code ;?></td><?
         ?><td><?echo $price ;?></td><?
         ?><td><?echo $stock ;?></td><?
         ?><td><?if (sizeof($arr)) echo implode('<br>',$arr) ;?></td><?
         ?><td><?if ($comment) echo '<span class="green">СОХРАНЕНО: '.$comment.'</span>' ;
                 elseif (!$sortament_id) echo '<span class="red">Товар не найден</span>' ;
                 else echo 'ok' ;
         ?></td><?
         ?></tr><?
         }

         $i++ ;    // if ($i>10) break ;
     }

   }
   ?></table><?
   echo 'Обработано '.$i.' позиций выгрузки из 1С<br> ' ;
   echo 'Обновлено  '.$cnt_update.' позиций <br> ' ;
   echo 'Не найдено  '.$cnt_notfound.' позиций <br> ' ;

   ?><h2>Переносим минимальную цену сортамента в товар</h2><?
   dublicate_price() ;
   update_price_site() ;
   return($mode) ;
   /*?><h2>Переносим категории сортамента в товар</h2><?*/
   //dublicate_category() ;
}

function dublicate_price()
 { execSQL_update('update '.TM_GOODS.' set cnt_sort=0') ; // сбрасываем число позиций сортамента у всех товаров
   //execSQL_update('update '.TM_GOODS.' set temp=1 where clss=200 and pkey in (select distinct(goods_id) from '.TM_SORTAMENT.')') ;
   // переносим в товары цены из сортамента активного и в наличии с ценой
   $recs=execSQL('select goods_id,count(pkey) as cnt_sort,min(price) as min_price,sum(stock) as cnt,max(stock2) as cnt2 from '.TM_SORTAMENT.' where enabled=1 group by goods_id',2) ;
   $cnt1=0 ; $cnt2=0 ;
   if (sizeof($recs)) foreach($recs as $goods_id=>$rec)
   {   $stock2=($rec['cnt2'])? 1:0;
       update_rec_in_table(TM_GOODS,array('cnt_sort'=>$rec['cnt_sort'],'price'=>$rec['min_price'],'stock'=>$rec['cnt']),'pkey='.$goods_id) ;
       $cnt1++ ;
   }
   execSQL_update('update '.TM_GOODS.' set enabled=0 where cnt_sort>0 and stock=0 and stock2=0 and clss=200') ;
   execSQL_update('update '.TM_GOODS.' set enabled=1 where cnt_sort>0 and (stock>0 or stock2=1) and clss=200') ;
   echo 'Обновлены цены у '.$cnt1.' товаров<br>' ;
   $disabled_brands=execSQL_row('select id,obj_name from obj_site_list where clss=222 and parent=1447 and enabled=0') ;
   if (sizeof($disabled_brands))
   { execSQL_update('update obj_site_goods set enabled=0 where brand_id>0 and brand_id in (select id from obj_site_list where clss=222 and parent=1447 and enabled=0)') ;
     echo 'Отключены товары для отключенных брендов ('.implode(',',$disabled_brands).')<br>' ;
   }
 }

function update_price_site()
{ ?><h1>Заполняем (обновляем) поле "price_site"</h1><?
  $list_goods=execSQL('select pkey as id,price,koof,val,sales_type,sales_value,obj_name from '.TM_GOODS.' where clss=200 order by pkey') ;
  execSQL_update('update '.TM_GOODS.' set price_site=NULL,price_on=NULL') ;
  $update=0 ;
  ?><table class=left><tr class=clss_header><td>Наименование</td><td>Цена</td><td>Валюта</td><td>Значение скидки</td><td>Цена на сайте</td></tr><?
  if (sizeof($list_goods)) foreach($list_goods as $rec)
  { if (!$rec['val']) $rec['val']=$_SESSION['VL_def_DB'] ;
    $_SESSION['goods_system']->exec_price($rec) ; // damp_array($rec) ;
    if ($rec['__price_4'])
    { $res=update_rec_in_table(TM_GOODS,array('price_site'=>$rec['__price_4'],'price_on'=>1),'pkey='.$rec['id']) ;
    $update+=$res['update'] ;
  }
    ?><tr><td><?echo $rec['obj_name']?></td>
          <td><?if ($rec['price']>0) echo $rec['price']?></td>
          <td><?if ($rec['price']>0) echo $_SESSION['VL_arr'][$rec['val']]['YA']?></td>
          <td><?if ($rec['sales_value']>0) echo $rec['sales_value'].(($rec['sales_type']==3)? '%':$_SESSION['VL_def_DB_ue'])?></td>
          <td><?if ($rec['__price_4']>0) echo $rec['__price_4']?></td>
      </tr>
    <?
  }
  ?></table><?
  echo 'Заполнены цены для '.$update.' товаров<br>' ;
  //damp_array($res) ;
  //print_2x_arr($list_goods) ;
  return ;
}

 // внимание! автоматически при каждом импорте обновляются поля тип, parent, obj_name, art, sostav, brand_id, sezon, comment
function covert_sortament_to_goods()
 { ?><h2>Создаем товары на основе свободного сортамента</h2><?
     $cnt=0 ;
     $recs=execSQL('select * from '.TM_SORTAMENT.' where clss=65 and (goods_id is null or goods_id=0)') ;
     echo 'Найдено <strong>'.sizeof($recs).'</strong> позиций свободного сортамента<br>' ;
     $recs_art=group_by_field('art',$recs) ;
     //damp_array($recs_art) ;
     echo 'Найдено <strong>'.sizeof($recs_art).'</strong> уникальных артикулов среди свободного сортамента<br>' ;
     if (sizeof($recs_art)) foreach($recs_art as $art=>$rec_art) if ($art)
     {  // проверяем, нет ли уже существующего товара для такого артикула
        $goods_id=execSQL_value('select pkey from '.TM_GOODS.' where art="'.$art.'"') ;
        if (!$goods_id)
        {   $reс_goods=array() ;
            $reс_goods['clss']=200 ;
            $reс_goods['enabled']=1 ;
            $reс_goods['obj_name']=$rec_art[0]['obj_name'] ;  // одно наименование на весь сортамент
            $reс_goods['art']=$rec_art[0]['art'] ;  // один артикул на весь сортамент
            $reс_goods['url_name']=_CLSS($reс_goods['clss'])->prepare_url_obj($reс_goods) ;
            //$reс_goods['parent']=_ID_PARENT_NEW_GOODS ;
            $reс_goods['parent']=$rec_art[0]['section_id'] ;
            $goods_id=adding_rec_to_table(TM_GOODS,$reс_goods) ;
            echo '<div>Добавлен новый товар: <span class=green>'.$reс_goods['obj_name'].'</span>, <span class="gray">'.$reс_goods['art'].'</span>, id='.$goods_id.'</div>' ;
            $cnt++ ;
        } else echo '<div>Найден существующий товар: id='.$goods_id.'</div>' ;

        update_rec_in_table(TM_SORTAMENT,array('goods_id'=>$goods_id),'art="'.$art.'"') ;
     }
     return($cnt) ;
 }




 // панель вывода сортамента у товара в админке
 function panel_goods_sortament($options=array())
 {  ?><table id="panel_goods_sortament"><tr><td class="mpanel"><?
     _CLSS(65)->show_list_items(TM_SORTAMENT,'goods_id='.$options['pkey']) ;
    ?></td><td class="mpanel list_goods_images"><?
     //panel_goods_images($options['pkey']) ;
    ?></td></tr><?
    ?></table><?



 }

 // показывает изображение, связанное с сортаментом
 function show_sortament_image($rec,$options=array())
 {
   $tkey_images=_DOT($_SESSION['TM_goods_image'])->pkey ;

   ?><div id=sort_img_<?echo $rec['pkey']?>><?
    //if ($rec['img_id'])
    if ($rec['color'])
    {   $img_id=execSQL_value('select pkey from '.TM_GOODS_IMAGES.' where parent='.$rec['parent'].' and color='.$rec['color'].' order by indx limit 1') ;
        //$rec_img=get_obj_info($rec['img_id'].'.'.$tkey_images) ;
        $rec_img=get_obj_info($img_id.'.'.$tkey_images) ;
        ?><div class="item grag_img" reffer="<?echo $rec['_reffer']?>" img_id="<?echo $rec_img['pkey']?>"><img src="<?echo img_clone($rec_img,'small')?>"><div class=name><?echo $rec_img['obj_name']?></div></div><?
    }
   ?></div><?
 }


 function panel_goods_images($goods_id)
 { $recs_images=execSQL('select * from '.TM_GOODS_IMAGES.' where parent='.$goods_id.' and not pkey in (select img_id from '.TM_SORTAMENT.' where goods_id='.$goods_id.' and img_id>0)') ;
   ?><div id="list_goods_images"><strong>Фото без сортамента</strong><?
        if (sizeof($recs_images)) foreach($recs_images as $rec)
        {?><div class="item grag_img" img_id="<?echo $rec['pkey']?>"><img src="<?echo img_clone($rec,'small')?>"><div class=name><?echo $rec['obj_name']?></div></div><?}
         ?><div class="clear"></div> </div>
          <script type="text/javascript">
              $j(document).ready(function()
              {  $j('div.item.grag_img').draggable({revert:'invalid',scope:'test'}) ;

                 $j('table.list tr.item').droppable({scope:'test',tolerance:'pointer',hoverClass:'drop_ok',drop:
                         function(e,ui)
                         { var img_id=$j(ui.draggable).attr('img_id') ;
                           var sort_reffer=$j(this).attr('reffer') ;
                           send_ajax_request({cmd:'set_img_to_sortament',script:'mod/1c',img_id:img_id,sort_reffer:sort_reffer}) ;
                           $j('div.grag_img[img_id="'+img_id+'"]').remove() ;
                         }}) ;

                 $j('td.list_goods_images').droppable({scope:'test',tolerance:'pointer',hoverClass:'drop_ok',drop:
                         function(e,ui)
                         { var img_id=$j(ui.draggable).attr('img_id') ;
                           send_ajax_request({cmd:'clear_img_to_sortament',script:'mod/1c',img_id:img_id}) ;
                         }}) ;


              }) ;
          </script>
          <style type="text/css">
              table#panel_goods_sortament{width:100%;table-layout:auto;border-collapse:collapse;background:white;border:none;}
              table#panel_goods_sortament td.mpanel{vertical-align:top;background:none;border:none;}
              table#panel_goods_sortament td.mpanel.list_goods_images{width:300px;}
              div.item.grag_img{text-align:center;margin:5px;border:1px solid #eaeaea;background:white;padding:5px;width:128px;float:left;}
              div.item.grag_img:nth-child(2n){clear: both;}
              div.item.grag_img.ui-draggable-dragging{
                  -webkit-box-shadow: 0px 0px 5px 0px rgba(50, 50, 50, 0.75);
                  -moz-box-shadow:    0px 0px 5px 0px rgba(50, 50, 50, 0.75);
                  box-shadow:         0px 0px 5px 0px rgba(50, 50, 50, 0.75);
                  z-index:1000;;
              }
              div.item.grag_img.drop_ok{border:1px solid green;
                  -webkit-box-shadow: 0px 0px 18px 0px rgba(12, 255, 101, 1);
                  -moz-box-shadow:    0px 0px 18px 0px rgba(12, 255, 101, 1);
                  box-shadow:         0px 0px 18px 0px rgba(12, 255, 101, 1);
              }
              .drop_ok{background: #aad8be;;
                  -webkit-box-shadow: inset 0px 0px 18px 0px rgba(12, 255, 101, 1);
                  -moz-box-shadow:    inset 0px 0px 18px 0px rgba(12, 255, 101, 1);
                  box-shadow:         inset 0px 0px 18px 0px rgba(12, 255, 101, 1);
              }
          </style>
        <?
 }


?>