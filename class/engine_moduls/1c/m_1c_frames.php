<?php
include_once(_DIR_TO_ENGINE."/admin/c_site.php") ;
include_once(_DIR_TO_ENGINE."/admin/i_clss_func.php") ;
include_once(_DIR_TO_ENGINE.'/admin/c_menu_tabs.php') ;

include_once('i_1c.php') ;



class c_editor_1c extends c_editor_obj
{
  function body($options=array()) { $this->body_frame_2x(array('fra_tree'=>_PATH_TO_ADMIN.'/editor_1c_tree.php')) ; }
}


class c_editor_1c_tree extends c_fra_tree
{

    function body($options=array())
     {?><body id=tree_objects>
         <ul class="tree_root" on_click_script="editor_1c_viewer.php">
            <li clss=1>1с</li>
            <ul><li cmd=sync_1c>Синхронизация с 1С</li>
                <li cmd=covert_sortament_to_goods>Подцепить свободный сортамент к товарам</li>
                <li clss="1">Отладка и тестирование</li>
                <ul>
                    <li cmd=check_1c_files>Проверить статус выгрузки</li>
                    <li cmd=group_goods_by_artikle>Объеденить товары по артикулам</li>
                    <li cmd=extract_zip_1c>Распаковать последнюю версию выгрузки 1С</li>
                    <li cmd=import_sortament_from_1C_XML>Загрузить сортамент из 1C-XML</li>
                    <li cmd=import_price_from_1C_XML>Загрузить цены из 1C-XML</li>
                    <li cmd=dublicate_price>Перенести цены и склад из сортамента в товары</li>

                    <li cmd=check_goods_artikle>Проверка артикулов товара</li>
                    <li cmd=group_goods_by_artikle>Сгруппировать товары по артикулу</li>
                    <li cmd=check_goods_name>Проверка наименований товара</li>
                    <li cmd=find_XML_item>Поиск в выгрузке 1С</li>
                    <li cmd=link_goods_sortament>Сопоставить товар и сортамент 1С по артикулу</li>
                    <li cmd=select_color_from_name>Выделить цвет из названия сортамента</li>
                </ul>
                <li clss="1">Контроль</li>
                <ul>
                    <li cmd=show_free_sortament>Показать сортамент без товара</li>
                    <li cmd=show_bad_sortament>Показать сортамент c несуществующими товарами</li>
                    <li cmd=show_free_goods>Показать товар без сортамента</li>
                    <li cmd=show_free_sortament_group_by_art>Показать сортамент без товара сгруппированый по артикулу</li>
                    <li cmd=show_goods_not_art>Показать товары без артикула</li>
                    <li cmd=show_sort_info>Показать информацию по сортаменту</li>
                </ul>
            </ul>
         </body>
     <?
   }

}


class c_editor_1c_viewer extends c_fra_based
{
  public $top_menu_name='top_menu_list_1c' ;
  public $system_name='1c_system' ; // временно, пока во всех  editor_1c_viewer.php не будет  прописана эта опция: $options['system']='1c_system' ;

 function body(&$options=array()) {$this->body_frame($options) ; }

  function sync_1c()
  { ob_start() ;
    $this->extract_zip_1c() ;
    //$this->check_1c_files() ;
    upload_1c_XML_offer() ;
    $mode=import_price_from_XML() ;  // 1 - 'Только измемения' ; 0 -  'Все данные' ;
    $text_mode=($mode)? 'Только измемения':'Все данные' ;
    covert_sortament_to_goods() ;
    $text=ob_get_clean()  ;
    $name=date('d_m_Y_H_i_s',time()).'.html' ;
    file_put_contents(_DIR_TO_ROOT.'/1c/logs/'.$name,$text) ;
    _event_reg('Импорт из 1С','<a href="/1c/logs/'.$name.'" target=_blank>Отчет</a> - '.$text_mode) ;
    echo $text ;
  }


  function extract_zip_1c()
  {//          execSQL_update('update '.TM_GOODS.' set tkey=205') ;
   //      execSQL_update('update '.TM_GOODS_IMAGES.' set tkey=210') ;
     ?><h1>Распаковываем последний архив</h1><?
     //ob_start() ;
      $list_files=get_files_list(_DIR_TO_ROOT.'/1c/1cobmen/export/',array('name_pattern'=>'offers')) ; //damp_array($list_files,1,-1) ;
      if (sizeof($list_files))
      { rsort($list_files) ; // более новый файл будет в конце
        $import_zip_file=$list_files[0] ;
        $time=filemtime($import_zip_file) ;
        echo 'Распаковываем <strong>'.basename($import_zip_file).'</strong> [загружен '.date('d.m.Y H:i',$time).']<br>' ;
        unzip_arhive($import_zip_file,_DIR_TO_ROOT.'/1c/1cobmen/work/') ;
        $fname=str_replace('.zip','',basename($import_zip_file)) ;
        rename(_DIR_TO_ROOT.'/1c/1cobmen/work/'.$fname.'.xml',_DIR_TO_ROOT.'/1c/1cobmen/work/offers.xml') ;
        //unlink($import_zip_file) ;
        //echo 'Архив выгрузки удален<br>' ;
      } else echo 'Папка выгрузки 1С пуста<br>' ;


      $list_files=get_files_list(_DIR_TO_ROOT.'/1c/1cobmen/export/',array('name_pattern'=>'import')) ; //damp_array($list_files,1,-1) ;
      if (sizeof($list_files))
      { rsort($list_files) ; // более новый файл будет в конце
        $import_zip_file=$list_files[0] ;
        $time=filemtime($import_zip_file) ;
        echo 'Распаковываем <strong>'.basename($import_zip_file).'</strong> [загружен '.date('d.m.Y H:i',$time).']<br>' ;
        unzip_arhive($import_zip_file,_DIR_TO_ROOT.'/1c/1cobmen/work/') ;
        $fname=str_replace('.zip','',basename($import_zip_file)) ;
        rename(_DIR_TO_ROOT.'/1c/1cobmen/work/'.$fname.'.xml',_DIR_TO_ROOT.'/1c/1cobmen/work/import.xml') ;
        //unlink($import_zip_file) ;
        //echo 'Архив выгрузки удален<br>' ;
      }
     //$text=ob_get_clean() ;

     //_event_reg('Распраковка архива выгрузки 1С',$text) ;
     //echo $text ;
  }

 function check_1c_files()
 {
   $xml=simplexml_load_file(_DIR_TO_ROOT.'/1c/1cobmen/work/offers.xml') ;
   $arr_attr=$xml->ПакетПредложений->attributes() ;
   if ($arr_attr['СодержитТолькоИзменения']=='true') echo 'Только измемения' ; else echo 'Все данные' ;
   echo '<br>' ;

   $xml=simplexml_load_file(_DIR_TO_ROOT.'/1c/1cobmen/work/import.xml') ;
   $arr_attr=$xml->Каталог->attributes() ;
   if ($arr_attr['СодержитТолькоИзменения']=='true') echo 'Только измемения' ; else echo 'Все данные' ;
   echo '<br>' ;



   $xml=simplexml_load_file(_DIR_TO_ROOT.'/1c/offers63569201666.xml') ;
   $arr_attr=$xml->ПакетПредложений->attributes() ;
   if ($arr_attr['СодержитТолькоИзменения']=='true') echo 'Только измемения' ; else echo 'Все данные' ;
   echo '<br>' ;

   $xml=simplexml_load_file(_DIR_TO_ROOT.'/1c/import63570302713.xml') ;
   $arr_attr=$xml->Каталог->attributes() ;
   if ($arr_attr['СодержитТолькоИзменения']=='true') echo 'Только измемения' ; else echo 'Все данные' ;
   //print_r($arr_attr) ;
   //        /ПакетПредложений СодержитТолькоИзменения="true"
   //<Каталог СодержитТолькоИзменения="false">
   //damp_array($arr_attr,1,-1) ;
 }


    function find_XML_item()
    {
        ?><h2>Информация по сортаменту</h2>
        <form>
            Код 1С <input type="text" name="code"> или артикул <input type="text" name="art"> <input type="submit" cmd=find_XML_item_apply value="Найти" validate="form"></form>

        <?
        $list_files=get_files_list(_DIR_TO_ROOT.'/1c/export/',array('name_pattern'=>'xml')) ; //damp_array($list_files,1,-1) ;
        damp_array($list_files,1,-1) ;

    }

    function find_XML_item_apply()
    {
      $list_files=get_files_list(_DIR_TO_ROOT.'/1c/export/',array('name_pattern'=>'xml')) ; //damp_array($list_files,1,-1) ;
      //damp_array($list_files,1,-1) ;
      //damp_array($_POST);
      if ($_POST['code']) echo 'ищем штрих-код:'.$_POST['code'].'<br>' ;
      if ($_POST['art']) echo 'ищем артикул:'.$_POST['art'].'<br>' ;
      //damp_string($_POST['art']) ;
      //damp_string('06-2-DS-R-000351') ;
      ?><table><?
      if (sizeof($list_files)) foreach($list_files as $fname)
      { ?><h2><?echo $fname?></h2><?
        $xml=simplexml_load_file($fname) ;
        if (sizeof($xml->Каталог->Товары->Товар))  foreach ($xml->Каталог->Товары->Товар as $goods) // проходим по списку сортаментов, группируем их в товары
           { $code=(string)$goods->Штрихкод ;
             $art=trim((string)$goods->Артикул) ;
             $show=0 ;
             if ($_POST['code'] and $code==$_POST['code']) $show=1 ;
             if ($_POST['art'] and $_POST['art']==$art) $show=1 ;
             //echo $art.'<br>' ; var_dump($art) ; var_dump($_POST['art']) ;
             if ($show) {?><textarea style="width:100%;height:100px;"><?print_r($goods) ;?></textarea><?}
             //break ;
           }
        if (sizeof($xml->ПакетПредложений->Предложения->Предложение)) foreach ($xml->ПакетПредложений->Предложения->Предложение as $offer=>$price_info)
            { $code=(string)$price_info->Штрихкод ;
              $art=trim((string)$price_info->Артикул) ;
              $show=0 ;
              if ($_POST['code'] and $code==$_POST['code']) $show=1 ;
              if ($_POST['art'] and $_POST['art']==$art) $show=1 ;
              //echo $art.'<br>' ; var_dump($art) ; var_dump($_POST['art']) ;
              if ($show) {?><textarea style="width:100%;height:100px;"><?print_r($price_info) ;?></textarea><?}
              //break ;
            }



      }
      ?></table><?
    }

 function import_sortament_from_1C_XML()
 {
     upload_1c_XML_offer() ;
 }

 function import_price_from_1C_XML()
 {
     import_price_from_XML() ;
 }

 function covert_sortament_to_goods()
 {
     covert_sortament_to_goods() ;
 }

 function dublicate_price()
 {
     dublicate_price() ;
 }

 function link_goods_sortament()
 {
    execSQL_update('update '.TM_SORTAMENT.' t1 set t1.goods_id=(select t2.pkey from '.TM_GOODS.' t2 where t2.art!="" and t2.art=t1.art and (t2.is_sortament=0 or t2.is_sortament is null))',array('debug'=>2)) ;
 }
   /*
 function link_goods_sortament_by_name()
 {
    execSQL_update('update '.TM_SORTAMENT.' t1 set t1.goods_id=(select t2.pkey from '.TM_GOODS.' t2 where t2.obj_name!="" and t2.obj_name=t1.obj_name and (t2.is_sortament=0 or t2.is_sortament is null)) where (goods_id is null or goods_id=0)',array('debug'=>2)) ;
 }   */

 function check_goods_artikle()
 {  ?><h2>Выявляем товары (is_sortament=0) c неуникальым артикулом</h2><?
    $recs=execSQL('select art,count(pkey) as cnt from '.TM_GOODS.' where clss=200 and art!="" and not art is NULL and (is_sortament=0 or is_sortament is null) group by art having cnt>1') ;
    if (sizeof($recs)) foreach($recs as $rec)
    { ?><h2>Товары с артикулом "<?echo $rec['art']?>" - <?echo $rec['cnt']?> шт.</h2><?
      _CLSS(200)->show_list_items(TM_GOODS,'art="'.$rec['art'].'"') ;
    }
    else echo 'ВСЕ товары имеют уникальные артикулы' ;
    //damp_array($recs) ;
 }

 function check_goods_name()
 {  ?><h2>Выявляем товары (is_sortament=0) c неуникальым наименованием</h2><?
    $recs=execSQL('select obj_name,count(pkey) as cnt from '.TM_GOODS.' where clss=200 and obj_name!="" and not obj_name is NULL and (is_sortament=0 or is_sortament is null) group by obj_name having cnt>1') ;
    if (sizeof($recs)) foreach($recs as $rec)
    { ?><h2>Товары с наименованием "<?echo $rec['obj_name']?>" - <?echo $rec['cnt']?> шт.</h2><?
      _CLSS(200)->show_list_items(TM_GOODS,'obj_name="'.$rec['obj_name'].'"') ;
    }
    else echo 'ВСЕ товары имеют уникальные наименования' ;
    //damp_array($recs) ;
 }

 function group_goods_by_artikle()
 { $arr_goods_old_group=array() ;
   $arr_goods_old=execSQL('select pkey,art,obj_name from '.TM_GOODS.' where clss=200 and art!="" and not art is null and (is_sortament=0 or is_sortament is null) order by pkey') ;
   echo 'В базе '.sizeof($arr_goods_old).' товаров с артикулами<br>' ;     //Обработано 1965 позиций выгрузки, найдено 1156 товаров с сортаментом
   if (sizeof($arr_goods_old)) foreach($arr_goods_old as $pkey=>$rec) $arr_goods_old_group[trim($rec['art'])][$pkey]=$rec['obj_name'] ;
   echo 'В базе '.sizeof($arr_goods_old_group).' уникальных артикулов<br>' ;     //Обработано 1965 позиций выгрузки, найдено 1156 товаров с сортаментом
   if (sizeof($arr_goods_old_group)) foreach($arr_goods_old_group as $art=>$arr_ids) if (sizeof($arr_ids)>1)
   { list($main_id,$main_rec)=each($arr_ids) ;
     // переносим названия товара в название фото
     if (sizeof($arr_ids)) foreach($arr_ids as $pkey=>$name)
     {  //$sql='update '.TM_GOODS_IMAGES.' set obj_name="'.addslashes($name).'",parent='.$main_id.' where parent='.$pkey ;
        $res=update_rec_in_table(TM_GOODS_IMAGES,array('obj_name'=>addslashes($name),'parent'=>$main_id),'parent='.$pkey) ;
     }
     unset($arr_ids[$main_id]) ;
     // переносим картинки в основной товар
     execSQL_update('update '.TM_GOODS_IMAGES.' set parent='.$main_id.' where parent in ('.implode(',',array_keys($arr_ids)).')') ;
     // удаляем вторичный товар
     execSQL_update('delete from '.TM_GOODS.' where pkey in ('.implode(',',array_keys($arr_ids)).')') ;
     echo 'Объединен товар артикул <strong>'.$art.'</strong><br>' ;
   }
 }

 function group_goods_by_name()
 { $arr_goods_old_group=array() ;
   $arr_goods_old=execSQL('select pkey,art,obj_name from '.TM_GOODS.' where clss=200 and obj_name!="" and not obj_name is null and (is_sortament=0 or is_sortament is null) order by pkey') ;
   echo 'В базе '.sizeof($arr_goods_old).' товаров с наименованиями<br>' ;     //Обработано 1965 позиций выгрузки, найдено 1156 товаров с сортаментом
   if (sizeof($arr_goods_old)) foreach($arr_goods_old as $pkey=>$rec) $arr_goods_old_group[trim($rec['obj_name'])][$pkey]=$rec['art'] ;
   echo 'В базе '.sizeof($arr_goods_old_group).' уникальных наименований<br>' ;     //Обработано 1965 позиций выгрузки, найдено 1156 товаров с сортаментом

   if (sizeof($arr_goods_old_group)) foreach($arr_goods_old_group as $name=>$arr_ids) if (sizeof($arr_ids)>1)
   { list($main_id,$main_rec)=each($arr_ids) ;

     // переносим названия товара в название фото
     if (sizeof($arr_ids)) foreach($arr_ids as $pkey=>$name) execSQL_update('update '.TM_GOODS_IMAGES.' set obj_name="'.addslashes($name).'",parent='.$main_id.' where parent='.$pkey) ;
       echo 'Объединен товар  <strong>'.$name.'</strong> = ['.sizeof($arr_ids).'], артикулы '.implode(',',$arr_ids).'<br>' ;
     unset($arr_ids[$main_id]) ;

     // переносим товар в товар
     //execSQL_update('update '.TM_GOODS.' set parent='.$main_id.',is_sortament=1 where pkey in ('.implode(',',array_keys($arr_ids)).')') ;
     // переносим картинки в основной товар
     //execSQL_update('update '.TM_GOODS_IMAGES.' set parent='.$main_id.' where parent in ('.implode(',',array_keys($arr_ids)).')') ;

   }
 }

 function show_free_sortament()
 {
    ?><h2>Сортамент 1С без товара на сайте</h2><?
    _CLSS(65)->show_list_items(TM_SORTAMENT,'(goods_id is null or goods_id=0)') ;
 }

function show_sortament_not_price()
 {
    ?><h2>Сортамент 1С без цены</h2><?
    _CLSS(65)->show_list_items(TM_SORTAMENT,'price=0') ;
 }

 function show_bad_sortament()
 {
    ?><h2>Сортамент 1С c goods_id на несуществующие товары</h2><?
    _CLSS(65)->show_list_items(TM_SORTAMENT,'not goods_id in (select pkey from '.TM_GOODS.')') ;
 }

 function show_free_goods()
 {
    ?><h2>Товары сайта без сортамента в 1С</h2><?
    _CLSS(200)->show_list_items(TM_GOODS,'not pkey in (select goods_id from '.TM_SORTAMENT.' where goods_id>0) and enabled=1 and _enabled=1 and (is_sortament=0 or is_sortament is null)',array('order'=>'parent')) ;

    //_CLSS(65)->show_list_items(TM_SORTAMENT,'(goods_id is null or goods_id=0)') ;
 }

 function show_goods_not_art()
 {
    ?><h2>Товары на сайте без артикула, включенные к показу на сайте</h2><?
    _CLSS(200)->show_list_items(TM_GOODS,'(art is null or art="") and enabled=1') ;

   //$recs=execSQL('select pkey,obj_name,')
 }


 function show_free_sortament_group_by_art()
 {
    ?><h2>Сортамент 1С без товара на сайте сгруппированный по артикулу, более одного товара в группе:</h2><?
    $recs=execSQL_row('select art,count(pkey) as cnt from '.TM_SORTAMENT.' where (goods_id is null or goods_id=0) group by art having cnt>1') ;
    //damp_array($recs) ;
    if (sizeof($recs)) foreach($recs as $art=>$count)
    {  ?><h2><?echo $art?></h2><?
       _CLSS(65)->show_list_items(TM_SORTAMENT,'art="'.$art.'"',array('buttons'=>array())) ;
    }
 }

 function select_color_from_name()
 {
     $arr_art_uni=execSQL_row('select art,count(pkey) as cnt from '.TM_SORTAMENT.' where clss=65 group by art having cnt>1') ;
     //damp_array($arr_art_uni) ; return ;
        ?><table class="debug"><?
        if (sizeof($arr_art_uni))  foreach($arr_art_uni as $art=>$cnt)
        {    $recs=execSQL_row('select pkey,obj_name from '.TM_SORTAMENT.' where art="'.$art.'"') ;

             $arr_res=array() ;  list($id,$name)=each($recs) ;  reset($recs);
                 ?><tr><th colspan="4"><h3><?echo $name?></h3></th></tr><?
                 foreach($recs as $sort_id=>$value)
                 { $value=str_replace(array('(',')'),array(' ( ',' ) '),$value) ;
                   $arr=explode(' ',$value) ;
                   if (sizeof($arr)) foreach($arr as $str) if (trim($str)) $arr_res[$str]++ ;
                 }

                 foreach($recs as $sort_id=>$value)
                 { $value=str_replace(array('(',')'),array(' ( ',' ) '),$value) ;
                   $arr=explode(' ',$value) ;  $arr_str_main=array() ;  $arr_str_second=array() ; $new_value_main='' ; $new_value_second='' ;
                   if (sizeof($arr)) foreach($arr as $str) if ($arr_res[$str]>1) $arr_str_main[]=$str ; else $arr_str_second[]=$str ;
                   if (sizeof($arr_str_main))  $new_value_main=implode(' ',$arr_str_main) ;
                   if (sizeof($arr_str_second))  $new_value_second=implode(' ',$arr_str_second) ;

                     $new_value_main=trim(str_replace(array('  ','( )'),array(' ',''),$new_value_main)) ;
                     $new_value_second=trim(str_replace(array('  ','( )'),array(' ',''),$new_value_second)) ;
                     //if (!$new_value_second) $new_value_second=$new_value_main ;
                   echo '<tr><td>'.$sort_id.'</td><td class="left">'.$value.'</td><td class="left">'.$new_value_main.'</td><td>'.$new_value_second ;
                   //       if ($sort_id==1178) damp_string()
                            //if ($sort_id==1677) damp_array($arr_res,1,-1) ;
                     echo '</td></tr>' ;
                    execSQL_update('update '.TM_SORTAMENT.' set color_name="'.addslashes($new_value_second).'" where pkey='.$sort_id) ;
                 }
        }
        ?></table><?
 }



 function show_sort_info()
 {
     ?><h2>Информация по сортаменту</h2><form><input type="text" name="code"><input type="submit" cmd=show_sort_info_apply value="Проверить" validate="form"></form><?

 }

 function show_sort_info_apply()
 {
     //damp_array($_POST) ;
     ?><h2>Информация по сортаменту "<?echo $_POST['code']?>"</h2><?
     $recs=execSQL('select * from '.TM_SORTAMENT.' where code="'.$_POST['code'].'" or art="'.$_POST['code'].'"') ;
     print_2x_arr($recs) ;

     if (sizeof($recs))
     { foreach($recs as $rec) $arr[]=$rec['goods_id'] ;
       ?><h2>Информация по товару сортамента "<?echo $_POST['code']?>"</h2><?
       $rec=execSQL('select * from '.TM_GOODS.' where pkey in ("'.implode(',',$arr).'")') ;
       print_2x_arr($rec) ;
     }

     if (sizeof($recs))
     { ?><h2>Информация по складу"<?echo $_POST['code']?>"</h2><?
       $rec=execSQL('select * from obj_site_1c_stock where parent in ("'.implode(',',array_keys($recs)).'")') ;
       print_2x_arr($rec) ;
     }
 }


}

?>