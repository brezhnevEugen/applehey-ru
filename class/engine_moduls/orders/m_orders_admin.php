<?php
$__functions['init'][]		='_orders_admin_vars' ;
$__functions['alert'][]		='_orders_admin_alert' ;
$__functions['boot_admin'][]='_orders_admin_boot' ;
$__functions['install'][]	='_orders_install_modul' ;


include_once('clss_82.php') ;

function _orders_admin_vars() //
{
	//-----------------------------------------------------------------------------------------------------------------------------
	// описание переменных модуля
	//-----------------------------------------------------------------------------------------------------------------------------
    //$_SESSION['list_type_rash']=array('1'=>'Закупка товара','2'=>'Текущие расходы','3'=>'Передача наличности','4'=>'Оплата почтовых услуг','5'=>'Оплата курьера','6'=>'Процент менеджера') ;
    //$_SESSION['list_type_prih']=array('1'=>'Оплата заказа (курьер)','2'=>'Оплата заказа (перевод)','3'=>'Прием наличности','4'=>'Другие продажи') ;
    global $TM_list ;
    _DOT($TM_list)->rules[]=array('to_pkey'=>3,'only_clss'=>84) ;
    _DOT($TM_list)->rules[]=array('to_pkey'=>8,'only_clss'=>85) ;

	//-----------------------------------------------------------------------------------------------------------------------------
	// описание классов модуля
	//-----------------------------------------------------------------------------------------------------------------------------
	// заказы
	$_SESSION['descr_clss'][82]['name']='Заказ' ;
	$_SESSION['descr_clss'][82]['parent']=0 ;
	$_SESSION['descr_clss'][82]['fields']['status']='int(11)' ;
    $_SESSION['descr_clss'][82]['fields']['mode']='int(11)' ;
    $_SESSION['descr_clss'][82]['fields']['member_typ']='varchar(32)' ; // тип покупателя: юр лицо, физ лицо
    $_SESSION['descr_clss'][82]['fields']['account_reffer']='any_object' ;
    $_SESSION['descr_clss'][82]['fields']['name']='varchar(255)' ;
    $_SESSION['descr_clss'][82]['fields']['name_f']='varchar(255)' ;
    $_SESSION['descr_clss'][82]['fields']['name_i']='varchar(255)' ;
    $_SESSION['descr_clss'][82]['fields']['name_o']='varchar(255)' ;
    $_SESSION['descr_clss'][82]['fields']['org_name']='varchar(255)' ;
    $_SESSION['descr_clss'][82]['fields']['email']='varchar(255)' ;
    $_SESSION['descr_clss'][82]['fields']['phone']='varchar(255)' ;
    $_SESSION['descr_clss'][82]['fields']['phone2']='varchar(255)' ;
    $_SESSION['descr_clss'][82]['fields']['comment']='text' ;
    $_SESSION['descr_clss'][82]['fields']['zip']='varchar(6)' ;
    $_SESSION['descr_clss'][82]['fields']['city']='varchar(255)' ;
    $_SESSION['descr_clss'][82]['fields']['metro']=array('type'=>'indx_select','array'=>'IL_metro') ;
    $_SESSION['descr_clss'][82]['fields']['adres']='text' ;
    $_SESSION['descr_clss'][82]['fields']['rekvezit']='text' ;
    $_SESSION['descr_clss'][82]['fields']['end_price']='float(10,2)' ;  // сумма заказа на оплату - всесте со всеми скидками, доставками и услугами
    $_SESSION['descr_clss'][82]['fields']['ya_id']='int(11)' ;  // идентификатор яндекса для системы "заказ через яндекс"
    $_SESSION['descr_clss'][82]['fields']['accepted']='int(1)' ;  // подтверждение заказа менеджером для возможности оплаты

    $_SESSION['descr_clss'][82]['fields']['payments_type']=array('type'=>'indx_select','array'=>'IL_payments_type','indx_field'=>'admin') ; // тип оплаты: нал/безнал/карты
    $_SESSION['descr_clss'][82]['fields']['payments_status']=array('type'=>'indx_select','array'=>'ARR_orders_payments') ; // платежный статус заказа
    $_SESSION['descr_clss'][82]['fields']['payments_method']='varchar(32)' ; // сервис оплаты  - для вызова соответсвующего сервиса после оформления заказа
    $_SESSION['descr_clss'][82]['fields']['payments_amount']='float(10,2)' ; // размер оплаты, произведенный через платежную систему. Сюда сохраняется ответ сервера оплаты по сумме отплаты
    $_SESSION['descr_clss'][82]['fields']['payments_info']='serialize' ;  //  вся инфа, пришедшея от сервиса оплаты
    $_SESSION['descr_clss'][82]['fields']['payments_data']='timedata' ;  //  дата оплаты заказа
    $_SESSION['descr_clss'][82]['fields']['sbrf_uid']='varchar(128)' ;  //  уканикальный код заказа от сбера

    $_SESSION['descr_clss'][82]['fields']['phone2']='varchar(255)' ;
    $_SESSION['descr_clss'][82]['fields']['price_info']='serialize' ;
    $_SESSION['descr_clss'][82]['fields']['kupon_number']='varchar(52)' ;
    $_SESSION['descr_clss'][82]['fields']['kupon_pin']='varchar(16)' ;
    $_SESSION['descr_clss'][82]['fields']['kupon_sales']='float(10,2)' ;
    $_SESSION['descr_clss'][82]['fields']['uid']='varchar(32)' ;
    $_SESSION['descr_clss'][82]['fields']['track']='varchar(255)' ;
    $_SESSION['descr_clss'][82]['fields']['referer_host']='varchar(255)' ;  // источник, с которого был произведен переход на сайт
    $_SESSION['descr_clss'][82]['fields']['subscript']='int(1)' ;  // подписка на новости
    $_SESSION['descr_clss'][82]['fields']['what']='varchar(255)' ;  // откуда узнали о сайте
    $_SESSION['descr_clss'][82]['fields']['info']='serialize' ;  // вся прочая информация, которая не входит в стандартные поля заказа


	$_SESSION['descr_clss'][82]['on_change_event']['status']='on_change_event_clss_82_status' ;

    $_SESSION['descr_clss'][82]['menu'][]=array("name" => "Информация о заказе",	"cmd" => 'show_order_info') ;
    $_SESSION['descr_clss'][82]['menu'][]=array("name" => "Уведомление клиенту",	"cmd" => 'show_email') ;

    $_SESSION['descr_clss'][82]['icons']=_PATH_TO_BASED_CLSS_IMG.'/7.png' ;

	$_SESSION['descr_clss'][82]['list']['field']['status']				=array('title'=>'Состояние','indx_select'=>'list_status_orders') ;
	$_SESSION['descr_clss'][82]['list']['field']['payments_status']	    =array('title'=>'Оплата','read_only'=>1) ;
	//$_SESSION['descr_clss'][82]['list']['field']['obj_name']			=array('title'=>'Номер, дата','read_only'=>1) ;
	$_SESSION['descr_clss'][82]['list']['field']['pkey']			=array('title'=>'Номер','read_only'=>1) ;
	$_SESSION['descr_clss'][82]['list']['field']['c_data']			=array('title'=>'Дата','read_only'=>1) ;
	//$_SESSION['descr_clss'][82]['list']['field'][]			            =array('title'=>'Купон','use_func'=>'clss_82_show_kupon_info') ;
    $_SESSION['descr_clss'][82]['list']['field']['payments_type']		=array('title'=>'Способ оплаты','read_only'=>1) ;
    $_SESSION['descr_clss'][82]['list']['field']['payments_method']		=array('title'=>'Сервис оплаты','read_only'=>1) ;
    $_SESSION['descr_clss'][82]['list']['field']['end_price']			=array('title'=>'Сумма к оплате','format_as_price'=>1,'read_only'=>1) ;
   	$_SESSION['descr_clss'][82]['list']['field']['payments_data']	    =array('title'=>'Дата<br>оплаты','read_only'=>1) ;
   	$_SESSION['descr_clss'][82]['list']['field']['payments_amount']	    =array('title'=>'Оплаченная<br>сумма','format_as_price'=>1,'read_only'=>1) ;
	$_SESSION['descr_clss'][82]['list']['field']['name']				=array('title'=>'Имя','td_class'=>'left','use_func'=>'get_order_member_name') ;
	//$_SESSION['descr_clss'][82]['list']['field']['email']				=array('title'=>'Почта','td_class'=>'left','read_only'=>1) ;
	//$_SESSION['descr_clss'][82]['list']['field']['phone']				=array('title'=>'Телефон','td_class'=>'left','read_only'=>1) ;
	//$_SESSION['descr_clss'][82]['list']['field']['subscript']			=array('title'=>'Подписка') ;
	//$_SESSION['descr_clss'][82]['list']['field']['adres']				=array('title'=>'Адрес доставки','td_class'=>'left','read_only'=>1) ;
	//$_SESSION['descr_clss'][82]['list']['field'][]						=array('title'=>'Состав заказа','td_class'=>'left','use_func'=>'orders_get_art_info') ;
    $_SESSION['descr_clss'][82]['list']['options']['buttons']=array('save','delete') ;

    $_SESSION['descr_clss'][83]['name']='Купон' ;
   	$_SESSION['descr_clss'][83]['parent']=0 ;
   	$_SESSION['descr_clss'][83]['fields']['typ']='int(11)' ; // код типа позиции сметы
    $_SESSION['descr_clss'][83]['fields']['typ_name']='varchar(32)' ; // название позиции сметы: товар
    $_SESSION['descr_clss'][83]['fields']['member_typ']='varchar(32)' ;
    $_SESSION['descr_clss'][83]['fields']['account_reffer']='any_object' ;
    $_SESSION['descr_clss'][83]['fields']['name']='varchar(255)' ;
    $_SESSION['descr_clss'][83]['fields']['email']='varchar(255)' ;

    
    // тип доставки
    $_SESSION['descr_clss'][84]['name']='Тип доставки' ;
    $_SESSION['descr_clss'][84]['parent']=20 ;
	$_SESSION['descr_clss'][84]['fields']['admin']='varchar(32)' ;
	$_SESSION['descr_clss'][84]['fields']['site']='varchar(200)' ;
	$_SESSION['descr_clss'][84]['fields']['price']='int(11)' ;
	$_SESSION['descr_clss'][84]['fields']['price_ext']='int(11)' ;
	$_SESSION['descr_clss'][84]['fields']['manual']='text' ;
	$_SESSION['descr_clss'][84]['parent_to']=array(3) ;

    $_SESSION['descr_clss'][84]['list']['field']['id']			    ='Код' ;
    $_SESSION['descr_clss'][84]['list']['field']['enabled']		    ='Сост.' ;
    $_SESSION['descr_clss'][84]['list']['field']['indx']			='Позиция' ;
	$_SESSION['descr_clss'][84]['list']['field']['obj_name']		=array('title'=>'Название','td_class'=>'left') ;
	$_SESSION['descr_clss'][84]['list']['field']['admin']			=array('title'=>'Название<br> в админке','td_class'=>'left') ;
	$_SESSION['descr_clss'][84]['list']['field']['price']			=array('title'=>'Базовая цена','td_class'=>'left') ;
	$_SESSION['descr_clss'][84]['list']['field']['price_ext']       =array('title'=>'Дополнительная цена','td_class'=>'left') ;
	//$_SESSION['descr_clss'][84]['list']['field']['manual']			=array('title'=>'Описание','use_HTML_editor'=>1) ;

    $_SESSION['descr_clss'][84]['details']=$_SESSION['descr_clss'][84]['list'] ;

    // тип оплаты
    $_SESSION['descr_clss'][85]['name']='Вариант оплаты' ;
    $_SESSION['descr_clss'][85]['parent']=20 ;
	$_SESSION['descr_clss'][85]['fields']['admin']='varchar(32)' ;
	$_SESSION['descr_clss'][85]['fields']['site']='varchar(200)' ;
	$_SESSION['descr_clss'][85]['fields']['method']='varchar(32)' ;
	$_SESSION['descr_clss'][85]['fields']['price']='int(11)' ;
	$_SESSION['descr_clss'][85]['fields']['manual']='text' ;
	$_SESSION['descr_clss'][85]['parent_to']=array(3) ;

    $_SESSION['descr_clss'][85]['list']['field']['id']			    ='Код' ;
    $_SESSION['descr_clss'][85]['list']['field']['enabled']		    ='Сост.' ;
    $_SESSION['descr_clss'][85]['list']['field']['indx']			='Позиция' ;
	$_SESSION['descr_clss'][85]['list']['field']['obj_name']		=array('title'=>'Название','td_class'=>'left') ;
	$_SESSION['descr_clss'][85]['list']['field']['admin']			=array('title'=>'Название<br> в админке','td_class'=>'left') ;
	$_SESSION['descr_clss'][85]['list']['field']['method']			=array('title'=>'Сервис оплаты','td_class'=>'left') ;
	//$_SESSION['descr_clss'][85]['list']['field']['manual']			=array('title'=>'Описание','use_HTML_editor'=>1) ;

    $_SESSION['descr_clss'][85]['details']=$_SESSION['descr_clss'][85]['list'] ;    
    
    // услуга: подьем на этаж, сборка
    $_SESSION['descr_clss'][81]['name']='Услуга' ;
    $_SESSION['descr_clss'][81]['parent']=20 ;
	$_SESSION['descr_clss'][81]['fields']['admin']='varchar(32)' ;
	$_SESSION['descr_clss'][81]['fields']['site']='varchar(200)' ;
	$_SESSION['descr_clss'][81]['fields']['price']='int(11)' ;
	$_SESSION['descr_clss'][81]['fields']['manual']='text' ;
	$_SESSION['descr_clss'][81]['parent_to']=array(3) ;

    $_SESSION['descr_clss'][81]['list']['field']['id']			    ='Код' ;
    $_SESSION['descr_clss'][81]['list']['field']['enabled']		    ='Сост.' ;
    $_SESSION['descr_clss'][81]['list']['field']['indx']			='Позиция' ;
	$_SESSION['descr_clss'][81]['list']['field']['obj_name']		=array('title'=>'Название','td_class'=>'left') ;
	$_SESSION['descr_clss'][81]['list']['field']['admin']			=array('title'=>'Название<br> в админке','td_class'=>'left') ;
	$_SESSION['descr_clss'][81]['list']['field']['price']			=array('title'=>'Стоимость','td_class'=>'left') ;
	//$_SESSION['descr_clss'][81]['list']['field']['manual']			=array('title'=>'Описание','use_HTML_editor'=>1) ;

    $_SESSION['descr_clss'][81]['details']=$_SESSION['descr_clss'][81]['list'] ;
    
	//Ссылка на товар
    $_SESSION['descr_clss'][120]['name']='Позиция сметы заказа' ;
	$_SESSION['descr_clss'][120]['parent']=100 ;
	$_SESSION['descr_clss'][120]['fields']['obj_name']='varchar(255)' ;
	$_SESSION['descr_clss'][120]['fields']['reffer']='any_object' ; // код объекта или код услуги (sales,fast,shipping)
	$_SESSION['descr_clss'][120]['fields']['sortament_reffer']='any_object' ; // код сортамента
	$_SESSION['descr_clss'][120]['fields']['img_id']='int(11)' ; // id изображения сортамента (обычно нужно когда заказывают другой цвет)
	$_SESSION['descr_clss'][120]['fields']['code']='varchar(32)' ; // штрих-код товара
	$_SESSION['descr_clss'][120]['fields']['price']='float(10,2)' ;
	$_SESSION['descr_clss'][120]['fields']['cnt']='int(11)' ;
	$_SESSION['descr_clss'][120]['fields']['sales_type']='int(11)' ;
	$_SESSION['descr_clss'][120]['fields']['sales_value']='float' ;
	$_SESSION['descr_clss'][120]['fields']['price_info']='text' ;

    $_SESSION['descr_clss'][120]['icons']=_PATH_TO_BASED_CLSS_IMG.'/100.png' ;

	$_SESSION['descr_clss'][120]['list']['field']['enabled']=array('title'=>'Сост.','edit_element'=>3) ;
	$_SESSION['descr_clss'][120]['list']['field']['indx']=array('title'=>'Позиция','edit_element'=>1,'size'=>1) ;
	$_SESSION['descr_clss'][120]['list']['field']['cnt']=array('title'=>'Количество','edit_element'=>1,'size'=>1) ;
	$_SESSION['descr_clss'][120]['list']['options']=array('no_icon_edit'=>1);



	//----------------------------------------------------------------------------------------------------------------------
	// описание пунктов меню админа сайта
	//-----------------------------------------------------------------------------------------------------------------------

    $_SESSION['menu_admin_site']['Модули']['order']=array('name'=>'Заказы','href'=>'editor_orders.php','icon'=>'menu/shopping_cart.jpg') ;

	//----------------------------------------------------------------------------------------------------------------------
	// описание пунктов верхнего меню админа сайта
	//-----------------------------------------------------------------------------------------------------------------------


	//----------------------------------------------------------------------------------------------------------------------
	// описание меню фрейма объекта
	//-----------------------------------------------------------------------------------------------------------------------
	global $menu_set ;
	$menu_set['any'][82][]=array("name" => "Просмотр",				"action" => 'view','def'=>1) ; // заказ
	//$menu_set['any'][82][]=array("name" => "Правка",				"action" => 'edit') ;
	$menu_set['any'][82][]=array("name" => "Добавить комментарий",	"action" => 'add_comment') ;
	$menu_set['any'][82][]=array("name" => "История заказа",		"action" => 'history') ;
	$menu_set['any'][82][]=array("name" => "Контакт",				"action" => 'send_mess') ;

}

function _orders_admin_boot($options)
{
	create_system_modul_obj('orders') ;
}

function _orders_install_modul($DOT_root)
{   echo '<h2>Инсталируем модуль <strong>Система заказов</strong></h2>' ;

    $file_items=array() ;
    // editor_orders.php
    $file_items['editor_orders.php']['include'][]				='_DIR_TO_MODULES."/orders/m_orders_frames.php"' ;
    $file_items['editor_orders.php']['options']['use_table_code']='"TM_orders"' ;
    $file_items['editor_orders.php']['options']['title']		='"Заказы"' ;
    $file_items['editor_orders_tree.php']['include'][]			='_DIR_TO_MODULES."/orders/m_orders_frames.php"' ;
    $file_items['editor_orders_viewer.php']['include'][]		='_DIR_TO_MODULES."/orders/m_orders_frames.php"' ;
    $file_items['editor_orders_ajax.php']['include'][]		    ='_DIR_TO_MODULES."/orders/m_orders_ajax.php"' ;
    create_menu_item($file_items) ;

    // таблица заказов
	global $TM_orders ;
	$pattern=array() ;

	$pattern['table_title']				= 	'Заказы' ;
	$pattern['use_clss']				= 	'1,82' ;
	$pattern['def_recs'][]				=	array ('parent'=>0,	'clss'=>1,	'enabled'=>1,	'indx'=>0,	'obj_name'=>'Система заказов') ;
   	$pattern['table_name']				= 	$TM_orders ;
	$pattern['table_clss']				= 	101 ;
	$pattern['table_parent']			= 	$DOT_root;
   	$DOT_order=create_table_by_pattern($pattern) ;

   	// товары в заказах - OUT table
   	global $TM_orders_goods ;
	$pattern=array() ;
	$pattern['table_title']				= 	'Товары' ;
	$pattern['use_clss']				= 	'120' ;
   	$pattern['table_name']				= 	$TM_orders_goods ;
	$pattern['table_clss']				= 	101 ;
	$pattern['table_parent']			= 	$DOT_order;
   	create_table_by_pattern($pattern) ;

    // комментарии в заказах - OUT table
	global $TM_orders_comment ;
   	$pattern=array() ;
	$pattern['table_title']				= 	'Комментарии' ;
	$pattern['use_clss']				= 	'15' ;
   	$pattern['table_name']				= 	$TM_orders_comment ;
	$pattern['table_clss']				= 	101 ;
	$pattern['table_parent']			= 	$DOT_order;
   	create_table_by_pattern($pattern) ;

    SETUP_get_img('/orders/admin_img_menu/shopping_cart.jpg',_PATH_TO_ADMIN.'/img/menu/shopping_cart.jpg') ;

    create_dir('/orders/') ;
    SETUP_get_file('/orders/system_dir/htaccess.txt','/orders/.htaccess');
    SETUP_get_file('/orders/system_dir/index.php','/orders/index.php');
    SETUP_get_file('/orders/system_dir/cart.php','/orders/cart.php');
    SETUP_get_file('/orders/system_dir/checkout.php','/orders/checkout.php');
    SETUP_get_file('/orders/system_dir/complette.php','/orders/complette.php');

    $id_sect=SETUP_add_section_to_site_setting('Настройка почтовых уведомлений') ;
    SETUP_add_rec_to_site_setting($id_sect,'Адрес, на который отправлять уведомление о совершении заказа','debug@12-24.ru','LS_orders_system_email_alert') ;

    SETUP_add_class_to_table(TM_LIST,84) ; // добавляем поддержку "Варианты доставки" в списки сайта
    SETUP_add_class_to_table(TM_LIST,85) ; // добавляем поддержку "Варианты оплаты" в списки сайта

    $id_sect=SETUP_add_section_to_site_list('Система заказов') ;
    $id_list=SETUP_add_list_to_site_list($id_sect,'Варианты доставки','type_shiping') ;
    SETUP_add_rec_to_site_list($id_list,84,array('id'=>1,'obj_name'=>'Курьер (только по Москве)','admin'=>'Курьер','price'=>300))  ;
    SETUP_add_rec_to_site_list($id_list,84,array('id'=>2,'obj_name'=>'Почта России','admin'=>'Почта','price'=>300))  ;
    SETUP_add_rec_to_site_list($id_list,84,array('id'=>3,'obj_name'=>'Транспортная компания','admin'=>'Тр.комп.','price'=>300))  ;
    SETUP_add_rec_to_site_list($id_list,84,array('id'=>4,'obj_name'=>'Самовывоз со склада в Москве','admin'=>'Самовывоз','price'=>0))  ;

    $id_list=SETUP_add_list_to_site_list($id_sect,'Варианты оплаты','payments_type') ;
    SETUP_add_rec_to_site_list($id_list,85,array('id'=>1,'obj_name'=>'Наличные (только для Москвы)','admin'=>'Наличные'))  ;
    SETUP_add_rec_to_site_list($id_list,85,array('id'=>2,'obj_name'=>'Безналичный расчет','admin'=>'Безнал'))  ;
    SETUP_add_rec_to_site_list($id_list,85,array('id'=>3,'obj_name'=>'Наложенный платеж (только для частных лиц)','admin'=>'Налож.платеж.'))  ;
    SETUP_add_rec_to_site_list($id_list,85,array('id'=>4,'obj_name'=>'Электронный платеж','admin'=>'Электронный платеж','method'=>'robox'))  ;

    import_setting_from_xml(_PATH_TO_SERVER_SETUP.'?m=orders&s=mails',array('parent_tkey'=>$_SESSION['pkey_by_table'][TM_MAILS],'parent_pkey'=>1,'check_unique_name'=>1)) ;
}

function _orders_admin_alert()
{ global $TM_orders ;
  $res=execSQL_van('select count(pkey) as cnt from '.$TM_orders.' where clss=82 and status=0') ;
  if ($res['cnt']) echo "<div>У Вас <span class='red bold'>".$res['cnt']."</span> <a href='"._PATH_TO_ADMIN."/editor_orders.php'>необработанных заказа</a></div><br>";
}


//-----------------------------------------------------------------------------------------------------------------------------
// Функции вывода полей
//-----------------------------------------------------------------------------------------------------------------------------


  function clss_82_show_kupon_info(&$rec)
    { $text='' ;
      if ($rec['kupon_number']) $text='Купон № '.$rec['kupon_number'].'<br>PIN: '.$rec['kupon_pin'].'<br>Скидка: '.$rec['kupon_sales'] ;
      return($text);
    }

  function get_order_member_name(&$rec)
  {
  	if ($rec['member_typ']) $text='<div class=green>'.$rec['member_typ'].'</div>' ;
  	$text.=$rec['name'] ;

  	if ($rec['account_reffer']) $text.='<br><span class=green>'.generate_obj_text($rec['account_reffer']).'</span>';
  	return($text) ;
  }

  // возвращает список наименований заказа
  function orders_get_art_info(&$rec,$tkey,$pkey)
  { global $descr_obj_tables ;
    $tkey_child=$descr_obj_tables[$tkey]->list_clss_ext[120]['out'] ;
    $ref_table=$descr_obj_tables[$tkey_child]->table_name ;
    $list=execSQL('select * from '.$ref_table.' where parent='.$pkey) ;
    $text='<ul>' ;
    if (sizeof($list)) foreach($list as $rec2) $text.='<li>'.$rec2['obj_name'].', '.$rec2['price'].' = '.$rec2['cnt'].' шт.</li>' ;
    $text.='</ul>' ;
    return($text) ;
  }


//-----------------------------------------------------------------------------------------------------------------------------
// Функции вывода списков и свойств
//-----------------------------------------------------------------------------------------------------------------------------

 function cmd_show_clss_82_page_view() // редактирование заказов
	 {  global $tkey,$pkey,$descr_obj_tables,$obj_info,$orders_system ;
	    //damp_array($obj_info) ;
        $objs_info=select_reffers_objs($descr_obj_tables[$tkey]->list_clss[120],' parent='.$obj_info['pkey']) ;
        $comment_info=$orders_system->get_comment_order($obj_info['pkey']) ;
        //damp_array($comment_info) ;
	    ?><a href="javascript:print();">Распечатать</a><br><br><?
   		?><table>
		        <tr><td class='no_border' valign=top><? cmd_show_clss_0_page_2() ;?> </td>
		            <td class='no_border' valign=top><? cmd_show_list_objects($objs_info,array('title'=>'Состав заказа','small'=>0,'no_page_panel'=>1,'read_only'=>1)) ;
                                                        cmd_show_list_objects($comment_info,array('title'=>'Комментарии к заказу','small'=>0,'no_page_panel'=>1,'read_only'=>1,'is_obj_array'=>1)) ;
                                                     ?></td>
		        </tr>
		 </table>
         <?
         if (sizeof($obj_info['obj_clss_15'])) cmd_show_array_objects(15,$obj_info['obj_clss_15'],array('title'=>'Комментарии к заказу','read_only'=>1)) ;
	 }

 function cmd_show_clss_82_page_history()
 { global $obj_info ;
   show_history('links like "%'.$obj_info['_reffer'].'%" or reffer="'.$obj_info['_reffer'].'"') ;
 }

 function cmd_show_clss_82_page_add_comment()
 { global $obj_info ;
   ?><br><strong>Новый комментарий</strong><br>
     <textarea name="comment" rows=5 cols=100 wrap="off" class=my_support_message_text></textarea><br><br>
     <button onclick="exe_cmd('order_add_comment')">Добавить</button>
   <?
 }

 function cmd_exe_order_add_comment()
 { global $obj_info,$tkey,$pkey,$comment,$member,$events_system ;
   if (!$pkey or !$tkey or !$comment) return ;
   // создаем новый сообщение
   $finfo=array(	'parent'		=>	$pkey, // все сообщения в корне
			        'clss'			=>	15, // 27 - сообщеие в отзывы
    				'account_reffer'=> 	$member->reffer,
    				'obj_name'		=>  strip_tags($comment),
   				) ;

   $_reffer=save_obj_in_table($tkey,$finfo,array('debug'=>0)) ;
   //print_r($id) ;
   //echo 'id='.$id ;
   $reffer=$_reffer[1].'.'.$_reffer[0] ;
   // регистрируем событие 'Открытие существующей темы'
   $events_system->reg('Добавление комментария к заказу','Сообщение: "'.mb_substr($comment,0,30).'..."',$reffer,$obj_info['_reffer']) ;
 }


 /* почта --------------------------------------------------------------------------------------------------------------------------------------*/

 function cmd_show_clss_82_page_send_mess()
 { global $obj_info,$email_site,$LS_public_site_name ;
   ?>   <h1>Отправка письма на e-mail клиента</h1>
        <input name="send_email_to" type="hidden" value="<?echo $obj_info['email']?>">
        <input name="send_email_from" type="hidden" value="<?echo $email_site?>">
		Куда: <span class="black bold"><?echo $obj_info['email']?></span>&nbsp;&nbsp;&nbsp;Откуда: <span class="black bold"><?echo $email_site?></span><br><br>
        <div class="green bold">Тема письма:</div>
        <input name="send_email_theme" type="text" class=text value="Заказ <?echo $obj_info['obj_name']?>" size=123>
	 	<div class="green bold">Текст сообщения:</div>
	    <textarea name="send_email_message" rows=7 cols=120 wrap="on">Уважаемый(ая) <?echo $obj_info['name']?>!



C уважением, администрация сайта <?echo $LS_public_site_name?>.
	    </textarea><br><br>
	 	<div><button id=upload_button class=button cmd=send_email_message mode=dialog>Отправить сообщение</button></div>
	<?
 }


//-----------------------------------------------------------------------------------------------------------------------------
// Функции - обработчики событий
//-----------------------------------------------------------------------------------------------------------------------------

 function on_change_event_clss_82_status($tkey,$pkey,$fvalue)
 {  global $LS_orders_system_email_alert ;
   // Сохраняем в журнал событий изменение статуса заказа
   $obj_info=select_db_obj_info($tkey,$pkey) ;
   if ($obj_info['status']==$fvalue) return(0) ; // возвращаем, что значение не изменилось
   //damp_array($obj_info) ;
   //echo 'Обрабатываем событие: измерение статуса для pkey='.$pkey.'<br>' ;

    global $events_system ; $evt_id=$events_system->reg('Изменение статуса заказа','Изменен статус заказа '.$obj_info['obj_name'].' с "'.$_SESSION['list_status_orders'][$obj_info['status']].'" на "'.$_SESSION['list_status_orders'][$fvalue].'"',$obj_info['_reffer'],$obj_info['account_reffer'],$obj_info['partner_reff']) ;

    // если оформлен заказ через yandex - отправвляем туда уведомление о смене статуса заказа
    if (isset($obj_info['ya_id']))
    { include_once(_DIR_EXT.'/yandex/i_yandex_api.php') ;
      $yandex_api = new c_yandex_api() ;
      $result=$yandex_api->update_orders_status($obj_info['ya_id'],$obj_info['status'],$fvalue) ;

      if ($result['code']==200) echo '<strong>Сервис "Покупка через маркет":</strong>: <span class=green>'.$result['message'].'</span><br>' ;
      elseif ($result['code'])   echo '<strong>Сервис "Покупка через маркет":</strong>: <span class=red>'.$result['message'].'</span><br>' ;
    }

	global  $mail_system,$LS_public_site_name,$LS_mail_bottom_podpis ;
	$params=array() ;
    $params['site_name']=$LS_public_site_name ;
    $params['name']=$obj_info['name'] ;
    $params['order_name']=$obj_info['obj_name'] ;
    $params['alt_status']=$_SESSION['list_status_orders'][$obj_info['status']] ;
    $params['new_status']=$_SESSION['list_status_orders'][$fvalue] ;
    $params['patch_to_site']=_PATH_TO_SITE.'/' ;
    $params['mail_bottom_podpis']=$LS_mail_bottom_podpis ;

    $mail_system->send_mail_to_pattern($obj_info['email'],'orders_system_info_change_order_status',$params,array('debug'=>0,'use_event_id'=>$evt_id)) ;
    $mail_system->send_mail_to_pattern($LS_orders_system_email_alert,'orders_system_info_change_order_status',$params,array('debug'=>0,'use_event_id'=>$evt_id)) ;

    echo 'Изменение статуса заказа "'.$obj_info['obj_name'].'" зарегистрированно в журнале событий, на адрес <strong>'.$obj_info['email'].'</strong> отправлено уведомление о изменении статуса заказа<br>' ;

    if ($fvalue==4 and $obj_info['partner_premia']>0 and $obj_info['partner_reff']>0)
    {
        $partner_info=get_obj_info($obj_info['partner_reff']) ;
        // отправляем сообщение о начислении премии партнеру сайта
		$params=array() ;
	    $params['name']=$partner_info['obj_name'] ;
	    $params['order_name']=$obj_info['obj_name'] ;
	    $params['prem_summ']=$obj_info['partner_premia'] ;
	    $params['data']=time() ;
	    $params['mail_bottom_podpis']=$LS_mail_bottom_podpis ;

	    $mail_system->send_mail_to_pattern($partner_info['email'],'partners_system_info_premia_added',$params,array('debug'=>0,'use_event_id'=>$evt_id)) ;
      	echo 'В рамках партнерской программы на адрес <strong>'.$partner_info['email'].'</strong> отправлено уведомление о начислении премии за доставленный заказ<br>' ;
    }


    return(1) ; // возвращаем, что значение изменилось

 }

   function on_change_event_clss_82_track($tkey,$pkey,$fvalue)
   {
     echo 'Изменился трек - отправляем почту' ;
     global $mail_system,$mail_bottom_podpis,$descr_obj_tables;
     $order_info=execSQL_van('select * from '.$descr_obj_tables[$tkey]->table_name.' where pkey='.$pkey) ;

     $params['name']=$order_info['name'] ;
     $params['order_name']=$order_info['obj_name'] ; ;
     $params['track']=$fvalue ;

     $params['patch_to_site']=_PATH_TO_SITE.'/' ;
     $params['mail_bottom_podpis']=$mail_bottom_podpis ;

     //$order_info['email']='xrashid@gmail.com' ;
     //$order_info['email']='debug@12-24.ru' ;

     $event_id=_event_reg('Изменение номера трека','Трек: '.$fvalue,$order_info['_reffer']) ;
     $mail_system->send_mail_to_pattern($order_info['email'],'orders_system_info_change_track',$params,array('debug'=>0,'use_event_id'=>$event_id)) ;


     if ($order_info['phone'] and strpos('7',$order_info['phone'])==0)
       { include_once(_DIR_TO_SITE_EXT.'/smsc.ru/send_sms.php') ;
         $order_number='№ '.$order_info['pkey'] ;
         $price=_format_price($order_info['end_price']) ;
         $options['reffer']=$order_info['_reffer'] ;
         send_SMS_by_pattern($order_info['phone'],$_SESSION['LS_sms_on_trace_change'],array('xxx','yyy'),array($order_number,$price),$options) ;
       }

     return(1) ;
   }

?>