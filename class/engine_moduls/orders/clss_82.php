<?
include_once(_DIR_TO_ENGINE.'/class/clss_0.php') ;
class clss_82 extends clss_0
{

    function prepare_public_info(&$rec)
    {   $name=$rec['obj_name'] ;
        $name=trim(strip_tags($name))  ;   // удаляем начальные и конечные пробелы и HTML-теги
        $name=str_replace(array("\n"),array(''),$name) ; // удяляем переносты строк
        if (!$name) $name='# '.$rec['pkey'] ;
        if ($rec['payments_status']==2) $rec['_in_tree_name']='<span class="green bold">'.$name.'</span>' ;
        else                     $rec['_in_tree_name']=$name ;
        if (isset($rec['_child_cnt'])) $rec['_in_tree_child_cnt']=$this->prepare_child_cnt_to_tree($rec['_child_cnt']) ;
        if (isset($this->parent_to) and !sizeof($this->parent_to)) $rec['_in_tree_child_cnt']=0 ;
    }


    function show_props($obj_rec,$options=array())
    {  _CUR_PAGE()->obj_info=$obj_rec ;
       $this->show_order_info() ;
    }


    function show_list_items($table_id,$usl='',$options=array())
    { $options['debug']=0 ;
      $options['show_itog']=array('end_price','payments_amount') ;
      parent::show_list_items($table_id,$usl,$options) ;
    }

   // команда для пункта меню - ифнормация по заказу
   function show_order_info()
   { global $orders_system ;
     $cur_order_id=_CUR_PAGE()->obj_info['pkey'] ;
     $cur_order=ORDERS()->get_order($cur_order_id,array('goods_full_info'=>1)) ;
     $goods=&$cur_order['goods'] ;  // потом передалать все вызовы функций на  $cur_order
     $service=&$cur_order['service'] ;
     ?><table class="order_info_panels item" reffer="<?echo $cur_order['_reffer']?>">
         <tr class=title><td>Информация по заказу</td><td>Информация по покупателю</td></tr>
         <tr><td><?$this->panel_order_info($cur_order,$goods,$service)?></td>
             <td><?$this->panel_order_member($cur_order)?></td>
         </tr>
         <tr class=title><td>Дополнительная информация</td><td>Комментариии к заказу</td></tr>
         <tr><td><?$this->panel_order_dop_info($cur_order,$goods)?></td>
               <td><?$this->show_order_comment($cur_order) ;?></td>
           <?/*<tr><td><?damp_array($cur_order) ;?></td>
               <td><?damp_array($goods) ;?></td>*/?>
         </tr>
         <tr class=title><td colspan=2>Состав заказа</td></tr>
         <tr><td colspan=2><?$this->panel_order_goods($cur_order,$goods,$service)?></td></tr>
         <tr><td colspan=2><?$this->panel_adding_goods($cur_order);
                             $this->panel_change_shipping($cur_order);
                             $this->panel_change_sales($cur_order);

                          ?>
         </td></tr>
         <? // деобходимо доработать панелт поиска
            // чтобы она использовала параметры поиска не из div.list_item -  их может быть несколько
            // а из заранее заданного списка
         ?>
         <tr class=title><td colspan=2>История заказа</td></tr>
         <tr><td colspan=2><?$this->show_order_histroy($cur_order) ;?></td></tr>
       </table>
       <style type="text/css">
         table.order_info_panels{width:100%;}
         table.order_info_panels > tbody > tr > td{vertical-align:top;padding:0;}
         table.order_info_panels > tbody > tr > td:first-child{width:300px;}
         table.order_info_panels > tbody > tr > td:last-child{width:auto;}
         table.order_info_panels tr.title td{vertical-align:middle;text-align:left;font-size:14px;padding:10px;background-color:#edf0f7;color:#1c588e;}

         table.panel_order_info{width:100%;table-layout:fixed;border-collapse:collapse;}
         table.panel_order_info > tbody > tr > td{vertical-align:top;border:none;background-color:#F9F9F7;_background-color:#fafae7;padding:5px;border-bottom:1px solid #E1E1E1;}
         table.panel_order_info > tbody > tr > td:first-child{width:150px;font-style:italic;color:#1c588e;padding-right:10px;vertical-align:middle;text-align:right;}
         table.panel_order_info > tbody > tr > td:last-child{color:black;}
         table.panel_order_info > tbody > tr > td.oplata_sucess{font-size:20px;color:green;}
         table.panel_order_info > tbody > tr:hover td{background-color:#fafae7;}

         table.panel_order_goods{width:100%;margin:0;color:black;}
         table.panel_order_goods tr.td_title td{font-size:11px;color:black;text-align:center;font-weight:bold;background-color:#fafae7;white-space:nowrap;}
         table.panel_order_goods tr.itog1 td{font-size:11px;color:black;text-align:right;background-color:#fafae7;white-space:nowrap;padding-right:10px;}
         table.panel_order_goods tr.itog1 td:first-child{font-style:italic;font-weight:bold;}
         table.panel_order_goods tr.itog1 td:last-child{font-size:14px;}
         table.panel_order_goods tr.itog2 td{font-size:14px;color:black;text-align:right;background-color:#fafae7;white-space:nowrap;padding-right:10px;}
         table.panel_order_goods tr.itog2 td:first-child{font-style:italic;font-weight:bold;}
         table.panel_order_goods tr.itog2 td:last-child{font-size:18px;}
         table.panel_order_goods td.price{white-space:nowrap;font-size:14px;text-align:right;padding-right:10px;}
         table.panel_order_goods td.art{text-align:center;}
         table.panel_order_goods td.cnt{text-align:center;}
         table.panel_order_goods td.cnt input{width:20px;}
         table.panel_order_goods td.sales{text-align:center;}
         table.panel_order_goods td.name{text-align:left;}
         table.panel_order_goods td div.name{font-size:14px;color:black;font-style:italic;}
         table.panel_order_goods td.delete{text-align:center;vertical-align:middle;}
         table.panel_order_goods td.delete div{width:16px;height:16px;background:url('<?echo _PATH_TO_ADMIN_IMG?>/close.png') no-repeat;margin:0 auto;cursor:pointer;}
         table.panel_order_goods td.delete div:hover{background-position:0 -16px;}
         table.panel_order_goods td div.sales_info{font-size:11px;margin:10px 0 10px 0;}
         table.panel_order_goods td div.sales_info span{color:#272727;}
         table.panel_order_goods td span.price_alt{text-decoration:line-through;font-size:10px;white-space:nowrap;}
         table.panel_order_goods td span.price_new{white-space:nowrap;}

         div#panel_adding_goods{margin:10px 0 10px 20px;text-align:left;}
         div#panel_adding_goods input.text{margin-left:10px;}
         div#panel_change_shipping{margin:10px 0 10px 20px;text-align:left;}
         div#panel_change_shipping input.text{margin-left:10px;}
       </style>
     <?
      // damp_array($goods) ;
       //$goods=$orders_system->get_orders_goods($cur_order_id) ;
       //damp_array($goods) ;

   }

   // информация по составу заказа
   function show_order_goods()
   { global $orders_system ;
     $cur_order=_CUR_PAGE()->obj_info ;
   }

  function panel_order_goods($rec_order,$goods_recs,$service_recs)
  { global $TM_goods_image,$picture;
//  damp_array($goods_recs);
  ?>
    <table class=panel_order_goods>
      <tr class=td_title><td>Фото</td><td>Артикул</td><td>Штрих-код</td><td>Наименование</td><td>Наличие</td><td>Цена</td><td>Кол-во</td><td>Скидка</td><td>Сумма</td><td>Удалить</td></tr><?
      $sum_price=0 ;
      $sum_sales=0 ;
      $count_goods=0 ;
      if (sizeof($goods_recs)) foreach($goods_recs as $rec)
      { $cart_rec=$rec['__reffer_from'] ;
        if (sizeof($cart_rec['price_info'])) $price_info=$cart_rec['price_info'] ;
        else //  обеспечиваем совместимость с заказами сделанными в старых версиях движка
        { $price_info['__price_3']=$cart_rec['price'] ;
          $price_info['__price_4']=$cart_rec['price'] ;
          $price_info['__price_5']=_format_price($cart_rec['price']) ;
          $price_info['__price_used_sales_value']=$cart_rec['sales_value'] ;
          $price_info['__price_used_sales']='action' ;
          $price_info['__price_action_sales_text']='' ;
        }
        $count_goods+=$cart_rec['cnt'] ;
        $cur_goods_sales=$price_info['__price_used_sales_value']*$cart_rec['cnt'] ; // скидка по текущему товару
        $cur_goods_price=$price_info['__price_3']*$cart_rec['cnt'] ;                // стоимость товара без скидки
        $cur_goods_end_price=$cur_goods_price-$cur_goods_sales ;                    // стоимость товара со скидками
        $sum_sales+=$cur_goods_sales ;  // суммарная скидка по всем товарам
        $sum_price+=$cur_goods_price ;  // сумма стоимости товара без скидки
        $sort_info=get_obj_info($rec['__reffer_from']['sortament_reffer']) ;
        if ($sort_info['pkey']) $stock_info=execSQL('select pkey,obj_name,cnt from '.TM_SORTAMENT_STOCK.' where parent="'.$sort_info['pkey'].'"') ;?>
        <tr>
          <td><?
           if ($rec['sortament_type']==3)
           { $picture=execSQL_van('select * from '.$TM_goods_image.' where pkey='.$rec['_sortament_rec']['img_id'].'');?>
             <img src="<?echo img_clone($picture,'small')?>" alt="<?echo $rec['img_alt']?>"><?
           }
           else  if (!$rec['_sortament_rec']['_image_name']) {?><img src="<?echo img_clone($rec,'small')?>" alt="<?echo $rec['img_alt']?>"><?}
           else {?><img src="<?echo img_clone($rec['_sortament_rec'],'small')?>" alt="<?echo $rec['img_alt']?>"><?}?>
          </td>
          <td class=art>
            <? if ($rec['_sortament_rec']['art']) echo $rec['_sortament_rec']['art'];
            else if ($rec['art']) echo $rec['art']; else echo "не задан";?>
          </td>
          <td class=barcode><?echo (sizeof($rec['_sortament_rec']))? '<img src="http://'._MAIN_DOMAIN.'/orders/barcode'.$rec['_sortament_rec']['code'].'.png">':'' ;?></td>
          <td class=name>
            <div class=name>
              <strong><?echo $rec['obj_name']?></strong> <br>
<!--              <strong>--><?//echo $rec['icon_obj_name']?><!--</strong> <br>-->
            </div>
            <div class="_comment"><?echo $rec['icon_obj_comment']?></div> <?
            if (isset($rec['_sortament_rec']))
            { ?>
              <div class="sortament_name"><?
                if ($rec['sortament_type']==1 and $rec['_sortament_rec']['obj_name']) echo 'Цвет: <strong>'.$rec['_sortament_rec']['obj_name'].'</strong> ' ;
                if ($rec['sortament_type']==2 and $rec['_sortament_rec']['obj_name']) echo 'Размер: <strong>'.$rec['_sortament_rec']['obj_name'].'</strong> ' ;
                if ($rec['sortament_type']==3 and $rec['_sortament_rec']['obj_name']) echo 'Рисунок: <strong>'.$picture['manual'].'</strong><br> Размер: <strong>'.$rec['_sortament_rec']['obj_name'].'</strong> ' ;
               ?></div><?
            }
            if (is_object($_SESSION['1c_system']) and sizeof($rec['_sortament_rec'])) echo $_SESSION['1c_system']->get_sortament_info($rec['_sortament_rec'],array('delimiter'=>'<br>','show_code'=>1,'show_barcode'=>0)) ;
            //damp_array($rec['_sortament_rec']) ;
            echo '<br><a href="'.$rec['__href'].'" target="_blank">Ссылка на сайт</a>' ;
            if ($price_info['__price_used_sales_value'])
            { switch($price_info['__price_used_sales'])
              { case 'action': $sales_source='Задана в карточке товара' ; break ;
                case 'admin': $sales_source='Администратор через панель управления' ; break ;
                default:       $sales_source='Не удалось определить' ;
              }
              echo '<div class=sales_info>Источник скидки:<span> '.$sales_source.'</span><br>Размер скидки:<span> '.$price_info['__price_action_sales_text'].'</span></div>' ;
            }?>
          </td>
          <td class="stock center"><?
            if ($rec['stock2']==1)
            { ?><storng class="green">в наличии</storng><?
            } else if ($sort_info['pkey']) {?><strong class="red">нет в наличии</strong><?}
            ?>
          </td>
          <td class=price><?  if ($rec['__reffer_from']['price_info']['sales_size']) echo '<div class="sales_info">Цена без скидки: '._format_price($rec['__reffer_from']['price_info']['price_source']).'<br>Размер скидки: '.$rec['__reffer_from']['price_info']['sales_size'].'%</div>' ;
            //damp_array($rec) ;
            echo _format_price($price_info['__price_3'])?></td>
          <td class=cnt>x <input type=text name="cart_cnt[<?echo $rec['__reffer_from']['pkey']?>]" class="small text" value="<?echo $cart_rec['cnt']?>"> шт.</td>
          <td class=sales><?if ($price_info['__price_used_sales_value']) echo _format_price($price_info['__price_used_sales_value']) ; else echo '-';?></td>
          <td class=price><?echo _format_price($cur_goods_end_price)?></td>
          <td class=delete><div class=v2 cmd=del_from_orders script="mod/orders" order_id="<?echo $rec_order['pkey']?>" smeta_id="<?echo $rec['__reffer_from']['pkey']?>"></div></td>

        </tr><?
      }
     //$rec2=$rec ;
     ?><tr class=itog1><td colspan=8>Всего (без скидок)</td><td>= <?echo _format_price($sum_price)?></td><td></td><td></td></tr><?
     if ($sum_sales) {?><tr class=itog1><td colspan=8>Скидка:</td><td><? echo '-'._format_price($sum_sales)?></td><td></td><td></td></tr><?} ;
     $sum_price=$sum_price-$sum_sales ;
       if (sizeof($service_recs)) foreach($service_recs as $rec)
        { ?><tr><td></td>
                <td></td>
                <td></td>
                <td><div class=name><?echo $rec['obj_name'] ?></td>
                <td></td>
                <td class=price><?echo _format_price($rec['price'],array('use_if_null'=>'-'))?></td>
                <td></td>
                <td></td>
                <td class=price><?echo _format_price($rec['price'],array('use_if_null'=>'-'))?></td>
                <td></td>
                <td></td>
          </tr><?
          $sum_price=$sum_price+$rec['price'] ;
       }
       else
         // старый способов вывода доставки и скидки
       { if ($rec_order['price_dost']) {?><tr class=itog1><td colspan=8>Доставка:</td><td><?echo '+'._format_price($rec_order['price_dost'])?></td></tr><?}
         $sum_price=$sum_price+$rec_order['price_dost'] ;
       }
       ?><tr class=itog2><td colspan=8>ИТОГО:</td><td><?echo _format_price($sum_price)?></td><td></td><td></td></tr><?

       ?></table><?
      //damp_array($rec2) ;
  }

  function panel_adding_goods($rec)
  { global $goods_system ;
    ?><div id=panel_adding_goods>Добавить новый товар:<input type="text" class=text name=new_goods_code> (код товар, артикул или штрих-код) <button cmd=add_goods_to_order class="v2" order_id="<?echo $rec['pkey']?>" script="mod/orders">Добавить в заказ</button>&nbsp;&nbsp;&nbsp;&nbsp;<button class="button_green float_right v2"  cmd=update_order script="mod/orders" order_id="<?echo $rec['pkey']?>">Пересчитать заказ</button></div><?
    /*
    $clss=200 ;
    ?><div id=panel_search script_url="<?echo _PATH_TO_ADMIN?>/ajax.php" cmd="list_items_search" clss=<?echo $clss?> tkey=<?echo $goods_system->tkey?> show_van_rec_as_item=0 place_result_div="adding_gooods_select">
          <strong>ПОИСК:</strong>
            <input type=text name=text_search class="text"><img src="<?echo _PATH_TO_ENGINE?>/admin/images/find_icon.gif" class=go_search>
            <input type=radio name=target_search value=obj_name checked>Наименованию
            <input type=radio name=target_search value=art>Артикулу
        </div>
      <div id=adding_gooods_select></div>
    <? */

  }

  function panel_change_shipping($rec_order)
  { ?><div id=panel_change_shipping>Изменить тип доставки:
        <select name=change_shiping><option value=0></option><? foreach($_SESSION['IL_type_shiping'] as $id=>$rec) echo '<option value='.$id.' '.(($rec_order['type_shiping']==$id)? 'selected':'').'>'.$rec['obj_name'].' - '._format_price($rec['price'],array('null_price_text'=>'Бесплатно')).'</option>' ; ?></select>
        &nbsp;&nbsp;&nbsp;задать стоимость <input type="text" class="text" name=price_shiping>
        <button class=v2  order_id="<?echo $rec_order['pkey']?>" script="mod/orders" cmd=change_shipping>Применить</button></div><?
  }

  function panel_change_sales($rec_order)
  { ?><div id=panel_adding_goods>Изменить размер скидки: <input type="text" class=text name=code> штрих-код или артикул   <input type="text" class="text small" name=sales_value> размер скидки, руб <input type="text" class="text small" name=sales_value2> размер скидки, % <button cmd=change_sales_to_goods class="v2" order_id="<?echo $rec_order['pkey']?>" script="mod/orders">Применить к товару</button></div><?
     //$rec_order2=$rec_order['goods'][2] ;
     //damp_array($rec_order2) ;
  }

  function panel_order_info($rec,$goods_recs,$service_recs)
  { //damp_array($rec) ;
    ?><table class=panel_order_info>
        <tr><td>Номер заказа</td><td><?echo $rec['obj_name']?></td></tr>
        <tr><td>Тип доставки</td><td><?echo $service_recs['shiping']['obj_name']?></td></tr>
        <tr><td>Стоимость доставки</td><td><?echo _format_price($service_recs['shiping']['price'])?></td></tr>
        <tr><td>Число товаров в заказе</td><td><?echo sizeof($goods_recs)?></td></tr>
        <!--<tr><td>Сумма к оплате</td><td><? echo _format_price($rec['end_price'])  ?></td></tr>-->
        <?   generate_tr_td_field($rec,'end_price','Сумма к оплате',array()) ;

            $options=array() ;
            if ($rec['payments_amount']) $options['read_only']=1 ;
            generate_tr_td_field($rec,'payments_type','Тип оплаты',$options) ;
        ?>
        <tr><td>Состояние оплаты</td><td><?echo $_SESSION['ARR_orders_payments'][$rec['payments_status']]?></td></tr>
        <tr><td>Оплаченная сумма</td><td class=oplata_sucess><?echo _format_price($rec['payments_amount'])?></td></tr>
        <?generate_tr_td_field($rec,'track','Трек') ;?>
        <?generate_tr_td_field($rec,'accepted','Разрешить оплату заказа') ;?>
      </table>
    <?
    //damp_array($rec) ;
  }

  function panel_order_member($rec)
  {
    ?><table class=panel_order_info><?
      generate_tr_td_field($rec,'member_typ','Тип клиента') ;
      generate_tr_td_field($rec,'name','Имя') ;
      generate_tr_td_field($rec,'phone','Телефон') ;
      generate_tr_td_field($rec,'phone2','Доп.телефон') ;
      generate_tr_td_field($rec,'email','Email') ;
      generate_tr_td_field($rec,'city','Город') ;
      generate_tr_td_field($rec,'adres','Адрес доставки') ;
      generate_tr_td_field($rec,'rekvezit','Реквизиты') ;
      generate_tr_td_field($rec,'comment','Комментарий покупателя<br>к заказу') ;
      ?><tr><td>Загруженные файлы</td><td>
        <?  //damp_array($rec) ;
            if (sizeof($rec['files']))
            { ?><ul><?
              foreach($rec['files'] as $rec_file) {?><li><a href="<?echo $rec_file['__href']?>" target=_blank><?echo $rec_file['obj_name']?></a> (<?echo format_size($rec_file['size'])?>)</li><?}
              ?></ul><?
            }

        ?>
      </td></tr><?
    ?></table>
      <button cmd=save_list>Сохранить</button>
      <button class=v2 cmd=get_panel_create_comment script=mod/orders reffer="<?echo $rec['_reffer']?>">Добавить комментарий</button><?

      ?><a href="http://<?echo _MAIN_DOMAIN?>/orders/pdf/<?echo $rec['uid']?>.pdf" target="_blank">Версия для печати</a><?
      echo '<br>' ;
      echo '<br>' ;
  }

  function panel_order_dop_info($rec)
  { ?><table class=panel_order_info>
        <tr><td>Подписка</td><td><?echo $rec['subscript']?></td></tr>
        <?if ($rec['info']['source_direct']){?><tr><td>SOURCE_DIRECT</td><td><?echo $rec['info']['source_direct']?></td></tr><?}?>
        <tr><td>Откуда узнали про сайт</td><td><?echo $rec['what']?></td></tr>
        <tr><td>Откуда переход на сайт</td><td><?echo $rec['referer_host']?></td></tr>
        <tr><td>Ссылка для оплаты заказа</td><td><textarea rows="4" cols="24" readonly="1">http://<? echo _MAIN_DOMAIN ?>/orders/payments/<? echo $rec['uid']?></textarea></td></tr>
      </table>
    <?
  }

  function show_order_histroy($rec)
  {
      show_history('links like "%'.$rec['_reffer'].'%" or reffer="'.$rec['_reffer'].'"') ;
  }

  function show_order_comment($rec)
  {
      _CLSS(15)->show_list_items($_SESSION['TM_orders_comment'],'parent='.$rec['pkey'],array('debug'=>0,'default_order'=>'c_data','read_only'=>1,'no_check'=>1,'no_icon_edit'=>1,'no_table_header'=>1,'all_enabled'=>1,'no_htmlspecialchars'=>1,'buttons'=>array())) ;

  }

 function on_change_event_payments_type($tkey,$pkey,&$fvalue)
 {  _event_reg('Изменение способа оплаты',$_SESSION['IL_payments_type'][$fvalue]['admin'],$pkey.'.'.$tkey) ;
    return(true) ;
 }


 // отправляем уведомление клиенту
 function show_email()
 { global $TM_mails ;
   $list_templates=execSQL('select * from '.$TM_mails.' where kod like "orders_system%"') ;
   ?>Выберите шаблон письма: <select name="template_id"><?
   if (sizeof($list_templates)) foreach($list_templates as $rec) {?><option value="<?echo $rec['pkey']?>"><? echo $rec['obj_name']?></option><?}
   ?></select><button cmd=show_email_template>Подготовить письмо</button><br><?
 }

 function show_email_template()
 { global $TM_mails,$obj_info,$mail_system,$goods_system,$orders_system ;
   $rec_template=execSQL_van('select * from '.$TM_mails.' where pkey='.$_POST['template_id']) ;
   if (!$rec_template['pkey']) {?>Не найден шаблон письма<? return ;}
   ?><h1>Готовим письмо на основе шаблона "<?echo $rec_template['obj_name']?>"</h1><?
   //damp_array($obj_info) ;
   //---------------------------------------------------------------------------------
   $order_info=$orders_system->get_order($obj_info['pkey'],array('goods_full_info'=>1)) ;
   $order_goods=&$order_info['goods'] ;
   $order_service=&$order_info['service'] ;

   $params=array() ;
     $params['site_name']=$_SESSION['LS_public_site_name'] ;
     $params['patch_to_site']='http://'._MAIN_DOMAIN ;
        $params['data']=date("d.m.y G:i") ;
        $params['name']=$order_info['name'] ;
        $params['track']=$obj_info['track'] ;
        $params['order_uid_url']='http://'._MAIN_DOMAIN.'/orders/payments/'.$order_info['uid'] ;
        $params['order_name']=$order_info['obj_name'] ;
        $status_usl='http://'._MAIN_DOMAIN.'/orders/status/'.$order_info['uid'] ;
        $params['status_info']='<a href="'.$status_usl.'">'.$status_usl.'</a>' ;
        $params['order_price']=($order_info['end_price'])? 'Сумма к оплате: '.$order_info['end_price'].$goods_system->VL_main_ue:'---' ;
   if ($orders_system->include_price_shiping_to_order and sizeof($order_service['shiping'])) $params['order_price'].=', в том числе доставка "'.$order_service['shiping']['obj_name'].'" - '._format_price($order_service['shiping']['price'],array('null_price_text'=>'Бесплатно')) ;

   $params['order_typ_oplaty']=(isset($order_info['payments_type']))? 'Тип оплаты: '.$_SESSION['IL_payments_type'][$order_info['payments_type']]['obj_name']:'' ;

   if ($order_info['payments_method']=='robox') $params['order_typ_oplaty'].='. Если Вы не оплатили свой заказ сразу, для оплаты перейдите по этой <a href="http://'._MAIN_DOMAIN.'/orders/payments/'.$order_info['uid'].'">ссылке</a>' ;
         //damp_array($order_info) ;
         //damp_array($options) ;
        //if ($order_info['kupon_number']) list($sales_summa,$sales_kupon,$alert)=$orders_system->check_kupon($order_info['kupon_number'],$_POST['kupon']['pin'],$use_cart->end_price) ;

   if (sizeof($order_goods))
         { $_temp=array() ;
     	  $_temp[]='<tr class=header><td>Артикул</td><td>Фото</td><td>Наименование</td><td>Цена</td><td>Количество</td><td>Скидка</td><td>Стоимость</td></tr>' ;
     foreach ($order_goods as $rec)
      {  // переносим информацию по ценам за запись в товару
         $cart_goods_info=array_merge($rec['__reffer_from'],$rec['__reffer_from']['price_info']) ; unset($cart_goods_info['price_info']) ; //damp_array($cart_goods_info) ;
         $price_info=$orders_system->prepare_price_info($cart_goods_info) ;// damp_array($price_info) ;
           	 $href=(strpos($rec['__href'],'http://')!==0)? _PATH_TO_SITE.$rec['__href']:$rec['__href'] ;
         $name=($rec['__href'])? '<a href="'.$href.'">'.$cart_goods_info['obj_name'].'</a>':$cart_goods_info['obj_name'] ;
              $_temp[]='<tr><td>'.$rec['__art'].'</td>'.
                            '<td class=left><img src="'.img_clone($rec,'small').'" alt=""></td>'.
                        	   '<td class=left>'.$name.'</td>'.
                  '<td align=right>'.$price_info['price'].'</td>'.
                            '<td align=center>'.$price_info['cnt'].'</td>'.
                            '<td>'.$price_info['sales_title'].'<br>'.$price_info['sales_text'].'</td>'.
                            '<td align=right>'.$price_info['sum_price'].'</td>'.
                        '</tr>' ;
           }

      if (sizeof($order_service)) foreach($order_service as $rec) $_temp[]='<tr><td></td><td></td><td><div class=name>'.$rec['obj_name'].'</td><td align=right>'._format_price($rec['price'],array('use_if_null'=>'-')).'</td><td></td><td></td><td align=right>'._format_price($rec['price'],array('use_if_null'=>'-')).'</td></tr>' ;
      //if ($order_info['kupon_sales']>0) $_temp[]='<tr><td colspan=2>Скидка по купону № '.$order_info['kupon_number'].'</td><td align=left></td><td>&nbsp;</td><td>&nbsp;</td><td  align=right>'._format_price($order_info['kupon_sales']).'</td></tr>' ;
      $_temp[]='<tr valign=top><td colspan=6>ИТОГО</td><td align=right><strong>'._format_price($order_info['end_price']).'</strong></td></tr>' ;
           $params['order_goods']='<table>'.implode('',$_temp).'</table>' ;
         } else $params['order_goods']="" ;

        $_temp=array() ;
        									$_temp[]='<tr class=header><td colspan=2>Информация по покупателю</td></tr>' ;
        if ($order_info['member_typ']) 	$_temp[]='<tr><td>Получатель заказа</td><td>'.$order_info['member_typ'].'</td></tr>' ;
     								   	$_temp[]='<tr><td>Имя</td><td>'.$order_info['name'].'</td></tr>' ;
     								   	$_temp[]='<tr><td>Телефон для связи с кодом города</td><td>'.$order_info['phone'].'</td></tr>' ;
     								   	$_temp[]='<tr><td>E-mail</td><td>'.$order_info['email'].'</td></tr>' ;
     								   	$_temp[]='<tr><td>Адрес доставки</td><td>'.$order_info['adres'].'</td></tr>' ;
        if ($order_info['rekvezit']) 	$_temp[]='<tr><td>Реквизиты</td><td>'.$order_info['rekvezit'].'</td></tr>' ;
        									$_temp[]='<tr><td>Примечание</td><td>'.$order_info['comment'].'</td></tr>' ;
        //if ($order_info['kupon_number']) $_temp[]='<tr><td>Номер купона</td><td>'.$order_info['kupon_number'].'</td></tr>' ;
        //if ($order_info['kupon_pin'])    $_temp[]='<tr><td>PIN купона</td><td>'.$order_info['kupon_pin'].'</td></tr>' ;
        //if ($order_info['kupon_sales'])  $_temp[]='<tr><td>Скидка по купону</td><td>'.$order_info['kupon_sales'].'</td></tr>' ;

        $params['member_info']='<table>'.implode('',$_temp).'</table>' ;
   //---------------------------------------------------------------------------
     list($email_theme,$email_text)=$mail_system->get_mail_to_pattern($rec_template['kod'],$params) ;
     if ($rec_template['is_HTML']!=1) $email_text=nl2br($email_text) ;

   ?><h2>Email получателя:</h2><input type="text" class=text name=mail_adres value="<?echo $obj_info['email']?>"><h2>Тема письма</h2><input class="text big" type=text name=mail_theme value="<?echo $email_theme?>"><h2>Содержание письма</h2><?
   show_window_ckeditor_v3(array(),'',300,array('use_element_name'=>'mail_value','content'=>$email_text)) ;
   ?><br><button cmd=show_email_template_send>Отправить письмо</button><br><?
 }

 function show_email_template_send()
 { global $obj_info ;
   ?><h1>Отправляем письмо</h1><?
     _send_mail($_POST['mail_adres'],$_POST['mail_theme'],$_POST['mail_value'],array('debug'=>1,'reg_events'=>1,'events_reffer_obj'=>$obj_info['_reffer'])) ;
 }
}
include_once(_DIR_TO_CLSS.'/clss_100.php') ;
class clss_120 extends clss_100
{

}

?>