<?php
include_once(_DIR_TO_ENGINE."/admin/c_site.php") ;
include_once(_DIR_TO_ENGINE."/admin/i_clss_func.php") ;
include_once(_DIR_TO_ENGINE.'/admin/c_menu_tabs.php') ;

// редактор заказов
class c_editor_orders extends c_editor_obj
{
  function body($options=array()) { $this->body_frame_2x(array('fra_tree'=>_PATH_TO_ADMIN.'/editor_orders_tree.php','title'=>$options['title'])) ; }
}


class c_editor_orders_tree extends c_fra_tree
{ public $system_name='orders_system' ;

  function body($options=array())
 {?><body id=tree_objects><div id=tree_menu><?$this->top_menu() ;?></div><?
     $this->generate_time_tree('Заказы',$this->tkey,array('usl_select'=>'clss=82','on_click_script'=>'editor_orders_viewer.php','on_click_item'=>array(82=>'fra_viewer.php')));
  ?></body><?
  }

}

//-----------------------------------------------------------------------------------------------------------------------------
// ЗАКАЗЫ
//-----------------------------------------------------------------------------------------------------------------------------

class c_editor_orders_viewer extends c_fra_based
{ public $system_name='orders_system' ;
  public $top_menu_name='top_menu_list_orders' ;

 function body(&$options=array()) {$this->body_frame($options) ; }

// заголовок фрейма
 function show_page_obj_header()
 { $title=($_GET['pkey']=='root')? 'Заказы':'Заказы '.$this->get_time_title($this->start_time,$this->end_time) ;
   ?><p class='obj_header'><?echo $title?></p><?
 }

 // меню фрейма
 function show_page_obj_menu($obj_info)
    {  // создаем объект top_menu
       if (!is_object($_SESSION[$this->top_menu_name])) $_SESSION[$this->top_menu_name]=new c_top_menu() ;
       // формируем набор пунктом меню
       $menu_set=array() ;
       // пункты меню для корня дерева
       if ($_GET['pkey']=='root')
       { $menu_set[]=array("name" => 'Поиск по заказам','cmd' =>'show_list_items','mode'=>'last_items','count'=>20) ;
         $menu_set['stats']=array("name" => 'Стастистика',"cmd" =>'show_stats') ;
       }
       // или если выбран один из временных интервалов
       else
       { $time_usl=$this->get_time_usl($this->start_time,$this->end_time) ;
         $arr_status=$_SESSION['orders_system']->get_count_by_status($time_usl) ;
         /// !!! внимание - переделать, чтобы были вкладки "ТИПЫ ОПЛАТЫ"
         $arr_oplata=$_SESSION['orders_system']->get_count_by_field('payments_method',$time_usl) ;
         if (sizeof($arr_status)) foreach($arr_status as $status=>$cnt) {$menu_set[]=array("name" => $_SESSION['list_status_orders'][$status].'('.$cnt.')','cmd' =>'show_list_items','status'=>$status) ; }
         if ($arr_oplata['robox']) $menu_set[]=array("name" => 'Robox ('.$arr_oplata['robox'].')',"cmd" =>'show_list_items','robox'=>1) ;
         $arr_oplata=$_SESSION['orders_system']->get_count_by_field('payments_status',$time_usl) ;
         if ($arr_oplata[2]) $menu_set[]=array("name" => 'Оплаченные ('.$arr_oplata[2].')',"cmd" =>'show_list_items','payments_status_2'=>1) ;

         global $TM_orders ;
         $market_cnt=execSQL_value('select count(pkey) from '.$TM_orders.' where ya_id>0 and '.$time_usl);
         if ($market_cnt) $menu_set[]=array("name" => 'Маркет ('.$market_cnt.')',"cmd" =>'show_list_items','market'=>1) ;

         $menu_set[]=array("name" => 'Все ('.array_sum($arr_status).')',"cmd" =>'show_list_items') ;
         $menu_set['stats']=array("name" => 'Стастистика',"cmd" =>'show_stats') ;
       }
       // выводим меню
       $this->cur_menu_item=$_SESSION[$this->top_menu_name]->show($menu_set) ;  // показываем меню
       return($this->cur_menu_item) ;
    }

  function show_list_items()
  { $options=array() ;
    if ($this->start_time or $this->end_time)  $_usl[]=$this->get_time_usl($this->start_time,$this->end_time) ;
    if (isset($this->cur_menu_item['status'])) $_usl[]='status='.$this->cur_menu_item['status'] ;
    if (isset($this->cur_menu_item['robox']))  $_usl[]='payments_method=\'robox\'' ;
    if (isset($this->cur_menu_item['payments_status_2']))  $_usl[]='payments_status=2' ;
    if (isset($this->cur_menu_item['market']))  $_usl[]='ya_id>0' ;
    if (sizeof($_usl)) $usl=implode(' and ',$_usl) ;
    $title='Заказы '.$this->get_time_title($this->start_time,$this->end_time) ;
    if (isset($this->cur_menu_item['status'])) $title.=' в состоянии "'.$_SESSION['list_status_orders'][$this->cur_menu_item['status']].'"' ;
    if (isset($this->cur_menu_item['robox'])) $title.=' способ оплаты - через Robox' ;
    if (isset($this->cur_menu_item['payments_status_2'])) $title.=' оплаченные' ;
    if (isset($this->cur_menu_item['market'])) $title.=' оформленные через Yandex Маркет' ;
    if ($this->cur_menu_item['mode']=='last_items')
    { $options['count']=$this->cur_menu_item['count'] ;
      $options['order']='c_data desc' ;
      $title.=' - последние '.$this->cur_menu_item['count'] ;
    }
    $options['default_order']='c_data desc' ;
    $options['title']=$title ;
    $options['buttons']=array('save','delete') ;
    _CLSS(82)->show_list_items($this->tkey,$usl,$options) ;
  }

 function show_stats()
  { $options=array() ;
    if ($this->start_time or $this->end_time)  $_usl[]=$this->get_time_usl($this->start_time,$this->end_time) ;
    if (isset($this->cur_menu_item['status'])) $_usl[]='status='.$this->cur_menu_item['status'] ;
    if (isset($this->cur_menu_item['robox']))  $_usl[]='payments_type=4' ;
    $usl=implode(' and ',$_usl) ;
    $title='Заказы '.$this->get_time_title($this->start_time,$this->end_time) ;
    if (isset($this->cur_menu_item['status'])) $title.=' в состоянии "'.$_SESSION['list_status_orders'][$this->cur_menu_item['status']].'"' ;
    if (isset($this->cur_menu_item['robox'])) $title.=' оплаченные через Robox' ;
    if ($this->cur_menu_item['mode']=='last_items')
    { $options['count']=$this->cur_menu_item['count'] ;
      $options['order']='c_data desc' ;
      $title.=' - последние '.$this->cur_menu_item['count'] ;
    }
    $options['default_order']='c_data desc' ;
    $options['title']=$title ;
    $options['buttons']=array('save','delete') ;
    if ($usl) $res_usl=$usl.' and reffer like "%.56"' ;
    else      $res_usl='reffer like "%.56"' ;
    //echo 'usl='.$usl.'<br>' ;
    //damp_array($options) ;
    global $TM_orders_goods,$goods_system,$TM_orders ;



    $recs=execSQL('select reffer,count(pkey) as cnt from '.$TM_orders_goods.' where '.$res_usl.' group by reffer order by cnt desc') ;
    $result=array() ; $i=1 ; $sum_price=array() ;
    if (sizeof($recs)) foreach($recs as $rec)
    { $rec_goods=select_db_ref_info($rec['reffer']) ;
      $goods_system->prepare_public_info($rec_goods) ;
      if (isset($goods_system->tree[$rec_goods['parent']]))
      { $section_id=$rec_goods['parent'] ;
        $result[$section_id][$i]['Наименование']='<a href="'.$rec_goods['__href'].'" target=_blank>'.$rec_goods['obj_name'].'</a>' ;
        $result[$section_id][$i]['Продано']=$rec['cnt'] ;
        if ($sum_price[$section_id]) $sum_price[$section_id]+=$rec_goods['price'] ;
        else                         $sum_price[$section_id]=$rec_goods['price'] ; ;
      }
      $i++ ;
    }
    if (sizeof($result[1])) echo 'Средняя стоимость букета: <stong>'._format_price($sum_price[1]/sizeof($result[1])).'</strong>' ;
    //$order_info=execSQL_value('select avg(price) from '.$TM_orders.' where '.$usl) ;
    //echo 'Средняя стоимость заказа: <stong>'._format_price($order_info).'</strong>' ;


    if (sizeof($result)) foreach($result as $section_id=>$stats_recs)
    { ?><h1><?echo $goods_system->tree[$section_id]->name?></h1><?
      print_2x_arr($stats_recs) ;
    }
    //_CLSS(82)->show_list_items($this->tkey,$usl,$options) ;
  }

  function panel_fast_search()
    { ?><div id=panel_search script_url="<?echo _PATH_TO_ADMIN?>/editor_orders_ajax.php" cmd="get_list_orders">
            <strong>ПОИСК:</strong>
            <input type=text name=text_search class="text"><img src="<?echo _PATH_TO_ENGINE?>/admin/images/find_icon.gif" class=go_search>
            <input type=radio name=target_search value=number>По номеру, дате
            <input type=radio name=target_search value=client checked>По клиенту
            <input type=radio name=target_search value=adres>По адресу
            <input type=radio name=target_search value=rekv>По реквизитам
            <!--<input type=radio name=target_search value=goods>По товару-->
        </div>
     <?
    }
}

class c_editor_orders_viewer2 extends c_site
{

 function body(&$options=array()) {$this->body_frame($options) ; }

 function select_obj_info()
  { global $descr_obj_tables,$TM_orders,$tkey,$start_time,$end_time,$pkey,$obj_info,$cmd,$obj_info,$start_time ;
    //echo 'tkey='.$tkey.'<br>' ; echo 'pkey='.$pkey.'<br>' ; echo 'start_time='.$start_time.'<br>' ; echo 'end_time='.$end_time.'<br>' ;


    if (!$tkey) $tkey=$_SESSION['pkey_by_table'][$TM_orders] ;
    $table_name=$descr_obj_tables[$tkey]->table_name ;
    if (!$table_name) { echo '<p class=alert>Не найдена таблица заказов.</p>' ; _exit() ; }

    if ($cmd and function_exists('cmd_exe_'.$cmd)) { $func_cmd='cmd_exe_'.$cmd ; $func_cmd() ; }

    if ($pkey=='root') unset($pkey) ;

    if ($pkey and !$start_time) $obj_info=select_db_obj_info($tkey,$pkey,array('debug'=>0)) ;
    //damp_array($obj_info) ; echo '<br>' ;

    global $list_orders_top_menu ;  if (!isset($list_orders_top_menu)) 	{$list_orders_top_menu=new c_top_menu() ; session_register('list_orders_top_menu') ; }
    global $orders_stat_menu ; 		if (!isset($orders_stat_menu)) 		{$orders_stat_menu=new c_top_menu() ; session_register('orders_stat_menu') ; }
    global $order_menu ; 			if (!isset($order_menu))  			{$order_menu=new c_top_menu() ; session_register('order_menu') ; }
  }

 // $menu_state[tkey][clss] - текущая страница/поле/класс для таблица/класс

 function block_main()
    { global $obj_info,$descr_obj_tables,$tkey,$list_orders_top_menu,$orders_stat_menu,$menu_action,$cmd,$show_status ;
      global $start_time,$end_time,$func_dialog,$time_usl,$status_usl ;
      //echo 'start_time='.$start_time.'<br>' ;
      //echo 'end_time='.$end_time.'<br>' ;
      //echo 'show_status='.$show_status.'<br>' ;

      $first_data=getdate($start_time) ;
      $last_data=getdate($end_time) ;

      if ($first_data['mday']==$last_data['mday'] and $first_data['mon']==$last_data['mon'] and $first_data['year']==$last_data['year']) $title=' на '.date('d.m.y',$start_time) ;
        else $title=' с '.date('d.m.y',$start_time).' по '.date('d.m.y',$end_time) ;

      if ($start_time and $end_time) $time_usl='and c_data>='.$start_time.' and c_data<='.$end_time ; else $title='Заказы' ;

      //echo 'cmd='.$cmd.'<br>' ;

 	   ?><form name="form" method="post" ENCTYPE="multipart/form-data">
		 <input name="space" type="hidden" value="765" />
	     <input name="cmd"  type="hidden" value="" id='cmd' />
	     <input name="func_dialog"  type="hidden" value=""  id='func_dialog' />

        <?if ($cmd=='orders_info') // заголовк и меню для статистики
          {
	      	?><p class='obj_header'>Статистика <? echo $title ?></p><?
	      	$name1=(isset($show_status) and $show_status!=777)? $_SESSION['list_status_orders'][$show_status]:'Все заказы' ;
            $menu_set[1]=array("name" => 'Товары в "'.$name1.'"',"action" => 1001) ;
            $menu_set[3]=array("name" => 'Статистика по заказам "'.$name1.'"',"action" => 1003) ;
            $orders_stat_menu->show($obj_info,array('menu_set'=>$menu_set)) ;
	      }
	      else // заголовок и меню для группы заказов
          { $stats=execSQL('select status,count(pkey) as cnt from '.$descr_obj_tables[$tkey]->table_name.' where clss=82 '.$time_usl.' group by status',0,0) ;
          	$stats_all=execSQL_van('select count(pkey) as cnt from '.$descr_obj_tables[$tkey]->table_name.' where clss=82 '.$time_usl,0,0) ;
          	if (sizeof($stats)) foreach($stats as $rec) {$menu_set[$rec['status']]=array("name" => $_SESSION['list_status_orders'][$rec['status']].'('.$rec['cnt'].')',"action" => $rec['status']) ; }
          	$menu_set[777]=array("name" => 'Все ('.$stats_all['cnt'].')',"action" => 777) ;
		    ?><p class='obj_header'>Заказы <? echo $title ?></p><?
           	$list_orders_top_menu->show($obj_info,array('menu_set'=>$menu_set)) ;
	      }

	    if (!$menu_action) $menu_action=0 ;
	    if (!isset($show_status)) $show_status=$menu_action ;
	    if ((isset($show_status) and $show_status!=777))
	    { $status_usl='and status='.$show_status ;
	      $status_title=' "'.$_SESSION['list_status_orders'][$show_status].'" ' ;
	    }
	    else { $status_usl='' ; $status_title='' ; }

	    //echo '$status_usl='.$status_usl.'<br>' ;
        //echo '$show_status='.$show_status.'<br>' ;
        //echo '$menu_action='.$menu_action.'<br>' ;

	    // отрабатываем команду диалогового режима
	    if ($func_dialog and function_exists($func_dialog)) $res_dialog=$func_dialog() ; else $res_dialog='ok' ;
        if ($res_dialog!='ok') return ;

	    switch ($menu_action)
	    {   // меню "Статистика/Товары"
            case 1001:  $status_usl=(isset($show_status) and $show_status!=777)? 'and status='.$show_status:'' ;
             			$list_orders=execSQL('select *,'.$tkey.' as tkey from '.$descr_obj_tables[$tkey]->table_name.' where clss=82 '.$time_usl.' '.$status_usl.' order by c_data desc') ;
                        if (sizeof($list_orders))
                        { $str_pkeys=implode(',',array_keys($list_orders)) ;
                          global $TM_orders_goods ;
                          $usl_select='parent in ('.$str_pkeys.')' ;
                          $objs_info=select_reffers_objs($_SESSION['pkey_by_table'][$TM_orders_goods],$usl_select) ;
   						  ?><a href="javascript:print();">Распечатать</a><br><br><?
   						  cmd_show_list_objects($objs_info,array('read_only'=>1)) ;
   						}
   						else echo 'Заказов в данный период не найдено' ;
              			break;
            // меню "Статистика/Статистика по заказам"
            case 1003:  $status_usl=(isset($show_status) and $show_status!=777)? 'and status='.$show_status:'' ;
             			$list_orders=execSQL_van('select count(pkey) as cnt, sum(end_price) as summ from '.$descr_obj_tables[$tkey]->table_name.' where clss=82 '.$time_usl.' '.$status_usl.' order by c_data desc') ;
                        global $goods_system ;
                        echo 'Всего произведенно '.$list_orders['cnt'].' заказов на общую сумму '.$goods_system->format_price($list_orders['summ']) ;
                        //if (sizeof($list_orders))
                        break ;
            default:	?><div id="viewer_main"><?
			            $list_orders=execSQL('select *,'.$tkey.' as tkey from '.$descr_obj_tables[$tkey]->table_name.' where clss=82 '.$time_usl.' '.$status_usl.' order by c_data desc') ;
				        set_reffer_values($list_orders,82) ;
				        _CLSS(82)->show_list_items($list_orders,array('no_check'=>0,'onclick_func_name'=>'set_order_to_view','no_page_panel'=>1,'title'=>'Заказы '.$status_title.$title)) ;
				        if (sizeof($list_orders))
			             { ?><button class=button cmd=save_list>Сохранить</button><?
                           ?><button onclick="if (confirm('Вы действительно хотите удалить выбранные объекты?')) exe_cmd('delete_check');else return false;">Удалить</button><?
			               if ($menu_action==6) {?><button class=button cmd=clear_delete_orders mode=dialog>Очистить от удаленных заказов</button><?}
			             }
			         	?></div><?
	      				/*?><script>parent.fra_order.location="<?echo _PATH_TO_ADMIN?>/editor_orders_viewer.php?cmd=orders_info&tkey=<?echo $tkey?>&show_status=<?echo $menu_action?>&start_time=<?echo $start_time?>&end_time=<?echo $end_time?>";</script><?*/
         }

        ?></form>
        <script type="text/javascript">
		  function set_order_to_view(tkey,pkey)
		  {  var rn=Math.random();
		     if (parent.fra_order!=null)
		       parent.fra_order.location=patch_to_admin+"fra_viewer.php?tkey="+tkey+'&pkey='+pkey+"&rn="+rn
		  }
		</script>
        <?
    }

}

  function clear_delete_orders()
 {  global $tkey,$descr_obj_tables,$time_usl,$status_usl ;
 	//echo '$status_usl='.$status_usl.'<br>' ;
 	$list_orders=execSQL('select *,'.$tkey.' as tkey from '.$descr_obj_tables[$tkey]->table_name.' where clss=82 '.$time_usl.' '.$status_usl.' order by c_data desc') ;
 	if (!sizeof($list_orders)) return ;
 	?><h1>Окончательно удаляем все заказы в статусе "Удален"</h1>
 	Эти заказы будут удалены окончательно, без возможности восстаноления:<br><br>
 	<? foreach($list_orders as $rec) echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Заказ '.$rec['obj_name'].', статус - "<strong>'.$_SESSION['list_status_orders'][$rec['status']].'</strong>"<br>' ;?>
 	<br>
 	<button onclick="exe_cmd('')">Не, ну нах..</button>
    <button class=button cmd=apply_clear_delete_orders mode=dialog>А мне пох...</button><?
 }

 function apply_clear_delete_orders()
 {  global $tkey,$descr_obj_tables,$time_usl,$status_usl,$events_system ;
 	$table_name=$descr_obj_tables[$tkey]->table_name ;
 	$table_name_goods=$descr_obj_tables[$descr_obj_tables[$tkey]->list_clss[120]]->table_name ;
 	$table_name_commnent=$descr_obj_tables[$descr_obj_tables[$tkey]->list_clss[15]]->table_name ;

 	$list_orders=execSQL('select *,'.$tkey.' as tkey from '.$table_name.' where clss=82 '.$time_usl.' '.$status_usl.' order by c_data desc') ;
 	if (!sizeof($list_orders)) return ;
 	foreach($list_orders as $rec)
 	{ echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Удаляем заказ '.$rec['obj_name'].', статус - "<strong>'.$_SESSION['list_status_orders'][$rec['status']].'</strong>"<br>' ;

 	  $sql='delete from '.$table_name.' where pkey='.$rec['pkey'] ;
 	  $sql_goods='delete from '.$table_name_goods.' where parent='.$rec['pkey'] ;
 	  $sql_comment='delete from '.$table_name_commnent.' where parent='.$rec['pkey'] ;

 	  //echo 'sql='.$sql.'<br>' ;
 	  //echo 'sql='.$sql_goods.'<br>' ;
 	  //echo 'sql='.$sql_comment.'<br>' ;
 	  if (!$result = mysql_query($sql)) SQL_error_message($sql) ;
 	  if (!$result = mysql_query($sql_goods)) SQL_error_message($sql_goods) ;
 	  if (!$result = mysql_query($sql_comment)) SQL_error_message($sql_comment) ;
      // регистрируем событие 'Удаление заказа из БД'
      $events_system->register(array('obj_name'=>'Удаление заказа из БД','reffer'=>'','comment'=>'Заказ '.$rec['obj_name'].', статус - <strong>'.$_SESSION['list_status_orders'][$rec['status']].'</strong>')) ;

 	}

    ?><br><?
    return('ok') ;
 }







?>