<?php
include_once (_DIR_TO_ENGINE.'/c_page_XML_AJAX.php') ;
include_once (_DIR_TO_ENGINE.'/admin/i_XML_import_export.php') ;
include_once (_DIR_TO_ENGINE.'/admin/i_admin_system.php') ;
class c_editor_category_ajax extends c_page_XML_AJAX
{





  // подгрузка содержимого дерева
  // $_POST['reffer'] - родитель
  // $_POST['clss'] - класс родителя
  function upload_types_items()
  { //list($pkey,$tkey)=explode('.',$_POST['reffer']) ;
    //$arr_child_clss=_CLSS($_POST['clss'])->get_adding_clss($tkey) ;
    //print_r($arr_child_clss) ;

   //$options['use_page_view_sort_setting']=0 ; // используем настройки сортировки редактора объектов, если они таб быти определееы
   //damp_array($_POST) ;
   //--------------------------------------------------------------------------------------------------------------------------------------------------
   // если передан reffer объекта, значит надо подгрузить дочерние объекты
   //--------------------------------------------------------------------------------------------------------------------------------------------------
   if ($_POST['reffer'])
   {  global $goods_system,$TM_goods;
      $debug=0 ;
      // сначала ищем по линкам. Могут быть слинкованы как разделы, так и бренды, коллекции, товары.
      list($pkey,$tkey)=explode('.',$_POST['reffer']) ;
      $recs=execSQL('select t.pkey as id,t.o_id,t.o_tkey,t.o_clss,t.p_id,t.p_tkey,t.p_clss,tg1.obj_name as g_name1,tg2.obj_name as g_name2,tc1.obj_name as c_name1,tc2.obj_name as c_name2 from '.TM_LINK.' t
                     left join obj_site_goods tg1 on tg1.pkey=t.o_id
                     left join obj_site_goods tg2 on tg2.pkey=t.p_id
                     left join obj_site_category tc1 on tc1.pkey=t.o_id
                     left join obj_site_category tc2 on tc2.pkey=t.p_id
                     where (t.o_id='.$pkey.' and t.o_tkey='.$tkey.') or (t.p_id='.$pkey.' and t.p_tkey='.$tkey.')
                    ',$debug) ;
      if ($debug) print_2x_arr($recs) ;
      // для разделеов каталога получаем списое дочерних брендов
      $child_brands=array() ;
      if (sizeof($recs)) foreach($recs as $rec)
        { $g_id=($rec['p_tkey']==$tkey)? $rec['o_id']:$rec['p_id'] ;
          // если связан раздел - ищем бренды врутри и снаружи
          if (isset($goods_system->tree[$g_id]))
          { $str=$goods_system->tree[$g_id]->get_list_child(CLSS_BRAND) ;     if ($str) $child_brands[]=$str ;
            $str=$goods_system->tree[$g_id]->get_parent_by_clss(CLSS_BRAND) ; if ($str) $child_brands[]=$str ;
          }
          // иначе ищем бренды снаружи
          else
          { $parent=execSQL_value('select parent from obj_site_goods where pkey='.$g_id) ;
            $str=$goods_system->tree[$parent]->get_parent_by_clss(CLSS_BRAND) ; if ($str) $child_brands[]=$str ;
          }
        }
      // получаем все разделы и товары, в которых прямо указан тип товара
      $recs=execSQL('select pkey,parent,clss,obj_name from obj_site_goods where type='.$pkey,$debug) ;
      if (sizeof($recs)) foreach($recs as $rec)
       { if (isset($goods_system->tree[$rec['pkey']]))
          { $str=$goods_system->tree[$rec['pkey']]->get_list_child(CLSS_BRAND) ; if ($str) $child_brands[]=$str ;
            $str=$goods_system->tree[$rec['pkey']]->get_parent_by_clss(CLSS_BRAND) ; if ($str) $child_brands[]=$str ;
          }
         // иначе ищем бренды снаружи
         else
         { $str=$goods_system->tree[$rec['parent']]->get_parent_by_clss(CLSS_BRAND) ; if ($str) $child_brands[]=$str ;
         }
       }

      $child_brands=array_unique($child_brands) ;
      if ($debug) damp_array($child_brands) ;

    if (sizeof($child_brands))
    { $str_brands=implode(',',$child_brands) ;
      $recs_brands=execSQL('select * from '.$TM_goods.' where pkey in ('.$str_brands.')',$debug) ;
      ?><ul><? if (sizeof($recs_brands))
        { $cnt=_CLSS(CLSS_BRAND)->print_list_recs_to_tree($recs_brands,array('attr'=>'type='.$pkey)) ; $list_clss[]=CLSS_BRAND ;

        }?></ul><?
    }

     /*
      // получаем всех потомков текущего объекта
     $list_clss=array() ; $cnt=0 ;
     $list_obj=select_db_obj_child_recs($_POST['reffer'],array('order_by'=>_DEFAULT_TREE_ORDER,'fill_count_childs'=>1,'no_image'=>1)) ;
       ?><ul><? if (sizeof($list_obj)) foreach ($list_obj as $tkey_arr) if (sizeof($tkey_arr)) foreach ($tkey_arr as $clss=>$recs) { $cnt+=_CLSS($clss)->print_list_recs_to_tree($recs) ; $list_clss[]=$clss ; }?></ul><?
     */
     generate_clss_style_icon($list_clss) ;
     // получаем инфу по текущему объекту без инфы по дочерним объектам
     $cur_obj=get_obj_info($_POST['reffer'],array('no_child_obj'=>1)) ;
     _CLSS($cur_obj['clss'])->prepare_public_info($cur_obj) ;
     $this->add_element('cur_obj_name',$cur_obj['_in_tree_name']) ;
     $this->add_element('cur_obj_child_cnt',$cnt) ;

   }
  }



   function get_list_category($doc,$xml)
   { $options=array() ; $usl='' ; $arr_fields=array() ;  $_usl_filter=array() ;
     $title='Заказы ' ;
     if ($_POST['search_text'] and $_POST['target'])
     {   $arr_words=explode(' ',trim($_POST['search_text'])) ; // разбираем на пробелы
         switch($_POST['target'])
          { case 'number': $arr_fields=array('obj_name') ; $title.=' - номер или дата содержит "'.$_POST['search_text'].'"' ; break ;
            case 'client': $arr_fields=array('name','phone','email') ; $title.=' - ФИО, телефон, или почта содержит "'.$_POST['search_text'].'"' ; break ;
            case 'adres':  $arr_fields=array('adres') ; $title.=' - адрес содержит "'.$_POST['search_text'].'"' ; break ;
            case 'rekv':   $arr_fields=array('rekvezit') ; $title.=' - реквизиты содержат "'.$_POST['search_text'].'"' ;  break ;
            case 'goods':  //$arr_fields=array('name','phone','email') ; break ;
          }
         if (sizeof($arr_words)) foreach($arr_words as $word)
         { $_usl2=array() ;
           if (sizeof($arr_fields)) foreach($arr_fields as $fname) $_usl2[]="UCASE(".$fname.") rlike UCASE('[[:<:]]".mb_strtoupper($word)."')" ;
           if (sizeof($_usl2)) $_usl_filter[]='('.implode(' or ',$_usl2).')' ;
         }


     }
     if ($_POST['usl'])                  $usl=stripslashes($_POST['usl']) ;
     if ($_POST['show_van_rec_as_item']) $options['show_van_rec_as_item']=$_POST['show_van_rec_as_item'] ;
     if (sizeof($_usl_filter))           $options['usl_filter']=implode(' and ',$_usl_filter) ;
     $options['default_order']='c_data desc' ;
     $options['title']=$title ;
     // получаем и показывем строки
     $pages_info=_CLSS(82)->show_list_items($_SESSION['category_system']->tkey,$usl,$options) ;
      // выводим информацию по отображенным строкам
      add_element($doc,$xml,'list_id',$_POST['list_id']) ;
      add_element($doc,$xml,'from_pos',$pages_info['from']) ;
      add_element($doc,$xml,'to_pos',$pages_info['to']) ;
      add_element($doc,$xml,'all_pos',$pages_info['all']) ;
      add_element($doc,$xml,'next_page',$pages_info['next_page']) ;
      add_element($doc,$xml,'info_count_text','1...'.$pages_info['to'].' из '.$pages_info['all'])  ;
   }
}
?>