<?php
include_once(_DIR_TO_ENGINE."/admin/c_site.php") ;
include_once(_DIR_TO_ENGINE."/admin/i_clss_func.php") ;
include_once(_DIR_TO_ENGINE.'/admin/c_menu_tabs.php') ;

class c_editor_category extends c_editor_obj
{
  function body($options=array()) { $this->body_frame_2x(array('fra_tree'=>_PATH_TO_ADMIN.'/editor_category_tree.php','title'=>$options['title'])) ; }
}


class c_editor_category_tree extends c_fra_tree
{ public $system_name='category_system' ;

 function _body($options=array())
 {?><body id=tree_objects><div id=tree_menu><?$this->top_menu() ;?></div><?
     $this->generate_time_tree('Заказы',$this->tkey,array('usl_select'=>'clss=82','on_click_script'=>'editor_category_viewer.php','on_click_item'=>array(82=>'fra_viewer.php')));
  ?></body><?
  }

 function body($options=array())
 { if (!$this->tkey) { echo 'Не задана рабочая таблица.' ; _exit() ; }
   $this->check_reffer() ; // проверяем, откуда происходит вызов скрипта
   ?><body id=tree_objects><div id=tree_menu><?$this->top_menu() ;?></div><?
   $this->generate_object_tree($this->tkey,array()); // генерим начало дерева
   ?><script type="text/javascript">$j('ul.tree_root > li:first-child').each(tree_item_click)</script><? // имитируем клик по корню
   ?></body><?
 }

 function generate_object_tree($tkey,$options=array())
  { global $category_system,$TM_goods ;
    $root=execSQL_van('select pkey,clss,obj_name,enabled,1 as cnt from '._DOT($tkey)->table_name.' where  parent=0 limit 1') ;
    //$_SESSION['tree_filter'][$tkey][10]=array('sort'=>'obj_name desc') ;
    //$_SESSION['tree_filter'][$tkey][200]=array('filter'=>'sales_value>0') ;
    $options['debug']=0 ;
    $options['order_by']=_DEFAULT_TREE_ORDER;
    //$options['use_page_view_sort_setting']=0 ; // используем настройки сортировки редактора объектов, если они таб быти определееы
    $options['limit']='limit 100' ;
    $options['no_image']=1 ; //информация по фото не нужна
    $options['fill_count_childs']=1 ; // получаем кол-во дочерних объектов
    $options['no_group_tkey_clss']=1 ; // получаем кол-во дочерних объектов
    $list_obj=select_db_obj_child_recs($root['_reffer'],$options) ;  $list_clss=array() ;
    //damp_array($list_obj) ;
    // надо заполнить поле _in_tree_child_cnt по фактическому наличию товаров данных типов
    // 1. смотрим в линках
    $recs=execSQL('select pkey,o_id,o_tkey,o_clss,p_id,p_tkey,p_clss from '.TM_LINK.' where o_tkey='.$category_system->tkey.' or p_tkey='.$category_system->tkey) ;
    if (sizeof($recs)) foreach($recs as $rec_link)
    { if ($rec_link['o_tkey']==$category_system->tkey and isset($list_obj[$rec_link['o_id']])) $list_obj[$rec_link['o_id']]['_in_tree_child_cnt']++ ;
      if ($rec_link['p_tkey']==$category_system->tkey and isset($list_obj[$rec_link['p_id']])) $list_obj[$rec_link['p_id']]['_in_tree_child_cnt']++ ;
    }
    // 2. смотрим в указанных типах товаров
    $recs=execSQL('select pkey,type from '.$TM_goods.' where not type is null and type>0') ;
    if (sizeof($recs)) foreach($recs as $rec_cat) if (isset($list_obj[$rec_cat['type']])) $list_obj[$rec_cat['type']]['_in_tree_child_cnt']++ ;

    //print_2x_arr($list_obj) ;
    ?><ul class="tree_root" table_code="<?echo _DOT($tkey)->table_code?>" on_click_script="editor_category_viewer.php" on_expand_script="editor_category_ajax.php" on_expand_cmd="upload_types_items">
         <li clss="<?echo $root['clss']?>"  <?if (!$root['parent']) echo 'is_root=1'?> reffer="<?echo $root['_reffer']?>"><span class=name><?echo $root['obj_name']?></span><span class=cnt></span></li>
         <ul><? if (sizeof($list_obj))  foreach ($list_obj as $rec) { $list_clss[$rec['clss']]+=_CLSS($rec['clss'])->print_rec_to_tree($rec) ; }?></ul>
      </ul>
    <?
    //$list_clss=array_unique($list_clss) ;
    //damp_array($list_clss) ;
    generate_clss_style_icon(array_keys($list_clss)) ;
  }



}

//-----------------------------------------------------------------------------------------------------------------------------
// ЗАКАЗЫ
//-----------------------------------------------------------------------------------------------------------------------------

class c_editor_category_viewer extends c_fra_based
{ public $system_name='category_system' ;
  public $top_menu_name='top_menu_list_category' ;


 function body(&$options=array()) {$this->body_frame($options) ; }

 function select_obj_info()
 { global $obj_info,$type_info,$category_system  ;
   if ($_GET['reffer']) $obj_info=get_obj_info($_GET['reffer']) ;
   if ($_GET['type']) $type_info=get_obj_info($_GET['type'].'.'.$category_system->tkey) ;
 }

// заголовок фрейма
 function show_page_obj_header()
 { global $obj_info,$type_info ;
   //damp_array($obj_info) ;
   ?><p class='obj_header'><?
     if ($type_info['pkey']) echo $type_info['obj_name'].' ' ;
     echo $obj_info['obj_name']

    ?></p><?
 }

 // меню фрейма
 function show_page_obj_menu($obj_info)
    {  // создаем объект top_menu
       if (!is_object($_SESSION[$this->top_menu_name])) $_SESSION[$this->top_menu_name]=new c_top_menu() ;
       // формируем набор пунктом меню
       $menu_set=array() ;
       //damp_array($_GET) ;
       // пункты меню для корня дерева
       if ($_GET['is_root'])
       { $menu_set[]=array("name"=>"Типы товаров",'cmd'=>'panel_types');
         $menu_set[]=array('name'=>'Поставщики','script'=>'mod/category/i_categor.php','cmd'=>'panel_view_postav') ;
         $menu_set[]=array('name'=>'Бренды','script'=>'mod/category/i_categor.php','cmd'=>'panel_view_brands');
         $menu_set[]=array('name'=>'Товары','script'=>'mod/category/i_menu_panels.php','cmd'=>'panel_view_goods_of_type');
         $menu_set[]=array('name'=>'Операции','cmd'=>'panel_operation');
       }
       // пункты меню для типа товара
       else if ($obj_info['clss']==60)
       { $menu_set[]=array('name'=>'Товары','script'=>'mod/category/i_menu_panels.php','cmd'=>'panel_view_goods_of_type');
         $menu_set[]=array('name'=>'Свойства типа','cmd'=>'panel_props_of_type');
         $menu_set[]=array('name'=>'Списки типа','cmd'=>'panel_list_of_type');
       }
       else if ($obj_info['clss']==CLSS_BRAND)
       { $menu_set[]=array('name'=>'Товары бренда','script'=>'mod/category/i_menu_panels.php','cmd'=>'panel_view_goods_of_type');

       }
       else if ($obj_info['clss']==CLSS_COLL)
       { $menu_set[]=array('name'=>'Товары серии','script'=>'mod/category/i_menu_panels.php','cmd'=>'panel_view_goods_of_type');

       }
       // выводим меню
       $this->cur_menu_item=$_SESSION[$this->top_menu_name]->show($menu_set) ;  // показываем меню
       return($this->cur_menu_item) ;
    }

  function show_list_items()
  { $options=array() ;
    if ($this->start_time or $this->end_time)  $_usl[]=$this->get_time_usl($this->start_time,$this->end_time) ;
    if (isset($this->cur_menu_item['status'])) $_usl[]='status='.$this->cur_menu_item['status'] ;
    if (isset($this->cur_menu_item['robox']))  $_usl[]='payments_method="robox"' ;
    if (isset($this->cur_menu_item['market']))  $_usl[]='ya_id>0' ;
    if (sizeof($_usl)) $usl=implode(' and ',$_usl) ;
    $title='Заказы '.$this->get_time_title($this->start_time,$this->end_time) ;
    if (isset($this->cur_menu_item['status'])) $title.=' в состоянии "'.$_SESSION['list_status_category'][$this->cur_menu_item['status']].'"' ;
    if (isset($this->cur_menu_item['robox'])) $title.=' оплаченные через Robox' ;
    if (isset($this->cur_menu_item['market'])) $title.=' оформленные через Yandex Маркет' ;
    if ($this->cur_menu_item['mode']=='last_items')
    { $options['count']=$this->cur_menu_item['count'] ;
      $options['order']='c_data desc' ;
      $title.=' - последние '.$this->cur_menu_item['count'] ;
    }
    $options['default_order']='c_data desc' ;
    $options['title']=$title ;
    $options['buttons']=array('save','delete') ;
    _CLSS(82)->show_list_items($this->tkey,$usl,$options) ;
  }


  function panel_fast_search() {}
/*
    { ?><div id=panel_search script_url="<?echo _PATH_TO_ADMIN?>/editor_category_ajax.php" cmd="get_list_category">
            <strong>ПОИСК:</strong>
            <input type=text name=text_search class="text"><img src="<?echo _PATH_TO_ENGINE?>/admin/images/find_icon.gif" class=go_search>
            <input type=radio name=target_search value=number>По номеру, дате
            <input type=radio name=target_search value=client checked>По клиенту
            <input type=radio name=target_search value=adres>По адресу
            <input type=radio name=target_search value=rekv>По реквизитам
            <!--<input type=radio name=target_search value=goods>По товару-->
        </div>
     <?
    }
  */


}









?>