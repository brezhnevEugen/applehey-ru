<?php

$__functions['init'][]		='_category_site_vars' ;
$__functions['boot_site'][]	='_category_site_boot' ;

define('CLSS_BRAND',201) ;
define('CLSS_COLL',210) ;

function _category_tree($id) { if (isset($_SESSION['category_system']->tree[$id])) return($_SESSION['category_system']->tree[$id]) ; else return (null);}

function _category_site_vars() //
{   $_SESSION['TM_category']='obj_'.SITE_CODE.'_category' ; $GLOBALS['TM_category']=&$_SESSION['TM_category'] ;
    $_SESSION['TM_category_image']='obj_'.SITE_CODE.'_category_image' ; $GLOBALS['TM_category_image']=&$_SESSION['TM_category_image'] ;

	$_SESSION['init_options']['category']['debug']=0 ;
	$_SESSION['init_options']['category']['patch_mode']='TREE_NAME' ;
	$_SESSION['init_options']['category']['root_dir']	='category' ;
    $_SESSION['init_options']['category']['tree_fields']='pkey,parent,clss,indx,enabled,href,obj_name,top' ;
    $_SESSION['init_options']['category']['tree']['debug']=0 ;
    $_SESSION['init_options']['category']['tree']['clss']='1,60' ;
	$_SESSION['init_options']['category']['tree']['include_space_section']=1 ;
    $_SESSION['init_options']['category']['tree']['order_by']='indx' ;
    $_SESSION['init_options']['category']['tree']['get_count_by_clss']=0 ;
}

// парамерт options будет передан из session_boot($options) <= show_page($options)
function _category_site_boot($options=array())
{   $options['debug']=0 ;
	create_system_modul_obj('category',$options) ; // создаем подсистему "category"


}

class c_category_system extends c_system_catalog
{
  public $function_check_filter='parse_url_dir_to_filter_value' ;

  // $create_options в подсистему передается как объединение init_options[category] и options в _category_site_boot
  function	c_category_system(&$create_options=array())
  { global $TM_category;
    // устанавливаем значения опций по умолчанию, если они не были заданы ранее
    if (!$create_options['table_name']) 			$create_options['table_name']=$TM_category ;
    if (!$create_options['section_page_name']) 	    $create_options['section_page_name']='catalog' ; // названия для префиксной системы страниц
    if (!$create_options['item_page_name']) 	    $create_options['item_page_name']='category' ; // названия для префиксной системы страниц
    if (!$create_options['root_dir'])				$create_options['root_dir']='catalog' ;
    //if (!$create_options['usl_show_items'])         $create_options['usl_show_items']='clss in(2,200) and enabled=1' ;

    if (!$create_options['tree']['order_by'])	    $create_options['tree']['order_by']='indx' ;

    parent::c_system_catalog($create_options) ;


   //damp_array($GLOBALS['arr_props_by_parent']) ;
   //damp_array($_SESSION['IL_goods_types']) ;
   $this->reload_list_types() ;
  }



  function reload_list_types()
  {
       // список всех типов товара
      $all_types=execSQL('select pkey,obj_name,url_name,input,list_id,ed_izm from '.$this->table_name.' where clss=60 and enabled=1 order by indx') ;
      $_SESSION['IL_goods_types']=array() ;
      // список всех свойств товара
      $arr_props=execSQL('select pkey,parent,id,indx,obj_name as title,input,list_id,url_name as dir from '.$this->table_name.' where clss=61 and enabled=1 order by parent,indx') ;
      if (sizeof($arr_props)) foreach($arr_props as $pkey=>$rec_prop)
          { if ($rec_prop['input']==1) $arr_props[$pkey]['indx_select']=$_SESSION['index_list_names'][$rec_prop['list_id']] ;
            if ($rec_prop['input']==2) $arr_props[$pkey]['edit_element']='checkbox' ;
            if ($rec_prop['input']==3) $arr_props[$pkey]['edit_element']='input_num' ;
            if ($rec_prop['input']==4) $arr_props[$pkey]['edit_element']='input_str' ;
            if (!$rec_prop['dir'])     $arr_props[$pkey]['dir']=safe_text_to_url($rec_prop['title']) ;
            unset($arr_props[$pkey]['input']) ;
            unset($arr_props[$pkey]['pkey']) ;
            unset($arr_props[$pkey]['tkey']) ;
            unset($arr_props[$pkey]['_reffer']) ;
            unset($arr_props[$pkey]['_parent_reffer']) ;
            unset($arr_props[$pkey]['list_id']) ;
          }

       $arr_props_by_parent=group_by_field('parent',$arr_props,'id') ;
       //damp_array($arr_props_by_parent) ;



       if (sizeof($all_types)) foreach($all_types as $rec_type)
           $_SESSION['IL_goods_types'][$rec_type['pkey']]=array('obj_name'=>$rec_type['obj_name'],'url_name'=>$rec_type['url_name'],'ed_izm'=>$rec_type['ed_izm']) ;


       if (sizeof($arr_props_by_parent)) foreach($arr_props_by_parent as $type_pkey=>$arr_props)
       {  $_SESSION['IL_goods_types'][$type_pkey]['_props']=$arr_props ;
          $this->tree[$type_pkey]->props=$arr_props ;
       }


      // Обнуляем все флаги во всех подразделах
      $flag_name='cnt_goods_m' ;  $parent_field='type' ;  $table_name='obj_site_goods' ;  ; $options=array() ;
      $arr_usl=array() ;
      $arr_usl[]='clss=200 and enabled=1 and _enabled=1 and _image_name!="" and m=1' ;
      $res_usl=implode(' and ',$arr_usl) ;
      $where_usl=(sizeof($arr_usl))? 'where '.$res_usl:'' ;
      $this->clear_cnt_objects($flag_name) ;
      // получаем список разделов и количество объектов
      $list_parent=execSQL('select '.$parent_field.',count(pkey) as cnt from '.$table_name.' '.$where_usl.' group by '.$parent_field.' order by '.$parent_field,$options) ;
      // проставляем флаги только в тех подразделах, которые включают в себя текущий бренд
      if (sizeof($list_parent)) foreach($list_parent as $rec) if (isset($this->tree[$rec[$parent_field]])) $this->tree[$rec[$parent_field]]->$flag_name=$rec['cnt'] ;
      // рекурсивно переносим флаги с детей на родителей
      $this->tree['root']->update_field_value($flag_name) ;
      // сохраняем условие, по которому был сделан подсчет
      //if ($options['debug']) echo 'сохраняем условие, по которому был сделан подсчет: '.$res_usl.'<br>' ;
      $this->tree['root']->usl_cnt_objects[$flag_name]=$res_usl ;

      // Обнуляем все флаги во всех подразделах
      $flag_name='cnt_goods_w' ;  $parent_field='type' ;  $table_name='obj_site_goods' ;  ; $options=array() ;
      $arr_usl=array() ;
      $arr_usl[]='clss=200 and enabled=1 and _enabled=1 and _image_name!="" and w=1' ;
      $res_usl=implode(' and ',$arr_usl) ;
      $where_usl=(sizeof($arr_usl))? 'where '.$res_usl:'' ;
      $this->clear_cnt_objects($flag_name) ;
      // получаем список разделов и количество объектов
      $list_parent=execSQL('select '.$parent_field.',count(pkey) as cnt from '.$table_name.' '.$where_usl.' group by '.$parent_field.' order by '.$parent_field,$options) ;
      // проставляем флаги только в тех подразделах, которые включают в себя текущий бренд
      if (sizeof($list_parent)) foreach($list_parent as $rec) if (isset($this->tree[$rec[$parent_field]])) $this->tree[$rec[$parent_field]]->$flag_name=$rec['cnt'] ;
      // рекурсивно переносим флаги с детей на родителей
      $this->tree['root']->update_field_value($flag_name) ;
      // сохраняем условие, по которому был сделан подсчет
      //if ($options['debug']) echo 'сохраняем условие, по которому был сделан подсчет: '.$res_usl.'<br>' ;
      $this->tree['root']->usl_cnt_objects[$flag_name]=$res_usl ;


      // Обнуляем все флаги во всех подразделах
      $flag_name='cnt_goods_k' ;  $parent_field='type' ;  $table_name='obj_site_goods' ;  ; $options=array() ;
      $arr_usl=array() ;
      $arr_usl[]='clss=200 and enabled=1 and _enabled=1 and _image_name!="" and k=1' ;
      $res_usl=implode(' and ',$arr_usl) ;
      $where_usl=(sizeof($arr_usl))? 'where '.$res_usl:'' ;
      $this->clear_cnt_objects($flag_name) ;
      // получаем список разделов и количество объектов
      $list_parent=execSQL('select '.$parent_field.',count(pkey) as cnt from '.$table_name.' '.$where_usl.' group by '.$parent_field.' order by '.$parent_field,$options) ;
      // проставляем флаги только в тех подразделах, которые включают в себя текущий бренд
      if (sizeof($list_parent)) foreach($list_parent as $rec) if (isset($this->tree[$rec[$parent_field]])) $this->tree[$rec[$parent_field]]->$flag_name=$rec['cnt'] ;
      // рекурсивно переносим флаги с детей на родителей
      $this->tree['root']->update_field_value($flag_name) ;
      // сохраняем условие, по которому был сделан подсчет
      //if ($options['debug']) echo 'сохраняем условие, по которому был сделан подсчет: '.$res_usl.'<br>' ;
      $this->tree['root']->usl_cnt_objects[$flag_name]=$res_usl ;
  }

    function prepare_public_info(&$rec,$options=array())
    { parent::prepare_public_info($rec,$options) ;   // в классе catalog_system будут заполнены поля __href, __name, __data, __img_alt
      if (is_object($this->tree[$rec['parent']])) $rec['__parents']=$this->tree[$rec['parent']]->get_arr_parent() ;
     }

  // возвращает  условие для отбора товаров по типу
  // должен учитывать наследование типов
  function get_usl_to_type($type)
  {  $child_types=$this->tree[$type]->get_list_child() ;
     //echo 'type='.$type.'<br>' ;
     //echo '$child_types='.$child_types.'<br>' ;
     //damp_array($this->tree[$type]);
     $result='type in ('.$child_types.')';
     return($result) ;
  }

  // возвращает дополительное условие для отбора товаров по свойствам
  function get_usl_to_props($props)
  { $usl_for_props=array() ; $usl='' ;
    //damp_array($props) ;
    if (sizeof($props)) foreach($props as $type_id=>$arr_props)
      {  if (sizeof($arr_props)) foreach($arr_props as $prop_id=>$arr_values)
         {  $arr=array() ;
            if (isset($arr_values['from']) or isset($arr_values['to']))
            {   $from=$arr_values['from'] ; $to=$arr_values['to'] ;
                if ($from and !$to) $usl_for_props[]='pkey in (select parent from obj_site_goods_props where type='.$type_id.' and id='.$prop_id.' and num>='.$from.')' ;
                if (!$from and $to) $usl_for_props[]='pkey in (select parent from obj_site_goods_props where type='.$type_id.' and id='.$prop_id.' and num<='.$to.')' ;
                if ($from and $to)  $usl_for_props[]='pkey in (select parent from obj_site_goods_props where type='.$type_id.' and id='.$prop_id.' and num>='.$from.' and num<='.$to.')' ;
            }
            else
            { if (sizeof($arr_values)==1) $arr[]='id='.$prop_id.' and value="'.$arr_values[0].'"' ;
              elseif (sizeof($arr_values)>1) foreach($arr_values as $value) {$arr[]='(id='.$prop_id.' and value="'.$value.'")' ;}
              if (sizeof($arr)==1) $usl_for_props[]='pkey in (select parent from obj_site_goods_props where type='.$type_id.' and '.$arr[0].')' ;
              elseif (sizeof($arr)>1) $usl_for_props[]='pkey in (select parent from obj_site_goods_props where type='.$type_id.' and ('.implode(' or ',$arr).'))' ;
            }
         }
      }
     //damp_array($usl_for_props) ;
     if (sizeof($usl_for_props)) $usl=' and '.implode(' and ',$usl_for_props);
     return($usl) ;
  }

  // возвращает товары, аналогичные текущему на основании набора свойств
  // $rec['obj_clss_3']- изображения
  // $rec['obj_clss_6']- свойства
  function convert_props_to_usl($rec)
  { $props=array() ;
    if (sizeof($rec['obj_clss_6'])) foreach($rec['obj_clss_6'] as $rec_prop)  $props[$rec['type']][$rec_prop['id']][]=$rec_prop['value'] ;
    return($props) ;
  }

  // возвращает массив тип объекта=число объектов в тиекущем объекте
  function get_types_count_info($pkey)
  {  global $goods_system ;
     if (!isset($goods_system->tree[$pkey])) return(array()) ;
     $child_ids=$goods_system->tree[$pkey]->get_list_child() ;
     $arr_types=execSQL_row('select type,count(pkey) from obj_site_goods where parent in ('.$child_ids.') and clss in ('.$goods_system->clss_items.') group by type') ;
     $arr_types2=execSQL_row('select type2,count(pkey) from obj_site_goods where parent in ('.$child_ids.') and clss in ('.$goods_system->clss_items.') and type2>0 group by type2') ;
     $arr_types3=execSQL_row('select type3,count(pkey) from obj_site_goods where parent in ('.$child_ids.') and clss in ('.$goods_system->clss_items.') and type3>0 group by type2') ;
     // добавляем 2 и 3-й типы к общий список
     if (sizeof($arr_types2)) foreach($arr_types2 as $type=>$count) if (isset($arr_types[$type])) $arr_types[$type]+=$count ; else $arr_types[$type]=$count ;
     if (sizeof($arr_types3)) foreach($arr_types3 as $type=>$count) if (isset($arr_types[$type])) $arr_types[$type]+=$count ; else $arr_types[$type]=$count ;
     return($arr_types) ;
  }

  // определяет тип объекта (по собственному оснеовному type или основному типу родительского элемента)
  function get_obj_type($rec)
  { global $goods_system ;
    if ($rec['type']) return($rec['type']) ;
    if (isset($GLOBALS['parents_type'][$rec['parent']])) $type=$GLOBALS['parents_type'][$rec['parent']] ;
    else if (isset($goods_system->tree[$rec['parent']]))
      { $parent_ids=$goods_system->tree[$rec['parent']]->get_list_parent() ;
        $arr_type=execSQL_row('select pkey,type from '.$goods_system->table_name.' where pkey in ('.$parent_ids.') order by pkey') ;
        $art_parent_ids=array_reverse(explode(',',$parent_ids)) ;
        $type=0 ;
        if (sizeof($art_parent_ids)) foreach($art_parent_ids as $parent_id) if ($arr_type[$parent_id]) { $type=$arr_type[$parent_id] ; break ; }
        // запоминмаем в GLOBAL чтобы при следующем товаре у аналогичного parent не делать второй раз звапрос
        $GLOBALS['parents_type'][$rec['parent']]=$type ;
      }
      return($type) ;
    }

  // возвращает массив с описанием свойст переданного объекта
  function get_array_props_info($rec)
  { $type=(is_array($rec))? $this->get_obj_type($rec):$rec ;
    return(($type)? $_SESSION['IL_goods_types'][$type]['_props']:array()) ;
  }

  // возвращаем список свойств товара на основе описания свойств и списка значений свойств из БД
  function get_props_list($props_info,$list_props)
    { $arr=array() ; //damp_array($props_info) ;
      // собираем список параметров на основе шаблона вывода свойств товара
      /*
      if (sizeof($_SESSION['show_params'])) foreach($_SESSION['show_params'] as $title=>$param_format)
       {  if (!is_array($param_format))
          { if (sizeof($list_props[$param_format])) $arr[$title.':']=$this->get_prop_value($props_info[$param_format],$list_props[$param_format]) ;
          }
          else
          { $ss2=array() ; $flag=0 ; $cnt_param=0 ;
            foreach($param_format as $ss) if (is_int($ss))
            { $cnt_param++ ;
              if (sizeof($list_props[$ss])) $ss2[]=$this->get_prop_value($props_info[$ss],$list_props[$ss]) ;
              else $flag++ ; // $flag>0 если один из параметров не определен
            } else $ss2[]=$ss ;
            //if (!$flag) $arr[$title]=implode('',$ss2) ; //условие, если НЕ выводим, если хотя бы один из параметров НЕ определен
            if ($flag<$cnt_param) $arr[$title.':']=implode('',$ss2) ; //условие, если НЕ выводим, если ВСЕ параметры НЕ определен
          }
       }
      */
      // собираем список параметров на только на основе заданных в админке параметров
      //if (sizeof($props_info)) foreach($props_info as $prop_rec) if (isset($list_props[$prop_rec['id']])) $arr[$prop_rec['title'].':']=$this->get_prop_value($prop_rec,$list_props[$prop_rec['id']]) ;
      if (sizeof($list_props)) foreach($list_props as $prop_rec) if (isset($props_info[$prop_rec['id']]))
      {  $rec_info=$props_info[$prop_rec['id']] ;
         $title=$rec_info['title'] ;
         $indx=$rec_info['indx'] ;
         if (!isset($arr[$indx])) $arr[$indx]=array('title'=>$title,'value'=>$this->get_prop_value($rec_info,$prop_rec)) ;
         else                     $arr[$indx]['value'].=', '.$this->get_prop_value($rec_info,$prop_rec) ;
      }
      ksort($arr) ; // сортируем по возрастанию индексов
      //damp_array($arr) ;
      return($arr) ;
    }

    function get_prop_value($prop_rec_descr,$prop_obj_rec)
    { $value='' ;
      if ($prop_rec_descr['indx_select']) $value=$_SESSION[$prop_rec_descr['indx_select']][$prop_obj_rec['value']]['obj_name'] ;
      else switch($prop_rec_descr['edit_element'])
      { case 'input_num': $value=($prop_obj_rec['num'])*1 ; break ;
        case 'input_str': $value=$prop_obj_rec['str'] ; break ;
        case 'checkbox':  $value=($prop_obj_rec['value'])? 'Да':'Нет' ; break ;
      }
      return($value) ;
    }

    function save_prop_value($goods_id,$goods_type,$prop_id,$prop_value,$prop_checked,$prop_info)
    { // если тип свойства - выпадающий список
      if ($prop_info['indx_select'])
      {  // получаем запись по свойству в таблице, если есть
         $rec_props=execSQL_van('select * from obj_site_goods_props where parent='.$goods_id.' and id='.$prop_id.' and value="'.$prop_value.'"') ;
         if ($rec_props['pkey'] and !$prop_checked) execSQL_update('delete from obj_site_goods_props where pkey='.$rec_props['pkey']) ;
         if (!$rec_props['pkey'] and $prop_checked) adding_rec_to_table('obj_site_goods_props',array('clss'=>6,'enabled'=>1,'parent'=>$goods_id,'id'=>$prop_id,'type'=>$goods_type,'value'=>$prop_value)) ;
      }
      if ($prop_info['edit_element']=='input_num')
      {  $rec_props=execSQL_van('select * from obj_site_goods_props where parent='.$goods_id.' and id='.$prop_id) ;
         if ($rec_props['pkey']) update_rec_in_table('obj_site_goods_props',array('num'=>$prop_value),'pkey='.$rec_props['pkey']) ;
         else                    adding_rec_to_table('obj_site_goods_props',array('clss'=>6,'enabled'=>1,'parent'=>$goods_id,'id'=>$prop_id,'type'=>$goods_type,'num'=>$prop_value)) ;
      }
      if ($prop_info['edit_element']=='input_str')
      {  $rec_props=execSQL_van('select * from obj_site_goods_props where parent='.$goods_id.' and id='.$prop_id) ;
         if ($rec_props['pkey']) update_rec_in_table('obj_site_goods_props',array('str'=>$prop_value),'pkey='.$rec_props['pkey']) ;
         else                    adding_rec_to_table('obj_site_goods_props',array('clss'=>6,'enabled'=>1,'parent'=>$goods_id,'id'=>$prop_id,'type'=>$goods_type,'str'=>$prop_value)) ;
      }
      if ($prop_info['edit_element']=='checkbox')
      {  $rec_props=execSQL_van('select * from obj_site_goods_props where parent='.$goods_id.' and id='.$prop_id) ;
         if ($rec_props['pkey']) update_rec_in_table('obj_site_goods_props',array('value'=>$prop_checked),'pkey='.$rec_props['pkey']) ;
         else                    adding_rec_to_table('obj_site_goods_props',array('clss'=>6,'enabled'=>1,'parent'=>$goods_id,'id'=>$prop_id,'type'=>$goods_type,'value'=>$prop_checked)) ;
      }
    }

}

?>