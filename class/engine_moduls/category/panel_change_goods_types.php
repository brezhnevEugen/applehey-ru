<?
// $options:
//  mode       - main
//             - second
//             - goods
// select_ids   - массив id checked элементов
// allow_ids   - массив id visible эдментов
function panel_change_goods_types($options=array())
{ global $category_system,$goods_system ;
  $obj_info=$options['cur_page']->obj_info ;
  // получаем инфу по допустимым типам товаров
  $main_type=$obj_info['type'] ;
  $allows_types[$main_type]=$category_system->tree[$main_type]->name ;
  $str_links=get_linked_ids_to_rec($obj_info['_reffer']) ;
  if ($str_links)
  { $second_type_ids=explode(',',$str_links) ;
    if (sizeof($second_type_ids)) foreach($second_type_ids as $second_type) $allows_types[$second_type]=$category_system->tree[$second_type]->name ;
  }
  // получаем инфу то типам дочерних элементов
  $arr_types=$category_system->get_types_count_info($obj_info['pkey']) ;
  ?><h2>Производим замену типа во всех дочерних объектах</h2>
    Меняем тип товаров <select name="from_type"><option value="0"></option><?if (sizeof($arr_types)) foreach($arr_types as $type=>$count) {?><option value="<?echo $type?>"><?echo $category_system->tree[$type]->name ?> [<?echo $count?>]</option><?}?></select>
    на тип <select name="to_type"><option value="0"></option><?if (sizeof($allows_types)) foreach($allows_types as $type=>$name) {?><option value="<?echo $type?>"><?echo $name ?></option><?}?></select>
    <button cmd="change_goods_types_exec" script="mod/category/panel_change_goods_types.php">Изменить</button><br><br>
    Задаем для товаров с названием <input type="text" name="from_name" class="big">....
    тип товара <select name="to_type_by_text"><option value="0"></option><?if (sizeof($allows_types)) foreach($allows_types as $type=>$name) {?><option value="<?echo $type?>"><?echo $name ?></option><?}?></select>
    <button cmd="change_goods_types_exec" script="mod/category/panel_change_goods_types.php">Изменить</button>
  <?
}


function panel_view_count_of_type($recs)
{ global $category_system ;
  ?><table><tr><th>Тип товара</th><th>Кол-во</th></tr><?
  if (sizeof($recs))foreach($recs as $type=>$count) {?><tr><td><?echo $category_system->tree[$type]->name ?></td><td><?echo $count?></td></tr><?}
  ?></table><?

}


function change_goods_types_exec($options)
{ global $goods_system,$category_system ;

  ?><h2>Производим изменение типа дочерних объектов</h2><?
   $arr_types_before=$category_system->get_types_count_info($_POST['pkey']) ;
   //panel_view_count_of_type($arr_types) ;
  // получаем инфу то типам дочерних элементов
  $child_ids=$goods_system->tree[$_POST['pkey']]->get_list_child() ;
  if ($child_ids)
  { if ($_POST['from_type'] and $_POST['to_type'])
      {   execSQL_update('update obj_site_goods set type='.$_POST['to_type'].' where type='.$_POST['from_type'].' and parent in ('.$child_ids.')',array('log_result'=>1)) ;
          execSQL_update('update obj_site_goods set type2='.$_POST['to_type'].' where type2='.$_POST['from_type'].' and parent in ('.$child_ids.')',array('log_result'=>1)) ;
          execSQL_update('update obj_site_goods set type3='.$_POST['to_type'].' where type3='.$_POST['from_type'].' and parent in ('.$child_ids.')',array('log_result'=>1)) ;
      }
    if ($_POST['from_name'] and $_POST['to_type_by_text']) execSQL_update('update obj_site_goods set type='.$_POST['to_type_by_text'].' where obj_name like "%'.$_POST['from_name'].'%"  and parent in ('.$child_ids.')',array('log_result'=>1)) ;
  }
  echo '<br><br>' ;
  $arr_types_after=$category_system->get_types_count_info($_POST['pkey']) ;
  $res=array() ;
  if (sizeof($arr_types_before)) foreach($arr_types_before as $type=>$count) $res[$type]['before']=$count ;
  if (sizeof($arr_types_after)) foreach($arr_types_after as $type=>$count) $res[$type]['after']=$count ;

  ?><table><tr><th>Тип товара</th><th>Кол-во до</th><th>Кол-во после</th></tr><?
  if (sizeof($res))foreach($res as $type=>$info) if ($info['before']!=$info['after']) {?><tr><td><?echo $category_system->tree[$type]->name ?></td><td><?echo $info['before']?></td><td><?echo $info['after']?></td></tr><?}
  ?></table><br><br><?
  panel_change_goods_types($options) ;


//  panel_view_count_of_type($arr_types) ;



}
?>