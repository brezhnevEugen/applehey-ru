<?
// подключение панели в меню
// ext_panel_register(array('frame'=>array('table_code'=>'goods','is_root'=>1),'menu_item'=>array('name'=>'Операции','script'=>'mod/goods/i_goods_root_operation_actions.php','cmd'=>'panel_goods_root_operation_actions'))) ;

// операции
 function panel_goods_root_operation_actions()
    { ?><table class="left"><?
//        ?><!--<tr><td>Внести значения brand_id и series_id в записи товара</td><td><button cmd="update_brand_id" script="mod/goods/i_goods_root_operation_actions.php">Выполнить</button></td></tr>--><?//
//        ?><!--<tr><td>Обновить поле price_site</td><td><button cmd="update_price_site" script="mod/goods/i_goods_root_operation_actions.php">Выполнить</button></td></tr>--><?//
//        ?><!--<tr><td>Показать товары без бренда</td><td><button cmd="view_no_brand_goods"  script="mod/goods/i_goods_root_operation_actions.php">Выполнить</button></td></tr>--><?//
//        ?><!--<tr><td>Показать товары без колллекции</td><td><button cmd="view_no_collection_goods"  script="mod/goods/i_goods_root_operation_actions.php">Выполнить</button></td></tr>--><?//
//        ?><!--<tr><td>Показать товары без типа</td><td><button cmd="view_no_types_goods"  script="mod/goods/i_goods_root_operation_actions.php">Выполнить</button></td></tr>--><?//
        ?><tr><td>Показать разделы и товары с запретом индексации Yandex</td><td><button cmd="view_no_yandex_index_goods"  script="mod/goods/i_goods_root_operation_actions.php">Выполнить</button></td></tr><?
        ?><tr><td>Показать разделы и товары с запретом индексации Google</td><td><button cmd="view_no_google_index_goods"  script="mod/goods/i_goods_root_operation_actions.php">Выполнить</button></td></tr><?
      ?></table><?
    }


function view_no_yandex_index_goods()
{ global $TM_goods ;
  ?><h2>Список разделов с запретом на индексацию <span class="red">Yandex:</span></h2><?
  _CLSS(10)->show_list_items($TM_goods,'clss=10 and no_index_yandex=1',array()) ;
 ?><h2>Список товаров с запретом на индексацию <span class="red">Yandex:</span></h2><?
  _CLSS(200)->show_list_items($TM_goods,'clss=200 and no_index_yandex=1',array()) ;
}

function view_no_google_index_goods()
{ global $TM_goods ;
  ?><h2>Список разделов с запретом на индексацию <span class="red">google:</span></h2><?
  _CLSS(10)->show_list_items($TM_goods,'clss=10 and no_index_google=1',array()) ;
  ?><h2>Список товаров с запретом на индексацию <span class="red">google:</span></h2><?
  _CLSS(200)->show_list_items($TM_goods,'clss=200 and no_index_google=1',array()) ;
}



 //---------------------- неиспользуемые функции -----------------

function update_brand_id()
{ global $goods_system ;
  ?><h2>Заполняем значения brand_id и series_id в записи товара</h2><?
  execSQL_update('update obj_site_goods set brand_id=null,series_id=null') ;
  $list_brands=execSQL('select pkey,obj_name from obj_site_goods where clss=201') ;
  if (sizeof($list_brands)) foreach($list_brands as $rec)
  { $child_sectiond_ids=$goods_system->tree[$rec['pkey']]->get_list_child() ;
    if ($child_sectiond_ids) execSQL_update('update obj_site_goods set brand_id='.$rec['pkey'].' where parent in ('.$child_sectiond_ids.')') ;
    echo 'Заполнено значение для бренда "'.$rec['obj_name'].'"<br>' ;
  }
  $list_collection=execSQL('select pkey,obj_name from obj_site_goods where clss=210') ;
  if (sizeof($list_collection)) foreach($list_collection as $rec)
  { $child_sectiond_ids=$goods_system->tree[$rec['pkey']]->get_list_child() ;
    if ($child_sectiond_ids) execSQL_update('update obj_site_goods set series_id='.$rec['pkey'].' where parent in ('.$child_sectiond_ids.')') ;
    echo 'Заполнено значение для коллекции "'.$rec['obj_name'].'"<br>' ;
  }
}


function view_no_brand_goods()
{ global $TM_goods ;
  _CLSS(200)->show_list_items($TM_goods,'clss=200 and (brand_id=0 or brand_id is null)',array()) ;
}

function view_no_collection_goods()
{ global $TM_goods ;
  _CLSS(200)->show_list_items($TM_goods,'clss=200 and (series_id=0 or series_id is null)',array()) ;
}

function view_no_types_goods()
{ global $TM_goods ;
  _CLSS(200)->show_list_items($TM_goods,'clss=200 and (type=0 or type is null)',array()) ;
}


/*=====================================================================================================================================================================================================================================
 *
 * УТИЛИТА ДЛЯ ПРИНУДИТЕЛЬНОГО ОБНОВЛЕНИЯ ПОЛЯ price_site
 *
 *=====================================================================================================================================================================================================================================*/

function update_price_site()
{ ?><h1>Заполняем (обновляем) поле "price_site"</h1>
    Если на сайте цены на товары в базе заданы в разных валютах, для сортировки по цене необходимо иметь поле, в котором цена будет представлена в абсолютной величине.<br>
    Функция заполняет поле site_price, используя метод goods_system->exec_price().<br><br>
    <button cmd=update_price_site_apply script="mod/goods/i_goods_root_operation_actions.php">Заполнить</button><br><br>
  <?
}

function update_price_site_apply()
{ ?><h1>Заполняем (обновляем) поле "price_site"</h1><?
  $list_goods=execSQL('select pkey as id,price,koof,val,sales_type,sales_value,obj_name from '.$_SESSION['goods_system']->table_name.' where clss=2 or clss=200 order by pkey') ;
  execSQL_update('update '.$_SESSION['goods_system']->table_name.' set price_site=NULL,price_on=NULL') ;
  $update=0 ;
  ?><table class=left><tr class=clss_header><td>Наименование</td><td>Цена</td><td>Валюта</td><td>Значение скидки</td><td>Цена на сайте</td></tr><?
  if (sizeof($list_goods)) foreach($list_goods as $rec)
  { if (!$rec['val']) $rec['val']=$_SESSION['VL_def_DB'] ;
    $_SESSION['goods_system']->exec_price($rec) ; // damp_array($rec) ;
    if ($rec['__price_4'])
    { $res=update_rec_in_table($_SESSION['goods_system']->table_name,array('price_site'=>$rec['__price_4'],'price_on'=>1),'pkey='.$rec['id']) ;
    $update+=$res['update'] ;
  }
    ?><tr><td><?echo $rec['obj_name']?></td>
          <td><?if ($rec['price']>0) echo $rec['price']?></td>
          <td><?if ($rec['price']>0) echo $_SESSION['VL_arr'][$rec['val']]['YA']?></td>
          <td><?if ($rec['sales_value']>0) echo $rec['sales_value'].(($rec['sales_type']==3)? '%':$_SESSION['VL_def_DB_ue'])?></td>
          <td><?if ($rec['__price_4']>0) echo $rec['__price_4']?></td>
      </tr>
    <?
  }
  ?></table><?
  echo 'Заполнены цены для '.$update.' товаров<br>' ;
  //damp_array($res) ;
  //print_2x_arr($list_goods) ;
  return ;
}

?>