<?php

$__functions['init'][]		='_goods_site_vars' ;
$__functions['boot_site'][]	='_goods_site_boot' ;

//include(_DIR_EXT.'/goods_kit/ext_goods_kit.php') ; // подклбючаем к модулю расширение - наборы товаров

function _format_price($price,$options=array()) { if (is_object($_SESSION['goods_system'])) return($_SESSION['goods_system']->format_price($price,$options)) ; else return($price); }
function _goods_tree($id) { if (isset($_SESSION['goods_system']->tree[$id])) return($_SESSION['goods_system']->tree[$id]) ; else return (null);}

function _goods_site_vars() //
{   $_SESSION['TM_goods']='obj_'.SITE_CODE.'_goods' ; $GLOBALS['TM_goods']=&$_SESSION['TM_goods'] ;
    $_SESSION['TM_goods_image']='obj_'.SITE_CODE.'_goods_image' ; $GLOBALS['TM_goods_image']=&$_SESSION['TM_goods_image'] ;


	// валюты сайта --------------------------------------------------------------------------------------------------------------------------------
    // новая система валют
    //массив - описание валют для сайта
    $_SESSION['VL_arr']=array() ;
    $_SESSION['VL_arr'][0]=array('name'=>'р','ue'=>' руб. ','var'=>'LS_kurs_rur','round'=>0,'dec'=>0,'dec_point'=>'.','title'=>'В рублях','YA'=>'RUR') ;
    //$_SESSION['VL_arr'][1]=array('name'=>'$','ue'=>' $ ','var'=>'LS_kurs_dol','round'=>0,'dec'=>2,'dec_point'=>'.','title'=>'В долларах','YA'=>'USD') ;
    //$_SESSION['VL_arr'][2]=array('name'=>'гривна','ue'=>' грн. ','var'=>'LS_kurs_grn','round'=>0,'dec'=>2,'dec_point'=>'.','title'=>'В гривнах','YA'=>'GRN') ;
    //$_SESSION['VL_arr'][3]=array('name'=>'тенге','ue'=>' тнг. ','var'=>'LS_kurs_tgn','round'=>0,'dec'=>2,'dec_point'=>'.','title'=>'В тенге','YA'=>'TGN') ;
    //$_SESSION['VL_arr'][2]=array('name'=>'Э','ue'=>' eur. ','var'=>'LS_kurs_eur','round'=>0,'dec'=>2,'dec_point'=>'.','title'=>'В евро','YA'=>'EUR') ;
    $_SESSION['VL_kurs']=0 ; // валюта, относительно которой заданы курсы в настройках сайта
    $_SESSION['VL_def_DB']=0 ; // валюта, используемая по умолчанию в товарной таблиц (если поле val==null)
    $_SESSION['VL_main']=0 ; // основная валюта на сайте, оформление заказа и цены позиций заказах хранятся именно в этой валюте

    // описание системы товаров  -------------------------------------------------------------------------------------------------------------------
	$_SESSION['init_options']['goods']['debug']=0 ;
	$_SESSION['init_options']['goods']['patch_mode']='TREE_NAME' ;
	$_SESSION['init_options']['goods']['root_dir']	='catalog' ;
    //$_SESSION['init_options']['goods']['usl_show_items']='clss in(2,200) and enabled=1 and _enabled=1' ;
    $_SESSION['init_options']['goods']['clss_items']='200' ;
    $_SESSION['init_options']['goods']['usl_show_items']='enabled=1 and _enabled=1' ;
    //$_SESSION['init_options']['goods']['tree_fields']='pkey,parent,clss,indx,enabled,obj_name,href,_image_name' ;
    $_SESSION['init_options']['goods']['tree_fields']='pkey,parent,clss,indx,enabled,href,obj_name,brand_id,country_id' ;


    // определяем условия для формирование дерева
    $_SESSION['init_options']['goods']['tree']['debug']=0 ;
    $_SESSION['init_options']['goods']['tree']['clss']='1,10,62' ;
	$_SESSION['init_options']['goods']['tree']['include_space_section']=0 ;
    $_SESSION['init_options']['goods']['tree']['order_by']='indx' ;
    $_SESSION['init_options']['goods']['tree']['get_count_by_clss']=1 ;

    // созранить данные по количеству товара в свойстве объета cnt
    // данный массив является устаревшив, так это информация дублируется в cur_cnt_clss, all_cnt_clss
    //$_SESSION['init_options']['goods']['tree']['get_count_by_usl']['usl']='clss in(2,200) and enabled=1' ;
    //$_SESSION['init_options']['goods']['tree']['get_count_by_usl']['flag_name']='cnt' ;
    // ПРАВИЛЬНО:
    //$_SESSION['init_options']['goods']['tree']['get_count_by_usl'][]=array('usl'=>'clss in(2,200) and enabled=1 and _enabled=1 and gold>0','flag_name'=>'is_gold')  ; // подсчет в деревее числа золотых часов


    $_SESSION['init_options']['goods']['pages']['size']=array('20'=>'По 20 позиций','all'=>'Все') ;
	$_SESSION['init_options']['goods']['pages']['sort_type']=array() ;
	$_SESSION['init_options']['goods']['pages']['sort_type'][1]=array('name'=>'По порядку',	'order_by'=>'indx',			'diapazon_title'=>'Позиции:') ;
	$_SESSION['init_options']['goods']['pages']['sort_type'][2]=array('name'=>'По цене',		'order_by'=>'price',		'diapazon_title'=>'Цены:') ;
	$_SESSION['init_options']['goods']['pages']['sort_type'][3]=array('name'=>'По наименованию','order_by'=>'obj_name',	'diapazon_title'=>'Наименования:') ;
	$_SESSION['init_options']['goods']['pages']['sort_type'][4]=array('name'=>'По артикулу',	'order_by'=>'art',			'diapazon_title'=>'Артикул:') ;
    $_SESSION['init_options']['goods']['pages']['sort_mode']=array() ;
    $_SESSION['init_options']['goods']['pages']['sort_mode'][1]='asc' ;
    $_SESSION['init_options']['goods']['pages']['sort_mode'][2]='desc' ;
}

// парамерт options будет передан из session_boot($options) <= show_page($options)
function _goods_site_boot($options=array())
{   $options['debug']=0 ;
	create_system_modul_obj('goods',$options) ; // создаем подсистему "goods"
}

class c_goods_system extends c_system_catalog
{
  // $create_options в подсистему передается как объединение init_options[goods] и options в _goods_site_boot
  function	c_goods_system(&$create_options=array())
  { global $TM_goods;
    // устанавливаем значения опций по умолчанию, если они не были заданы ранее
    if (!$create_options['table_name']) 			$create_options['table_name']=$TM_goods ;
    if (!$create_options['section_page_name']) 	    $create_options['section_page_name']='catalog' ; // названия для префиксной системы страниц
    if (!$create_options['item_page_name']) 	    $create_options['item_page_name']='goods' ; // названия для префиксной системы страниц
    if (!$create_options['root_dir'])				$create_options['root_dir']='catalog' ;
    if (!$create_options['clss_items'])             $create_options['clss_items']='2,200' ;
    if (!$create_options['usl_show_items'])         $create_options['usl_show_items']='enabled=1 and _enabled=1' ;

    if (!$create_options['tree']['order_by'])	    $create_options['tree']['order_by']='indx' ;
    if (!$create_options['tree']['class_name'])		$create_options['tree']['class_name']=(class_exists('c_obj_goods_site'))? 	'c_obj_goods_site':'c_obj_goods' ;


    $this->VL_main_ue=$_SESSION['VL_arr'][$_SESSION['VL_main']]['ue'] ; // обозначение для основной валюты на сайте
    $this->VL_def_DB_ue=$_SESSION['VL_arr'][$_SESSION['VL_def_DB']]['ue'] ; // обозначение для дефаултной валюты в админке


    // при наличии фильтрации условие подсчета товаров в разделах задается в $base_filter_goods
    // если фильтрация не применяется, для подсчета товаров в разделах берем условие из $this->usl_show_items
	//$_SESSION['goods_show_usl']=$this->usl_show_items ;
    //$create_options['debug']=0 ;

    parent::c_system_catalog($create_options) ;

    //damp_array($this) ;

  }

  /*
  // возвращает результаты поиска по стандартному алгоритму в обычных полях товара
  // в принице может вернуть только условие поиска, а выборка может бытб сделана в рамках функции show_list_items
  function get_items_search($usl,$options=array())
  { $text=($options['search_text'])? $options['search_text']:$usl ; // текст для поиска передается либо в первом параметре либо через опции
    if (!$options['search_text']) $usl=$this->usl_show_items ;      // если текст поиска передан через первый пареметр, то обязательное условие отбора берем из
    if (!$text) return ;
	// подготовливает условие для поиска по стандартным полям товара
    $_usl[]=$this->prepare_search_usl('art','like%%',$text) ;
    $_usl[]=$this->prepare_search_usl('obj_name','like%%',$text) ;
    $_usl[]=$this->prepare_search_usl('pkey','=',$text) ;
    $_usl[]=$this->prepare_search_usl('manual','like%%',$text) ;
    $res_usl='('.implode(' or ',$_usl).') and '.$this->usl_show_items ;
    if ($usl) $res_usl.=' and '.$usl ;
    if ($options['get_only_usl']) return($res_usl) ; //echo $usl.'<br>' ;
    $list_obj=$this->get_items($res_usl,$options) ;
    return($list_obj) ;
  }

  // возвращает результаты обычного поиска по артикулу, коду или наименованию
  function get_items_search_fast($usl,$options=array())
  { $text=($options['search_text'])? $options['search_text']:$usl ; // текст для поиска передается либо в первом параметре либо через опции
    if (!$options['search_text']) $usl=$this->usl_show_items ;      // если текст поиска передан через первый пареметр, то обязательное условие отбора берем из
    if (!$text) return ;
    // подготовливает условие для поиска по стандартным полям товара
    $_usl[]=$this->prepare_search_usl('art','like%%',$text) ;
    $_usl[]=$this->prepare_search_usl('obj_name','like%%',$text) ;
    $_usl[]=$this->prepare_search_usl('pkey','=',$text) ;
    $res_usl='('.implode(' or ',$_usl).')' ;
    if ($usl) $res_usl.=' and '.$usl ;
    if ($options['get_only_usl']) return($res_usl) ;
    $list_obj=$this->get_items($res_usl,$options) ;
    return($list_obj) ;
  }

  function get_cnt_items_search_fast($usl,$options=array())
  { $options['get_only_usl']=1 ;  // чтобы не дублировать формирование условий поиска
    $usl=$this->get_items_search_fast($usl,$options) ;
    $cnt=$this->get_cnt_items($usl,$options) ;
    return($cnt) ;
  }


  //возвращает результаты fulltext поиска
  function get_items_search_fulltext($text,$options=array())
  { $mode=' in boolean mode' ;
    $fields='art,obj_name' ;
    $list_recs=execSQL('select pkey,art,obj_name,match('.$fields.') against (\''.$text.'\''.$mode.') as score from '.$this->table_name.' where match('.$fields.') against (\''.$text.'\''.$mode.') and clss=200 order by score desc limit 0,10',2) ;
    return($list_recs) ;
  }

  //возвращает результаты аналитического поиска
  // сначала ищется по артикулу, потом по коду, потом по наименованию, потом по описанию
  // эта функция не может вернуть условие поиска - только результаты, так как результат объединяет несколько поисков по разным полям
  function get_items_search_anal($text,$options=array())
  { $list_recs=array() ;
    $this->search_anal_merge_result($list_recs,$this->prepare_search_usl('art','like%',$text)) ;
    $this->search_anal_merge_result($list_recs,$this->prepare_search_usl('pkey','=',$text)) ;
    $this->search_anal_merge_result($list_recs,$this->prepare_search_usl('art','like%%',$text)) ;
    $this->search_anal_merge_result($list_recs,$this->prepare_search_usl('obj_name','like%%',$text)) ;
    $this->search_anal_merge_result($list_recs,$this->prepare_search_usl('manual','like%%',$text)) ;
	return($list_recs);
  }

  function search_anal_merge_result(&$list_recs,$usl)
  { if (!$usl) return ;
    $list_recs_next=$this->get_items($usl,array('debug'=>2));
    if (sizeof($list_recs_next)) foreach($list_recs_next as $pkey=>$rec) if (!$list_recs[$pkey]) $list_recs[$pkey]=$rec ;
  }
  */

 function dop_save_info(&$obj,&$rec)
  {
    //$obj->title=($rec['title'])? $rec['title']: $obj->name.' копии часов, наручные часы '.$obj->name.', каталог '.$obj->name ;
    //$obj->description=($rec['description'])? $rec['description']:'В нашем магазине вы найдете 100% копии часов известного часового бренда '.$obj->name.' европейского производства. Самые последние и модные коллекции.';
    //$obj->keywords=($rec['keywords'])? $rec['keywords']:$obj->name.' копии часов, наручные часы '.$obj->name.', каталог '.$obj->name;
  }


  function get_name_obj(&$rec)
  {
     if ($rec['obj_name']) return($rec['obj_name']) ;
     else return($this->tree[$rec['parent']]->name.' # '.$rec['pkey']) ;
  }

  // расчет цены товара с учетом скидки и коэффициэнта наценки
 // данная функция будет буфером между ценой в базе данных и всеми функциями использующими цену (добавление в корзину, вывод на экран и т.д.)

 // $obj_info - информация по товару, используются поля price,koof,val,sales_type,sales_value
 // round_to - глобальная переменная, значение до которого необходимо округлить цену (до 10,100,1000) (только для главной валюты сайта!!!)
 // для учета скидки должна быть установлена опция "use_sales"!!!!

 // все расчеты ведуться в главной валюте сайта - $VL_main
 // внимание!!! для окончательного вывода цены необходимо использовать функцию format_price которая учитывает курс

 // price_1 - цена товара по базе в валюте

 // конвертирует цену из валюты val_from в валюту val_to
 // дано: price=100 eur
 //       курс: 44р (задан относительно рубля)
 // перевести в доллары
 // решение:
 // 1. Вычисляем коэф.перевода = соотшение курсов обех валют
 //
 function convert_price($price_from,$val_from,$val_to,$options=array())
 {  global $debug_price_convert ;
 	$var_name_from=$_SESSION['VL_arr'][$val_from]['var'] ;
 	if (!$_SESSION[$var_name_from]) $_SESSION[$var_name_from]=1 ;
  	$kurs_from=$_SESSION[$var_name_from] ;
  	if ($debug_price_convert) echo 'Курс '.$var_name_from.' = '.$kurs_from.'<br>' ;

  	$var_name_to=$_SESSION['VL_arr'][$val_to]['var'] ;
  	if (!$_SESSION[$var_name_to]) $_SESSION[$var_name_to]=1 ;
  	$kurs_to=$_SESSION[$var_name_to] ;
  	if ($debug_price_convert) echo 'Курс '.$var_name_to.' = '.$kurs_to.'<br>' ;

  	$koof=$kurs_from/$kurs_to ;
  	if ($debug_price_convert) echo 'koof = '.$koof.'<br>' ;

  	$price_to=$price_from*$koof ;
  	if ($debug_price_convert) echo 'Цена в '.$var_name_from.' = '.$price_from.'<br>' ;
  	if ($debug_price_convert) echo 'Цена в '.$var_name_to.' = '.$price_to.'<br>' ;

    return($price_to) ;
 }

 // пересчитывает цену из БД в цену на сайте с учетом наценки и текущей валюты
 // внимание! указание валюты цены - обязательно, если цена в базовой валюте - передавать $VL_main
 function exec_any_price($price,$val,$koof=1,$options=array())
 { global $debug_price ;
   if ($debug_price) $options['debug']=1 ;
   if ($options['debug']) echo '<br><br><strong>Начинаем отладку exec_any_price</strong><br>' ;

   if (!$koof or $koof=='0.00') $koof=1 ;

   // переводим цену товара price в валюте val в главную валюту
   $price_1=$this->convert_price($price,$val,$_SESSION['VL_main'],$options) ;
   if ($options['debug']) echo 'Главная валюта: '.$_SESSION['VL_main'].'<br>' ;
   if ($options['debug']) echo 'Цена 1 - цена в главной валюте : '.$price_1.' '.$this->VL_main_ue.'<br>' ;

   // учитывем наценку на товар - она может быть как >1, так и <1
   $price_2=$price_1*$koof ;
   if ($options['debug']) echo 'Цена 2 - цена c наценкой в главной валюте : '.$price_2.' '.$this->VL_main_ue.'<br>' ;

   return($price_2) ;

 }

 // Возвращаем цену в текущей валюте. Не учитывает скидки, наценку. Возвращает неформатирование число.
 // price   - цена товара
 // val     - валюта цены товара
 function get_price($price,$val=-1)
 { if ($val==-1) $val=$_SESSION['VL_def_DB'] ;
   $price=$this->convert_price($price,$val,$_SESSION['VL_main']) ;
   return($price) ;
 }

 // вписывает в массив информации по объекту дополнительные поля по цене товара
 // 25.08.11 -  переработка функции из-за некорректного учета скидок для авторизованного клиента
 //             внимание!! ранее для расчета скидок было необходимо ОБЯЗАТЕЛЬНО указывать опцию use_sales. Теперь эта опция включается автоматически, если цена берется из поля price

 // $obj_info['__price_1']   - 	Цена 1 - цена в главной валюте
 // $obj_info['__price_2'] 	 -  Цена 2 - цена c наценкой в главной валюте
 // $obj_info['__price_3']   = price2
 // $obj_info['__price_4']   =  Цена 4 - итоговая цена c наценкой в главной валюте со скидкой и округлением   -

 // опции:
 // field_price_name
 // price_min
 // sales_type
 // sales_value
 // use_sales

 //



 function exec_price(&$obj_info,$options=array())
 { global $debug_price,$member ;
   //$options['test']=1 ;
   if ($debug_price) { echo '<br><br><strong>Начинаем отладку EXEC_PRICE для : #'.$obj_info['pkey'].' '.$obj_info['obj_name'].'</strong><br>' ; damp_array($options) ; echo '<br>'; }

   $fname=($options['field_price_name'])? $options['field_price_name']:'price';
   $use_sales=($fname=='price')? 1:$options['use_sales'] ;

   // наценка на товар или процент к курсу ЦБ - задавать необходимо только одно. Если задан процент ЦБ, то наценка не учитывается
   $koof=$obj_info['koof'] ; if (!$koof or $koof=='0.00') $koof=1 ;
   $koof_CB=$obj_info['koof_CB'] ; if (!$koof_CB or $koof_CB=='0.00') $koof_CB=0 ;
   if ($koof_CB!=0) $koof=$koof+$koof_CB/100 ;


   $use_val=(isset($obj_info['val']))? $obj_info['val']:$_SESSION['VL_def_DB'] ;
   if ($options['round_to']) $round_to=$options['round_to'] ; else $round_to=$_SESSION['VL_arr'][$use_val]['round'] ;
   if ($debug_price) echo '<strong>Исходные данные:</strong><br>Цена товара по базе: '.$obj_info[$fname].'<br>Валюта цены товара: '.$use_val.'<br>Округление цены товара, до : '.$round_to.'<br>Тип скидки на товар: '.$obj_info['sales_type'].'<br>Значение скидки на товар: '.$obj_info['sales_value'].'<br>Скидка клиента: '.$member->info['sales_value'].' %<br>' ;

   // переводим цену товара в базувую валюту
   if ($debug_price) echo 'Главная валюта: '.$_SESSION['VL_main'].print_r($_SESSION['VL_arr'][$_SESSION['VL_main']],true).'<br>' ;
   $price_1=$this->convert_price($obj_info[$fname],$use_val,$_SESSION['VL_main']) ;
   if ($debug_price) echo 'Цена 1 ( __price_1 ) - цена в главной валюте : '.$price_1.' '.$this->VL_main_ue.'<br>' ;
   $obj_info['__price_1']=$price_1 ;

   // учитывем наценку на товар - она может быть как >1, так и <1
   $price_2=$price_1*$koof ;     //echo 'price_2='.$price_2.'<br>' ;
   if ($debug_price) echo 'Цена 2 ( __price_2 ) - цена c наценкой в главной валюте : '.$price_2.' '.$this->VL_main_ue.'<br>' ;
   $obj_info['__price_2']=$price_2 ;

   // если задано ограничение по минимальной цене, то применяем его
   if ($options['price_min'] and $price_2<$options['price_min']) $price_3=$options['price_min'] ; else $price_3=$price_2 ;
   if ($debug_price) echo 'Цена 3 ( __price_3 ) - цена c наценкой в главной валюте + ограничения на минимальную цену : '.$price_3.' '.$this->VL_main_ue.'<br>' ;

     if ($debug_price) echo '<strong>Производим учет округления</strong><br>' ;
     $dec=$_SESSION['VL_arr'][$use_val]['dec'] ;
     if ($debug_price) echo 'Число точек после запятой:'.$dec.'<br>' ;

     // округляем цену в соотвествии с настройками для базовой валюты
     $price_3=iconv('windows-1251','utf-8',$price_3) ; // это важно ;
     if ($round_to) $price_3=(round($price_3/$round_to))*$round_to ;
     $price_3=round((float)$price_3,$dec) ;


   $obj_info['__price_3']=$price_3 ;


   // учет скидки на товар, товарной или клиентской. Клиентская скидка только в процентах, товарная в рублях или процентах
   // 2 - скидка клиента на данный товар не распостраняется
   // 3 - скидка на товар в процентах
   // 4 - скидка на товар в указанной сумме
   if ($use_sales)
   { if ($debug_price) echo '<strong>Производим учет скидок</strong><br>' ;

     $sales_type=($options['sales_type'])?  $options['sales_type']:$obj_info['sales_type'] ;
   	 $sales_value=($options['sales_value'])? $options['sales_value']:$obj_info['sales_value'] ;
   	 $member_sales=($options['member_sales'])?  $options['member_sales']:$member->info['sales_value'] ;
   	 $sales_value_descr=($options['sales_value_descr'])?  $options['sales_value_descr']:$member->info['sales_value_descr'] ;

     if ($debug_price and $sales_type) echo 'Тип скидки на товар: '.$sales_type.'<br>' ;
     if ($debug_price and $sales_value) echo 'Размер скидки на товар: '.$sales_value.'<br>' ;
     if ($debug_price and $member_sales) echo 'Скидка клиента на товар: '.$member_sales.' %<br>' ;
     if ($sales_type==2 and $member_sales) {$member_sales=0 ;  if ($debug_price) echo '<span class=red>Скидка на товар запрещены</span><br>' ; $obj_info['__price_used_sales']='denited' ; }

     $obj_info['__price_4']=$price_3 ; // итоговая цена пока без скидок

     // считаем скидку на товар c учетом скидки по акции
     // внимание!!!!
     // название полей
     // __price_used_sales_text - текст по размеру и источнику скидки. Выводиться в корзине и в письме-уведомлении
     // __price_used_sales_value -  размер скидки в остновной валюте сайта (100 руб)
     // __price_action_sales_text - размер скидки по акции (10% или 100 руб)

     if ($sales_type==3 and $sales_value)  { $sales_action_value=$price_3/100*$sales_value ;
                                             $obj_info['__price_4_sales_action']=$price_3-$sales_action_value ;
                                             $obj_info['__price_4']=$obj_info['__price_4_sales_action'] ;
                                             $obj_info['__price_used_sales']='action' ;
                                             //$obj_info['__price_sales_value']=$sales_value.' %' ; нигде не исполузется
                                             $obj_info['__price_used_sales_value']=$sales_action_value ;
                                             if (!$obj_info['__price_used_sales_text']) $obj_info['__price_used_sales_text']=round($sales_value).' %' ;   // выводиться в m_orders:prepare_price_info $order_info['sales_title']='Скидка на товар :<br><strong>'.$rec['__price_used_sales_text'].'</strong>' ;
                                             $obj_info['__price_action_sales_text']=round($sales_value).' %' ;
                                             if ($debug_price) echo '<span class=green>Цена 4 акция</span> - цена c наценкой в главной валюте со скидкой клиента и округлением: '.$obj_info['__price_4_sales_action'].' '.$this->VL_main_ue.'<br>' ;
                                           }
     if ($sales_type==4 and $sales_value)  {  // переводим размер скидки из текущей валюты в базовую
                                              $sales_action_value=$this->convert_price($sales_value,$use_val,$_SESSION['VL_main']) ;
                                              $obj_info['__price_4_sales_action']=$price_3-$sales_action_value ;
                                              $obj_info['__price_4']=$obj_info['__price_4_sales_action'] ;
                                              $obj_info['__price_used_sales']='action' ;
                                              $obj_info['__price_used_sales_value']=$sales_action_value ;
                                              if (!$obj_info['__price_used_sales_text']) $obj_info['__price_used_sales_text']=$sales_action_value.' '.$this->get_cur_val_ue() ;
                                              $obj_info['__price_action_sales_text']=$sales_action_value.' '.$this->get_cur_val_ue() ;
                                              if ($debug_price) echo '<span class=green>Цена 4 акция</span> - цена c наценкой в главной валюте со скидкой клиента и округлением: '.$obj_info['__price_4_sales_action'].' '.$this->VL_main_ue.'<br>' ;
                                           }

     if ($options['func_exec_sales'] and function_exists($options['func_exec_sales'])) $options['func_exec_sales']($obj_info) ;


     // получаем цену клиента - клиенкая скидка предоставляется если только скидки на товар не запрещены (sales_type!=2)
     if ($member_sales) { $sales_member_value=$price_3/100*$member_sales ;
                          $obj_info['__price_4_sales_member']=$price_3-$sales_member_value ;
                          // если на товар нет скидки по акции, то однозначно используем используем скидку клиента
                          if (!$obj_info['__price_4_sales_action']) $obj_info['__price_4']=$obj_info['__price_4_sales_member'] ;
                          // если уже есть скидка по акции, но цена с ней больше чем со скидкой клиента, то используем скидку клиента
                          else if ($obj_info['__price_4_sales_member']<$obj_info['__price_4_sales_action']) $obj_info['__price_4']=$obj_info['__price_4_sales_member'] ;
                          if ($obj_info['__price_4']==$obj_info['__price_4_sales_member'])
                          { $obj_info['__price_used_sales']='member' ;
                            $obj_info['__price_used_sales_value']=$sales_member_value ;
                            $obj_info['__price_used_sales_text']=$member_sales.' %' ;
                          }
                          $obj_info['__price_member_sales_text']=$member_sales.' %' ;
                          $obj_info['__price_used_sales_descr']=$sales_value_descr ; // описание скидки клиента
                          if ($debug_price) echo '<span class=green>Цена 4 клиента</span> - цена c наценкой в главной валюте со скидкой клиента и округлением: '.$obj_info['__price_4_sales_member'].' '.$this->VL_main_ue.'<br>' ;
                        }

   }
   else
   { if ($debug_price) echo '<strong>Учет скидок не производился</strong><br>' ;
     $obj_info['__price_4']=$price_3 ;
   }

   if ($debug_price) echo 'Цена 4 ( __price_4 )- цена c наценкой в главной валюте со скидкой и округлением: '.$obj_info['__price_4'].' '.$this->VL_main_ue.'<br>' ;
   if ($debug_price)
   {   echo '<strong>Заполнение полей цены в массиве по товару:</strong><br>' ;
       if (sizeof($obj_info)) foreach($obj_info as $name=>$value) if (strpos($name,'__price')!==false) echo $name.'='.$value.'<br>' ;
       //damp_array($obj_info) ;
   }

   return($obj_info['__price_4']) ;
 }

 // получает цену в главной валюте сайта
 // возвращает цену, отформатированную в текущей валюте
  function format_price($price,$options=array())
  { global $cur_val,$debug_price_format ;
    //if ($debug_price_format) trace() ;
    if ($debug_price_format) echo 'Цена до форматирования:'.$price.'<br>' ;
    if (!isset($cur_val)) $cur_val=$_SESSION['VL_main'] ;

    $use_val=(isset($options['use_val']))? $options['use_val']:$cur_val ;
    if ($options['use_def_val']) $use_val=$_SESSION['VL_main'] ;

    if ($debug_price_format) echo 'Текущая валюта цены:'.$use_val.'<br>' ;
    if ($debug_price_format) echo 'Главная валюта сайта:'.$_SESSION['VL_main'].'<br>' ;
    $member_price=$this->convert_price($price,$_SESSION['VL_main'],$use_val) ;
    if ($debug_price_format) echo 'Цена после перевода в главную валюту сайта:'.$member_price.'<br>' ;

    $ue=get_cur_lang_field($_SESSION['VL_arr'][$use_val],'ue') ;
    $dec=$_SESSION['VL_arr'][$use_val]['dec'] ;
    //$round=$_SESSION['VL_arr'][$use_val]['round'] ;
    $dec_point=$_SESSION['VL_arr'][$use_val]['dec_point'] ;
    if ($debug_price_format) echo 'Разделитель:'.$dec_point.'<br>' ;
    if ($debug_price_format) echo 'Число точек после запятой:'.$dec.'<br>' ;
    $res=number_format($member_price,$dec,$dec_point,' ') ;
    if ($debug_price_format) echo 'Цена после форматирования:'.$res.'<br>' ;
    if (!$options['no_ue']) $res.=' <span>'.$ue.'</span>' ;
    if (!$price>0 and $options['null_price_text']) $res=$options['null_price_text'] ;
    if (!$price>0 and $options['use_if_null']) $res=$options['use_if_null'] ;
    return($res) ;
  }

  // возвращает обозначение текущей валюты
  function get_cur_val_ue()
  { global $cur_val ;
    return(get_cur_lang_field($_SESSION['VL_arr'][$cur_val],'ue')) ;
  }

 function prepare_public_info(&$rec,$options=array())
	{   parent::prepare_public_info($rec,$options) ;   // в классе catalog_system будут заполнены поля __href, __name, __data, __img_alt
        //echo 'href='.$rec['__href'].'<br>';
	    //echo 'name='.$rec['__name'].'<br>';
        if (is_object($this->tree[$rec['parent']])) $rec['__parents']=$this->tree[$rec['parent']]->get_arr_parent() ;
        $rec['__art']=($rec['art'])? $rec['art']:$rec['pkey']  ;
	    //if ($rec['parent']) $rec['__parents_name']=$this->tree[$rec['parent']]->get_arr_parent_name() ;

		// вывод цены со скидкой на товар
        $this->exec_price($rec) ;
        if ($rec['__price_4']>0)
        { if ($rec['__price_used_sales']=='action' or $rec['__price_used_sales']=='member') // если исподьзуется скидка по акции
	       $rec['__price_5']='<span class=price_alt> '.$this->format_price($rec['__price_3'],array('no_ue'=>1)).' '.'</span><span class=price_new> '.$this->format_price($rec['__price_4']).'</span>' ;
	      else $rec['__price_5']=$this->format_price($rec['__price_4']);
        } else $rec['__price_5']='' ;

        $rec['__stock_info']=$rec['stock'];
        $rec['__img_alt']=$this->tree[$rec['parent']]->name.' - '.$rec['obj_name'];

        // дополнительные данные по товару
        // __brand, __brand_href - название и ссылка на бренд
        // __collection, __collection_href - название и ссылка на коллекцию
        // __country - название страны
        // раскрываем название бренда
        if (isset($this->tree[$rec['parent']]))
        {   $brand_id=$rec['brand_id'] ;
            if ($brand_id) { $rec['__brand']=$this->tree[$brand_id]->name; $rec['__brand_href']=$this->tree[$brand_id]->href ;  }
            // раскрываем название страны - задано в бренде или в самом товаре
            if ($rec['country_id']) $rec['__country']=$_SESSION['IL_country'][$rec['country_id']]['obj_name'] ;
            // раскрываем название и ссылку на коллекцию
            $collection_id=$rec['series_id'] ;
            if ($collection_id) { $rec['__collection']=$this->tree[$collection_id]->name;  $rec['__collection_href']=$this->tree[$collection_id]->href ; }
        }
        //$rec['__ed_izm']=$_SESSION['IL_goods_types'][$rec['type']]['ed_izm'] ; //единица измерения по основному типу товара

  }

  function add_goods_to_overview($id)
  { // ищем в массиве позицию, если товар был сохранен ранее
    //$_SESSION['last_view']=array() ;
    $indx=array_search($id,$_SESSION['last_view']) ;
    if ($indx!==false) unset($_SESSION['last_view'][$indx]) ;
    // сохраняем в конце списка
    $_SESSION['last_view'][]=$id ;
  }

  function add_section_to_overview($id)
  { // ищем в массиве позицию, если товар был сохранен ранее
    //$_SESSION['last_view']=array() ;
    $indx=array_search($id,$_SESSION['last_section']) ;
    if ($indx!==false) unset($_SESSION['last_section'][$indx]) ;
    // сохраняем в конце списка
    $_SESSION['last_section'][]=$id ;

  }

  // возвращает список из cnt количества товара из раздела section_id (и его дочерних разделов) с ценой, меньше цены price, список отсортирован от меньшей цены к большей
  function get_list_goods_menee_price($price,$options=array())
  { if ($options['usl_select'])       $usl_select=$options['usl_select'] ;
    else if ($options['section_id'])  $usl_select=$this->usl_show_items.' and parent in ('.$this->tree[$options['section_id']]->get_list_child().')' ;
    else                              $usl_select=$this->usl_show_items ;
                                      $usl_select.=' and price<'.$price ;
    if ($options['dop_usl_select'])   $usl_select.=' and '.$options['dop_usl_select'] ;
    $cnt=($options['count'])? $options['count']:1 ;
    $list_goods=execSQL('select * from '.$this->table_name.' where '.$usl_select.' order by price desc limit '.$cnt) ;
    fill_field_image_name($list_goods) ;  // временно, пока не будут сдела вьюверы
    $arr=array_reverse($list_goods,true) ;
    $this->prepare_public_info_for_arr($arr) ;
    return($arr) ;
  }

  // возвращает список из cnt количества товара из раздела section_id (и его дочерних разделов) с ценой, меньше цены price, список отсортирован от меньшей цены к большей
  function get_list_goods_bolee_price($price,$options=array())
  { if ($options['usl_select'])       $usl_select=$options['usl_select'] ;
    else if ($options['section_id'])  $usl_select=$this->usl_show_items.' and parent in ('.$this->tree[$options['section_id']]->get_list_child().')' ;
    else                              $usl_select=$this->usl_show_items ;
                                      $usl_select.=' and price>'.$price ;
    if ($options['dop_usl_select'])   $usl_select.=' and '.$options['dop_usl_select'] ;
    $cnt=($options['count'])? $options['count']:1 ;
    $list_goods=execSQL('select * from '.$this->table_name.' where '.$usl_select.' order by price  limit '.$cnt) ;
    fill_field_image_name($list_goods) ;    // временно, пока не будут сдела вьюверы
    $this->prepare_public_info_for_arr($list_goods) ;
    return($list_goods) ;
  }


    function generate_page_url($page,$options=array())
     { // номер страницы передается в виде /page_2.html
       if (_PAGE_NUMBER_SOURCE=='in_name')
       { if ($options['pages_base_url']) $url=$options['pages_base_url'] ;
         else                            $url=_CUR_PAGE_PATH ;

         list($from_page,$url)=extract_page_number_from_path($url) ;
         $page_info=parse_url_path($url) ;
         //echo 'page='.$page.'<br>url='.$url.'<br>' ; damp_array($page_info,1,-1) ;
         $new_url=$page_info['_CUR_PAGE_DIR'] ;
         if ($page>1)                    $new_url.='page_'.$page.'.html' ;
         elseif ($page=='all')           $new_url.='page_all.html' ;
         if ($page_info['_CUR_PAGE_QUERY'])   $new_url.='?'.$page_info['_CUR_PAGE_QUERY'] ;
         //echo '$new_url='.$new_url.'<br>' ;
       }
       // номер страницы передается в виде /?page=2
       else
       { if ($options['pages_base_url']) $new_url=$this->generate_url(array('page'=>$page),$options['pages_base_url']) ;
         else                            $new_url=$this->generate_url(array('page'=>$page)) ;

       }

       $new_url=str_replace('/ua','',$new_url) ;
       return($new_url) ;
     }

}

//-----------------------------------------------------------------------------------------------------------------------------------
//
// ОБЪЕКТЫ - элементы дерева
//
//-----------------------------------------------------------------------------------------------------------------------------------

class c_obj_goods extends c_obj {}

?>