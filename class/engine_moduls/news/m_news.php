<?php

$__functions['init'][]		='_news_site_vars' ;
$__functions['boot_site'][]	='_news_site_boot' ;

// функции быстрого доступа
function _show_last_news($func_name,$options=array()) { $_SESSION['news_system']->show_last_items($func_name,$options) ;}


function _news_site_vars()
{   $_SESSION['TM_news']='obj_'.SITE_CODE.'_news' ;

    $_SESSION['ARR_news_themes']=execSQL_row('select pkey,obj_name from '.$_SESSION['TM_news'].' where clss in (1,10) and enabled=1 order by obj_name') ;

    // описание системы  -------------------------------------------------------------------------------------------------------------------
	$_SESSION['init_options']['news']['debug']=0 ;
    $_SESSION['init_options']['news']['patch_mode']	='TREE_NAME' ;
    $_SESSION['init_options']['news']['root_dir']	='news' ;
    $_SESSION['init_options']['news']['tree']['debug']=0 ;
    $_SESSION['init_options']['news']['tree']['order_by']	='c_data desc' ;
    $_SESSION['init_options']['news']['tree']['get_count_by_clss']=0 ;
    $_SESSION['init_options']['news']['pages']['size']=array('10'=>'По 10 новостей','20'=>'По 20 новостей','all'=>'Все') ;
    $_SESSION['init_options']['news']['pages']['sort_type']=array() ;
    $_SESSION['init_options']['news']['pages']['sort_type'][1]=array('name'=>'По дате',	'order_by'=>'c_data desc',	'diapazon_title'=>'Дата:') ;
    $_SESSION['init_options']['news']['pages']['sort_type'][2]=array('name'=>'По порядку',	'order_by'=>'indx',			'diapazon_title'=>'Позиции:') ;
    $_SESSION['init_options']['news']['pages']['sort_type'][3]=array('name'=>'По наименованию','order_by'=>'obj_name',	'diapazon_title'=>'Наименования:') ;
}


function _news_site_boot($options)
{
	create_system_modul_obj('news',$options) ; // создание объекта news_system
}

//-----------------------------------------------------------------------------------------------------------------------------------------------
//
// система новостей
//
//-----------------------------------------------------------------------------------------------------------------------------------------------


class c_news_system extends c_system_catalog
{ public $root_reffer ;
  function	c_news_system($create_options=array())
  { if (!$create_options['table_name']) 			$create_options['table_name']=$_SESSION['TM_news'] ;
    if (!$create_options['usl_show_items']) 		$create_options['usl_show_items']='clss=9 and enabled=1' ;
    if (!$create_options['root_dir'])				$create_options['root_dir']='news' ;
    if (!$create_options['order_by'])	            $create_options['order_by']='indx' ;
    parent::c_system_catalog($create_options) ;
    $this->root_reffer='1.'.$this->tkey ;
  }

  // перекрываем функцию опереления объекта - в url может быть передаено время выборки новостей
  function select_obj_info_by_tree($_CUR_PAGE_DIR,$_CUR_PAGE_NAME='')
  {   $t1=0 ; $t2=0 ;
      $arr=explode('/',$_CUR_PAGE_DIR) ;
      $last_dir=array_pop($arr) ; // будет пусто, так как в конце $_CUR_PAGE слеш
      $last_dir=array_pop($arr) ; // вощможно дата новости
      //echo 'last_dir='.$last_dir.'<br>' ;
      $arr_data=explode('_',$last_dir) ;
      // в дате должно быть до три составляющие
      if (sizeof($arr_data))
      { $res=true ;
        foreach($arr_data as $value) $res=($res and is_numeric($value)) ;
        // если в массиве все числа
        if ($res)
        { if (sizeof($arr_data)==1) // передан только год
          { $t1	= mktime(0, 0, 0, 1, 1, $arr_data[0]); // время начала года
            $t2	= mktime(0, 0, 0, 1, 1, $arr_data[0]+1)-1; // окончание года
          }
          if (sizeof($arr_data)==2) // передан год и месяц
          { $t1	= mktime(0, 0, 0, $arr_data[1], 1, $arr_data[0]); // время начала месяца
            $t2	= mktime(0, 0, 0, $arr_data[1]+1, 1, $arr_data[0])-1; // окончание месяца
          }
          if (sizeof($arr_data)==3) // передан год, месяц и число
          { $t1	= mktime(0, 0, 0, $arr_data[1], $arr_data[2], $arr_data[0]); // время начала месяца
            $t2	= mktime(23, 59, 59, $arr_data[1], $arr_data[2], $arr_data[0]); // окончание месяца
          }
          if ($t1 and $t2)
          { //echo date('d.m.Y H:i:s',$t1).'<br>' ;
            //echo date('d.m.Y H:i:s',$t2).'<br>' ;
            $_CUR_PAGE_DIR=implode('/',$arr).'/' ;
            //echo '$_CUR_PAGE_DIR='.$_CUR_PAGE_DIR.'<br>' ;
            //echo '$_CUR_PAGE_NAME='.$_CUR_PAGE_NAME.'<br>' ;
          }
        }

      }
     parent::select_obj_info_by_tree($_CUR_PAGE_DIR,$_CUR_PAGE_NAME) ;
     if ($t1) $GLOBALS['obj_info']['data_from']=$t1 ;
     if ($t2) $GLOBALS['obj_info']['data_to']=$t2 ;
  }

  // готовим информацию по новости
  function prepare_public_info(&$rec,$options=array())
  { $rec['__href']=$this->get_patch_item($rec) ;
  	$rec['__name']=ltrim($rec['obj_name'],'&nbsp;') ;
  	$rec['__data']=date("d.m.y",$rec['c_data']) ;
  	$arr_data=getdate($rec['c_data']) ;
  	$rec['__data_1']=$arr_data['mday'].' '.$_SESSION['list_mon_short_names'][$arr_data['mon']].' '.$arr_data['year'] ;

    //if (!$rec['value']) $rec['value']=$rec['annot'];

  	//if ($rec['mess'] and !$rec['value']) { $rec['value']=$rec['mess'] ; unset($rec['mess']) ;}// совместимость со старой версией
  	if ($rec['value'] or $rec['annot'])
  	{ 	$annot=ltrim($rec['annot'],'&nbsp;') ;
  	    $value=ltrim($rec['value'],'&nbsp;') ;
  	    // 6.10.2010 - переделан алгоритм формирования аннотации
  	    // если аннотация не прописана, текст аннотации берется из текста новости
  	    // $options['annot_strip_tags'] - удалить мета теги из текста новости
  	    // $options['annot_count']      - максимальный размер аннотации, получаемый из содержания нвости
  	    // ВНИМАНИЕ! если устанавливается значение для 'annot_count' то поле 'annot_strip_tags' автоматически устанавливается в 1 - чтобы не обрезать закрывающие теги в тексте статьи
  	    //if ($options['annot_count']) $options['annot_strip_tags']=1 ;
  	    //if ($annot) $rec['__annot']=$annot ;
  	    //else if (!$options['no_use_value_to_annot']) // 19.11.2010 - добавлена опция запрета использования текста новости для аннатации
  	    //        $rec['__annot']=($options['annot_strip_tags'])? strip_tags($value):$value ;

        // если указана максимальная длина аннотации, сокращаем анотации до необходимой длины
        //if ($options['annot_count'] and strlen($rec['__annot'])>$options['annot_count']) $rec['__annot']=mb_substr($rec['__annot'],0,$options['annot_count'],'UTF-8').' ...' ;
        //$rec['__value']=$value ;
    }
    //damp_array($rec) ;
  }

  function get_array_search_usl($text)
  { $_usl=array() ;
    $_usl[]=$this->prepare_search_usl('obj_name','like%%',$text) ;
    $_usl[]=$this->prepare_search_usl('value','like%%',$text) ;
    $_usl[]=$this->prepare_search_usl('annot','like%%',$text) ;
    $_usl[]=$this->prepare_search_usl('pkey','=',$text) ;
    return($_usl) ;
  }

  function get_count($usl_select='')
    { $sql='select count(pkey) as cnt from '.$this->table_name.' where clss=9 ' ;
      if ($usl_select) $sql.=' and '.$usl_select ;
      $cnt=execSQL_value($sql) ;
      return($cnt) ;
    }

}

?>