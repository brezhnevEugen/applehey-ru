<?php
include_once(_DIR_TO_ENGINE."/admin/c_site.php") ;
include_once(_DIR_TO_ENGINE."/admin/i_clss_func.php") ;
include_once(_DIR_TO_ENGINE.'/admin/c_menu_tabs.php') ;

class c_editor_news extends c_editor_obj
{
  function body($options=array()) { $this->body_frame_2x(array('fra_tree'=>_PATH_TO_ADMIN.'/editor_news_tree.php')) ; }
}


class c_editor_news_tree extends c_fra_tree
{ public $system_name='news_system' ; // временно, пока во всех  editor_news_viewer.php не будет  прописана эта опция: $options['system']='news_system' ;

 function body($options=array())
 {?><body id=tree_objects><div id=tree_menu><?$this->top_menu() ;?></div><?
     $this->generate_time_tree('Лента новостей',$this->tkey,array('usl_select'=>'clss=9','on_click_script'=>'editor_news_viewer.php','on_click_item'=>array(9=>'fra_viewer.php')));
  ?></body><?
 }

}


class c_editor_news_viewer extends c_fra_based
{
  public $top_menu_name='top_menu_list_news' ;
  public $system_name='news_system' ; // временно, пока во всех  editor_news_viewer.php не будет  прописана эта опция: $options['system']='news_system' ;

 function body(&$options=array()) {$this->body_frame($options) ; }

 // заголовок фрейма
 function show_page_obj_header()
 { $title=($_GET['pkey']=='root')? 'Новости':'Новости '.$this->get_time_title($this->start_time,$this->end_time) ;
   ?><p class='obj_header'><?echo $title?></p><?
 }

 // меню фрейма
 function show_page_obj_menu($obj_info)
    {  // создаем объект top_menu
       if (!is_object($_SESSION[$this->top_menu_name])) $_SESSION[$this->top_menu_name]=new c_top_menu() ;
       // формируем набор пунктом меню
       //damp_array($this) ;
       $menu_set=array() ;
       if ($this->pkey=='root')  // пункты меню для корня дерева
       { $menu_set[]=array("name" => 'Новая новость',"cmd" => 'new_news') ;
         $menu_set[]=array("name" => 'Темы новостей',"cmd" => 'news_themes') ;
         //$menu_set[]=array("name" => 'Помошь',"cmd" => 'help') ;
       }
       else // выбран один из временных интервалов
       { $time_usl=$this->get_time_usl($this->start_time,$this->end_time) ;
         $cnt=$_SESSION['news_system']->get_count($time_usl) ;
         //if ($stats['']) { $stats[0]=$stats[0]+$stats[''] ; unset($stats['']) ; }
         //if (sizeof($stats)) foreach($stats as $status=>$cnt) {$menu_set[]=array("name" => $list_status_news[$status].'('.$cnt.')','cmd' =>'show_list_newss','status'=>$status) ; }
         //$menu_set[]=array("name" => 'Все ('.array_sum($stats).')',"cmd" =>'show_list_newss') ;
         $menu_set[]=array("name" => 'Новости ('.$cnt.')',"cmd" =>'show_list_news') ;
       }
       // выводим меню
       $menu_item=$_SESSION[$this->top_menu_name]->show($menu_set) ;  // показываем меню
       return($menu_item) ; // результат будет сохранен в экземпляре стараницы
 }

 function get_theme_usl($theme_id)
 { $usl='parent='.$theme_id ;
   return($usl) ;
 }

 function show_list_news()
 { $_usl[]=$this->get_time_usl($this->start_time,$this->end_time) ;
   //if ($_GET['pkey']>0) $_usl[]=$this->get_theme_usl($_GET['pkey']) ;
   $usl=implode(' and ',$_usl) ;
   _CLSS(9)->show_list_items($this->tkey,$usl,array('default_order'=>'c_data desc','buttons'=>array('save','delete','copy'))) ;
 }

 function new_news()
 {  $list_themes=execSQL('select * from '.$_SESSION['TM_news'].' where clss=1 and parent=1') ;
    ?><div class="black bold">Укажите название новости и заполните содержание.</div><br><?
      if (sizeof($list_themes))
         { ?>Тема новости: <select name=news_theme><option value=0></option><?
             foreach($list_themes as $rec) {?><option value=<?echo $rec['pkey']?>><?echo $rec['obj_name']?></option><?}
           ?></select> <?
         }
      ?>
      Наименование: <input  type="text" class=text size=100 name="news_name" value="<?echo $_POST['news_name']?>"><br><br><?
      if (isset($_SESSION['descr_clss'][9]['details']['field']['annot']))
      { ?><h2>Аннотация</h2><?
          /*?><textarea name="news_annot" cols="200" rows="3"></textarea><?*/
          show_window_ckeditor_v3(array(),'',100,'news_annot') ;


      }
      ?><h2>Содержание</h2><?
      show_window_ckeditor_v3(array(),'',400,'news_value') ;

    ?><br><button cmd=create_news>Создать новость</button><?
 }

 function news_themes()
 { global $news_system ;
   $rec['_reffer']=$news_system->root_reffer ;
   $res=_CLSS(1)->show_list_items($this->tkey,'parent=1',array('default_order'=>'indx','cur_obj_rec'=>$rec)) ;
   if (!$res['all']) echo  '<br>'._CLSS(1)->get_create_button_html($news_system->root_reffer) ;
 }

 function create_news()
 { $news_name=$_POST['news_name'] ;
   $news_parent=($_POST['news_theme'])? $_POST['news_theme']:1;
   if (isset($_SESSION['descr_clss'][9]['details']['field']['annot']))
   { $news_annot=$_POST['news_annot'] ;
     $news_value=$_POST['news_value'] ;
   }
   else
   { $news_value=$_POST['news_value'] ;
     $news_annot='' ;
   }
   $obj_info=_CLSS(9)->obj_create(array('parent'=>$news_parent,'obj_name'=>$news_name,'annot'=>$news_annot,'value'=>$news_value),array('tkey'=>$this->tkey)) ;
   if ($obj_info['pkey']) $this->start_time=0 ;

   //header("Location: "._PATH_TO_ADMIN."/fra_viewer.php?tkey=".$this->tkey."&pkey=".$obj_info['pkey']."&menu_id=item_2");
   ?><script type="text/javascript">document.location="<?echo _PATH_TO_ADMIN."/fra_viewer.php?tkey=".$this->tkey."&pkey=".$obj_info['pkey']."&menu_id=prop_value"?>"</script><?
 }

 function help()
 { $cont=file_get_contents(_DIR_TO_MODULES.'/news/help/index.html') ;
   echo $cont ;
 }

}

?>