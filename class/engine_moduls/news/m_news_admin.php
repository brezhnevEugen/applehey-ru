<?php
$__functions['init'][]		='_news_admin_vars' ;
$__functions['install'][]	='_news_install_modul' ;
$__functions['boot_admin'][]='_news_admin_boot' ;


function _news_admin_vars() //
{
    _DOT($_SESSION['TM_news'])->rules[]=array('to_clss'=>1,'name_as'=>'Тема') ;  // в таблицу товаров в корень добавлять только разделы
	//Новость
	$_SESSION['descr_clss'][9]['name']='Новость' ;
	$_SESSION['descr_clss'][9]['parent']=0 ;
	$_SESSION['descr_clss'][9]['fields']=array('value'=>'mediumtext','annot'=>'text','top'=>'int(11)','url_name'=>'varchar(255)','_enabled'=>'int(1)') ;
    $_SESSION['descr_clss'][9]['fields']['parent']=array('type'=>'indx_select','array'=>'ARR_news_themes') ;
    $_SESSION['descr_clss'][9]['fields']['c_data']=array('type'=>'timedata','datepicker'=>1,'data_format'=>'d.m.y') ;
    $_SESSION['descr_clss'][9]['fields']['source']=array('type'=>'varchar(255)') ;
    $_SESSION['descr_clss'][9]['fields']['source_url']=array('type'=>'varchar(255)') ;
    $_SESSION['descr_clss'][9]['parent_to']=array(3,5) ;

    $_SESSION['descr_clss'][9]['icons']=_PATH_TO_BASED_CLSS_IMG.'/7.png' ;

	// новость списоком
	//$_SESSION['descr_clss'][9]['list']['field']['pkey']				='Код' ;
	$_SESSION['descr_clss'][9]['list']['field']['enabled']			='Сост.' ;
	$_SESSION['descr_clss'][9]['list']['field']['parent']			=array('title'=>'Тема.','td_class'=>'left','edit_element'=>'indx_select') ;
	$_SESSION['descr_clss'][9]['list']['field']['top']				='ТОР' ;
	$_SESSION['descr_clss'][9]['list']['field']['c_data']			=array('title'=>'Дата','edit_element'=>'data_input','data_format'=>'d.m.y','datepicker'=>1) ; // 'd.m.y G:i','d.m.y','d.m.y G:i'
	$_SESSION['descr_clss'][9]['list']['field']['obj_name']			=array('title'=>'Наименование','class'=>'small','td_class'=>'left') ;
	$_SESSION['descr_clss'][9]['list']['field']['annot']			=array('title'=>'Аннотация','class'=>'big','td_class'=>'justify') ;
	$_SESSION['descr_clss'][9]['list']['field']['source']			=array('title'=>'Источник','td_class'=>'justify') ;
	$_SESSION['descr_clss'][9]['list']['field']['source_url']	    =array('title'=>'URL источника','class'=>'big','td_class'=>'justify') ;
	//$_SESSION['descr_clss'][9]['list']['field']['mess']				=array('title'=>'Содержание','class'=>'big','td_class'=>'justify') ;

	//$_SESSION['descr_clss'][9]['details']['field']['pkey']			='Код' ;
	$_SESSION['descr_clss'][9]['details']['field']['enabled']		='Сост.';
    $_SESSION['descr_clss'][9]['details']['field']['parent']	    =array('title'=>'Тема.','td_class'=>'left') ;
	$_SESSION['descr_clss'][9]['details']['field']['top']			='ТОР' ;
	$_SESSION['descr_clss'][9]['details']['field']['c_data']		=array('title'=>'Дата','edit_element'=>'data_input','data_format'=>'d.m.y') ;
	$_SESSION['descr_clss'][9]['details']['field']['obj_name']		=array('title'=>'Наименование','class'=>'small','td_class'=>'left') ;
	$_SESSION['descr_clss'][9]['details']['field']['annot']			=array('title'=>'Аннотация','td_class'=>'justify','use_HTML_editor'=>1) ;
	$_SESSION['descr_clss'][9]['details']['field']['value']			=array('title'=>'Содержание','class'=>'big','td_class'=>'justify','use_HTML_editor'=>1) ;

	//----------------------------------------------------------------------------------------------------------------------
	// описание пунктов меню админа сайта
	//-----------------------------------------------------------------------------------------------------------------------

	$_SESSION['menu_admin_site']['Модули']['news']=array('name'=>'Новости','href'=>'editor_news.php','icon'=>'menu/news.jpg') ;

}

function _news_admin_boot($options)
{   $options['no_upload_rec']=1 ;
	create_system_modul_obj('news',$options) ;
}


function _news_install_modul($DOT_root)
{   echo '<h2>Инсталируем модуль <strong>Лента новостей</strong></h2>' ;

    $file_items=array() ;
    // editor_news.php
    $file_items['editor_news.php']['include'][]='_DIR_TO_MODULES."/news/m_news_frames.php"' ;
    $file_items['editor_news.php']['options']['system']='"news_system"' ;
    $file_items['editor_news.php']['options']['title']='"Каталог новостей"' ;
    $file_items['editor_news_tree.php']['include'][]='_DIR_TO_MODULES."/news/m_news_frames.php"' ;
    $file_items['editor_news_tree.php']['options']['system']='"news_system"' ;
    $file_items['editor_news_viewer.php']['include'][]='_DIR_TO_MODULES."/news/m_news_frames.php"' ;
    $file_items['editor_news_viewer.php']['options']['system']='"news_system"' ;

    create_menu_item($file_items) ;

	$pattern=array() ;
	$pattern['table_title']				= 	'Новости' ;
	$pattern['use_clss']				=	'1,9' ;
	$pattern['def_recs'][]				=	array ('parent'=>0,	'clss'=>1,	'enabled'=>1,	'indx'=>0,	'obj_name'=>'Новости') ;
   	$pattern['table_name']				= 	$_SESSION['TM_news'] ;
	$pattern['table_clss']				= 	101 ;
	$pattern['table_parent']			= 	$DOT_root;
   	create_table_by_pattern($pattern) ;

    SETUP_get_img('/news/admin_img_menu/news.jpg',_PATH_TO_ADMIN.'/img/menu/news.jpg') ;

    create_dir('/news/') ;

    $file_items=array() ;
    $file_items['index.php']['options']['system']='"news_system"' ;
    $file_items['index.php']['options']['use_include_patch']='_DIR_TO_MODULES."/news/"' ;

    $file_items['.htaccess']['text'][]='RewriteEngine On' ;
    $file_items['.htaccess']['text'][]='RewriteRule ^(.*)/(.*).html$ index.php[L]' ;
    $file_items['.htaccess']['text'][]='RewriteRule ^(.*).html$ index.php [L]' ;
    $file_items['.htaccess']['text'][]='RewriteRule ^(.*)/$ index.php [L]' ;
    create_script_to_page($file_items,_DIR_TO_ROOT.'/news/','../ini/patch.php') ;

    $list_templates_file=analize_class_file_to_templates(_DIR_TO_MODULES.'/news/c_news.php') ;
    SETUP_get_templates('/news/',$list_templates_file) ;
}

include_once(_DIR_TO_ENGINE.'/class/clss_0.php') ;
class clss_9 extends clss_0
{
    // подготовка url для страницы объекта
    function prepare_url_obj($rec)
    {   $url_name=substr(safe_text_to_url($rec['obj_name'],$rec['tkey']),0,32).'_'.$rec['pkey'] ;
        return($url_name) ;
    }
}



?>