<?php
include_once(_DIR_TO_ENGINE."/admin/c_site.php") ;
include_once(_DIR_TO_ENGINE."/admin/i_clss_func.php") ;
include_once(_DIR_TO_ENGINE.'/admin/c_menu_tabs.php') ;

// редактор службы поддержки
class c_editor_chat extends c_editor_obj
{
  function body($options=array()) { $this->body_frame_2x(array('fra_tree'=>_PATH_TO_ADMIN.'/editor_chat_tree.php','title'=>$options['title'])) ; }
}

// дерево тикетов
class c_editor_chat_tree extends c_fra_tree
{ public $system_name='chat_system' ;
  function body($options=array())
 {?><body id=tree_objects><div id=tree_menu><?$this->top_menu() ;?></div><?
     $options=array() ;
     $options['usl_select']='clss=53' ;
     $options['on_click_script']='editor_chat_viewer.php' ;
     $options['on_click_item']=array(53=>'fra_viewer.php') ;

     $this->generate_time_tree('Чат',$this->tkey,$options);
  ?></body><?
 }

}

class c_editor_chat_viewer extends c_fra_based
{  public $system_name='chat_system' ;
   public $top_menu_name='top_menu_list_chat' ;
   public $cur_obj_form=0 ;

  function __construct()
  { parent::__construct() ;
    $this->cur_obj_form=0 ;
  }

 function check_GET_params()
 { global $chat_system ;
   if ($_GET['reffer'])
   {  list($id,$tkey)=explode('.',$_GET['reffer']) ;
      $tiket=$chat_system->get_tiket_info_by_id($id) ;
      //damp_array($tiket) ;
      header("HTTP/1.1 301 Moved Permanently");
      header("Location: ".$tiket['__moderator_url']);
      _exit() ;
   } else parent::check_GET_params() ;
 }

 function body(&$options=array()) {$this->body_frame($options) ; }

// заголовок фрейма
 function show_page_obj_header()
 { $title=($_GET['pkey']=='root')? 'Чат':'Чат: сообщения '.$this->get_time_title($this->start_time,$this->end_time) ;
   ?><p class='obj_header'><?echo $title?></p><?
 }
    
 // меню фрейма
 function show_page_obj_menu($obj_info)
    {  global $chat_system ;
      // создаем объект top_menu
       if (!is_object($_SESSION[$this->top_menu_name])) $_SESSION[$this->top_menu_name]=new c_top_menu() ;
       // формируем набор пунктом меню
       $menu_set=array() ;
       // пункты меню для корня дерева
       if ($_GET['pkey']=='root')
       { $menu_set[]=array("name" => 'Операторы чата',"cmd" =>'show_list_operators','status'=>0) ;
         $menu_set[]=array("name" => 'Настройки',"cmd" =>'chat_settings','status'=>1) ;
       }
       // или если выбран один из временных интервалов
       else
       { $time_usl=$this->get_time_usl($this->start_time,$this->end_time) ;
         $arr_status=$_SESSION['chat_system']->get_count_by_status($time_usl) ;
         if (sizeof($arr_status)) foreach($arr_status as $status=>$cnt) {$menu_set[]=array("name" => $_SESSION['list_chat_status'][$status].'('.$cnt.')','cmd' =>'show_list_items','status'=>$status) ; }
         $menu_set[]=array("name" => 'Все ('.array_sum($arr_status).')',"cmd" =>'show_list_items') ;
       }
       // выводим меню
       $this->cur_menu_item=$_SESSION[$this->top_menu_name]->show($menu_set) ;  // показываем меню
       return($this->cur_menu_item) ;
    }    

    function show_list_items()
    { $options=array() ; $usl='' ; $_usl=array() ;
      if ($this->start_time or $this->end_time) $_usl[]=$this->get_time_usl($this->start_time,$this->end_time) ;
      if (isset($this->cur_menu_item['status'])) $_usl[]='status='.$this->cur_menu_item['status'] ;
      if (sizeof($_usl)) $usl=implode(' and ',$_usl) ;
      $title='Чаты '.$this->get_time_title($this->start_time,$this->end_time) ;
      if (isset($this->cur_menu_item['status'])) $title.=' в статусе "'.$_SESSION['list_chat_status'][$this->cur_menu_item['status']].'"' ;
      if ($this->cur_menu_item['mode']=='last_items')
      { $options['count']=$this->cur_menu_item['count'] ;
        $options['order']='c_data desc' ;
        $title.=' - последние '.$this->cur_menu_item['count'] ;
      }
      $options['default_order']='c_data desc' ;
      $options['title']=$title ;
      $options['buttons']=array('save','delete') ;
      if (!$this->cur_menu_item['no_show_list'] and $usl) _CLSS(53)->show_list_items($this->tkey,$usl,$options) ;
    }


    function show_list_operators()
    { global $TM_list,$chat_system ;
      $list_rec=execSQL_van('select * from '.$TM_list.' where pkey='.$chat_system->list_operator_id) ;
      $res=_CLSS(55)->show_list_items($TM_list,'parent='.$chat_system->list_operator_id.' and (sys=0 or sys is null)',array('cur_obj_rec'=>$list_rec)) ;
      if (!$res['all']) {?><button cmd="get_panel_create_manager" script="mod/chat" class="v2">Добавить оператора чата</button><?}

    }

    function chat_settings()
    { return ;
      ?><input type="submit" class="v2" cmd="create_new_manager"  script="mod/chat" validate="form" value="Создать нового оператора"><?
    }

}