<?php
$__functions['init'][] 			='_chat_admin_vars' ;
$__functions['boot_admin'][] 	='_chat_admin_boot' ;
$__functions['alert'][]			='_chat_admin_alert' ;
$__functions['install'][]		='_chat_install_modul' ;

include_once('clss_53.php');

function _chat_admin_vars() //
{

    //-----------------------------------------------------------------------------------------------------------------------------
	// описание переменных модуля
	//-----------------------------------------------------------------------------------------------------------------------------

    $_SESSION['arr_tree_clss_names'][53]=array('Тикет: ','pkey',' : ','obj_name') ;

	//-----------------------------------------------------------------------------------------------------------------------------
	// описание классов модуля
	//-----------------------------------------------------------------------------------------------------------------------------

	//тикеты службы поддержки
    $_SESSION['descr_clss'][53]['name']='Ветка чата' ;
    $_SESSION['descr_clss'][53]['parent']=0 ;
    // поля - автор тикета
    $_SESSION['descr_clss'][53]['fields']['obj_name']='varchar(32)' ;
    $_SESSION['descr_clss'][53]['fields']['account_reffer']='any_object' ;
    $_SESSION['descr_clss'][53]['fields']['autor_email']='varchar(100)' ;
    $_SESSION['descr_clss'][53]['fields']['autor_name']='varchar(100)' ;
    $_SESSION['descr_clss'][53]['fields']['autor_phone']='varchar(32)' ;
    $_SESSION['descr_clss'][53]['fields']['autor_info']='serialize' ;
    $_SESSION['descr_clss'][53]['fields']['XMPP_login']='varchar(100)' ;
    $_SESSION['descr_clss'][53]['fields']['XMPP_pass']='varchar(32)' ;
    $_SESSION['descr_clss'][53]['fields']['man_id']=array('type'=>'indx_select','array'=>'IL_chat_manager') ;   // оператор, ведущий данный тикет

    $_SESSION['descr_clss'][53]['fields']['uid']='varchar(32)' ;

    // дополнительные поля тикета
    $_SESSION['descr_clss'][53]['fields']['status']=array('type'=>'indx_select','array'=>'list_chat_status','read_only'=>1) ; // статус тикета

    $_SESSION['descr_clss'][53]['menu'][]=array("name" => "Чат",	                "cmd" => 'show_tiket') ;
    $_SESSION['descr_clss'][53]['menu'][]=array("name" => "История чата",			"cmd" => 'show_tiket_history') ;
    //$_SESSION['descr_clss'][53]['menu'][]=array("name" => "Ссылки",			        "cmd" => 'show_tiket_links') ;

    $_SESSION['descr_clss'][53]['list']['field']['c_data']					=array('title'=>'Дата','type'=>'time','nowrap'=>1) ;
    $_SESSION['descr_clss'][53]['list']['field']['status']					=array('title'=>'Статус') ;
    $_SESSION['descr_clss'][53]['list']['field']['man_id']					='Оператор' ;
    $_SESSION['descr_clss'][53]['list']['field'][]				            =array('title'=>'Сообщение','td_class'=>'left','use_func'=>'cmd_show_clss_53_tiket_mess') ;
    $_SESSION['descr_clss'][53]['list']['field'][]				            =array('title'=>'','td_class'=>'left','use_func'=>'cmd_show_clss_53_tiket_go') ;
    $_SESSION['descr_clss'][53]['list']['field']['autor_info']			    =array('title'=>'Информация по автору','td_class'=>'left') ;

	//$_SESSION['descr_clss'][53]['list']['field']['clss_53_goto']			=array('title'=>'Переход к тикету') ;
    $_SESSION['descr_clss'][53]['list']['options']=array('no_icon_edit'=>1) ;


	//сообщения тикетов службы поддержки, форума, заказов и т.п.
	$_SESSION['descr_clss'][54]['name']='Cообщение чата' ;
	$_SESSION['descr_clss'][54]['parent']=0 ;
	$_SESSION['descr_clss'][54]['fields']['man_id']				    =array('type'=>'int(11)') ; // man_id будет только у сообщение операторов

	$_SESSION['descr_clss'][54]['parent_to']						=array() ;
	$_SESSION['descr_clss'][54]['no_show_in_tree']					=1 ;
    $_SESSION['descr_clss'][54]['no_create_by_list']				=1 ;

	$_SESSION['descr_clss'][54]['list']['field']['c_data']			=array('title'=>'Дата','type'=>'time') ;
	$_SESSION['descr_clss'][54]['list']['field']['autor_name']	    =array('title'=>'Автор') ;
	$_SESSION['descr_clss'][54]['list']['field']['account_reffer']	=array('title'=>'Автор') ;
	$_SESSION['descr_clss'][54]['list']['field']['obj_name']		=array('title'=>'Текст','td_class'=>'left') ;


    $_SESSION['descr_clss'][54]['details']							=$_SESSION['descr_clss'][54]['list'] ;

    $_SESSION['descr_clss'][55]['name']='Оператор чата' ;
	$_SESSION['descr_clss'][55]['parent']=20 ;
	$_SESSION['descr_clss'][55]['fields']['function']='varchar(50)' ;
	$_SESSION['descr_clss'][55]['fields']['login']='varchar(32)' ;
	$_SESSION['descr_clss'][55]['fields']['pass']='varchar(32)' ;
	$_SESSION['descr_clss'][55]['fields']['sys']='int(1)' ;
    $_SESSION['descr_clss'][55]['parent_to']=array(3) ;
    $_SESSION['descr_clss'][55]['child_to']=array(21) ;
    //$_SESSION['descr_clss'][55]['icons']=_PATH_TO_BASED_CLSS_IMG.'/55.png' ;

    $_SESSION['descr_clss'][55]['menu'][]=array("name" => "Свойства",	        "type" => 'structure') ;
    $_SESSION['descr_clss'][55]['menu'][]=array("name" => "Параметры",			"cmd" => 'panel_operator_params') ;


    $_SESSION['descr_clss'][55]['list']=array() ;
	$_SESSION['descr_clss'][55]['list']['field']['id']			    ='Код' ;
	$_SESSION['descr_clss'][55]['list']['field']['enabled']			='Состояние' ;
	$_SESSION['descr_clss'][55]['list']['field']['obj_name']		=array('title'=>'Имя','td_class'=>'left') ;
    $_SESSION['descr_clss'][55]['list']['field']['function']	    ='Должность'  ;
	$_SESSION['descr_clss'][55]['list']['field']['login']		    =array('title'=>'Логин','td_class'=>'left','read_only'=>1) ;
	$_SESSION['descr_clss'][55]['list']['field']['pass']		    =array('title'=>'Пароль','td_class'=>'left') ;
	$_SESSION['descr_clss'][55]['list']['field'][]		            =array('title'=>'Состояние') ;
    //$_SESSION['descr_clss'][55]['list']['options']					=array('no_icon_edit'=>1);
    $_SESSION['descr_clss'][55]['list']['options']['buttons']=array(array('title'=>'Добавить оператора чата','cmd'=>'get_panel_create_manager','script'=>'mod/chat','class'=>'v2'),'save','delete') ; // добавлена кнопка с расширенным описанием

    $_SESSION['descr_clss'][55]['details']=$_SESSION['descr_clss'][55]['list'] ;
    
	//----------------------------------------------------------------------------------------------------------------------
	// описание пунктов меню админа сайта
	//-----------------------------------------------------------------------------------------------------------------------
	$_SESSION['menu_admin_site']['Модули']['chat']=array('name'=>'Чат','href'=>'editor_chat.php','icon'=>'menu/broadcast_help.jpg') ;

	//----------------------------------------------------------------------------------------------------------------------
	// описание меню фрейма объекта
	//-----------------------------------------------------------------------------------------------------------------------

    //$_SESSION['menu_set']['any'][53][]=array("name" => "Просмотр",				"cmd" => 'view','def'=>1) ; // заказ
    //$_SESSION['menu_set']['any'][53][]=array("name" => "История тикета",		"cmd" => 'history') ;
    //$_SESSION['menu_set']['any'][53][]=array("name" => "Ссылки",		        "cmd" => 'links') ;

    // встраиваем служба поддержки в заказы
    //$_SESSION['menu_set']['any'][82][]=array("name" => "Открыть тикет",			"action" => 'open_tiket') ;
    //$_SESSION['menu_set']['any'][82][]=array("name" => "Тикеты клиента",		"action" => 'show_tikets') ;

    // встраиваем служба поддержки в партнерку
    // кстати, эти записи по идее должны быть в модуле "партнерка"
    //$_SESSION['menu_set']['any'][91][]=array("name" => "Открыть тикет",			"action" => 'open_tiket') ;
    //$_SESSION['menu_set']['any'][91][]=array("name" => "Тикеты клиента",		"action" => 'show_tikets') ;

}

function _chat_admin_boot($options)
{
	create_system_modul_obj('chat',$options) ; // создание объекта chat_system - его функии используются для доступа к тикетам
}

function _chat_install_modul($DOT_root)
{   echo '<h2>Инсталируем модуль <strong>Служба поддержки</strong></h2>' ;

    $file_items=array() ;
    // editor_chat.php
    $file_items['editor_chat.php']['include'][]					='_DIR_TO_MODULES."/chat/m_chat_frames.php"' ;
    $file_items['editor_chat.php']['options']['use_table_code']	='"TM_chat"' ;
    $file_items['editor_chat.php']['options']['title']			='"Чат"' ;
    $file_items['editor_chat_tree.php']['include'][]			='_DIR_TO_MODULES."/chat/m_chat_frames.php"' ;
    $file_items['editor_chat_viewer.php']['include'][]			='_DIR_TO_MODULES."/chat/m_chat_frames.php"' ;
    $file_items['editor_chat_ajax.php']['include'][]		    ='_DIR_TO_MODULES."/chat/m_chat_ajax.php"' ;
    create_menu_item($file_items) ;

	// таблица чата с темами
    $pattern=array() ;
	$pattern['table_title']	            = 	'Чат' ;
	$pattern['use_clss']	            =	'1,53' ;
	$pattern['def_recs'][]	            =	array ('parent'=>0,	'clss'=>1,	'indx'=>0,	'obj_name'=>'Чат') ;
   	$pattern['table_name']				= 	$_SESSION['TM_chat'] ;
	$pattern['table_clss']				= 	101 ;
	$pattern['table_parent']			= 	$DOT_root;
   	$table_id=create_table_by_pattern($pattern) ;

	// таблица чата с сообщениями
    $pattern=array() ;
	$pattern['table_title']	            = 	'Сообщения чата' ;
	$pattern['use_clss']	            =	'54' ;
   	$pattern['table_name']				= 	$_SESSION['TM_chat_mess'] ;
	$pattern['table_clss']				= 	101 ;
	$pattern['table_parent']			= 	$table_id;
   	create_table_by_pattern($pattern) ;

    SETUP_get_img('/chat/admin_img_menu/broadcast_help.jpg',_PATH_TO_ADMIN.'/img/menu/broadcast_help.jpg') ;

    //create_dir('/chat/') ;
    //SETUP_get_file('/chat/system_dir/index.php','/chat/index.php');
    //SETUP_get_file('/chat/system_dir/create.php','/chat/create.php');
    //SETUP_get_file('/chat/system_dir/tiket.php','/chat/tiket.php');
    //SETUP_get_file('/chat/class/c_chat.php','/class/c_chat.php');
    //SETUP_get_file('/chat/class/c_chat_create.php','/class/c_chat_create.php');
    //SETUP_get_file('/chat/class/c_chat_tiket.php','/class/c_chat_tiket.php');
    //SETUP_get_file('/chat/templates/form_chat.php','/class/templates/form_chat.php');

    //import_setting_from_xml(_PATH_TO_SERVER_SETUP.'?m=chat&s=setting',array('parent_tkey'=>$_SESSION['pkey_by_table'][TM_SETTING],'parent_pkey'=>1,'check_unique_name'=>1)) ;
    //import_setting_from_xml(_PATH_TO_SERVER_SETUP.'?m=chat&s=mails',array('parent_tkey'=>$_SESSION['pkey_by_table'][TM_MAILS],'parent_pkey'=>1,'check_unique_name'=>1)) ;

    //$id_sect=SETUP_add_section_to_site_setting('Настройка почтовых уведомлений') ;
    //SETUP_add_rec_to_site_setting($id_sect,'Адрес, на который отправлять уведомление о совершении заказа','debug@12-24.ru','LS_orders_system_email_alert') ;

    SETUP_add_class_to_table(TM_LIST,55) ; // добавляем поддержку "Операторы чата" в списки сайта

    $id_sect=SETUP_add_section_to_site_list('Чат',array('hidden'=>1)) ;
    $id_list=SETUP_add_list_to_site_list($id_sect,'Операторы чата','chat_manager',array('hidden'=>1)) ;
    //SETUP_add_rec_to_site_list($id_list,84,array('id'=>1,'obj_name'=>'Курьер (только по Москве)','admin'=>'Курьер','price'=>300))  ;
    global $TM_list ;
    ?><h2>Создаем оператора "Сайт"</h2><?
    $result=_CLSS(55)->obj_create(array('parent'=>$id_list,'login'=>_BASE_DOMAIN,'sys'=>1),array('parent_reffer'=>$id_list.'.'._DOT($TM_list)->pkey)) ;
    damp_array($result,1,-1) ;


}

function _chat_admin_alert()
{ //$res=execSQL_van('select count(pkey) as cnt from '.$_SESSION['TM_chat'].' where clss=53 and status=0') ;
  //if ($res['cnt'])
  //{ echo "<div>У Вас <span class='red bold'>".$res['cnt']."</span> неотвеченных сообщений в <a href='"._PATH_TO_ADMIN."/editor_chat.php'>службе поддержки</a></div><br>";
  //}
}



function cmd_show_clss_53_tiket_mess(&$rec)
 { $list=execSQL_van('select * from '.$_SESSION['TM_chat_mess'].' where clss=54 and parent='.$rec['pkey'].' order by c_data desc limit 1') ;
   return '<i>'.$list['obj_name'].'</i>' ;
 }
function cmd_show_clss_53_tiket_go(&$rec)
 { $cnt=execSQL_value('select count(pkey) as cnt from '.$_SESSION['TM_chat_mess'].' where clss=54 and parent='.$rec['pkey']) ;
   return '<a href="/admin/fra_viewer.php?&tkey='.$rec['tkey'].'&clss='.$rec['clss'].'&reffer='.$rec['_reffer'].'&class=selected&obj_reffer='.$rec['_reffer'].'" class=nowrap><strong>'.$cnt.'</strong> сообщений</a>' ;
 }



?>
