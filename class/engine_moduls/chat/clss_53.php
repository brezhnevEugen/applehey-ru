<?
include_once(_DIR_TO_ENGINE.'/class/clss_0.php') ;
include_once(_DIR_TO_ENGINE.'/class/clss_20.php') ;
include_once('i_chat_panels.php') ;

class clss_55 extends clss_20
{
  // создаем объект нужного класса
  function obj_create($finfo,$options=array())
  { global $chat_system ; //damp_array($finfo) ;
    // сначала пробуем создать пользователя на XMPP сервере
    if (!$finfo['pass']) $finfo['pass']=rand(1,1000000) ;
    if (!$finfo['obj_name']) $finfo['obj_name']=$finfo['login'] ;
    $finfo['login']=str_replace(array('.'),'_',$finfo['login']) ;
    $result=$chat_system->XMPP_action_user('add',$finfo) ; //damp_array($result) ;
    if ($result['type']=='success')
    {  // добавляем нового пользователя в список контактов остальных менеджеров
       if (sizeof($_SESSION['IL_chat_manager'])) foreach($_SESSION['IL_chat_manager'] as $rec) if ($rec['login']!=$finfo['login']) $chat_system->XMPP_add_to_list_contact($rec['login'],$finfo['login']) ;

       $obj_info=parent::obj_create($finfo,$options) ;
    }
    $obj_info['result']=$result ;
    return($obj_info) ;
  }

  // событие удаления объекта
  function on_delete_event($obj_info,$options=array())
  { global $chat_system ;
    parent::on_delete_event($obj_info,$options) ;
    if ($obj_info['login']) $chat_system->XMPP_action_user('delete',array('login'=>$obj_info['login'])) ;
  }

  function on_change_event_enabled($tkey,$pkey,&$fvalue)
  { global $chat_system,$TM_list ;
    $rec=execSQL_van('select * from '.$TM_list.' where pkey='.$pkey) ;
    if ($fvalue) $chat_system->XMPP_action_user('enable',array('login'=>$rec['login'])) ;
    else         $chat_system->XMPP_action_user('disable',array('login'=>$rec['login'])) ;
    return(1) ;
  }

  function on_change_event_obj_name($tkey,$pkey,&$fvalue)
  { global $chat_system,$TM_list ;
    $rec=execSQL_van('select * from '.$TM_list.' where pkey='.$pkey) ;
    if ($fvalue) { $chat_system->XMPP_action_user('update',array('obj_name'=>$fvalue,'login'=>$rec['login'],'pass'=>$rec['pass'])) ;
                   return(1) ;
                 }
    else return(0) ;
  }

  // запрет изменения логина через админку
  function on_change_event_login($tkey,$pkey,&$fvalue)
  { return(0) ;
  }

  function on_change_event_pass($tkey,$pkey,&$fvalue)
  { global $chat_system,$TM_list ;
    $rec=execSQL_van('select * from '.$TM_list.' where pkey='.$pkey) ;
    if ($fvalue) { $chat_system->XMPP_action_user('update',array('obj_name'=>$rec['obj_name'],'login'=>$rec['login'],'pass'=>$fvalue)) ;
                   return(1) ;
                 }
    else return(0) ;
  }

  function panel_operator_params($options=array())
  { global $TM_list ;
    $rec=execSQL_van('select * from '.$TM_list.' where pkey='.$options['pkey']) ;
    ?><strong>Реквизиты для подключения оператора к чату:</strong>
     <table class="debug left">
         <tr><td>Сервер:</td><td><?echo _XMPP_SERVER_HOST?></td></tr>
         <tr><td>Порт:</td><td><?echo _XMPP_CLIENT_TO_SERVER_PORT?></td></tr>
         <tr><td>Jabber ID:</td><td><?echo $rec['login']._XMPP_LOGIN_DOMAIN?></td></tr>
         <tr><td>Пароль:</td><td><?echo $rec['pass']?></td></tr>
     </table><?
  }

}

// ветка чата
class clss_53 extends clss_0
{
  // блокируем создания обектов - только через сайт в чате
  function obj_create($finfo,$options=array())
  { return(array()) ;
  }

  function on_delete_event($obj_info,$options=array())
  { global $chat_system ;
    parent::on_delete_event($obj_info,$options) ;
    if ($obj_info['XMPP_login']) $chat_system->XMPP_action_user('delete',array('login'=>$obj_info['XMPP_login'])) ;
  }

    function show_props($obj_rec,$options=array())
    {  _CUR_PAGE()->obj_info=$obj_rec ;
       $this->show_order_info() ;
    }

 // показ тикета
 function show_tiket()
 {  global $chat_system,$obj_info ;
    $chat_system->prepare_public_info($obj_info) ;
    $list_messages=$chat_system->get_list_mess($obj_info['pkey'],array('back_order'=>1)) ; // список сообщений чата

    ?><h1>Тикет <?echo $obj_info['pkey']?> от <?echo date("d.m.y G:i",$obj_info['c_data'])?></h1>
      <div class=tiket_body>
      <input name="_check[<?echo $chat_system->tkey?>][53][<?echo $obj_info['pkey']?>]" value="1" type="hidden">
    <?

    // информация по чату
    $this->show_tiket_info($obj_info,sizeof($list_messages)) ;

    // сообщения чата
    $this->panel_list_messages($list_messages) ;


    ?><style type="text/css">
         div.tiket_body{margin-left:20px;}
         textarea.mess{width:600px;height:100px;margin-left:20px;}
         div.buttons{width:600px;margin:15px 0 15px 20px;}
         div#panel_list_messages{}
         div#panel_list_messages div.item{}
         div#panel_list_messages div.item div.title{font-family:Tahoma,Arial,sans-serif;font-size:11px;}
         div#panel_list_messages div.item div.mess{width:580px;border:1px solid #CECECE;margin:5px 25px;padding:10px;}
         div#panel_list_messages div.item.otvet div.mess{background:#FFFACD;}
      </style>
    <?
 }

  function panel_list_messages($recs)
   { ?><br><br><div id="panel_list_messages"><?
     if (sizeof($recs)) foreach($recs as $message)
      { $autor=($message['man_id'])? $_SESSION['IL_chat_manager'][$message['man_id']]['obj_name']:'' ;
        //$mess_autor=($message['status'])?  $_SERVER['SERVER_NAME'].' - '.generate_obj_text($message['account_reffer'],array('read_only'=>1)) ;
        ?><div class="item <?if ($message['man_id']) echo 'otvet'?>">
              <div class=title><? echo date("d.m.y G:i",$message['c_data']).' <strong>'.$autor.'</strong>' ; ?></div>
              <div class=mess><?echo nl2br($message['obj_name'])?></div>
          </div>
        <?
      }
     ?></div><?
   }


 function show_tiket_links()
 { global $obj_info,$chat_system ;
   $chat_system->prepare_public_info($obj_info) ;
   ?><h2>Прямые ссылки на тикет:</h2>
     <p>Ссылка для <span class="green bold">АВТОРА</span> тикета, для добавления нового <span class="green bold">ВОПРОСА</span>: <a href="<?echo $obj_info['__autor_url']?>" target=_blank><?echo $obj_info['__autor_url']?></a></p>
     <br>
     <br>
     <p>Ссылка для <span class="green bold">ОТВЕТА</span> на тикет через сайт: <a href="<?echo $obj_info['__moderator_url']?>" target=_blank><?echo $obj_info['__moderator_url']?></a></p>


  <?
 }

 function show_tiket_info($rec,$count)
 {  global $list_chat_status,$chat_system ;
    $tiket_autor=($rec['account_reffer'])? generate_obj_text($rec['account_reffer'],array('read_only'=>1)):'<span class="bold green">'.$rec['autor_name'].'</span> ('.$rec['autor_phone'].' '.$rec['autor_email'].')' ;
    $all_count=$chat_system->get_count('and autor_email="'.$rec['autor_email'].'"') ;
    if (!$rec['status']) $rec['status']=0 ;
    echo 'Состояние: <span class="red bold">'.(($rec['enabled'])? $list_chat_status[$rec['status']]:'Только чтение').'</span>' ;
    if ($rec['public']) echo ', <span class="green bold">Опубликован</span>' ;
    echo '&nbsp;&nbsp;&nbsp;' ;
    echo 'Клиент: '.$tiket_autor.'&nbsp;&nbsp;&nbsp;' ;
    echo 'Всего сообщений в тикете: <span class="red bold size18">'.$count.'</span>&nbsp;&nbsp;&nbsp;' ;
    echo get_serial_data($rec['autor_info']) ;
 }


 function tiket_add_message()
 { global $chat_system,$obj_info,$member ;
   //damp_array($_POST) ;
   if ($_POST['mess'])
   {   echo 'Добавляем новое сообщение' ;
       $obj_info['open_mode']=1 ; // почеаем, что это ответ
       $info=array() ;
       $info['value']=$_POST['mess'] ;
       $info['name']=$member->name ;
       $info['email']=$member->info['email'] ; if (!$info['email']) $info['email']=$_SESSION['LS_chat_system_email_alert'] ;
       $info['phone']=$member->info['phone'] ;
       $info['debug_mail']=1 ;
       ob_start() ;
       $res=$chat_system->add_message($obj_info['edit_code'],$info) ;// damp_array($res) ;
       $text=ob_get_clean() ;
       echo ($res['type']=='success')? '<span class="green bold"> - OK</span><br>':'<span class="red bold"> - ERROR ('.$res['code'].')</span><br>' ;
       echo $text ;
   }

   return('ok') ; // будет показан начальный экран
 }

 function tiket_delete()
 {
   echo 'Удаляем тикет' ;
 }

 function tiket_close()
 { global $chat_system,$obj_info ;
   $chat_system->close_tiket($obj_info) ;
   return('ok') ; // // будет показан начальный экран
 }

 function show_tiket_history()
   { global $obj_info ;
     show_history('links like "%'.$obj_info['_reffer'].'%" or reffer="'.$obj_info['_reffer'].'"') ;
   }

 function show_other_tiket_autor()
 { global $obj_info,$chat_system ;
   _CLSS(53)->show_list_items($chat_system->tkey,'autor_email="'.$obj_info['autor_email'].'" and pkey!='.$obj_info['pkey'],array('order'=>'c_data desc')) ;

 }
}


?>