<?php
$__functions['init'][]='_chat_site_vars' ;
$__functions['boot_site'][]='_chat_site_boot' ;

require_once(_DIR_EXT.'/chat/XMPPHP/XMPP.php');

function _chat_site_vars()
{
	$_SESSION['TM_chat']= 'obj_'.SITE_CODE.'_chat' ;
    $_SESSION['TM_chat_mess']='obj_'.SITE_CODE.'_chat_mess' ;
    $_SESSION['list_chat_status']=array('0'=>'Новый','1'=>'Отвечен','2'=>'Не отвечен','3'=>'Закрыт') ;

    // описание системы службы поддержки  -------------------------------------------------------------------------------------------------------------------
    $_SESSION['init_options']['chat']=array() ;
    $_SESSION['init_options']['chat']['debug']=0 ;
    $_SESSION['init_options']['chat']['usl_show_items']='clss in (53,54)' ;
    $_SESSION['init_options']['chat']['system_title']='Чат' ;
    $_SESSION['init_options']['chat']['system_href']='/chat/' ;
    $_SESSION['init_options']['chat']['moderator_name']='Администрация сайта' ;
    $_SESSION['init_options']['chat']['moderator_email']=$_SESSION['LS_chat_system_email_alert'] ;

}


function _chat_site_boot($options)
{
	create_system_modul_obj('chat',$options) ; // создание объекта chat_system
}


class c_chat_system extends c_system
{  public $XMPP_server_host ;
   public $XMPP_server_resource ;
   public $XMPP_client_to_server_port ;
   public $XMPP_admin_console_port ;
   public $XMPP_login_domain ;
   public $XMPP_secret_word ;
   public $cancel_message ;

   public $host ;
   public $list_operator_id ;

 function c_chat_system($create_options)
  {  $this->table_name=($create_options['table_name'])? $create_options['table_name']:$_SESSION['TM_chat'] ;
     $this->chats_parent=1 ;
     $this->table_mess=$_SESSION['TM_chat_mess'] ;
     $this->tkey_mess=_DOT($_SESSION['TM_chat_mess'])->pkey ;

     $this->XMPP_server_host=_XMPP_SERVER_HOST;
     $this->XMPP_server_resource='';
     $this->XMPP_client_to_server_port=_XMPP_CLIENT_TO_SERVER_PORT;
     $this->XMPP_admin_console_port=_XMPP_ADMIN_CONSOLE_PORT;
     $this->XMPP_login_domain=_XMPP_LOGIN_DOMAIN;
     $this->XMPP_secret_word=_XMPP_SECRET_WORD;
     $this->cancel_message=_BASE_DOMAIN.': Чат взят оператором *name*';

     set_time_point('Создание объекта <strong>chat_system</strong>') ;
     parent::c_system($create_options) ;
     unset($this->pages_info) ;

     global $TM_list ;
     $this->list_operator_id=execSQL_value('select pkey from '.$TM_list.' where arr_name="chat_manager"') ;
     $this->list_operator_reffer=$this->list_operator_id.'.'._DOT($TM_list)->pkey ;

     // запоминаем данные системного аккаунта
     $this->system_account=execSQL_van('select login,pass from '.$TM_list.' where parent="'.$this->list_operator_id.'" and clss=55 and sys=1') ;

     // загружаем список активных операторов
     // надо перенести на момент открытия окна чата и делать через AJAX, чтобы не подвисал сайт
     $this->active_operators=$this->get_operators_available() ;
     //$this->check_new_func_get_operators_available() ;
     //damp_array($this) ;
  }

 //-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 //
 // функции для работы с XMPP
 //
 //-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 // возвращает информацию о устройствах, на которых открыт аккаунт login
 function getPresence_info($login,$pass)
 {  if (!$login or !$pass) return(array()) ;
	// Load configuration
	//$conn = new XMPPHP_XMPP($this->XMPP_server_host,$this->XMPP_client_to_server_port,$login.$this->XMPP_login_domain,$pass,$this->XMPP_server_resource);
	$conn = new XMPPHP_XMPP($this->XMPP_server_host,$this->XMPP_client_to_server_port,$login.$this->XMPP_login_domain,$pass,'cur_connect');
	try
    {   $conn->connect();
	    $conn->processUntil('session_start');
        // Enter the chatroom
        //$conn->presence('online', "available");     // away,
        //$conn->getRoster();
        //$conn->presence('I m presence'.time());
        $conn->presence();
        $conn->disconnect();
	}
    catch(XMPPHP_Exception $e) { error_log($e->getMessage());}
    //$arr=$conn->roster->getPresence($login.$this->XMPP_login_domain)  ;
    $arr=$conn->roster->getRoster()  ;
    $res=$arr[$login.$this->XMPP_login_domain]['presence'] ;
    unset($res['cur_connect']) ;
    //damp_array($res) ;
    return($res) ;
 }

 // возвращает информацию о собеседниках списка контактов системного аккаунта
 function get_operator_status($login)
 {	$result=(in_array($login,$this->active_operators))? 1:0;
    return($result) ;
 }

 // возвращает информацию о собеседниках списка контактов системного аккаунта
 function get_operators_available()
 {	// Load configuration
    $available=array() ;
    if (sizeof($_SESSION['IL_chat_manager'])) foreach($_SESSION['IL_chat_manager'] as $id=>$rec_operator) if (!$rec_operator['sys'])
    { $url='http://'.$this->XMPP_server_host.':9090/plugins/presence/status?jid='.$rec_operator['login'].$this->XMPP_login_domain.'&type=xml' ;
      $res_xml=@file_get_contents($url) ;
      $dom = new DomDocument(); // читаем XML
      if ($res_xml and $dom->loadXML($res_xml))
      { $presence=$this->get_attr_tag_by_name($dom,'presence') ; // <presence type="unavailable" from="nikola@clss.ru">
        $show=$this->get_value_tag_by_name($dom,'show') ; // <show>away</show>
        if ($presence['type']!='unavailable' and $show!='away' and $show!='xa') $available[$id]=$rec_operator['login'] ;
      }
    }

    return($available) ;
 }

// возвращает информацию о собеседниках списка контактов системного аккаунта
 function get_operators_available2($options=array())
 {	// Load configuration
    $available=array() ;
     $jid=$this->system_account['login'].$this->XMPP_login_domain ;
    $conn = new XMPPHP_XMPP($this->XMPP_server_host,$this->XMPP_client_to_server_port,$jid,$this->system_account['pass'],'cur_connect');
	try
    {   $conn->connect();
	    $conn->processUntil('session_start');
        $conn->presence();     // away,

        //$conn->processUntil('roster_received') ;
        //$conn->processTime(2) ;
        //$conn->processUntil('presence') ;
        //$conn->getRoster();
        $conn->disconnect();
	}
    catch(XMPPHP_Exception $e) { error_log($e->getMessage());}
    //$arr=$conn->roster->getPresence($login.$this->XMPP_login_domain)  ;
    $arr=$conn->roster->getRoster()  ;
    unset($arr[$jid]) ;
    if ($options['debug']) { echo '<h2>conn->roster->getRoster()</h2>' ; damp_array($arr,1,-1) ; }
    if (sizeof($_SESSION['IL_chat_manager'])) foreach($_SESSION['IL_chat_manager'] as $id=>$rec_magager) $arr_man_jid[$rec_magager['login'].$this->XMPP_login_domain]=$id ;
     //damp_array($arr_man_jid) ;
     $available=array() ;
    if (sizeof($arr)) foreach($arr as $jid_login=>$rec)
        if (sizeof($rec['presence'])) foreach($rec['presence'] as $presens_name=>$present_info) if ($present_info['show']=='available')
        {  $id=$arr_man_jid[$jid_login] ;
           $login=$_SESSION['IL_chat_manager'][$id]['login'] ;
           $available[$id]=$login ;
        }


     //damp_array($available) ;
    /*
    $uStatus = $conn->roster->getPresence($jid);
    damp_array($uStatus) ;
    echo "Online status: " . $uStatus['show']; // tells whether available or unavailable or dnd
    echo "Status message: " . $uStatus['status']; // shows the user's status message
    */

     //$available[777]='test' ;
    return($available) ;
 }

 function check_new_func_get_operators_available()
 {   ob_start() ;
     $test=$this->get_operators_available2(array('debug'=>1)) ;
     $text_debug=ob_get_clean() ;
     $alert=0 ;
     if (sizeof($this->active_operators)!=sizeof($test)) $alert=1 ;
     else
     { ksort($this->active_operators) ;
       ksort($test) ;
       foreach($this->active_operators as $id=>$login) if ($test[$id]!=$login) $alert=1 ;
     }

     if ($alert)
     {
         ob_start() ;
         ?><h2>get_operators_available</h2><?
         damp_array($this->active_operators,1,-1) ;
         ?><h2>get_operators_available2</h2><?
         damp_array($test,1,-1) ;
         echo $text_debug ;
         ?><h2>NOW $conn->roster->getRoster</h2><?
         $jid=$this->system_account['login'].$this->XMPP_login_domain ;
         $conn = new XMPPHP_XMPP($this->XMPP_server_host,$this->XMPP_client_to_server_port,$jid,$this->system_account['pass'],'cur_connect');
         try {  $conn->connect();
         	    $conn->processUntil('session_start');
                $conn->presence();     // away,
                $conn->disconnect();
         	}
         catch(XMPPHP_Exception $e) { error_log($e->getMessage());}
         $arr=$conn->roster->getRoster()  ;
         unset($arr[$jid]) ;
         damp_array($arr,1,-1) ;
         $text=ob_get_clean() ;
         _send_mail(_EMAIL_ENGINE_DEBUG,'Некорректная работа chat_system->get_operators_available2',$text) ;
     }

 }

 function get_first_manager_available()
 {
    if (sizeof($this->active_operators))
    { $arr_ids=array_keys($this->active_operators) ;
      $id=$arr_ids[0] ;
      $rec=$_SESSION['IL_chat_manager'][$id] ;
    }
    else $rec=array() ;
    return($rec) ;
 }


 // возвращает информацию о числе работающих операторов сайта
 //function getRoster_count()
 //{	$roster=$this->get_operators_available() ;
//    return(sizeof($roster)) ;
 //}

 function XMPP_create_chat_user($chat_id)
 {$rec_client=array() ; $params=array() ;
  $site_id=_BASE_DOMAIN.'_'.$chat_id ;
  $params['login']=str_replace('.','_',$site_id) ;
  $params['pass']='12345' ; //$pass=rand(1,100) ;
  $params['obj_name']=$params['login'] ;
  $result=$this->XMPP_action_user('add',$params) ;
  if ($result['type']=='error' and $result['code']=='UserAlreadyExistsException')
  { $params['login']=str_replace('.','_',$site_id.'_'.rand(1,1000000)) ;
    $params['obj_name']=$params['login'] ;
    $result=$this->XMPP_action_user('add',$params) ;
  }
  if ($result['type']=='success') // если аккаунт успешно добавлен, вписываем его данные в ветку чата
  { $this->update_chat_XMPP_info($chat_id,$params['login'],$params['pass'],$params['obj_name']) ;
    $rec_client['XMPP_login']=$params['login'] ;
    $rec_client['XMPP_login_full']=$params['login'].$this->XMPP_login_domain ;
    $rec_client['name']=$params['obj_name'] ;
    $rec_client['XMPP_pass']=$params['pass'] ;
    // добавляем нового клиента в списки контактов всех операторов
    if (sizeof($_SESSION['IL_chat_manager'])) foreach($_SESSION['IL_chat_manager'] as $rec) if (!$rec['sys']) $this->XMPP_add_to_list_contact($rec['login'],$params['login']) ;
  }
  //else _event_reg('Ошибка XMPP','Не удалось создать клиента XMPP для чата на сайте') ;
  return($rec_client) ;
 }


 // операция с user
 // cmd = add,delete,disable,enable
 function XMPP_action_user($cmd,$params=array())
 {// удаляем аккаунт
  $url='http://'.$this->XMPP_server_host.':'.$this->XMPP_admin_console_port.'/plugins/userService/userservice' ;
  $par_url='type='.$cmd.'&secret='.$this->XMPP_secret_word.'&username='.$params['login'] ;
  if ($params['pass']) $par_url.='&password='.$params['pass'] ;
  if ($params['obj_name']) $par_url.='&name='.$params['obj_name'] ;
  if ($params['email']) $par_url.='&email='.$params['email'] ;
  $res_xml=file_get_contents($url.'?'.$par_url) ;
  // читаем XML
  $dom = new DomDocument();
  if (!$res_xml) return ;
  if (!$dom->loadXML($res_xml)) return ;
  // странный глюк - не видна функция get_value_tag_by_name из i_func.php, хотя файл подключен. Дублируем функцию в модуле
  $error=$this->get_value_tag_by_name($dom,'error') ; //create_chat error>UserAlreadyExistsException</error>
  $result=$this->get_value_tag_by_name($dom,'result') ; // <result>ok</result>
  if ($result=='ok')  { $res=prepare_result($cmd.'_success') ;
                        $res['rec']=array('server'=>$this->XMPP_server_host,'port'=>$this->XMPP_admin_console_port,'login'=>$params['login'].$this->XMPP_login_domain,'pass'=>$params['pass']) ;
                      }
  else                { $res=prepare_result($error) ;
                        _event_reg('XMPP server error',$cmd.' user ['.print_r($params,true).']: <strong>'.$error.'</strong>') ;
                      }
  return($res) ;
 }

 // Updates everyone's user interface with a message
 function send_XMPP_message($to_login,$from_login,$from_pass,$message,$options=array())
 {  list($to_login,$to_server)=explode('@',$to_login) ;
    list($from_login,$from_server)=explode('@',$from_login) ;
    if (!$to_login or !$from_login or !$from_pass) { echo 'Неверные параметры: '.$to_login.'/'.$from_login.'/'.$from_pass."\n" ; return ;}
    $conn = new XMPPHP_XMPP($this->XMPP_server_host,$this->XMPP_client_to_server_port,$from_login,$from_pass,$this->XMPP_server_resource,'', false, XMPPHP_Log::LEVEL_VERBOSE);
    try
    {   $conn->connect();
	    $conn->processUntil('session_start');
	    $conn->presence();
        //$conn->presence('online', "available");
        // $conn->getRoster();
	    $conn->message($to_login.$this->XMPP_login_domain, $message);
	    $conn->disconnect();
        //echo 'Отправлено сообщение от '.$from_login.' для '.$to_login.' - '.$message."\n" ;
	}
    catch(XMPPHP_Exception $e) { error_log($e->getMessage());}

    //if ($options['debug'])print_r($conn) ;
    return($conn->jid) ;
 }

// <notes>прим</notes>
 // $dom = new DomDocument();
 // if (!$dom->loadXML($XML_source)) return ;
 // $notes=get_value_tag_by_name($dom,'notes') ;
 function get_value_tag_by_name($dom,$tag_name)
 { $value='' ;
   $items=$dom->getElementsByTagName($tag_name);
   if (sizeof($items)) foreach ($items as $item) $value=trim($item->nodeValue) ;
   return($value) ;
 }

 function get_attr_tag_by_name($dom,$tag_name)
 { $rec=array() ;
   $items=$dom->getElementsByTagName($tag_name);
   if (sizeof($items)) foreach ($items as $item) $rec=get_XML_node_attr($item) ;
   return($rec) ;
 }

 // уведомляем операторов, что чат забран другим оператором и удаляем клиента из списка контактов других операторов
 function send_cancel_message($rec_chat)
 {  if (sizeof($_SESSION['IL_chat_manager'])) foreach($_SESSION['IL_chat_manager'] as $rec) if ($rec['id']!=$rec_chat['man_id'])
     {  // отпрявляем уведомление оператору
        if (in_array($rec['login'],$this->active_operators)) $this->send_XMPP_message($rec['login'],$rec_chat['XMPP_login'],$rec_chat['XMPP_pass'],str_replace('*name*',$rec_chat['autor_name'],$this->cancel_message)) ;
        // удаляем клиента из списка контактов оператора
        $this->delete_from_list_contact($rec['login'],$rec_chat['XMPP_login']) ;
     }
 }

 function XMPP_add_to_list_contact($login,$client_login,$client_name='')
 { list($login,$to_server)=explode('@',$login) ;
   list($client_login,$from_server)=explode('@',$client_login) ;
   if (!$client_name) $client_name=$client_login ;
   $url_req='http://'.$this->XMPP_server_host.':'.$this->XMPP_admin_console_port.
            '/plugins/userService/userservice?type=add_roster'.
            '&secret='.$this->XMPP_secret_word.
            '&username='.$login.
            '&item_jid='.$client_login.$this->XMPP_login_domain.
            '&name='.$client_name.
            '&subscription=3' ;
   $res_xml=file_get_contents($url_req) ;
   $url_req='http://'.$this->XMPP_server_host.':'.$this->XMPP_admin_console_port.
            '/plugins/userService/userservice?type=add_roster'.
            '&secret='.$this->XMPP_secret_word.
            '&username='.$client_login.
            '&item_jid='.$login.$this->XMPP_login_domain.
            '&name='.$login.
            '&subscription=3' ;
   $res_xml=file_get_contents($url_req) ;
   return($this->parse_XML_result($res_xml)) ;
 }

 function delete_from_list_contact($login,$client_login)
 { list($login,$to_server)=explode('@',$login) ;
   list($client_login,$from_server)=explode('@',$client_login) ;
   // удаляем клиента из контактов оператора
   $url_req='http://'.$this->XMPP_server_host.':'.$this->XMPP_admin_console_port.
             '/plugins/userService/userservice?type=delete_roster'.
             '&secret='.$this->XMPP_secret_word.
             '&username='.$login.
             '&item_jid='.$client_login.$this->XMPP_login_domain ;
   $res_xml=file_get_contents($url_req) ;
   // удаляем оператора из контактов клиентов
   $url_req='http://'.$this->XMPP_server_host.':'.$this->XMPP_admin_console_port.
            '/plugins/userService/userservice?type=delete_roster'.
            '&secret='.$this->XMPP_secret_word.
            '&username='.$client_login.
            '&item_jid='.$login.$this->XMPP_login_domain ;
   $res_xml=file_get_contents($url_req) ;
   return($this->parse_XML_result($res_xml)) ;
 }

  function parse_XML_result($res_xml)
  { $dom = new DomDocument();
    if (!$res_xml) return ;
    if (!$dom->loadXML($res_xml)) return ;
    // странный глюк - не видна функция get_value_tag_by_name из i_func.php, хотя файл подключен. Дублируем функцию в модуле
    $error=$this->get_value_tag_by_name($dom,'error') ; // error>UserAlreadyExistsException</error>
    $result=$this->get_value_tag_by_name($dom,'result') ; // <result>ok</result>
    if ($error) return(array('error'=>$error)) ;
    else        return(array('result'=>$result)) ;
  }


 //-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 //
 // функции для работы с БД
 //
 //-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

   function update_chat_XMPP_info($id,$login,$pass)
   {
      update_rec_in_table($this->tkey,array('XMPP_login'=>$login,'XMPP_pass'=>$pass),'pkey='.$id) ;
   }

   function get_XMPP_account($id)
   {
      $rec=execSQL_van('select XMPP_login,XMPP_pass from '.$this->table_name.' where pkey='.$id) ;
      return($rec) ;
   }

 
    function get_count($usl_select='')
    { $cnt=execSQL_value('select count(pkey) as cnt from '.$this->table_name.' where clss=53 '.$usl_select,0,0) ;
      return($cnt) ;
    }

    function get_count_by_status($usl_select='')
    { $sql='select status,count(pkey) as cnt from '.$this->table_name.' where clss=53 ' ;
      if ($usl_select!="") $sql.=' and '.$usl_select ;
      $sql.=' group by status' ;
      $cnt_arr=execSQL_row($sql) ;
      return($cnt_arr) ;
    }

    // приводим попдсистему к стандартному виду
    function get_items($usl,$options=array())
    { $list_chats=parent::get_items($usl,$options) ;
      //damp_array($list_chats) ;
      if (sizeof($list_chats))
      { $chat_pkeys=implode(',',array_keys($list_chats)) ;
        //echo '$chat_pkeys='.$chat_pkeys.'<br>' ;
        $list_messages=select_objs($this->tkey_mess,'','clss=54 and parent in ('.$chat_pkeys.') and enabled=1',array('group_by'=>'parent','order_by'=>'c_data')) ;
        //damp_array($list_messages) ;
        if (sizeof($list_messages[$this->tkey])) foreach($list_messages[$this->tkey] as  $chat_pkey=>$chat_messages) $list_chats[$chat_pkey]['messages']=$chat_messages ;
      }
      return($list_chats) ;
     }

    function chat_exists($uid=0)
    { if ($uid) $chat_id=execSQL_value('select pkey from '.$this->table_name.' where clss=53 and uid="'.$uid.'"') ;
      else      $chat_id=0 ;
      return($chat_id) ;
    }

    // получение записи по ветке чата, с сообщениями
    function get_active_chat_info()
    {   if ($_SESSION['chat_uid']) { $chat_rec=$this->get_chat_info_by_uid($_SESSION['chat_uid']) ;
                                     if (!$chat_rec['pkey']) unset($_SESSION['chat_uid']) ;
                                   }
        else                       { $chat_rec=array() ;
                                   }
        return($chat_rec) ;
    }

    // получение записи по ветке чата, с сообщениями
    function get_chat_info_by_uid($uid,$options=array())
    {   $chat=execSQL_van('select * from '.$this->table_name.' where enabled=1 and clss=53 and uid="'.$uid.'"') ;
        if ($chat['pkey'])
        { if (!$options['no_get_messages']) $chat['messages']=$this->get_list_mess($chat['pkey']) ;
          $this->prepare_public_info($chat) ;
        }
        return ($chat)  ;
    }

    function get_chat_info_by_id($id,$options=array())
    {  $chat=execSQL_van('select * from '.$this->table_name.' where enabled=1 and clss=53 and pkey="'.$id.'"') ;
        if ($chat['pkey'])
        { if (!$options['no_get_messages']) $chat['messages']=$this->get_list_mess($chat['pkey']) ;
          $this->prepare_public_info($chat) ;
        }
        return ($chat)  ;
    }


    function get_list_mess($id,$options=array())
    {   $order='c_data '.(($options['back_order'])? 'desc':'') ;
        $usl_time=($options['from_time'])? ' and c_data>'.$options['from_time']:'' ;
        $usl_id=($options['from_id'])? ' and pkey>'.$options['from_id']:''  ;
        $mess_list=execSQL('select * from '.$this->table_mess.' where clss=54 and parent="'.$id.'" '.$usl_time.$usl_id.' order by '.$order) ; // выбираем все сообщения темы
        return($mess_list) ;
    }

    function get_list_chat($usl,$options=array())
     { $chats=execSQL('select * from '.$this->table_name.' where clss=53 and '.$usl) ;
       if (sizeof($chats)) foreach($chats as $id=>$chat) $this->prepare_public_info($chats[$id],array('no_get_messages'=>1,'no_reffer_info'=>1)) ;
       return($chats) ;
     }


  // готовим информацию по объекту - эта функция должны быть перекрыта в кажом классе
  function prepare_public_info(&$rec,$options=array())
  { // готовим информацию по чату
    if ($rec['clss']==53)
    {   $arr_autor_info=array() ;
        $rec['obj_name']='Чат № '.$rec['pkey'].' от '.date("d.m.y G:i",$rec['c_data']) ;
        $rec['__name']=$rec['obj_name'] ;
        $rec['__data']=date("d.m.y h:i",$rec['c_data']) ;
        if ($rec['autor_name']) $arr_autor_info['ФИО']=$rec['autor_name'] ;
        if ($rec['autor_email']) $arr_autor_info['Email']=$rec['autor_email'] ;
        if ($rec['autor_phone']) $arr_autor_info['Телефон']=$rec['autor_phone'] ;
        if (sizeof($arr_autor_info)) $rec['__autor_info']=damp_serial_data($arr_autor_info);
        $rec['__chat_url']='http://'._MAIN_DOMAIN.'/chat/'.$rec['uid'].'.html' ;

        if ($rec['man_id'])
        {  $rec_oper=$_SESSION['IL_chat_manager'][$rec['man_id']] ;
           $rec['operator_login']=$rec_oper['login'] ;
           $rec['operator_name']=$rec_oper['obj_name'];
           $rec['operator_function']=$rec_oper['function'];
           if ($rec_oper['_image_name']) $rec['operator_avatar']=img_clone($rec_oper,'small') ;
           $rec['operator_available']=$this->get_operator_status($rec_oper['login']) ;

        }

        if ($rec['XMPP_login']) $rec['XMPP_login_full']=$rec['XMPP_login'].$this->XMPP_login_domain ;
    }
    // готовим информацию по сообщению
    if ($rec['clss']==54)
    {

    }
  }

  function get_operator_by_login($login)
  { $result=array() ;
    list($login,$device)=explode('/',$login) ; //  roman@clss.ru/iMac-Roman-Breznev
    if (sizeof($_SESSION['IL_chat_manager'])) foreach($_SESSION['IL_chat_manager'] as $rec)
      if ($login==$rec['login'].$this->XMPP_login_domain) $result=$rec ;
    return($result) ;

  }



 //-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 //
 // функции вызываемые при обработке страниц подсистемы
 //
 //-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 function select_obj_info()
 { if ($_GET['id'])    $GLOBALS['obj_info']=$this->get_chat_info_by_uid($_GET['id']) ; // получаем информацию по текущему чату
   if ($_GET['mid'])   $GLOBALS['obj_info']=$this->get_chat_info_by_uid($_GET['mid']) ; // получаем информацию по текущему чату

   if (($_GET['mid'] or $_GET['id']) and !$GLOBALS['obj_info']['pkey'])
   { $GLOBALS['obj_info']=prepare_result('theme_not_found') ; // одна система
     $GLOBALS['obj_info']['__open_result']=prepare_result('theme_not_found') ; //  вторая система
   }

   // в дальнейшем передалить систему авториризованного доступа к чатам
   /*  if ($chat_system->use_autorize_to_otwet and $obj_info['open_mode']==1 and !$member->id)
        {	?>Для совершения ответа на вопрос необходимо авторизоваться на сайте<?
        		account_panel_login_promp() ;
         	return ;
        }
   */
 }

 //-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 //
 // функции создания чата и все связанное с этим
 //
 //-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 // ОТКРЫВАЕМ ЧАТ:
 // - создаем новую ветку для текущего пользователя в таблице БД
 // - добавляем сообщение в ветку
 // - создаем нового user на XMPP сервере
 function create_chat($data,$options=array())
 { global $member ;
   $debug=$options['debug'] ;
   // если заполнены не все поля, показать ошибку
   if (!$data['message']) return(prepare_result('error_field')) ;
   $sys_info=array() ;
   $sys_info['IP']=$_SERVER['REMOTE_ADDR'] ;
   $sys_info['Браузер']=defines_vers($_SERVER['HTTP_USER_AGENT']) ;
   $sys_info['Информация агента']=$_SERVER['HTTP_USER_AGENT'] ;
   $sys_info['Монитор']=$data['screen_width'].' x '.$data['screen_height'].' px' ;
   $sys_info['Страница сайта']=$_SERVER['HTTP_REFERER'] ;

   // создаем новый чат
   $chat=array(	    'parent'		=>	$this->chats_parent, // код папки новых веток
			        'clss'			=>	53, // 53 - ветка
     				'obj_name'		=>	'Чат',
     				'c_data'		=>	time(),
    				'account_reffer'=> 	($member->id)? $member->reffer:'',
    				'uid'			=>  md5(uniqid(rand(),true)),
                    'status'		=>	0,     // =0 - новый, не отвечен, еще не взят никаким оператором
    				'autor_email'	=>  $data['email'],
    				'autor_name'	=>  $data['name'],
    				'autor_phone'	=>  $data['phone'],
                    'autor_info'	=>  $sys_info // расширенная информация по автору тикета
   				) ;

   if ($debug) damp_array($chat) ;
   $chat_id=adding_rec_to_table($this->tkey,$chat) ; // создаем новую ветку
   if (!$chat_id) return(prepare_result('error_db_create_chat')) ;

   $this->db_add_message($chat_id,array('message'=>$data['message']),array('no_update_status_chat'=>1)) ;

   $this->XMPP_create_chat_user($chat_id) ; // создаем нового user и добавляем его в списки контактов всех операторов

   $result=prepare_result('success') ;
   $result['rec']=$this->get_chat_info_by_id($chat_id,array('no_get_messages'=>1)) ;

   // регистрируем событие 'Открытие существующей темы'
   _event_reg('Новый чат','',$result['rec']['_reffer'],$result['rec']['account_reffer']) ;


   $result['chat_uid']=$chat['uid'] ;
   $result['chat_id']=$chat_id ;


   return($result) ;
 }

 function close_chat(&$chat_rec)
 { $sql='update '.$this->table_name.' set status=2 where pkey='.$chat_rec['pkey'] ;
   execSQL_update($sql) ;
   _event_reg('Закрытие чата','',$chat_rec['_reffer']) ;
   $chat_rec['status']=2 ;
 }

 // продолжает существующую тему
 // внимание - поле autor для сообщения заполняется только для персонала сайта
 // указываем uid чата, для которого необходимо добавить сообщение
 // функция возвращает массив - описание чата
 function db_add_message($chat_id,$rec=array(),$options=array())
 { global $member ;
   if (!$rec['message']) return(prepare_result('error_field')) ; // если заполнены не все поля, показать ошибку
   if (!$rec['status']) $rec['status']=0 ;
   $finfo=array('parent'        =>	$chat_id,
				'enabled'		=>	1,
				'clss'			=>	54, // 54 - сообщение
	     		'obj_name'		=>	$rec['message'],
	     		'man_id'		=>	$rec['man_id'] // id оператора, если это был ответ
	   	        ) ;

   if ($member->id and !$rec['man_id']) // если клиент авторизован и сейчас не ответ оператора
     { if (!$rec['account_reffer']) $rec['account_reffer']=$member->reffer ;
       if (!$rec['autor_name'])     $rec['autor_name']=$member->name ;
     }

   adding_rec_to_table($this->tkey_mess,$finfo) ; // продолжаем новую ветку

   // обновляем статус чата
   if (!$options['no_update_status_chat'])
   { $rec_update=array() ;
     $rec_update['status']=($rec['man_id'])? 1:2 ; // 1 - отвечен, это сообщение оператора, 2 - не отвечен, это сообщение клиента
     if ($rec['man_id']) $rec_update['man_id']=$rec['man_id'] ;
     update_rec_in_table($this->tkey,$rec_update,'pkey='.$chat_id) ;
   }

   $result=prepare_result('success') ;
   return($result) ;
 }

}

?>