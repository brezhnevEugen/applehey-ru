<?
 function chat_message_UserAlreadyExistsException()
 { ?><p class=alert>Оператор с таким логином уже создан. Используйте другой логин.</p><?}

 function chat_message_add_success($rec=array())
 { ?><p class=info>Оператор успешно добавлен.</p>
     <strong>Реквизиты для подключения оператора к чату:</strong>
     <table class="debug left">
         <tr><td>Сервер:</td><td><?echo $rec['server']?></td></tr>
         <tr><td>Порт:</td><td><?echo $rec['port']?></td></tr>
         <tr><td>Jabber ID:</td><td><?echo $rec['login']?></td></tr>
         <tr><td>Пароль:</td><td><?echo $rec['pass']?></td></tr>
     </table>
   <?
   //damp_array($rec) ;
 }



















 function chat_message_theme_not_found()
 { ?><p class=alert>Указанная Вами тема не существует. Возможно, она была удалена или Вы ошиблись при наборе ссылки.</p><?}

 function chat_message_tiket_not_found()
 { ?><p class=alert>Указанный Вами тикет не существует. Возможно, он был удален или Вы ошиблись при наборе ссылки.</p><?}

 function chat_message_error_check_code()
 { ?><p class=alert>Вы неправильно ввели цифровой проверочный код. Пожалуйста, попробуйте еще раз.</p><?}

 function chat_message_error_field()
 { ?><p class=alert>Вы не заполнили поля, необходимые для отправки сообщения. Пожалуйста, попробуйте еще раз.</p><?}

 // сообщение о создании нового тикета в службе поддержки
 function chat_message_create_tiket_success($tiket)
  {  ?>   <h2>Уважаемый(ая) <? echo $tiket['autor_name']?>!</h2>
          <p>Ваше сообщение в службу поддержки зарегистрированно за тикетом № <?echo $tiket['pkey']?> от <?echo date("d.m.Y",$tiket['c_data']) ;?>. </p>
	  	  <p>Просмотреть ответ и задать последующие вопросы вы можете, перейдя по ссылке:</p>
          <p class="center url"><a href="<?echo $tiket['__autor_url'] ?>"><?echo $tiket['__autor_url'] ?></a></p>
		  <p>В ближайшее время вы получите ответ на интересующий вас вопрос.</p>
		  <p>С уважением, служба поддержки <strong><?echo _MAIN_DOMAIN?></strong></p>
     <?
   }

 // сообщение о создании нового сообщения в тикет служьы поддержки
 function chat_message_create_wopros_success($tiket)
  {  ?>   <h2>Уважаемый(ая) <? echo $tiket['autor_name']?>!</h2>
          <p>Ваше сообщение в службу поддержки добавлено в тикет № <?echo $tiket['pkey']?> от <?echo date("d.m.Y",$tiket['c_data']) ;?>. </p>
	  	  <p>Просмотреть ответ и задать последующие вопросы вы можете, перейдя по ссылке:</p>
		  <p class="center url"><a href="<?echo $tiket['__autor_url'] ?>"><?echo $tiket['__autor_url'] ?></a></p>
		  <p>В ближайшее время вы получите ответ на интересующий вас вопрос.</p>
	      <p>С уважением, служба поддержки <strong><?echo _MAIN_DOMAIN?></strong></p>
     <?
 }

 // сообщение о создании нового сообщения в тикет служьы поддержки
 function chat_message_error_create_tiket()
  {  ?><h2>Внимание!</h2>
       <p>К сожалению, добавление Вашего сообщения по техническим причинам невозможно.Службе поддержки сайта направлено соответствующее сообшение.</p>
	   <p>С уважением, служба поддержки <strong><?echo _MAIN_DOMAIN?></strong></p>
     <?
 }

 function chat_message_tiket_is_closed()
  {  ?><h2>Тикет закрыт</h2>
       <p>Ваш тикет закрыт администрацией сайта, добавление новых сообщений в него невозможно.</p>
       <p>Пожалуйста, <a href="/support/">создайте новый тикет</a> с интересующим Вас вопросом </p>
       <p>С уважением, служба поддержки <strong><?echo _MAIN_DOMAIN?></strong></p>
     <?
 }

 function chat_message_create_otwet_success($tiket)
  {  ?><p>Ответ для тикета № <?echo $tiket['pkey']?> от <?echo date("d.m.Y",$tiket['c_data']) ;?> был успешно добавлен!</p>
	   <p>Просмотреть тикет и ответить повторно вы можете, перейдя по ссылке:</p>
       <p class="center url"><a href="<?echo $tiket['__moderator_url'] ?>" target=_top><?echo $tiket['__moderator_url'] ?></a></p>
     <?
 }

 // сообщение о с предложением создать тикет
 function chat_message_promt_create()
 { ?><div class=support_promt_create><a href="/support/">Задать вопрос</a></div>
    <style type="text/css">div.support_promt_create{text-align:center;font-size:16px;font-weight:bold;}</style>
    <?
 }

 // сообщение о создании нового тикета в службе поддержки
 function chat_message_create_tiket_to_price_success($tiket)
  { ?><h2>Уважаемый(ая) <? echo $tiket['autor_name']?>!</h2>
      <p>Ваш запрос на цену оборудования "<strong><?echo $tiket['obj_reffer_name']?></strong>" зарегистрирован за тикетом № <?echo $tiket['pkey']?> от <?echo date("d.m.Y",$tiket['c_data']) ;?>. </p>
      <p>Просмотреть ответ и задать последующие вопросы вы можете, перейдя по ссылке:</p>
      <p class="center url"><a href="<?echo $tiket['__autor_url'] ?>"><?echo $tiket['__autor_url'] ?></a></p>
      <p>В ближайшее время вы получите ответ по Вашему запросу.</p>
      <p>С уважением, администрация сайта <strong><?echo _MAIN_DOMAIN?></strong></p>
     <?
   }

 // сообщение о создании нового тикета в службе поддержки
 function chat_message_create_tiket_to_chet_success($tiket)
  { ?><h2>Уважаемый(ая) <? echo $tiket['autor_name']?>!</h2>
      <p>Ваш запрос на счет для "<strong><?echo $tiket['obj_reffer_name']?></strong>" зарегистрирован за тикетом № <?echo $tiket['pkey']?> от <?echo date("d.m.Y",$tiket['c_data']) ;?>. </p>
      <p>Просмотреть ответ и задать последующие вопросы вы можете, перейдя по ссылке:</p>
      <p class="center url"><a href="<?echo $tiket['__autor_url'] ?>"><?echo $tiket['__autor_url'] ?></a></p>
      <p>В ближайшее время вы получите ответ по Вашему запросу.</p>
      <p>С уважением, администрация сайта <strong><?echo _MAIN_DOMAIN?></strong></p>
     <?
   }

 // сообщение о создании нового тикета в службе поддержки
 function chat_message_create_tiket_to_commerce_success($tiket)
  { ?><h2>Уважаемый(ая) <? echo $tiket['autor_name']?>!</h2>
      <p>Ваш запрос на коммерческое предложение для "<strong><?echo $tiket['obj_reffer_name']?></strong>" зарегистрирован за тикетом № <?echo $tiket['pkey']?> от <?echo date("d.m.Y",$tiket['c_data']) ;?>. </p>
      <p>Просмотреть ответ и задать последующие вопросы вы можете, перейдя по ссылке:</p>
      <p class="center url"><a href="<?echo $tiket['__autor_url'] ?>"><?echo $tiket['__autor_url'] ?></a></p>
      <p>В ближайшее время вы получите ответ по Вашему запросу.</p>
      <p>С уважением, администрация сайта <strong><?echo _MAIN_DOMAIN?></strong></p>
     <?
   }

 // сообщение о создании нового тикета в службе поддержки
 function chat_message_create_tiket_success2($tiket)
  {  ?><h2>Уважаемый(ая) <? echo $tiket['autor_name']?>!</h2>
       <p>Ваша заявка зарегистрированна за тикетом № <?echo $tiket['pkey']?> от <?echo date("d.m.Y",$tiket['c_data']) ;?>. </p>
       <p>Просмотреть ответ и задать последующие вопросы вы можете, перейдя по ссылке:</p>
       <p class="center url"><a href="<?echo $tiket['__autor_url'] ?>"><?echo $tiket['__autor_url'] ?></a></p>
       <p>В ближайшее время вы получите ответ по Вашему запросу.</p>
       <p>С уважением, администрация сайта <strong><?echo _MAIN_DOMAIN?></strong></p><br><br>
     <?
   }


 // сообщение о создании нового тикета в службе поддержки
 function chat_message_create_tiket_success3($tiket)
  {  ?><h2>Уважаемый(ая) <? echo $tiket['autor_name']?>!</h2>
       <p>Ваша заявка на материал зарегистрированна за тикетом № <?echo $tiket['pkey']?> от <?echo date("d.m.Y",$tiket['c_data']) ;?>. </p>
       <p>Просмотреть ответ и задать последующие вопросы вы можете, перейдя по ссылке:<br><br>
       <p class="center url"><a href="<?echo $tiket['__autor_url'] ?>"><?echo $tiket['__autor_url'] ?></a></p>
       <p>В ближайшее время вы получите ответ по Вашему запросу.</p>
       <p>С уважением, администрация сайта <strong><?echo _MAIN_DOMAIN?></strong></p
     <?
 }











?>