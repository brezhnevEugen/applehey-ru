<?php
define('_BODY_CLASS','standart') ;
include_once(_DIR_EXT."/HTML/c_HTML.php") ;
include_once(_DIR_EXT.'/pages_history/i_history.php') ;
class c_page extends c_HTML
{

  public $body_class='' ;

 function set_head_tags()
 { // настройки, определяющие HEADER страниц сайта
   //$_SESSION['meta_info'][]=array('name'=>"verify-v1",'content'=>"fMcQzJ+VVOl+u8t3qcp0lyxkzRJn/QiqwNSBvoRB6Lg=") ;
   $this->HEAD['meta'][]=array('name'=>"yandex-verification",'content'=>"62b5ed449ed14807") ;
   $this->HEAD['favicon']='/favicon.ico'  ;

   // стандартный JQuery

   $this->HEAD['js'][]=_PATH_TO_EXT.'/jquery/jquery-3.2.0.js' ;
   $this->HEAD['js'][]=_PATH_TO_EXT.'/jquery/jquery-migrate-1.4.1.min.js';
   $this->HEAD['js'][]=_PATH_TO_EXT.'/jquery/jquery.timers-1.2.js' ;
   $this->HEAD['js'][]=_PATH_TO_EXT.'/jquery/jquery-ui-1.9.1.custom.min.js' ; ;
   $this->HEAD['js'][]=_PATH_TO_EXT.'/mBox/jquery.form.js' ;
   $this->HEAD['js'][]=_PATH_TO_EXT.'/jquery/jquery.maskedinput.min.js' ;

   // mootools
   $this->HEAD['js'][]=_PATH_TO_EXT.'/mootools/mootools-core-1.3.1.js' ;
   $this->HEAD['js'][]=_PATH_TO_EXT.'/mootools/mootools-more-1.3.1.1.js' ;

   // библитека jBox + замена mBox
   $this->HEAD['js'][]=_PATH_TO_EXT.'/mBox/jBox-master/Source/jBox.min.js' ;
   $this->HEAD['css'][]=_PATH_TO_EXT.'/mBox/jBox-master/Source/jBox.css' ;

   // библитека mBox - mForm + модальные окна
   $this->HEAD['css'][]='/assets/mBoxModal.css' ;
   $this->HEAD['js'][]=_PATH_TO_EXT.'/mBox/StephanWagner-mForm-0.2.6/mForm.Core.js' ;
   $this->HEAD['js'][]=_PATH_TO_EXT.'/mBox/StephanWagner-mForm-0.2.6/mForm.Element.js' ;
   $this->HEAD['js'][]=_PATH_TO_EXT.'/mBox/StephanWagner-mForm-0.2.6/mForm.Element.Select.js' ;
   $this->HEAD['js'][]=_PATH_TO_EXT.'/mBox/StephanWagner-mForm-0.2.6/mForm.Submit.js' ;
   $this->HEAD['css'][]='/assets/mForm.css' ; //_PATH_TO_EXT.'/mBox/StephanWagner-mForm-0.2.6/assets/mForm.css' ;
   $this->HEAD['css'][]='/assets/mFormElement-Select.css' ; //_PATH_TO_EXT.'/mBox/StephanWagner-mForm-0.2.6/assets/mFormElement-Select.css' ;
   $this->HEAD['js'][]=_PATH_TO_EXT.'/mBox/modal_window.jBox.js' ;

   // стандартное подключение highslide
   $this->HEAD['js'][]=_PATH_TO_EXT.'/highslide/highslide-4.1.13/highslide.js' ;
   $this->HEAD['css'][]=_PATH_TO_EXT.'/highslide/highslide-4.1.13/highslide.css' ;

   //оформление select
   $this->HEAD['js'][]=_PATH_TO_EXT.'/select/jquery.easydropdown.min.js' ;
   $this->HEAD['css'][]=_PATH_TO_EXT.'/select/themes/easydropdown.css' ;
//   $this->HEAD['css'][]=_PATH_TO_EXT.'/select/themes/easydropdown.flat.css' ;
//   $this->HEAD['css'][]=_PATH_TO_EXT.'/select/themes/easydropdown.metro.css' ;

   //расширение - служба поддержки
   $this->HEAD['css'][]=_PATH_TO_EXT.'/orders/style.css' ;

   //расширение - on-line чат XMPP
   //$this->HEAD['js'][]=_PATH_TO_EXT.'/chat/strophejs/strophe.js' ;
   //$this->HEAD['js'][]=_PATH_TO_EXT.'/chat/buzz.min.js' ;
   //$this->HEAD['js'][]=_PATH_TO_EXT.'/chat/script.js' ;
   //$this->HEAD['css'][]=_PATH_TO_EXT.'/chat/style.css' ;


   //font-awesome-4.2.0
   //$this->HEAD['css'][]=_PATH_TO_EXT.'/font-awesome-4.2.0/css/font-awesome.css' ;

   //расширение - служба поддержки
   //$this->HEAD['css'][]=_PATH_TO_EXT.'/support/style.css' ;

   //расширение - отзывы
   $this->HEAD['css'][]=_PATH_TO_EXT.'/responses/style.css' ;

   //расширение - отзывы
   $this->HEAD['js'][]='/js/jquery.rd-parallax.js' ;
   //расширение - простой поиск по сайту
   $this->HEAD['js'][]=_PATH_TO_EXT.'/search/jquery.search_lite.js' ;
   $this->HEAD['css'][]=_PATH_TO_EXT.'/search/style.css' ;

   // расширение tinyscroollbar
   //$this->HEAD['js'][]=_PATH_TO_EXT.'/tinyscroollbar/jquery.tinyscrollbar.min.js' ;

   //$this->HEAD['js'][]=_PATH_TO_EXT.'/slider/slider.js' ;
   //$this->HEAD['css'][]=_PATH_TO_EXT.'/slider/slider.css' ;
   //$this->HEAD['css'][]=_PATH_TO_EXT.'/slider/images4/1.css' ;
   //$this->HEAD['js'][]=_PATH_TO_EXT.'/panel_filter/script_filter.js' ;
   //$this->HEAD['css'][]=_PATH_TO_EXT.'/panel_filter/style.css' ;

   //расширение - карусель товаров
//   $this->HEAD['js'][]=_PATH_TO_EXT.'/carusel_goods/script.js' ;
//   $this->HEAD['css'][]=_PATH_TO_EXT.'/carusel_goods/style.css' ;
   //расширение - Банеры карусель
//   $this->HEAD['js'][] = _PATH_TO_EXT . '/carusel_banner/script.js';
//   $this->HEAD['css'][] = _PATH_TO_EXT . '/carusel_banner/style.css';



  $this->HEAD['css'][]='/css/grid.css';
   $this->HEAD['css'][]='/css/style.css';

   $this->HEAD['css'][]='/css/owl-carousel.css';
   $this->HEAD['css'][]='/css/subsribe_mailform.css';
   $this->HEAD['css'][]='/css/styleswitcher.css';


   // стандартные файлы - должны подключаться последними
   $this->HEAD['css'][]='/assets/clear-sans-fontfacekit/web fonts/clearsans_light_cyrillic/stylesheet.css' ;
   $this->HEAD['css'][]='/assets/clear-sans-fontfacekit/web fonts/clearsans_bold_cyrillic/stylesheet.css' ;
   $this->HEAD['js'][]='/script.js' ;
   $this->HEAD['css'][]='/style.css' ;

 }

 // ----------------------------------------------------------------------------------------------------------------------------
 // структура сайта
 // ----------------------------------------------------------------------------------------------------------------------------

  function body()
  { ?>
    <body class="x1 <?echo $this->body_class?>">
      <div class = "page">
        <header class="main_page"><?$this->header();?></header>
        <main><?$this->block_main();?></main>
        <footer class="main_page"><?$this->footer();?></footer>
      </div>
      <script src = "/js/script.js"></script>
    </body>

    <?
  }

 // ----------------------------------------------------------------------------------------------------------------------------
 // СТРУКТУРА БЛОКОВ САЙТА
 // ----------------------------------------------------------------------------------------------------------------------------

 function header()
 { ?>
   <div id = "stuck_container" class = "stuck_container">
     <div class = "container">

       <div class = "brand">
         <img id="logo" src="/images/logo.png">
         <h1 class = "brand_name">
           <a href = "/">Apple<span>Hey</span></a>
         </h1>
         <p class = "brand_slogan">service center</p>
       </div>
       <nav class = "nav"><?_show_menu(15,'menu/list_items_menu_ul_with_subitems',array('id'=>'panel_main_menu','class'=>'sf-menu','data-type'=>'navbar')) ; ?>
<!--         <ul class = "sf-menu" data-type = "navbar">-->
<!--           <li class = "active"><a href = "/">Home</a></li>-->
<!--           <li><a href = "index-1.html">About us</a></li>-->
<!--           <li><a href = "index-2.html">Services</a></li>-->
<!--           <li><a href = "index-3.html">Shop Online</a>-->
<!--             <ul>-->
<!--               <li><a href = "/#">in-store services</a></li>-->
<!--               <li><a href = "/#">cell phone parts</a>-->
<!--                 <ul>-->
<!--                   <li><a href = "/#">Batteries</a></li>-->
<!--                   <li><a href = "/#">Screen Protectors</a></li>-->
<!--                   <li><a href = "/#">Headphones</a></li>-->
<!--                   <li><a href = "/#">Adapters</a></li>-->
<!--                 </ul>-->
<!--               </li>-->
<!--               <li><a href = "/#">start your repair</a></li>-->
<!--             </ul>-->
<!--           </li>-->
<!--           <li><a href = "index-4.html">Repair</a></li>-->
<!--           <li><a href = "index-5.html">Contacts</a></li>-->
<!--         </ul>-->
       </nav>
     </div>
     </div>
   <?
  }


 // нижняя часть страницы
 function footer()
 { ?>
   <div class = "container">
     <div class = "brand">
       <h1 class = "brand_name">
         <a href = "/">Cellra<span>x</span></a>
       </h1>
       <p class = "brand_slogan">Cellular Repair Center</p>
     </div>
     <ul class = "inline-list">
       <li><a href = "/#" class = "fa-instagram"></a></li>
       <li><a href = "/#" class = "fa-twitter"></a></li>
       <li><a href = "/#" class = "fa-facebook"></a></li>
     </ul>
     <div class = "wrapper cntr">
       <p>Copyright © <span id = "copyright-year"></span> | <a href = "index-6.html">Privacy Policy</a></p>
     </div>
   </div>
   <?
 }






 // ----------------------------------------------------------------------------------------------------------------------------
 // стандартные блоки
 // ----------------------------------------------------------------------------------------------------------------------------



 function panel_search()
 {  include_once(_DIR_EXT.'/search/panel_search_lite.php') ;
    panel_search_lite() ;
 }

 function panel_backcall()
 {
    ?><div id="panel_backcall"><button class="v2" cmd="request_phone_callback/get_backcall_panel">Обратный звонок</button></div><?
 }

 function panel_last_news()
  { global $news_system ;
    ?><div id="panel_last_news"><div class=title>Новости</div><a href="/news/">все новости</a><?
     $news_system->show_last_items('news/list_news_data_a_name_annot',array('count'=>1,'annot'=>130)) ;

    ?></div><?
  }

  function panel_rand_responses()
  { global $responses_system ;
    ?><div id=panel_rand_responses><div class="title_gray">Отзывы</div><?
    $responses_system->show_list_items('','responses/list_responses_messages_small',array('debug'=>0,'no_show_ref_obj_info'=>0,'order_by'=>'rand()','count'=>5,'text_count'=>100)) ;
    ?><div class="go_all"><a href="/responses/">Читать все отзывы</a></div><?
    ?></div><?
  }

  function panel_phone_callback()
  { ?>
    <div id="panel_phone_callback">
      <button class="v2 button_white " cmd="request_phone_callback/get_backcall_panel">Заказать звонок</button>
    </div><?
  }

  function panel_banners($baner_group)
  {
     _banner_list($baner_group,'carusel_banner/list_banners',array('id' => 'panel_goods_banner','count' => 3));
     ?><script type = "text/javascript">$j('div#panel_goods_banner').carusel_banner({step: 300});</script><?
  }

}