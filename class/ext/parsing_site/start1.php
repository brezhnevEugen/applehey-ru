<?
//    ext_panel_register(array('frame'=>array('table_code'=>'goods','clss'=>201),'menu_item'=>array('name'=>'Импорт Терра','script'=>'terra.php','cmd'=>'parsing_site/import_brand_collection'))) ;
//    ext_panel_register(array('frame'=>array('table_code'=>'goods','clss'=>210),'menu_item'=>array('name'=>'Импорт Терра','script'=>'terra.php','cmd'=>'parsing_site/import_collection_goods'))) ;


include_once(_DIR_TO_ENGINE.'/admin/i_admin_system.php') ;
include_once('simple_html_dom.php') ;

function import_start1($options=array())
{
   ?><button class=v1 cmd="parsing_site/import_goods" script="start1.php">Загрузить товары</button><?
   ?><button class=v1 cmd="parsing_site/import_goods_img" script="start1.php">Загрузить фото товаров</button><?
}




 function import_goods($options=array())
 { //$XML_source=file_get_contents(_DIR_TO_ROOT.'/products3.xml') ;
   //import_goods_by_url($XML_source) ;
   //return ;
   $recs=execSQL_line('select source_url from obj_site_goods where source_url!="" and clss=62') ;
   if (sizeof($recs)) foreach($recs as $url)
   { $XML_source=get_page($url.'/products.xml',array('login'=>'products','password'=>'4Q4hmg97')) ;
     $XML_source=str_replace('version="1.1"','version="1.0"',$XML_source) ;
     ?><h1><?echo $url?></h1><?
     if (strpos($XML_source,'Not Found')!==false) echo '<h2 class="red">404</h2>' ;
     else import_goods_by_url($XML_source) ;
   }
   //
 }


 function import_goods_by_url($XML_source)
 {  global $goods_system,$category_system ;



    $market = new SimpleXMLElement($XML_source);

    $arr_brands=array() ;  $arr_cat=array() ; $arr_subcat=array() ;  $type_id_by_name=array() ;  $arr_attr_id_by_name=array() ; $arr_attr_type_by_name=array() ;
    $arr_cat_names=array() ;  $arr_subcat_names=array() ; $arr_subcat_parents=array();   $arr_brands_names=array() ;

    // получаем название брендов и их коды, проверяем существование бренда, добавляем в таблицу
     ?><h2>brands</h2><?

     foreach ($market->brands->brand as $brand)
     {  $cur_brand_id=execSQL_value('select pkey from obj_site_goods where clss=62 and obj_name like "'.(string)$brand->name.'"') ;
        if (!$cur_brand_id)
        {  $rec_item=array('parent'=>1,'clss'=>62,'obj_name'=>(string)$brand->name,'source_id'=>(int)$brand->id) ;
           $cur_brand_id=adding_rec_to_table('obj_site_goods',$rec_item) ;
           echo 'Добавлен бренд <strong>'.$brand->name.'</strong><br>' ;
        } else echo 'Бренд <strong>'.$brand->name.'</strong> уже существует<br>' ;
        $arr_brands[(int)$brand->id]=$cur_brand_id ;
        $arr_brands_names[(int)$brand->id]=(string)$brand->name ;
        echo '<br>' ;
     }

    damp_array($arr_brands) ;

    ?><h2>category</h2><?
    foreach ($market->categories->category as $cat)
    {
       $arr_cat_names[(int)$cat->id]=(string)$cat->name ;
    }

   ?><h2>subcategory</h2><?
   if (sizeof($market->categories->category)) foreach ($market->subcategories->subcategory as $subcat)
   { $arr_subcat_names[(int)$subcat->id]=(string)$subcat->name ;
     $arr_subcat_parents[(int)$subcat->id]=(int)$subcat->category ;
   }

   /*?><h2>type</h2><? foreach ($market->types->type as $type) { echo $type->id. ' - '.$type->name.'<br>' ; } */


   ?><h2>attributes</h2><?
   foreach ($market->attributes->attribute as $attribute)
   { echo $attribute->id. ' - '.$attribute->name.'<br>' ;
     $arr_attr[(int)($attribute->id)]=(string)$attribute->name ;
   }

   ?><h2>products</h2><?
   $i=0 ;
   foreach ($market->products->product as $product)
   {  // загружаем описание товара
      $id=execSQL_value('select pkey from obj_site_goods where clss=200 and source_id="'.(int)$product->id.'" and obj_name="'.(string)$product->name.'"') ;
      $cur_cat_id=0 ;  $cur_subcat_id=0 ;
      $rec_item=array('obj_name'=>(string)$product->name,
                      'source_id'=>(int)$product->id,
                      'price'=>(float)$product->price,
                      'intro'=>(string)$product->short_description,
                      'manual'=>(string)$product->description,
                      'manual2'=>(string)$product->technology,
                      'temp'=>1 // для последующего обновления фото
                     ) ;
      if (!$id)
       {  // внимание!!! раздел и подраздел, в котом будет находиться товар, возможно, еще не создан
          // надо проверить их существование и создать при необходимости. Существование проверяем по cat_id
          $product_id=(int)$product->id ;
          $cur_brand_idd=(int)$product->brand ;
          $cur_brand_id=$arr_brands[$cur_brand_idd] ;
          if ((string)$product->brand=='None') { $cur_brand_id=13 ; $cur_brand_idd='none' ; }

          $cur_brand_name=$arr_brands_names[(int)$product->brand] ;
          $cur_subcat_idd=(int)$product->subcategory ;
          $cur_cat_idd=($cur_subcat_idd)? $arr_subcat_parents[$cur_subcat_idd]:(int)$product->category ;
          $cur_cat_name=$arr_cat_names[$cur_cat_idd] ;
          // проверяем существование раздела в бренде
          if (!$arr_cat[$cur_brand_idd][$cur_cat_idd])
          { $cur_cat_id=execSQL_value('select pkey from obj_site_goods where clss=10 and source_id="'.$cur_cat_idd.'" and parent='.$cur_brand_id) ;
               if (!$cur_cat_id)
               {  $rec_section=array('parent'=>$cur_brand_id,'clss'=>10,'obj_name'=>$cur_cat_name,'source_id'=>$cur_cat_idd,'brand_id'=>$cur_brand_id) ;
                  $cur_cat_id=adding_rec_to_table('obj_site_goods',$rec_section) ;
                  echo 'Добавлен раздел <strong>'.$cur_cat_name.'</strong> в бренд <strong>'.$cur_brand_name.'</strong><br>' ;
               } else echo 'Раздел <strong>'.$cur_cat_name.'</strong> уже существует<br>' ;
             $arr_cat[$cur_brand_idd][$cur_cat_idd]=$cur_cat_id ;
          } else $cur_cat_id=$arr_cat[$cur_brand_idd][$cur_cat_idd] ;
          // проверяем существование подраздела в разделе
          if ($cur_subcat_idd)
          {  $cur_subcat_name=$arr_subcat_names[$cur_subcat_idd] ;
             if (!$arr_subcat[$cur_brand_idd][$cur_cat_idd][$cur_subcat_idd])
              {  $cur_subcat_id=execSQL_value('select pkey from obj_site_goods where clss=10 and source_id="'.$cur_subcat_idd.'" and parent='.$cur_cat_id) ;
                 if (!$cur_subcat_id)
                 {  $rec_subsection=array('parent'=>$cur_cat_id,'clss'=>10,'obj_name'=>$cur_subcat_name,'source_id'=>$cur_cat_idd,'brand_id'=>$cur_brand_id) ;
                     $cur_subcat_id=adding_rec_to_table('obj_site_goods',$rec_subsection) ;
                    echo 'Добавлен подраздел <strong>'.$cur_subcat_name.'</strong> в раздел <strong>'.$cur_cat_name.'</strong><br>' ;
                 } else echo 'Подаздел <strong>'.$cur_subcat_name.'</strong> уже существует<br>' ;
                 $arr_subcat[$cur_brand_idd][$cur_cat_idd][$cur_subcat_idd]=$cur_subcat_id ;
              } else $cur_subcat_id=$arr_subcat[$cur_brand_idd][$cur_cat_idd][$cur_subcat_idd] ;
          }

          $rec_item['clss']=200 ;
          $rec_item['parent']=($cur_subcat_idd)? $cur_subcat_id:$cur_cat_id ;
          $rec_item['brand_id']=$cur_brand_id ;
          $id=adding_rec_to_table('obj_site_goods',$rec_item) ;
          echo 'Товар <strong>'.$product->name.'</strong> добавлен<br>' ;
       }
       else
       { update_rec_in_table('obj_site_goods',$rec_item,'pkey='.$id) ;
         echo 'Товар <strong>'.$product->name.'</strong>,  обновлен<br>' ;
       }
      //damp_array($rec_item,1,-1) ;

      // загружаем фото  - производиться отдельно
      /*
      $goods_reffer=$id.'.'.$goods_system->tkey ;
      foreach ($product->images->image as $image)
          {  //var_dump($image) ; echo '<br>' ;
             $img_url=(string)$image->url ;
             $img_name=basename($img_url) ;
             $rec_id=execSQL_value('select pkey from obj_site_goods_image where parent='.$id.' and file_name like "%'.$img_name.'"') ;
             if (!$rec_id)
             { $res=obj_upload_image_by_href($goods_reffer,$img_url,array('view_upload'=>1,'debug'=>0,'finfo'=>array('obj_name'=>$rec_item['obj_name']))) ;
               if ($res[1]) echo '<strong>Фото</strong> '.$img_url.' => '.hide_server_dir($res[1]).'<br>' ;
             } else echo 'Фото <strong>'.$img_name.'</strong> уже загружено<br>' ;
          }
      */

      // загружаем видео
      foreach ($product->videos->video as $video)
          {  $video_url=(string)$video ;
             $rec_id=execSQL_value('select pkey from obj_site_goods_video where parent='.$id.' and file_name like "'.$video_url.'"') ;
             if (!$rec_id)
             { adding_rec_to_table('obj_site_goods_video',array('parent'=>$id,'clss'=>4,'obj_name'=>$rec_item['obj_name'],'file_name'=>$video_url)) ;
               echo 'Видео <strong>'.$video_url.'</strong> => добавлно<br>' ;
             } else echo 'Видео <strong>'.$video_url.'</strong> уже добавлено<br>' ;
          }

       // загужаем свойства
       //$source_cat_id=$arr_subcat_parents[(int)$product->subcategory] ;  // массив id подкатегории - id категории
       $source_cat_id=((int)$product->subcategory)? $arr_subcat_parents[(int)$product->subcategory]:(int)$product->category ;
       $source_cat_name=$arr_cat_names[$source_cat_id] ; // массив id категории - название категории
       $type_id=$type_id_by_name[$source_cat_name] ; // массив название категории - тип товара
       echo 'Тип товара:<strong>'.$source_cat_name.'</strong><br>' ;
       if (!$type_id)
       {  $rec_type=execSQL_van('select pkey,obj_name,url_name,input,list_id,ed_izm from '.$category_system->table_name.' where clss=60 and obj_name like "'.$source_cat_name.'" order by indx') ;
          if (!$rec_type['pkey'])
           { // добавляем новый тип товара
             $type_id=adding_rec_to_table($category_system->table_name,array('parent'=>1,'clss'=>60,'obj_name'=>$source_cat_name)) ;
             $type_id_by_name[$source_cat_name]=$type_id ;
           }
          else
          { // сохраеям в массиве, чтобы не искать в базе второй раз
            $type_id_by_name[$source_cat_name]=$rec_type['pkey'] ;
            $type_id=$rec_type['pkey'] ;
          }
       }
       //damp_array($type_id_by_name) ;
       execSQL_update('update obj_site_goods set type='.$type_id.' where pkey='.$id) ; // задаем тип товара
       if ($cur_cat_id) execSQL_update('update obj_site_goods set type='.$type_id.' where pkey='.$cur_cat_id) ; // задаем тип раздела, подразумевается в одном разделе все товары одного типа
       if ($cur_subcat_id) execSQL_update('update obj_site_goods set type='.$type_id.' where pkey='.$cur_subcat_id) ; // задаем тип раздела, подразумевается в одном разделе все товары одного типа
       //echo 'id типа товара:<strong>'.$type_id.'</strong><br>' ;

       $arr_props=array() ; $arr_props_name=array() ;

       // получаем все существующие свойства товара
       $arr_props_exist=execSQL('select id,pkey,value,num,str,type from obj_site_goods_props where parent='.$id.' and type='.$type_id) ;
       //print_2x_arr($arr_props_exist) ;

       foreach ($product->attributes->attribute as $attr)
       {  $attr_value=(string)$attr->value ;
          $attr_unit=(string)$attr->unit ;
          $attr_name=$arr_attr[(int)$attr->id] ;

          if (!$arr_attr_id_by_name[$type_id][$attr_name]) // если атрибут еще не встречался
          {  $rec_attr=execSQL_van('select pkey,obj_name,id,url_name,input,list_id,ed_izm from '.$category_system->table_name.' where clss=61 and parent='.$type_id.' and obj_name like "'.$attr_name.'" order by indx') ;
             if (!$rec_attr['pkey'])
                 { // добавляем новый тип товара
                   $last_id=execSQL_value('select max(id) from '.$category_system->table_name.' where clss=61 and parent='.$type_id) ;
                   $last_id++ ;
                   adding_rec_to_table($category_system->table_name,array('parent'=>$type_id,'clss'=>61,'id'=>$last_id,'obj_name'=>$attr_name,'unit'=>$attr_unit,'input'=>4)) ;
                   $arr_attr_id_by_name[$type_id][$attr_name]=$last_id ;
                   $arr_attr_type_by_name[$type_id][$attr_name]=4 ;
                 }
                else
                { $arr_attr_id_by_name[$type_id][$attr_name]=$rec_attr['id'] ; // сохраеям в массиве, чтобы не искать в базе второй раз
                  $arr_attr_type_by_name[$type_id][$attr_name]=$rec_attr['input'] ;
                }
          }

          $attr_id=$arr_attr_id_by_name[$type_id][$attr_name] ;
          $attr_type=$arr_attr_type_by_name[$type_id][$attr_name] ; //echo '$attr_type='.$attr_type.'<br>' ;
          $arr_props[$attr_id]=$attr_value ;
          $arr_props_name[$attr_name]=$attr_value ;
          // определяемся что будем делать со свойством - удалять, обновлять или добавлять
          switch($attr_type)
          {
              case 4: if (!isset($arr_props_exist[$attr_id])) adding_rec_to_table('obj_site_goods_props',array('parent'=>$id,'clss'=>6,'type'=>$type_id,'id'=>$attr_id,'str'=>$attr_value)) ;  // если свойства нет в списке существующих - добавляем
                      else  if ($arr_props_exist[$attr_id]['str']!=$attr_value) update_rec_in_table('obj_site_goods_props',array('str'=>$attr_value),'parent='.$id.' and id='.$attr_id) ; // если есть то сравнимваем значение новое со старым. Способ сравнения зависит от типа аттрибута
                      unset($arr_props_exist[$attr_id]) ; // удаляем из массива существующих - чтобы потом не удалился
                      break ;

          }

       }
       //print_2x_arr($arr_props_exist) ;
       // удаляем старые свойства, которых не было в списке новых свойств
       if (sizeof($arr_props_exist)) execSQL_update('delete from obj_site_goods_props where parent='.$id.' and id in ('.implode(',',array_keys($arr_props_exist)).')') ;

      //damp_array($arr_props_name,1,-1) ;

      // break ;

      //$i++ ; if ($i==30) break ;
      //echo '<br>' ;
   }

   $category_system->reload_list_types() ;

 }

  function import_goods_img($options=array())
  {
    ?><button class=v1 cmd="parsing_site/import_goods_img" script="start1.php">Загрузить фото товаров</button><?

    $XML_source=file_get_contents(_DIR_TO_ROOT.'/products2.xml') ;

    $market = new SimpleXMLElement($XML_source);
   $i=0 ;
   ?><h1>products</h1><?
   foreach ($market->products->product as $product)
   {  // загружаем описание товара
      $rec=execSQL_van('select pkey,obj_name from obj_site_goods where clss=200 and source_id="'.(int)$product->id.'" and obj_name="'.(string)$product->name.'" and temp=1') ;
      if ($rec['pkey'])
      { echo '<h2>Товар <strong>'.$product->name.'</strong>, id='.$product->id.'</h2>' ;
        $recs_img=execSQL_row('select pkey,REPLACE(file_name,CONCAT(pkey,"_"),"") as fname from obj_site_goods_image where parent='.$rec['pkey']) ;
        // загружаем фото
        foreach ($product->images->image as $image)
              {  $img_url=(string)$image->url ;
                 $arr=parse_url($img_url) ;
                 $img_name=safe_file_names(basename($arr['path'])) ;
                 if ($img_name)
                 { $rec_id=array_search($img_name,$recs_img) ; //execSQL_value('select pkey from obj_site_goods_image where parent='.$rec['pkey'].' and file_name like "%'.$img_name.'"') ;
                   if (!$rec_id)
                     { $res=obj_upload_image_by_href($rec['_reffer'],$img_url,array('view_upload'=>0,'debug'=>0,'finfo'=>array('obj_name'=>$rec['obj_name']))) ;
                       if ($res[1]) echo '<strong>Фото</strong> '.$img_url.' => '.hide_server_dir($res[1]).'<br>' ;
                       //damp_array($recs_img) ; damp_array($arr,1,-1) ; echo 'img_name='.$img_name.'<br>' ;
                     } else echo 'Фото <strong>'.$img_name.'</strong> уже загружено<br>' ;
                 }
              }
        execSQL_update('update obj_site_goods set temp=0 where pkey='.$rec['pkey']) ;
        $i++ ; if ($i==20) break ;
      }


   }

 }




?>