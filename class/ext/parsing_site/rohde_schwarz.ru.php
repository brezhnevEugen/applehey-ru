<?
include_once(_DIR_TO_ENGINE.'/admin/i_admin_system.php') ;
include_once('simple_html_dom.php') ;

class parse_site
{

    function parse_site()
    { $this->parse_domain='www.rohde-schwarz.ru' ;
      $this->parse_dir=_DIR_TO_ROOT.'/parse/www.rohde-schwarz.ru' ;
      $this->root_section_id=19083 ;

      set_time_limit(0) ;
      // парсим названия разделов из левой панели
      // новые разделы будут добавлены в указанные раздел каталога
      // $this->parsing_section_urls($this->root_section_id) ;


      // парсим одиночную страницу раздела
      // $this->parsing_section_page('http://www.rohde-schwarz.ru/products/test_and_measurement/spectrum_analysis/list/',19088) ;

      // парсим все страницы разделов
      /*
       //$list_sections=execSQL('select * from obj_site_goods where parent=19089 and clss=10') ;
       $list_sections=execSQL('select * from obj_site_goods where source_url!="" and clss=10') ;
       if (sizeof($list_sections)) foreach($list_sections as $rec)
       {  ?><h1><?echo $rec['obj_name']?></h1><?
          $this->parsing_section_page($rec['source_url'],$rec['pkey']) ;
       }
       return ;
      */

      // парсим одиночную страницу товара
      //$this->parsing_goods_page('http://www.rohde-schwarz.ru/products/test_and_measurement/Oscilloscopes/RTO/',19187) ;
      //return ;
      //execSQL_update('update obj_site_goods set temp=0') ;
      $list_goods=execSQL('select * from obj_site_goods where source_url!="" and not source_url is null and clss=200 and temp!=1 limit 20') ;
        if (sizeof($list_goods)) foreach($list_goods as $rec)
        {  ?><h1><?echo $rec['obj_name']?></h1><?
           echo $rec['source_url'].'<br>' ;
           echo 'id='.$rec['pkey'].'<br>' ;
           $this->parsing_goods_page($rec['source_url'],$rec['pkey']) ;
           execSQL_update('update obj_site_goods set temp=1 where pkey='.$rec['pkey']) ;
        }

    }


    // парсим названия разделов из меню каталога в левой панели
    // пака считается, что вручную добавлены четыре основных раздела
    function parsing_section_urls($root_id)
    {   $list_sections=execSQL('select * from obj_site_goods where parent='.$root_id) ;
        if (sizeof($list_sections)) foreach($list_sections as $rec)
        {   $html=$this->get_file_http($rec['source_url']) ;
            $a_sections=$html->find('div[id=left_col] ul li ul li a') ;
            if (sizeof($a_sections)) foreach($a_sections as $a) if ($a->href)
            {  adding_rec_to_table('obj_site_goods',array('parent'=>$rec['pkey'],'clss'=>10,'obj_name'=>$a->innertext,'source_url'=>$a->href)) ;
               $pages_URLS[$a->innertext]=$a->href ;
            }
            damp_array($pages_URLS,1,-1) ;
        }
    }


    // парсим страницу раздела - выделяем товары
    function parsing_section_page($url,$section_id=0)
    { // получем адрес вкладки "Продукция"
      $html=$this->get_file_http($url) ;
      if (!is_object($html)) return ;
      $content=$html->find('div[id=content_div]',0)->outertext ;
      // вытаскиваем краткое описание раздела
      preg_match_all("/<div style='margin:20px 0;'>(.+?)<\/div>/is",$content,$matches,PREG_SET_ORDER) ;
      if ($matches[0][1]) $this->parsing_save_text_content($section_id,$matches[0][1],'intro') ;

      foreach ($html->find('div[id=multipage] ul li a') as $a) if ($a->href) $pages_URLS[$a->innertext]=$a->href ;
      ?><h2>Вкладки меню</h2><?
      damp_array($pages_URLS,1,-1) ;
      $url_section=$pages_URLS['Продукция'] ;
      if (!$url_section) { echo '<div class="red">Не найдена панель "Продукция"</div>' ; return ; }
      $html=$this->get_file_http($url_section) ;
      // проверяем, есть ли подсерии
      $subserion_URLS=array() ;  $models_URLS=array() ; $models_old_URLS=array() ;
      $a_subseria=$html->find('ul[id=panels_nav] li a') ;
      if (sizeof($a_subseria))
      { echo '<strong>Обнаружены подразделы</strong><br>' ;
        foreach($a_subseria as $a_sub) if ($a_sub->href)
          {
              list($temp,$subsetion_dir)=explode('#',$a_sub->href) ;
              $subserion_URLS[$a_sub->innertext]=$subsetion_dir ;
              $sub_id=execSQL_value('select pkey from obj_site_goods where parent='.$section_id.' and clss=10 and obj_name="'.$a_sub->innertext.'"') ;
              if (!$sub_id) $sub_id=adding_rec_to_table('obj_site_goods',array('parent'=>$section_id,'clss'=>10,'obj_name'=>$a_sub->innertext,'source_url'=>'')) ;

              $a_models=$html->find('div[id="'.$subsetion_dir.'_panel"] div.catalog_list p a') ;
              if (sizeof($a_models)) foreach($a_models as $a_mod) if ($a_mod->href)
              { $goods_id=execSQL_value('select pkey from obj_site_goods where parent='.$sub_id.' and clss=200 and obj_name="'.$a_mod->innertext.'"') ;
                if (!$goods_id) $goods_id=adding_rec_to_table('obj_site_goods',array('parent'=>$sub_id,'clss'=>200,'obj_name'=>$a_mod->innertext,'source_url'=>$a_mod->href)) ;
                $models_URLS[$a_sub->innertext][$a_mod->innertext]=$a_mod->href ;
              }
          }
      }
      else
      {   echo '<strong>Подразделов не обнаружено</strong><br>' ;
          $a_models=$html->find('div.catalog_list p a') ;
          if (sizeof($a_models)) foreach($a_models as $a) if ($a->href)
          {  $goods_id=execSQL_value('select pkey from obj_site_goods where parent='.$section_id.' and clss=200 and obj_name="'.$a->innertext.'"') ;
             if (!$goods_id) $goods_id=adding_rec_to_table('obj_site_goods',array('parent'=>$section_id,'clss'=>200,'obj_name'=>$a->innertext,'source_url'=>$a->href)) ;
             $models_URLS[$a->innertext]=$a->href ;
          }
      }

      if (sizeof($subserion_URLS)) { ?><h2>Подсерии</h2><? damp_array($subserion_URLS,1,-1) ;}
      if (sizeof($models_URLS)) { ?><h2>Модели</h2><? damp_array($models_URLS,1,-1) ;}

      $a_old_models=$html->find('ol[id=prod_old] li a') ;
      if (sizeof($a_old_models)) foreach($a_old_models as $a) if ($a->href)
        { $goods_id=execSQL_value('select pkey from obj_site_goods where parent='.$section_id.' and clss=200 and obj_name="'.$a->innertext.'"') ;
          if (!$goods_id) $goods_id=adding_rec_to_table('obj_site_goods',array('parent'=>$section_id,'clss'=>200,'obj_name'=>$a->innertext,'source_url'=>$a->href,'old'=>1)) ;
          $models_old_URLS[$a->innertext]=$a->href ;

        }

      if (sizeof($models_old_URLS)) { ?><h2>Снято с производства</h2><? damp_array($models_old_URLS,1,-1) ;}
    }

    function parsing_goods_page($main_url,$goods_id)
    {  set_time_limit(0) ;  $content=array() ;
       $html=$this->get_file_http($main_url) ;
       if (!is_object($html)) return ;

       $intro=$html->find('div[id=content_div] p b',0)->innertext ;
       if ($intro) { execSQL_update('update obj_site_goods set intro="'.addslashes($intro).'" where pkey='.$goods_id) ;
                     echo '<strong>Краткое описание</strong>:'.$intro.'<br>' ;
                   }

       foreach ($html->find('div[id=multipage] ul li a') as $a) if ($a->href) $pages_URLS[$a->innertext]=$a->href ; //damp_array($pages_URLS,1,-1) ;

       // добавляем изображения с главной страницы в изображения товара
       $main_images=$html->find('div[id=content_div] a[rel=imagebox]') ;
       if (sizeof($main_images)) foreach($main_images as $indx=>$a_img)
        { $html->find('div[id=content_div] a[rel=imagebox]',$indx)->outertext='' ;
          $this->parsing_save_img($goods_id,$a_img->href,basename($a_img->href)) ;
        }

       foreach($pages_URLS as $tab_name=>$page_URL)
       { if ($page_URL!=$main_url) $html=$this->get_file_http($page_URL) ;
         if (is_object($html)) switch($tab_name)
         { case 'Файлы':        $files=$html->find('table.table_files a') ;
                                if (sizeof($files)) foreach ($files as $a) if ($a->href) $this->parsing_save_file($goods_id,$a->href,$a->innertext) ;
                                else $content['errors'][]='Не удалось найти таблицу файлов по table.table_files' ;
                                break ;
           case 'Изображения':  $imgs=$html->find('div[id=imgList] img') ;
                                $headers=$html->find('div[id=imgList] h3') ;
                                if (sizeof($headers)) foreach ($headers as $indx=>$h3) $this->parsing_save_img($goods_id,$imgs[$indx]->src,$h3->innertext) ;
                                else $content['errors'][]='Не удалось найти заголовки изображений по h3' ;
                                break ;
           default:             $cont=$this->clean_goods_page($html,$tab_name) ;
                                if ($tab_name=='Ключевые факты') $tab_name='Описание' ;
                                $this->parsing_save_text_content($goods_id,$cont,$tab_name) ;
         }
       }

       damp_array($content,1,-1) ;

    }

    function clean_goods_page(&$html,$tab_name)
       {
           $html->find('div[id=content_div] div[id=breadcrumbs]',0)->outertext='' ;
           $html->find('div[id=content_div] div[id=multipage]',0)->outertext='' ;
           $html->find('div[id=content_div] div[id=copyright]',0)->outertext='' ;
           $html->find('div[id=content_div] h1',0)->outertext='' ;

           $content=$html->find('div[id=content_div]',0)->innertext ;
           // удаляем мусорный контент
           $math="<b style='color\:\#00A0DD'>(.+?)<\/b>" ;
           $content=preg_replace('/'.$math.'/is','',$content) ;
           $math="<strong>$tab_name<\/strong>" ;
           $content=preg_replace('/'.$math.'/is','',$content) ;
           $math="(<p>)(\W+?)(<br>){2,}" ;
           $content=preg_replace('/'.$math.'/is','<p>',$content) ;
           return($content) ;
       }



    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // проверяем на изображения и ссылки текстовое поле
    // обнаруженные изображение скачивается из копии сайта в  /public/site_goods/
    // найденые линки проверяется по url. Если url совпадает с source_url одного из объектов - url заменяется на url этого объекта
    function parsing_save_text_content($obj_id,$section_intro,$fname)
    {  global $goods_system ;
       $arr_replace_from=array() ;  $arr_replace_to=array() ;
       $html_intro=str_get_html($section_intro) ;
       /*?><h2>Обработка текста "<?echo $fname?>"</h2><?*/
       // проверяем налилие фото в тексте
       $imgs=$html_intro->find('img') ;
       if (sizeof($imgs))
       { foreach($imgs as $img)
         {  // копируем изображения из временной папки в папку изображение редактора
            $source_url=$img->src ;
            $desct_url='/public/site_goods/'.$obj_id.'_'.basename($source_url) ;
            $source_dir=$this->parse_dir.$source_url ; // '/openwysiwyg/uploads/hmo_titil_2.jpg' ;
            $desct_dir=_DIR_TO_ROOT.$desct_url ; // '/openwysiwyg/uploads/hmo_titil_2.jpg' ;
            if (!file_exists($source_dir)) $source_dir=$this->absolute_url($source_url) ;
            if (!file_exists($desct_dir)) copy($source_dir,$desct_dir) ;

            if (file_exists($desct_dir))
            { $arr_replace_from[]=$source_url ;
              $arr_replace_to[]=$desct_url ;
              echo '<strong>Изображение:</strong> '.$source_url.' => '.$desct_url.'<br>' ;
            }
         }
       }

       $as=$html_intro->find('a') ;
       if (sizeof($as))
       { foreach($as as $a) if ($a->href)
         {  $source_url=$a->href ;
            echo '<strong>Линк:</strong> '.$source_url ;
            $arr=parse_url($source_url) ;
            if (!$arr['host'] or $arr['host']=='www.rohde-schwarz.ru')
            {   $source_dir=$this->convert_url($source_url) ; // echo '$source_dir='.$source_dir.'<br>' ;
                $arr=parse_url_path($source_url) ; $file_name=$arr['_CUR_PAGE_NAME'] ;
                $desct_url='' ;
                //echo '$file_name='.$file_name.'<br>' ; damp_array($arr) ;

                $rec_id=execSQL_value('select pkey from obj_site_goods where source_url="'.$source_url.'"') ;
                // проверяем ссылку на каталог
                if ($rec_id) $desct_url='http://'.$goods_system->get_url_by_id($rec_id)  ;
                else
                { $arr=explode('/',$source_url) ;
                  unset($arr[sizeof($arr)-2]) ;
                  $source_url_correct=implode('/',$arr) ;
                  echo ' - не найден<br>' ;
                  echo '<STRONG>Ищем откорректированную ссылку:</strong>'.$source_url_correct.' ' ;
                  $rec_id=execSQL_value('select pkey from obj_site_goods where source_url="'.$source_url_correct.'"') ;
                  if ($rec_id) $desct_url='http://'.$goods_system->get_url_by_id($rec_id)  ;
                  else         echo ' - не найден<br>' ;
                }

                if (!$rec_id and $file_name) // проверяем ссылку на файл на сервере
                { echo '<STRONG>Ищем файл на сервере:</strong>'.$file_name.' ' ;
                  $desct_url='/public/site_goods/'.$obj_id.'_'.$file_name ;
                  $desct_dir=_DIR_TO_ROOT.$desct_url ;
                  if (!file_exists($source_dir)) echo ' => <span class="red">файл не найден</span><br>' ;
                  else if (!file_exists($desct_dir)) copy($source_dir,$desct_dir) ;
                  if (!file_exists($desct_dir)) $desct_url='' ;
                }



                if ($desct_url)
                { $arr_replace_from[]=$source_url ;
                  $arr_replace_to[]=$desct_url ;
                  echo ' => '.$desct_url.'<br>' ;
                }
                else
                { //$a->outertext=$a->innertext ; // удаляем ссылку
                  //echo '<strong>Линк:</strong> удален<br>' ;
                  echo ' - <STRONG>не найдена</STRONG><br>' ;
                }
            } else echo ' - <STRONG>пропущен</STRONG><br>' ;
         }
       }
       //ob_start() ; dump_html_tree($html_intro) ; $section_intro2=ob_get_clean() ; echo $section_intro2 ;
       //$section_intro2=$html_intro->save() ;
       // заменяем автозамены в тексте
       if (sizeof($arr_replace_from)) $section_intro=str_replace($arr_replace_from,$arr_replace_to,$section_intro) ;

       /*?><h2>Краткое описание раздела</h2><?*/
       //echo $section_intro ;

       $rec_id=execSQL_value('select pkey from obj_site_goods_texts where parent='.$obj_id.' and obj_name="'.$fname.'"') ;
       if ($rec_id) update_rec_in_table('obj_site_goods_texts',array('value'=>$section_intro),'pkey='.$rec_id) ;
       else         adding_rec_to_table('obj_site_goods_texts',array('parent'=>$obj_id,'clss'=>41,'obj_name'=>$fname,'value'=>$section_intro)) ;

       echo '<div class=green>Сохранен текст "<strong>'.$fname.'"</strong></div>' ;
    }

    // сохраняем внешнее изображение в объекте
    function parsing_save_img($goods_id,$imgs_src,$name)
    { $obj_reffer=$goods_id.'.'._DOT('obj_site_goods')->pkey ;
      if (!$name) $name=basename($imgs_src) ;
      $file_src=$this->convert_url($imgs_src) ; // сначала проеряем существование скачанного локально файла
      if (!file_exists($file_src)) $file_src=$this->absolute_url($imgs_src) ; // иначе преобразовывем url в абсолютную ссылку
      $file_id=execSQL_value('select pkey from obj_site_goods_image where parent='.$goods_id.' and clss=3 and file_name like "%_'.basename($imgs_src).'"') ;
      if (!$file_id)
      {  $res=obj_upload_image_by_href($obj_reffer,$file_src,array('view_upload'=>0,'debug'=>0,'finfo'=>array('obj_name'=>$name))) ;
         if ($res[1]) echo '<strong>Фото</strong> '.$imgs_src.' => '.hide_server_dir($res[1]).'<br>' ;
         else         echo '<strong>Фото</strong> '.$imgs_src.' - <span class=red>ошибка загрузки</span><br>' ;
      }
      else echo '<strong>Фото</strong> '.$imgs_src.' - <span class=green>уже сохранен</span><br>' ;
    }

    // сохраняем внешний файл в объекте
    function parsing_save_file($goods_id,$file_src,$name)
    { $obj_reffer=$goods_id.'.'._DOT('obj_site_goods')->pkey ;
      if (!$name) $name=basename($file_src) ;
      $file=$this->convert_url($file_src) ; // сначала проеряем существование скачанного локально файла
      if (!file_exists($file)) $file=$this->absolute_url($file_src) ; // иначе преобразовывем url в абсолютную ссылку
      $file_id=execSQL_value('select pkey from obj_site_goods_files where parent='.$goods_id.' and clss=5 and file_name like "%_'.basename($file).'"') ;
      if (!$file_id)
        {  $res=obj_upload_file_by_href($obj_reffer,$file,array('view_upload'=>0,'finfo'=>array('obj_name'=>$name))) ;
           if ($res[1]) echo '<strong>Файл</strong> '.$file.' => '.hide_server_dir($res[1]).'<br>' ;
           else         echo '<strong>Файл</strong> '.$file.' - <span class=red>ошибка загрузки</span><br>' ;
        }
        else echo '<strong>Файл</strong> '.$file.' - <span class=green>уже сохранен</span><br>' ;
    }

    function convert_url($url,$suff='')
    { $arr=parse_url($url) ;// damp_array($arr) ;
      $arr2=parse_url_path($url) ; $file_name=$arr2['_CUR_PAGE_NAME'] ;
      $suff=(!$file_name)? 'index.html':'' ;
      $res=(!$arr['host'] or $arr['host']=='www.rohde-schwarz.ru')? $this->parse_dir.$arr['path'].$suff:''  ;
      return($res) ;
    }

    // восстанавлием из url типа /data/catalog_pics/394/hms_big1.jpg => http://www.rohde-schwarz.ru//data/catalog_pics/394/hms_big1.jpg
    function absolute_url($url)
    { $arr=parse_url($url) ;
      if (!$arr['scheme'])  $arr['scheme']='http' ;
      if (!$arr['host'])    $arr['host']=$this->parse_domain ;
      $res=$arr['scheme'].'://'.$arr['host'].$arr['path'] ;
      return($res) ;
    }

    function get_file_http($url)
    { $url=$this->convert_url($url) ;
      if ($url) $content=iconv('windows-1251','utf-8',file_get_contents($url)) ;
      else      $content='' ;
      if ($content) $html=str_get_html($content) ;
      //else          $html=str_get_html('<div></div>') ;
      else          $html='' ;
      return($html) ;
    }

}

?>