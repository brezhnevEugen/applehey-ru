<?
include('messages.php') ;
include(_DIR_TO_ENGINE.'/c_page_XML_AJAX.php') ;
class c_page_ajax extends c_page_XML_AJAX
{

 function get_backcall_panel()
 { $this->add_element('success','modal_window') ;
   $this->add_element('title','Запрос обратного звонка') ;
   $this->add_element('no_close_button',1) ;
   //$this->add_element('modal_panel_width','350px') ;
   include_once('panel_phone_callback.php') ;
   panel_phone_callback() ;
 }

 // принимающая функция, вызывается из ajax.php:
 // просто отправляем сообщение о звонке на email
 function request_phone_to_mail()
 {  global $mail_system;
    save_form_values_to_session($_POST) ;
    $params['site_name']=_MAIN_DOMAIN ;
    $params['name']=$_POST['name'] ;
    $params['phone']=$_POST['phone'] ;
    $evt_id=_event_reg('Запрос обратного звонка',$_POST['name'].', '.$_POST['phone']) ;
    $mail_system->send_mail_to_pattern($_SESSION['LS_request_phone_sending_email_alert'],'request_phone_sending',$params,array('use_event_id'=>$evt_id)) ;
    echo '<p>Ваше заявка на обратный звонок получена.</p><p>В ближайшее время менеджер перезвонит Вам.</p><div class="button"><button class="cancel">Закрыть</button></div>' ;
    $this->add_element('close_mBox_window','panel_phone_callback') ; // закрываем ранее открытое модальное окно
    $this->add_element('title','Запрос обратного звонка') ;
    $this->add_element('success','modal_window') ;
    $this->add_element('JSCODE',"<script type='text/javascript'>$j(document).ready(function(){yaCounter28778656.reachGoal('CALLBACK');});</script>") ;
 }

 // - регистрирем заявку на обратный звонок в системе запросов
 // для вызова обработчика задать в форме cmd=request_phone_register
 function request_phone_register()
  { global $request_system ;
    save_form_values_to_session($_POST) ;
    $data['theme']='Запрос обратного звонка' ;
    $data['code']='request_phone_backcall';
    $data['autor_name']=$_POST['name'] ;
    $data['autor_phone']=$_POST['phone'] ;
    $result=$request_system->create_message($data,array('mail_template'=>'request_phone_sending')) ;
    show_message_by_id('request_message',$result) ;
    if ($result['type']=='success')
    { $this->add_element('close_mBox_window','panel_phone_callback3') ; // закрываем ранее открытое модальное окно
      // тут должна быть отправка sms-уведомления менеджеру

    }
    $this->add_element('title','Запрос обратного звонка') ;
    $this->add_element('success','modal_window') ;
  }
}
