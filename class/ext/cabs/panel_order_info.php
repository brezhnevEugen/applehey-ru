<?
function panel_order_info($order)
{ global  $orders_system ;
  //damp_array($order) ;
  ?><div id=panel_order_info>
    <h2>Заказ <?echo $order['obj_name']?></h2>
    <table id=order_info><colgroup><col id='c0'><col id='c1'></colgroup>
      <tr><td class=title>Сумма заказа</td><td><?echo _format_price($order['end_price'])?></td></tr>
      <tr><td class=title>Состояние заказа<br></td><td><?  echo $_SESSION['list_status_orders'][$order['status']] ;?></td></tr>
      <tr><td class=title>Фамилия Имя</td><td><?echo $order['name']?></td></tr>
      <tr><td class=title>email</td><td><?echo $order['email']?></td></tr>
      <tr><td class=title>Телефон</td><td><?echo $order['phone']?></td></tr>
      <tr><td class=title>Адрес</td><td><?echo $order['adres']?></td></tr>
      <tr><td class=title>Комментарий</td><td><?echo $order['comment']?></td></tr>
    </table>
   <?
   //damp_array($order) ;
   if (sizeof($order['goods']))
   {?><table id=order_goods><colgroup><col id='c1'><col id='c2'><col id='c3'><col id='c4'></colgroup>
       <tr class='header'>
                   <td></td>
      			   <td colspan="2">Товар/Цена</td>
      			   <td>Количество</td>
      			   <td>Всего</td>
      			  </tr>
       <?
      //damp_array($order['goods']) ;
      foreach ($order['goods'] as $rec)
       { //$price_info=$rec['__reffer_from']['price_info'] ;
         $cart_info=$orders_system->prepare_price_info($rec['__reffer_from'],array('show_sales_to_van_goods'=>1)) ; // готовим поля для отображения корзины
         ?><tr><td class=img rowspan=2><img src="<?echo img_clone($rec,'small')?>" alt="<?echo $rec['__img_alt']?>" class="goods" border=0></td>
			   <td class=name colspan=4><a href="<?echo $rec['__href']?>" target=_blank><?echo $rec['obj_name']?></a>
                  <?  if (isset($rec['_sortament_rec']))
                    { ?><div class="sortament_name"><?
                      if ($rec['_sortament_rec']['color_name']) echo 'Цвет: <strong>'.$rec['_sortament_rec']['color_name'].'</strong> ' ;
                      if ($rec['_sortament_rec']['size']) echo 'Размер: <strong>'.$rec['_sortament_rec']['size'].'</strong>' ;
                      ?></div><?
                    }               
                  ?>
               </td>
           </tr>
           <tr>
               <td class=price><?echo $cart_info['price'];?></td>
               <td class=sales><?if ($cart_info['sales_title']) echo $cart_info['sales_title'].'<br><span class=value>'.$cart_info['sales_text']?></span></td>
			   <td class=count><?echo $cart_info['cnt']?> шт</td>
			   <td class=price_res><?echo $cart_info['sum_price']?></td>
		   </tr>
	    <?
	}
     ?></table><br><?
   }
   else echo '<div class="alert">Заказ пуст</div>' ;
   ?></div><?
   //damp_array($rec) ;
}

?>