<?php
include("c_cabs.php");
class c_page_cabs_contact extends c_page_cabs
{
 public $h1='Личный кабинет - данные для заказа' ;

 function block_main()
 { $this->page_title() ;
   $this->panel_account_org() ;
 }

 function panel_account_org()
 {  global $member ;
    ?><div id=panel_account_org><form method="POST" id=contact_setting action=""><input type="hidden" name=cmd value="change_contact">
         <table class="contact_form">
           <?if ($member->info['clss']==88){?>
           <tr><td class=title>Название организации:</td><td><input type=text name=account[name] data-required value="<?echo htmlspecialchars($member->info['obj_name'])?>"></td></tr>
           <tr><td class=title>Контактное лицо:</td><td><input type=text name=account[contact_name] data-required value="<?echo htmlspecialchars($member->info['contact_name'])?>"></td></tr>
           <tr><td class=title>Должность:</td><td><input type=text name=info[status] value="<?echo htmlspecialchars($member->ext_info['status'])?>"></td></tr>
           <tr><td class=title>Сайт:</td><td><input type=text name=info[site] value="<?echo htmlspecialchars($member->ext_info['site'])?>"></td></tr>
           <!--<tr><td class=title>Email:</td><td><input type=text name=account[email] data-required data-validate="email"  is_checked="check_email" ext=account alert_text="Этот email уже используется"  value="<?echo htmlspecialchars($member->info['email'])?>"></td></tr>-->
           <tr><td class=title>Телефон:</td><td><input type=text name=account[phone] data-required value="<?echo htmlspecialchars($member->info['phone'])?>"></td></tr>
           <tr><td class=title>Реквизиты:</td><td><textarea name=account[rekvezit] data-required><?echo htmlspecialchars($member->info['rekvezit'])?></textarea></td></tr>
           <?}else{?>
             <tr><td class=title>Ваше имя:</td><td><input type=text name=account[name] data-required value="<?echo htmlspecialchars($member->name)?>"></td></tr>
             <tr><td class=title>Email:</td><td><input type=text name=account[email] data-required data-validate="email" value="<?echo htmlspecialchars($member->info['email'])?>"></td></tr>
             <tr><td class=title>Телефон:</td><td><input type=text name=info[phone]  value="<?echo htmlspecialchars($member->info['phone'])?>"></td></tr>
            <?}?>
            <tr><td class=title>Индекс:</td><td><input type=text name=info[zip] value="<?echo htmlspecialchars($member->ext_info['zip'])?>"></td></tr>
            <tr><td class=title>Страна:</td><td><input type=text name=info[country] value="<?echo htmlspecialchars($member->ext_info['country'])?>"></td></tr>
            <tr><td class=title>Регион:</td><td><input type=text name=info[region] value="<?echo htmlspecialchars($member->ext_info['region'])?>"></td></tr>
            <tr><td class=title>Город:</td><td><input type=text name=account[city] value="<?echo htmlspecialchars($member->info['city'])?>"></td></tr>
            <tr><td class=title>Адрес доставки:</td><td><textarea name=account[adres]><?echo htmlspecialchars($member->info['adres'])?></textarea></td></tr>
            <tr><td colspan=2 class=buttons><input type=submit class=button_green value="Сохранить изменения"></td></tr>
         </table>
      </form>
      </div>
      <script type="text/javascript">
         $j('div#panel_account_org').ajax_check({content_class:'alert',position:{x: ['right', 'outside'],y:'center'}}) ;
         new mForm.Submit({form: 'contact_setting',ajax: false});
      </script>
    <?
    //var_dump($member->ext_info) ;
    //damp_array($member) ;
 }


  function CMD_change_contact($arr_params=array())
  { global $member,$account_system ;
    $arr_result=array() ;
    // изменяем основную информацию аккаунта
    damp_array($_POST['account']) ;
    if (sizeof($_POST['account'])) $arr_result=$account_system->change_member_arr_info($member,$_POST['account']) ;
    if (sizeof($arr_result)) foreach($arr_result as $result) $account_system->show_message($result) ;
      //damp_array($arr_result) ;
    // изменяем дополнительную информацию аккаунта
      if (sizeof($_POST['info'])) $arr_result=$account_system->change_member_ext_arr_info($member,$_POST['info']) ;
    if (sizeof($arr_result))  foreach($arr_result as $result) $account_system->show_message($result) ;
    //damp_array($arr_result) ;

    $member->update_info() ;
  }


}



?>