<?php
include("c_cabs.php");
class c_page_cabs_orders extends c_page_cabs
{
 public $h1='Личный кабинет - Ваши заказы' ;

 function block_main()
 { $this->page_title() ;

   if ($_GET['order_id']) $this->panel_order() ;
   else                   $this->panel_list_orders() ;
 }

 // показывем список заказов
 function panel_list_orders()
 {  global $orders_system,$member ;
    $options=array('id'=>'list_orders','debug'=>0) ;
    //$options['subselect'][]='pkey as _pkey' ;
    //$options['subselect'][]='(select sum(cnt) from '.$_SESSION['TM_orders_goods'].' where parent=_pkey and reffer RLIKE "[0-9].[0-9]") as _sum_cnt' ;
    //$options['subselect'][]='(select sum(cnt*price) from '.$_SESSION['TM_orders_goods'].' where parent=_pkey and reffer RLIKE "[0-9].[0-9]") as _sum_price' ;
    include_once('list_orders_full_info.php') ;
    $orders_system->show_list_items('account_reffer="'.$member->reffer.'"','list_orders_full_info',$options) ;
 }

 // показываем выбранный заказ
 function panel_order()
 { global $member,$orders_system ;
   if ($_GET['order_id'])
   { $order=$orders_system->get_order($_GET['order_id'],array('account_reffer'=>$member->reffer)) ;
     if ($order['pkey'])
     { include_once('panel_order_info.php') ;
       panel_order_info($order) ;
     }
     else $orders_system->show_message($order) ;
   }

 }

}
?>