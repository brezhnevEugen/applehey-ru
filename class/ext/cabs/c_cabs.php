<?php
include(_DIR_TO_CLASS."/c_page.php");
include(_DIR_EXT."/account/messages.php");
class c_page_cabs extends c_page
{
 public $no_sitemap=1 ;
 public $h1='Личный кабинет' ;

 function on_send_header_before() { if (!$_SESSION['member']->id) redirect_301_to('/account/') ; }
 function command_execute() {}


 function set_head_tags()
 { parent::set_head_tags() ;
   $this->HEAD['css'][]=_PATH_TO_EXT.'/cabs/style.css' ;
 }

// function panel_search() {}

 // базовый заголовк страницы - вверху путь, снизу заголовок - для шаблонов
 function page_title($title='',$path=array(),$options=array())
  { if (is_array($title)) { $options=$title ; $title='' ; $path=array() ; } // на случай, если page_title вызвана с путем в первом параметре
      $this->block_tabs() ;
      echo $this->result_exec_cmd ; // выводим текст от выполнения команды
      $this->panel_title($title,$options);  // заголовк страницы по умолчанию

    //$this->panel_path($path,$options);  // заголовк страницы по умолчанию
  }

 function select_obj_info()
 {  global $member ;
    if ($member->info['parent']!=6809)
    { $this->tabs['/cabs/']='Заказы' ;
      $this->tabs['/cabs/support/']='Запросы в службу поддержки' ;
      $this->tabs['/cabs/account/']='Аккаунт' ;
      $this->tabs['/cabs/contact/']='Данные для заказа' ;
    }
    else
    { $this->tabs['/cabs/']='Задания' ;

    }
 }

 //function panel_main_menu() {}

 function block_tabs()
 { ?><div id=block_tabs><?
   $cur_page=str_replace($this->tabs_dir,'',_CUR_PAGE_DIR) ;
   //echo '_CUR_PAGE_DIR='._CUR_PAGE_DIR.'<br>' ; echo 'tabs_dir='.$this->tabs_dir.'<br>' ; echo '$cur_page='.$cur_page.'<br>' ;
   ?><div id="cab_tabs">
       <? if (sizeof($this->tabs)) foreach($this->tabs as $page=>$name) {?><div class="item <?if ($cur_page==$page) echo 'active'?>"><a href="<?echo $this->tabs_dir.$page?>"><?echo $name?></a></div><?}?>
     <div class=clear></div></div>
   </div><?
 }

 function exec_cmd()
 { $cmd='CMD_'.$_POST['cmd'] ;
   if (method_exists($this,$cmd)) $this->$cmd($_POST) ;
 }

 function block_main()
 { global $member ;
   $this->page_title() ;
   if ($member->info['parent']==6809) $this->panel_cabs_index_to_courir() ;
   else                               $this->panel_cabs_index_to_member() ;
 }

 // главная страница личного кабинета для обычного клиента
 function panel_cabs_index_to_member()
 {
   $this->panel_orders_stats() ;
   $this->panel_list_orders() ;
 }

 // главная страница личного кабинета для курьера
 function panel_cabs_index_to_courir()
 {

 }


 function panel_orders_stats()
 { global $orders_system,$member ;
   $orders_stats=$orders_system->get_orders_stats(array('account_reffer'=>$member->reffer)) ;
   ?><div id=panel_orders_stats>
      Общая сумма доставленных заказов: <strong><?echo _format_price($orders_stats[4])?></strong><br>
      Ваша скидка: <strong><?echo ($member->info['sales_value'])? $member->info['sales_value']:0 ?>%</strong>
    </div><?

 }

 function panel_list_orders()
 {  global $orders_system,$member ;
    $options=array('id'=>'list_orders','debug'=>0) ;
    $options['subselect'][]='pkey as _pkey' ;
    $options['subselect'][]='(select sum(cnt) from '.$_SESSION['TM_orders_goods'].' where parent=_pkey) as _sum_cnt' ;
    $options['subselect'][]='(select sum(cnt*price) from '.$_SESSION['TM_orders_goods'].' where parent=_pkey) as _sum_price' ;
    include_once('list_orders_full_info.php') ;
    $orders_system->show_list_items('account_reffer="'.$member->reffer.'"','list_orders_full_info',$options) ;
 }

}



?>