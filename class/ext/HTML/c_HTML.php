<?php
include_once (_DIR_TO_ENGINE.'/c_core_HTML.php') ;
class c_HTML extends c_core_HTML
{ public $allow_GET_params=array() ;  // $allow_GET_params=array('dd'=>'int') ;
  public $checked_GET_params=0 ;      // разрешение проверки корректности параметров
  public $false_GET_params_action=404 ; // действие в случае некоректного параметра или некоректного url
  public $only_dir_in_url=0 ; // эта страница должны открываться только по url /catalog/sales/, /catalog/sales/)%7Bthis.close()%7D%7D%5D выдаст 404 ошибку или 301 редирект
  public $prev_HTML; //  код в модулях, выводимый в поток при подклбчении подулей (код вне функций). Выводится в зависимости от типа текущей страницы (HTML, XML)
  public $noindex=0; //  запрет индексации страницы роботами
  public $HEAD=array() ;
  //public $DOCTYPE='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">' ;
  public $DOCTYPE='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">' ;
  public $HTML_TAG=array('<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" class=" desktop landscape">','</html>') ;
  public $cur_page_head;

  function c_HTML()
  {                                                     
      ob_start() ;
      $this->set_head_tags() ;
      $this->cur_page_head=ob_get_clean() ;
  }

  // отдаем HTML контент
  // пока оставляем как есть, но в следующей версии необходимо убрать обработку опций  content_type,no_html_doc_type,no_html_head
  // теперь вывод XML страниц только через свой скрипт c_page_XML
  function HTML(&$options=array())
  {


    echo $this->DOCTYPE ; // <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
    echo $this->HTML_TAG[0] ; // <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

    // сначала выполняем body, так как в шаблонах может быть подключение стилей шаблонов
    ob_start() ;
    $method_body=($options['use_body'])? ($options['use_body']):'body' ; // необходмый шаблон макета страницы может быть передан через $options. Используется для страниц, не имеющий собственного скрипта в /class/
    $this->$method_body($options) ;
    $body_text=ob_get_clean() ;

    $this->site_top_head($options) ;  // заголовок страницы

    echo $body_text ;

    echo $this->HTML_TAG[1] ; // </html>
  }

  function set_head_tags()
  { // преобразовываем теги из старого формата в новый формат
    if (isset($_SESSION['favicon']) and !isset($this->HEAD['favicon']))     $this->HEAD['favicon']=$_SESSION['favicon'] ;
    if (sizeof($_SESSION['meta_info']) and !sizeof($this->HEAD['meta']))    $this->HEAD['meta']=$_SESSION['meta_info'] ;
    if (sizeof($_SESSION['arr_file_js']) and !sizeof($this->HEAD['js']))    $this->HEAD['js']=$_SESSION['arr_file_js'] ;
    if (sizeof($_SESSION['arr_file_css']) and !sizeof($this->HEAD['css']))  $this->HEAD['css']=$_SESSION['arr_file_css'] ;
  }

  function site_top_head($options=array())
  { ?><head>
       <title><? echo trim($this->title);?></title>
        <META name="keywords" content="<?echo trim($this->keywords);?>" />
        <META name="description" content="<?echo trim($this->description)?>" />
        <META name="news_keywords" content="<?echo trim($this->keywords);?>" />
        <META http-equiv="Content-Type" content="text/html; charset=utf-8">
        <META NAME="Document-state" CONTENT="Dynamic">
     <? $no_index=$this->check_page_robots_index() ;
        if ($this->noindex) $no_index=1 ;
        if ($options['no_robots_index'] or $GLOBALS['no_access_to_robots'] or $_SESSION['member']->id or isset($_GET['size']) or isset($_GET['sort'])) $no_index=1 ;  // прямой запрет индексации страницы или запрет индексации после авторизации клиента
        if (!$no_index) { echo "<META NAME=\"revizit-after\" CONTENT=\"5 days\"> \n" ;
                          echo "<META NAME=\"revisit\"  CONTENT=\"5 days\"> \n" ;
                        }
                        else echo "<meta name=\"robots\" content=\"noindex, nofollow, noarchive\">\n" ;

        if (sizeof($this->HEAD['meta'])) foreach($this->HEAD['meta'] as $info) { ?><META name="<?echo $info['name']?>"  content="<?echo $info['content']?>"><? echo "\n\t" ;}
        if ($this->HEAD['favicon']){?><link href="<?echo $this->HEAD['favicon']?>" rel="shortcut icon" type="image/x-icon"><? echo "\n\t" ;?><link rel="icon" href="<?echo $this->HEAD['favicon']?>" type="image/x-icon"><?echo "\n\t" ;}
        if (sizeof($this->HEAD['css'])) foreach($this->HEAD['css'] as $file_name){?><link rel="stylesheet" type="text/css" href="<?echo $file_name?>" charset="utf-8"><? echo "\n\t" ; }
        ?><SCRIPT type=text/javascript>var _MAIN_DOMAIN="<?echo _MAIN_DOMAIN?>" ;</SCRIPT><?
        if (sizeof($this->HEAD['js']))  foreach($this->HEAD['js']  as $file_name){?><SCRIPT type="text/javascript" src="<?echo $file_name?>" ></SCRIPT><? echo "\n\t" ;}
        if ($_SESSION['send_turn_mail']) {?><SCRIPT type="text/javascript">$j(document).ready(function(){send_ajax_request({cmd:'send_turn_mail'});});</SCRIPT><? echo "\n\t" ; unset($_SESSION['send_turn_mail']);} // вызов AJAX для асинхронной отправки почты
        if (sizeof($this->HEAD['jscode']))  foreach($this->HEAD['jscode']  as $js_code){?><SCRIPT type="text/javascript"><?echo $js_code?></SCRIPT><? echo "\n\t" ;}
        if ($_SESSION['__fast_edit_mode']=='enabled') $this->fastedit_config() ;
        if (sizeof($GLOBALS['__functions']['page_head'])) foreach($GLOBALS['__functions']['page_head'] as $func_name)  $func_name() ;
        $this->local_page_head();
        echo $this->cur_page_head ;
         ?>
      </head>
      <?
      set_time_point('site_top_head - OK') ;
  }

  function check_page_robots_index() {return(0);} // возвращает 1 если страница не должна индексироваться роботами
  function local_page_head(){} // локальные заголовки, если они необходмы в конкретной странице

  function fastedit_config()
  { ?><SCRIPT type="text/javascript" src="<?echo _EXT?>/<?echo _CKEDITOR_?>/ckeditor.js" ></SCRIPT>
      <SCRIPT type="text/javascript" src="<?echo _PATH_TO_ENGINE?>/admin/fastedit.js" ></script>
      <SCRIPT type=text/javascript>
          var _PATH_TO_ADMIN='<?echo _PATH_TO_ADMIN?>/' ;
          var _PATH_TO_ADMIN_ADDONS='<?echo _PATH_TO_ADMIN_ADDONS?>' ;
          var _EXT='<?echo _EXT?>' ;
          var _EXT_DIR="<?echo _PATH_TO_EXT?>" ;
          var _FE_MODE=1 ;
          var _MAIN_DOMAIN="<?echo _MAIN_DOMAIN?>" ;
      </SCRIPT>
      <link rel="stylesheet" type="text/css" href="<?echo _PATH_TO_ENGINE?>/admin/fastedit.css" charset="utf-8">
    <?
  }

  // стандартный вывод тела страницы
  function body()
  { ?><body>
	      <div id=wrapper>
		     <div id=block_top> <? $this->block_top() ; ?></div>
		     <div id=block_left><? $this->block_left() ; ?></div>
		     <div id=block_main><? $this->block_main() ; ?></div>
	         <div id=block_right><? $this->block_right() ; ?></div>
	         <div class=clear></div>
	         <div id=block_bottom><? $this->block_bottom() ; ?></div>
	      </div>
      </body><?
   }

 function block_top() {}
 function block_left() {}
 function block_right() {}
 function block_bottom() {}

  // вывод страницы по умолчанию
  // по имени страницы проверяется, есть ли такая страница в БД
  // 9.01.2011 - добавлена возможность для создания страницек с прямым редактированием в БД
  // 1.04.2011 - отказ от site_page_block_main и $_block_templates
  // 5.08.2011 - вывод страницы вынесен в page_system
  function block_main()
  { $this->page_title() ; // стандартный заголовок страницы
    ?><div><?$_SESSION['pages_system']->show_page_content($GLOBALS['obj_info']) ;?></div><?
  }

  // базовый заголовк страницы - вверху путь, снизу заголовок - для шаблонов
  function page_title($title='',$path=array(),$options=array())
  { ?><div id="page_title"><?
    if (is_array($title)) { $options=$title ; $title='' ; $path=array() ; } // на случай, если page_title вызвана с путем в первом параметре
    $this->panel_path($path,$options);  // заголовк страницы по умолчанию
    $this->panel_title($title,$options);  // заголовк страницы по умолчанию
    ?></div><?
  }

  // показ заголовка h1 текущей страницы
  function panel_title($title='')
  { $text_h1=($title)? $title:$this->h1 ;
    if ($this->SEO_info['h1']) $text_h1=$this->SEO_info['h1'] ; // h1, заданный в метатегах, имеет наивысший приоритет
    ?><h1><? echo $text_h1 ?></h1><?
    if ($GLOBALS['obj_info']['pkey']) echo $GLOBALS['obj_info']['_fast_edit_icon'] ; // выводим иконку редактирования в режиме быстрого редактирования
  }

  // показ пути текущей страницы
 // 13.08.11 - используем шаблон для вывода пути
 // шаблон должен подключаться в $use_tempates_static[]='templates/page_path.php' ;
  // 25.09.11 - автоформирование path перенесено в prepare_page_title
  function panel_path($path=array(),$options=array())
  { // если путь не задан, берем сгенерированный на основе текущего объекта
    if (!sizeof($path)) $path=$this->path ; // формируется в c_core_HTML
    // совместимость со старой опцией
    if ($_SESSION['patch_rasdel']) $options['path_rasdel']=$_SESSION['patch_rasdel'] ;
    // выводим путь
    if (function_exists('page_path')) page_path($path,$options) ; // показываем путь через шаблон
    else $this->page_path($path,$options) ; // старый способ показа пути страницы
  }

  function page_path($path,$options=array())
   { $str=array() ; $res='' ;  $after_patch='' ;
     if ($options['after_path']) $after_patch=$options['after_path'] ;
     if (sizeof($path)) foreach($path as $href=>$name) $str[]='<a  href="'.$href.'">'.$name.'</a>' ;
     if ($options['from_level']) array_splice($str,0,$options['from_level']) ;

     if (sizeof($str)) $res=implode(' / ',$str) ;
       $res='<div id=panel_path>'.$res.$after_patch.'</div>' ;
       echo $res ;
   }

  function panel_404()
      {  ?><table align="center" cellpadding="0" cellspacing="0" height="100%">
                	<tbody><tr>
                		<td height="100%">
                			<table align="center" cellpadding="0" cellspacing="0">
                				<tbody>
                              <tr>
                					<td><img src="http://www.12-24.ru/images/404.jpg" alt="404"></td></tr><tr>
                					<td><h2><strong>К сожалению, запрашиваемой Вами страницы не существует на нашем сайте.</strong></h2>
                                        <p>Возможно, это случилось по одной из этих причин:</p>
                                        <p>
                                        </p><ul>
                                        <li>Вы ошиблись при наборе адреса страницы (URL)</li>
                                        <li>перешли по «битой» (неработающей, неправильной) ссылке</li>
                                        <li>запрашиваемой страницы никогда не было на сайте или она была удалена</li></ul>
                                        <p>Мы просим прощение за доставленные неудобства и предлагаем следующие пути:</p>
                                        <ul>
                                        <li>вернуться назад при помощи кнопки браузера back</li>
                                        <li>проверить правильность написания адреса страницы (URL)</li>
                                        <li>перейти на <a href="/"><strong>главную страницу сайта</strong></a></li>
                                        <li>воспользоваться картой сайта или поиском</li>
                                        <p>Если Вы уверены в правильности набранного адреса страницы и считаете, что эта ошибка произошла по нашей вине,<br> пожалуйста, сообщите об этом разработчикам (или владельцам) сайта при помощи контактной формы или электронной почты.<br>
                                           <br><br>E-mail: <a href="mailto:<?echo $_SESSION['LS_email_site']?>"><?echo $_SESSION['LS_email_site']?></a>
                                       </p>
                                    </td>
                				</tr>
                			</tbody></table>
                		</td>
                	</tr>
                </tbody></table>
         <?
 }

     //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   //
   //  ФОРМИРОВАНИЕ МЕТА-ТЕГОВ СТРАНИЦЫ
   //
   //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    // получить текущие мета-теги страницы  - вызывается в HTML перед выводом head страницы()
    // приоритет мета-тегов (с меньшего к большему)
    // 1. Теги по умолчанию
    // 2. Теги в скрипте
    // 3. Теги в объекте (но заданы в скрипте)
    // 4. Теги с SEO
    // к вызову функции уже могут быть заданы $this->title, $this->keywords, $this->description, $this->h1
    // а также $obj_info['title'], $obj_info['keywords'], $obj_info['description'], $obj_info['h1']
    function get_cur_page_meta_tags()
     { global $obj_info ; // описание объекта
       global $debug_metatags,$cur_system ;

       // берем сначала теги по умолчанию  - задаются в настройках сайта
       $def_title=$_SESSION['LS_def_page_title'] ;
       $def_keywords=$_SESSION['LS_def_page_keywords'] ;
       $def_description=$_SESSION['LS_def_page_descr'] ;
       $def_h1='Страница сайта' ;
       if ($this->result==404) $def_h1='Страница не найдена - 404' ;
       if ($debug_metatags) echo '<div class="green bold">Адрес страницы в карте сайта - '.$this->SEO_url.'</div>' ;
       if ($debug_metatags) echo '<div class="green bold">Объект страницы - '.$obj_info['obj_name'].'</div><br>' ;
       if ($debug_metatags) echo '<div class="green bold">Теги по умолчанию:</div><span class="red bold">title: </span>'.$_SESSION['LS_def_page_title'].'<br><span class="red bold">keywords: </span>'.$_SESSION['LS_def_page_keywords'].'<br><span class="red bold">descr: </span>'.$_SESSION['LS_def_page_descr'].'<br><span class="red bold">h1: </span>'.$def_h1.'<br>' ;
       // если определен текущий объект, то теги по умолчанию получаем на основе имени объекта
       if (sizeof($obj_info))
       { $obj_title='' ; $obj_keywords='' ;$obj_description='' ; $obj_h1='' ;
         if ($obj_info['obj_name'])                      $obj_title=$obj_info['obj_name'].(($_SESSION['LS_def_page_title'])? ' - '.$_SESSION['LS_def_page_title']:'') ;
         if ($obj_info['__name'])                        $obj_title=$obj_info['__name'].(($_SESSION['LS_def_page_title'])? ' - '.$_SESSION['LS_def_page_title']:'') ;
         if ($obj_info['__title'])                       $obj_title=$obj_info['__title'] ;

         if ($obj_info['obj_name'])                      $obj_h1=$obj_info['obj_name'] ;
         if ($obj_info['__name'])                        $obj_h1=$obj_info['__name'];
         if ($obj_info['__h1'])                          $obj_h1=$obj_info['__h1'] ;
         if ($_SESSION['__fast_edit_mode']=='enabled') $obj_h1.='<span class="fe" reffer="'.$obj_info['_reffer'].'"></span>' ;
         if ($debug_metatags) echo '<br><div class="green bold">Теги по умолчанию полученные из текущего объекта:</div><span class="red bold">title: </span>'.$obj_title.'<br><span class="red bold">keywords: </span>'.$obj_keywords.'<br><span class="red bold">descr: </span>'.$obj_description.'<br><span class="red bold">h1: </span>'.$obj_h1.'<br>' ;
         // переностим теги объекта в теги по умолчанию
         if ($obj_title)        $def_title=$obj_title ;
         if ($obj_keywords)     $def_keywords=$obj_title ;
         if ($obj_description)  $def_description=$obj_title ;
         if ($obj_h1)           $def_h1=$obj_h1 ;
       }
       // если заголовок еще не задан принудительно, пытаемся его получить из текущих объектов
       //if (!$this->h1 and $this->title)            $def_h1=$this->title ; // используем метатег title страницы
       //if (!$def_title and $this->h1)                  $def_title=$this->h1.' - '.$_SESSION['LS_def_page_title']  ;

       if ($debug_metatags) echo '<br><div class="green bold">Теги скрипта:</div><span class="red bold">title: </span>'.$this->title.'<br><span class="red bold">keywords: </span>'.$this->keywords.'<br><span class="red bold">descr: </span>'.$this->description.'<br><span class="red bold">h1: </span>'.$this->h1.'<br>' ;

       // если теги скрипта не заполнены - заполняем тегами по умолчнию (с участием объекта или из настроек сайта)
       //if (!$this->title)        $this->title=(($this->h1)? $this->h1.' - ':'').$def_title ;
       if (!$this->title)        $this->title=$def_title ;
       if (!isset($this->keywords))     $this->keywords=$def_keywords ;
       if (!isset($this->description))  $this->description=$def_description ;
       if (!$this->h1)           $this->h1=$def_h1 ;

       if (_CUR_PAGE_NUMBER>1)   { //$this->title='Страница '._CUR_PAGE_NUMBER.' - '.$this->title ;
                                   $this->title.=' - страница '._CUR_PAGE_NUMBER ;
                                   $this->h1.='<span class=paginator> cтраница '._CUR_PAGE_NUMBER.'</span>' ;
                                   if ($debug_metatags) echo '<br><div class="green bold">Теги страницы c учетом пагинатора:</div><span class="red bold">title: </span>'.$this->title.'<br>h1: </span>'.$this->h1.'<br>' ;
                                 }

       // если есть теги в описании товара, используем их
       // 21.03.12 - отключено - теги в объектах более не используются
       //if ($obj_info['title'])         $this->title=$obj_info['title'] ;
       //if ($obj_info['keywords'])      $this->keywords=$obj_info['keywords'] ;
       //if ($obj_info['description'])   $this->description=$obj_info['description'] ;
       //if ($debug_metatags) echo '<div class="green bold">Теги объекта:</div><span class="red bold">title: </span>'.$obj_info['title'].'<br><span class="red bold">keywords: </span>'.$obj_info['keywords'].'<br><span class="red bold">descr: </span>'.$obj_info['description'].'<br>' ;

       if ($this->SEO_info['title'])       $this->title=$this->SEO_info['title'] ;
       if ($this->SEO_info['keywords'])    $this->keywords=$this->SEO_info['keywords'] ;
       if ($this->SEO_info['description']) $this->description=$this->SEO_info['description'] ;
       if ($this->SEO_info['h1'])          $this->h1=$this->SEO_info['h1'] ;
       if ($debug_metatags) echo '<br><div class="green bold">Теги страницы в SEO:</div><span class="red bold">title: </span>'.$this->SEO_info['title'].'<br><span class="red bold">keywords: </span>'.$this->SEO_info['keywords'].'<br><span class="red bold">descr: </span>'.$this->SEO_info['description'].'<br><span class="red bold">h1: </span>'.$this->SEO_info['h1'].'<br>' ;

       // добавляем приставку и суффикс к заголовку сайта
       if ($_SESSION['LS_pre_title'])  $this->title=$_SESSION['LS_pre_title'].' '.$this->title ;
       if ($_SESSION['LS_after_title']) $this->title.=' '.$_SESSION['LS_after_title'] ;
       if ($_SESSION['__fast_edit_mode']=='enabled') $this->title.=' - быстрое редактирование' ;

       if (!$this->title and $this->h1)
       {  $this->title=$this->h1 ;
          if ($debug_metatags) echo '<br><div class="green bold"><span class="red bold">this->title</span> не задан, используем значение <span class="red bold">this->h1</span>'.$this->h1.'<br>' ;
       }

       // очищаем метатеги от html-форматирования
       $this->title=strip_tags($this->title) ;
       $this->keywords=strip_tags($this->keywords) ;
       $this->description=strip_tags($this->description) ;

       if ($debug_metatags) echo '<br><div class="green bold">Теги итог:</div><span class="red bold">title: </span>'.$this->title.'<br><span class="red bold">keywords: </span>'.$this->keywords.'<br><span class="red bold">descr: </span>'.$this->description.'<br><span class="red bold">h1: </span>'.$this->h1.'<br>' ;

       // если путь не задан, пытаемся его получить из текущих объектов
       //if (!sizeof($this->path) and $obj_info['__page_path'])    $this->path=$obj_info['__page_path'] ; // сначала пытаемся получить путь у текущего объекта  - 28.08.11 - устарело, в топку
       if (!sizeof($this->path) and is_object($cur_system))      $this->path=$cur_system->get_cur_page_path() ; // потом - у текущей системы
       if (!sizeof($this->path))                                 $this->path['/']=_INDEX_PAGE_NAME ;
     }

}

?>