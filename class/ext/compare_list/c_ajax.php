<?
include_once('messages.php') ;
include(_DIR_TO_ENGINE.'/c_page_XML_AJAX.php') ;
class c_page_ajax extends c_page_XML_AJAX
{
    function to_compare_list()
    {  global $goods_system ;
       $reffer=$_POST['reffer'] ;
       if (!isset($_SESSION['compare_list'][$reffer]))
       {   $_SESSION['compare_list'][$reffer]=$goods_system->select_db_obj_info($reffer) ;
           $goods_system->prepare_public_info($_SESSION['compare_list'][$reffer]) ;
           //$this->add_element('success','modal_window') ;
           //$this->add_element('title','Cписок сравнения') ;
           //$this->add_element('no_close_button',1) ;
           /*
           ?><p>Товар успешно добавлен в <a href="/comparison/">список сравнения</a><br>
               Сейчас в списке сравнения <strong><?echo sizeof($_SESSION['compare_list']).' '.get_case_by_count_1_2_5(sizeof($_SESSION['compare_list']),'товар,товара,товаров')?></strong></p><br>
             <p><button class=cancel>Закрыть</button>&nbsp;&nbsp;&nbsp;<button class=v1 href="/comparison/">Сравнить</button></p>
           <?*/
           $this->add_element('success','show_notife') ;
           $this->add_element('type','success') ;
           $this->add_element('pos_y','top') ;
           ?><p>Товар успешно добавлен в <a href="/comparison/">список сравнения</a><?
           ob_start() ;
           ?><script type="text/javascript">
                $j('div[reffer="<?echo $reffer?>"] div.to_compare').addClass('active').show() ;
                $j('div[reffer="<?echo $reffer?>"] button.to_compare_button').replaceWith('<button class="button_gray v1 to_compare_list" href="/comparison/">Перейти в сравнение</button>');
            </script><?
           $this->add_element('JSCODE',ob_get_clean()) ;

       }
       else
       {   unset($_SESSION['compare_list'][$_POST['reffer']]) ;
           $this->add_element('success','show_notife') ;
           $this->add_element('type','success') ;
           $this->add_element('pos_y','top') ;
           ?><p>Товар удален из <a href="/comparison/">списка сравнения</a><?

           ob_start() ;
           ?><script type="text/javascript">$j('div[reffer="<?echo $reffer?>"] div.to_compare').removeClass('active').hide() ;</script><?
           $this->add_element('JSCODE',ob_get_clean()) ;
       }
       // обновляем панель статуса сравнения в любом случае
       include_once('panel_compare_list_info.php') ;
       ob_start() ; panel_compare_list_info() ; $this->add_element('panel_compare_list_info',ob_get_clean()) ;

       ob_start() ;
       if (!sizeof($_SESSION['compare_list'])) echo 'Список сравнения пуст' ;
       else print_template($_SESSION['compare_list'],'list_goods_icons',array('id'=>'panel_compare_goods')) ;
       $this->add_element('panel_compare_goods',ob_get_clean()) ;
    }

    function del_from_compare_list()
    {  unset($_SESSION['compare_list'][$_POST['reffer']]) ;
       $this->panel_compare_list_info() ;

       // обновляем панель статуса сравнения в любом случае
       include_once('panel_compare_list_info.php') ;
       ob_start() ; panel_compare_list_info() ; $this->add_element('panel_compare_list_info',ob_get_clean()) ;
       
       $this->list_goods_compare_props() ;

    }

    function change_show_only_differences()
    {  $_SESSION['COMPARE_show_only_differences']=$_POST['checked'] ;
       $this->list_goods_compare_props() ;
    }

    function panel_compare_list_info()
    { include_once('panel_compare_list_info.php') ;
      ob_start() ;
      panel_compare_list_info() ;
      $this->add_element('panel_compare_list_info',ob_get_clean()) ;
    }


    function  list_goods_compare_props()
    { include_once('list_goods_compare_props.php') ;
      ob_start() ;
      list_goods_compare_props($_SESSION['compare_list'],array('id'=>'panel_compare_goods')) ;
      $this->add_element('panel_compare_goods',ob_get_clean()) ;
    }


}
