<?
  function list_goods_compare_props(&$arr_rec,$options=array())
  { $id=($options['id'])? 'id='.$options['id']:'' ;
    $res=array() ; $res_title=array() ; $arr_props_by_goods=array() ;  $arr_ids=array() ;

    // переделать в дальнейшем - тип товара должен получаться на основе его свойств
    //if (sizeof($arr_rec)) foreach($arr_rec as $rec) $arr_ids[]=$rec['pkey'] ;
    //if (sizeof($arr_ids)) $arr_props=execSQL('select pkey as pk,type,parent,id,value,num,str from '.$_SESSION['TM_goods_props'].' where parent in ('.implode(',',$arr_ids).')') ;

    ?><div class=list_goods_compare_props <?echo $id?>><?
    ?><label><input type="checkbox" value=1 class=v2 ext=compare_list cmd=change_show_only_differences <?if ($_SESSION['COMPARE_show_only_differences']) echo 'checked'?>> Показать только отличия</label><?
    $group=group_by_field('__type',$arr_rec,'pkey') ;
    if (sizeof($group)) foreach($group as $type_id=>$recs)
    {  // получаем описание свойств для текущего типа товара
       $props_info=clss_2_get_array_props($type_id) ;
       //damp_array($props_info) ;
       // получаем набор свойст для всех товаров в массиве
       $arr_props=execSQL('select pkey as pk,parent,id,value,num,str from '.$_SESSION['TM_goods_props'].' where parent in ('.implode(',',array_keys($recs)).')') ;
       if (sizeof($arr_props)) $arr_props_by_goods=group_by_field('parent',$arr_props,'pk') ;// damp_array($arr_props_by_goods) ;
       // заполяем список сравнения
       if (sizeof($recs)) foreach($recs as $rec)
          { // заполняем список сравнения для значений полей товара
            $res[$rec['pkey']]['img']='<div><a href="'.$rec['__href'].'"><img src="'.img_clone($rec,'small').'"></a><div class="delete v2" cmd=compare_list/del_from_compare_list reffer="'.$rec['_reffer'].'"></div></div>' ; $res_title['img']='' ;
            $res[$rec['pkey']]['name']='<a href="'.$rec['__href'].'">'.$rec['obj_name'].'</a>' ; $res_title['name']='Наименование' ;

            if ($rec['__brand'])      { $res[$rec['pkey']]['brand']=$rec['__brand'] ; $res_title['brand']='Бренд' ; }
            if ($rec['__collection']) { $res[$rec['pkey']]['collection']=$rec['__collection'] ; $res_title['collection']='Коллекция' ;    }
            if ($rec['__country'])    { $res[$rec['pkey']]['counry']=$rec['__country'] ; $res_title['counry']='Страна-производитель' ; }
            // заполняем список сравнения значениями свойства
            $res_props=array() ;
            if (sizeof($arr_props_by_goods[$rec['pkey']])) $res_props=get_props_list($props_info,$arr_props_by_goods[$rec['pkey']]) ;
            if (sizeof($res_props)) foreach($res_props as $prop_id=>$prop_info) { $res[$rec['pkey']]['prop_'.$prop_id]=$prop_info['value'] ; $res_title['prop_'.$prop_id]=$prop_info['title'] ; }
            // цена в конце
            if ($rec['__price_4']) { $res[$rec['pkey']]['price']=$rec['__price_5'].'<br><button class="v2" cmd=orders/add_to_cart reffer="'.$rec['_reffer'].'">В корзину</button>' ; $res_title['price']='Цена' ; }
            //damp_array($res_props)  ;
          }
        //damp_array($res) ;
        //damp_array($res_title) ;
       ?><table><?
       foreach($res_title as $title_id=>$title)
        { ob_start() ;
          $cnt_not_space=0 ; $cnt_different=0 ; unset($last_value) ;
          ?><td><? echo $title?></td><?
          foreach($recs as $rec)
            { $value=$res[$rec['pkey']][$title_id] ;
              ?><td><?echo $value?></td><?
              if ($value) $cnt_not_space++ ;
              if (!isset($last_value)) $last_value=$value ; elseif ($last_value!=$value) $cnt_different++ ;
            }
          ?><?
          $text=ob_get_clean() ;
          $tr_class=($cnt_different)? 'diff':'' ;
          if ($cnt_not_space and (!$_SESSION['COMPARE_show_only_differences'] or $cnt_different)) echo '<tr class="'.$tr_class.' '.$title_id.'">'.$text.'</tr>'  ;
        }
        ?></table><?
     }
     ?></div><?


  }

?>