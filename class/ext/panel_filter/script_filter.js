$j.fn.panel_filter = function(options)
        { var panel=this ;

          var settings = $j.extend(
                   {  'ajax_script' : '/ajax.php',
                      'ext'         : 'panel_filter',
                      'cmd'         : 'set_filter',
                      'target'      : 'div#block_main'
                   }, options);

          //$j(this).find('div.filter_item div.title').live('click',fixed_right_on_off) ;
          //$j(this).find('div.filter_item div.title').live('touchstart',fixed_right_on_off) ;
          //$j(this).find('div.filter_item div.fixed_right').hover(function(){},fixed_right_auto_off) ;
          $j(this).find('div.filter_set.checkbox div.item').live('click',item_checkbox_on_click) ;
          $j(this).find('div.filter_set.radio div.item').live('click',item_radio_on_click) ;
          $j(this).find('button.apply_filter').live('click',set_filter_value) ;

          if (!is_touch_device()) $j(this).addClass('hovered') ;



           // выпадающие списки - со старой версии
          //$j('div#panel_filter div.as_select').live('click',function(){$j(this).children('div.list_items').show() ;});
          //$j('div#panel_filter div.as_select').hover(function(){},function(){$j(this).children('div.list_items').hide() ;});
          //$j('div#panel_filter div.as_select div.list_items').hover(function(){},function(){$j(this).hide() ;});


          function item_checkbox_on_click()
          {  $j(this).toggleClass('on') ;
             //set_filter_value() ;
             var el=$j(this).closest('div.filter_item').find('div.cur_filter_status div.item[value="'+$j(this).attr('value')+'"]') ;
             if (!el.length)  $j(this).closest('div.filter_item').find('div.cur_filter_status').append('<div class="item" value="'+$j(this).attr('value')+'">'+$j(this).text()+'<div class="del"></div></div>') ;
             else el.remove() ;
             return(false) ;
          }

          function item_radio_on_click()
          {  var is_check=($j(this).hasClass('on'))? 1:0;
             $j(this).closest('div.filter_set').find('div.item').removeClass('on') ;
             if (!is_check) $j(this).addClass('on') ;
             set_filter_value() ;
             return(false) ;
          }

          // собираем значения фильтра, отправляем на ajax
          function set_filter_value()
          { var filter_values= {} ;
            $j(panel).find('div.filter_item div.title').removeClass('active');
            filter_values['price']=new Array() ;
            //$j(panel).find('div.filter_set').each(function()
            $j(panel).find('div.filter_set').each(function()
            {  var filter_name=$j(this).attr('name') ;
               // собираем значения фильтров checkbox и radio
               if ($j(this).hasClass('checkbox'))
               { var select_items=$j(this).find('div.item.on') ;
                 var values= new Array ;
                 if (select_items.length)
                 {  select_items.each(function(){values.push($j(this).attr('value')) ;}) ;
                    if (values.length) filter_values[filter_name] = values ;
                 }
               }
               if ($j(this).hasClass('radio')) filter_values[filter_name]=$j(this).find('div.item.on').attr('value') ;

               //
               if ($j(this).find('input[type="text"][used=1]').length)
               { var arr={};
                 $j(this).find('input[type="text"][used=1]').each(function()
                  { var input_name=$j(this).attr('name') ;
                    arr[input_name]=$j(this).val();
                  }) ;
                 filter_values[filter_name]=arr ;
               }
            });

            var post_data={} ;
            post_data.filter=filter_values ;
            post_data.cmd=settings.ext+'/'+settings.cmd ;
            post_data.base_url=$j(panel).attr('base_url') ;
            post_data.cur_type=$j(panel).attr('cur_type') ;

            // отправляем ajax запрос
            $j.ajax(  {url:settings.ajax_script,dataType:'xml',data:post_data,type:'post',
                       success:function(data)
                        { $j(settings.target).html(get_xml_tag_value(data,'html')) ;
                          //$j('div#cnt_show').html(get_xml_tag_value(data,'cnt_show')) ;
                          // менякм адрес в адресной строке браузера - после изменения фильтра будет подгружена первая страница
                          history.pushState(null, null,get_xml_tag_value(data,'url_dir'));
                          $j('.v1').prepare_form({}) ;
                          $j('.v2').prepare_ajax({}) ;
                        }
                      });
          }

         function fixed_right_on_off()
         {   var f=$j(this).hasClass('active') ;
             $j('div#panel_filter div.filter_item div.title').removeClass('active');
            if (!f) $j(this).addClass('active') ;
         }

         function fixed_right_auto_off()
         {
             $j(this).prev('div.title').removeClass('active') ;
         }

        function is_touch_device() { return !!('ontouchstart' in window); }

        } ;