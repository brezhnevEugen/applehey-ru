<?
   // шаблон - элементы ссылками списком в две колонки
  include_once(_DIR_TO_TEMPLATES.'/panel_table.php') ;

  function panel_result_of_filter($options=array())
  { global $goods_system,$category_system ;
    $cur_type=$options['cur_type'] ;
    ?><div id="panel_result_of_filter"><?
       // получение условий выборки по параметрам фильтра с учетом свойств товара возможно только при указании текущего типа товара
       list($title,$usl_filter,$url_dir,$arr_props_title,$select_options)=get_usl_by_filter($_SESSION['filter'],$options) ;
       ?><h1><?echo $title?></h1><h2><?panel_table($arr_props_title,array('id'=>'panel_filter_props'))?></h2><?
       // делаем запрос на коллекции на основе цен товаров
       //$coll_ids=execSQL_row('select distinct(series_id), min(price_site) as min_price from obj_site_goods where '.$usl_filter.' and series_id>0 group by series_id order by min_price') ;
       // если товар идет в колекции, то показываем коллекции товара
       /*if ($category_system->tree[$cur_type]->rec['view_coll'])
       //if (sizeof($coll_ids))
          {   // коллекции надо вывести по возрастанию цены. Но минимальная цена определаяется текущим типом товара. Для раздела керамогранита будет своя минимальная цена, для мозаики - своя.
              if (sizeof($coll_ids)) $goods_system->show_list_section(array_keys($coll_ids),'catalog/list_collection_icons',array('debug'=>0,'min_prices'=>$coll_ids,'panel_select_pages'=>1)) ;
              $cnt_show=sizeof($coll_ids)  ;
              if ($cnt_show) echo '<br>Показано: '.$cnt_show.' коллекций<br>' ;
          }
          // иначе просто показываем товар
          else*/
          { $options_view_goods=array('order'=>'price_site','only_clss'=>200,'show_items_from_child'=>1,'panel_view_mode'=>'top','panel_compare_info'=>'0','panel_select_usl'=>'0','panel_select_pages'=>'1','debug'=>0) ;
            $options_view_goods=array_merge($options_view_goods,$select_options) ;
            $options_view_goods['pages_base_url']=$url_dir ; //для корректной навигации страниц
            //damp_array($options_view_goods) ;
            $cnt_show=$goods_system->show_list_items($usl_filter,'catalog/list_goods_icons',$options_view_goods) ;
          }
          if (!$cnt_show)
          { // при сборосе параметров фильтра необходимо перейти в текущйи раздел. Сбрасываем все фильтры
            $filter=array() ;
            list($title,$usl_filter,$_url_dir,$arr_props_title,$options_select)=get_usl_by_filter($filter,$options) ;
            echo '<div class=alert>Под текущие настройки фильтра не попадает ни один товар.<br><br><button class=v1 href="'.$_url_dir.'">Сбросить настройки фильтра</button></div>' ;
          }
     ?></div><?

    return(array($cnt_show,$url_dir)) ;
  }

?>