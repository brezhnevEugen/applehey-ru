<?

function panel_filter($options=array())
{ global $goods_system,$category_system ;
  //damp_array($_SESSION['filter'],1,-1) ;
  // передача cur_type необходима для определния набора свойстd
  ?><div id=panel_filter base_url="<?echo $options['base_url']?>" cur_type="<?echo $options['cur_type']?>"><div class=inner><?

      /*?><div class=title_filter>Поиск по параметрам</div><?*/
      $usl_type='(type='.$options['cur_type'].' or type2='.$options['cur_type'].' or type3='.$options['cur_type'].')' ;
      // ЦЕНА - слайдер


      ?><div class="filter_item"><div class=title>Цена</div><?
         $price_step=100 ;
         // получаем минимальную и максималую цену для товаров текущего типа или товаров, тип которых не задан
         $sql='select min(price_site) as min,max(price_site) as max from obj_site_goods where clss=200 and _enabled=1 and enabled=1 and '.$usl_type ;
         $rec=execSQL_van($sql) ;
         $price_start=floor($rec['min']/$price_step)*$price_step ;
         $price_end=ceil($rec['max']/$price_step)*$price_step ;
         $price_from=($_SESSION['filter']['price']['from'])? $_SESSION['filter']['price']['from']:$price_start ;
         $price_to=($_SESSION['filter']['price']['to'])? $_SESSION['filter']['price']['to']:$price_end ;  ;
         panel_slider('price',$price_start,$price_end,$price_from,$price_to,$price_step) ;
      ?></div><?

      // БРЕНДЫ - первый уровень каталога
      // внимание!!! тут id бренда идет в индексах массива
      $cur_brands_ids=execSQL_line('select distinct(brand_id) from obj_site_goods where  clss=200 and brand_id>0 and _enabled=1 and enabled=1 and '.$usl_type) ;
      if (sizeof($cur_brands_ids))
          { $check_items=(sizeof($_SESSION['filter']['brand']))? $_SESSION['filter']['brand']:array() ;
            ?><div class="filter_item"><div class=title>Производитель</div>
                <div name=brand class="filter_set checkbox fixed_right"><? $goods_system->show_list_section($cur_brands_ids,'panel_filter/list_items_table_4cols_a_name',array('check_items'=>$check_items,'debug'=>0)) ; ?></div><?
                ?><div class="cur_filter_status"><? if (sizeof($_SESSION['filter']['brand'])) foreach($_SESSION['filter']['brand'] as $brand_id) {?><div class="item" value="<?echo $brand_id?>"><?echo $goods_system->tree[$brand_id]->name?></div><?}?></div><?
            ?></div><?
          }


      // страны-производители, задаются в каждом бренде
      // получаем список стран, представленных текущми брендами
      if (sizeof($cur_brands_ids)) foreach($cur_brands_ids as $brand_id)
        { $country_id=$goods_system->tree[$brand_id]->rec['country_id'] ;
          if ($country_id) $arr_country[$country_id]=$_SESSION['IL_country'][$country_id] ;
        }
      if (sizeof($arr_country))
      { $check_items=(sizeof($_SESSION['filter']['country']))? $_SESSION['filter']['country']:array() ;
        ?><div class="filter_item"><div class=title>Страна</div>
             <div name=country class="filter_set checkbox fixed_right"><? print_template($arr_country,'panel_filter/list_items_table_4cols_a_name',array('key_field'=>'id','check_items'=>$check_items)) ; ?></div><?
             ?><div class="cur_filter_status"><? if (sizeof($_SESSION['filter']['country'])) foreach($_SESSION['filter']['country'] as $country_id) {?><div class="item" value="<?echo $country_id?>"><?echo $_SESSION['IL_country'][$country_id]['obj_name']?></div><?}?></div><?
        ?></div><?
      }

      if (!isset($_SESSION['div_status']['div#panel_filter_props'])) $_SESSION['div_status']['div#panel_filter_props']=array('text'=>'Больше параметров','status'=>'close') ;
      ?><div class="clear"></div>
        <div class="change_view_filter_props"><span class="open_target_div" t="div#panel_filter_props" t1="Больше параметров" t2="Меньше параметров"><?echo $_SESSION['div_status']['div#panel_filter_props']['text']?></span></div>
        <div class="clear"></div>
        <div id="panel_filter_props" class="<?echo $_SESSION['div_status']['div#panel_filter_props']['status']?>"><?
      //  если задан текущий тип, получаем список параметров
      $cur_type=$options['cur_type'] ;
      if ($cur_type) $props_info=$category_system->get_array_props_info($cur_type) ;   // damp_array($props_info) ;
      if (sizeof($props_info)) foreach($props_info as $prop)
      { $prop_filter_name='prop_'.$cur_type.'_'.$prop['id']  ;
           if (isset($prop['indx_select']))
           {   ?><div class="filter_item indx_select"><div class=title><?echo $prop['title']?></div><?
               $prop_recs=$_SESSION[$prop['indx_select']] ;
              //damp_array($prop_recs) ;
              ?><div name="<?echo $prop_filter_name?>" class="filter_set checkbox fixed_right">
               <? $col=print_template($prop_recs,'panel_filter/list_items_table_4cols_a_name',array('key_field'=>'id','check_items'=>$_SESSION['filter'][$prop_filter_name])) ; ?></div><?
              ?><div class="cur_filter_status"><? if (sizeof($_SESSION['filter'][$prop_filter_name])) foreach($_SESSION['filter'][$prop_filter_name] as $value) {?><div class="item" value="<?echo $value?>"><?echo $prop_recs[$value]['obj_name']?></div><?}?></div><?
              ?></div><?

           }

           if ($prop['edit_element']=='input_num')
           {  ?><div class="filter_item input_num"><div class=title><?echo $prop['title']?></div><?
              $step=1 ;
              // получаем минимальную и максималую цену для товаров текущего типа или товаров, тип которых не задан
              $rec=execSQL_van('select  min(num) as min,max(num) as max from obj_site_goods_props where type='.$cur_type.' and id='.$prop['id'].' and num>0 and not num is null order by num') ;
              $value_start=floor($rec['min']/$step)*$step ;
              $value_end=ceil($rec['max']/$step)*$step ;
              $value_from=($_SESSION['filter'][$prop_filter_name]['from'])? $_SESSION['filter'][$prop_filter_name]['from']:$value_start ;
              $value_to=($_SESSION['filter'][$prop_filter_name]['to'])? $_SESSION['filter'][$prop_filter_name]['to']:$value_end ;  ;
              panel_slider($prop_filter_name,$value_start,$value_end,$value_from,$value_to,$step) ;
              ?></div><?
           }

           if ($prop['edit_element']=='checkbox')
           {  ?><div class="filter_item checkbox"><div class=title><?echo $prop['title']?></div><?
              $prop_recs=array(array('id'=>1,'obj_name'=>'Да')) ;
             ?><div name="<?echo $prop_filter_name?>" class="filter_set checkbox fixed_right"><? print_template($prop_recs,'panel_filter/list_items_table_4cols_a_name',array('key_field'=>'id','check_items'=>$_SESSION['filter'][$prop_filter_name])) ; ?></div><?
             //damp_array($_SESSION['filter'][$prop_filter_name]) ;
             //damp_array($_SESSION['filter'][$prop_filter_name]) ;
             ?><div class="cur_filter_status"><? if ($_SESSION['filter'][$prop_filter_name][0]) {?><div class="item" value="1">Да</div><?}?></div><?
             ?></div><?
           }
           /*if ($col>=4) {?><div class="clear"></div><? $col=0 ;}*/

      }
      ?></div><div class="clear"></div><?
      ?><div class="apply_filter"><button class="apply_filter button_green">Показать</button></div><?
      ?><div _class=debug></div></div></div>
      <script type="text/javascript">$j('#panel_filter').panel_filter({target:'div#panel_result_of_filter'}) ;</script>
  <?
  //damp_array($_SESSION['filter']) ;
}

  function panel_slider($slider_name,$start,$end,$from,$to,$step)
  { // внимание!!! весь внутренний контент div.slider будет затерт
    ?><div class="slider input_text filter_set" name=<?echo $slider_name?> id="<?echo $slider_name?>_slider"></div>
     <script type="text/javascript">$j(document).ready(function()
        { $j('div#<?echo $slider_name?>_slider').clss_slider({start:<?echo $start?>,
                                                              end:<?echo $end?>,
                                                              from:<?echo $from?>,
                                                              to:<?echo $to?>,
                                                              step:<?echo $step?>,
                                                              from_name:'<?echo $slider_name?>_from',
                                                              to_name:'<?echo $slider_name?>_to'
                                                             }) ;
        });</script>
   <?
  }


?>