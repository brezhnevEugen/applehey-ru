<?php  // поиск по сайту
// новая схема поиска
// по каждой подсистеме по собственному аглоритму отбираем необходмые записи
// массив записий уже показываем через шаблон
//
include(_DIR_TO_CLASS.'/c_page.php') ;
class c_page_search extends c_page
{
  public $h1='Поиск по сайту' ;

 function block_main()
 { $search_text=$_GET['search_text'] ;
   if (!$search_text) $search_text=_CUR_PAGE_NAME ;
   if (!$search_text) { $arr=explode('/',_CUR_PAGE_DIR) ; $search_text=$arr[2] ; }

   $search_text=strip_tags($search_text) ;
   $search_text=addslashes($search_text) ;

   if (!isset($_SESSION['search_stat'])) $_SESSION['search_stat']=new c_search() ;

   $this->page_title() ;

   if ($search_text)
   {   ?><p class=info>Вы искали: <span class=bold>'<? echo $search_text?>'</span></p><?
       $recs=array() ;  $search_recs=array() ;
       //$recs[]=$this->get_brands($search_text) ;            // поиск по брендам
       //$recs[]=$this->get_catalog_keywords($search_text) ;  // поиск по ключевым словам в каталоге
       $recs[]=$this->get_catalog_sections($search_text) ;  // поиск по разделам каталога
       $recs[]=$this->get_catalog_collections($search_text) ;  // поиск по разделам каталога
       $recs[]=$this->get_catalog_goods($search_text) ;     // поиск по товарам
       //$recs[]=$this->get_responses($search_text) ;         // поиск по отзывам
       //$recs[]=$this->get_faq($search_text) ;               // поиск по FAQ
       $recs[]=$this->get_pages($search_text) ;               // поиск по страницам сайта
       //$recs[]=$this->get_art($search_text) ;               // поиск по статьям
       //$recs[]=$this->get_news($search_text) ;              // поиск по новостям
       //$recs[]=$this->get_forum_themes($search_text) ;        // поиск по названию тем на форуме
       //$recs[]=$this->get_forum_messages($search_text) ;      // поиск по содержанию сообщений на форуме
       include('list_search_result_img.php') ;
       if (sizeof($recs)) foreach($recs as $recs2) if (sizeof($recs2)) foreach($recs2 as $rec) $search_recs[]=$rec ;
       if (sizeof($search_recs))
       { echo 'Всего найдено: <strong>'.sizeof($search_recs).' '.get_case_by_count_1_2_5(sizeof($search_recs),'совпадение,совпадения,совпадений').'</strong>' ;
         print_template($search_recs,'list_search_result_img') ;
       }
       else {?><p class=alert>К сожалению, по Вашему запросу ничего не найдено. Пожалуйста, задайте новые условия поиска и попробуйте еще раз.<p><?
             include_once('panel_search_main.php') ;
             panel_search_main() ;
            }
       return ;
   }
   else
   {
     ?><p class=alert>Не задано условие поиска</p><br><?
     include_once('panel_search_main.php') ;
     panel_search_main() ;
   }

 }

 function get_catalog_sections($text)
 { global $goods_system ;
   // подготовливает условие для поиска по стандартным полям товара
   //$recs=execSQL('select * from '.$goods_system->table_name.' where '.$goods_system->tree_usl_select.' and (obj_name like "%'.$text.'%" or pkey="'.$text.'")',2 ) ;
   $recs=execSQL('select * from '.$goods_system->table_name.' where clss in (1,10) and enabled=1 and _enabled=1 and (obj_name like "%'.$text.'%" or pkey="'.$text.'")') ;
   $goods_system->prepare_public_info_for_arr($recs) ;
   if (sizeof($recs)) foreach($recs as $i=>$rec) if (isset($goods_system->tree[$rec['parent']]))
       { $arr_parents=$goods_system->tree[$rec['parent']]->get_arr_parent_name(array('to_level'=>1)) ;
         $str_parents=(sizeof($arr_parents))? implode(' / ',$arr_parents).' / ':'' ;
         $recs[$i]['__theme']='Раздел каталога ' ;
         $recs[$i]['__title']=$str_parents.'<strong>'.$rec['obj_name'].'</strong>' ;
         $recs[$i]['__intro']=$rec['intro'] ;
         $recs[$i]['__url']=$rec['__href'] ;
       }
   return($recs) ;
 }

 function get_catalog_brands($text)
 { global $goods_system ;
   // подготовливает условие для поиска по стандартным полям товара
   $recs=execSQL('select * from '.$goods_system->table_name.' where clss=201 and enabled=1 and _enabled=1 and (obj_name like "%'.$text.'%" or pkey="'.$text.'")' ) ;
   $goods_system->prepare_public_info_for_arr($recs) ;
   if (sizeof($recs)) foreach($recs as $i=>$rec) if (isset($goods_system->tree[$rec['parent']]))
       { $arr_parents=$goods_system->tree[$rec['parent']]->get_arr_parent_name(array('to_level'=>1)) ;
         $str_parents=(sizeof($arr_parents))? implode(' / ',$arr_parents).' / ':'' ;
         $recs[$i]['__theme']='Бренд ' ;
         $recs[$i]['__title']=$str_parents.'<strong>'.$rec['obj_name'].'</strong>' ;
         $recs[$i]['__intro']=$rec['intro'] ;
         $recs[$i]['__url']=$rec['__href'] ;
       }
   return($recs) ;
 }

 function get_catalog_collections($text)
 { global $goods_system ;
   // подготовливает условие для поиска по стандартным полям товара
   $recs=execSQL('select * from '.$goods_system->table_name.' where clss=210 and enabled=1 and _enabled=1 and (obj_name like "%'.$text.'%" or pkey="'.$text.'")' ) ;
   $goods_system->prepare_public_info_for_arr($recs) ;
   if (sizeof($recs)) foreach($recs as $i=>$rec) if (isset($goods_system->tree[$rec['parent']]))
       { $arr_parents=$goods_system->tree[$rec['parent']]->get_arr_parent_name(array('to_level'=>1)) ;
         $str_parents=(sizeof($arr_parents))? implode(' / ',$arr_parents).' / ':'' ;
         $recs[$i]['__theme']='Коллекция ' ;
         $recs[$i]['__title']=$str_parents.'<strong>'.$rec['obj_name'].'</strong>' ;
         $recs[$i]['__intro']=$rec['intro'] ;
         $recs[$i]['__url']=$rec['__href'] ;
       }
   return($recs) ;
 }

 function get_catalog_goods($text)
 { global $goods_system ;
   $_usl[]=$goods_system->prepare_search_usl('art','like%%',$text) ;
   $_usl[]=$goods_system->prepare_search_usl('obj_name','like%%',$text) ;
   $_usl[]=$goods_system->prepare_search_usl('pkey','=',$text) ;
   $_usl[]=$goods_system->prepare_search_usl('manual','like%%',$text) ;
   $res_usl='('.implode(' or ',$_usl).')';
   //$recs=execSQL('select * from '.$goods_system->table_name.' where '.$goods_system->usl_show_items.' and '.$res_usl,2) ;
   //$recs=execSQL('select * from '.$goods_system->table_name.' where clss=200 and enabled=1 and _enabled=1  and '.$res_usl) ;
   $recs=select_db_recs($goods_system->table_name,'clss=200 and enabled=1 and _enabled=1  and '.$res_usl,array()) ;
   $goods_system->prepare_public_info_for_arr($recs) ;
   if (sizeof($recs)) foreach($recs as $i=>$rec) if (isset($goods_system->tree[$rec['parent']]))
     { $recs[$i]['__theme']='Товар ' ;
       $recs[$i]['__title']=implode(' / ',$goods_system->tree[$rec['parent']]->get_arr_parent_name(array('to_level'=>1))).' / <strong>'.$rec['obj_name'].'</strong>' ;
       $recs[$i]['__intro']=$rec['intro'] ;
       $recs[$i]['__url']=$rec['__href'] ;
     }
   return($recs) ;
 }

 function get_brands($text)
 { global $goods_system,$TM_goods ;
   $recs=execSQL('select * from '.$TM_goods.' where clss=201 and enabled=1 and (obj_name like "%'.$text.'%" or intro like "%'.$text.'%")') ;
   $goods_system->prepare_public_info_for_arr($recs) ;
   if (sizeof($recs)) foreach($recs as $i=>$rec)
     { $arr_parents=$goods_system->tree[$rec['parent']]->get_arr_parent_name(array('to_level'=>1)) ;
       $recs[$i]['__theme']='Бренд ' ;
       $recs[$i]['__title']='<strong>'.$rec['obj_name'].'</strong>' ;
       $recs[$i]['__url']=$rec['__href'] ;
     }
    return($recs) ;
 }

 function get_catalog_keywords($text)
 { global $goods_system,$TM_goods ;
   $recs=execSQL('select * from '.$TM_goods.' where search_keywords like "%'.$text.'%"') ;
   $goods_system->prepare_public_info_for_arr($recs) ;
   if (sizeof($recs)) foreach($recs as $i=>$rec)
    { $arr_parents=$goods_system->tree[$rec['parent']]->get_arr_parent_name(array('to_level'=>1)) ;
      $str_parents=(sizeof($arr_parents))? implode(' / ',$arr_parents).' / ':'' ;
      $recs[$i]['__theme']=($rec['clss']==200)? 'Товар ':'Раздел каталога ' ;
      $recs[$i]['__title']=$str_parents.'<strong>'.$rec['obj_name'].'</strong>' ;
      $recs[$i]['__url']=$rec['__href'] ;
    }
   return($recs) ;
 }

 function get_responses($text)
 { global $responses_system ;
   $_usl[]=$responses_system->prepare_search_usl('value','like%%',$text) ;
   $_usl[]=$responses_system->prepare_search_usl('comment','like%%',$text) ;
   $res_usl='('.implode(' or ',$_usl).')';
   $recs=execSQL('select * from '.$responses_system->table_name.' where '.$responses_system->usl_show_items.' and '.$res_usl) ;
   $responses_system->prepare_public_info_for_arr($recs) ;
   if (sizeof($recs)) foreach($recs as $i=>$rec)
   { $recs[$i]['__theme']='Отзыв для ' ;
     $recs[$i]['__title']=$rec['autor_name'].' '.date("d.m.y",$rec['c_data']) ; ;
     $recs[$i]['__url']=$rec['__href'] ;
   }
   return($recs) ;
 }

 function get_faq($text)
 { global $faq_system ;
   $_usl[]=$faq_system->prepare_search_usl('value','like%%',$text) ;
   $_usl[]=$faq_system->prepare_search_usl('comment','like%%',$text) ;
   $res_usl='('.implode(' or ',$_usl).')';
   $recs=execSQL('select * from '.$faq_system->table_name.' where '.$faq_system->usl_show_items.' and '.$res_usl) ;
   $faq_system->prepare_public_info_for_arr($recs) ;
   if (sizeof($recs)) foreach($recs as $i=>$rec)
   { $recs[$i]['__theme']='Вопрос-ответ для ' ;
     $recs[$i]['__title']=$rec['autor_name'].' '.date("d.m.y",$rec['c_data']) ; ;
     $recs[$i]['__url']=$rec['__href'] ;
   }
   return($recs) ;
 }

 // поиск в статьях
 function get_art($text)
 { global $art_system ;
   $_usl[]=$art_system->prepare_search_usl('obj_name','like%%',$text) ;
   $_usl[]=$art_system->prepare_search_usl('intro','like%%',$text) ;
   $_usl[]=$art_system->prepare_search_usl('manual','like%%',$text) ;
   $res_usl='('.implode(' or ',$_usl).')';
   $recs=execSQL('select * from '.$art_system->table_name.' where '.$art_system->usl_show_items.' and '.$res_usl) ;
   $art_system->prepare_public_info_for_arr($recs) ;
    if (sizeof($recs)) foreach($recs as $i=>$rec)
      { $recs[$i]['__theme']='Статья ' ;
        $recs[$i]['__title']=implode(' / ',$art_system->tree[$rec['parent']]->get_arr_parent_name(array('to_level'=>1))).' / <strong>'.$rec['obj_name'].'</strong>' ;
        $recs[$i]['__url']=$rec['__href'] ;
      }
    return($recs) ;
 }

 // поиск по страницам сайта
 function get_pages($text)
 { global $pages_system ;
   $_usl[]=prepare_search_usl('obj_name','like%%',$text) ;
   $_usl[]=prepare_search_usl('value','like%%',$text) ;
   $res_usl='('.implode(' or ',$_usl).')';
   $recs=execSQL('select * from '.$pages_system->table_name.' where '.$pages_system->usl_show_items.' and '.$res_usl) ;
   $pages_system->prepare_public_info_for_arr($recs) ;

    if (sizeof($recs)) foreach($recs as $i=>$rec)
      { $recs[$i]['__theme']='Страница сайта  ' ;
        $recs[$i]['__title']='<strong>'.$rec['obj_name'].'</strong>' ;
        $recs[$i]['__url']=$rec['__href'] ;
      }
    return($recs) ;
 }

 // поиск в новостях
 function get_news($text)
 { global $news_system ;
    $_usl[]=$news_system->prepare_search_usl('obj_name','like%%',$text) ;
    $_usl[]=$news_system->prepare_search_usl('annot','like%%',$text) ;
    $_usl[]=$news_system->prepare_search_usl('value','like%%',$text) ;
    $res_usl='('.implode(' or ',$_usl).')';
    $recs=execSQL('select * from '.$news_system->table_name.' where '.$news_system->usl_show_items.' and '.$res_usl) ;
    $news_system->prepare_public_info_for_arr($recs) ;
    if (sizeof($recs)) foreach($recs as $i=>$rec)
       { $recs[$i]['__theme']='Новость ' ;
         $recs[$i]['__title']=implode(' / ',$news_system->tree[$rec['parent']]->get_arr_parent_name(array('to_level'=>1))).' / <strong>'.$rec['obj_name'].'</strong>' ;
         $recs[$i]['__url']=$rec['__href'] ;
       }
   return($recs) ;
 }

 // поиск в темах форума
 function get_forum_themes($text)
 { global $forum_system ;
   $_usl[]=$forum_system->prepare_search_usl('obj_name','like%%',$text) ;
   $res_usl='('.implode(' or ',$_usl).')';
   $recs=execSQL('select * from '.$forum_system->table_name.' where '.$forum_system->usl_show_items.' and clss=28 and '.$res_usl) ;
   $forum_system->prepare_public_info_for_arr($recs) ;
   if (sizeof($recs)) foreach($recs as $i=>$rec)
   { $recs[$i]['__theme']='Тема на форуме ' ;
     $recs[$i]['__title']=$rec['obj_name'] ;
     $recs[$i]['__url']=$rec['__href'] ;
   }
   return($recs) ;
 }

 // поиск в сообщениях форума
 function get_forum_messages($text)
 { global $forum_system ;
   $_usl[]=$forum_system->prepare_search_usl('obj_name','like%%',$text) ;
   $res_usl='('.implode(' or ',$_usl).')';
   $recs=execSQL('select * from '.$forum_system->table_name.' where '.$forum_system->usl_show_items.' and clss=15 and '.$res_usl) ;
   if (sizeof($recs))
   { $recs_by_parent=group_by_field('parent',$recs) ;
     $recs_parent=execSQL('select * from '.$forum_system->table_name.' where '.$forum_system->usl_show_items.' and clss=28 and pkey in ('.implode(',',array_keys($recs_by_parent)).')') ;
     $forum_system->prepare_public_info_for_arr($recs_parent) ;
     foreach($recs as $i=>$rec)
       { $recs[$i]['__theme']='Сообщение на форуме ' ;
         $recs[$i]['__title']=$rec['obj_name'] ;
         $recs[$i]['__url']=$recs_parent[$rec['parent']]['__href'].'#'.$rec['pkey'] ;
       }
   }
   return($recs) ;
 }



// возвращает результаты поиска по стандартному алгоритму в обычных полях товара
    // в принице может вернуть только условие поиска, а выборка может бытб сделана в рамках функции show_list_items
    function get_items_search($usl,$options=array())
    { //trace() ;
      $text=($options['search_text'])? $options['search_text']:$usl ; // текст для поиска передается либо в первом параметре либо через опции
      if (!$options['search_text']) $usl=$this->usl_show_items ;      // если текст поиска передан через первый пареметр, то обязательное условие отбора берем из
      if (!$text) return ;
      //damp_string($text) ;
      //$text=urldecode($text) ;


      //ищем раздел с заданным названием
      /*
      $rasd_id=execSQL_value('select pkey from '.$this->table_name.' where clss=10 and parent=1 and obj_name like "'.$text.'"') ;
      if ($rasd_id)
      {   $to_url=$this->tree[$rasd_id]->href ;
          header("HTTP/1.1 302 Moved Permanently");
          header("Location: ".$to_url);
          exit() ;
      }
      */
      /*
      // ищем в брендах товаров
      global $IL_brendy ;
      if (sizeof($IL_brendy)) foreach($IL_brendy as $rec)
      { // полное совпадение с брендом
        if(trim($rec['obj_name'])==trim($text))
        {  header("HTTP/1.1 302 Moved Permanently");
           header("Location: ".$rec['__href']);
           exit() ;
        }
        // в посике бренд + модель
        if (stripos($text,$rec['obj_name'])!==false)
        { $text=trim(str_replace($rec['obj_name'],'',$text)) ; // удаляем бренд из поиска - останеться модель, будет переход на страницу моджели по правилам ниже

        }
      }
       */
      // ищем  по title в карте сайта
      /*
      $url_page=execSQL_value('select url from obj_site_sitemap where not url is null and status=200 and (no_sitemap is null or no_sitemap=0) and title like "'.$text.'" limit 1') ;
      if ($url_page)
      {  header("HTTP/1.1 302 Moved Permanently");
         header("Location: http://".$url_page);
         exit() ;
      }
      */



      // ищем по ключевым словам.
      // 1. Получаем коды и классы объектов, содержащие ключевые слова
      //trace() ;
      global $TM_goods ;
      $list_ids=execSQL_row('select pkey,clss from '.$TM_goods.' where search_keywords like "%'.$text.'%"') ;
      damp_array($list_ids) ;
      $id_goods=array() ; $id_section=array() ;
      if (sizeof($list_ids)) foreach($list_ids as $id=>$clss) if ($clss==2) $id_goods[]=$id ; else $id_section[]=$id ;
      //damp_array($id_section) ;
      //damp_array($id_goods) ;

      // если по ключевому слову найдена всего одна категория, то делаем переход на эту категорию
      if (sizeof($id_section)==1)
      {   $to_url=$this->tree[$id_section[0]]->href ;
          header("HTTP/1.1 302 Moved Permanently");
          header("Location: ".$to_url);
          exit() ;
      }

      // если по ключевому слову найден всего однин товар, то делаем переход на эту товар
      if (sizeof($id_goods)==1 and 0)
      {   $goods_info=execSQL_van('select pkey,parent,obj_name,url_name,href from '.$this->table_name.' where clss=2 and pkey='.$id_goods[0]) ;
          $this->prepare_public_info($goods_info) ;
          header("HTTP/1.1 302 Moved Permanently");
          header("Location: ".$goods_info['__href']);
          exit() ;
      }

      if ($id_section) $_usl[]='parent in ('.implode(',',$id_section).')' ;
      if ($id_goods) $_usl[]='pkey in ('.implode(',',$id_goods).')' ;

  	  // подготовливает условие для поиска по стандартным полям товара
      $_usl[]=$this->prepare_search_usl('art','like%%',$text) ;
      $_usl[]=$this->prepare_search_usl('obj_name','like%%',$text) ;
      $_usl[]=$this->prepare_search_usl('pkey','=',$text) ;
      $_usl[]=$this->prepare_search_usl('manual','like%%',$text) ;
      //damp_array($_usl) ;
      $res_usl='('.implode(' or ',$_usl).')';
      if ($usl) $res_usl.=' and '.$usl ;
      //echo 'usl='.$usl.'<br>';
      //echo 'res_usl='.$res_usl.'<br>';
      //trace() ;
      if ($options['get_only_usl']) return($res_usl) ; //echo $usl.'<br>' ;
      $list_obj=$this->get_items($res_usl,$options) ;
      if (sizeof($list_obj)==1)
      { list($id,$rec)=each($list_obj) ;
        header("HTTP/1.1 302 Moved Permanently");
        header("Location: ".$rec['__href']);
      }
      return($list_obj) ;
    }



}








?>