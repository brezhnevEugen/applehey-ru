// поиск по сайту - доработано с целью перехвата событий нажания enter и клика по submit
$j.fn.search_lite = function(options)
{  var input=$j(this).find('input[type="text"]') ;
  var button=$j(this).find('.submit') ;

  var settings = $j.extend(
       {  'action' : '/search/'
       }, options);

  $j(button).live('click',go_search) ; // поиск по клику на кнопку
  $j(input).live('keydown',function(e){if(e.keyCode==13)go_search() ;}) ; // поиск по нажатию на enter
  function go_search() { document.location=settings['action']+$j(input).val(); }
       };

