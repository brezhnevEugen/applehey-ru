<?php
/*
прописать в htaccess
RewriteRule price.xls price_to_excel.php

файл  /price_to_excel.php
<?
 include ("ini/patch.php");
 include (_DIR_TO_ENGINE."/i_system.php") ;
 $options['ext']='price_to_excel' ;
 show_page($options) ;
?>

если необходимо использовать другой адрес,задать опции:
    $options['ext']='price_to_excel' ;
    $options['class_file']='c_price_to_excel.php' ;
    $options['class_name']='c_page_price_to_excel' ;


 function panel_price_list()
    {
       ?><div id=panel_price_list><a href="/price.xls">Скачать прайс-лист</a></div><?
    }

#block_top div#panel_price_list{position:absolute;top:70px;right:00px;width:185px;height:25px;padding:15px 0 0 45px;background:url('/images/excel2007.png') no-repeat left center;}
#block_top div#panel_price_list a{color:black;text-decoration:underline;}
#block_top div#panel_price_list a:hover{color:#b12448;text-decoration:underline;}



 */


class c_page_price_to_excel //extends c_base
{

 function check_location() {}
 function on_send_header_before() {}
 function command_execute() {}
 function select_obj_info() {}
 function autogenerate_meta_tags() {}
 function site_top_head() {}

 public $first_col_char=65 ; // A
 public $first_row=1 ; // A

 public $cur_col=65 ; // A
 public $cur_row=1 ; // A

 public $file_name='price.xls' ;
 public $usl_select_goods='act=0' ;


 public $fields=array
 (  //'URL'=>'Товар на сайте',
    //'pkey'=>'Код товара',
    'art'=>'Артикул',
    'obj_name'=>'Наименование',
    //'price_site'=>'Цена'
    'price'=>'Цена'
    //'stock'=>'Примечание'
 ) ;

 function show() //
 {    global $goods_system ;
      set_include_path(_DIR_EXT.'/PHPExcel/');
      include_once(_DIR_EXT.'/PHPExcel/PHPExcel.php');
      include_once(_DIR_EXT.'/PHPExcel/PHPExcel/IOFactory.php');

     $this->file_name=basename($_SERVER['REQUEST_URI']) ;

      $pExcel = new PHPExcel();
      $pExcel->setActiveSheetIndex(0);
      $aSheet = $pExcel->getActiveSheet();
      $aSheet->setTitle('Прайс-лист '._MAIN_DOMAIN);
      //настройки для шрифтов
      $boldFont = array('font'=>array('name'=>'Arial Cyr','size'=>'10','bold'=>true));
      $center = array('alignment'=>array('horizontal'=>PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'=>PHPExcel_Style_Alignment::VERTICAL_TOP));

      $cur_col_char=$this->first_col_char ;

      if (sizeof($this->fields)) foreach($this->fields as $fname=>$title)
      { $col_char=iconv('windows-1251','UTF-8',chr($cur_col_char)) ; //echo $col_char.'-' ;
        //$col_char=chr($cur_col_char) ; //echo $col_char.'-' ;
        $aSheet->setCellValue($col_char.$this->cur_row,$title); $cur_col_char++ ;
        $aSheet->getStyle($col_char.$this->cur_row)->applyFromArray($boldFont)->applyFromArray($center);
      }

      $this->cur_row++ ;
      $GLOBALS['EE_first_col_char']=$this->first_col_char ;
      $GLOBALS['EE_cur_row']=$this->cur_row ;
      $GLOBALS['EE_fields']=$this->fields ;
      $GLOBALS['aSheet']=$aSheet ;

      $goods_system->tree['root']->exec_func_recursive('export_rec_to_xls') ;

       //damp_array($goods_system,1,-1) ; return ;
       //damp_array($goods_system->tree['root'],1,-1) ; return ;
       //echo sys_get_temp_dir() ;
       //damp_array($_ENV) ; return ;
       //damp_array($_SERVER) ; return ;
      //отдаем пользователю в браузер

      include("PHPExcel/Writer/Excel5.php");
      $objWriter = new PHPExcel_Writer_Excel5($pExcel);
      header('Content-Type: application/vnd.ms-excel;name="'.$this->file_name.'"');
      header('Content-Disposition: attachment;filename="'.$this->file_name.'"');
      header('Cache-Control: max-age=0');
      $objWriter->save('php://output');

 }
}


// выдача товаров раздела
 function export_rec_to_xls($pkey)
   { global $goods_system ;
     // название раздела
     $titleFont = array('font'=>array('name'=>'Arial Cyr','size'=>'12','bold'=>true));
     //$value=iconv('windows-1251','UTF-8',$goods_system->tree[$pkey]->name) ; //echo $col_char.'-' ;
     $value=$goods_system->tree[$pkey]->name ; //echo $col_char.'-' ;
     $GLOBALS['aSheet']->setCellValueByColumnAndRow(1,$GLOBALS['EE_cur_row'],$value);
     $GLOBALS['aSheet']->getStyle('B'.$GLOBALS['EE_cur_row'])->applyFromArray($titleFont);
     $GLOBALS['EE_cur_row']++ ;

     $goods_system->show_list_items($pkey,'price_to_excel/list_goods_price_xls',array())  ;
   }

?>
