<?
  // шаблон - список статей: наименование-ссылка, аннотация
  function list_goods_price_xls(&$list_recs,$options=array())
  { global $goods_system ;
    //$aSheet=$options['']
    if (sizeof($list_recs)) foreach($list_recs as $rec)
     {  $cur_col_char=$GLOBALS['EE_first_col_char'] ;  $cur_col=0 ;
        $goods_system->prepare_public_info($rec) ;
        if (sizeof($GLOBALS['EE_fields'])) foreach($GLOBALS['EE_fields'] as $fname=>$title)
        {  $value='' ;
           $no_set_value=0 ;
           $col_char=iconv('windows-1251','UTF-8',chr($cur_col_char)) ; //echo $col_char.'-' ;

           switch($fname)
           { case 'parent': $parent_id_obj=$goods_system->tree[$rec['parent']]->get_parent_by_clss(10) ;
                            $value=strip_tags($goods_system->tree[$parent_id_obj]->name) ;
                            $GLOBALS['aSheet']->getColumnDimension($col_char)->setWidth(40); //
                            break ;
             case 'brand':  $brand_id_obj=$goods_system->tree[$rec['parent']]->get_parent_by_clss(201) ;
                            $value=strip_tags($goods_system->tree[$brand_id_obj]->name) ;
                            $GLOBALS['aSheet']->getColumnDimension($col_char)->setWidth(40);
                            break ;
             case 'image':  $url=img_clone($rec,'400') ;
                            $Hyperlink = new PHPExcel_Cell_Hyperlink($url,$url);
                            $Hyperlink->setUrl($url) ;
                            $Hyperlink->setTooltip($url);
                            $GLOBALS['aSheet']->setCellValueByColumnAndRow($cur_col,$GLOBALS['EE_cur_row'],$url)->setHyperlink($Hyperlink) ;
                            $no_set_value=1 ;
                            $GLOBALS['aSheet']->getColumnDimension($col_char)->setWidth(80);
                            break ;
             case 'val':    $value=$_SESSION['VL_arr'][$rec[$fname]]['YA']; break ;
             case 'price':  $value=($rec[$fname]>0)? _format_price($rec['__price_4']):'';
                            break ;
             case 'stock':  //$value=($rec[$fname])? 'Есть':''; break ;
                            if (!$rec['__stock_info'] and $rec['waiting']) $value='Ожидается поступление' ;
                            else if  (!$rec['__stock_info'] and !$rec['waiting']) $value='Под заказ';
                            else $value='' ;
                            break ;

             case 'enabled':$value=($rec[$fname])? 'Вкл.':'Выкл.'; break ;
             case 'obj_name':$value=($rec[$fname])? $rec[$fname]:$goods_system->tree[$rec['parent']]->name.' # '.$rec['pkey'] ;
                            $GLOBALS['aSheet']->getColumnDimension($col_char)->setWidth(100);
                            break ;
             case 'ya':     $value=($rec[$fname])? 'Да':'';
                            break ;
             case 'URL':    $url='http:/'._MAIN_DOMAIN.$rec['__href'] ;
                            //$value=$url ;
                            $Hyperlink = new PHPExcel_Cell_Hyperlink($url,$url);
                            $Hyperlink->setUrl($url) ;
                            $Hyperlink->setTooltip($url);
                            //$GLOBALS['aSheet']->getCell($col_char.$GLOBALS['EE_cur_row'])->setValue($url)->setHyperlink($Hyperlink) ;
                            $GLOBALS['aSheet']->setCellValueByColumnAndRow($cur_col,$GLOBALS['EE_cur_row'],'Перейти')->setHyperlink($Hyperlink) ;
                            $GLOBALS['aSheet']->getColumnDimension($col_char)->setWidth(20);
                            $no_set_value=1 ;
                            break ;

             default:       $value=$rec[$fname] ;
           }
           $value=strip_tags($value) ;
           //$value=iconv('windows-1251','UTF-8',$value) ; //echo $col_char.'-' ;

           //if (!$no_set_value) $GLOBALS['aSheet']->setCellValue($col_char.$GLOBALS['EE_cur_row'],$value);
           if (!$no_set_value) $GLOBALS['aSheet']->setCellValueByColumnAndRow($cur_col,$GLOBALS['EE_cur_row'],$value);
           $cur_col_char++ ;
           $cur_col++ ;
           if ($cur_col_char>90) break ;
     }
        //print_r($rec) ; break ;
        /*
        if ($_POST['export_fileds']['files'] and sizeof($arr2_files[$rec['pkey']]))
        foreach($arr2_files[$rec['pkey']] as $rec_file)
        {  $col_char=iconv('windows-1251','UTF-8',chr($cur_col_char)) ; //echo $col_char.'-' ;
           $url=_DOT(83)->patch_to_file.$rec_file['file_name'] ;
           $Hyperlink = new PHPExcel_Cell_Hyperlink($url,$url);
           $Hyperlink->setUrl($url) ;
           $Hyperlink->setTooltip($url);
           //$GLOBALS['aSheet']->getCell($col_char.$GLOBALS['EE_cur_row'])->setValue($url)->setHyperlink($Hyperlink) ;
           $GLOBALS['aSheet']->setCellValueByColumnAndRow($cur_col,$GLOBALS['EE_cur_row'],$url)->setHyperlink($Hyperlink) ;
           $GLOBALS['aSheet']->getColumnDimension($col_char)->setWidth(80);
           $cur_col_char++ ;
           $cur_col++ ;
           if ($cur_col_char>90) break ;
        }
        */

       $GLOBALS['EE_cur_row']++ ; //echo '<br>' ;


    }
  }
?>