<?php
/**
 * @package canvasio3D
 * @version 1.1.8
 */
/*
Plugin Name: canvasio3D Basic
Plugin URI: http://www.virtuellwerk.de/canvasio3D_en
Description: WebGL 3D-Plugin for WordPress from virtuellwerk.de - Restrictions in Canvasio3D Basic are: Object window size is maximum 320 width / 320 height and one 3D-Model per side. - Documentation in: <a href="http://www.virtuellwerk.de/pub/doc/canvasio3D/english/" target="_blank">Englisch</a> and <a href="http://www.virtuellwerk.de/pub/doc/canvasio3D/deutsch/" target="_blank">German</a>
Author: Thomas Scholl
Version: 1.1.8
Author URI: http://www.virtuellwerk.de
*/

$canvasioId = 0;
//
add_filter('upload_mimes', 'custom_upload_mimes');
//
include('inc/admin.inc.php');
include('inc/init.inc.php');
//
function custom_upload_mimes ( $existing_mimes=array() ) {
	$existing_mimes['js'] = 'application/javascript';
	$existing_mimes['stl'] = 'application/octet-stream';
	$existing_mimes['obj'] = 'application/octet-stream';
	$existing_mimes['dae'] = 'application/octet-stream';
	$existing_mimes['mtl'] = 'application/octet-stream';
	$existing_mimes['kml'] = 'application/xml';
	return $existing_mimes;
}
?>