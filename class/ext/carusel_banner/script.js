$j.fn.carusel_banner = function(options)
  { $j(this).wrapInner('<div class=carusel><div class=content></div></div>') ;
    // $j(this).append('<div class=to_prev></div>') ;
    // $j(this).append('<div class=to_next></div>') ;

    var banner=this ;
    var div_content=$j(this).find('div.content') ;

    var settings = $j.extend(
             {  'speed' : '500',
                'step' : '100',
                'interval' : 5000
             }, options);


    function start() { $j(banner).everyTime(settings['interval'],'ban_timer',next); }
    function stop() { $j(banner).stopTime('ban_timer'); }



    function to_next_on_click() { stop() ; next() ;}
    function to_prev_on_click() { stop() ; prev() ;}

    $j(banner).find('div.to_next').live('click',to_next_on_click) ;
    $j(banner).find('div.to_prev').live('click',to_prev_on_click) ;
    start() ;


    function next()
     {  // дублируем первый элемент в конец списка, после окончания анимации удаляем первый элемент из начала списка и возвращаем div_content в 0
        var first_item=$j(div_content).find('div.item:first-child') ;
        var last_item=$j(div_content).find('div.item:last-child') ;
        $j(first_item).clone().insertAfter(last_item) ;
        $j(div_content).animate({left:"-="+settings['step']},settings['speed'],'swing',function()
        { $j(div_content).find('div.item:first-child').remove() ;
          $j(div_content).css('left',0);
        }) ;
     }

    function prev()
     {  // дублируем последний элемент в конец списка одновременно двигая div_content влево, после окончания анимации удаляем последний элемент из конца списка
        var first_item=$j(div_content).find('div.item:first-child') ;
        var last_item=$j(div_content).find('div.item:last-child') ;
        $j(div_content).css('left','-'+settings['step']+'px');
        $j(last_item).clone().insertBefore(first_item) ;
        $j(div_content).animate({left:"+="+settings['step']},settings['speed'],'swing',function()
        { $j(div_content).find('div.item:last-child').remove() ;

        }) ;
     }

  } ;