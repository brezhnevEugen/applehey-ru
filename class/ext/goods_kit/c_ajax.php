<?
include(_DIR_TO_ENGINE.'/c_page_XML_AJAX.php') ;
class c_page_ajax extends c_page_XML_AJAX
{

  function panel_props($doc,$xml)
  { global $goods_system ;
    add_element($doc,$xml,'title','Товары набора') ;
    add_element($doc,$xml,'modal_panel_width','600px') ;
    //add_element($doc,$xml,'result_status',get_result_type($result)) ;
    //damp_array($_POST) ;
    // получаем инфу по набору
    $rec_kit=select_db_obj_info(2325,$_POST['kit_id']) ;
    //damp_array($rec_kit) ;
    // получаем инфу по всей одежде из комплектов
    $recs_goods=select_reffers_objs($_SESSION['TM_goods_kit_item'],'parent='.$_POST['kit_id']) ;
    $goods_kit=$recs_goods[2325][200] ;
    $goods_system->prepare_public_info_for_arr($goods_kit) ;
    ?><div id=panel_props><form>
        <div class=main_img><img src="<?echo img_clone($rec_kit,'313')?>"></div>
        <h2>Добавить новые товары в набор</h2>
        Артикул товара: <input type="text" value="" class="text v2 goods_search" event=keyup ext=goods_kit cmd=search_goods_by_art success=search_goods_by_art_success  kit_id="<?echo $_POST['kit_id']?>">
        <div class=search_result></div>
        <div class=goods_of_kit><? $this->list_kit_goods_icons($goods_kit,array('show_delete_check'=>1)) ;?></div>
        <div class=clear></div>
        <script type="text/javascript">
              $j(document).ready(function()
                {   //  клик по изображению товара набора - выделяем рамкой и показываем положение на картинке
                    $j('div#panel_props div.goods_of_kit img').live('click',function()
                    { $j('div#panel_props div.goods_of_kit div.item').removeClass('checked') ;
                      $j('div#panel_props div.goods_of_kit div.item').find('input.is_check').attr('disabled',true) ;
                      $j(this).closest('div.item').addClass('checked');
                      $j(this).closest('div.item').find('input.is_check').attr('disabled',false);
                      var item_id=$j(this).closest('div.item').attr('item_id') ;
                      $j('div#panel_props div.main_img div.marker').removeClass('active') ;
                      $j('div#panel_props div.main_img div.marker[item_id="'+item_id+'"]').addClass('active') ;
                    }) ;

                    // клик по маркеру - выделяем рамкой товар маркета
                    $j('div#panel_props div.main_img div.marker').live('click',function()
                    {  var item_id=$j(this).attr('item_id') ;
                       $j('div#panel_props div.goods_of_kit div.item').removeClass('checked') ;
                       $j('div#panel_props div.goods_of_kit div.item[item_id="'+item_id+'"]').addClass('checked') ;
                       $j('div#panel_props div.main_img div.marker').removeClass('active') ;
                       $j(this).addClass('active') ;
                    }) ;

                    // наводим мышь на по маркер - выделяем рамкой товар маркета
                    $j('div#panel_props div.main_img div.marker').live('mouseover',function()
                    {  var item_id=$j(this).attr('item_id') ;
                       if (!$j('div#panel_props div.goods_of_kit div.item[item_id="'+item_id+'"]').hasClass('checked'))
                        { $j('div#panel_props div.goods_of_kit div.item').removeClass('checked') ;
                          $j('div#panel_props div.goods_of_kit div.item[item_id="'+item_id+'"]').addClass('checked') ;
                          $j('div#panel_props div.main_img div.marker').removeClass('active') ;
                          $j(this).addClass('active') ;
                        }
                    }) ;
                }) ;

              // обработка ответа AJAX по операции "удаление товара из набора"
              function delete_goods_from_kit_success(data)
              {
                $j('div.goods_of_kit').html(data.html) ; // обновляем список товара
              }

              // обработка ответа AJAX по операции "добавление товара в набора"
              function add_goods_to_kit_success(data)
              { $j('div.goods_of_kit').html(data.html) ;
                $j('div.search_result').html('') ;
                $j('input.goods_search').val('') ;
              }

              // обработка ответа AJAX по операции "поиск товара"
              function search_goods_by_art_success(data)
              { if (data.cnt) { $j('div#panel_props div.goods_of_kit').html(data.html) ; $j('div#panel_props div.search_result').html('Найдено: '+data.cnt+' товаров') ; }
                else          { $j('div#panel_props div.goods_of_kit').html(data.html) ; $j('div#panel_props div.search_result').html('') ; }
              }
          </script>
          <style type="text/css">
                 div#panel_props div.main_img{float:right;width:313px;height:674px;position:relative;}
                 div#panel_props div.main_img img{border:1px solid #eeeeee;}
                 div#panel_props div.main_img div.marker{width: 15px;height: 15px;background: url('/images/control2.png') no-repeat -274px -335px;position: absolute;cursor: pointer;}
                 div#panel_props div.main_img div.marker:hover{background: url('/images/control2.png') no-repeat -299px -335px;}
                 div#panel_props div.main_img div.marker.active{background: url('/images/control2.png') no-repeat -299px -335px;}
                 div#panel_props div.goods_of_kit{float:left;width:270px;}
                 div#panel_props div.list_kit_goods_icons div.item{float:left;margin:10px;position:relative;border:1px solid #eeeeee;text-align:center;padding:5px;width:100px;}
                 div#panel_props div.list_kit_goods_icons div.item img{margin:5px 0;}
                 div#panel_props div.list_kit_goods_icons div.item div.panel_info{padding:10px;background:white;position:absolute;top:-1px;left:110px;display:none;width:100px;z-index:100;text-align:left;}
                 div#panel_props div.list_kit_goods_icons div.item div.panel_info > div{margin:5px 0;}
                 div#panel_props div.list_kit_goods_icons div.item div.panel_info div.checked{color:black;}
                 div#panel_props div.list_kit_goods_icons div.item:hover{border:1px solid rgba(0, 0, 0, 0.4);-webkit-box-shadow:0 0 15px rgba(0, 0, 0, 0.35);cursor:pointer;}
                 div#panel_props div.list_kit_goods_icons div.item:hover div.panel_info{display:block;border:1px solid rgba(0, 0, 0, 0.4);-webkit-box-shadow:0 0 15px rgba(0, 0, 0, 0.35);cursor:pointer;border-left:none;}
                 div#panel_props div.list_kit_goods_icons div.item.checked{border:1px solid #98cb38;
                     -webkit-box-shadow: 0px 0px 12px rgba(0, 255, 0, 0.73);
                     -moz-box-shadow:    0px 0px 12px rgba(0, 255, 0, 0.73);
                     box-shadow:         0px 0px 12px rgba(0, 255, 0, 0.73);
                 }
                 div#panel_props div.list_kit_goods_icons div.item.checked:hover{border:1px solid rosybrown;}
                 div#panel_props div.search_result {margin:10px 0;font-weight:bold;font-style:italic;}
          </style>
      </form></div>
    <?
    //damp_array($rec) ;
  }

  function search_goods_by_art($doc,$xml)
  { global $goods_system ;
    //damp_array($_POST) ;
    if ($_POST['value'] and strlen($_POST['value'])>=3)
    { $list_goods=select_db_recs('obj_site_goods','art like "%'.$_POST['value'].'"') ;
      $goods_system->prepare_public_info_for_arr($list_goods) ;
      $this->list_kit_goods_icons($list_goods,array('show_adding_check'=>1)) ;
      add_element($doc,$xml,'cnt',sizeof($list_goods)) ;
    }
    else
    { $recs_goods=select_reffers_objs($_SESSION['TM_goods_kit_item'],'parent='.$_POST['kit_id']) ;
      $goods_kit=$recs_goods[2325][200] ;
      $goods_system->prepare_public_info_for_arr($goods_kit) ;
      $this->list_kit_goods_icons($goods_kit,array('show_delete_check'=>1)) ;
      add_element($doc,$xml,'cnt',0) ;
    }
  }

  function delete_goods_from_kit($doc,$xml)
  { global $goods_system ;
    // удалем отмеченные товары из набора стандартнми старедствами движка
    include_once(_DIR_TO_ENGINE.'/admin/i_admin_system.php') ;
    cmd_delete_obj($_POST['_check']) ;
    $recs_goods=select_reffers_objs($_SESSION['TM_goods_kit_item'],'parent='.$_POST['kit_id']) ;
    $goods_kit=$recs_goods[2325][200] ;
    $goods_system->prepare_public_info_for_arr($goods_kit) ;
    $this->list_kit_goods_icons($goods_kit,array('show_delete_check'=>1)) ;
    add_element($doc,$xml,'cnt',sizeof($goods_kit)) ;
  }

  function add_goods_to_kit($doc,$xml)
  { global $goods_system ;
    //damp_array($_POST) ;
    $recs=array() ;
    // все товары kit
    $recs_goods=select_reffers_objs($_SESSION['TM_goods_kit_item'],'parent='.$_POST['kit_id']) ;
    $goods_kit=$recs_goods[2325][200] ;
    $goods_system->prepare_public_info_for_arr($goods_kit) ;
    // получаем инфу по товарам
    if (sizeof($_POST['_check'])) $recs=execSQL('select * from obj_site_goods where pkey in ('.implode(',',$_POST['_check']).')') ;
    // добавляем товары в kit
    if (sizeof($recs)) foreach($recs as $rec) if (!isset($goods_kit[$rec['pkey']])) adding_rec_to_table($_SESSION['TM_goods_kit_item'],array('obj_name'=>$rec['obj_name'],'clss'=>39,'parent'=>$_POST['kit_id'],'reffer'=>$rec['_reffer'],'cnt'=>1),array('debug'=>0)) ;
    // выводим товары списком
    $recs_goods=select_reffers_objs($_SESSION['TM_goods_kit_item'],'parent='.$_POST['kit_id']) ;
    $goods_kit=$recs_goods[2325][200] ;
    $goods_system->prepare_public_info_for_arr($goods_kit) ;
    $this->list_kit_goods_icons($goods_kit,array('show_delete_check'=>1)) ;
    add_element($doc,$xml,'cnt',sizeof($goods_kit)) ;
  }

  function set_marker_pos($doc,$xml)
  { if ($_POST['item_id']) execSQL_update('update '.$_SESSION['TM_goods_kit_item'].' set x="'.$_POST['x'].'",y="'.$_POST['y'].'" where pkey='.$_POST['item_id']) ;

  }

  function list_kit_goods_icons($list_recs,$options=array())
  { // удаляем все маркеры товаров с изоражения товара
    ?><script type="text/javascript">$j('div#panel_props div.main_img div.marker').remove()</script><?
    ?><div class=list_kit_goods_icons><?
      if (sizeof($list_recs)) foreach($list_recs as $rec)
        {   $tkey=$rec['__reffer_from']['tkey'] ;
            $clss=$rec['__reffer_from']['clss'] ;
            $pkey=$rec['__reffer_from']['pkey'] ;
            $x=$rec['__reffer_from']['x'] ;
            $y=$rec['__reffer_from']['y'] ;
            ?><div class="item  <?if (sizeof($list_recs)==1 and $options['show_adding_check']) echo 'checked'?>" item_id=<?echo $pkey?>>
                <img src="<?echo img_clone($rec,'100')?>">
                <div class=panel_info>
                    <div class=art>aрт.<?echo $rec['art']?></div>
                    <div class=name><?echo $rec['__name']?></div>
                    <div class=price><?echo $rec['__price_5']?></div>
                    <?if ($options['show_delete_check']){?><input class=is_check name="_check[<?echo $tkey?>][<?echo $clss?>][<?echo $pkey?>]" type="hidden" value="17330" disabled><?}?>
                    <?if ($options['show_adding_check']){?><input class=is_check  name="_check[]" type="hidden" value="<?echo $rec['pkey']?>" <?if (sizeof($list_recs)!=1) echo 'disabled'?>><?}?>
                </div>
            </div>
           <?
           // каждый товар набора добавляет на фото свою метку
           if ($options['show_delete_check']) {?><script type="text/javascript">$j('div#panel_props div.main_img').append('<div class=marker item_id=<?echo $pkey?> style="left:<?echo $x?>px;top:<?echo $y?>px;"></div>')</script><?}
        }
     ?><div class=clear></div><?
     if (sizeof($list_recs))
     { if ($options['show_adding_check']) {?><button class="v2" event=click ext=goods_kit cmd=add_goods_to_kit success=add_goods_to_kit_success kit_id="<?echo $_POST['kit_id']?>">Добавить отмеченные товары в набор</button><?}
       if ($options['show_delete_check']) {?><button class="v2" event=click ext=goods_kit cmd=delete_goods_from_kit success=delete_goods_from_kit_success kit_id="<?echo $_POST['kit_id']?>">Удалить отмеченные товары из набора</button><?}
     }
     ?></div>
       <script type="text/javascript">
           $j('div#panel_props div.main_img div.marker').draggable(
              {containment: 'parent',
               stop:function(e,ui)
               {$j.ajax({url:'ajax.php',type:'POST',dataType:'xml',cache:false,data:{ext:'goods_kit',cmd:'set_marker_pos',item_id:ui.helper.attr('item_id'),x:ui.position.left,y:ui.position.top}});}
              }
           );
       </script>
     <?
     //damp_array($rec['__reffer_from']) ;
  }

}
