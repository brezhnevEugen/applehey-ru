<?

$__functions['init'][]		='_goodskit_props_vars' ;
//$__functions['install'][]	='_goods_install_modul' ;

function _goodskit_props_vars()
{
    $_SESSION['TM_goods_kit_item']='obj_'.SITE_CODE.'_goods_kit' ;

    //
    $_SESSION['ARR_kit_type_sales']=array('1'=>'Скидка в процентах','2'=>'Значение скидки') ;

    // Товарный набор
    $_SESSION['descr_clss'][38]['name']='Товарный набор' ;
   	$_SESSION['descr_clss'][38]['parent']=10 ;
   	$_SESSION['descr_clss'][38]['parent_to']=array(3,5,39) ;
    $_SESSION['descr_clss'][38]['fields']['art']='varchar(255)' ; // артикул
    $_SESSION['descr_clss'][38]['fields']['intro']='varchar(255)' ; // краткое описание
    $_SESSION['descr_clss'][38]['fields']['manual']='varchar(255)' ; // подробное описание
    $_SESSION['descr_clss'][38]['fields']['hit']='int(1)' ; // хит
    $_SESSION['descr_clss'][38]['fields']['new']='int(1)' ; // новиника
    $_SESSION['descr_clss'][38]['fields']['price']='float(10,2)' ; // цена - пересчитывается при каждом изменении в составе набора
    //$_SESSION['descr_clss'][38]['no_show_in_tree']					=1 ;

    $_SESSION['descr_clss'][38]['menu'][]=array("name" => "Состав набора",	"cmd" => 'show_kit') ;
    $_SESSION['descr_clss'][38]['menu'][]=array("name" => "Добавить", "type" => 'structure') ;
    //$_SESSION['descr_clss'][38]['menu'][]=array("name" => "Уведомление клиенту",	"cmd" => 'show_email') ;

    
    $_SESSION['descr_clss'][38]['list']['field']['enabled']=array('title'=>'Сост.') ;
    $_SESSION['descr_clss'][38]['list']['field']['art']=array('title'=>'Артикул') ;
   	$_SESSION['descr_clss'][38]['list']['field']['obj_name']=array('title'=>'Наименование') ;
   	$_SESSION['descr_clss'][38]['list']['field']['_kit_price']=array('title'=>'Цена набора','read_only'=>1) ;
   	$_SESSION['descr_clss'][38]['list']['field']['_kit_info']=array('title'=>'Содержание набора','td_class'=>'left') ;

	//Позиция товарного набора
    $_SESSION['descr_clss'][39]['name']='Позиция товарного набора' ;
	$_SESSION['descr_clss'][39]['parent']=100 ;
	$_SESSION['descr_clss'][39]['fields']['obj_name']='varchar(255)' ;
	$_SESSION['descr_clss'][39]['fields']['id']='int(11)' ; // код объекта (товара)
	$_SESSION['descr_clss'][39]['fields']['reffer']='any_object' ; // код объекта (товара)
	$_SESSION['descr_clss'][39]['fields']['cnt']='int(11)' ;
	$_SESSION['descr_clss'][39]['fields']['sales_type']=array('type'=>'indx_select','array'=>'ARR_kit_type_sales','default'=>1) ;
	$_SESSION['descr_clss'][39]['fields']['sales_value']='float' ;
	$_SESSION['descr_clss'][39]['fields']['x']=array('type'=>'int(11)','default'=>0) ;
	$_SESSION['descr_clss'][39]['fields']['y']=array('type'=>'int(11)','default'=>0) ;
    $_SESSION['descr_clss'][39]['child_to']=array(38) ;
    $_SESSION['descr_clss'][39]['parent_to']=array() ;
    $_SESSION['descr_clss'][39]['table_code']='kititem' ;

    $_SESSION['descr_clss'][39]['icons']=_PATH_TO_BASED_CLSS_IMG.'/100.png' ;

	$_SESSION['descr_clss'][39]['list']['field']['enabled']=array('title'=>'Сост.') ;
    $_SESSION['descr_clss'][39]['list']['field']['indx']=array('title'=>'Позиция') ;
    $_SESSION['descr_clss'][39]['list']['field']['']=array('title'=>'Фото','edit_element'=>'clss_100_image')  ;
	$_SESSION['descr_clss'][39]['list']['field'][]=array('title'=>'Товар','view'=>'show_goods_info') ;
	$_SESSION['descr_clss'][39]['list']['field']['cnt']=array('title'=>'Количество') ;
	$_SESSION['descr_clss'][39]['list']['field'][]=array('title'=>'Цена') ;
	$_SESSION['descr_clss'][39]['list']['field']['sales_type']=array('title'=>'Тип скидки') ;
	$_SESSION['descr_clss'][39]['list']['field']['sales_type']=array('title'=>'Размер скидки') ;
	$_SESSION['descr_clss'][39]['list']['field'][]=array('title'=>'Цена в наборе') ;
	$_SESSION['descr_clss'][39]['list']['options']=array('no_icon_edit'=>1);
}

include_once(_DIR_TO_ENGINE.'/class/clss_1.php') ;
class clss_38 extends clss_1
{   public $count_obj_by_page=1000 ; // число объектов на одной странице до подгрузки

    // подготовка url для страницы объекта
     function prepare_url_obj($rec)
     {   $url_name=substr(safe_text_to_url($rec['obj_name'],$rec['tkey']),0,32).'_'.$rec['pkey'] ;
         return($url_name) ;
     }

    // доработка списка комплектов перед выводом - получаем доп.информацию по товарам каждого комплекта и сумманой цене
    function prepare_public_info_for_arr(&$recs)
    { // получаем все товары из списка
      list($id,$rec)=each($recs) ;
      $tkey=$rec['tkey'] ;
      $tkey_kit=_DOT($tkey)->list_clss[39] ;
      $recs_goods=select_reffers_objs($tkey_kit,'parent in ('.implode(',',array_keys($recs)).')') ;
      //damp_array($recs_goods) ;
      $arr_names=array() ;$arr_price=array() ;
      if (sizeof($recs_goods[$tkey][200]))
          { foreach($recs_goods[$tkey][200] as $rec_goods)
            { $arr_names[$rec_goods['__reffer_from']['parent']][]='<li>'._list('IL_brands',$rec_goods['brand']).' '.$rec_goods['obj_name'].' (#'.$rec_goods['pkey'].')'.'</li>' ;
              $arr_price[$rec_goods['__reffer_from']['parent']][]=$rec_goods['price'] ;
            }
          }

      //damp_array($arr_names) ;
      //  damp_array($arr_price) ;
      if (sizeof($arr_names)) foreach($arr_names as $id=>$rec_names) $recs[$id]['_kit_info']='<ul>'.implode('',$rec_names).'</ul>' ;
      if (sizeof($arr_price)) foreach($arr_price as $id=>$rec_price) $recs[$id]['_kit_price']=array_sum($rec_price) ;
      //  if (sizeof($recs)) foreach($recs as $rec) list($ids[],$tkey)=explode($rec[''])

    }

    function show_kit($options)
    { //damp_array($options) ;
      $tkey_kit=_DOT($options['tkey'])->list_clss[39] ;
      //$recs_goods=select_reffers_objs($tkey_kit,'parent='.$options['pkey']) ;
      _CLSS(39)->show_list_items($tkey_kit,'parent='.$options['pkey']);
      /*
      ?><table><tr class=clss_header><td>Выбор</td><td><?
      if (sizeof($recs_goods[$options['tkey']][200])) foreach($recs_goods[$options['tkey']][200] as $rec)
        { ?><tr><td><input type="checkbox" name="_check[<?echo $rec['tkey']?>][200][<?echo $rec['pkey']?>]" value="1"></td>
                <td class=left><img src="<?echo img_clone($rec,'small')?>"></td>
                <td class=left><?echo _list('IL_brands',$rec['brand']).$rec['obj_name']?></td>
                <td class=left><?echo _format_price($rec['price'])?></td>

            </tr>
          <?

        }
      ?></table><?
      */
    }

    function show_based_obj_list($list_items,$options=array())
    { ?><script type="text/javascript" src="<?echo _PATH_TO_ADMIN_ADDONS?>/jquery-ui-1.9.2/jquery.ui.mouse.js"></script>
          <script type="text/javascript" src="<?echo _PATH_TO_ADMIN_ADDONS?>/jquery-ui-1.9.2/jquery.ui.draggable.js"></script>
          <script type="text/javascript" src="<?echo _PATH_TO_ADMIN_ADDONS?>/jquery-ui-1.9.2/jquery.ui.droppable.js"></script>
          <div class="list_kit_icons">
            <? if (sizeof($list_items)) foreach ($list_items as $rec) $this->view_obj_item($rec) ; ?>
          <div class=clear></div></div>
          <style type="text/css">
              div#panel_props{text-align:center;}
              div#panel_props h2{text-transform:uppercase;}
              div#panel_props div.add_goods_to_kit{}
              div#panel_props div.add_goods_to_kit div#search_area{}
              div.list_kit_icons div.item{float:left;margin:10px;position:relative;border:1px solid white;width:80px;height:200px;text-align:center;}
              div.list_kit_icons div.item img{margin:5px 0;}
              div.list_kit_icons div.item button{display:none;}
              div.list_kit_icons div.item:hover{border:1px solid rgba(0, 0, 0, 0.4);-webkit-box-shadow:0 0 15px rgba(0, 0, 0, 0.35);cursor:pointer;}
              div.list_kit_icons div.item:hover button{display:inline;}
          </style>
        <?
    }

    function view_obj_item(&$rec)
    {?><div class="item" reffer="<?echo $rec['_reffer']?>">
               <img class="v2" event=click cmd=panel_props ext=goods_kit success=ajax_to_modal_window kit_id="<?echo $rec['pkey']?>" src="<?echo img_clone($rec,70)?>">
               <button class="button_small v2" event=click cmd=panel_obj_img no_send_form=1 success=ajax_to_modal_window modal_panel_width="600px" parent_reffer="<?echo $rec['_reffer']?>">Фото</button>
        </div>
      <?
    }

}

include_once(_DIR_TO_ENGINE.'/class/clss_100.php') ;
class clss_39 extends clss_100
{
    function show_goods_info($rec,$options)
    {
        $res=generate_obj_text($rec['_reffer']).'<br>Цена:' ;
        return($res) ;
    }

   // доработка списка комплектов перед выводом - получаем доп.информацию по товарам каждого комплекта и сумманой цене
   function _prepare_public_info_for_arr(&$recs)
   {

   }


}



?>