Узнать архитектуру процессора
>lscpu
>uname -m

i686 = 32 bit
x86_64 = 64 bit

Установка Java

http://java.com/ru/download/manual.jsp - выбираем свой дист (Linux x64 RPM) и закачиваем на сервер через wget

Установка openfire


Обновление openfire

1. Посмотреть адрес свежей версии тут: http://www.igniterealtime.org/downloads/index.jsp
2. Зайти под рутом на сервер, загрузить свежий дист.

wget http://www.igniterealtime.org/downloadServlet?filename=openfire/openfire_3.9.1_all.deb

3. устанавиливаем обновление

dpkg -i openfire_3.9.1_all.deb
(Reading database ... 71572 files and directories currently installed.)
Preparing to replace openfire 3.8.2 (using openfire_3.9.1_all.deb) ...
Stopping openfire: openfire.
Unpacking replacement openfire ...
Setting up openfire (3.9.1) ...
Installing new version of config file /etc/init.d/openfire ...
Starting openfire: openfire.


Настройка openfire:
    -------
    Server -> Server Setting -> Resource Police -> Conflict Policy -> Always kick - If there is a resource conflict, immediately kick the other resource.

    Необходимо установить, чтобы каждое новое соединение клиента с прежним ресурсом убивало старое соединение (например, при обновленении страницы)
    Иначе, после обновления страницы, сервер не будет некоторое время передавать сообщения на обновленую страницу, так как у него будет ожидаение связи со старой страницей.

    Server -> System Propertis -> route.all-resources = true
    удалить параметры             xmpp.httpbind.worker.threads
                                  xmpp.client.processing.threads

    Установить плагин user service. После установки необходимо запустить плагин Server -> Server Setting -> User Service -> Enabled


    Перезагрузка service openfire restart

Проверка открытости портп

> telnet 217.65.2.160 9090

Открытие портов на сервере

> iptables -I INPUT -p tcp --dport 9090 -j ACCEPT
> iptables -I INPUT -p tcp --dport 5222 -j ACCEPT
> iptables -I INPUT -p tcp --dport 5223 -j ACCEPT
> iptables -I INPUT -p tcp --dport 9091 -j ACCEPT
> iptables -I INPUT -p tcp --dport 7070 -j ACCEPT
> iptables -I INPUT -p tcp --dport 7443 -j ACCEPT

В apache2.conf прописать

# XMPP proxy rule
ProxyRequests Off
ProxyPass /xmpp-httpbind http://62.152.56.20:7070/http-bind/
ProxyPassReverse /xmpp-httpbind http://62.152.56.20:7070/http-bind/

и проверить наличие ссылок на модули в папке /etc/apache2/mods-enabled

@proxy.conf
@proxy.load
@proxy_http.load

проверка конфигурации: apache2ctl configtest
перезапуск апача: apache2ctl restart



