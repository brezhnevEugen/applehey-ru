var BOSH_SERVICE = "http://62.152.56.20/xmpp-httpbind" ;
var connection = null;


$j.fn.panel_chat = function(options)
  { var panel_chat=this ;
    var panel_messages=$j(panel_chat).find('div#panel_chat_messages')  ;
    var text_area=$j(panel_chat).find('textarea#client_mess')  ;
    var panel_title=$j(panel_chat).find('div#panel_chat_title')  ;

    $j(panel_chat).find('div.title').live('click',toggle_open) ;
    $j(panel_chat).find('div.close').live('click',toggle_open) ;

    var settings = $j.extend(
               {  'auto_open' : 0,
                  'uid' : 0,
                  'login' : '',
                  'resurce' : '',
                  'pass' : '12345',
                  'debug' : 0
               }, options);

    if (settings.debug)
    { $j(panel_chat).after('<div id="chat_debug_info"><div class="info"></div><br><strong>Отладка AJAX:</strong><textarea></textarea></div>') ;
      show_debug_info('init') ;
    }

    //$j(panel_chat).live('click',show_panel_chat_if_is_closed) ;

    $j(panel_chat).find('button#send').click(chat_send_mess);
    $j(text_area).live('keydown',function(e)
    {
        if(e.keyCode==13&&!e.shiftKey){chat_send_mess() ;return(false)}
    }) ; // отправка по нажатию на enter


    if (panel_messages[0]!=undefined) panel_messages.animate({scrollTop:panel_messages[0].scrollHeight-panel_messages[0].clientHeight});

    //$j(panel_chat).everyTime(5000,'panel_chat_timer',check_connection);
    //$j(window).focus(check_connection) ;

    $j(text_area).focus(check_connection) ;
    check_connection() ;

    //if (settings.auto_open)  $j(panel_chat).oneTime(settings.auto_open,'auto_open_timer',show_panel_chat_if_is_closed);

    //var mySound = new buzz.sound( "/sounds/myfile", {formats: [ "ogg", "mp3", "aac" ]});
    var mySound_responses = new buzz.sound("/class/ext/chat/sounds/sounds-958-thin.mp3");
    var mySound_send_mess = new buzz.sound("/class/ext/chat/sounds/sounds-1049-knob.mp3");

    function show_debug_info(title,data)
    { var debug='UID='+settings.uid+'<br>' ;
      debug+='login='+settings.login+'<br>' ;
      debug+='resurce='+settings.resurce+'<br>' ;
      $j('div#chat_debug_info div.info').html(debug) ;
      var d = new Date();
      //var data_str=htmlspecialchars(JSON.stringify(data, null, 4)) ;
      var data_str=htmlspecialchars(print_r(data,true)) ;
      debug=d.toUTCString()+" - "+title+"\n\n"+data_str+"\n----------------------------------------------------------------------------------\n" ;
      var textarea=$j('div#chat_debug_info textarea') ;
      $j(textarea).append(debug) ;
      $j(textarea).animate({scrollTop:textarea[0].scrollHeight-textarea[0].clientHeight});
    }

    function chat_send_mess()
      { var params= {} ;
        params['cmd']='chat/send_mess' ;
        params['chat_uid']=settings.uid ;
        params['time']=$j(panel_messages).find('div.item:last-child').attr('time') ;
        params['message']=$j(text_area).val() ;
        params['screen_width']=screen.width ;
        params['screen_height']=screen.height ;
        $j(text_area).val('') ;

        $j.ajax({url:'/ajax.php',type:'POST',dataType:'xml',cache:false,success:chat_send_mess_success,data:params});
        if (settings.debug)  show_debug_info('chat_send_mess',params) ;

        mySound_send_mess.play();
      }

    function chat_update()
      { var params= {} ;
        params['cmd']='chat/update' ;
        params['chat_uid']=settings.uid ;
        params['time']=$j(panel_messages).find('div.item:last-child').attr('time') ;
        $j.ajax({url:'/ajax.php',type:'POST',dataType:'xml',cache:false,success:chat_update_success,data:params});
        if (settings.debug)  show_debug_info('chat_update',params) ;
      }


    function check_connection()
    { if (settings.login)
      { if (connection==null) BOSH_Connection() ;
        else if (connection.authenticated==false) BOSH_Connection() ;
      }
    }

    function chat_send_mess_success(data,status,link)
    {   if (status=='success')
           {  var responseXML = link.responseXML;
              var data=convert_XML_to_obj(responseXML) ;
              if (data.uid)        settings.uid=data.uid ;
              if (data.jid)        settings.login=data.jid ;
              if (data.chat_title) panel_title.html(data.chat_title) ;
              if (data.messages)   { panel_messages.append(data.messages) ;
                                     panel_messages.animate({scrollTop:panel_messages[0].scrollHeight-panel_messages[0].clientHeight});
                                   }
              check_connection() ;
              $j(text_area).val('') ;
              if (settings.debug)  show_debug_info('chat_send_mess_success',data) ;
           }
    }

    function chat_update_success(data,status,link)
    {   if (status=='success')
           {  var responseXML = link.responseXML;
              var data=convert_XML_to_obj(responseXML) ;
              if (data.chat_title) panel_title.html(data.chat_title) ;
              if (data.messages)   { mySound_responses.play();
                                     panel_messages.append(data.messages) ;
                                     panel_messages.animate({scrollTop:panel_messages[0].scrollHeight-panel_messages[0].clientHeight});
                                   }
              if (data.go_url)     { mySound_responses.play();
                                     document.location=data.go_url ;
                                   }
           }
        if (settings.debug)  show_debug_info('chat_update_success',data) ;

    }
     /*
    function show_panel_chat_if_is_closed()
    { if ($j(panel_chat).hasClass('open')) return ;
      $j(panel_chat).die('click') ;
      $j(panel_chat).addClass('open') ;
      var params={cmd:'chat/open_chat_panel',uid:settings.uid} ;
      $j.ajax({url:'/ajax.php',type:'POST',dataType:'xml',cache:false,success:chat_update_success,data:params});
      if (settings.debug)  show_debug_info('show_panel_chat_if_is_closed',params) ;
    }  */

    /*
    function panel_chat_close_up()
    { $j(panel_chat).removeClass('open') ;
      $j(panel_chat).live('click',show_panel_chat_if_is_closed) ;
      var params={cmd:'chat/close_chat_panel',uid:settings.uid} ;
      $j.ajax({url:'/ajax.php',type:'POST',dataType:'xml',cache:false,data:params});
      if (settings.debug)  show_debug_info('show_panel_chat_if_is_closed',params) ;
      return(false) ;
    } */

     function  toggle_open()
     { if ($j(panel_chat).hasClass('open')) $j(panel_chat).removeClass('open',500,function(){send_ajax_request({cmd:'chat/set_chat_panel',status:'close'})})  ;
       else                                 $j(panel_chat).addClass('open',500,function(){send_ajax_request({cmd:'chat/set_chat_panel',status:'open'})})  ;
       /*
       $j(panel_chat).toggleClass('open',0,function()
       {  var status=($j(panel_chat).hasClass('open'))? 'open':'close' ;
          send_ajax_request({cmd:'chat/set_chat_panel',status:status}) ;
       }) ;*/
    }

    // тут при коннекте надо обязательно указывать ресурс, иначе не будет работать
    // логин должен быть в полной форме, chat_clss_ru_1527@clss.ru
    function BOSH_Connection()
    {   connection = new Strophe.Connection(BOSH_SERVICE);
        connection.connect(settings.login+'/'+settings.resurce,settings.pass,onConnect);
        //connection.connect(settings.login,settings.pass,onConnect);
        //connection.connect(login,pass,onConnect);
    }

    function onConnect(status)
        {   if (status == Strophe.Status.CONNECTED)
            {  connection.addHandler(notifyUser, null, 'message', null, null,  null);
               //connection.send($pres({type: "unavailable"}).tree());
               //connection.send($pres({type: "Available"}).tree());
               connection.send($pres().tree());
            }
        }

    // сообщение при наборе первого символа
    // <body xmlns='http://jabber.org/protocol/httpbind'><message xmlns="jabber:client" type="chat" id="purple1d150427" to="chat_clss_ru_905@clss.ru/11335aa6" from="roman@clss.ru/iMac-Roman-Breznev"><composing xmlns="http://jabber.org/protocol/chatstates"/></message></body>

    function notifyUser(msg)
        {  var elems = msg.getElementsByTagName('body');
           var composing = msg.getElementsByTagName('composing')[0];
           var body = elems[0];
           if (body!=undefined) var mess=Strophe.getText(body) ;

           var params= {} ; var j ;
           if (msg.attributes.length) for(j=0; j<msg.attributes.length; j++) params[msg.attributes[j].nodeName]=msg.attributes[j].nodeValue ;
           if (composing!=undefined && composing.attributes.length) for(j=0; j<composing.attributes.length; j++) params[composing.attributes[j].nodeName]=composing.attributes[j].nodeValue ;

           if (mess) params['cmd']='chat/put_mess' ;
           params['from']=msg.getAttribute('from') ;
           params['chat_uid']=settings.uid ;
           params['time']=$j(panel_messages).find('div.item:last-child').attr('time') ;
           params['message']=mess ;
           params['screen_width']=screen.width ;
           params['screen_height']=screen.height ;
           if (mess) {  $j.ajax({url:'/ajax.php',type:'POST',dataType:'xml',cache:false,success:chat_update_success,data:params});
                        //show_panel_chat_if_is_closed() ;
                     }
           if (settings.debug)  show_debug_info('notifyUser',params) ;



           return true;
        }

    // переносим данные из XML в массив
     function convert_XML_to_obj(XML)
     {  var arr= {} ; var param ;
       // переводим все аттрибуты кликнутого элемента в скрытые переменные
       var xml_doc=XML.childNodes[0] ;
       for(var j=0; j<xml_doc.childNodes.length; j++)
       { param=xml_doc.childNodes[j] ;
         var name=param.nodeName ;
         if (param.childNodes.length) arr[name]=param.childNodes[0].nodeValue ;
         else                         arr[name]="" ;
       }
       return(arr) ;
     }

      function print_r(printthis, returnoutput)
      {  var output = '';
          if($j.isArray(printthis) || typeof(printthis) == 'object') for(var i in printthis) output += i + ' : ' + print_r(printthis[i], true) + '\n';
          else output += printthis;
          if (returnoutput && returnoutput == true) return output;
          else alert(output);
      }

      function htmlspecialchars(string){ return $j('<span>').text(string).html() ;}
} ;


$j.fn.panel_support = function(options)
  { var panel_support=this ;

      var settings = $j.extend(
                     {  'uid' : 0,
                        'login' : '',
                        'resurce' : '',
                        'pass' : '12345',
                        'debug' : 0
                     }, options);

    $j(panel_support).find('div.title').live('click',toggle_open) ;
    $j(panel_support).find('div.close').live('click',toggle_open) ;
    //$j(panel_support).find('div#close').click(panel_chat_close_up);

    function  toggle_open()
    { $j(panel_support).toggleClass('open') ;
      var status=($j(panel_support).hasClass('open'))? 'open':'close' ;
      send_ajax_request({cmd:'chat/set_chat_panel',status:status}) ;
    }

     function show_panel_chat_if_is_closed()
     { if ($j(panel_support).hasClass('open')) return ;
       $j(panel_support).die('click') ;
       $j(panel_support).addClass('open') ;
            var params={cmd:'chat/open_chat_panel'} ;
            $j.ajax({url:'/ajax.php',type:'POST',dataType:'xml',cache:false,data:params});

     }

     function panel_chat_close_up()
     { $j(panel_support).removeClass('open') ;
       $j(panel_support).live('click',show_panel_chat_if_is_closed) ;
        var params={cmd:'chat/close_chat_panel'} ;
        $j.ajax({url:'/ajax.php',type:'POST',dataType:'xml',cache:false,data:params});

        return(false) ;

     }

} ;

