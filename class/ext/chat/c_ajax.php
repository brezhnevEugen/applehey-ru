<?
include_once('messages.php') ;
include_once(_DIR_EXT.'/chat/panels.php') ;
include_once(_DIR_TO_ENGINE.'/c_page_XML_AJAX.php') ;
class c_page_ajax extends c_page_XML_AJAX
{

 // обоработка сообщения клиента
 function send_mess()
 { global $chat_system ;
   if (!$_POST['message']) return ;
   $rec_chat=array() ; $senders=array() ;
   $message=htmlspecialchars(strip_tags($_POST['message'])) ;
   // если передано chat_uid, открываем ветку чата, иначе создаем новый чат
   if ($_POST['chat_uid'])
   { $rec_chat=$chat_system->get_chat_info_by_uid($_POST['chat_uid'],array('no_get_messages'=>1)) ; // вся инфа по чату, без сообщений
     if ($rec_chat['pkey'] and $rec_chat['status']!=3) // если удалось открыть ветку чата и статус ветки != "ЗАКРЫТО"
       { $options['no_update_status_chat']=(!$rec_chat['man_id'])? 1:0 ; // если у чата еще нет оператора, должен оставаться статус часа = 0
         $chat_system->db_add_message($rec_chat['pkey'],array('message'=>$message),$options) ; // сохраняем сообщение в БД
         // готовим список рассылки
         if ($rec_chat['man_id']) $senders[]=$rec_chat['operator_login'] ; // сообщение будет отправлено только оператору чата
         else                     $senders=$chat_system->active_operators ;  // сообщение будет отправлено всем доступным операторам
       }
   }
   else
   {  // создаем чат в БД, создаем user на XMPP server, add user to room of operator
     $result=$chat_system->create_chat(array('message'=>$message,'name'=>$_POST['name'],'email'=>$_POST['email'],'phone'=>$_POST['phone'],'screen_width'=>$_POST['screen_width'],'screen_height'=>$_POST['screen_height'])) ;
     if ($result['type']=='success')
     { $rec_chat=$result['rec'] ;
       $senders=$chat_system->active_operators  ; // сообщение будет отправлено всем доступным операторам
     }
   }

   $this->add_element('operator_login',$rec_chat['operator_login']) ;
   $this->add_element('operator_available',$chat_system->get_operator_status($rec_chat['operator_login'])) ;
   $this->add_element('operators_work',sizeof($chat_system->active_operators)) ; // возвращает число работающих операторов

   ob_start() ; panel_chat_title($rec_chat)  ; $this->add_element('chat_title',ob_get_clean()) ;

   // независимо от того, первое сообщение в чате или нет, чат может быть как взятым, так и не взятым оператором
   // рассылаем сообщение по списку операторов $senders
   if (sizeof($senders)) foreach($senders as $login) $chat_system->send_XMPP_message($login,$rec_chat['XMPP_login'],$rec_chat['XMPP_pass'],$message);



   if ($rec_chat['XMPP_login_full'])
   { $this->add_element('jid',$rec_chat['XMPP_login_full']) ;
     if ($_POST['time']=='undefined') $_POST['time']=0 ;
     $list_messages=$chat_system->get_list_mess($rec_chat['pkey'],array('from_time'=>$_POST['time'])) ; // список сообщений чата
     if (sizeof($list_messages)) { ob_start() ; chat_list_messages($list_messages) ; $this->add_element('messages',ob_get_clean()) ;}
     $this->add_element('uid',$rec_chat['uid']) ;
     $_SESSION['chat_uid']=$rec_chat['uid'] ;
   }

 }

 // обоработка сообщения, пришедшего от XMPP
 function put_mess()
 { global $chat_system ;
   $result=array() ;
   $rec_operator=$chat_system->get_operator_by_login($_POST['from']) ; // определяем, от кого сообщение
   if ($_POST['chat_uid'])
   { $rec_chat=$chat_system->get_chat_info_by_uid($_POST['chat_uid'],array('no_get_messages'=>1)) ;
       if ($rec_chat['pkey'])
       { if (_XMPP_DEBUG) _event_reg('XMPP_put_mess',$_POST['from'].' => '.$rec_chat['XMPP_login'].': "'.$_POST['message'].'" <strong>ok</strong>') ;
         // если пишет другой оператор, который не ведет данный чат
         if ($rec_chat['man_id'] and $rec_operator['id']!=$rec_chat['man_id'])
         {   // пишем ему, что он не может учавствовать в даннм чате
             $chat_system->send_XMPP_message($rec_operator['login'],$rec_chat['XMPP_login'],$rec_chat['XMPP_pass'],_BASE_DOMAIN.': чат уже принят другим оператором, Ваше сообщение не доставлено.');
         }
         else // иначе сохраняем сообщение в базе и далее показываем клиенту
              // при сохранении сообщения в запись по ветке чата будет автоматически добавлено id и имя ответивщего оператора
         {   $message=htmlspecialchars(strip_tags($_POST['message'])) ;
             $cmd=substr($message,0,2) ;
             switch($cmd)
             {  case '*i': $chat_system->send_XMPP_message($rec_operator['login'],$rec_chat['XMPP_login'],$rec_chat['XMPP_pass'],$this->CMD_get_system_info()); break ;
                case '*H':
                case '*h': $url=$this->CMD_goto($message);
                           $result=$chat_system->db_add_message($rec_chat['pkey'],array('message'=>'>>> <a href="'.$url.'">'.$url.'</a>','man_id'=>$rec_operator['id'])) ;
                           $rec_chat=$chat_system->get_chat_info_by_id($rec_chat['pkey'],array('no_get_messages'=>1)) ; // обновляем информацию по чату
                           ob_start() ; panel_chat_title($rec_chat)  ; $this->add_element('chat_title',ob_get_clean()) ;
                           break ;
                default:   // сохраняем сообщение в базу
                           $result=$chat_system->db_add_message($rec_chat['pkey'],array('message'=>$_POST['message'],'man_id'=>$rec_operator['id'])) ;
                           $rec_chat=$chat_system->get_chat_info_by_id($rec_chat['pkey'],array('no_get_messages'=>1)) ; // обновляем информацию по чату
                           ob_start() ; panel_chat_title($rec_chat)  ; $this->add_element('chat_title',ob_get_clean()) ;
             }
         }

       }

       if ($result['type']=='success')
       { // если прежний статус чата = 0 (новый) уведомляем операторов, что чат забран другим оператором и удаляем клиента из списка контактов других операторов
         if (!$rec_chat['status'])
         {  $rec_chat['man_id']=$rec_operator['id'] ;
            $rec_chat['autor_name']=$rec_operator['obj_name'] ;
            $chat_system->send_cancel_message($rec_chat) ;
         }

         // получаем все сообщения чата, от последнего выведенного сообщения
         $list_messages=$chat_system->get_list_mess($rec_chat['pkey'],array('from_time'=>$_POST['time'])) ; // список сообщений чата
         if (sizeof($list_messages)) { ob_start() ; chat_list_messages($list_messages) ; $this->add_element('messages',ob_get_clean()) ;}

         $this->add_element('operator_name',$rec_operator['obj_name']) ; // имя будет показано в заголовке чата
       }
   }
 }

 function close_promt_panel()
 {
    $_SESSION['chat_promt_panel_status']='close' ;
 }

 function open_chat_panel()
 {
   $_SESSION['chat_panel_status']='open' ;
   $_SESSION['chat_promt_panel_status']='close' ;
 }

 function close_chat_panel()
 {
   $_SESSION['chat_panel_status']='close' ;
   $_SESSION['chat_no_auto_open']='1' ;
 }

 function set_chat_panel()
 {
   $_SESSION['chat_panel_status']=$_POST['status'] ;
   if ($_POST['status']=='close') $_SESSION['chat_no_auto_open']='1' ;
 }



 // возвращаем служебную информацию по клиенту
 function CMD_get_system_info()
 { $sys_info=array() ;
   $sys_info[]='IP: '.$_SERVER['REMOTE_ADDR'] ;
   $sys_info[]='Браузер: '.defines_vers($_SERVER['HTTP_USER_AGENT']) ;
   $sys_info[]='Информация агента: '.$_SERVER['HTTP_USER_AGENT'] ;
   $sys_info[]='Размер экрана монитора: '.$_POST['screen_width'].' x '.$_POST['screen_height'].' px' ;
   $sys_info[]='Страница сайта: '.$_SERVER['HTTP_REFERER'] ;
   $message=implode("\n",$sys_info) ;
   return($message) ;
 }

 function CMD_goto($mess)
 {  $url=str_replace('*','',$mess) ;
    $this->add_element('go_url',$url) ;
    return($url) ;
 }

 // обновление чата - не используется
 function update()
 { global $chat_system ;
   if ($chat_id=$chat_system->chat_exists($_POST['chat_uid']))
     { $list_messages=$chat_system->get_list_mess($chat_id,array('from_time'=>$_POST['time'])) ; // список сообщений чата
       if (sizeof($list_messages)) { ob_start() ; chat_list_messages($list_messages) ; $this->add_element('messages',ob_get_clean()) ;}
     }

 }

 // создание нового чата
 /*
 function new_chat()
 { $_SESSION['chat_uid']='' ;
   $this->add_element('panel_chat_messages','',array('mode'=>'update')) ;
   $this->add_element('client_mess','') ;
   ob_start() ;
   ?><script type="text/javascript">
        $j('div#panel_chat').attr('uid','');
        $j('div#panel_chat').stop('chat_update') ;
    </script>
   <?
   $this->add_element('JSCODE',ob_get_clean()) ;

 }*/

 // панель с окном чата
 /*
 function get_panel_chat()
 { global $chat_system ;
   include_once('panel_chat.php') ;

   $chat_uid=$_SESSION['chat_uid'] ; $chat_id=0 ;
   if ($chat_uid and $chat_id=$chat_system->chat_exists($chat_uid)) $XMPP_account=$chat_system->get_XMPP_account($chat_id) ;

   panel_chat(array('chat_id'=>$chat_id,'chat_uid'=>$chat_uid)) ;
   if ($chat_id) // прослушиваем аккаунт оператора
   { ob_start()  ;
     ?><script type="text/javascript">
         connection = new Strophe.Connection(BOSH_SERVICE);
         connection.connect('<?echo $XMPP_account['XMPP_login']?>@clss.ru','<?echo $XMPP_account['XMPP_pass']?>',onConnect);
       </script>
     <?
     $this->add_element('JSCODE',ob_get_clean()) ;
   }
   $this->add_element('title','Он-лайн чат') ;
   $this->add_element('success','ajax_to_modal_window') ;


   ?><div><button class="v2" cmd="chat/new_chat">Новый чат</button><br><br></div><?
 } */

}
