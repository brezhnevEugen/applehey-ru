<?
// внимание! элементы с присвоенными id - не удалять и не изменять id.
// остальные элемены - можно изменять по своему усмотрению
function panel_chat_fixed()
{   global $chat_system ;
    include_once(_DIR_TO_CLASS.'/ext/chat/panels.php') ;
    $chat_rec=$chat_system->get_active_chat_info() ;
    //$man_rec=$chat_system->get_first_manager_available() ;
    $man_rec=$_SESSION['IL_chat_manager'][3] ;
    ?><div id="panel_chat_fixed" class="panel_fixed <?if ($_SESSION['chat_panel_status']=='open') echo 'open'?>">
          <div class="title"></div>
          <!--<div id="promt">Есть вопрос? <strong>Спрашивайте!</strong></div>-->
          <div id="panel_chat_title"><?  panel_chat_title($chat_rec) ; ?></div>
          <div id="panel_chat_messages"><?  chat_list_messages($chat_rec['messages']) ; ?></div>
          <div id="panel_title2">Ваш вопрос:</div>
          <form>
              <textarea id="client_mess" data-required name="message"></textarea>
              <div id="panel_bottom_text">Для отправки сообщения нажмите Enter</div>
          </form>
          <div class="close"></div>
      </div>
      <div id="panel_chat_promt">
          <!--<img src="<?echo img_clone($man_rec,'small')?>" width="70" height="70">-->
          <img src="/images2/chat_logo.jpg" width="70" height="70">
          <div class="name"><?echo $man_rec['obj_name'] ?></div>
          <div class="welcome_text">Здравствуйте! Чем я могу Вам помочь?</div>
          <div class="buttons"><button class="close">Закрыть</button><button class="open v2" cmd="chat/open_chat_panel">Ответить</button></div>
          <div class="decor"></div>
      </div>
      <script type="text/javascript">
          $j(document).ready(function(){
              $j('div#panel_chat_fixed').panel_chat({auto_open:<?echo ($_SESSION['chat_no_auto_open'])? 0:5000?>,uid:"<?echo $chat_rec['uid']?>",login:"<?echo $chat_rec['XMPP_login_full']?>",resurce:"<?echo session_id().'_'.rand(1,100)?>"});
              <? if ($_SESSION['chat_promt_panel_status']!='close')
              { ?>$j('div#panel_chat_promt').oneTime(5000,'panel_chat_promt',function(){$j(this).show('drop',{direction:'right'},500)});
                  $j('div#panel_chat_promt button.close').click(function()
                    { send_ajax_request({cmd:'chat/close_promt_panel'}) ;
                      $j('div#panel_chat_promt').hide('drop',{direction:'right'},500);
                    }) ;
                  $j('div#panel_chat_promt button.open').click(function()
                    { send_ajax_request({cmd:'chat/open_chat_panel'}) ;
                      $j('div#panel_chat_fixed').addClass('open',500,function()
                       {send_ajax_request({cmd:'chat/set_chat_panel',status:'open'});
                        $j('div#panel_chat_promt').hide('drop',{direction:'right'},500);
                       })
                    }) ;
                <?
              }


              ?>
          });
      </script>
    <?
}


?>