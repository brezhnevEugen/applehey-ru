var tooltip_city ;
$j(document).ready(function()
{ if (update_city_info==1)
   { $j.ajax({url:'/ajax.php',type:'POST',dataType:'xml',cache:false,success:ajax_success,
                      data:{   ext:$j(this).attr('ext'),
                               cmd:'geoIP/check_city'
                           }
                     });

     function ajax_success(data,status,link)
     { if (status=='success')
        { var xml = link.responseXML;
          if (xml==undefined) return ;
          var data=convert_XML_to_obj(xml) ;
          var city = data.city;
          $j('div#panel_GEO_city').html(city) ;
          tooltip_city=new jBox('Tooltip',
            {   target: $j('div#panel_GEO_city'),
                content: data.html,
                position: {x: 'center',y: 'top'},outside: 'y',
                addClass: 'cityEvent'
            });
          tooltip_city.open() ;
          $j('.v2').prepare_ajax({}) ;
          $j('button#to_select_city').click(function()
            {$j('div#panel_select_city').removeClass('hidden');
             $j('div#panel_city_question').addClass('hidden');
             new mForm.Element.Select({original: 'myCity'}) ; ;
            }) ;
        }
     }
   }

})