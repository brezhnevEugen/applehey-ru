<?

//*************************************************************************************************************************************************************************************
//
// БЫСТРАЯ ПОКУПКА ТОВАРА
//
//*************************************************************************************************************************************************************************************
// <button class="button_green v2" ext=orders cmd=get_fast_order_panel >Купить в 1 клик</button>
// элемент должен находится в контейнете item c заданным атрибутом reffer для текущего объекта. Или задать атрибут в параметрах кнопки

function panel_fast_order($options=array())
{ global $member ;
  if (!$_SESSION['form_values']['phone'] and $member->id) $_SESSION['form_values']['phone']=$member->info['phone'] ;
  ?><div id=panel_fast_order>
    <p>Вы можете оформить заказ, указав только свой номер телефона. После оформления заказа с Вами свяжется менеджер для уточнения адреса доставки и вариантов оплаты.</p>
    <form>
     <table class="contact_form small"><colgroup><col id='c1'><col id='c2'></colgroup>
        <?if (!$member->id){?> <tr><td class=title>Ваше имя</td><td><input  name="name" data-required type="text" value="<?echo_form_saved('name')?>"/></td></tr><?}
         else {?><tr><td class=title>Ваше имя</td><td><input  name="name" data-required type="text" value="<?echo_form_saved('name')?>"/></td></tr><?}?>
         <tr><td class=title>Контактный телефон</td><td><input name="phone" data-required type="text" value="<?echo_form_saved('phone')?>"/><br><span class=comment>Укажите номер в формате "+7 999 123-45-67"</span></td></tr>
         <tr><td><img alt="" src="/images/img_check_code.php" class=checkcode></td><td><input data-required  class=checkcode name="_check_code" type="text"><br><span class=comment>Введите проверочный код</span></td></tr>
         <tr><td colspan=2 class=buttons>
                 <input type=button id=cancel class=cancel value="Отмена">
                 <input type=submit class="button_green v2" value="Оформить заказ" cmd=orders/fast_order validate=form reffer="<?echo $options['reffer']?>" color="<?echo $options['color']?>">
         </td></tr>
     </table>
     </form></div>
     <?
}

?>