<?php // оформление заказа
// скрипт вызывается только через 301 редирект в случае успешного создания заказа
// с передачей результатов создания заказа через 'result_order'
include_once('messages.php');
include_once(_DIR_EXT.'/account/messages.php') ;
include(_DIR_TO_CLASS.'/c_page.php') ;
class c_page_orders_complette extends c_page
{
  public $h1='Оформление заказа'  ;
  public $path=array('/'=>'Главная','/orders/'=>'Корзина') ;

  //function panel_search() {}

  function block_main()
  { // заголовок
    //$this->panel_path();  // заголовк страницы по умолчанию

    if ($_SESSION['result_account'])
    {
      ?><h1>Регистрация на сайте</h1><?
      show_message_by_id('account_message',$_SESSION['result_account']) ;
    }

    $this->page_title();  // заголовк страницы по умолчанию

    $result=$_SESSION['result_order'] ; // заказ уже создан, результат создания передается через сесиию
      
    // показываем результат оформления заказа
    if (sizeof($result))
    { show_message_by_id('orders_message',$result) ;
      // готовим дданные для гугла
      if ($result['type']=='success')
      { //include_once(_DIR_TO_SITE_EXT.'/google_EcommerceGA/order_info.php') ;
        //google_EcommerceGA_order_info($result['rec']['pkey']) ;
      }

    }
    else // 301 редирект на страницу оформления заказа
    {
       redirect_301_to('/orders/') ;
    }
    unset($_SESSION['result_order'],$_SESSION['result_account']) ;
  }
}

?>