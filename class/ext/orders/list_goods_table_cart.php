<?
  function list_goods_table_cart(&$arr_rec,$options=array())
  { global $orders_system, $TM_goods_image,$picture ;
//    damp_array($arr_rec);
    if (sizeof($arr_rec))
    { ?>
      <table class="list_goods_table_cart cart" id=cart_goods>
        <tr class='header'> <td>Фото</td>
          <td>Товар</td>
          <td>Стоимость за единицу</td>
          <td>Количество</td>
          <td>Всего</td>
          <td></td>
        </tr><?
        foreach ($arr_rec as $indx=>$rec)
        { $cart_info=$orders_system->prepare_price_info($rec) ;
          //damp_array($rec,1,-1) ;
          ?>
          <tr class='data' reffer="<?echo $rec['_reffer']?>">
            <td class=img>
              <a href="<?echo $rec['__href']?>"><?
                if ($rec['color_id']){$picture=execSQL_van('select * from '.$TM_goods_image.' where pkey='.$rec['color_id'].'');
                 ?><img src="<?echo img_clone($picture,'small')?>" alt="<?echo $rec['img_alt']?>"><?
                 }
                else  if (!$rec['_sortament_rec']['_image_name']) {?><img src="<?echo img_clone($rec,'small')?>" alt="<?echo $rec['img_alt']?>"><?}
                else {?><img src="<?echo img_clone($rec['_sortament_rec'],'small')?>" alt="<?echo $rec['img_alt']?>"><?}?>
              </a>
            </td>
            <td class=name>
              <div class=obj_name><?  echo $rec['icon_obj_name'] ;?></div><?
              if (isset($rec['_sortament_rec']))
              {  ?><div class="sortament_name"><?
                    if ($rec['sortament_type']==1 and $rec['_sortament_rec']['obj_name']) echo 'Цвет: <strong>'.$rec['_sortament_rec']['obj_name'].'</strong> ' ;
                    if ($rec['sortament_type']==2 and $rec['_sortament_rec']['obj_name']) echo 'Размер: <strong>'.$rec['_sortament_rec']['obj_name'].'</strong> ' ;
                    if ($rec['sortament_type']==3 and $rec['_sortament_rec']['obj_name']) echo 'Рисунок: <strong>'.$picture['manual'].'</strong><br> Размер: <strong>'.$rec['_sortament_rec']['obj_name'].'</strong> ' ;
                    //if ($rec['_sortament_rec']['color_name']) echo 'Цвет: <strong>'.$rec['_sortament_rec']['color_name'].'</strong> ' ;
                    //if ($rec['_sortament_rec']['size']) echo 'Размер: <strong>'.$rec['_sortament_rec']['size'].'</strong>' ;
                 ?></div><?
              } ?>
              <div class="comment"><?echo $rec['icon_obj_comment']?></div>

              <?

              if ($cart_info['sales_text']) echo '<div class=sales_info>'.str_replace(':<br>',' ',$cart_info['sales_title']).' - '.$cart_info['sales_text'].'</div>';
              //damp_array($cart_info) ;
              //if (!$rec['__stock_info']) echo  '<div class=no_stock>Нет в наличии, срок ожидания может составить от 2 до 6 недель</div>' ;
              ?>

            </td>
            <td class="price"><div class=price><?echo $cart_info['price'] ;?></div></td>
            <td class=cnt><span class="dec v2" cmd=orders/cnt_change mode="dec" min_value=1 sortament_reffer="<?echo $rec['_sortament_rec']['_reffer']?>"></span>
                         <input type=text class="text cnt v2" cmd=orders/cnt_set event=keyup value=<?echo $cart_info['cnt']?> name="cart_cnt_value" disabled>
                         <span class="inc v2" cmd=orders/cnt_change mode="inc" sortament_reffer="<?echo $rec['_sortament_rec']['_reffer']?>"></span>
            </td>
            <td class=price><?echo $cart_info['sum_price'] ;?></td>
            <td class=del><div class="del_from_cart v2" cmd=orders/del_from_cart cart_indx="<?echo $indx?>"></div></td>
          </tr>
          <?
        }
        global $goods_system ;
        ?>
        <tr class=itog_value>
          <td colspan=2 class="itog_title">ИТОГО:</td>
          <td colspan=2></td>
          <td class="itog_value"><div class="price"><?echo _format_price($_SESSION['cart']->sum_price)?></div></td>
          <td></td>
        </tr>
        <tr class=itog_action>
          <td colspan=2><div class="go_back v1" href="<?echo $goods_system->tree[$rec['parent']]->href?>">Вернуться к покупкам</div></td>
          <td ></td>
          <td colspan=3><div class="buttons"><button class="v1" href="/orders/checkout.php">Оформить заказ ></button></div></td>
        </tr>
      </table><?
    }
  }

?>