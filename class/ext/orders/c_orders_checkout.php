<?php // оформление заказа
include_once('messages.php');
include(_DIR_TO_CLASS.'/c_page.php');
class c_page_orders_checkout extends c_page
{
  public $h1='Оформление заказа' ;
  public $title='Оформление заказа' ;


  function block_main()
  { $this->page_title() ;
    global $orders_system,$member ;
    //$_SESSION['cart']->service=array() ;
    $orders_system->cart_update_info();
    // создаем заказ
    $result_order=array();
    if ($_POST['cmd']=='order_create') $result_order=$this->create_order() ;
    // показ результата команды
    if ($result_order['code']=='order_create_success')
    {  //сохраняем результат оформления заказа в сессии в переходим на страницу "complette"
       $_SESSION['result_order']=$result_order ;
       redirect_301_to('/orders/complette.php') ;
    }
    else
    { show_message_by_id('orders_message',$result_order) ;

      if ($_SESSION['cart']->all_count)
      {  if ($member->id)    { include_once('panel_form_checkout_full_member.php'); panel_form_checkout_full_member() ; }
         else                { include_once('panel_form_checkout_full.php'); panel_form_checkout_full() ; }

      }
      else                              echo  'Ваша корзина пуста' ;
    }

    //damp_array($_SESSION['cart']) ;
  }

  function create_order()
  {  global $account_system,$orders_system ;
     $order_info=array() ; $options=array() ;
     // информация заказа - КЛИЕНТ
     $member_typ=(!$_POST['typ'])? $_POST['typ']:1 ; // по умолчанию - физическое лицо
     $order_info['member_typ']=$_SESSION['arr_mem_type'][$member_typ]['admin'] ;
     $order_info['name']=$_POST['ORDER']['name'] ;
     $order_info['email']=$_POST['ORDER']['email'] ;
     $order_info['phone']=$_POST['ORDER']['phone'] ;

     // информация заказа - ДАННЫЕ ПО ОРГАНИЗАЦИИ
     $order_info['org_name']=$_POST['ORDER']['org_name'] ;
     //$order_info['rekvezit']=$_POST['rekvezit'];

     // информация заказа - ОПЛАТА
     $payments_type=($_POST['opl_type'])? $_POST['opl_type']:$_SESSION['cart']->payments_type ;    // id способа оплаты ;
     $order_info['payments_type']=$payments_type ;  // оплата наличными
     $order_info['payments_method']=$_SESSION['IL_payments_type'][$payments_type]['method'] ; // код метода оплаты - для вызова в дальшейшем соответствующего сервера

     // информация заказа - ДОСТАВКА
     $order_info['adres']=$_POST['adres'] ;
     $order_info['city']=$_POST['city'];
     $order_info['zip']=$_POST['zip'];

     // информация заказа - ФАЙЛЫ
     if (sizeof($_FILES['order_doc'])) $options['files'][]=$_FILES['order_doc'] ;

     // информация заказа - ДАННЫЕ ПО АККАУНТУ
     if ($_SESSION['member']->id)  $order_info['account_reffer']=$_SESSION['member']->reffer ;
     else // если клиент не авторизован, ищем его аккаунт по email и добавляем ссылку на этот аккаунт в заказ
     if (is_object($account_system))
     { $account=$account_system->get_account_info_by_email($_POST['email']) ;
       if ($account['pkey']) $order_info['account_reffer']=$account['_reffer'] ;
     }

     // информация заказа - РАСШИРЕННАЯ ИНФОРМАЦИЯ (сохраняется в info[serial])
     $order_info['info']['use_SMS']=$_POST['use_SMS'] ;

     // информация заказа - ДОПОЛНИТЕЛЬНАЯ ИНФОРМАЦИЯ
     $order_info['subscript']=(isset($_POST['subscript']))? $_POST['subscript']:1 ;  // подписка на новости

     $result=$orders_system->order_create_by_data($_SESSION['cart'],$order_info,$options) ;
     return($result) ;
  }


}

?>