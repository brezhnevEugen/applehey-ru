<?
include_once('messages.php');
include(_DIR_TO_ENGINE.'/c_page_XML_AJAX.php') ;
class c_page_ajax extends c_page_XML_AJAX
{

      // стандарная инфа по содержимому корзины
     function add_cart_info()
     { $this->add_element('sum_count',$_SESSION['cart']->sum_count.' шт.') ;
       $this->add_element('sum_price',_format_price($_SESSION['cart']->sum_price)) ;
       $this->add_element('end_price',_format_price($_SESSION['cart']->end_price)) ;
     }

    //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // КОРЗИНА
    //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    function add_to_cart()
    {  
       // создаем новую козину, если еще не создана
       if (!is_object($_SESSION['cart'])) $_SESSION['cart']=new c_cart ;


       $reffer=$_POST['reffer'] ; $sort_reffer=$_POST['sortament_reffer'] ; $color_id=$_POST['color_id'] ; $cart_indx='' ; $goods_id=0 ; $goods_tkey=0 ; $sort_id=0 ;  $sort_tkey=0 ;
       if ($reffer and !$sort_reffer) $cart_indx=$reffer ;  // для добавления в корзину товара без сортамента
       if ($reffer and $sort_reffer) $cart_indx=$reffer.'.'.$sort_reffer; // для добавления в корзину товаров с сортаментом
       if ($reffer and $sort_reffer and $color_id) $cart_indx=$reffer.'.'.$sort_reffer.'.'.$color_id; // для добавления в корзину товаров с сортаментом


       $cnt=$_POST['cnt'] ;
       check_input_var($cnt,'int') ;

       // если все товары с сортаментом - берем первый сортамент из списка
       //if (!$sort_reffer and is_object($_SESSION['1c_system']))
       //{ $sortament_rec=$_SESSION['1c_system']->get_first_sortament_to_goods($_POST['reffer']) ;
       //  if ($sortament_rec['pkey']) $_POST['sortament_reffer']=$sortament_rec['_reffer'] ;
       //}

       if (!isset($_SESSION['cart']->goods[$cart_indx])) // добавляем, если только еше нет в корзину
       { ORDERS()->add_to_cart($_SESSION['cart'],$_POST['reffer'],$_POST['cnt'],array('sortament_reffer'=>$_POST['sortament_reffer'],'cart_indx'=>$cart_indx)) ;
         if ($color_id)
         { $_SESSION['cart']->goods[$cart_indx]['color_id']=$color_id ;
           $_SESSION['cart']->goods[$cart_indx]['_sortament_rec']['img_id']=$color_id ;
           $_SESSION['cart']->goods[$cart_indx]['__name'].=' (цвет '.$color_id.') 1' ;
           $_SESSION['cart']->goods[$cart_indx]['obj_name'].=' (цвет '.$color_id.') 2' ;
         }

         ORDERS()->cart_update_info($_SESSION['cart']) ;

       }

       $this->add_cart_info() ; // стандарная инфа по содержимому корзины
       $this->add_function_out('panel_cart_small') ; // готовим текстовку для панели корзины в шапке
       $this->add_function_out('panel_cart_info_in_page_goods') ; // готовим текстовку для панели корзины на странице товара


       // готовим текст для всплывающего уведомления о добавлении в корзину - только для обычных товаров
       if (!$sort_reffer)
       { include_once('panel_cart_preview_van.php');
         panel_cart_preview_van($_SESSION['cart']->goods[$cart_indx]) ;
         // переход на страницу корзины
         //$this->go_url='/orders/' ;
         // или параметры модального окна
         $this->success='modal_window' ;
         $this->modal_panel_title='Товар добавлен в корзину' ;
         $this->no_close_button=1 ;
       }

         $cart_rec=$_SESSION['cart']->goods[$reffer] ;
         $this->add_element('summa',_format_price($cart_rec['__price_4']*$cart_rec['__cart_cnt'])) ;
         $this->add_element('out_cnt',$cart_rec['__cart_cnt'].' шт<br>') ;

        $this->add_element('notice','Товар добавлен в корзину') ;
        $this->add_element('cart_indx',$cart_indx) ;


        return($cart_indx) ;
    }

    function del_from_cart()
    {  ORDERS()->del_from_cart($_SESSION['cart'],$_POST['cart_indx']) ;
       ORDERS()->cart_update_info() ;

       $this->add_function_out('panel_cart_small') ; // готовим текстовку для панели корзины в шапке
        $this->add_function_out('panel_cart_info_in_page_goods') ; // готовим текстовку для панели корзины на странице товара
       $this->add_function_out('panel_cart_main') ; ; // выводим повторно панедь корзины - полностью

       $this->add_element('summa','') ;
       $this->add_element('out_cnt','') ;

       $this->add_element('notice','Товар удален из корзины') ;

    }



    function cnt_change()
    {
      $reffer=$_POST['reffer'] ; $sort_reffer=$_POST['sortament_reffer'] ; $color_id=$_POST['color_id'] ; $cart_indx='' ; $goods_id=0 ; $goods_tkey=0 ; $sort_id=0 ;  $sort_tkey=0 ;
      list($pkey,$pkey)=explode('.',$reffer) ;
      $spec_cnt=execSQL_value('select spec_cnt from obj_site_goods where pkey="'.$pkey.'"') ;
      if ($reffer and !$sort_reffer and isset($_SESSION['cart']->goods[$reffer])) $cart_indx=$reffer ;  // для добавления в корзину товара без сортамента
      if ($reffer and $sort_reffer and isset($_SESSION['cart']->goods[$reffer.'.'.$sort_reffer])) $cart_indx=$reffer.'.'.$sort_reffer; // для добавления в корзину товаров с сортаментом
      if ($reffer and $sort_reffer and $color_id and isset($_SESSION['cart']->goods[$reffer.'.'.$sort_reffer.'.'.$color_id])) $cart_indx=$reffer.'.'.$sort_reffer.'.'.$color_id; // для добавления в корзину товаров с сортаментом

      $this->add_element('cart_indx',$cart_indx) ;
      $this->add_element('sortament_reffer',$_POST['sortament_reffer']) ;
      if ($_POST['mode']=='inc')
      { if (!$cart_indx)
        { $_POST['cnt']=$spec_cnt;
          $cart_indx=$this->add_to_cart() ;  //если !$cart_indx - товара еще нет в корзине, добавляем
        }
        else
        {
          $_SESSION['cart']->goods[$cart_indx]['__cart_cnt']=$_SESSION['cart']->goods[$cart_indx]['__cart_cnt']+$spec_cnt;
//          $_SESSION['cart']->goods[$cart_indx]['__cart_cnt']++ ; // иначе просто увеличиваем кол-во
               ORDERS()->cart_update_info() ; // обновление информации по корзине
             }
      }
      if ($_POST['mode']=='dec' and $cart_indx) // уменьшаем только если есть товар в корзине
      {  $min_value=($_POST['min_value'])? $_POST['min_value']:0 ;
         if ($_SESSION['cart']->goods[$cart_indx]['__cart_cnt']>$min_value)
           $_SESSION['cart']->goods[$cart_indx]['__cart_cnt']=$_SESSION['cart']->goods[$cart_indx]['__cart_cnt']-$spec_cnt;
//         $_SESSION['cart']->goods[$cart_indx]['__cart_cnt']-- ; // иначе просто увеличиваем кол-во
         if (!$_SESSION['cart']->goods[$cart_indx]['__cart_cnt']) unset($_SESSION['cart']->goods[$cart_indx]) ; // если долшли до нуля - удаляем товар из корзины
         ORDERS()->cart_update_info() ; // обновление информации по корзине
      }

      // вписиываем в input кол-во товара
      list($goods_id,$goods_tkey)=explode('.',$reffer) ;
      if ($sort_reffer)  list($sort_id,$sort_tkey)=explode('.',$sort_reffer) ;
      $input_id=($sort_id)? 'input_cnt_'.$goods_id.'_'.$sort_id:'input_cnt_'.$goods_id ;
      $this->add_element($input_id,$_SESSION['cart']->goods[$cart_indx]['__cart_cnt']) ;




      // текстовку для всплывающего уведомления о добавлении в корзину   - если изменение кол-ва производиться в этом окне
      // только для изменения кол-ва товара, не сортамента
      if (!$sort_reffer)
      { ob_start() ;
        include_once('panel_cart_preview_van.php');
        panel_cart_preview_van($_SESSION['cart']->goods[$_POST['reffer']]) ;
        $this->add_element('panel_cart_preview_van',ob_get_clean()) ;
      }

      $this->add_function_out('panel_cart_small') ; // готовим текстовку для панели корзины в шапке
      $this->add_function_out('panel_cart_info_in_page_goods') ; // готовим текстовку для панели корзины на странице товара
      $this->add_function_out('panel_cart_main') ; ; // выводим повторно панедь корзины - полностью
    }

  function cnt_change_ext()
     {
       $reffer=$_POST['reffer'] ; $sort_reffer=$_POST['sortament_reffer'];$pic_group=$_POST['pic_group'] ; $cart_indx='' ; $goods_id=0 ; $goods_tkey=0 ; $sort_id=0 ;  $sort_tkey=0 ;
       list($pkey,$pkey)=explode('.',$reffer) ;
       $spec_cnt=execSQL_value('select spec_cnt from obj_site_goods where pkey="'.$pkey.'"') ;
       if ($reffer and !$sort_reffer and isset($_SESSION['cart']->goods[$reffer])) $cart_indx=$reffer ;  // для добавления в корзину товара без сортамента
       if ($reffer and $sort_reffer and isset($_SESSION['cart']->goods[$reffer.'.'.$sort_reffer])) $cart_indx=$reffer.'.'.$sort_reffer; // для добавления в корзину товаров с сортаментом

       $this->add_element('cart_indx',$cart_indx) ;
       $this->add_element('sortament_reffer',$_POST['sortament_reffer']) ;
       if ($_POST['mode']=='inc')
       { if (!$cart_indx)
       { $_POST['cnt']=$spec_cnt;
         $cart_indx=$this->add_to_cart() ;  //если !$cart_indx - товара еще нет в корзине, добавляем
       }
         else {
           $_SESSION['cart']->goods[$cart_indx]['__cart_cnt']=$_SESSION['cart']->goods[$cart_indx]['__cart_cnt']+$spec_cnt;
 //          $_SESSION['cart']->goods[$cart_indx]['__cart_cnt']++ ; // иначе просто увеличиваем кол-во
                ORDERS()->cart_update_info() ; // обновление информации по корзине
              }
       }
       if ($_POST['mode']=='dec' and $cart_indx) // уменьшаем только если есть товар в корзине
       {  $min_value=($_POST['min_value'])? $_POST['min_value']:0 ;
          if ($_SESSION['cart']->goods[$cart_indx]['__cart_cnt']>$min_value)
            $_SESSION['cart']->goods[$cart_indx]['__cart_cnt']=$_SESSION['cart']->goods[$cart_indx]['__cart_cnt']-$spec_cnt;
 //         $_SESSION['cart']->goods[$cart_indx]['__cart_cnt']-- ; // иначе просто увеличиваем кол-во
          if (!$_SESSION['cart']->goods[$cart_indx]['__cart_cnt']) unset($_SESSION['cart']->goods[$cart_indx]) ; // если долшли до нуля - удаляем товар из корзины
          ORDERS()->cart_update_info() ; // обновление информации по корзине
       }

       // вписиываем в input кол-во товара
       list($goods_id,$goods_tkey)=explode('.',$reffer) ;
       if ($sort_reffer)  list($sort_id,$sort_tkey)=explode('.',$sort_reffer) ;
       $input_id=($sort_id)? 'input_cnt_'.$goods_id.'_'.$sort_id:'input_cnt_'.$goods_id ;
       $this->add_element($input_id,$_SESSION['cart']->goods[$cart_indx]['__cart_cnt']) ;

       // текстовку для всплывающего уведомления о добавлении в корзину   - если изменение кол-ва производиться в этом окне
       // только для изменения кол-ва товара, не сортамента
       if (!$sort_reffer)
       { ob_start() ;
         include_once('panel_cart_preview_van.php');
         panel_cart_preview_van($_SESSION['cart']->goods[$_POST['reffer']]) ;
         $this->add_element('panel_cart_preview_van',ob_get_clean()) ;
       }

       $this->add_function_out('panel_cart_small') ; // готовим текстовку для панели корзины в шапке
       $this->add_function_out('panel_cart_info_in_page_goods') ; // готовим текстовку для панели корзины на странице товара
       $this->add_function_out('panel_cart_main') ; ; // выводим повторно панедь корзины - полностью
     }

    function cnt_set()
    {
      $reffer=$_POST['reffer'] ;
      $value=$_POST['value'] ;
      check_input_var($value,'int') ;
      if ($value<1) $value=1 ;
      $_SESSION['cart']->goods[$reffer]['__cart_cnt']=$value ;

      if ($_SESSION['cart']->goods[$_POST['reffer']]['__ed_izm']==1)
      { list($m2_res,$n_pack,$n_plit)=check_plit_size($_SESSION['cart']->goods[$reffer]) ;
        $_SESSION['cart']->goods[$reffer]['__cart_cnt_old']=$value ;
        $_SESSION['cart']->goods[$reffer]['__cart_cnt']=$m2_res ;
      }
      ORDERS()->cart_update_info() ;

      // обновляем текстовку для всплывающего уведомления о добавлении в корзину
      ob_start() ;
        include_once('panel_cart_preview_van.php');
        panel_cart_preview_van($_SESSION['cart']->goods[$_POST['reffer']]) ;
      $this->add_element('panel_cart_preview_van',ob_get_clean()) ;

      $this->add_cart_info() ; // стандарная инфа по содержимому корзины
      $this->add_function_out('panel_cart_small') ; // готовим текстовку для панели корзины в шапке
      $this->add_function_out('panel_cart_info_in_page_goods') ; // готовим текстовку для панели корзины на странице товара
      $this->add_function_out('panel_cart_main') ; ; // выводим повторно панедь корзины - полностью


      $cart_rec=$_SESSION['cart']->goods[$_POST['reffer']] ;
      $this->add_element('summa',_format_price($cart_rec['__price_4']*$cart_rec['__cart_cnt'])) ;
      if ($cart_rec['__ed_izm']==1) $this->add_element('out_cnt',$n_pack.' уп.<br>'.$m2_res.' м2') ;
      else                          $this->add_element('out_cnt',$cart_rec['__cart_cnt'].' шт<br>') ;

      $this->add_element('notice','Изменено количество товара в корзине') ;
    }


    function update_cart_cnt()
    {
      $value=$_POST['value'] ;
      $reffer=$_POST['reffer'] ;
      check_input_var($value,'int') ;
      if (!$value) $value=1 ;
      if (sizeof($_SESSION['cart']->goods[$reffer])) $_SESSION['cart']->goods[$reffer]['__cart_cnt']=$value ;
      ORDERS()->cart_update_info() ;

      //$this->add_cart_info() ; // стандарная инфа по содержимому корзины
      $this->add_function_out('panel_cart_small') ; // готовим текстовку для панели корзины в шапке
      $this->add_function_out('panel_cart_info_in_page_goods') ; // готовим текстовку для панели корзины на странице товара
      //$this->add_function_out('panel_cart_main')  ; // выводим повторно панедь корзины - полностью

      //include_once(_DIR_TO_SITE_EXT.'/cart/panel_cart_preview_van.php');
      //ob_start() ;
      // panel_cart_preview_van($_SESSION['cart']->goods[$reffer]) ;
      //$text=ob_get_clean() ;
      //$this->add_element('panel_cart_preview_van',$text) ;
    }

    //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ДОСТАВКА
    //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    // выбор типа доставки
    function shiping_type_select_on_change() {$this->update_shipping_type();}
    function update_cart_shipping()          {$this->update_shipping_type();}

    function update_shipping_type()
    {

      //ORDERS()->set_type_shiping($_POST['value']) ;
      ORDERS()->set_service_in_cart('shiping',$_POST['value']) ;

      $this->add_element('type_shiping',$_SESSION['cart']->type_shiping) ;
      $this->add_element('price_shiping',$_SESSION['cart']->price_shiping) ;
      if ($_SESSION['cart']->price_shiping) $this->add_element('price_shiping_text','<div id=price_shiping_text>'._format_price($_SESSION['cart']->price_shiping).'</div>') ;
      else                                  $this->add_element('price_shiping_text','<div id=price_shiping_text>бесплатно</div>') ;
      $this->add_element('text_shiping',$_SESSION['cart']->text_shiping) ;
        //print_r($_SESSION['cart']) ;
      // стандарная инфа по содержимому корзины
      //$this->add_cart_info() ;
      //$this->add_function_out('panel_cart_main')  ;
      //$this->add_function_out('panel_cart_summary')  ;
      //  инфа по заказу в форме корзины
      ob_start() ;
      include_once('list_goods_table_cart_no_edit.php');
      list_goods_table_cart_no_edit($_SESSION['cart']->goods) ;
      $text=ob_get_clean() ;
      $this->add_element('order_result.HTML',$text) ;

      ob_start() ;
      ?><script type="text/javascript">
           <?if ($_POST['value']=='1') {?>$j('tr#adres_dost').hide();$j('tr#time_dost').hide();$j('tr#trans_select').hide();<?}?>
           <?if ($_POST['value']=='2') {?>$j('tr#time_dost').show();$j('tr#adres_dost').show();$j('#com_dost_curer').show();<?}else{?>$j('tr#time_dost').hide();$j('#com_dost_curer').hide();<?}?>
           <?if ($_POST['value']=='3') {?>$j('tr#trans_select').show();$j('tr#adres_dost').show();$j('#com_dost_post').show();<?}else{?>$j('tr#trans_select').hide();$j('#com_dost_post').hide();<?}?>

      </script><?
      $text=ob_get_clean() ;
      $this->add_element('JSCODE',$text) ;


    }


    //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ОПЛАТА
    //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    function payments_type_select_on_change() {$this->update_payments_type();}
    function update_cart_payments() {$this->update_payments_type();}
    function update_payments_type()
    { $_SESSION['orders_system']->set_service_in_cart('payments',$_POST['value']) ;
      //$this->add_function_out('panel_cart_summary')  ;
    }

    // задаем режим оплаты - наличные и переводим клиента на страницу оформления заказа
    function buy_for_cash()
    {  $_SESSION['orders_system']->set_service_in_cart('payments',1) ;
       $this->go_url='/orders/checkout.php' ;
    }

    // задаем режим оплаты - кредит и переводим клиента на страницу оформления заказа
    function buy_on_credit()
    {  $_SESSION['orders_system']->set_service_in_cart('payments',5) ;
       $this->go_url='/orders/checkout.php' ;
    }

    //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ВЫБОР УСЛУГ
    //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    function update_wake_up_type()
    {
      ORDERS()->set_service_in_cart('wake_up',$_POST['value']) ;
      $this->add_element('price_service_text','<div id=price_service_text>'._format_price($_SESSION['cart']->service['wake_up']['price']+$_SESSION['cart']->service['assembling']['price']).'</div>') ;
    }

    function update_assembling_type()
    {
      ORDERS()->set_service_in_cart('assembling',$_POST['value']) ;
      $this->add_element('price_service_text','<div id=price_service_text>'._format_price($_SESSION['cart']->service['wake_up']['price']+$_SESSION['cart']->service['assembling']['price']).'</div>') ;
    }

    //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ВЫБОР ТИПА КЛИЕНТА
    //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    function update_member_type()
      { save_form_values_to_session($_POST) ;
        $_SESSION['member_typ']=$_POST['value'] ;
        $this->add_function_out('panel_form_checkout_full')  ;
      }

    //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // ОФОРМЛЕНИЕ ЗАКАЗА
    //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    function get_panel_order_create()
    {
        include_once(_DIR_EXT.'/orders/panel_checkout_small2.php') ;
        panel_checkout_small2() ;
        $this->add_element('show_modal_window','HTML',array('title'=>'Оформление заказа')) ;

    }


    function prepare_rec_order()
    {  global $goods_system,$member ;
       save_form_values_to_session($_POST) ;
       $data=array() ;
       // вносим в запись запроса стандартные поля
       $data['theme']=($_POST['SUPPORT']['theme'])? $_POST['SUPPORT']['theme']:'Вопрос в службу поддержки' ;
       $data['name']=$_POST['SUPPORT']['name'] ;
       $data['email']=$_POST['SUPPORT']['email'] ;
       $data['phone']=$_POST['SUPPORT']['phone'] ;
       $data['value']=$_POST['SUPPORT']['value'] ;
       // если поля по автору не заполнены и клиент авторизован - берем данные из аккаунта
       if (!$data['name'] and $member->id) $data['name']=$member->info['name'] ;
       if (!$data['email'] and $member->id) $data['email']=$member->info['email'] ;
       if (!$data['phone'] and $member->id) $data['phone']=$member->info['phone'] ;

       // выделяем в отдельный массив поля, которые будут сериализированы
       if (isset($_POST['SUPPORT']['org_name'])) $data['autor_info']['org_name']=$_POST['SUPPORT']['org_name'] ;
       if (isset($_POST['SUPPORT']['status'])) $data['autor_info']['status']=$_POST['SUPPORT']['status'] ;
       if (isset($_POST['SUPPORT']['inn'])) $data['autor_info']['inn']=$_POST['SUPPORT']['inn'] ;
       if (isset($_POST['SUPPORT']['kpp'])) $data['autor_info']['kpp']=$_POST['SUPPORT']['kpp'] ;
       if (isset($_POST['SUPPORT']['adres'])) $data['autor_info']['adres']=$_POST['SUPPORT']['adres'] ;
       // подгружаемый файлы
       if (sizeof($_FILES['upload_files'])) $data['upload_files']=conver_FILES_to_recs($_FILES['upload_files']) ;
       return($data) ;
    }


    function order_create()
     { global $member ;
       if (!is_object($_SESSION['cart'])){ $_SESSION['cart']=new c_cart() ; $GLOBALS['cart']=&$_SESSION['cart'] ; }
       // готовим инеформацию по заказу
       $order_info=array() ; $options=array() ; $account=array() ;
       save_form_values_to_session($_POST) ;
       //damp_array($_POST,1,-1) ;
       // информация заказа - КЛИЕНТ
       $member_typ=$_POST['member_type']  ; // по умолчанию - физическое лицо
       if (!$member_typ and $member->id) switch($member->clss)
       { case 87: $member_typ=2 ; break ;
         case 88: $member_typ=1 ; break ;
       }
       $order_info['member_typ']=$_SESSION['arr_mem_type'][$member_typ]['admin'] ;
       $order_info['name']=$_POST['name_f'].' '.$_POST['name_i'].' '.$_POST['name_o'] ;
       if (!$_POST['name_f'] and !$_POST['name_i'] and !$_POST['name_o'])  $order_info['name']=$member->info['contact_name'];
       $order_info['name_f']=$_POST['name_f'] ;
       $order_info['name_i']=$_POST['name_i'] ;
       $order_info['name_o']=$_POST['name_o'] ;
       $order_info['email']=$_POST['email'] ;
       $order_info['phone']=$_POST['phone'] ;
       $order_info['comment']=$_POST['comment'] ;
       $order_info['org_name']=$_POST['org_name'] ;

       // информация заказа - ДАННЫЕ ПО ОРГАНИЗАЦИИ
       $order_info['org_name']=$_POST['org_name'] ;
       $order_info['rekvezit']=$_POST['rekvezit'];

       // информация заказа - ОПЛАТА
       $payments_type=($_SESSION['cart']->payments_type)? $_SESSION['cart']->payments_type:1 ;    // id способа оплаты ;
       $order_info['payments_type']=$payments_type ;  // оплата наличными
       $order_info['payments_method']=$_SESSION['IL_payments_type'][$payments_type]['method'] ; // код метода оплаты - для вызова в дальшейшем соответствующего сервера

       // информация заказа - ДОСТАВКА
       $order_info['adres']=$_POST['zip']." ".$_POST['state']." ".$_POST['city']." ".$_POST['adres'] ;
       $order_info['city']=$_POST['city'];
       $order_info['zip']=$_POST['zip'];

       // информация заказа - ФАЙЛЫ
       if (sizeof($_FILES['order_doc'])) $options['files']=$_FILES['order_doc'] ;

       // информация заказа - ДАННЫЕ ПО АККАУНТУ
       if ($_SESSION['member']->id)  $order_info['account_reffer']=$_SESSION['member']->reffer ;
       //else // если клиент не авторизован, ищем его аккаунт по email и добавляем ссылку на этот аккаунт в заказ
       //{ $account=$account_system->get_account_info_by_email($_POST['email']) ;
       //  if ($account['pkey']) $order_info['account_reffer']=$account['_reffer'] ;
       //}

       // информация заказа - РАСШИРЕННАЯ ИНФОРМАЦИЯ (сохраняется в info[serial])
       //$order_info['info']['use_SMS']=$_POST['use_SMS'] ;
       if ($_SESSION['cart']->type_shiping==2) $order_info['info']['time_dost']=$_POST['time_dost'] ;
       if ($_SESSION['cart']->type_shiping==3) $order_info['info']['trans_select']=$_SESSION['IL_trans'][$_POST['trans_select']]['obj_name'] ;

       // информация заказа - ДОПОЛНИТЕЛЬНАЯ ИНФОРМАЦИЯ
       $order_info['subscript']=(isset($_POST['subscript']))? $_POST['subscript']:1 ;  // подписка на новости
       $order_info['what']=$_POST['what'] ;  // подписка на новости
       $order_info['phone2']=$_POST['phone2'] ;  // подписка на новости
       //damp_array($order_info,1,-1) ;
       $result=ORDERS()->order_create_by_data($_SESSION['cart'],$order_info,$options) ;

       if ($result['type']=='success')
       {  // в случае успешного создания заказа переходим на страницу /orders/complette.php, котороя покажет сообщение о оформлении заказа
          // отдельную страницу требуют гугл для учета конверсии поситителей
          //$this->add_element('success','modal_window') ;
          //damp_array($result) ;
          $this->add_element('go_url','/orders/complette.php') ;
          $_SESSION['result_order']=$result ;
          $_SESSION['result_order']['account']=$account ;
       }
       else
       {  // иначе модальное оконо или предупреждение - на выбор
          $this->add_element('title','Оформление заказа') ;
          $this->add_element('success','modal_window') ;
          ORDERS()->show_message($result) ;
       }
     }

    //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // БЫСТРАЯ ПОКУПКА
    //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

     function get_fast_order_panel()
     { $this->add_element('success','modal_window') ;
       $this->add_element('title','Быстрое оформление заказа') ;
       $this->add_element('no_close_button',1) ;
       include_once('panel_fast_order.php') ;
       panel_fast_order(array('reffer'=>$_POST['reffer'],'sortament_reffer'=>$_POST['sortament_reffer'])) ;
     }

     function fast_order()
     { // готовим инеформацию по заказу
       $order_info=array() ; $account=array() ;
       $order_info['name']=$_POST['name'] ;
       $order_info['phone']=$_POST['phone'] ;
       $order_info['member_typ']=$_SESSION['arr_mem_type'][1]['admin'] ;  // физическое лицо
       $order_info['payments_type']=1 ;  // оплата наличными
       $order_info['subscript']=1 ;  // подписка на новости

       // информация заказа - ДАННЫЕ ПО АККАУНТУ
       if ($_SESSION['member']->id)  $order_info['account_reffer']=$_SESSION['member']->reffer ;

       // email не заполняется, соотвественно личный кабинет найти не можем
       //else // если клиент не авторизован, ищем его аккаунт по email и добавляем ссылку на этот аккаунт в заказ
       //   { $account=$account_system->get_account_info_by_email($_POST['email']) ;
       //     if ($account['pkey']) $order_info['account_reffer']=$account['_reffer'] ;
       //   }

       // если не товар из общей корзины, то добавляем товар в отдельную корзину
       if ($_POST['reffer']!='cur_cart')
       {  $temp_cart=new c_cart() ; // будет создана пустая корзина с доставкой и оплатой с первыми из списка
          //ORDERS()->set_type_shiping(1,$temp_cart) ; // самовывоз со скада
          ORDERS()->set_service_in_cart('shiping',1) ;  // самовывоз со скада
          ORDERS()->add_to_cart($temp_cart,$_POST['reffer'],1) ;
          ORDERS()->cart_update_info($temp_cart) ;
          // создаем заказ
          $result=ORDERS()->order_create_by_data($temp_cart,$order_info) ;
       }
       // создаем заказ на основе существующей корзины
       else $result=ORDERS()->order_create_by_data($_SESSION['cart'],$order_info) ;

       // сообщение о результате оформления заказа
       // задаем опции для вывода ответа в модальном окне, если не удалось оформит заказа
       if ($result['type']!='success')
       { $this->add_element('title','Оформление заказа') ;
         $this->add_element('success','modal_window') ;
       }
       // иначе переходим на страницу "complette"
       else
       {  $result['code']='fast_order_create_success' ; // сообщение о быстром оформлении заказа немного другое

          // ВАРИАНТ 1: переход на /orders/complette.php
          $this->add_element('go_url','/orders/complette.php') ;
          $_SESSION['result_order']=$result ;
          //$_SESSION['result_order']['account']=$account ;

          // ВАРИАНТ 2: показ уведомления в всплывающем окне
          /*
          ?><script type="text/javascript">$j('div#panel_fast_order').mBox_instances().close() ;</script><? // закрываем ранее открытое модальное окно
          ORDERS()->show_message($result) ;
          $this->add_element('title','Оформление заказа') ;
          $this->add_element('success','modal_window') ;
          $this->add_cart_info() ; // готовим текстовку для панели корзины в шапке
          $this->add_function_out('panel_cart_small') ; // готовим текстовку для панели корзины в шапке
          */
       }
     }

    //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // СОЗДАНИЕ ЗАКАЗА С СОЗДАНИЕМ АККАУНТА
    //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    function account_to_order_create()
    {  global $account_system ;
       include_once(_DIR_EXT.'/account/messages.php') ;
       // 1. создаем аккаунт
       $account_rec=prepare_rec_account() ; // функция аналогична prepare_rec_account в /ext/account/ajax.php
       $account_result=$account_system->create_account($account_rec,array('no_act_code'=>0)) ;
       if ($account_result['type']=='success')
       {   // 2. авторизуем клиента
           $account_system->autorize($account_result['rec'],array('event_name'=>'Авторизация на сайте через создание заказа')) ;

           // 3. создаем заказ
           $order_info['member_typ']=$account_result['rec']['__member_type'] ;
           $order_info['name']=$account_result['rec']['__contact_name'] ;
           $order_info['email']=$account_result['rec']['email'] ;
           $order_info['phone']=$account_result['rec']['phone'] ;

           // информация заказа - ДАННЫЕ ПО ОРГАНИЗАЦИИ
           $order_info['org_name']=$_POST['ACCOUNT']['org_name'] ;
           $order_info['rekvezit']=$_POST['ACCOUNT']['rekvezit'];

           // информация заказа - ДОСТАВКА
           $order_info['adres']=$_POST['ACCOUNT']['zip']." ".$_POST['ACCOUNT']['state']." ".$_POST['ACCOUNT']['city']." ".$_POST['ACCOUNT']['adres'] ;
           $order_info['city']=$_POST['ACCOUNT']['city'];
           $order_info['zip']=$_POST['ACCOUNT']['zip'];

           // информация заказа - ДАННЫЕ ПО АККАУНТУ
           $order_info['account_reffer']=$account_result['rec']['_reffer'] ;

           $order_result=ORDERS()->order_create_by_data($_SESSION['cart'],$order_info) ;

           if ($order_result['type']=='success')
           {  // в случае успешного создания заказа переходим на страницу /orders/complette.php, котороя покажет сообщение о оформлении заказа
              // отдельную страницу требуют гугл для учета конверсии поситителей
              $this->add_element('go_url','/orders/complette.php') ;
              $_SESSION['result_order']=$order_result ;
              $_SESSION['result_account']=$account_result ;
           }
           else
           {  // иначе модальное оконо или предупреждение - на выбор
              $this->add_element('title','Оформление заказа') ;
              $this->add_element('success','modal_window') ;
              ORDERS()->show_message($order_result) ;
           }
       }
       else // ошибка при создании аккаунта
       {
          $account_system->show_message($account_result);
          // готовим модальное окно для вывода результатов ajax
          $this->add_element('success','modal_window') ;
          $this->add_element('title','Создание аккаунта') ;
       }
    }

}


function prepare_rec_account()
  {   save_form_values_to_session($_POST) ;
      // подготавливаем данные для регистрации аккаунта на основе _POST
      $account_rec=array() ;
      $account_rec['obj_name']=$_POST['ACCOUNT']['name'] ;
      $account_rec['email']=$_POST['ACCOUNT']['email'] ;
      $account_rec['phone']=$_POST['ACCOUNT']['phone'] ;
      $account_rec['login']=$_POST['ACCOUNT']['login'] ;
      $account_rec['city']=$_POST['ACCOUNT']['city'] ;
      $account_rec['adres']=$_POST['ACCOUNT']['adres'] ;
      $account_rec['password']=$_POST['ACCOUNT']['password'] ;
      $account_rec['clss']=87 ;

      // для организаций немного другой порядок полей
      if ($_POST['ACCOUNT']['member_type']==1)
      { $account_rec['clss']=88 ;
        $account_rec['obj_name']=$_POST['ACCOUNT']['org_name'] ;
        $account_rec['contact_name']=$_POST['ACCOUNT']['name'] ;
        $account_rec['rekvezit']=$_POST['ACCOUNT']['rekvezit'] ;
        $account_rec['ext_info']['status']=$_POST['ACCOUNT']['status'] ;
        $account_rec['ext_info']['site']=$_POST['ACCOUNT']['site'] ;
      }

      // выделяем в отдельный массив поля, которые будут сериализированы
      $account_rec['ext_info']['country']=$_POST['ACCOUNT']['country'] ;
      $account_rec['ext_info']['region']=$_POST['ACCOUNT']['region'] ;
      $account_rec['ext_info']['zip']=$_POST['ACCOUNT']['zip'] ;
      return($account_rec) ;
  }
