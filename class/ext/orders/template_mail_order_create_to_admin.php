<?
function template_mail_order_create($options=array())
{
    global $goods_system,$mail_system ;
      $order_goods=&$order_info['goods'] ;
      $order_service=&$order_info['service'] ;
      //damp_array($order_goods) ;
      //damp_array($order_service) ;

      $params=array() ;
      $params['data']=date("d.m.y G:i") ;
      $params['name']=$order_info['name'] ;
      $params['order_name']=$order_info['obj_name'] ;
      $status_usl='http://'._MAIN_DOMAIN.'/orders/status/'.$order_info['uid'] ;
      $params['status_info']='<a href="'.$status_usl.'">'.$status_usl.'</a>' ;
      $params['order_price']=($order_info['end_price'])? 'Сумма к оплате: '.$order_info['end_price'].$goods_system->VL_main_ue:'---' ;
      /// !!!!
      //if ($this->include_price_shiping_to_order)  if (sizeof($order_service['shiping'])) $params['order_price'].=', в том числе доставка "'.$order_service['shiping']['obj_name'].'" - '._format_price($order_service['shiping']['price'],array('null_price_text'=>'Бесплатно')) ;

      $params['order_typ_oplaty']=(isset($order_info['payments_type']))? 'Тип оплаты: '.$_SESSION['IL_payments_type'][$order_info['payments_type']]['obj_name']:'' ;

      if ($order_info['payments_method']=='robox') $params['order_typ_oplaty'].='. Если Вы не оплатили свой заказ сразу, для оплаты перейдите по этой <a href="http://'._MAIN_DOMAIN.'/orders/payments/'.$order_info['uid'].'">ссылке</a>' ;
      //if ($order_info['kupon_number']) list($sales_summa,$sales_kupon,$alert)=$this->check_kupon($order_info['kupon_number'],$_POST['kupon']['pin'],$use_cart->end_price) ;

      if (sizeof($order_goods))
       { $_temp=array() ;
   	  $_temp[]='<tr class=header><td>Код</td><td>Артикул</td><td>Фото</td><td>Наименование</td><td>Цена</td><td>Количество</td><td>Скидка</td><td>Стоимость</td></tr>' ;
         foreach ($order_goods as $rec)
         {  // переносим информацию по ценам за запись в товару
            $cart_goods_info=array_merge($rec['__reffer_from'],$rec['__reffer_from']['price_info']) ; unset($cart_goods_info['price_info']) ; //damp_array($cart_goods_info) ;
         	 $price_info=$this->prepare_price_info($cart_goods_info) ; //damp_array($price_info) ;
         	 $href=(strpos($rec['__href'],'http://')!==0)? _PATH_TO_SITE.$rec['__href']:$rec['__href'] ;
            $name=($rec['__href'])? '<a href="'.$href.'">'.$cart_goods_info['obj_name'].'</a>':$cart_goods_info['obj_name'] ; // obj_name - запись уже не товара, а позиции заказа
            $stock_info=(!$rec['__stock_info'] and 0)? '<div class=no_stock>Нет в наличии, срок ожидания может составить от 2 до 6 недель</div>':'' ;
            $img=($rec['_image_name'])?  '<img src="'.img_clone($rec,'small').'" alt="">':'' ;
            $sortament_info=(is_object($_SESSION['1c_system']) and sizeof($rec['_sortament_rec']))? $_SESSION['1c_system']->get_sortament_info($rec['_sortament_rec'],array('delimiter'=>'<br>','show_barcode'=>1)):'' ;
            // $stock_info=''
            // ;
            $_temp[]='<tr><td>'.$rec['pkey'].'</td>'.
                          '<td>'.$rec['__art'].'</td>'.
                          '<td class=left>'.$img.'</td>'.
                      	   '<td class=left>'.$name.$sortament_info.$stock_info.'</td>'.
                          '<td align=right>'.$price_info['price'].'</td>'.
                          '<td align=center>'.$price_info['cnt'].'</td>'.
                          '<td>'.$price_info['sales_title'].'</td>'.
                          '<td align=right>'.$price_info['sum_price'].'</td>'.
                      '</tr>' ;
         }

         if (sizeof($order_service)) foreach($order_service as $rec)
         { $price=($rec['price']>0)? _format_price($rec['price'],array('use_if_null'=>'-')):'' ;
           $_temp[]='<tr><td></td><td></td><td></td><td><div class=name>'.$rec['obj_name'].'</td><td align=right>'.$price.'</td><td></td><td></td><td align=right>'.$price.'</td></tr>' ;
         }
         //if ($order_info['kupon_sales']>0) $_temp[]='<tr><td colspan=2>Скидка по купону № '.$order_info['kupon_number'].'</td><td align=left></td><td>&nbsp;</td><td>&nbsp;</td><td  align=right>'._format_price($order_info['kupon_sales']).'</td></tr>' ;
         $_temp[]='<tr valign=top><td colspan=7>ИТОГО</td><td align=right><strong>'._format_price($order_info['end_price']).'</strong></td></tr>' ;
         $params['order_goods']='<table>'.implode('',$_temp).'</table>' ;
       } else $params['order_goods']="" ;

      //$options['debug']=2 ;

      $_temp=array() ;
      									$_temp[]='<tr class=header><td colspan=2>Информация по покупателю</td></tr>' ;
      if ($order_info['member_typ']) 	$_temp[]='<tr><td>Получатель заказа</td><td>'.$order_info['member_typ'].'</td></tr>' ;
      if ($order_info['org_name']) 	$_temp[]='<tr><td>Организация</td><td>'.$order_info['org_name'].'</td></tr>' ;
   								   	$_temp[]='<tr><td>Контактное лицо</td><td>'.$order_info['name'].'</td></tr>' ;
   								   	$_temp[]='<tr><td>Телефон для связи</td><td>'.$order_info['phone'].'</td></tr>' ;
   								   	$_temp[]='<tr><td>E-mail</td><td>'.$order_info['email'].'</td></tr>' ;
   								   	$_temp[]='<tr><td>Адрес доставки</td><td>'.$order_info['city'].' '.$order_info['adres'].'</td></tr>' ;
      if ($order_info['rekvezit']) 	$_temp[]='<tr><td>Реквизиты</td><td>'.$order_info['rekvezit'].'</td></tr>' ;
      									$_temp[]='<tr><td>Примечание</td><td>'.$order_info['comment'].'</td></tr>' ;
      //if ($order_info['kupon_number']) $_temp[]='<tr><td>Номер купона</td><td>'.$order_info['kupon_number'].'</td></tr>' ;
      //if ($order_info['kupon_pin'])    $_temp[]='<tr><td>PIN купона</td><td>'.$order_info['kupon_pin'].'</td></tr>' ;
      //if ($order_info['kupon_sales'])  $_temp[]='<tr><td>Скидка по купону</td><td>'.$order_info['kupon_sales'].'</td></tr>' ;

      $params['member_info']='<table>'.implode('',$_temp).'</table>' ;

      $mail_system->send_mail_to_pattern($order_info['email'],'orders_system_info_create_order',$params,$options) ;

      if ($order_info['referer_host']) $_temp[]='<tr><td>Откуда переход</td><td>'.$order_info['referer_host'].'</td></tr>' ;
      $params['member_info']='<table>'.implode('',$_temp).'</table>' ;

      if (sizeof($order_info['files']))
        {  $_temp=array() ;
           foreach($order_info['files'] as $file_info) $_temp[]='<a href="'.$file_info['__href'].'">'.$file_info['obj_name'].'</a>' ;
           $params['files_info']='К заказу приложены файлы: '.implode(', ',$_temp) ;
        }

      $options['reg_events']=1 ;
      //$options['use_event_id']=0 ;
      if ($order_info['email']!=_EMAIL_ENGINE_DEBUG) $mail_system->send_mail_to_pattern($_SESSION['LS_orders_system_email_alert'],'orders_system_info_create_order_alert',$params,$options) ;
      else                                           $mail_system->send_mail_to_pattern(_EMAIL_ENGINE_DEBUG,'orders_system_info_create_order_alert',$params,$options) ;
}
?>