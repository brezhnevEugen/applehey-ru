<?php   // корзина
include(_DIR_TO_CLASS.'/c_page.php');
class c_page_orders extends c_page
{
  public $h1='Ваша корзина' ;
  public $title='Ваш заказ' ;
  public $path=array('/'=>'Главная') ;

  function set_head_tags()
  { parent::set_head_tags() ;
    //расширение - карусель товаров
    //$this->HEAD['js'][]=_PATH_TO_EXT.'/orders/script.js' ;
    $this->HEAD['css'][]=_PATH_TO_EXT.'/orders/style.css' ;
  }

  function block_main()
  {	?><h1 class="cart">Ваша корзина</h1><?
    include_once('panel_cart_main.php');
    panel_cart_main($_SESSION['cart']->goods) ;
  }


}

?>