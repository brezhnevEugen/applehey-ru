<?
function panel_form_checkout_full_member()
 {   global $member ;  // damp_array($member) ;
     if ($member->id)
     { if (!$_SESSION['form_values']['name']) $_SESSION['form_values']['name']=$member->info['contact_name'] ;
       if (!$_SESSION['form_values']['adres'])
       {  $arr=array() ;
          if ($member->ext_info['zip'])     $arr[]=$member->ext_info['zip'] ;
          if ($member->ext_info['country']) $arr[]=$member->ext_info['country'] ;
          if ($member->ext_info['region'])  $arr[]=$member->ext_info['region'] ;
          if ($member->info['city'])        $arr[]=$member->info['city'] ;
          if ($member->info['adres'])       $arr[]=$member->info['adres'] ;
          if (sizeof($arr))  $_SESSION['form_values']['adres']=implode(', ',$arr) ;
       }
     }
     ?><div id="panel_form_checkout_full"><form>
        <table class="contact_form"><colgroup><col id='c1'><col id='c2'></colgroup>
          <?if (!$member->id){?>
              <tr><td></td>
                  <td id=member_type_select>
                  <label><input tabindex=1 type="radio" name=member_type value=1 checked> Организация </label>&nbsp;&nbsp;&nbsp;<label><input type="radio" name=member_type value=2> Частное лицо </label></td>
              </tr><?}?>
              <tr only="org">
                <td>Организация:</td>
                <td><?if (!$member->id){?><input  tabindex=7 name="org_name" type="text" data-required value="<?echo_form_saved('org_name');?>" /><?}else echo $member->info['obj_name']?></td>
              </tr>
              <?if (!$member->id){?>
              <tr>
                 <td>Фамилия: </td>
                 <td><input  name="name_f" type="text" data-required value="<?echo_form_saved('name_f');?>" /></td>
	          </tr>
              <tr>
                 <td>Имя: </td>
                 <td><input  name="name_i" type="text" data-required value="<?echo_form_saved('name_i');?>" /></td>
	          </tr>
              <tr>
                 <td>Отчество: </td>
                 <td><input  name="name_o" type="text" data-required value="<?echo_form_saved('name_o');?>" /></td>
	          </tr>
              <?}else{?><tr><td>ФИО:</td><td><?echo $member->info['contact_name']?></td><?}?>
              <tr>
                 <td>E-mail:</td>
                 <td><input name="email" type="text" data-required value="<?echo_form_saved('email');?>" /></td>
              </tr>
              <tr>
                 <td>Мобильный телефон: </td>
                 <td><input name="phone" type="text" data-required value="<?echo_form_saved('phone');?>" mask="+9 (999) 999-99-99" /><div class="comment">Пример: +7918456789</div></td>
 	          </tr>
              <tr>
                <td>Способ доставки:</td>
                <td><select class="v2 data-select" cmd=orders/update_shipping_type>
                   <? if (sizeof($_SESSION['IL_type_shiping'])) foreach($_SESSION['IL_type_shiping'] as $rec)
                      { $selected=($_SESSION['cart']->type_shiping==$rec['id'])? 'selected':'' ;
                        $price=($rec['id']==$_SESSION['cart']->service['shiping']['id'])?  $_SESSION['cart']->service['shiping']['price']:$rec['price'] ;
                        $format_price=($price>0)? _format_price($price):'бесплатно' ;
                        ?><option <?echo $selected?> value="<?echo $rec['id']?>"><?echo $rec['obj_name']?> - <span class=price><?echo $format_price?></span></option><?
                      }
                   ?>
                  </select>
                  <div class="comment"><? if ($_SESSION['cart']->service['shiping']['__price_used_sales_text']) echo $_SESSION['cart']->service['shiping']['__price_used_sales_text'];?>
                  </div>
                </td>
             </tr>
             <tr id="trans_select" class="<?if ($_SESSION['cart']->type_shiping!=3) echo 'hidden'?>">
                <td>Транспортная компания:</td>
                <td><select class="v2 data-select" name="trans_select">
                       <? if (sizeof($_SESSION['IL_trans'])) foreach($_SESSION['IL_trans'] as $rec)
                          {?><option value="<?echo $rec['id']?>"><?echo $rec['obj_name']?></option><?}
                       ?>
                      </select>
                </td>
	         </tr>
             <tr id="adres_dost" class="<?if ($_SESSION['cart']->type_shiping==1) echo 'hidden'?>">
                <td>Адрес доставки:</td>
                <td><textarea name="adres" wrap="on" data-required><?echo_form_saved('adres');?></textarea><br>
                    <?if ($_SESSION['cart']->type_shiping==2){?><div class="comment" id="com_dost_curer">Адрес, метро</div><?}?>
                    <?if ($_SESSION['cart']->type_shiping==3){?><div class="comment" id="com_dost_post">Индекс, область, город, адрес</div><?}?>
                </td>
	         </tr>
             <tr id="time_dost" class="<?if ($_SESSION['cart']->type_shiping!=2) echo 'hidden'?>">
                <td>Время доставки:</td>
                <td><input  name="time_dost" type="text" value="<?echo_form_saved('time_dost');?>" /><br>
                    <div class="comment">Укажите время, в которое Вам будет удобно получить Ваш заказ</div>
                </td>
	         </tr>
              <tr>
                <td>Способ оплаты:</td>
                <td>
                    <select class="v2 data-select" ext=orders cmd=update_payments_type>
                      <? if (sizeof($_SESSION['IL_payments_type'])) foreach($_SESSION['IL_payments_type'] as $rec)
                        { $selected=($_SESSION['cart']->payments_type==$rec['id'])? 'selected':'' ;
                          ?><option <?echo $selected?> value="<?echo $rec['id']?>"><?echo $rec['obj_name']?></option><?
                        }
                      ?></select>
                </td>
              </tr>
              <tr><td>Состав заказа:</td>
                  <td><div id="order_result">
                    <? include_once('list_goods_table_cart_no_edit.php');
                       list_goods_table_cart_no_edit($_SESSION['cart']->goods) ;
                    ?></div>

              </td></tr>
              <tr><td>Комментарий к заказу:</td>
                  <td><textarea class="comment" name="comment"></textarea>
              </td></tr>
              <tr><td><td><button class="v1" href="/orders/">< Вернуться в корзину</button>
                                         <input type="submit" class=" go_checkout v2" value="Оформить заказ" cmd=orders/order_create validate=form>
              </td></tr>
	  </table><br>

	 </form>
     </div>
     <script type="text/javascript">
             $j('div#panel_form_checkout_full').ajax_check({content_class:'alert',position:{x: ['right', 'outside'],y:'center'}}) ;
             $j('div#panel_form_checkout_full input').live('focus',function(){$j(this).removeClass('input_error')}) ;

             $j(document).ready(function()
                       { $j('div#panel_form_checkout_full input[name="member_type"]').click(function()  { set_form_mode($j(this).val()) ; })
                         $j('div#panel_form_checkout_full input[name="email"]').change(function()  { $j('div#panel_form_checkout_full input[name="login"]').val($j(this).val()) ; })
                         set_form_mode(1) ;
                       }) ;

             function set_form_mode(mode)
             {  if (mode==1)
                 { $j('div#panel_form_checkout_full tr[only="org"]').show() ;
                   $j('div#panel_form_checkout_full tr[only="org"] :input[type="text"]').removeAttr('disabled') ;
                   $j('div#panel_form_checkout_full tr[only="org"] :input[_data-required]').attr({"data-required":true}) ;
                   $j('div#panel_form_checkout_full tr[only="org"] :input[_data-required]').removeAttr("_data-required") ;

                   $j('div#panel_form_checkout_full tr[only="member"]').hide() ;
                   $j('div#panel_form_checkout_full tr[only="member"] :input[type="text"]').attr('disabled',true) ;
                   $j('div#panel_form_checkout_full tr[only="member"] :input[data-required]').attr({"_data-required":true}) ;
                   $j('div#panel_form_checkout_full tr[only="member"] :input[data-required]').removeAttr("data-required") ;
                 }
                else
                 { $j('div#panel_form_checkout_full tr[only="member"]').show() ;
                   $j('div#panel_form_checkout_full tr[only="member"] :input[type="text"]').removeAttr('disabled') ;
                   $j('div#panel_form_checkout_full tr[only="member"] :input[_data-required]').attr({"data-required":true}) ;
                   $j('div#panel_form_checkout_full tr[only="member"] :input[_data-required]').removeAttr("_data-required") ;

                   $j('div#panel_form_checkout_full tr[only="org"]').hide() ;
                   $j('div#panel_form_checkout_full tr[only="org"] :input[type="text"]').attr('disabled',true) ;
                   $j('div#panel_form_checkout_full tr[only="org"] :input[data-required]').attr({"_data-required":true}) ;
                   $j('div#panel_form_checkout_full tr[only="org"] :input[data-required]').removeAttr("data-required") ;
                 }

             }

         </script>
  <?
 }
?>