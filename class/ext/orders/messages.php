<?
 // сообщение о пустой корзине
 function orders_message_space_cart() { ?>Ваша корзина пуста<?}

 function orders_message_error_check_code()
 {?><p class=alert>Вы неправильно ввели цифровой проверочный код. Пожалуйста, попробуйте еще раз.</p><?}

 // сообщение о успешном оформлении заказа
 function orders_message_order_create_success($order)
   {   ?><script type='text/javascript'>$j(document).ready(function(){$j('body').oneTime(3000,'ya_asyns_timer',function(){yaCounter28778656.reachGoal('ORDER');});})</script>
       <p>Уважаемый(ая) <strong><?echo $order['name']?></strong>!</p><?
      // при разных методах оплаты будут разные сообщения по результату оформления заказа
      if ($order['payments_method'])
      { list($payments_service,$payments_method)=explode('/',$order['payments_method']) ;
        include_once(_DIR_EXT.'/'.$payments_service.'/'.$payments_service.'.php') ;
        $func_name='panel_payments_by_'.$payments_service ;
        if (function_exists($func_name))  $func_name($order,$payments_method)  ;
      }
      else
       { ?><p>Вами был оформлен заказ, <strong><?echo $order['obj_name'] ?></strong>.<br>
       	  	  На указанный Вами адрес электронной почты отправлено уведомление о оформлении заказа.<br>
       		  В течении рабочего дня менеджер свяжется с вами.</p>
           <p><strong>Спасибо за покупку!</strong></p>

         <?
       }
   }

 // сообщение о успешном оформлении заказа
 function orders_message_fast_order_create_success($order)
   {  ?><h2>Уважаемый <strong><?echo  $order['_name']?></strong>!</h2>
        <p>Вами был оформлен заказ, <strong><?echo $order['obj_name'] ?></strong>.</p>
       	<p>В ближайшее время с Вами по указанному Вами контактному телефону свяжется менеджер.</p>
        <p><strong>Спасибо за покупку!</strong></p>
      <?
   }

 // сообщение о невозможности оформлении заказа
 function orders_message_order_create_error($order)
   { ?><h2>Заказ НЕ оформлен</h2>
	     <p>Уважаемый <strong><?echo $order['_name']?></strong>!<br>
			К сожалению, оформление Вашего заказа в настоящий момент невозможно по техничеким причинам.<br>
			Вы можете оформить заказ по телефону или произвести его поздее.<br>
			Техническому администратору сайта направлено уведомление о возникщем сбое, в самое ближайшее время работа системы заказав будет восстановлена.<br><br>
		 </p>
         <p><strong>Надеемся на Ваше понимание!</strong>
         <div class=center><button href="/">Вернутся на сайт</button></div>
	   <?
   }

?>