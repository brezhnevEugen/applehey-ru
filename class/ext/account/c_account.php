<?php
include_once('messages.php') ;
include(_DIR_TO_CLASS.'/c_page.php') ;
class c_page_account extends c_page
{
  public $h1='Авторизация на сайте' ;
  public $path=array('/'=>'Главная страница') ;

  //function panel_account_top() {} // скрываем панель авторизации в шапке сайта
  //function panel_account() {} // скрываем панель авторизации в правой панели сайта
  //function panel_login() {}

  function on_send_header_before()
  {
    if ($_SESSION['member']->id) redirect_301_to('/cabs/') ; // если пользователь авторизован - работа только через личный кабинет
  }


 function block_main()
  {
    $this->page_title() ; // заголовок страниы

    include_once(_DIR_TO_SITE_EXT.'/account/panel_autorize_new.php') ;
    panel_autorize_new() ;

    /*?><h1>Регистрация на сайте:</h1><?

      account_panel_form_create_account() ; // форма создания нового аккаунта
    */
  }
} 
?>