<?php
include_once('messages.php') ;
include("c_account.php");
class c_page_account_forgot extends c_page_account
{
  public $body_class='account' ;
  public $h1='Забыли пароль?' ;
  public $title='Восстановление пароля' ;

  function block_main()
  { ?>
    <div class="main">  <?
    $this->page_title() ;
    include_once(_DIR_EXT.'/account/panel_form_forgot_pass.php') ;
    panel_form_forgot_pass() ;
    include_once(_DIR_EXT.'/account/panel_account_promt.php') ;
    panel_account_promt() ; ?>
    </div>
         <div class="right"><h2>Вход через соц сети</h2></div>
         <div class="clear"></div><?
  }
}
?>