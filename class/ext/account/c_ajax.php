<?
include_once('messages.php') ;
include(_DIR_TO_ENGINE.'/c_page_XML_AJAX.php') ;
class c_page_ajax extends c_page_XML_AJAX
{

  function prepare_rec_account()
  {   save_form_values_to_session($_POST) ;
     // damp_array($_POST) ;
    // подготавливаем данные для регистрации аккаунта на основе _POST
    $account_rec=array() ;
    $account_rec['obj_name']=$_POST['name'].' '.$_POST['forname'].' '.$_POST['lastname'] ;
    $account_rec['email']=$_POST['email'] ;
    $account_rec['phone']=$_POST['phone'] ;
    $account_rec['login']=$_POST['login'] ;
    $account_rec['city']=$_POST['city'] ;
    $account_rec['adres']=$_POST['adres'] ;
    $account_rec['password']=$_POST['password'] ;
    $account_rec['subscript']=1 ;
    $account_rec['clss']=87 ;

    // для организаций немного другой порядок полей
    if ($_POST['member_type']==1)
    { $account_rec['clss']=88 ;
      $account_rec['obj_name']=$_POST['org_name'] ;
      $account_rec['contact_name']=$_POST['name'].' '.$_POST['forname'].' '.$_POST['lastname'] ; ;
      $account_rec['rekvezit']=$_POST['rekvezit'] ;
      $account_rec['ext_info']['status']=$_POST['status'] ;
      $account_rec['ext_info']['site']=$_POST['site'] ;
    }

    // выделяем в отдельный массив поля, которые будут сериализированы
    $account_rec['ext_info']['country']=$_POST['country'] ;
    $account_rec['ext_info']['region']=$_POST['region'] ;
    $account_rec['ext_info']['zip']=$_POST['zip'] ;
      return($account_rec) ;
  }

  function account_create()
  { global $account_system ;
    $account_rec=$this->prepare_rec_account() ;
   //   damp_array($account_rec) ;
    $result=$account_system->create_account($account_rec,array('no_act_code'=>0)) ;
    //damp_array($result) ;
    $account_system->show_message($result);
    // готовим модальное окно для вывода результатов ajax
    $this->add_element('success','modal_window') ;
    $this->add_element('title','Создание аккаунта') ;
    // для аккаунтов с активацией
    if ($result['type']=='success')
    {   if ($account_system->use_activation) $this->add_element('after_close_go_url','/account/activation/') ;
        else                                 $this->add_element('after_close_go_url','/cabs/') ;
    }
  }

  function account_activation()
  { global $account_system ;
    $result=$account_system->activation_account_by_form($_POST) ;
    $account_system->show_message($result);
    //$this->add_element('result_type',$result['type']) ;
    $this->add_element('success','modal_window') ;
    $this->add_element('title','Активация аккаунта') ;
    if ($result['type']=='success') $this->add_element('after_close_go_url','/cabs/') ;
  }

  function account_forgot()
  { global $account_system ;
    $result=$account_system->forgot_pass($_POST['email']) ;
    $account_system->show_message($result);
    $this->add_element('success','modal_window') ;
    $this->add_element('title','Восстановление пароля') ;
    if ($result['type']=='success') $this->add_element('after_close_go_url','/cabs/') ;
  }

  function get_login_panel()
  { $this->add_element('success','modal_window') ;
    $this->add_element('title','Авторизация на сайте') ;
    $this->add_element('no_close_button',1) ;
    include_once('panel_form_autorize.php') ;
    panel_form_autorize() ;
  }


  function account_login()
  { global $account_system ;
    $result=$account_system->login($_POST['user_login'],$_POST['user_pass']) ;
    // действия после авторизации - выбрать что-то одно
    if ($result['type']=='success') { $this->add_element('go_url','/cabs/') ;   // переходим на страницу личного кабинета
                                      //$this->add_element('update_page','1') ; // просто обновляем страницу
                                  }
    else
    { $this->add_element('success','show_notife') ;
      //$this->add_element('title','Авторизация на сайте') ;
    $account_system->show_message($result); // -> HTML
    }
  }

  function account_logout()
  { global $account_system ;
    $result=$account_system->logout() ;
    $account_system->show_message($result);
    $this->add_element('result_type',$result['type']) ;
    if ($result['type']=='success') $this->add_element('update_page',1) ;  // после выхода - перегружаем текущую страницу
  }

  function check_login()
  { global $account_system ;
    $id=$account_system->check_login($_POST['value']);
    $result=($id=='free')? 1:0 ;
    $this->add_element('result',$result) ;
  }

  function check_email()
  { global $account_system ;
    $id=$account_system->check_email($_POST['value']);
    $result=($id=='free')? 1:0 ;
    $this->add_element('result',$result) ;
  }



}
