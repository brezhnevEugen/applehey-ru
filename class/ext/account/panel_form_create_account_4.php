<?

// форма для создания нового аккаунта
 function panel_form_create_account_4()
 { ?>
   <div id=panel_form_create_account>
     <form>
       <table class=contact_form>
<!--         <tr>-->
<!--           <td id=member_type_select>-->
<!--             <label><input tabindex=1 type="radio" name=member_type value=1 checked> Организация </label>&nbsp;&nbsp;&nbsp;-->
<!--             <label><input type="radio" name=member_type value=2> Частное лицо </label>-->
<!--           </td>-->
<!--         </tr>-->
         <tr>
           <td>
             <div class=title>Ф.И.О.</div>
             <input  name="name" type="text" data-required maxlength="50" value="<?echo htmlspecialchars($_SESSION['form_values']['name']);?>" />    
             <div class="info_marker"><span class="hidden comment">Указанное Вами имя не будет публиковаться на сайте - оно необходимо исключительно для корректного обращения к Вам в письмах.</span></div>
           </td>
         </tr>
         <tr>
           <td>
             <div class=title>E-mail:&nbsp;</div>
             <input name="email" type="text" id=email data-required is_checked="check_email" ext=account alert_text="Этот email уже используется" value="<?echo htmlspecialchars($_SESSION['form_values']['email']);?>" />
             <div class="info_marker"><span class="hidden comment">Пожалуйста, указывайте действующий e-mail - без этого Вы не сможете пройти регистрацию на сайте.</span></div>
           </td>
         </tr>
         <tr>
           <td>
             <div class=title>Придумайте пароль:&nbsp;</div>
             <input name="pass" type="password" maxlength="50" value="" />
             <div class="info_marker"><span class="hidden comment">Вы можете не указывать пароль, в этому случае он будет сгенерирован автоматически.</span></div>
           </td>
         </tr>
         <tr>
           <td>
             <div class=title>Подтвердите пароль:&nbsp;</div>
             <input name="pass_confirm" type="password" maxlength="50" value="" />
             <div class="info_marker"><span class="hidden comment">Пароли должны совпадать.</span></div>
           </td>
         </tr>
       </table>
       <input type="submit" value="Зарегистрироваться" class=v2 ext=account cmd=account_create validate=form>  <br>
       <span class="v1 login" href="/account/login.php">Уже зарегистрированы? Авторизуйтесь.</span>
     </form>
    </div>
    <script type="text/javascript">
        $j('div#panel_form_create_account_4').ajax_check({content_class:'alert',position:{x: ['right', 'outside'],y:'center'}}) ;
    </script>
  <?
 }
?>