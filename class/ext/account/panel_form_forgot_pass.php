<?
  // форма для восстановления забытого пароля
  function panel_form_forgot_pass($options=array())
  { ?>
    <div id="panel_form_forgot_pass">
      <form>
        <div class="contact_form">
          <div class=comment>Введите адрес электронной почты и мы <br>вышлем ссылку для восстановления пароля</div>
          <div class=title>E-mail</div>
          <input  data-validate="email" data-required placeholder="primer@mail.ru" name="email" type="text" value="<?echo_form_saved('email');?>">
          <div class="check_code">
            <div class=text_capha>Введите проверочный код</div>
            <img alt="" src="/images/img_check_code.php" border="0" class="checkcode">
            <!--<img class=refresh_checkcode src = "/images/refresh_small.png" alt = "refresh">-->
            <input data-required class=checkcode name="_check_code" type="text">

          </div>
          <input type="submit"  class="button_green v2" id=submit value="Восстановить" cmd=account/account_forgot validate=form>
        </div>
      </form>
    </div><?
  }
?>