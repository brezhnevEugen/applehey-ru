<?

// форма для создания нового аккаунта
 function panel_form_create_account_2()
  { ?><div id=panel_form_create_account_2>
       <form>
         <div class=info>Внимание! Регистрация на сайте не свободная, а с одобрения администрации сайта. После заполнения регистрационной формы с Вами свяжется менеджер, после чего Вам или будет предоставлен доступ в личный кабинет, или будет отказано.  Администрация сайта оставляет за собой право отказать в доступе к личному кабинету без объяснения причин.</div>
         <table class=contact_form><colgroup><col id='c1'><col id='c2'></colgroup>
                  <tr><td colspan=2><h2>Представтесь:</h2></td></tr>
                     <tr>
                        <td class=title>Фамилия Имя Отчество:&nbsp;</td>
                        <td><input  name="name" type="text" data-required maxlength="50" value="<?echo htmlspecialchars($_SESSION['form_values']['name']);?>" /><br>
                            <span class=comment>Указанное Вами имя не будет публиковаться на сайте - оно необходимо исключительно для корректного обращения к Вам в письмах.</span>
                        </td>
                     </tr>
                     <tr>
                        <td class=title>Организация:&nbsp;</td>
                        <td><input  name="org_name" type="text" data-required maxlength="50" value="<?echo htmlspecialchars($_SESSION['form_values']['EXT']['org_name']);?>" /></td>
                     </tr>
                     <tr>
                        <td class=title>Должность:&nbsp;</td>
                        <td><input  name="status"  type="text" data-required maxlength="50" value="<?echo htmlspecialchars($_SESSION['form_values']['EXT']['status']);?>" /></td>
                     </tr>
                     <tr>
                        <td class=title>Телефон:&nbsp;</td>
                        <td><input  name="phone" type="text" data-required maxlength="50" value="<?echo htmlspecialchars($_SESSION['form_values']['EXT']['phone']);?>" /><br>
                            <span class=comment>Телефонный номер в формате +7 ххх ххх-хх-хх</span>
                        </td>
                     </tr>
                     <tr>
                       <td class=title>Ваш E-mail:&nbsp;</td>
                       <td><input name="email" type="text" id=email data-required is_checked="check_email" ext=account alert_text="Этот email уже используется" value="<?echo htmlspecialchars($_SESSION['form_values']['email']);?>" /><br>
                           <span class=comment>Пожалуйста, указывайте действующий e-mail - без этого Вы не сможете пройти регистрацию на сайте.</span>
                       </td>
                     </tr>
                     <tr>
                       <td class=title>Сайт вашей компании:&nbsp;</td>
                       <td><input name="site" type="text" maxlength="50" value="<?echo htmlspecialchars($_SESSION['form_values']['EXT']['site']);?>" /><br>
                           <span class=comment>Если у Вашей компании есть сайт - укажите его.</span>
                       </td>
                     </tr>
                     <tr><td colspan=2><h2>Данные для входа на сайт:</h2></td></tr>
                     <tr>
                        <td class=title>Ваш псевдоним (логин):&nbsp;</td>
                        <td><input  name="login" type="text" id=login data-required is_checked="check_login" ext=account alert_text="Этот логин уже используется" maxlength="50" value="<?echo htmlspecialchars($_SESSION['form_values']['login']);?>" /><br>
                            <span class=comment>Указанный Вами псевдоним будет использоваться в качестве логина при авторизации на сайте.</span>
                        </td>
                      </tr>
                      <tr>
                        <td class=title>Ваш пароль:&nbsp;</td>
                        <td><input name="pass" type="password" maxlength="50" value="" /><br />
                            <span class=comment>Вы можете не указывать пароль, в этому случае он будет сгенерирован автоматически.</span>
                         </td>
                      </tr>
                      <tr>
                        <td class=title><img src="/images/img_check_code.php" border="0" class=checkcode></td>
                        <td>Пожалуйста, укажите здесь числовой код:<br><input data-required name="_check_code" type="text" ></td>
                      </tr>
          </table>
          <div class="center">
                <br>
                <input type="submit" value="Зарегистрироваться" class=v2 ext=account cmd=account_create validate=form>
          </div>
         </form>
    </div>
    <script type="text/javascript">
        $j('div#panel_form_create_account_2').ajax_check({content_class:'alert',position:{x: ['right', 'outside'],y:'center'}}) ;
    </script>
  <?
 }
?>