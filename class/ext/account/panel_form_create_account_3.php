<?

// форма для создания нового аккаунта
 function panel_form_create_account_3()
  { ?><div id=panel_form_create_account class="panel_form_create_account_3">
       <form id=myForm>
         <div class=info></div>
         <table class=contact_form><colgroup><col id='c1'><col id='c2'><col id='c3'><col id='c4'></colgroup>
                 <tr><td></td>
                     <td id=member_type_select>
                        <label><input tabindex=1 type="radio" name=member_type value=1 checked> Организация </label>&nbsp;&nbsp;&nbsp;<label><input type="radio" name=member_type value=2> Частное лицо </label>
                     </td>
                     <td></td>
                     <td></td>
                 </tr>
                 <tr><td></td>
                     <td><h2>Контактная информация:</h2></td>
                     <td></td>
                     <td></td>
                 </tr>
                 <tr>
                    <td class=title>Фамилия:</td>
                    <td><input  tabindex=2 name="name" type="text" data-required value="<?echo_form_saved('name');?>" /></td>
                    <td class=title>Телефон:</td>
                    <td><input tabindex=5 name="phone" type="text" data-required value="<?echo_form_saved('phone');?>" mask="+9 (999) 999-99-99"/></td>
                  </tr>
                  <tr>
                    <td class=title>Имя:</td>
                    <td><input  tabindex="3" name="forname" type="text" data-required value="<?echo_form_saved('forname',2);?>" /></td>
                    <td class=title>Ваш E-mail:</td>
                    <td><input tabindex=6 name="email" type="text" id=email data-required is_checked="check_email" ext=account alert_text="Этот email уже используется" value="<?echo_form_saved('email');?>" /></td>
                  </tr>
                  <tr>
                      <td class=title>Отчество:</td>
                      <td><input tabindex=4  name="lastname" type="text" value="<?echo_form_saved('lastname');?>" />
                      </td>
                      <td></td>
                      <td><span class=comment>Пожалуйста, указывайте действующий e-mail - без этого Вы не сможете пройти регистрацию на сайте.</span></td>
                  </tr>
                  <tr><td></td>
                     <td><h2>Ваш аккаунт:</h2></td>
                     <td></td>
                     <td></td>
                  </tr>
                  <tr>
                    <td class=title>Логин:</td>
                    <td><input  tabindex=2 type="text" name=login value="<?echo_form_saved('login',1);?>" readonly="1"/><br>
                        <span class=comment>Вашим логином будет Ваш адрес e-mail</span>
                    </td>
                    <td class=title>Пароль:</td>
                    <td><input tabindex=5 name="password" type="password" value=""/><br>
                        <span class=comment>Если Вы не укажите пароль, он будет сгенерирован автоматически</span>
                    </td>
                   </tr>
                  <tr only="org">
                      <td></td>
                      <td><h2>Информация о организации:</h2></td>
                      <td></td>
                      <td></td>
                  </tr>
                  <tr only="org">
                        <td class=title>Организация:</td>
                        <td><input  tabindex=7 name="org_name" type="text" data-required value="<?echo_form_saved('org_name');?>" /></td>
                        <td  class=title>Реквизиты организации:</td>
                        <td  rowspan=3><textarea tabindex=10 name="rekvezit"><?echo_form_saved('rekvezit')?></textarea>
                                       <span class=comment>Пожалуйста, будьте внимательны! Эта информация будет использована для выставления счета.</span>
                        </td>
                     </tr>
                  <tr only="org">
                        <td class=title>Должность:</td>
                        <td><input tabindex=8 name="status"  type="text" data-required value="<?echo_form_saved('status')?>" /></td>
                        <td></td>
                     </tr>
                     <tr  only="org">
                       <td class=title>Сайт вашей компании:</td>
                       <td><input tabindex=9 name="site" type="text" value="<? echo_form_saved('site')?>" /><br>
                           <span class=comment>Если у Вашей компании есть сайт - укажите его.</span>
                       </td>
                       <td></td>
                     </tr>
                     <tr><td></td><td colspan="3"><h2>Информация для доставки:</h2></td></tr>
                        <tr>
                           <td class=title>Страна:</td>
                           <td><input tabindex=11 name="country" type="text"  value="<?echo_form_saved('country');?>" /></td>
                           <td class=title>Регион:</td>
                           <td><input tabindex=12 name="region" type="text" value="<?echo_form_saved('region');?>" /></td>
                        </tr>
                        <tr>
                           <td class=title>Город:</td>
                           <td><input  tabindex=13 name="city" type="text"  value="<?echo_form_saved('city');?>" /></td>
                           <td class=title>Почтовый адрес:</td>
                           <td><input  tabindex=14 name="adres" type="text" value="<?echo_form_saved('adres');?>" /></td>
                        </tr>
                        <tr>
                           <td class=title>Почтовый индекс:</td>
                           <td><input tabindex=15 name="zip" type="text" value="<?echo_form_saved('zip');?>" class="small" mask="999999"/></td>
                           <td></td>
                           <td></td>
                        </tr>
                      <tr>
                        <td class=title><img src="/images/img_check_code.php" border="0" class=checkcode></td>
                        <td>Пожалуйста, укажите здесь числовой код:<br><input tabindex=16 data-required name="_check_code" type="text" ></td>
                      </tr>
          </table>
          <div class="center">
                <br>
                <input type="submit" value="Зарегистрироваться" class=v2 validate=form cmd=account/account_create>
          </div>
         </form>
    </div>
    <script type="text/javascript">
        $j('div#panel_form_create_account').ajax_check({content_class:'alert',position:{x: ['right', 'outside'],y:'center'}}) ;
        $j('div#panel_form_create_account input').live('focus',function(){$j(this).removeClass('input_error')}) ;

        $j(document).ready(function()
                  { $j('div#panel_form_create_account input[name="member_type"]').click(function()  { set_form_mode($j(this).val()) ; })
                    $j('div#panel_form_create_account input[name="email"]').change(function()  { $j('div#panel_form_create_account input[name="login"]').val($j(this).val()) ; })
                    set_form_mode(1) ;
                  }) ;

        function set_form_mode(mode)
        {  if (mode==1)
            { $j('div#panel_form_create_account tr[only="org"]').show() ;
              $j('div#panel_form_create_account tr[only="org"] :input[type="text"]').removeAttr('disabled') ;
              $j('div#panel_form_create_account tr[only="org"] :input[_data-required]').attr({"data-required":true}) ;
              $j('div#panel_form_create_account tr[only="org"] :input[_data-required]').removeAttr("_data-required") ;

              $j('div#panel_form_create_account tr[only="member"]').hide() ;
              $j('div#panel_form_create_account tr[only="member"] :input[type="text"]').attr('disabled',true) ;
              $j('div#panel_form_create_account tr[only="member"] :input[data-required]').attr({"_data-required":true}) ;
              $j('div#panel_form_create_account tr[only="member"] :input[data-required]').removeAttr("data-required") ;
            }
           else
            { $j('div#panel_form_create_account tr[only="member"]').show() ;
              $j('div#panel_form_create_account tr[only="member"] :input[type="text"]').removeAttr('disabled') ;
              $j('div#panel_form_create_account tr[only="member"] :input[_data-required]').attr({"data-required":true}) ;
              $j('div#panel_form_create_account tr[only="member"] :input[_data-required]').removeAttr("_data-required") ;

              $j('div#panel_form_create_account tr[only="org"]').hide() ;
              $j('div#panel_form_create_account tr[only="org"] :input[type="text"]').attr('disabled',true) ;
              $j('div#panel_form_create_account tr[only="org"] :input[data-required]').attr({"_data-required":true}) ;
              $j('div#panel_form_create_account tr[only="org"] :input[data-required]').removeAttr("data-required") ;
            }

        }

    </script>
  <?
 }
?>