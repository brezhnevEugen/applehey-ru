<?

//----------------------------------------------------------------------------------------------------------------------
// функция централизированного вывода сообзений о ошибках
//-----------------------------------------------------------------------------------------------------------------------

 // сообщение для клиента в случае успешного создания аккаунта
 function account_message_create_success($rec)
 { //damp_array($rec) ;
   $name=($rec['contact_name'])? $rec['contact_name']:$rec['obj_name'] ;
   ?><p>Уважаемый(ая) <strong><?echo $name?></strong>!</p>
 	 <p>Создание Вашего аккаунта успешно завершено.</p>
     <?if ($rec['act_code']){?><p>Теперь Вам необходимо активировать свой аккаунт.<br>Для этого на указанный Вами e-mail было выслано письмо с инструкцией по активации аккаунта. Это письмо обычно приходит моментально, однако, если Вы не получили его в течении получаса, обратитесь в нашу <a href="/support/">службу поддержки</a> - мы обязательно поможем Вам!</p><?}
       else                 {?><p>На указанный Вами e-mail было выслано письмо с логином и паролем для доступа в личный кабинет. Это письмо обычно приходит моментально, однако, если Вы не получили его в течении получаса, обратитесь в нашу <a href="/support/">службу поддержки</a> - мы обязательно поможем Вам!</p><?}
    ?><p class="center"><button class="cancel">Закрыть</button></p>
   <?
   return(1) ;
 }

 // сообщение для клиента в случае успешной активации аккаунта
 function account_message_activation_success($rec)
 { $name=($rec['contact_name'])? $rec['contact_name']:$rec['obj_name'] ;
   ?><p>Уважаемый(ая) <strong><?echo $name?></strong>!</p>
 	 <p>Активация Вашего аккаунта произведена успешно.</p>
 	 <p>Теперь вы сможете авторизоваться на сайте, Ваш логин и пароль отправлены на Ваш почтовый ящик.</p>
     <p class="center"><button class="cancel">Закрыть</button></p>
   <?
   return(1) ;
 }

 function account_message($text)
 {?><p class=alert><?echo $text?></p><?}

 function account_message_access_denited()
 {?><p class=alert>Доступ запрещен.</p><?}

 function account_message_space_field()
 {?><p class=alert>Вы заполнили  не все необходимые поля. Пожалуйста, попробуйте еще раз.</p><?}

 function account_message_error_field()
 {?><p class=alert>Вы заполнили  не все необходимые поля. Пожалуйста, попробуйте еще раз.</p><?}

 function account_message_dubl_email()
 {?><p class=alert>На указанный Вами e-mail уже существует аккаунт. Выполните <a href=/account/forgot.php>восстановление пароля</a> или используйте для регистрации другой e-mail</p><?}

 function account_message_dubl_change_email()
 {?><p class=alert>На указанный Вами e-mail уже существует аккаунт. Выберите другой e-mail.</p><?}

 function account_message_dubl_login()
 {?><p class=alert>Указанный Вами логин уже занят. Используйте другой логин.</p><?}

 function account_message_email_is_busy($rec)
 {?><div class=alert>email <strong>"<?echo $rec['email']?>"</strong> уже используется</div><?}

 function account_message_login_is_busy($rec)
 {?><div class=alert>Логин <strong>"<?echo $rec['login']?>"</strong> уже используется</div><?}

 function account_message_db_error()
 {?><p class=alert>Создание аккаунта временно заблокировано по техническим причинам. Попробуйте, пожалуйста, повторить попытку спустя некоторое время</p><?}

 function account_message_update_ok()
 {?><p class=alert>Обновление данных произведено успешно</p><?}

 function account_message_email_change_success()
 {?><p class=alert_info>Смена email произведено успешно</p><?}

 function account_message_pass_change_success()
 {?><p class=alert_info>Смена пароля произведено успешно</p><?}

 function account_message_params_change_success()
 {?><p class=alert_info>Данные изменены успешно</p><?}

 function account_message_name_change_success()
 {?><p class=alert_info>Смена имени произведена успешно</p><?}

 function account_message_login_change_success()
 {?><p class=alert_info>Смена логина произведена успешно</p><?}

 function account_message_account_info_change_success()
 {?><p class=alert_info>Информация аккаунта успешно изменена</p><?}

 function account_message_change_success()
 {?><p class=alert_info>Данные успешно сохранены</p><?}

 function account_message_member_not_found()
 {?><p class=alert>Аккаунта не существует или неверно указан пароль</p><?}

 function account_message_nochange()
 {?><p class=alert_info>Изменений не найдено</p><?}

 function account_message_error_check_code()
 {?><p class=alert>Вы неправильно ввели цифровой проверочный код. Пожалуйста, попробуйте еще раз.</p><?}

 function account_message_no_activation_for_member()
 {?><p class=alert>Для активации нового аккаунта Вам необходимо выйти из текущего аккаунта.</p><? return(1) ;}

 function account_message_no_login_for_member()
 { ?><p class=alert>Для авторизации нового аккаунта Вам необходимо выйти из текущего аккаунта.</p>
	<form name="form_account_logout" method="post" action='/account/login.php'>
	  <input name="cmd" type="hidden" value="logout">
	 <div class="center"><input type="submit" value="Выход"></div>
	</form>
  <?
 }

 function account_message_no_create_for_member()
 {?><p class=alert>Для создания нового аккаунта Вам необходимо выйти из текущего аккаунта.</p><?}

 function account_message_activation_code_not_found()
 {
   ?><p class=alert>Аккаунта с данным кодом активации не существует или аккаунт уже активирован. Внимательно скопируйте код из полученного Вами письма и попробуйте еще раз.</p>
     <p>Если Вы забыли пароль для своего аккаунта выполните <a href="/account/forgot.php">восстановление пароля</a></p>
     
   <?
 }

 function account_message_forgot_success()
 {?><p>Новый пароль был выслан на указанный Вами адрес электронной почты</p><p class=buttons><button class="cancel">Закрыть</button></p><?}


?>