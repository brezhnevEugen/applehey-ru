<?php
include_once('messages.php') ;
include('c_account.php');
class c_page_account_logout extends c_page_account
{
  function on_send_header_before()
  {
    $_SESSION['account_system']->logout() ;
    redirect_301_to('/') ;
  }
}
?>