<?
// форма для создания нового аккаунта
function panel_form_create_account_1cols($options=array())
	{ $panel_id="panel_create_account" ;
	  $form_id="form_create_account" ;
      ?><div id="<?echo $panel_id?>">
        <form name="<?echo $form_id?>" id="<?echo $form_id?>" method="POST" action="/ajax.php"><input name="cmd" type="hidden" value="account_create">
	    <table class="contact_form">
          <tr><td colspan=2 class=comment>Обязательные поля отмечены *</td></tr>
	   	    <tr>
                <td colspan=2 >
                    <div class=title>Имя:</div><input  class="text" data-required id=name3 name="name" type="text" value="<?echo $_SESSION['form_values']['name'];?>"><br>
                    <span class=comment>Указанное Вами имя не будет публиковаться на сайте - оно необходимо исключительно для корректного обращения к Вам в письмах.</span>
               </td>
	        </tr>
            <tr>
                <td colspan=2 ><div class=title>Ваш псевдоним (логин):&nbsp;</div><input  class="text" data-required id=login name="login" type="text" value="<?echo $_SESSION['form_values']['login'];?>"><br>
                    <span class=comment>Указанный Вами псевдоним будет использоваться в качестве логина при авторизации на сайте. Также им будут подписаны Ваши сообщения на форуме.</span>
                </td>
	        </tr>

	        <tr><td><div class=title>Email:</div><input class="text " data-validate="email" data-required id=email name="email" type="text" value="<?echo $_SESSION['form_values']['email'];?>"></td>
	        <tr><td><div class=title>Телефон:</div><input class="text " id=phone name="phone" type="text" value="<?echo $_SESSION['form_values']['phone'];?>"></td></tr>
            <tr><td colspan=2>
                <div class=title>Ваш пароль:&nbsp;</div><br>
                <input name="password" id='password' type="password" class=text maxlength="50" value="<?echo $_SESSION['form_values']['pass'];?>" /><br />
            	<span class=comment>Вы можете не указывать пароль, в этому случае он будет сгенерирован автоматически.</span>
            	</td>
            </tr>
	  	    <tr><td colspan=2 class=checkcode><div class=title><img alt="" src="/images/img_check_code.php" border="0" class="checkcode"><!--<img class=refresh_checkcode src = "/images/refresh_small.png" alt = "refresh">--></div><input class="text checkcode isCorner" data-required class=checkcode name="_check_code" type="text"></td></tr>
	  	    <tr><td colspan=2 class=button><input type="submit"  id=submit value="Зарегистрироваться"></td></tr>
	  	    </table>
	   </form>
       </div>
     <?
     prepare_JS_to_show_result_ajax_in_modal_window(array('panel_id'=>$panel_id,'form_id'=>$form_id,'title'=>'Создание аккаунта','mBox_options'=>'onClose:function(){if (result=="create_success") document.location="/account/activation.php";}')) ;
	}
?>