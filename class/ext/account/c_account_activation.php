<?php
include_once('messages.php') ;
include("c_account.php");
class c_page_account_activation extends c_page_account
{ public $h1='Активация аккаунта на сайте' ;
  public $title='Активация аккаунта на сайте' ;

  function block_main()
  { $this->page_title() ;
    include('panel_form_account_activation.php');
    panel_form_account_activation() ; // показываем форму для ввода кода
  }
}
?>