<?
  function list_items_menu_table_with_subitems(&$list_recs,$options=array())
  { global $obj_info,$art_system ;
    $id=($options['id'])? 'id='.$options['id']:'' ;?>
    <table class=list_items_menu_table <?echo $id?> cellpadding="0" cellspacing="0">
      <tr><?
        if (sizeof($list_recs)) foreach($list_recs as $rec)
        { $selected=($rec['href']==_CUR_PAGE_DIR._CUR_PAGE_NAME)? 'selected':'' ;
          if (isset($obj_info) and isset($obj_info['__parents'][1]) and $obj_info['__parents'][1]==$rec['pkey']) $selected='selected' ;
          if ($rec['pkey_istochik'])
          { ?>
            <td class="item <?echo $selected?>">
              <div class="link_style">
                <span class="name"><? echo $rec['obj_name'];?></span>
                <span class="marker" ></span>
              </div><?
              // выводим подразделы текущего раздела
              $art_system->show_list_section($rec['pkey_istochik'],'menu/list_section_ul_a_name',array('class'=>'submenu hidden', 'item_class'=>'subitem','no_any_panel'=>1)) ;
              $art_system->show_list_items($rec['pkey_istochik'],'menu/list_section_ul_a_name',array('class'=>'submenu hidden', 'item_class'=>'subitem','no_any_panel'=>1)) ;?>
            </td><?
          } 
          else
          { ?>
            <td class="item v1 <?echo $selected?>" href="<? echo $rec['href']?>">
              <a href="<? echo $rec['href']?>"><span class="name"><? echo $rec['obj_name'];?></span></a>
            </td><?
          }
        }?>
      </tr>
    </table>
    <script type="text/javascript">
      $j('table#panel_main_menu td.item').hover(
       function(){$j(this).children('ul.submenu').removeClass('hidden') ;},
       function(){$j(this).children('ul.submenu').addClass('hidden') ;}
      );
    </script><?
  }
?>