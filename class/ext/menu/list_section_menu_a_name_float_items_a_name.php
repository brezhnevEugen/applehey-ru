<?
//шаблон - список подразделов для верхнего меню - разделы и всплывающие подразделы
function list_section_menu_a_name_float_items_a_name(&$list_recs,$options = array())
{
    global $menu_system;
    $id = ($options['id'])? 'id=' . $options['id']:'';?>
    <div class = "list_section_menu_a_name_float_items_a_name " <? echo $id; ?> ><?
        if (sizeof($list_recs))
        {
            $i = 0;
            $j = sizeof($list_recs)-1;
            foreach($list_recs as $rec)
            {   $class = array();
                $class[] = 'item';
                $class[] = ($rec['__href']==_CUR_PAGE_DIR . _CUR_PAGE_NAME)? 'selected':'';
                if ($options['item_class']) $class[] = $options['item_class'];
                if (!$i) $class[] = 'first ';
                if ($i==$j) $class[] = 'last ';
                if (sizeof($rec['list_obj'])) $class[] = 'marker';
                $_class = (sizeof($class))? 'class="' . implode(' ',$class) . '"':'';?>
            <div <? echo $_class; ?> id = sect_<? echo $rec['pkey'] ?>>
                <div class="wrapper">
                    <a class = top href = "<? echo $rec['href'] ?>"> <? echo $rec['obj_name'] ?></a><?
                    if (sizeof($rec['list_obj']))
                    {
                        $ii = 0;
                        $jj = sizeof($rec['list_obj'])-1;?>
                        <div class = "list_subitems hidden "><?
                            foreach($rec['list_obj'] as $child_id)
                            {
                                $class_subsection = array();
                                $class_subsection[] = 'subitem';
                                if ($options['item_class']) $class_subsection[] = $options['item_class'];
                                if (!$ii) $class_subsection[] = 'first';
                                if ($ii==$jj) $class_subsection[] = 'last ';
                                $_class_subsection = (sizeof($class_subsection))? 'class="' . implode(' ',$class_subsection) . '"':'';
                                echo '<div ' . $_class_subsection . '><a class=isTextShadow href="' . $menu_system->tree[$child_id]->href . '">' . $menu_system->tree[$child_id]->name . '</a></div>';
                                $ii++;
                            }?>
                        </div><?
                    }?>
                </div>
            </div><?
                $i++;
            }
        }?>
    </div>
    <style type = "text/css"> .hidden
        {
            display : none;
        }  </style>


    <script type = "text/javascript">
        $j(document).ready(function ()
        {
             $j('#<? echo $options['id']; ?> .item').hover(function ()
                                       {
                                           clearTimeout($j.data(this,'timer'));
                                           $j('div.list_subitems',this).stop(true,true).slideDown(200);
                                       },function ()
                                       {
                                           $j.data(this,'timer',setTimeout($j.proxy(function ()
                                                                                    {
                                                                                        $j('div.list_subitems',this).stop(true,true).slideUp(200);
                                                                                    },this),400));

                                       });
         });
      </script>

<?
}

?>