<?
include(_DIR_TO_ENGINE.'/c_page_XML_AJAX.php') ;
include_once(_DIR_EXT.'/panel_filter/i_filter.php') ;
class c_page_ajax extends c_page_XML_AJAX
{
  function add_to_favorite($doc, $xml)
  { $this->add_element('show_notife','Товар добавлен в избранное') ;
    list($pkey, $tkey) = explode('.', $_POST['reffer']);
    $_SESSION['list_goods_favorite'][$pkey] = $pkey;
    include_once(_DIR_EXT . '/pages_favorite/panel_favorite_header.php');
    add_element($doc, $xml, 'panel_favorite_header', get_func_out('panel_favorite_header'));
  }

  function del_from_favorite($doc, $xml)
  { $this->add_element('show_notife','Товар удален из избранного') ;
    list($pkey, $tkey) = explode('.', $_POST['reffer']);
    unset($_SESSION['list_goods_favorite'][$pkey]);
    include_once(_DIR_EXT . '/pages_favorite/panel_favorite_header.php');
    add_element($doc, $xml, 'panel_favorite_header', get_func_out('panel_favorite_header'));
  }
}
