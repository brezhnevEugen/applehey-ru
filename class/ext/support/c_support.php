<?php
include('messages.php') ;
include(_DIR_TO_CLASS.'/c_page.php') ;
class c_page_support extends c_page
{
 public $title='Служба поддержки' ;
 public $h1='Служба поддержки' ;

 function set_head_tags()
 { parent::set_head_tags() ;
   $this->HEAD['css'][]=_PATH_TO_EXT.'/support/style.css' ;
 }

 function block_main()
 {
   $this->page_title() ;

   $this->support_message_default() ; // сообщение о порядке работы службы поддержки

   include_once('panel_support_new_tiket.php') ;
   panel_support_new_tiket() ;  // форма запроса в службу поддержки

 }

 // сообщение-приветствие при прямом переходе на страницу службы поддержки
 function support_message_default()
 { //_info_id(14) ;
 }


}




?>
