<?
// форма для создания запроса в слуюбу поддерки по поводу заказа проекта на сайте
//
 function panel_support_new_tiket_ext($options=array())
  { ?><div id=panel_support_new_tiket_ext  class="<?echo $options['class']?>">
         <form>
         <table class="contact_form <?echo $options['class']?>"><colgroup><col id='c1'><col id='c2'></colgroup>
                     <tr>
                        <td class=title>Ваше Имя:&nbsp;</td>
                        <td><input  name="SUPPORT[name]" type="text" data-required maxlength="50" value="<?echo_form_saved('SUPPORT','name')?>" /><br>
                            <span class=comment>Указанное Вами имя не будет публиковаться на сайте - оно необходимо исключительно для корректного обращения к Вам в письмах.</span>
                        </td>
                     </tr>
                     <tr>
                        <td class=title>Организация:&nbsp;</td>
                        <td><input  name="SUPPORT[org_name]" type="text" data-required maxlength="50" value="<?echo_form_saved('SUPPORT','org_name')?>" /></td>
                     </tr>
                     <tr>
                        <td class=title>Должность:&nbsp;</td>
                        <td><input  name="SUPPORT[status]" type="text" data-required maxlength="50" value="<?echo_form_saved('SUPPORT','status')?>" /></td>
                     </tr>
                     <tr>
                        <td class=title>Телефон:&nbsp;</td>
                        <td><input  name="SUPPORT[phone]" type="text" data-required  maxlength="50" value="<?echo_form_saved('SUPPORT','phone')?>" /><br>
                            <span class=comment>Телефонный номер в формате +7 ххх ххх-хх-хх</span>
                        </td>
                     </tr>
                     <tr>
                       <td class=title>Ваш E-mail:&nbsp;</td>
                       <td><input name="SUPPORT[email]" type="text" data-required data-validate="email" value="<?echo_form_saved('SUPPORT','email')?>" /><br>
                           <span class=comment>Пожалуйста, указывайте действующий e-mail - без этого Вы не сможете пройти регистрацию на сайте.</span>
                       </td>
                     </tr>
                     <tr><td class=title>Подробности Вашего вопроса</td>
                         <td><textarea data-required  name="SUPPORT[value]"></textarea></td></tr>
                      <tr>
                        <td class=title><img src="/images/img_check_code.php" border="0" class=checkcode></td>
                        <td>Пожалуйста, укажите здесь числовой код:<br><input data-required name="_check_code" type="text" ></td>
                      </tr>
          </table>
          <div class="center">
                <br>
                <input type="submit" value="Отправить" class=v2 validate=form cmd=support_create_ticket ext=support>
          </div>
         </form>
    </div>
  <?
 }
?>