<?php
include_once('messages.php') ;
include_once('c_ajax.php') ;
include_once(_DIR_TO_CLASS.'/c_page.php') ;
class c_page_support_complette extends c_page
{
 public $title='Служба поддержки' ;
 public $h1='Служба поддержки' ;

    public $allow_GET_params=array('id','mid') ; // все страницы сайта работают без параметров
    public $checked_GET_params=0 ;     // разрешить проверку параметров URL
    public $body_class='support_tiket' ;


 function block_main()
 { // заголовок
   $this->page_title() ;
   // показ результата команды
   show_message_by_id('support_message',$this->cmd_exec_result) ;
   // форма тикета
   include_once('panel_support_new_tiket.php') ;
   if ($this->cmd_exec_result['type']!='success') panel_support_new_tiket() ;
 }

 // создаем тикет на основе данных формы
 function CMD_support_create_ticket()
 {  global $support_system ;
    $page_ajax=new c_page_ajax() ;
    $data=$page_ajax->prepare_rec_tiket($_POST) ;
    $this->cmd_exec_result=$support_system->create_tiket($data) ;

 }

 // создаем тикет на основе данных формы
 function CMD_support_ticket_add_message()
 {  global $support_system ;
    $page_ajax=new c_page_ajax() ;
    $data=$page_ajax->prepare_rec_tiket_message($_POST) ;
    $this->cmd_exec_result=$support_system->add_message($data) ;
 }

 // создаем запрос на счет на основе данных формы
 function CMD_support_create_ticket_to_chet()
 {  global $support_system ;
    $page_ajax=new c_page_ajax() ;
    $data=$page_ajax->prepare_rec_tiket($_POST) ;
    $data['theme']='Запрос на выставление счета' ;
    $this->cmd_exec_result=$support_system->create_tiket($data) ;
    //damp_array($this->cmd_exec_result) ;
 }


}




?>
