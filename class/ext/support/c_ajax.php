<?
include_once('messages.php') ;
include_once(_DIR_TO_ENGINE.'/c_page_XML_AJAX.php') ;
class c_page_ajax extends c_page_XML_AJAX
{
 /*------------------------------------------------------------------------------------------------------------------------------*/
 //
 // ПОДГОТОВКА ДАННЫХ ДЛЯ СОЗДАНИЯ ЗАПРОСА НА ОСНОВЕ POST
 //
 /*------------------------------------------------------------------------------------------------------------------------------*/

 function prepare_rec_tiket()
 {  global $goods_system,$member ;
    save_form_values_to_session($_POST) ;
    $data=array() ;
    // вносим в запись запроса стандартные поля
    $data['theme']=($_POST['theme'])? $_POST['theme']:'Вопрос в службу поддержки' ;
    $data['name']=$_POST['name'] ;
    $data['email']=$_POST['email'] ;
    $data['phone']=($_POST['phone_pref'])? '('.$_POST['phone_pref'].') '.$_POST['phone']:$_POST['phone'] ;
    $data['value']=$_POST['message'] ;
    // если поля по автору не заполнены и клиент авторизован - берем данные из аккаунта
    if (!$data['name'] and $member->id) $data['name']=$member->info['name'] ;
    if (!$data['email'] and $member->id) $data['email']=$member->info['email'] ;
    if (!$data['phone'] and $member->id) $data['phone']=$member->info['phone'] ;

    // выделяем в отдельный массив поля, которые будут сериализированы
    if (isset($_POST['org_name'])) $data['autor_info']['org_name']=$_POST['org_name'] ;
    if (isset($_POST['status'])) $data['autor_info']['status']=$_POST['status'] ;
    if (isset($_POST['inn'])) $data['autor_info']['inn']=$_POST['inn'] ;
    if (isset($_POST['kpp'])) $data['autor_info']['kpp']=$_POST['kpp'] ;
    if (isset($_POST['adres'])) $data['autor_info']['adres']=$_POST['adres'] ;
    // если передан код объекта, получаем данные по нему и сохраняем в запросе
    if ($_POST['reffer'])
    { $rec=get_obj_info($_POST['reffer']) ;
      if ($rec['pkey'])
      { $goods_system->prepare_public_info($rec) ; // если мы точно знаем, что запрос будет на товар. Инача проверять код объекта и обрабатывать соотвествующей подсистемой
        $data['_reffer']=$_POST['reffer'] ;
        $data['_reffer_name']=$rec['obj_name'] ;
        $data['_reffer_href']=$rec['__href'] ;
        $data['_reffer_parent']=$rec['parent'] ;
      }
    }
    // данные по объекту могут быть уже переданы в разобранном виде
    if ($_POST['_reffer']) $data['_reffer']=$_POST['_reffer'] ;
    if ($_POST['_reffer_name']) $data['_reffer_name']=$_POST['_reffer_name'] ;
    if ($_POST['_reffer_href']) $data['_reffer_href']=$_POST['_reffer_href'] ;
    if ($_POST['_reffer_parent']) $data['_reffer_parent']=$_POST['_reffer'] ;

    // подгружаемый файлы
    if (sizeof($_FILES['upload_files'])) $data['upload_files']=conver_FILES_to_recs($_FILES['upload_files']) ;
    return($data) ;
 }

 function prepare_rec_tiket_message()
 {  global $member ;
    $data=array() ;
    // вносим в запись запроса стандартные поля
    $data['name']=$_POST['name'] ;
    $data['email']=$_POST['email'] ;
    $data['phone']=$_POST['phone'] ;
    $data['value']=$_POST['message'] ;
    // если поля по автору не заполнены и клиент авторизован - берем данные из аккаунта
    if (!$data['name'] and $member->id) $data['name']=$member->info['name'] ;
    if (!$data['email'] and $member->id) $data['email']=$member->info['email'] ;
    if (!$data['phone'] and $member->id) $data['phone']=$member->info['phone'] ;
    return($data) ;
 }

 /*------------------------------------------------------------------------------------------------------------------------------*/
 //
 // СОЗДАНИЕ ОБЫЧНОГО ТИКЕТА
 //
 /*------------------------------------------------------------------------------------------------------------------------------*/

 // создание тикета из формы на странице
 function support_create_ticket()
 { global $support_system ;
   // готовим запись по тикету на основе данных _POST
   $data=$this->prepare_rec_tiket() ;
   // создаем тикет
   $result=$support_system->create_tiket($data) ; //damp_array($result) ;
   // показываем результат
   show_message_by_id('support_message',$result) ;
   // формируем модальное окно
   $this->add_element('title','Обращение в службу поддержки') ;
   $this->add_element('success','ajax_to_modal_window') ;
   if ($result['type']!='error') $this->add_element('after_close_update_page',1) ;
 }

 // создание тикета из модального окна
 function support_create_ticket_modal()
 { global $support_system ;
   // готовим запись по тикету на основе данных _POST
   $data=$this->prepare_rec_tiket() ;
   // создаем тикет
   $result=$support_system->create_tiket($data) ; //damp_array($result) ;
   // показываем результат
   show_message_by_id('support_message',$result) ;
   // закрываем ранее открытое модальное окно
   if ($result['type']!='error') $this->add_element('close_mBox_window','panel_support_new_tiket_modal') ;
   // формируем модальное окно
   $this->add_element('title','Обращение в службу поддержки') ;
   $this->add_element('success','ajax_to_modal_window') ;
 }

 // тикет в службу поддержки для авторизированного пользователя
 function support_create_ticket_to_member()
 { global $member,$support_system ;
   // готовим запись по тикету на основе данных _POST
   $data=$this->prepare_rec_tiket() ;
   $data['name']=$member->contact_name ;
   $data['email']=$member->info['phone'] ;
   $data['phone']=$member->info['email'] ;
   // создаем тикет
   $result=$support_system->create_tiket($data) ; //damp_array($result) ;
   // выводим сообщение
   show_message_by_id('support_message',$result) ;
   // формируем модальное окно
   $this->add_element('title','Обращение в службу поддержки') ;
   $this->add_element('success','ajax_to_modal_window') ;
   if ($result['type']!='error') $this->add_element('after_close_update_page',1) ;
 }

 // добавление нового сообщения в тикет
 function support_ticket_add_message()
 { global $support_system ;
   // готовим запись по сообщению тикета на основе данных _POST
   $data=$this->prepare_rec_tiket_message() ;
   // добавляем сообщение
   $result=$support_system->add_message($_POST['uid'],$data) ;
   show_message_by_id('support_message',$result) ;
   // формируем модальное окно
   $this->add_element('title','Служба поддержки') ;
   $this->add_element('success','ajax_to_modal_window') ;
   if ($result['type']!='error') $this->add_element('after_close_update_page',1) ;
 }

 // возвращает панель создания тикета, которая будет показана в модальном окне
 function get_support_create_tiket_panel()
 { include_once('panel_support_new_tiket_modal_ext.php') ;
   panel_support_new_tiket_modal_ext(array('class'=>'small','reffer'=>$_POST['reffer'])) ;  // форма запроса в службу поддержки

   $this->add_element('modal_panel_width','350px') ;
   $this->add_element('success','ajax_to_modal_window') ;
   $this->add_element('no_close_button',1) ;
   $this->add_element('title','Cлужба поддержки') ;
 }

 /*------------------------------------------------------------------------------------------------------------------------------*/
 //
 // ЗАПРОС ЦЕНЫ ЧЕРЕЗ СИСТЕМУ ТИКЕТОВ
 //
 /*------------------------------------------------------------------------------------------------------------------------------*/

 // панель тикета для запроса цены на товар
 function get_support_create_tiket_price_panel()
 {
   include_once('panel_support_new_tiket_modal_price.php') ;
   panel_support_new_tiket_modal_price(array('class'=>'small','reffer'=>$_POST['reffer'])) ;  // форма запроса в службу поддержки

   $this->add_element('success','ajax_to_modal_window') ;
   $this->add_element('no_close_button',1) ;
   $this->add_element('title','Запрос цены на товар') ;
 }


 function support_create_ticket_to_price()
    { global $support_system ;
      // готовим запись по тикету на основе данных _POST
      $data=$this->prepare_rec_tiket() ;
      $data['theme']='Запрос цены' ;
      // создаем тикет
      $result=$support_system->create_tiket($data) ; //damp_array($result) ;
      if ($result['code']=='create_tiket_success') $result['code']='create_tiket_to_price_success' ; //
      // закрываем ранее открытое модальное окно
      if ($result['type']!='error') $this->add_element('close_mBox_window','panel_support_new_tiket_modal_price') ;
      // выводим сообщение
      show_message_by_id('support_message',$result) ;
      // формируем модальное окно
      $this->add_element('title','Запрос цены') ;
      $this->add_element('success','ajax_to_modal_window') ;
    }

 /*------------------------------------------------------------------------------------------------------------------------------*/
 //
 // ЗАКАЗ УСЛУГИ ЧЕРЕЗ ТИКЕТ
 //
 /*------------------------------------------------------------------------------------------------------------------------------*/

 function support_create_order()
    { global $support_system ;
      // готовим запись по тикету на основе данных _POST
      $data=$this->prepare_rec_tiket() ;
      // создаем тикет
      $result=$support_system->create_tiket($data) ; //damp_array($result) ;
      if ($result['code']=='create_tiket_success') $result['code']='create_tiket_success2' ; //
      // выводим сообщение
      show_message_by_id('support_message',$result) ;
      // формируем модальное окно
      $this->add_element('title','Заказ проекта') ;
      $this->add_element('success','ajax_to_modal_window') ;
      if ($result['type']!='error') $this->add_element('after_close_go_url','/') ;
    }

    function support_cabs_create_order()
    { global $support_system ;
      // готовим запись по тикету на основе данных _POST
      $data=$this->prepare_rec_tiket() ;
      // создаем тикет
      $result=$support_system->create_tiket($data) ; //damp_array($result) ;
      // для данного типа тикеты выводим свое сообщение
      if ($result['code']=='create_tiket_success') $result['code']='create_tiket_success2' ; //
      // выводим сообщение
      show_message_by_id('support_message',$result) ;
      // формируем модальное окно
      $this->add_element('title','Заказ материала') ;
      $this->add_element('success','ajax_to_modal_window') ;
      if ($result['type']!='error') $this->add_element('after_close_go_url','/cabs/') ;
    }

    /*------------------------------------------------------------------------------------------------------------------------------*/
    //
    // ЗАКАЗ СЧЕТА и КОММЕРЧКСКОГО ПРЕДЛОЖЕНИЯ ЧЕРЕЗ ТИКЕТ
    //
    /*------------------------------------------------------------------------------------------------------------------------------*/


    function support_create_ticket_to_chet()
       { global $support_system ;
         // готовим запись по тикету на основе данных _POST
         $data=$this->prepare_rec_tiket() ;
         $data['theme']='Запрос на выставление счета' ;
         //damp_array($data) ;
         // создаем тикет
         $result=$support_system->create_tiket($data) ; //damp_array($result) ;
         if ($result['code']=='create_tiket_success') $result['code']='create_tiket_to_chet_success' ; //
         // выводим сообщение
         show_message_by_id('support_message',$result) ;
         // формируем модальное окно
         $this->add_element('title','Запрос счета') ;
         $this->add_element('success','ajax_to_modal_window') ;
         if ($result['type']!='error') $this->add_element('after_close_update_page',1) ;
       }

    function support_create_ticket_to_commerce()
       { global $support_system ;
         // готовим запись по тикету на основе данных _POST
         $data=$this->prepare_rec_tiket() ;
         $data['theme']='Запрос коммерческого предложения' ;
         // создаем тикет
         $result=$support_system->create_tiket($data) ; //damp_array($result) ;
         if ($result['code']=='create_tiket_success') $result['code']='create_tiket_to_commerce_success' ; //
         // выводим сообщение
         show_message_by_id('support_message',$result) ;
         // формируем модальное окно
         $this->add_element('title','Запрос коммерческого предложения') ;
         $this->add_element('success','ajax_to_modal_window') ;
         if ($result['type']!='error') $this->add_element('after_close_update_page',1) ;
       }

}
