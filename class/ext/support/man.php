<?php
$options=array() ;
include_once($_SERVER['DOCUMENT_ROOT']."/ini/patch.php")  ;
include_once(_DIR_TO_ENGINE."/i_system.php") ;
include_once(_DIR_TO_ENGINE."/c_HELP.php") ;
class c_page_man extends c_HELP
{
  public $h1='Служба поддержки' ;
  public $title='Служба поддержки' ;
  public $path=array('/'=>'Главная') ;

  function set_head_tags()
  { parent::set_head_tags() ;
    $this->HEAD['css'][]='/class/ext/support/style.css' ;
  }

  function block_main()
  {	$this->page_title() ;
    //$_SESSION['form_values']=array() ;
    ?><h3>Требования к панели создания тикета</h3>
        <ul><li>Название скрипта шаблона, функции шаблона и id панели должны быть одинаковыми</li>
            <li>Единственным входным параметром функции шаблона должен быть $options=array()</li>
            <li>Форма шаблона строиться на основе table class="contact_form"</li>
            <li>Форма указывается без атрибутов, все атрибуты подставляется автоматически</li>
            <li>Названия полей передаются в массиве SUPPORT. Стандартные названия полей формы:
                <table class=params>
                   <tr><td>Имя</td><td>SUPPORT[name]</td></tr>
                   <tr><td>Телефон</td><td>SUPPORT[phone]</td></tr>
                   <tr><td>Email</td><td>SUPPORT[email]</td></tr>
                   <tr><td>Вопрос</td><td>SUPPORT[value]</td></tr>
                </table>
                Для остальных полей можно указывать собственные имена - не забыть только, что в принимающей функции в AJAX необходимо поместить дополнительные поля в массив для serialize
            </li>
            <li>На обязательных полях необходимо указывать <strong>data-required</strong></li>
            <li>Для автозаполнения поля использовать <strong>value="&lt;? echo htmlspecialchars($_SESSION['form_values']['phone']);?&gt;"</strong><br>
                <strong>ВНИМАНИЕ!</strong> Использование <strong>htmlspecialchars</strong> обязательно!
            </li>
            <li>Проверочный код:<br>
                <div><img src="/images/img_check_code.php" border="0" class=checkcode><input  data-required name="_check_code" type="text"></div>
                <textarea class=source_code></textarea>
            </li>
            <li>Кнопка для отправка формы:<br>
                <div><input type="submit" class=v2 value="Отправить" cmd=support_create_ticket ext=support validate=form></div>
                <textarea class=source_code></textarea>
            </li>
        </ul>

      <div class=template_title>Кнопка для формы запроса в модальном окне</div>
      <div><button class=v2 ext=support cmd=get_support_create_tiket_panel>Задать вопрос</button></div>
      <textarea class=source_code></textarea><?


    ?><div class=template_title>panel_support_new_tiket</div><?
    include_once('panel_support_new_tiket.php') ;
    panel_support_new_tiket() ;  // форма запроса в службу поддержки

    ?><div class=template_title>panel_support_new_tiket_2</div><?
    include_once('panel_support_new_tiket_2.php') ;
    panel_support_new_tiket_2() ;  // форма запроса в службу поддержки

    ?><div class=template_title>panel_support_new_tiket_ext</div><?
    include_once('panel_support_new_tiket_ext.php') ;
    panel_support_new_tiket_ext() ;  // форма запроса в службу поддержки

  }


}

$options['class_file']='this' ;
show_page($options) ;
?>