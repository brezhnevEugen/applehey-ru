<?
function panel_support_new_tiket_2($options=array())
{ ?><div id="panel_support_new_tiket_2">
        <form><? obj_info_to_input($options) ; ?>
            <table class="contact_form <?echo $options['class']?>"><colgroup><col id='c1'><col id='c2'></colgroup>
                 <tr>
                    <td class=title>Ваше имя:&nbsp;</td>
                    <td><input  name="SUPPORT[name]" type="text" data-required maxlength="50" value="<?echo_form_saved('SUPPORT','name')?>" /><br>
                        <span class=comment>Указанное Вами имя не будет публиковаться на сайте - оно необходимо исключительно для корректного обращения к Вам в письмах.</span>
                    </td>
                 </tr>
                 <tr>
                    <td class=title>Телефон:&nbsp;</td>
                    <td><input  name="SUPPORT[phone]" type="text" maxlength="50" value="<?echo_form_saved('SUPPORT','phone')?>" /><br>
                        <span class=comment>Телефонный номер в формате +7 ххх ххх-хх-хх</span>
                    </td>
                 </tr>
                 <tr>
                   <td class=title>Ваш E-mail:&nbsp;</td>
                   <td><input name="SUPPORT[email]" type="text" data-required data-validate="email" value="<?echo_form_saved('SUPPORT','email')?>" /><br>
                       <span class=comment>Пожалуйста, указывайте действующий e-mail - без этого Вы не сможете получить уведомление о ответе на Ваш вопрос.</span>
                   </td>
                 </tr>
                 <tr><td class=title>Ваш вопрос:</td>
                     <td><textarea data-required  name="SUPPORT[value]"><?echo htmlspecialchars($_POST['value'])?></textarea></td></tr>
                  <tr>
                    <td class=title><img src="/images/img_check_code.php" border="0" class=checkcode></td>
                    <td>Пожалуйста, укажите здесь числовой код:<br><input data-required name="_check_code" type="text" ></td>
                  </tr>
        </table>
            <div class="center"><br><input type="submit" value="Задать вопрос" class=v2 validate=form cmd=support_create_ticket ext=support></div>
        </form>
    </div>
 <?
}
?>