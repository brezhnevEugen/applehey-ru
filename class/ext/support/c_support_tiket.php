<?php
// страница существующего тикета. Два режима работы - либо вопрос, либо ответ.
// в режиме ответа возможна обязательная авторизация автора
// переход к тикету для вопроса производится по параметру id
// переход к тикету для ответа производится по параметру mid
include('messages.php') ;
include(_DIR_TO_CLASS."/c_page.php");
class c_page_support_tiket extends c_page
{
  public $allow_GET_params=array('id','mid') ; // все страницы сайта работают без параметров
  public $checked_GET_params=0 ;     // разрешить проверку параметров URL
  //public $body_class='support_tiket' ;

 function set_head_tags()
 { parent::set_head_tags() ;
   $this->HEAD['css'][]=_PATH_TO_EXT.'/support/style.css' ;
 }

 function block_main()
 { global $support_system,$obj_info  ;
   $result=array() ;

   $this->page_title() ;
   if (!$obj_info['pkey'])         $support_system->show_message(prepare_result('tiket_not_found')) ;
   elseif ($obj_info['status']==2) $support_system->show_message(prepare_result('tiket_is_closed')) ;
   else
   {  // добавляем сообщение, если было отправлено ранее
      if ($_POST['cmd']=='support_ticket_add_message') $result=$this->exec_cmd_ticket_add_message() ;
      // показываем тикет
      if ($result['type']!='success') $this->panel_tiket() ;
   }
 }

 // добавление нового соообщения в тикет
 // $obj_info['open_uid'] - определяет это вопрос или ответ
 // если клиент авторизован, в данные автора сообщения должны быть вписаны его данные
 function exec_cmd_ticket_add_message()
 {  global $member,$support_system,$obj_info ;
    $mess=array() ;
    if ($member->id)
    { $mess['name']=$member->info['name'] ;
      $mess['phone']=$member->info['phone'] ;
      $mess['email']=$member->info['email'] ;
    }
    elseif (!$obj_info['open_mode']) // сообщение - это новый вопрос, списываем в автора данные автора тикета
    { $mess['name']=$obj_info['autor_name'] ;
      $mess['phone']=$obj_info['autor_phone'] ;
      $mess['email']=$obj_info['autor_email'] ;
    }
    else // если сообщение - ответ, отвечающий может сам вписать свои данные
    { $mess['name']=$_POST['name'] ;
      $mess['email']=$_POST['email'] ;
      $mess['phone']=$_POST['phone'] ;
    }
    $mess['value']=$_POST['message'] ;
    $result=$support_system->add_message($obj_info['open_uid'],$mess,$_FILES['tiket_files']) ;  // передавать надо именно  open_uid, так как он определяет это вопрос или ответ
    $support_system->show_message($result) ;
    return($result) ;
 }

 // показываем тикета
 function panel_tiket()
 { global $support_system,$obj_info ;
   ?><div id=panel_tiket><?
      ?><div id=panel_tiket_info><?
          $this->panel_theme() ; // информация по объекту тикета
          $this->panel_reffer_obj_info() ; // информация по объекту тикета
          $this->panel_tiket_info() ; // информация по автору тикета для персонала сайта
      ?></div><?
      $support_system->show_list_items('parent='.$obj_info['pkey'].' and clss=15','list_tiket_messages',array('debug'=>0)) ;
   ?></div><?

   // показываем стандартную форму для добавления сообщения c файлами или без них
   $this->panel_support_add_message() ;
   //$this->panel_support_add_message_files() ;
 }

 function panel_theme()
 { global $obj_info ;
   ?><p class=thema>Тема: <strong><?echo $obj_info['__theme']?></strong></p><?
 }

 function panel_reffer_obj_info()
 { global $obj_info ;
   //if ($obj_info['obj_reffer_info']['_image_name']) echo '<img class=obj_reffer_img src="'.img_clone($obj_info['obj_reffer_info'],'small').'" alt="" border="0">' ;
   if ($obj_info['obj_reffer']) echo '<p>Товар: <a href="'.$obj_info['obj_reffer_href'].'" target=_blank>'.$obj_info['obj_reffer_name'].'</a></p>' ;
 }

 function panel_tiket_info()
 { global $obj_info ;
   //if ($obj_info['open_mode']==1)
       echo $obj_info['__autor_info'] ; // table подготавливается в prepare_public_info
 }

 // форма нового вопроса в тикет. Добавление вопроса через AJAX в модальном окне
 function panel_support_add_message()
 { include_once('panel_support_add_message.php') ;
   panel_support_add_message() ;
 }

 // форма нового вопроса в тикет с возможностью добавления файлов. Добавление вопроса через POST на новую страницу
 function panel_support_add_message_files()
 { include_once('panel_support_add_message_files.php') ;
   panel_support_add_message_files() ;
 }

}
?>
