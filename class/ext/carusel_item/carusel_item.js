$j.fn.carusel_item = function(options)
{   // добавляем элементы карусели
   $j(this).addClass('carusel_item') ;
   $j(this).append('<div class=to_prev></div><div class=to_next></div><div class=content><div class=inner></div></div>') ;
   $j(this).find('div.inner').append($j(this).find('div.item')) ;

   var banner=this ;
   var banner_inner=this.find('div.inner') ;

   var item=$j(banner).find('div.item:last-child') ;
   var w_item=$j(item).outerWidth(true) ;
   var step_next="-="+w_item+"px" ;

   var settings = $j.extend(
   {  'interval' : 1000,
      'speed' : 1000
   }, options);

   function start() { $j(banner).everyTime(settings['interval'],'ban_timer',next); }

   function stop() { $j(banner).stopTime('ban_timer'); }

   function next()
   { stop() ;
     var first_item=$j(banner).find('div.item:first-child') ;
     $j(banner_inner).animate({left:step_next},settings.speed,'swing',function()
         {$j(banner_inner).append(first_item);
          $j(banner_inner).css({left:0}) ;
         }) ;
   }


   function prev()
   { stop() ;
     var last_item=$j(banner).find('div.item:last-child') ;
     $j(banner_inner).prepend(last_item);
     $j(banner_inner).css({left:-w_item+'px'}) ;
     $j(banner_inner).animate({left:"0px"},settings.speed,'swing',function(){}) ;
   }

   function to_next_on_click() { stop() ; next() ;}
   function to_prev_on_click() { stop() ; prev() ;}

   $j(banner).find('div.to_next').live('click',to_next_on_click) ;
   $j(banner).find('div.to_prev').live('click',to_prev_on_click) ;
   start() ;

} ;
