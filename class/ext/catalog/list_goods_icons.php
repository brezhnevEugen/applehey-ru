<?


  function list_goods_icons(&$arr_rec,$options=array())
  { global $goods_system;
    if (!sizeof($arr_rec)) return ;
    if (!$options['view_mode'] and ( !$_SESSION['list_goods_icons_mode'] or $_SESSION['list_goods_icons_mode']==0 )) $view_mode='icon';
    if (!$options['view_mode'] and $_SESSION['list_goods_icons_mode']==1) $view_mode='property';
    if (!$options['view_mode'] and $_SESSION['list_goods_icons_mode']==2) $view_mode='list';
    $id=($options['id'])? 'id='.$options['id']:'' ;
    $clone=($options['clone'])? $options['clone']:'175' ;
    if (!$options['item_to_line']) $options['item_to_line']=3 ;
    ?><div class="list_goods_icons <?echo $view_mode?>" <?echo $id?>><?
	if (sizeof($arr_rec)) foreach ($arr_rec as $rec)
     { // обрабатываем имя через регулярку, чтобы заключить все, что в ковычках в отдельный span
       //$name=preg_replace('/"(.*?)"/m','<span>"$1"</span>',$rec['__name']);
       $parent=$goods_system->tree[$rec['parent']];
       $rec['price_min_in_sortament']=execSQL_value('select MIN(price) from '._TM_SORTAMENT.' where parent='.$rec['pkey'].' and enabled=1 ',array('no_indx'=>1)) ; //выбираем минимальное значение стоимости сортамента
       ?><div class="item"  reffer="<?echo $rec['_reffer']?>">
       <?$sales_value=round($rec['sales_value']);
       if ($rec['sales_type']==4) $sales_value=round(($rec['sales_value'])*100/$rec['price']);
       if ($rec['sales_type']){?><div class="sales"><span>- <?echo $sales_value?> %</span></div><?}
       if ($rec['top']){?><div class="top">hit</div><?}
       if ($rec['new']){?><div class="new">new</div><?}?>
         <div class="img">
           <a class='highslide' id="thumb1" href='<? echo img_clone($rec,'500')?>' onclick="return hs.expand(this)" title="<? echo $rec['__name'] ?>"><img src='<? echo img_clone($rec,$clone)?>' alt=''/></a><br />
<!--            <img src="--><?//echo img_clone($rec,$clone)?><!--" alt="--><?//echo $rec['__img_alt']?><!--" title="--><?//echo $rec['__img_alt']?><!--"  width=175>-->
         </div>

         <div class="name_wrap">
           <div class=name><?if (!$rec['icon_obj_name']) echo $rec['__name']; else echo $rec['icon_obj_name']; echo $rec['_fast_edit_icon'] ?></div>
           <div class=icon_comment><? echo $rec['icon_obj_comment'] ?></div>
         </div>
       <div class="specification">
         <? if ($rec['spec_material']) echo '<span class="title">Материал:</span><span class="spec_item material">'.$rec['spec_material'].'</span>';?>
         <? if ($rec['spec_size']) echo '<span class="title">Размер:</span><span class="spec_item size">'.$rec['spec_size'].'</span>';?>
         <? if ($rec['spec_color']) echo '<span class="title">Цвет:</span><span class="spec_item color">'.$rec['spec_color'].'</span>';?>
        <? if ($rec['spec_cnt']) echo '<span class="title">Мин. заказ:</span><span class="spec_item min">'.$rec['spec_cnt'].'</span>';?>
        <? if ($rec['spec_min']) echo '<span class="title">В упаковке:</span><span class="spec_item cnt">'.$rec['spec_min'].'</span>';?>
       </div>
      <div class="price"><?
       if ($rec['sortament_type']==2)
        {  echo '<span class="ot">от</span>'.$rec['price_min_in_sortament'].' р.';}
        else {echo $rec['__price_5'];}?>
      </div>
       <?if ($rec['manual']){?><div class="intro"><?echo $rec['manual']?></div><?}?>

<!--       <button class="inform v1 small" href="--><?//echo $rec['__href']?><!--"></button>-->
<!--       <button class="favorite v1 small" href="--><?//echo $rec['__href']?><!--"></button>-->

       <?
          if (!$_SESSION['list_goods_favorite']) $_SESSION['list_goods_favorite']=array();
          if (array_key_exists($rec['pkey'], $_SESSION['list_goods_favorite']))
          {
            $del_item='display: block;';
            $add_item='display: none;';
          }
          else {
            $del_item='display: none;';
            $add_item='display: block;';
          }
       ?>
       <div class="favorite">
         <div class="v2 to_favorite fav" cmd=pages_favorite/add_to_favorite style="<?echo $add_item?>"></div>
         <div class="v2 del_favorite fav" cmd=pages_favorite/del_from_favorite style="<?echo $del_item?>"></div>
       </div>
       <script>
         $j('.fav').click(function(){
                   if ($j(this).attr('class') == 'v2 to_favorite fav ok')
                   {  $j(this).css('display','none');
                      $j(this).parent().find('.del_favorite').css('display','block');
                   } else {
                       $j(this).css('display','none');
                       $j(this).parent().find('.to_favorite').css('display','block');
                   }
         });
       </script>

       <div class="buttons"><?
         if (!$_SESSION['cart']->goods[$rec['_reffer']])
           { ?><button class="to_cart v1 small" href="<?echo $rec['__href']?>">Купить</button><? }
           else {?><button class="in_cart v1 small" href="/orders/">В корзине</button><?}
         ?>
       </div>
       </div><?
     }
    else {?><div class=alert>Товаров не найдено</div><?}
    if (!$options['no_clear_div'])
    {?><div class=clear></div><?}?>
    </div>

    <?

  }

?>