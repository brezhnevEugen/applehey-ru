<?
  function list_goods_icons2(&$arr_rec,$options=array())
  { if (!sizeof($arr_rec)) return ;
    $id=($options['id'])? 'id='.$options['id']:'' ;
    $clone=($options['view_mode'])? '250w':'250' ;
    if (!$options['item_to_line']) $options['item_to_line']=3 ;
    ?><div class="list_goods_icons <?if ($options['view_mode']) echo 'mode_'.$options['view_mode']?>" <?echo $id?>><?
	if (sizeof($arr_rec)) foreach ($arr_rec as $rec)
     {  // обрабатываем имя через регулярку, чтобы заключить все, что в ковычках в отдельный span
        //$name=preg_replace('/"(.*?)"/m','<span>"$1"</span>',$rec['__name']);
        ?><div class="item"  reffer="<?echo $rec['_reffer']?>">
             <div class=name><table><tr><td><a href="<?echo $rec['__href']?>"><?echo $rec['__name']?></a><? echo $rec['_fast_edit_icon'] ?></td></tr></table></div>
             <div class="img"><a href="<?echo $rec['__href']?>" class="img"><img src="<?echo img_clone($rec,$clone)?>" alt="<?echo $rec['__img_alt']?>" width=250></a></div>
             <?if ($rec['__price_4']){?><div class="price"><?echo $rec['__price_5']?></div><?}?>
             <div class="buttons"><?
               if ($rec['__price_4'])
                    { if (!$_SESSION['cart']->goods[$rec['_reffer']]){?><button class="to_cart v2 small" cmd=orders/add_to_cart>В корзину</button><?}
                      else                                           {?><button class="in_cart v1 small" href="/orders/">уже в корзине</button><?}
                    }
             ?>
             </div>
         </div>
        <?
     }
    else {?><div class=alert>Товаров не найдено</div><?}
    if (!$options['no_clear_div']) {?><div class=clear></div><?}
    ?></div><?
    include_css_style('list_goods_icons2_css1')  ;
  }
?>
    <style type="text/css">
      div.list_goods_icons{margin:0 auto;}
      div.list_goods_icons div.item{width:250px;height:300px;float:left;position:relative;text-align:center;margin:8px 0 12px 20px;background:#dce1e3;}
      div.list_goods_icons div.item:nth-child(3n+1){margin-left:0;}
      div.list_goods_icons div.item img{}
      div.list_goods_icons div.item div.price{color:#474646;position:absolute;width:230px;font-size:32px;left:0;bottom:37px;padding-left:20px;}
      div.list_goods_icons div.item div.price span{font-size:12px;}
      div.list_goods_icons div.item div.price span.price_alt{font-size:20px;text-decoration:line-through;}
      div.list_goods_icons div.item div.price span.price_new{font-size:32px;color:red;}
      div.list_goods_icons div.item div.art{color:#999999;}
      div.list_goods_icons div.item div.name{font-size:14px;color:white;background:#3C94E3;padding:4px 10px;}
      div.list_goods_icons div.item div.name table{table-layout:fixed;border-collapse:collapse;width:230px;}
      div.list_goods_icons div.item div.name table td{height:40px;vertical-align:middle;text-align:center;line-height:1.2em;font-size:16px;}
      div.list_goods_icons div.item div.name a{text-decoration:none;color:white;}
      div.list_goods_icons div.item div.name a:hover{text-decoration:none;color: #fffacd;}
      div.list_goods_icons div.item div.buttons{position:absolute;bottom:10px;left:0;text-align:center;width:250px;}
    </style>
