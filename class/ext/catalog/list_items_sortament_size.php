<?
function list_items_sortament_size(&$arr_rec,$options = array())
{    global  $TM_goods;
  $sortament_parametr_name=$options['sortament_parametr_name'];
  if (sizeof($arr_rec))
  { ?>
    <table class = "list_items_sortament size">
      <colgroup><col id = "c1"><col id = "c2"><col id = "c3"></colgroup>
    <tr><th><? if ($sortament_parametr_name=='') echo 'Размер:'; else echo $sortament_parametr_name;?></th><th class="price">Цена:</th><th class="count">Количество:</th></tr><?
      foreach($arr_rec as $rec )
      { $cart_indx=$rec['_parent_reffer'].'.'.$rec['_reffer'];
        $cart_cnt=($_SESSION['cart']->goods[$cart_indx]['__cart_cnt'])? $_SESSION['cart']->goods[$cart_indx]['__cart_cnt']:'' ;
        ?><tr reffer="<?echo $rec['_parent_reffer']?>" class = "sort item_<?echo $rec['pkey']?>">
          <td class = name>
            <span class="_name "><? echo $rec['obj_name']; ?></span>
            <span class="_in_cart " >добавлено</span>
            <span class="_out_cart " >удалено</span>
          </td>
          <td class = price><? echo $rec['price'] ?></td>
          <td class = cnt>
            <span class="inc v2" cmd=orders/cnt_change mode="inc" sortament_reffer="<? echo $rec['_reffer'] ?>"></span>
            <input type=text class="text cnt v2" cmd=orders/cnt_set event=keyup value='<?echo $cart_cnt?>' disabled name="cart_cnt_value" sortament_reffer="<?echo $rec['_reffer']?>" id="input_cnt_<?echo $rec['parent'].'_'.$rec['pkey']?>">
            <span class="dec v2" cmd=orders/cnt_change mode="dec" sortament_reffer="<? echo $rec['_reffer'] ?>"></span>
          </td><?
        //        проверяем наличие картинки для сортамента, и при ее отсутствии подставляем картинку родителя
        if ($rec['_image_name']){$source=$rec; } else  {$source=execSQL_van('select * from '.$TM_goods.' where pkey='.$rec['parent'].''); }
        $sortament_image='<a href="'. img_clone($rec,'source').'" title="'. $rec['obj_info'].'" onclick="return hs.expand(this)"><img class="back_light" src="'. img_clone($source,450).'" alt="'.$options['alt'].'" title="'. $options['title'].'" border="0"></a>';
        ?>
        <script type="text/javascript">
          $j('.item_<?echo $rec['pkey']?>').hover(function() {$j("#main_img .item.visible").replaceWith('<div class="item main visible"><?echo $sortament_image;?></div>');});
          $j('.inc').click(function ()
                           {
                             $j(this).parent().parent().children('.name').children('._name').css({'display':'none'});
                             $j(this).parent().parent().children('.name').children('._in_cart').css({'display':'block'});
                             $j(this).parent().parent().children('.name').children('._in_cart').oneTime("0.7s",function(){
                               $j(this).css({'display':'none'});
                               $j(this).parent().children('._name').css({'display':'block'});
                             });
                           })
          $j('.dec').click(function ()
                            {
                              $j(this).parent().parent().children('.name').children('._name').css({'display':'none'});
                              $j(this).parent().parent().children('.name').children('._out_cart').css({'display':'block'});
                              $j(this).parent().parent().children('.name').children('._out_cart').oneTime("0.7s",function(){
                                $j(this).css({'display':'none'});
                                $j(this).parent().children('._name').css({'display':'block'});
                              });
                            })
        </script>
        </tr><?
      }?>
    </table><?


  }
}
?>