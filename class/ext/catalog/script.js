$j(document).ready(function()
{
    function select_sortament()
            { var reffer=$j('div#panel_goods_item').attr('reffer') ;
              var set=$j(this).parent().attr('set') ;


              if (!$j(this).hasClass('on'))
              { $j(this).jBox('Нет в наличии');
                return(false) ;
              }
              else if ($j(this).hasClass('selected'))
              {   $j(this).removeClass('selected') ;
                  send_ajax_request({cmd:'clear_sortament_reffer',reffer:reffer,set:set});
              }
              else
              {   // выделяем эелемент
                  $j(this).parent().find('div.item').removeClass('selected') ;
                  $j(this).addClass('selected') ;
                  // если есть фото - ставим фото по центру
                  if ($j(this).find('img').length)
                  { $j('img#main_img').attr('src',$j(this).find('img').attr('u1')) ;
                    $j('img#main_img').closest('a').attr('href',$j(this).find('img').attr('u2')) ;
                  }
                  // получаем текущие значения выделенных элементов
                  var cur_size=$j('div#panel_select_size div.item.selected').text() ;
                  var cur_color=$j('div#panel_select_color div.item.selected div.name').text() ;
                  // отправляем данные на сервер
                  send_ajax_request({cmd:'get_sortament_reffer',size:cur_size,color:cur_color,reffer:reffer,set:set});
              }
            }

    function select_sortament_from_table()
           { var reffer=$j('div#panel_goods_item').attr('reffer') ;
             var set=$j(this).parent().attr('set') ;
             var sort_reffer=$j(this).attr('reffer') ;


             if ($j(this).hasClass('selected'))
             {   $j(this).removeClass('selected') ;
                 send_ajax_request({cmd:'clear_sortament_reffer',reffer:reffer,sort_reffer:sort_reffer});
             }
             else
             {   // выделяем эелемент
                 $j('table.list_items_sortament tr').removeClass('selected') ;
                 $j(this).addClass('selected') ;
                 // получаем текущие значения выделенных элементов
                 var cur_size=$j('table.list_items_sortament tr.selected td.size').text() ;
                 var cur_color=$j('table.list_items_sortament tr.selected td.color').text() ;
                 // отправляем данные на сервер
                 send_ajax_request({cmd:'get_sortament_reffer',size:cur_size,color:cur_color,reffer:reffer,set:set,no_get_photo_color:1});
             }
           }


    $j('div#panel_select_color div.item').click(select_sortament);
    $j('div#panel_select_size div.item').click(select_sortament);
    $j('table.list_items_sortament tr.sort').click(select_sortament_from_table) ;

}) ;