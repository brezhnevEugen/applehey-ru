<?php
define('_BODY_CLASS','goods') ;
include(_DIR_TO_CLASS.'/c_page.php');
class c_page_catalog_goods extends c_page
{
  function set_head_tags()
  { parent::set_head_tags() ;
    $this->HEAD['js'][]=_PATH_TO_EXT.'/catalog/script.js' ;
    //расширение - карусель товаров
    $this->HEAD['js'][]=_PATH_TO_EXT.'/elevatezoom-master/jquery.elevatezoom.js' ;
    $this->HEAD['js'][]=_PATH_TO_EXT.'/elevatezoom-master/jquery.fancybox.pack.js' ;
    //расширение - tabs
    $this->HEAD['js'][]=_PATH_TO_EXT.'/tabs/script.js' ;
    $this->HEAD['css'][]=_PATH_TO_EXT.'/tabs/style.css' ;
  }

  function select_obj_info()
  { parent::select_obj_info() ;
   if ($this->result==404)
   {  global $TM_goods,$goods_system ;
      list($url_dir,$ext)=explode('.',_CUR_PAGE_NAME) ;
      $goods_rec=execSQL_van('select * from '.$TM_goods.' where url_name="'.$url_dir.'"') ;
      if ($goods_rec['pkey'])
      { $parent_rec=execSQL_van('select * from '.$TM_goods.' where pkey='.$goods_rec['parent'].'') ;
          if ($parent_rec['pkey'])
          { $goods_system->prepare_public_info($parent_rec) ;
            redirect_301_to($parent_rec['__href']) ;
          }
      }
   }

   // вписываем информацию по сортаменту в запись по товару
   if ($GLOBALS['obj_info']['clss']==200) $GLOBALS['obj_info']['_sortament']=execSQL('select pkey,obj_name,code,price,stock,size,color_name,img_id from '._TM_SORTAMENT.' where parent='.$GLOBALS['obj_info']['pkey'].' and enabled=1',array('no_indx'=>1)) ;
    $min_price=array();
   if (sizeof($GLOBALS['obj_info']['_sortament']))
   {  foreach($GLOBALS['obj_info']['_sortament'] as $rec_sortament)
       { if ($rec_sortament['color_name'] and $rec_sortament['img_id']) $GLOBALS['obj_info']['_sort_colors'][$rec_sortament['color_name']]=$rec_sortament['img_id'] ;
         if ($rec_sortament['size']) $GLOBALS['obj_info']['_sort_size'][$rec_sortament['size']]++ ;
         $min_price[]=$rec_sortament['price'];
       }
   }
    $GLOBALS['obj_info']['min_price_sortament']=min($min_price);
    //   для сортамента "по размеру" ищем минимальное значение цены

  }

  function block_main()
  { global $obj_info ;
//    $this->page_title(array('after_path'=>$obj_info['obj_name'],'no_active_first_level'=>1)) ;
    if ($obj_info['icon_obj_name']) $name=$obj_info['icon_obj_name']; else $name=$obj_info['obj_name'];
    $this->panel_path(array(),array('after_path'=>$name,'no_active_first_level'=>1)) ;
    $this->panel_goods_item($obj_info) ;
//    damp_array($obj_info['obj_clss_3']);

  }

  function panel_goods_item($rec)
  { global $goods_system;?>
    <div id=panel_goods_item reffer="<?echo $rec['_reffer']?>">
      <?$this->panel_title($rec['icon_obj_name']); ?>
    <div id="obj_comment"><?echo $rec['icon_obj_comment'];?> <span><?if ($rec['sortament_type']==3)  {echo '/ рисунок - '.$rec['obj_clss_3'][0]['manual'] ;}?></span></div>


      <div class=panel_center> <?$this->panel_center($rec)?></div>

      <div class=panel_right_goods_info> <?$this->panel_right_goods_info($rec)?></div>
      <div class=clear></div>

      <div id=panel_tabs><?$this->panel_tabs($rec);?></div>
      <script type="text/javascript">$j('div#panel_tabs').tabs() ;</script><?
    ?></div><?
    $this->panel_analog($rec) ;
 }

  function panel_right_goods_info ($rec)
  { switch  ($rec['sortament_type'])
    { case 1 : $this->panel_info_goods_color($rec) ; break;
      case 2 : $this->panel_info_goods_size($rec) ; break;
      case 3 : $this->panel_info_goods_size_ext($rec) ; break;
    }
    if (sizeof($rec['obj_clss_65'])) { ?><div id="sortament_outer"><?$this->panel_sortament($rec) ;?></div><?}?>
    <div id="last_elments"><?
      include_once(_DIR_EXT.'/orders/panel_cart_info_in_page_goods.php') ;panel_cart_info_in_page_goods() ; ?>
         <div class="buttons">
           <button class="v1  " href=/orders/ id="button_to_cart">перейти в корзину</button>
           <button class="v1 back" href="<?echo $_SESSION['goods_system']->tree[$rec['parent']]->href;?>" id="button_to_cart">назад к покупкам</button>
           <div class="clear"></div>
         </div>              
    </div>   
    <script type="text/javascript">
      $j(document).ready(function(){
          // перезапоминаем фрагмент html и класс который пойдет под замену
          main_image_visible_class=$j("#main_img .visible").attr('class');
          main_image_visible=$j("#main_img .visible").html();
        // рассчитываем высоту блока с сортаментом
        height_panel_right_goods_info=$j(".panel_right_goods_info").height();
//        height_panel_right_goods_info_hover=$j("#panel_sortament").height()+230;
        <?if ($rec['sortament_type']==1) {?> height_panel_right_goods_info_hover=$j(".panel_right_goods_info").height() + $j("#panel_sortament").height()- 307 ;<?}?>
        <?if ($rec['sortament_type']==2) {?> height_panel_right_goods_info_hover=$j(".panel_right_goods_info").height() + $j("#panel_sortament").height()- 307 ;<?}?>
        <?if ($rec['sortament_type']==3) {?> height_panel_right_goods_info_hover=$j(".panel_right_goods_info").height() + $j("#panel_sortament").height()- 268 ;<?}?>
        height_sortament_outer=$j("#sortament_outer").height()+2;
        height_panel_right_goods_info_new=height_sortament_outer + 180;
        $j('.panel_right_goods_info').hover(
          function()
          {
            $j("#sortament_outer").css({"overflow" : "visible"});
            $j("#sortament_outer").css({"height" : "auto"});
            $j("#sortament_outer").css({"max-height" : "auto"});
            $j(".panel_right_goods_info").height(height_panel_right_goods_info_hover);

          },
          function()
          { $j(this).oneTime("0s", function()
            { $j("#sortament_outer").css({"overflow" : "hidden"});
              $j("#sortament_outer").height(height_sortament_outer);
              $j(".panel_right_goods_info").height(height_panel_right_goods_info);
              $j("#main_img .item.visible a").replaceWith(main_image_visible);
              $j("#main_img .item.visible").addClass(main_image_visible_class);
            });
          }
        );
      })
    </script><?
  }

  function panel_center ($rec)
  { ?>
    <!--основная картинка    -->
    <div id=panel_main_img><?
     if (sizeof($rec['obj_clss_3'])) print_template($rec['obj_clss_3'],'highslide/list_images_highslide',array('id'=>'main_img','clone'=>'450','clone_big'=>'source','alt'=>$rec['__img_alt'],'slideshowGroup'=>$rec['pkey'])) ;
      else echo '<img src="'.img_clone($rec,450).'" alt="'.$rec['__img_alt'].'" title="'.$rec['__img_alt'].'" >';?>
    </div>
    <!--превьюшки -->
    <div id=panel_preview_images><?
     if (sizeof($rec['obj_clss_3']))
     {
       include_once(_DIR_EXT.'/highslide/list_images.php') ;
       list_images($rec['obj_clss_3'],array('id'=>'prev_img','clone'=>'73','alt'=>$rec['__img_alt'])) ;
     }?>
    </div>
    <script type="text/javascript">
      $j(document).ready(function(){
        //  действия при клике на первьюшку
        $j("#prev_img .prev_item").click( function(){
          var $pkey_prev=$j( this ).attr("pkey");
            //меняем выбор в списке для выбранного рисунка
            $j('#select_list_picture').easyDropDown('select', ''+$pkey_prev+'');
            //   смена главного изображения

            
          $j("#main_img .item").css("display","none");
          $j("#main_img .item").removeClass("visible");
          $j("#prev_img .prev_item").removeClass("active_img");
          $j(".pkey_"+$pkey_prev).css("display","block");
          $j(".pkey_"+$pkey_prev).addClass("visible");
          $j('select#select_list_picture').val($pkey_prev) ;
          $j(this).addClass("active_img");

          //только для смешаного типа сортамента (добавить в название наименование рисунка / отработать select <-> сменить картинку / сделать подмену списка сортамента с учетом корзины и рисунка)
          <?
          if ($rec['sortament_type']==3)
          {?>
            // смена списка сортамента с учетом выбора рисунка и добавленного в корзину
//            send_ajax_request({cmd:'catalog/get_panel_sortament',reffer:'<?//echo $rec['_reffer']?>//',color_id:$pkey}) ;
            // делаем замену названия рисунка в общем названии страницы
            var active_image_alt=$j("#prev_img .active_img img").attr('alt');
            $j("#obj_comment span").html( '/ рисунок - ' + active_image_alt + '');

          <?
          }?>
        });
      })
      </script>
    <div id="panel_like"><?$this->panel_like()?></div><?
  }

  // если более одного сортамента в товаре - выключаем кнопки, клиент должен сам выбрать нужный ему цвет и размер
  // если один сортамент - прописываем в кнопке sortament_reffer, для тог, чтобы код сортамента был передан в корзину вместе с кодом товара
  // при выводе элементов делаем проверку по количеству (более 9) и типу сортамента
   function panel_like()
   { ?>
     <span class="title">Поделиться:</span>
     <script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
     <script src="//yastatic.net/share2/share.js"></script>
     <div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki,twitter" data-limit="4"></div>
     <?

   }


  function panel_info_goods_color($rec)
  { ?>
    <div id=panel_info_goods_color>
      <h3 >Цена</h3>
      <?if ($rec['__price_4']){?><div id=price_goods class="price"><span class="tit_price"></span><?echo $rec['__price_5']?></div><?}?>
      <div class="spec_count"><span>Минимальный заказ: </span><?echo $rec['spec_cnt']?> шт.</div>
    </div>
    <?
  }

  function panel_info_goods_size($rec)
  { ?>
    <div id=panel_info_goods_size>
      <h3 >ПАРАМЕТРЫ</h3>

      <div id=price_goods class="price"><span class="tit_price">ЦЕНА: от </span><?echo $GLOBALS['obj_info']['min_price_sortament']?>р.</div>
      <div class="spec_count"><span>Минимальный заказ: </span><?echo $rec['spec_cnt']?> шт.</div>
    </div>
    <?
  }
  function panel_info_goods_size_ext($rec)
  { ?>
    <div id=panel_info_goods_size_ext>
      <h3 >ПАРАМЕТРЫ</h3>
      <div id=price_goods class="price"><span class="tit_price">ЦЕНА: от </span><?echo $GLOBALS['obj_info']['min_price_sortament']?>р.</div>
      <div class="spec_count"><span>Минимальный заказ: </span><?echo $rec['spec_cnt']?> шт.</div>
      <!--      добляем вывод списка рисунков для сортамента третьего типа-->
      <div class=select_picture>
        <? $arr_images=$rec['obj_clss_3'] ;
        if (sizeof($arr_images))
        { ?>
          <div class=panel_list_picture>Рисунок:
            <select name="picture" id="select_list_picture" onchange="select_change();return(false);" class="dropdown">
              <option value="" class="label" value=""></option>
              <? $iii=0;
              foreach ($arr_images as $rec_image)
              { $iii++;
                if ($iii==1) $selected='selected'; else $selected='';?>
                <option value="<?echo $rec_image['pkey']?>" <?echo $selected;?>><?echo $rec_image['manual']?></option><?}?>
            </select>
          </div>
          <script type="text/javascript">
            function select_change()
            { var color_id=$j('select#select_list_picture').val() ;
              $j('span.inc').attr('color_id',color_id) ;
              send_ajax_request({cmd:'catalog/get_panel_sortament',reffer:'<?echo $rec['_reffer']?>',color_id:color_id}) ;
              var selected=$j('#select_list_picture option:selected').val();
              // делаем замену названия рисунка в общем названии страницы  и смену активной превьюшки
              $j("#prev_img .prev_item").removeClass('active_img');
              $j("#prev_img .prev_item." + selected).addClass('active_img');
              // делаем замену названия рисунка в общем названии страницы
              var active_image_alt=$j("#prev_img .active_img img").attr('alt');
              $j("#obj_comment span").html( '/ рисунок - ' + active_image_alt + '');
              //  делаем смену главвного изображения
              $j("#main_img .item").css("display","none");
              $j("#main_img .item").removeClass("visible");
              $j(".pkey_" + selected).css("display","block");
              $j(".pkey_" + selected).addClass("visible");
//              $j(this).addClass("active");
              // перезапоминаем фрагмент html и класс который пойдет под замену
              main_image_visible_class=$j("#main_img .visible").attr('class');
              main_image_visible=$j("#main_img .visible").html();

                // делаем замену названия рисунка в общем названии страницы

            }
          </script><?
        }?>
      </div>
    </div><?
  }

  function panel_sortament(&$rec)
  { ?>
   <div id="panel_sortament"><?
     // проверяем состав сортамента по полю 'sortament_type' и соответственно делаем различный вывод
     switch  ($rec['sortament_type'])
         { case 1 : include_once(_DIR_EXT.'/catalog/list_items_sortament_color.php') ;list_items_sortament_color($rec['obj_clss_65']) ;break;
           case 2 : include_once(_DIR_EXT.'/catalog/list_items_sortament_size.php') ;list_items_sortament_size($rec['obj_clss_65'],array('sortament_parametr_name'=>$rec['sortament_parametr_name'])) ;break;
           case 3 : include_once(_DIR_EXT.'/catalog/list_items_sortament_size_ext.php') ;list_items_sortament_size_ext($rec['obj_clss_65'],$rec['obj_clss_3'][0]['pkey'],array('arr_pic'=>$rec['obj_clss_3'],'sortament_parametr_name'=>$rec['sortament_parametr_name'])) ;break;
         } ?>
   </div>


    <?
  }



  function panel_tabs($rec)
  { ?>
    <div class=tabitem><div class="btitle">Характеристики</div><?$this->panel_specifications($rec);?></div>
    <?if ($rec['manual']){?><div class=tabitem><div class="btitle">Описание</div><? echo $rec['manual'] ;?></div><?}?>
    <div class=tabitem><div class="btitle">Отзывы</div><?$this->panel_responses($rec);?></div>
    <div class=tabitem><div class="btitle">Задать вопрос</div><?$this->panel_support2($rec);?></div>
    <?
  }

  function panel_specifications($rec)
  { ?>
    <ul id="panel_specifications">
    <?if ($rec['spec_name_1']){?><li class="item"><span class="spec_name"><?echo $rec['spec_name_1'];?>:</span><span><?echo $rec['spec_1'];?></span></li> <?}?>
    <?if ($rec['spec_name_2']){?><li class="item"><span class="spec_name"><?echo $rec['spec_name_2'];?>:</span><span><?echo $rec['spec_2'];?></span></li> <?}?>
    <?if ($rec['spec_name_3']){?><li class="item"><span class="spec_name"><?echo $rec['spec_name_3'];?>:</span><span><?echo $rec['spec_3'];?></span></li> <?}?>
    <?if ($rec['spec_name_4']){?><li class="item"><span class="spec_name"><?echo $rec['spec_name_4'];?>:</span><span><?echo $rec['spec_4'];?></span></li> <?}?>
    <?if ($rec['spec_min']){?><li class="item"><span class="spec_name">В упаковке:</span><span><?echo $rec['spec_min'];?> шт.</span></li> <?}?>
    </ul><?
  }

  function panel_responses($rec)
  { global $responses_system ;
    ?><div id="panel_responses"><!--<div class=title_box_gray>Отзывы к товару</div>--><?
    $cnt=$responses_system->show_list_items('obj_reffer="'.$rec['_reffer'].'"','responses/list_responses_messages',array('order_by'=>'r_data desc','no_show_ref_obj_info'=>1)) ;
    if (!$cnt) echo '<div class="info">Ваш отзыв может быть первым</div>' ;
    ?><br><div class=center><button class="v2" cmd=responses/get_responses_create_panel reffer="<?echo $rec['_reffer']?>">Оставить свой отзыв</button></div><?
    ?></div><?
  }

  function panel_support2($rec)
  { include_once(_DIR_EXT.'/support/panel_support_new_tiket.php') ;
    panel_support_new_tiket(array('class'=>'small')) ;
  }

  function panel_analog($rec)
  { global $goods_system ;
    ?><div class="clear"></div><div id="panel_analog"><div class=h1>Похожие товары</div><?
    //damp_array($rec) ;
    $view_mode=$goods_system->tree[$rec['parent']]->rec['view_mode'] ;
    $options=array('order'=>'rand()','count'=>6,'show_items_from_child'=>1,'view_mode'=>$view_mode) ;
    $cnt=$goods_system->show_list_items('pkey!='.$rec['pkey'].' and parent='.$rec['parent'],'catalog/list_goods_icons',$options) ;
    ?></div><?
  }








}
?>