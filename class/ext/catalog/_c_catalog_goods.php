<?php
define('_BODY_CLASS','goods') ;
include(_DIR_TO_CLASS.'/c_page.php');
class c_page_catalog_goods extends c_page
{



function set_head_tags()
{ parent::set_head_tags() ;
  $this->HEAD['js'][]=_PATH_TO_EXT.'/catalog/script.js' ;
  //расширение - карусель товаров
  $this->HEAD['js'][]=_PATH_TO_EXT.'/elevatezoom-master/jquery.elevatezoom.js' ;
  $this->HEAD['js'][]=_PATH_TO_EXT.'/elevatezoom-master/jquery.fancybox.pack.js' ;
  //$this->HEAD['js'][]=_PATH_TO_EXT.'/carusel_goods/script.js' ;
  //$this->HEAD['css'][]=_PATH_TO_EXT.'/carusel_goods/style.css' ;
  //расширение - tabs
  $this->HEAD['js'][]=_PATH_TO_EXT.'/tabs/script.js' ;
  $this->HEAD['css'][]=_PATH_TO_EXT.'/tabs/style.css' ;
}

 function select_obj_info()
 { parent::select_obj_info() ;
   if ($this->result==404)
   {  global $TM_goods,$goods_system ;
      list($url_dir,$ext)=explode('.',_CUR_PAGE_NAME) ;
      $goods_rec=execSQL_van('select * from '.$TM_goods.' where url_name="'.$url_dir.'"') ;
      if ($goods_rec['pkey'])
      { $parent_rec=execSQL_van('select * from '.$TM_goods.' where pkey='.$goods_rec['parent'].'') ;
          if ($parent_rec['pkey'])
          { $goods_system->prepare_public_info($parent_rec) ;
            redirect_301_to($parent_rec['__href']) ;
          }
      }
   }
   // вписываем информацию по сортаменту в запись по товару
   if ($GLOBALS['obj_info']['clss']==200) $GLOBALS['obj_info']['_sortament']=execSQL('select pkey,obj_name,code,price,stock,size,color_name,img_id from '._TM_SORTAMENT.' where parent='.$GLOBALS['obj_info']['pkey'].' and enabled=1',array('no_indx'=>1)) ;
   if (sizeof($GLOBALS['obj_info']['_sortament']))
   {  foreach($GLOBALS['obj_info']['_sortament'] as $rec_sortament)
       { if ($rec_sortament['color_name'] and $rec_sortament['img_id']) $GLOBALS['obj_info']['_sort_colors'][$rec_sortament['color_name']]=$rec_sortament['img_id'] ;
         if ($rec_sortament['size']) $GLOBALS['obj_info']['_sort_size'][$rec_sortament['size']]++ ;
       }
   }

 }


 function block_main()
 { global $obj_info ;
//   damp_array($GLOBALS['obj_info']['_sortament']);
   $this->panel_goods_item($obj_info) ;
 }


  function panel_goods_item($rec)
  { global $goods_system;?>
    <div id=panel_goods_item reffer="<?echo $rec['_reffer']?>">
      <?$this->panel_title($title,$options); ?>
      <div id="obj_comment"><?echo $rec['icon_obj_comment']?></div>
        <div class=panel_left> <?$this->panel_left($rec)?></div>
        <div class=panel_center>
          <!--основная картинка    -->
            <div id=panel_main_img><?
              if (sizeof($rec['obj_clss_3'])) print_template($rec['obj_clss_3'],'highslide/list_images_highslide',array('id'=>'main_img','clone'=>'450','clone_big'=>'source','alt'=>$rec['__img_alt'],'slideshowGroup'=>$rec['pkey'])) ; ?>
            </div>
            <!--превьюшки -->
            <div id=panel_preview_images><?
              if (sizeof($rec['obj_clss_3'])>1) print_template($rec['obj_clss_3'],'highslide/list_images',array('id'=>'prev_img','clone'=>'73','alt'=>$rec['__img_alt'])) ;?>
            </div>

            <script type="text/javascript">
                    $j("#prev_img .item").click(
                    function()
                    { var $pkey=$j( this ).attr("pkey");
                      $j(this).parent().find(".item").removeClass("active");
                      $j(this).addClass("active");
                      $j(this).parent().parent().parent().find("#main_img .item_main").css("display","none");
                      $j(".pkey_"+$pkey).css("display","block");
                    });
                  </script>
              <div class="share42init"></div><script type="text/javascript" src="/assets/share42/share42.js"></script>
        </div>
        <div class=clear></div>
        <div class=panel><?

            $this->panel_tabs($rec) ;

        ?></div><?
        //$this->panel_linked_goods($rec) ;

    ?></div><?
    //$this->panel_rating($rec) ;
    //$this->panel_responses($rec) ;
    //$this->panel_soputka($rec) ;
    $this->panel_analog($rec) ;

     //damp_array($_SESSION['cart']) ;
 }

  function panel_left ($rec)
  { $this->panel_info_goods($rec) ;
    if (sizeof($rec['obj_clss_65'])) {?><div id="sortament_outer"><?$this->panel_sortament($rec) ;?></div><?}?>
    <div id=panel_buy_buttons><?$this->panel_buy_buttons($rec) ;?></div>
    <script type="text/javascript">
      height_panel_left=$j(".panel_left").height();
      height_sortament_outer=$j("#sortament_outer").outerHeight();
      height_panel_left_new=height_sortament_outer + 180;
      $j('.panel_left').hover(
              function()
              { $j("#sortament_outer").css({"overflow" : "visible"});
                $j("#sortament_outer").css({"height" : "auto"});
              },
              function()
              {
                $j("#sortament_outer").css({"overflow" : "hidden"});
                $j("#sortament_outer").height(height_sortament_outer);
              }
      );
    </script>

    <?






  }

 function panel_main_img($rec)
  {  if ($rec['_image_name'])
     {?><a href="<?echo img_clone($rec,'source')?>" rel="highslide"><img id="main_img" width=450 height=450  src="<?echo img_clone($rec,'450')?>" alt="<?echo $rec['__img_alt']?>" title="<?echo $rec['__img_alt']?>"></a><?}
     else {?><img id="main_img" width="400" src="<?echo img_clone($rec,'500')?>" alt="<?echo $rec['__img_alt']?>" title="<?echo $rec['__img_alt']?>"><?}
  }


 function panel_images($rec)
  { ?><div id=panel_images><?
    if (!$rec['_sort_colors'] and sizeof($rec['obj_clss_3'])>1) print_template($rec['obj_clss_3'],'highslide/list_images_highslide',array('id'=>'goods_img_small','clone_small'=>'small','clone_big'=>'source','alt'=>$rec['__img_alt'])) ;
    ?></div><?
  }

 function panel_intro($rec)
  { ?><div id=panel_intro><?
      echo $rec['intro'] ;
    ?></div><?
  }

 function panel_sortament(&$rec)
  { ?>
    <div id="panel_sortament"><?
      // проверяем состав сортамента по полю 'sortament_type' и соответственно делаем различный вывод
      if ($rec['sortament_type']==1) print_template($rec['obj_clss_65'],'catalog/list_items_sortament_color') ;
      else if ($rec['sortament_type']==2)  print_template($rec['obj_clss_65'],'catalog/list_items_sortament_size') ; ?>
    </div><?
  }

  // если более одного сортамента в товаре - выключаем кнопки, клиент должен сам выбрать нужный ему цвет и размер
  // если один сортамент - прописываем в кнопке sortament_reffer, для тог, чтобы код сортамента был передан в корзину вместе с кодом товара

//  при выводе элементов делаем проверку по количеству (более 9) и типу сортамента

  function panel_buy_buttons($rec)
  { ?>
    <button class="v1 button_big button_yellow" href=/orders/ id="button_to_cart">Перейти в корзину</button>
    <?
  }

  function panel_info_goods($rec)
  { ?>
    <div id=panel_info_goods>
      <h3 >ПАРАМЕТРЫ</h3>
      <?if ($rec['__price_4']){?><div id=price_goods class="price"><span class="tit_price">Цена: </span><?echo $rec['__price_5']?></div><?}?>
      <div class="spec_count"><span>Количество в упаковке: </span><?echo $rec['spec_cnt']?> шт.</div>
    </div>
    <?
  }

  function panel_rating($rec)
  {
    if ($rec['rating']){?><div class=hr></div><div id=panel_rating><h3>Наша оценка</h3><?echo $rec['rating']?></div><?}
  }

  function panel_tabs($rec)
    { ?><div id=panel_tabs>
           <?if ($rec['manual']){?><div class=tabitem><div class="btitle">Описание</div><? echo $rec['manual'] ;?></div><?}?>
           <div class=tabitem><div class="btitle">Характеристики</div><?$this->panel_specifications($rec);?></div>
           <div class=tabitem><div class="btitle">Отзывы</div><?$this->panel_responses($rec);?></div>
           <div class=tabitem><div class="btitle">Задать вопрос</div><?$this->panel_support2($rec);?></div>
        </div>
        <script type="text/javascript">$j('div#panel_tabs').tabs() ;</script>
      <?
    }

  function panel_specifications($rec)
  { ?>
    <div id="panel_specifications">
      <?if($rec['__brand']){?><div class="brand"><?echo $rec['__brand']?></div><?}?>
      <?if($rec['__country']){?><div>Страна: <strong><?echo $rec['__country']?></strong></div><?}?>
      <? if($rec['sostav']){?><div class="sostav">Состав: <strong><?echo $rec['sostav']?></strong></div><?}?>
      <? if($rec['size']){?><div class="size">Размер: <strong><?echo $rec['size']?></strong></div><?}?>
    </div><?
  }

  function panel_responses($rec)
  { global $responses_system ;
    ?><div id="panel_responses"><!--<div class=title_box_gray>Отзывы к товару</div>--><?
    $cnt=$responses_system->show_list_items('obj_reffer="'.$rec['_reffer'].'"','responses/list_responses_messages',array('order_by'=>'r_data desc','no_show_ref_obj_info'=>1)) ;
    if (!$cnt) echo '<div class="info">Ваш отзыв может быть первым</div>' ;
    ?><br><div class=center><button class="v2" cmd=responses/get_responses_create_panel reffer="<?echo $rec['_reffer']?>">Оставить свой отзыв</button></div><?
    ?></div><?
  }

  function panel_support2($rec)
  { include_once(_DIR_EXT.'/support/panel_support_new_tiket.php') ;
    panel_support_new_tiket(array('class'=>'small')) ;
  }


  function panel_soputka($rec)
  { global $goods_system ;
    ?><div id="panel_soputka"><div class=title_box_gray>C этим товаров покупают</div><?
    $goods_system->show_list_items($rec['parent'],'catalog/list_goods_icons',array('order'=>'rand()','count'=>6)) ;
    ?></div><?
  }

  function panel_analog($rec)
  { global $goods_system ;
    ?><div class="clear"></div><div id="panel_analog"><div class=h1>Похожие товары</div><?
    //damp_array($rec) ;
    $view_mode=$goods_system->tree[$rec['parent']]->rec['view_mode'] ;
    $options=array('order'=>'rand()','count'=>6,'show_items_from_child'=>1,'view_mode'=>$view_mode) ;
    $cnt=$goods_system->show_list_items('pkey!='.$rec['pkey'].' and parent='.$rec['parent'],'catalog/list_goods_icons',$options) ;
    ?></div><?
  }








}
?>