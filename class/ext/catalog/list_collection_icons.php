<?
  function list_collection_icons(&$arr_rec,$options=array())
  { if (!sizeof($arr_rec)) return ;
    global $goods_system,$obj_info ;
    //damp_array($obj_info) ;
    $id=($options['id'])? 'id='.$options['id']:'' ;
    if (!$options['item_to_line']) $options['item_to_line']=3 ;

    ?><div class="list_collection_icons" <?echo $id?>><?
	if (sizeof($arr_rec)) foreach ($arr_rec as $rec)
     {  // обрабатываем имя через регулярку, чтобы заключить все, что в ковычках в отдельный span
        //$name=preg_replace('/"(.*?)"/m','<span>"$1"</span>',$rec['__name']);
        $min_price=isset($options['min_prices'][$rec['pkey']])? _format_price($options['min_prices'][$rec['pkey']]):'';
        ?><div class="item"  reffer="<?echo $rec['_reffer']?>">
             <a href="<?echo $rec['__href']?>" class="img"><img src="<?echo img_clone($rec,'200')?>" alt="<?echo $rec['__img_alt']?>"  title="<?echo $rec['__img_alt']?>" width=200 height="170"></a>
             <div class=name><a href="<?echo $rec['__href']?>"><?echo $rec['__name']?></a><? echo $rec['_fast_edit_icon'] ?></div>
             <div class=brand><?echo $goods_system->tree[$rec['brand_id']]->name?></div>
             <?if ($rec['pkey']){?><div class=art>Арт: <?echo $rec['pkey']?></div><?}?></div>
             <?if ($min_price){?><div class="price">от <?echo $min_price?></div><?}?>
         </div>
        <?
     }
    else {?><div class=alert>Товаров не найдено</div><?}
    ?><div class=clear></div></div><?
    //damp_array($rec) ;
  }

?>