<?php // вывод подразделов товарного каталога
include(_DIR_TO_CLASS."/c_page.php");
class c_page_category extends c_page
{
  public $body_class='x3' ;

  // в URL страницы может передаваться значения набора фильтров
  // эти значения надо разобрать и установить соотвествующий фильтр
  function select_obj_info()
    {   //$_SESSION['filter']=array() ;
        // пытаемся определить по url текущий объект
        parent::select_obj_info() ;
        //if ($this->result==404) $this->result=parse_url_to_filter_value() ; // иначе, пытаемся определить набор фильтров по URL
        //else                    $_SESSION['filter']=array() ;
        if ($this->result!=404) $_SESSION['last_type_rec']=$GLOBALS['obj_info'] ;
        //echo 'result='.$this->result.'<br>' ;
        //damp_array($_SESSION['last_type_rec']) ;
        //damp_array($GLOBALS['obj_info']) ;
    }


 // базовый заголовк страницы - вверху путь, снизу заголовок - для шаблонов
 function _page_title($title='',$path=array(),$options=array())
  { global $obj_info ;

    if (is_array($title)) { $options=$title ; $title='' ; $path=array() ; } // на случай, если page_title вызвана с путем в первом параметре
    $this->panel_path($path,$options);  // заголовк страницы по умолчанию
    //if ($obj_info['clss']!=210)
        $this->panel_title($title,$options);  // заголовк страницы по умолчанию

  }

  function block_main()
    { global $obj_info,$category_system,$goods_system ;
      // путь страницы
      //$this->panel_path() ;

       //damp_array($this->result) ;
       //damp_array($obj_info)  ;
      $this->page_title() ;

      //$path=$category_system->tree[$obj_info['pkey']]->get_path(array('include_index'=>1,'to_level'=>1)) ;
      //print_template($path,'page_path') ;
      //damp_array($obj_info) ;
      if (!$obj_info['__level'])
      {
          $category_system->show_list_section('root','list_section_img_a_name_list_subsection') ;

      }
      else
      {
          // получение условий выборки по параметрам фильтра с учетом свойств товара возможно только при указании текущего типа товара
          //damp_array($_SESSION['filter']) ;
          list($title,$usl_filter,$url_dir,$arr_props_title,$select_options)=get_usl_by_filter($_SESSION['filter'],array('cur_type'=>$obj_info['pkey'])) ;
          //echo '$usl_filter='.$usl_filter.'<br>' ;

          //$select_options['debug']=1 ;
          $this->panel_best_goods_by_client($obj_info['pkey'],$select_options) ;
          $this->panel_best_goods_by_expert($obj_info['pkey'],$select_options) ;

          ?><div id=panel_catalog><?

          $cnt_goods=$goods_system->get_cnt_items($category_system->get_usl_to_type($obj_info['pkey']),array('debug'=>0)) ;

          if ($cnt_goods)
          { include_once(_DIR_EXT.'/panel_filter/panel_filter.php') ;
            panel_filter(array('cur_type'=>$obj_info['pkey'])) ;

            include_once(_DIR_EXT.'/panel_filter/panel_result_of_filter.php') ;
            panel_result_of_filter(array('cur_type'=>$obj_info['pkey'])) ;
          }
          else {?><div class="alert">К сожалению, товары данного типа временно отсутсвуют</div><?}

      }



      echo '<div id=SEO_text1>'.$this->SEO_info['SEO_text1'].'</div>' ;
      ?></div><?

    }


}








?>