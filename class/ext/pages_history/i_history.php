<?
 function get_size_history($name_history,$rec)
 { $n=0 ;
   if (sizeof($_SESSION[$name_history])) foreach($_SESSION[$name_history] as $indx=>$arr_rec) if ($arr_rec['pkey']==$rec['pkey']) { $n=1 ; break ; }
   return(sizeof($_SESSION[$name_history])-$n) ;
 }

 // добавляем объект obj_info в историю
 function save_obj_in_history($obj_info)
 { global $cur_page,$goods_system ;
   // если открыта обычная страница сайта, то добавляем её в историю
   if ($obj_info['tkey']==28)                           add_rec_to_history($obj_info,'last_pages') ;
   if ($obj_info['tkey']==$goods_system->tkey and $obj_info['clss']==10) add_rec_to_history($obj_info,'history_sections') ;
   if ($obj_info['tkey']==$goods_system->tkey and $obj_info['clss']==200)  add_rec_to_history($obj_info,'history_goods') ;
   //if (_CUR_ROOT_DIR_NAME=='dictionary') add_rec_to_history(array('pkey'=>$cur_page->h1,'__name'=>$cur_page->h1,'__href'=>'http://'.$cur_page->SEO_url),'last_pages') ;
   //if (_CUR_ROOT_DIR_NAME=='faq')        add_rec_to_history(array('pkey'=>$cur_page->h1,'__name'=>str_replace('Часто задаваемые вопросы','FAQ',$cur_page->h1),'__href'=>'http://'.$cur_page->SEO_url),'last_pages') ;
   //damp_array($cur_page) ;
   //damp_array($_SESSION['history_goods']) ;

 }


 function add_rec_to_history($rec,$name_history)
 { // ищем в массиве позицию, если товар был сохранен ранее, удяляем и помещаем в начало списка
   //$_SESSION[$name_history]=array() ;
   if (!is_array($_SESSION[$name_history])) $_SESSION[$name_history]=array() ;
   $indx_search=-1 ;
   if (sizeof($_SESSION[$name_history])) foreach($_SESSION[$name_history] as $indx=>$arr_rec) if ($arr_rec['pkey']==$rec['pkey']) { $indx_search=$indx ; break ; }
   if ($indx_search>-1) unset($_SESSION[$name_history][$indx_search]) ;
   // сохраняем минимальные сведения по объекту
   $save_rec=array('pkey'=>$rec['pkey'],'__name'=>$rec['obj_name'],'__href'=>$rec['href'],'_reffer'=>$rec['_reffer']) ;
   // у товара сохраняем чуть больше
   if ($rec['tkey'])        $save_rec['tkey']=$rec['tkey'] ;
   if ($rec['__name'])      $save_rec['__name']=$rec['__name'] ;
   if ($rec['small_name'])  $save_rec['small_name']=$rec['small_name'] ;
   if ($rec['__href'])      $save_rec['__href']=$rec['__href'] ;
   if ($rec['_image_name']) $save_rec['_image_name']=$rec['_image_name'] ;
   if ($rec['__price_5'])   $save_rec['__price_5']=$rec['__price_5'] ;
   $save_rec['add_data']=time() ;
   // вносим в начало списка
   $cnt=array_unshift($_SESSION[$name_history],$save_rec) ;
   // ограничиваем размер списка - удаляем последний элемент
   if ($GLOBALS['LS_history_cnt'] and $cnt>$GLOBALS['LS_history_cnt']) array_pop($_SESSION[$name_history]) ;
 }

 function del_rec_from_history($id,$name_history)
 {
   if (sizeof($_SESSION[$name_history])) foreach($_SESSION[$name_history] as $indx=>$arr_rec) if ($arr_rec['pkey']==$id) unset($_SESSION[$name_history][$indx]) ;
 }


?>