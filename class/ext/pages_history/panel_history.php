<?php
function panel_history()
  { global $goods_system ;
    //if (!sizeof($_SESSION['last_sections']) and  !sizeof($_SESSION['last_pages']) and !sizeof($_SESSION['last_goods'])) return ;
    damp_array($_SESSION['last_sections']);
    ?><div id=panel_history class="<?echo $_SESSION['history_panel_mode']?>">
      <div id=header><div id=title>История просмотров</div></div>
      <div id=rasdel></div>
      <div id=content><div class=inner>
          <? if ($_SESSION['history_goods'])
             {  // определяем класс для карусельки, который будет задавать её ширину
                $class='width_610' ;
                if (!sizeof($_SESSION['last_pages']) and !sizeof($_SESSION['last_sections'])) $class='width_1100' ;
                if (sizeof($_SESSION['last_pages']) or sizeof($_SESSION['last_sections']))    $class='width_850' ;
                if (sizeof($_SESSION['last_pages']) and sizeof($_SESSION['last_sections']))   $class='width_610' ;
                ?><div id=carousel_goods_history class="jcarousel_skin_goods_history <?echo $class?>"><div id=history_goods_fast_info class="hidden item"></div><?
                 $goods_system->print_template($_SESSION['history_goods'],'catalog/list_goods_icons') ;
                 ?></div>
                 <script type="text/javascript">$j(document).ready(function(){$j('#carousel_goods_history').jcarousel({ scroll:1});});</script>
                <?
             }

             if ($_SESSION['last_pages'])
             { ?><div id=last_pages><span class=title>Инфо.страницы</span>
                   <div id=scrollbar_last_pages1 class=scrollbar_last_pages>
                      <div class="scrollbar"><div class="track"><div class="thumb"><div class="end"></div></div></div></div>
                      <div class="viewport"><div class="history"><? print_template($_SESSION['last_pages'],'list_pages_a_name') ;?></div></div>
                   </div>
                 </div>
                 <script type="text/javascript">$j('#scrollbar_last_pages1').tinyscrollbar();</script><?
             }

             if ($_SESSION['history_sections'])
             { ?><div id=last_pages><span class=title>Категории</span>
                  <div id=scrollbar_last_pages2 class=scrollbar_last_pages>
                    <div class="scrollbar"><div class="track"><div class="thumb"><div class="end"></div></div></div></div>
                    <div class="viewport"><div class="history"><? $goods_system->print_template($_SESSION['history_sections'],'catalog/list_section_a_name',array('id'=>'list_last_pages')) ;?></div></div>
                  </div>
                 </div>
                 <script type="text/javascript">$j('#scrollbar_last_pages2').tinyscrollbar();</script><?
             }

          ?>
      </div></div></div>
    <?
  }