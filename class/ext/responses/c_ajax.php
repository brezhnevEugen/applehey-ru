<?
include('messages.php') ;
include(_DIR_TO_ENGINE.'/c_page_XML_AJAX.php') ;
class c_page_ajax extends c_page_XML_AJAX
{
 // панель для отзывов в модальном окне
 function get_responses_create_panel()
 { $this->add_element('success','modal_window') ;
   $this->add_element('title','Оставить свой отзыв') ;
   $this->add_element('no_close_button',1) ;
   $this->add_element('width', 470) ;
   include_once('panel_responses_create.php') ;
   panel_responses_create(array('reffer'=>$_POST['reffer'])) ;
 }

 // принимающая функция - вызывается в c_ajax.php
 function responses_create_message()
 { global $responses_system,$goods_system ;
   // вынести куда нить глобально
   //$this->save_form_values_to_session($_POST) ;

   $rec['autor_name']=$_POST['RESPONSES']['name'] ;
   $rec['autor_phone']=$_POST['RESPONSES']['phone'] ;
   $rec['autor_email']=$_POST['RESPONSES']['email'] ;
   $rec['enabled']=1 ; // 0 - публикация с модерацией, публикация без модерации
   $rec['value']=strip_tags($_POST['RESPONSES']['value']) ; // только удаление тегов, экранирование будет добавлено по необходимости автоматичски
   if ($_POST['reffer'])
   { $rec_obj=get_obj_info($_POST['reffer']) ;
     if ($rec_obj['pkey'])
     { $goods_system->prepare_public_info($rec_obj) ;
       $rec['obj_reffer']=$rec_obj['_reffer'] ;
       $rec['obj_reffer_name']=$rec_obj['__name'] ;
       $rec['obj_reffer_href']=$rec_obj['__href'] ;
       $rec['obj_reffer_parent']=$rec_obj['parent'] ;
     }
   }
   $result=$responses_system->create_message($rec,array('debug'=>0)) ;
   show_message_by_id('responses_message',$result) ;
   if ($result['type']=='success') $this->add_element('close_mBox_window','panel_responses_create') ; // закрываем ранее открытое модальное окно
   $this->add_element('title','Отзыв о товаре') ;
   $this->add_element('success','modal_window') ;
   if ($result['type']=='success') $this->add_element('after_close_update_page',1) ;  // если отзыв успешно добавлен - перегружаем модальное окно
 }
}
