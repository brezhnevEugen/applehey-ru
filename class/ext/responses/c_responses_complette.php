<?php
include(_DIR_TO_CLASS.'/c_page.php') ;
class c_page_responses_complette extends c_page
{
  public $title='Отзывы - новый отзыв' ;
  public $h1='Отзывы - новый отзыв' ;

  function block_main()
  { global $responses_system ;
    $result=array() ;
    // заголовок
    $this->page_title() ;
    // выполняем команду
    if ($_POST['cmd']=='responses_create_message')      $result=$responses_system->create_message($_POST) ;
    if ($_POST['cmd']=='support_ticket_add_message')    $result=$responses_system->add_message($_POST) ;
    // показ результата команды
    show_message_by_id('responses_message',$result) ;
  }

  function block_main2()
  { global $obj_info ;
    $this->page_title() ;
    // если возвращен код сообщения - выводим сообщение
    if ($obj_info['message_id']) $no_show_message_form=show_message_by_id('responses_message',$obj_info['message_id'],$obj_info) ;
    // форма для нового сообщенне, только если сообние не было ранее успешно создан
    if (!$no_show_message_form)
    { //responses_message_default() ; - приветствие не выводим
       responses_form_new_message() ;
    }
 }
}

?>
