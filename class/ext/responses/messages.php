<?

 function responses_message_create_success($rec)
 {   if (!$rec['enabled'])
     {?><p>Уважаемый(ая) <strong><?echo $rec['autor_name']?></strong>!</p>
		<p>Ваш отзыв будет опубликован на сайте сразу после проверки его Модератором.</p>
		<p>В рабочее время это происходит обычно в течении трех часов.</p>
		<p>С уважением, администраниция сайта <strong><?echo _MAIN_DOMAIN?></strong></p>
        <br>
        <p><a href="/" class=float_left>Перейти на главную страницу</a><a href="/responses/" class=float_right>Посмотреть все отзывы/вопросы</a></p>
        <script type='text/javascript'>$j(document).ready(function(){$j('body').oneTime(2000,'ya_asyns_timer',function(){yaCounter28778656.reachGoal('RESPONSES');});})</script>
     <?
     }
     else
     {?><p>Уважаемый(ая) <strong><?echo $rec['autor_name']?></strong>!</p>
        <p>Ваш отзыв принят.</p>
     	<p>Обращаем Ваше внимание на то, что все отзывы на сайте проходят пост-модерацию. В случае нарушения установленных на сайте правил комментирования модератор имеет право заблокировать публикуцию Вашего отзыва</p>
     	<p>С уважением, администраниция сайта <strong><?echo _MAIN_DOMAIN?></strong></p>
        <br>
        <p><a href="/" class=float_left>Перейти на главную страницу</a><a href="/responses/" class=float_right>Посмотреть все отзывы/вопросы</a></p>
        <script type='text/javascript'>$j(document).ready(function(){$j('body').oneTime(2000,'ya_asyns_timer',function(){yaCounter28778656.reachGoal('RESPONSES');});})</script>
       <?
     }
 }

 function responses_message_create_comment_success($rec)
 {   ?>   <p>Уважаемый(ая) <strong>Модератор</strong>!<br><br>
			  Внесенные вами данные успешно сохранены в базе.<br>
			  Повторно модерировать сообщение вы можете по этой <a href="<?echo $rec['__moderator_url']?>" >ссылке</a>.</p>
		  <p>С уважением, администраниция сайта <a href='/'><?echo _MAIN_DOMAIN?></a></p>
          <br>
          <br>
		  <a href="/" class=float_left>Перейти на главную страницу</a>
     	  <a href="/responses/" class=float_right>Посмотреть все отзывы/вопросы</a>
     <?
     return(1) ; // не показывать форму сообщения
 }

 function responses_message_no_change_success($rec)
 {   ?>   <p>Уважаемый(ая) <strong>Модератор</strong>!<br><br>
			  Изменений не обнаружено.<br>
			  Повторно модерировать сообщение вы можете по этой <a href="<?echo $rec['__moderator_url']?>" >ссылке</a>.</p>
		  <p>С уважением, администраниция сайта <a href='/'><?echo _MAIN_DOMAIN?></a></p>
          <br><br>
		  <a href="/" class=float_left>Перейти на главную страницу</a>
     	  <a href="/responses/" class=float_right>Посмотреть все отзывы/вопросы</a>
     <?
     return(1) ; // не показывать форму сообщения
 }

 function responses_message($text)
 {?><p class=alert><?echo $text?></p><br><?}

 function responses_message_error_field()
 {?><p class=alert>Вы заполнили  не все необходмые поля. Пожалуйста, попробуйте еще раз.</p><br><?}

 function responses_message_message_not_found()
 {?><p class=alert>Отзыв не найден. Проверте правильность ссылки для доступа к отзыву. Если ссылка правильна, и Вы все равно видите данное сообщение, отзыв уже удален.</p><br><?}

 function responses_message_db_error()
 {?><p class=alert>Создание отзыва временно заблокировано по техническим причинам. Попробуйте, пожалуйста, повторить попытку спустя некоторое время</p><br><?}

 function responses_message_error_check_code()
 {?><p class=alert>Вы неправильно ввели цифровой проверочный код. Пожалуйста, попробуйте еще раз.</p><br><?}

 // сообщение-приветствие при прямом переходе на страницу службы отзывов
 function responses_message_default()
	 { global $project_name ;
	   ?><p class=bold>Уважаемые посетители сайта <a href='/'><?echo $project_name?></a></p>
		 <p>Вы можете оставить здесь свое мнение о информации, размещенной на сайте, работе сайта, качестве услуг, уровне обслуживания.</p>
		 <p class='gold bold'>Запрещается:</p>
		 <ul><li>публикация сообщения на тему, не связанную с деятельностью сайта;</li>
		 	 <li>публикация рекламной информации;</li>
		     <li>употребление нецензурных выражений.</li>
		     <li>использование в тексте сообщений HTML-разметки, команд JavaScript, гиперссылок на другие ресурсы сети Интернет;</li>
		 </ul>
		 <p>Модератор оставляет за собой право частично или полностью удалять сообщения, которые (по мнению модератора) нарушают указанные правила.</p>
		 <p>В случае грубого нарушения данных правил Модератор имеет право заблокировать доступ нарушителя к сайту</p>
	   <?
	 }


?>