<?php
include(_DIR_TO_CLASS.'/c_page.php') ;
class c_page_responses_create extends c_page
{
 public $title='Отзывы - новый отзыв' ;
 public $h1='Отзывы - новый отзыв' ;

 function block_main()
 { global $responses_system ;
   $result=array() ;
   $this->page_title() ;
   if ($_POST['cmd']=='responses_create_message')      $result=$responses_system->create_message($_POST,array('debug'=>0)) ;
   // если возвращен код сообщения - выводим сообщение
   if ($result['code']) show_message_by_id('responses_message',$result) ;
   // форма для нового сообщенне, только если сообние не было ранее успешно создан
   if ($result['type']!='success')
   {  //responses_message_default() ;
      include_once(_DIR_EXT.'/responses/panel_responses_create.php') ;
       panel_responses_create() ;
   }
 }
}

?>
