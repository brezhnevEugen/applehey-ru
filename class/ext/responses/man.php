<?php
$options=array() ;
include_once($_SERVER['DOCUMENT_ROOT']."/ini/patch.php")  ;
include_once(_DIR_TO_ENGINE."/i_system.php") ;
include_once(_DIR_TO_ENGINE."/c_HELP.php") ;
class c_page_man extends c_HELP
{
  public $h1='Отзывы на сайте' ;
  public $title='Отзывы на сайте' ;
  public $path=array('/'=>'Главная') ;

  function set_head_tags()
  { //------------------------------------------------------
    // стандартный JQuery
    $this->HEAD['js'][]='http://12-24.ru/AddOns/jquery-1.8.3.min.js' ;
    $this->HEAD['js'][]='/AddOns/jquery.form.js' ;

    // mootools
    $this->HEAD['js'][]='/AddOns/mootools-core-1.3.1.js' ;
    $this->HEAD['js'][]='/AddOns/mootools-more-1.3.1.1.js' ;

    // библитека mBox
    $this->HEAD['js'][]='/AddOns/StephanWagner-mBox-0.2.6/Source/mBox.Core.js' ;
    $this->HEAD['js'][]='/AddOns/StephanWagner-mBox-0.2.6/Source/mBox.Modal.js' ;
    $this->HEAD['js'][]='/AddOns/StephanWagner-mBox-0.2.6/Source/mBox.Modal.Confirm.js' ;
    $this->HEAD['js'][]='/AddOns/StephanWagner-mBox-0.2.6/Source/mBox.Notice.js' ;
    $this->HEAD['js'][]='/AddOns/StephanWagner-mBox-0.2.6/Source/mBox.Tooltip.js' ;
    $this->HEAD['css'][]='/AddOns/StephanWagner-mBox-0.2.6/Source/assets/mBoxCore.css' ;
    $this->HEAD['css'][]='/AddOns/StephanWagner-mBox-0.2.6/Source/assets/mBoxModal.css' ;
    $this->HEAD['css'][]='/AddOns/StephanWagner-mBox-0.2.6/Source/assets/mBoxNotice.css' ;
    $this->HEAD['css'][]='/AddOns/StephanWagner-mBox-0.2.6/Source/assets/mBoxTooltip.css' ;
    $this->HEAD['css'][]='/AddOns/StephanWagner-mBox-0.2.6/Source/assets/themes/mBoxTooltip-Black.css' ;
    $this->HEAD['css'][]='/AddOns/StephanWagner-mBox-0.2.6/Source/assets/themes/mBoxTooltip-BlackGradient.css' ;

    // библитека mForm
    $this->HEAD['js'][]='/AddOns/StephanWagner-mForm-0.2.6/Source/mForm.Core.js' ;
    $this->HEAD['js'][]='/AddOns/StephanWagner-mForm-0.2.6/Source/mForm.Element.js' ;
    //$this->HEAD['js'][]='/AddOns/StephanWagner-mForm-0.2.6/Source/mForm.Element.Select.js' ;
    $this->HEAD['js'][]='/AddOns/StephanWagner-mForm-0.2.6/Source/mForm.Submit.js' ;
    $this->HEAD['css'][]='/AddOns/StephanWagner-mForm-0.2.6/Source/assets/mForm.css' ;
    //$this->HEAD['css'][]='/AddOns/StephanWagner-mForm-0.2.6/Source/assets/mFormElement-Select.css' ;

    // стандартное подключение highslide
    $this->HEAD['js'][]='/AddOns/highslide-4.1.13/highslide/highslide-with-gallery.min.js' ;
    $this->HEAD['css'][]='/AddOns/highslide-4.1.13/highslide/highslide.css' ;

    //расширение - модальные окна
    $this->HEAD['js'][]='/class/ext/mBox/modal_window.js' ;

    $this->HEAD['css'][]='http://12-24.ru/style_help.css' ;
  }

  function block_main()
  {	$this->page_title() ;
    ?><h3>Параметры элементов</h3>
     <ul><li><strong>Элементом</strong> может кнопка, div или любой другой элемент.</li>
         <li>Элемент может как находиться внутри <strong>формы</strong> так и вне её</li>
         <li>В AJAX-запрос будут переданы все атрибуты элемента (div,button,span,img)</li>
     </ul>
     <p>Теги элемента:</p>
         <table class=params>
             <tr><td>class</td><td><ul><li>v1 - открытие модального окна</li>
                                       <li>v2 - выполнение AJAX-запроса</li>
                                   </ul></td>
             </tr>
             <tr><td>event</td><td><ul><li>click (по умолчанию)</li>
                                       <li>keyup</li>
                                    </ul>
                                    Если используется событие <strong>click</strong> (обычное для кнопки или div) то тег <strong>event</strong> можно не указывать
                                </td>
             </tr>
             <tr><td>ext</td><td>Название расширения, в котором находится принимающий AJAX-запрос скрипт <strong>ajax.php</strong>. Если тег не указан, запрос будет отправлен в <strong>/class/c_ajax.php</strong></td>
             <tr><td>cmd</td><td>Команда для выполения скриптом. Будет вызван метод класса <strong>c_page_ajax</strong> c именем <strong>AJAX_...cmd...</strong> </td>
             <tr><td>validate</td><td><ul><li>form - к форме будет применен плагин mForm.Submit, производящий валидацию формы по определенным для элементам правилам</li>
                                      </ul>
                                  </td>
             </tr>
             <tr><td>success</td><td>Название функции JS которой будет вызвана после выполения AJAX запроса и которой будут переданы результаты запроса.
                                     Также этот параметр можно указать в результатах запроса:<br>
                                     <ul><li>modal_window - результат будет показан в модальном окне</li>
                                          <li>func_name - имя функции JS которой будут переданы результаты запроса</li>
                                     </ul>
                                     <strong>Аналоги для ajax.php:</strong><br>
                                     add_element($doc,$xml,'success','modal_window') ;<br>
                                     add_element($doc,$xml,'success','.....') ;
                                 </td>
             </tr>
             <tr><td>title</td><td>Заголовок модального окна<br>
                                   <strong>Аналог для ajax.php:</strong><br>
                                   add_element($doc,$xml,'title','.....') ;<br>
                               </td>
             </tr>
             <tr><td>no_close_button</td><td>Не показывать кнопку "Закрыть"<br>
                                   <strong>Аналог для ajax.php:</strong><br>
                                   add_element($doc,$xml,'no_close_button',1) ;<br>
                               </td>
             <tr><td>after_close_update_page</td><td>После закрытия модального окна страница будет перезагружена<br>
                                   <strong>Аналог для ajax.php:</strong><br>
                                   add_element($doc,$xml,'after_close_update_page',1) ;<br>
                               </td>
             <tr><td>after_close_go_url</td><td>После закрытия модального окна перейти по заданному url<br>
                                   <strong>Аналог для ajax.php:</strong><br>
                                   add_element($doc,$xml,'after_close_go_url','.....') ;<br>
                               </td>
             </tr>
         </table>


    <h2>Кпопка для открытия модального окна из скрытой панели на странице</h2>
    <p>Панель, которая будет отображена в модальном окне, должна находиться в скрытом блоке</p>
     <div>
        <p><button class=v1 event=click panel_id=demo_modal_panel title="ПРОСТОЕ МОДАЛЬНОЕ ОКНО" modal_width=200>Открыть окно</button></p>
        <div class=hidden><div id=demo_modal_panel>Содержимое панели</div></div>
     </div>
     <textarea class=source_code></textarea>

    <h2>Кнопка вне формы для выполнения AJAX-запроса с проверкой валидности формы перед отправкой, результат отображается в модальном окне</h2>
    <p>В AJAX-запрос будут переданы все атрибуты кликнутого элемента (div,button,span,img)</p>
    <p>Для передачи резудьтатов в модальное окно у элемента должен быть установлен атрибут <strong>success=ajax_to_modal_window</strong></p>
    <p><button class="v2" ext=mBox cmd=demo_ajax_request success=ajax_to_modal_window>Выполнить запрос</button></p>
    <textarea class=source_code></textarea>

    <h2>Кнопка в форме для выполнения AJAX-запроса, результат отображается в модальном окне</h2>
    <p>В AJAX-запрос будут переданы данные формы в которой находиться элемент и все атрибуты кликнутого элемента</p>
    <p>Для передачи резудьтатов в модальное окно у элемента должен быть установлен атрибут <strong>success=ajax_to_modal_window</strong></p>
    <form>
        <input type=text name=data_1 value="Проверка">
        <label><input type=checkbox name=data_3 value="1" checked> test check</label>
        <br><br>
        <button class="v2" ext=mBox cmd=demo_ajax_request success=ajax_to_modal_window>Выполнить запрос</button>
    </form>
    <textarea class=source_code></textarea>

    <h2>Кнопка вне формы для выполнения AJAX-запроса, результат передается в JS-функцию</h2>
    <p><button class="v2" ext=mBox cmd=demo_ajax_request success=demo_ajax_request_success1>Выполнить запрос</button>
       <script type="text/javascript">
           function demo_ajax_request_success1(data) {alert_array(data)}
       </script>
    </p>
    <textarea class=source_code></textarea>

    <h2>Кнопка в форме для выполнения AJAX-запроса, результат передается в JS-функцию</h2>
    <form><input type=text name=data_1 value="Привет">
       <label><input type=checkbox name=data_3 value="1" checked> test check</label>
       <br><br>
       <button class="v2" ext=mBox cmd=demo_ajax_request success=demo_ajax_request_success2>Выполнить запрос</button>
       <script type="text/javascript">
           function demo_ajax_request_success2(data) {$j('div#ajax_result').html(data.html)}
       </script>
       <div id=ajax_result></div>
    </form>
    <textarea class=source_code></textarea>

    <h2>Проверка данных при вводе в текстовое поле через AJAX-запрос, результат передается в JS-функцию</h2>
    <form><input type=text name=data_1 class="v2" event="keyup" ext=mBox cmd=demo_ajax_check success=demo_ajax_request_success3><span id=ajax_result3></span>
       <script type="text/javascript">
           function demo_ajax_request_success3(data) {$j('span#ajax_result3').html('Длина текста - '+data.len)}
       </script>
    </form>
    <textarea class=source_code></textarea>

    <?

  }


}

$options['class_file']='this' ;
show_page($options) ;
?>