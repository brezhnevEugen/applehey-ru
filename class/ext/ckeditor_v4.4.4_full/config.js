/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';

// http://ckeditor.com/latest/samples/plugins/toolbar/toolbar.html

    config.bodyClass='editor';
    config.autoParagraph = false;
    config.scayt_autoStartup=false;
    config.allowedContent = true;
    config.filebrowserBrowseUrl=_PATH_TO_ADMIN_ADDONS+'/cpojer-mootools-filemanager-751d4ef/CKEditor.php' ;
    config.skinPath=_EXT+'/ckeditor_v4.4.4_full/skins/kama/' ;
    config.filebrowserUploadUrl=_PATH_TO_ADMIN+'/file_uploader.php?cmd=quick_upload&type=files' ;
    config.filebrowserImageUploadUrl=_PATH_TO_ADMIN+'/file_uploader.php?cmd=quick_upload&type=images' ;
    config.filebrowserFlashUploadUrl=_PATH_TO_ADMIN+'/file_uploader.php?cmd=quick_upload&type=flash' ;
    config.extraAllowedContent = 'mcr';

    config.toolbar=
        [	['Source'],
            ['Cut','Copy','Paste','PasteText','PasteFromWord'],
            ['Undo','Redo','-','Find','Replace','-','RemoveFormat'],
            ['Link','Unlink','Anchor'],
            ['Image','Flash','Table','SpecialChar','Iframe','CreateDiv'],
            '/',
            ['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],
            ['Format','Font','FontSize'],
            ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
            ['TextColor','BGColor'],
            ['NumberedList','BulletedList','-','Outdent','Indent','Blockquote','CreateDiv']
        ];

};
