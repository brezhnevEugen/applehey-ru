<?php
include (_DIR_TO_CLASS."/c_page.php") ;
class c_page_price extends c_page
{ // получаем инфу по текущему объекту
  public $h1='Прайс-лист' ;
  public $path=array('/'=>'Главная страница','/price.php'=>'Прайс-лист') ;
  public $allow_GET_params=array('ckey'=>'int') ;  // $allow_GET_params=array('dd'=>'int') ;
  public $checked_GET_params=1 ;      // разрешение проверки корректности параметров
  public $false_GET_params_action=301 ; // действие в случае некоректного параметра или некоректного url

  function set_head_tags()
  { parent::set_head_tags() ;
    $this->HEAD['css'][]=_PATH_TO_EXT.'/price_list/style.css' ;
    $this->HEAD['js'][]=_PATH_TO_EXT.'/image_lazyload/jquery.lazyload.js' ;
  }


 function select_obj_info()
  { global $goods_system ;
    if ($_GET['ckey']) $goods_system->select_obj_info_by_page(_CUR_PAGE_DIR,'goods_'.$_GET['ckey'].'.html') ;
    global $obj_info ;
    if ($obj_info['pkey']) $this->h1='Прайс-лист '.(($obj_info['__level']==3)? $obj_info['__parents_obj'][2]->name.'::'.$obj_info['__parents_obj'][3]->name:$obj_info['__name']) ;
  }

 // вывод основной информации
 function block_main()
 { global $obj_info,$goods_system;

   $this->page_title() ;
    ?><table class=list_goods><colgroup><col id='c1'><col id='c2'><col id='c3'><col id='c4'><col id='c5'></colgroup><?
    if ($obj_info['pkey']) $obj_info['__this_obj']->print_template('list_goods_price') ;
    else                   $goods_system->tree['root']->print_template('list_goods_price') ;
	?></table>
     <script type="text/javascript">
         $j("img").lazyload();
     </script>
     <?

     /*?><p class=cat_ref><a href='<?echo $obj_info['__href']?>'>Вернуться в каталог</a><br><br></p><?*/
    //damp_array($obj_info) ;

 }

}

