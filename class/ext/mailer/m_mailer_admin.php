<?php
$__functions['init'][]		='_mailer_admin_vars' ;
$__functions['alert'][]		='_mailer_admin_alert' ;
$__functions['install'][]	='_mailer_install_modul' ;
$__functions['boot_admin'][]='_mailer_admin_boot' ;

define('_DIR_TO_TEMPLATES_MAIL',   _DIR_TO_ROOT.'/'._CLASS_.'/templates_mail/') ;

include('clss_33.php') ;

function _mailer_admin_vars() //
{   global $descr_clss ;

	//-----------------------------------------------------------------------------------------------------------------------------
	// описание переменных модуля
	//-----------------------------------------------------------------------------------------------------------------------------
	//-----------------------------------------------------------------------------------------------------------------------------
	// описание классов модуля
	//-----------------------------------------------------------------------------------------------------------------------------

 	// сообщение системы запросов
 	// в старой версии:
 	// текст сообщения - value => obj_name
 	// обхект сообщения - obj_ref => obj_reffer
 	// имя объекта сообщения - obj_ref_name => obj_reffer_name
 	// аккаунт автора -  member => account_reffer

	$descr_clss[33]['name']='Почтовая рассылка' ;
	$descr_clss[33]['parent']=0 ;
	$descr_clss[33]['fields']=array('status'=>'int(11)','recipients'=>'int(11)','posted'=>'int(11)','error'=>'int(11)','value'=>'text') ;
    $descr_clss[33]['default']['status']=0 ;
	$descr_clss[33]['parent_to']=array(34,100) ;
	$descr_clss[33]['child_to']=array(1) ;
    $descr_clss[33]['menu'][]=array("name" => "Содержание рассылки","cmd" => 'show_mailer_text') ;
    //$descr_clss[33]['menu'][]=array("name" => "Источники для презентаций","cmd" => 'show_present_source') ;
    //$descr_clss[33]['menu'][]=array("name" => "Подготовка презентаций","cmd" =>'show_present_HTML') ;
    $descr_clss[33]['menu'][]=array("name" => "Адреса рассылки","cmd" =>'show_mailer_submitter') ;
    $descr_clss[33]['menu'][]=array("name" => "Очередь на отправку","cmd" =>'clss_33_show_mailer_turn') ;
    $descr_clss[33]['menu'][]=array("name" => "Отправленно","cmd" =>'clss_33_show_mailer_turn_send') ;
    $descr_clss[33]['menu'][]=array("name" => "Ошибки","cmd" =>'clss_33_show_mailer_turn_error') ;
    $descr_clss[33]['menu'][]=array("name" => "Лог рассылки","cmd" =>'clss_33_show_mailer_log') ;

    $descr_clss[33]['icons']=_PATH_TO_BASED_CLSS_IMG.'/7.png' ;

	$descr_clss[33]['list']['field']['pkey']=array('title'=>'Код') ;
	$descr_clss[33]['list']['field']['c_data']=array('title'=>'Создана') ;
	$descr_clss[33]['list']['field']['status']=array('title'=>'Состояние') ;
	$descr_clss[33]['list']['field']['status']=array('title'=>'Статус','indx_select'=>'list_status_mailer') ;
	$descr_clss[33]['list']['field']['obj_name']=array('title'=>'Тема') ;
    $descr_clss[33]['list']['field']['recipients']=array('title'=>'Получателей') ;
    $descr_clss[33]['list']['field']['posted']=array('title'=>'Отправлено') ;
    $descr_clss[33]['list']['field']['error']=array('title'=>'Ошибок') ;

    $descr_clss[33]['details']=$descr_clss[33]['list'] ;
    $descr_clss[33]['details']['field']['value']=array('title'=>'Текст','use_HTML_editor'=>1) ;

    $descr_clss[34]['name']='Задание в очереди отправки' ;
   	$descr_clss[34]['parent']=0 ;
   	$descr_clss[34]['fields']=array('value'=>'longtext','mail_to'=>'varchar(255)','mail_from'=>'varchar(255)','options'=>'text','error'=>'int(1)','send_result'=>'varchar(255)') ;
   	$descr_clss[34]['parent_to']=array(33) ;
    $descr_clss[34]['no_show_in_tree']=1 ;
    $descr_clss[34]['no_export_in_xml']=1 ;
    $descr_clss[34]['no_use_trash']=1 ;
    $descr_clss[34]['list']['options']=array('no_icon_edit'=>1,'read_only'=>1);

   	//$descr_clss[34]['list']['field']['enabled']		=array('title'=>'Состояние','class'=>'inverse') ;
   	$descr_clss[34]['list']['field']['c_data']		='Время постановки в очередь'  ;
   	//$descr_clss[34]['list']['field']['r_data']		='Время отправки'  ;
   	$descr_clss[34]['list']['field']['mail_from']	=array('title'=>'Отправить') ;
   	$descr_clss[34]['list']['field']['mail_to']		=array('title'=>'Получатель') ;
   	$descr_clss[34]['list']['field']['indx']		=array('title'=>'Попыток отправки') ;
   	$descr_clss[34]['list']['field']['send_result']		=array('title'=>'Результат отправки') ;
   	//$descr_clss[34]['list']['field']['obj_name']	=array('title'=>'Тема') ;
   	//$descr_clss[34]['list']['field']['options']		=array('title'=>'Опции') ;

    $descr_clss[34]['details']=$descr_clss[34]['list'] ;
    $descr_clss[34]['details']['field']['value']	=array('title'=>'Содержание письма') ;


	//----------------------------------------------------------------------------------------------------------------------
	// описание пунктов меню админа сайта
	//-----------------------------------------------------------------------------------------------------------------------
    $_SESSION['menu_admin_site']['Модули']['mailer']=array('name'=>'Почтовая рассылка','href'=>'editor_mailer.php','icon'=>'menu/mailer.jpg') ;

}

function _mailer_admin_boot($options)
{
	create_system_modul_obj('mailer') ;
}


function _mailer_install_modul($DOT_root)
{   global $dir_to_modules,$admin_dir ;
    include_once($dir_to_modules.'setup/m_setup_admin.php') ;
	echo '<h2>Инсталируем модуль <strong>Почтовая рассылка</strong></h2>' ;
    $file_items=array() ;
    // editor_mailer.php
    $file_items['editor_mailer.php']['include'][]='$dir_to_modules."mailer/m_mailer_frames.php"' ;
    $file_items['editor_mailer.php']['options']['use_table_code']='"TM_mailer"' ;
    $file_items['editor_mailer.php']['options']['title']='"Рассылка новостей"' ;
    $file_items['editor_mailer_tree.php']['include'][]='$dir_to_modules."mailer/m_mailer_frames.php"' ;
    $file_items['editor_mailer_viewer.php']['include'][]='$dir_to_modules."mailer/m_mailer_frames.php"' ;
    create_menu_item($file_items) ;

	// таблица запросов
	global $TM_mailer ;
	$pattern=array() ;

	$pattern['table_title']				= 	'Почтовая рассылка' ;
	$pattern['use_clss']				= 	'1,33' ;
	$pattern['def_recs'][]				=	array ('parent'=>0,	'clss'=>1,	'enabled'=>1,	'indx'=>0,	'obj_name'=>'Почтовая рассылка') ;
   	$pattern['table_name']				= 	$TM_mailer  ;
	$pattern['table_clss']				= 	101 ;
	$pattern['table_parent']			= 	$DOT_root;
   	$id_DOT=create_table_by_pattern($pattern) ;

    global $TM_mailer_zadan ;
   	$pattern=array() ;
   	$pattern['table_title']				= 	'Задания' ;
   	$pattern['use_clss']				= 	'34' ;
    $pattern['table_name']				= 	$TM_mailer_zadan ;
   	$pattern['table_clss']				= 	101 ;
   	$pattern['table_parent']			= 	$id_DOT;
   	create_table_by_pattern($pattern) ;

    SETUP_get_img('/mailer/admin_img_menu/mailer.jpg',$admin_dir.'img/menu/mailer.jpg') ;

    create_dir('/mailer/') ;
    SETUP_get_file('/mailer/system_dir/cron.php','/mailer/cron.php');
    SETUP_get_file('/mailer/system_dir/crontab.php','/mailer/crontab.php');
    SETUP_get_file('/mailer/system_dir/unsubscribed.php','/mailer/unsubscribed.php');

    SETUP_get_file('/mailer/class/c_mailer_cron.php','/class/c_mailer_cron.php');
    SETUP_get_file('/mailer/class/c_mailer_unsubscribed.php','/class/c_mailer_unsubscribed.php');

    create_dir('/templates_mail/') ;

    $id_sect=SETUP_add_section_to_site_setting('Рассылка') ;
    SETUP_add_rec_to_site_setting($id_sect,'Число писем отпавляемых за раз','10','LS_mailer_email_max_count') ;

}

function _mailer_admin_alert()
{ global $mailer_system ;
  $stats=$mailer_system->get_all_mailer_stats() ;
  if (sizeof($stats)) echo "<div>У Вас <span class='red bold'>".sizeof($stats)."</span> активных рассылок<br>в очереди <span class='red bold'>".array_sum($stats)."</span> писем</div><br>";
}

 function clss_33_frame_title($obj_info,$title)
 { $title.=' - '.$_SESSION['list_status_mailer'][$obj_info['status']]  ;
   return($title)  ;
 }

 // перезапустить рассылку
 function clss_33_refresh_mailer_zadan()
 { global $mailer_system,$obj_info ;
   echo '<div class=green>Пересоздаем очередь рассылки</div><br>' ;
   $mailer_system->reload($obj_info['pkey']) ;
   return('ok') ;
 }

 // запуск рассылки
 function clss_33_start_mailer()
 { global $mailer_system,$obj_info ;
   $cnt=$mailer_system->start($obj_info['pkey']) ;
   echo '<div class=green>Стартуем рассылку - в очереди на отправку <span class=black>'.$cnt.'</span> писем</div><br>' ;
   return('ok') ;
 }

 // остановка рассылки
 function clss_33_stop_mailer()
 { global $mailer_system,$obj_info ;
   $cnt=$mailer_system->stop($obj_info['pkey']) ;
   echo '<div class=green>Останавливаем рассылку</div><br>' ;
   return('ok') ;
 }

 function cmd_show_clss_33_show_field($cur_menu_item)
 {  ?><div class="black_bold">После сохранения отправить тестовое письмо на адрес <input type=text class="text big" name="_check_email" value="<?echo $_SESSION['check_email']?>"><br><br>
     После изменения содержания письма в очереди рассылки остаются задания со старым содержимым. <button class=button mode=dialog cmd=clss_33_refresh_mailer_zadan>Пересоздать очередь рассылки</button><br><br>
     <strong>Внимание!</strong> Уже отправленные письма заново посталены в очередь не будут!<br>
     <strong>Внимание!</strong> После измения темы или содержания рассылки рассылка будет автоматически переведена в состояние <strong>"Остановлена"</strong> <button class=button cmd=clss_33_start_mailer>Запустить рассылку</button>.<br>
     </div><br><?
    cmd_show_clss_0_show_field($cur_menu_item) ;
 }


 function clss_33_del_all_email_from_mailer()
 { global $obj_info,$mailer_system ;
   $cnt=$mailer_system->delete_email_from_mailer($obj_info['pkey']) ;
   echo '<div class=green>Из очереди удалено <strong>'.$cnt.'</strong> писем</div><br>' ;
   _event_reg('Очистка очереди рассылки','Из очереди удалено <strong>'.$cnt.'</strong> писем',$obj_info['_reffer']) ;
   return('ok') ;
 }

 function clss_33_clear_error_flag()
 { global $obj_info,$mailer_system ;
   $cnt=$mailer_system->clear_error_flag($obj_info['pkey']) ;
   echo '<div class=green>Повторно поставлено в очередь <strong>'.$cnt.'</strong> писем</div><br>' ;
   _event_reg('Повторная поставновка в очередь','<strong>'.$cnt.'</strong> писем с ошибкой доставки',$obj_info['_reffer']) ;
   return('ok') ;
 }

 function clss_33_add_email_to_mailer()
 { global $obj_info,$mailer_system ;
   $cnt=$mailer_system->add_email_to_mailer($obj_info,$_POST['source_emails'],$_POST['list_emails']) ;
   echo '<br><div class="green">Поставлено в очередь <strong>'.$cnt.'</strong> писем.</div>' ;
   return('ok') ;
 }

 function clss_33_sel_all_email_to_stock()
 { global $obj_info,$mailer_system ;
   $cnt=$mailer_system->restock_email_from_mailer($obj_info['pkey']) ;
   echo '<br><div class="green">Поставлено в очередь <strong>'.$cnt.'</strong> писем.</div>' ;
   return('ok') ;
 }




?>