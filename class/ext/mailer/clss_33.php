<?
include_once('clss_0.php') ;
class clss_33 extends clss_0
{

 // изменяем тему
 function on_change_event_obj_name($tkey,$pkey,&$fvalue)
 {  global $mailer_system ;
    // останавливаем рассылку
    $mailer_system->stop($pkey) ;
    // возвращаем 1 - значение плоля будет сохранено
    return(1);
 }

  // изменяем содержание
 function on_change_event_value($tkey,$pkey,&$fvalue)
 {  global $mailer_system ;
    if (isset($_POST['_check_email'])) $_SESSION['check_email']=$_POST['_check_email'] ;
    // останавливаем рассылку
    $mailer_system->stop($pkey) ;
    // сохраняем значение поля
    update_rec_in_table($tkey,array('value'=>$fvalue),'pkey='.$pkey) ;
    // оправлем проверочное письмо
    if ($_SESSION['check_email']) $mailer_system->send($pkey,$_SESSION['check_email']) ;
    // возвращаем 0 - значение плоля уже сохранено
 }

 function show_mailer_text()
 { global $obj_info ;
   //damp_array($obj_info) ;

   ?><div>Тема рассылки: <?echo generate_element2($obj_info,'obj_name','text',array('size'=>200))?></div><br><?
   show_window_ckeditor_v3($obj_info,'value',300,array('style_file'=>'/style_emails.css','editor_config'=>'fullPage: true'));
   $list_tempates=get_files_list(_DIR_TO_TEMPLATES_MAIL.'/',1,'html') ; //damp_array($list_tempates) ;
   ?><br><button cmd=save_list>Сохранить</button><br><br>
     <div class="black_bold">После сохранения отправить тестовое письмо на адрес <input type=text class="text big" name="_check_email" value="<?echo $_SESSION['check_email']?>"><br><br>
          После изменения содержания письма в очереди рассылки остаются задания со старым содержимым. <button class=button mode=dialog cmd=clss_33_refresh_mailer_zadan>Пересоздать очередь рассылки</button><br><br>
          Внимание! После измения темы или содержания рассылки рассылка будет автоматически переведена в состояние <strong>"Остановлена"</strong> <button class=button cmd=clss_33_start_mailer>Запустить рассылку</button>.<br>
          <? if (sizeof($list_tempates)){ ?><br>Шаблон макета рассылки: <select name=use_template><?
                                           foreach($list_tempates as $tempate_name) { $name=str_replace('.html','',$tempate_name);?><option value="<?echo $name?>" <?if ($_POST['use_template']==$name) echo 'selected'?>><?echo $name?></option><?}
                                           ?></select>&nbsp;&nbsp;<button class=button cmd=clss_33_upload_template_to_mailer>Загрузить</button><?
                                        }
   ?></div><?


 }

 function show_present_source()
 { global $obj_info ;
   ?>Скопируйте в корзину админки товар или новости, которые Вы собираетесь разослать<?
   $res=_CLSS(100)->show_list_items('obj_site_mailer_out_100','parent='.$obj_info['pkey']) ;

 }

 function show_present_HTML()
 { global $obj_info ;
   $arr_clss_id=_CLSS(100)->get_arr_clss_of_reffer_objs('obj_site_mailer_out_100','parent='.$obj_info['pkey']) ; // получаем массив id записей класса 100, которые объединены по классам Reffer
   //damp_array($arr_clss_id) ;
   if (!sizeof($arr_clss_id)) {?><div class=alert>У вас не выбраны источники для презентации</div>Для выбора источников скопируйте их в корзину и сделайте ссылку на них в рассылке<? return ;}
   $list_tempates=get_files_list(_DIR_TO_TEMPLATES_MAIL.'/',1,'php') ; //damp_array($list_tempates) ;
   if (!sizeof($list_tempates)) {?><div class=alert>У вас не создано ни одного шаблона для вывода в почту</div>Обратитесь к разработчикам сайта для создания шаблонов<? return ;}

   ?>Выберите тип объектов <select name=use_clss><? foreach($arr_clss_id as $clss=>$ids) {?><option value="<?echo $clss?>" <?if ($_POST['use_clss']==$clss) echo 'selected'?>><?echo _CLSS($clss)->name()?></option><?}?></select>&nbsp;&nbsp;&nbsp;<?
   ?>Выберите шаблон вывода <select name=use_template><? foreach($list_tempates as $tempate_name) { $name=str_replace('.php','',$tempate_name);?><option value="<?echo $name?>" <?if ($_POST['use_template']==$name) echo 'selected'?>><?echo $name?></option><?}?></select>&nbsp;&nbsp;&nbsp;<?
   ?><button cmd=show_present_HTML>Подготовить презентацию</button><?
   if (!isset($_POST['use_clss']) or !isset($_POST['use_template'])) return ;
   $use_clss=$_POST['use_clss'] ; $use_template=$_POST['use_template'] ;
   $template_file=_DIR_TO_TEMPLATES_MAIL.'/'.$use_template.'.php' ;
   if (!file_exists($template_file)) {?><div class=alert>Не удалось получить доступ к "<?echo $use_template?>"</div><? return ;}
   include_once($template_file) ;
   if (!function_exists($use_template)) {?><div class=alert>В файле шаблона "<span class=black><?echo hide_server_dir($template_file)?></span>" отсутствует функция "<span class=black><?echo $use_template?>"</span></div><? return ;}
   $objs_refs_info=select_reffers_objs('obj_site_mailer_out_100','pkey in ('.$arr_clss_id[$use_clss].')') ;
   //damp_array($_SESSION['list_created_sybsystems']) ;
   ob_start() ;
   if (sizeof($objs_refs_info)) foreach($objs_refs_info as $tkey=>$arr_clss)
    if (sizeof($arr_clss)) foreach($arr_clss as $clss=>$recs) if ($clss==$use_clss)
    { if (isset($_SESSION['list_created_sybsystems'][$tkey]))
       { $system_code=$_SESSION['list_created_sybsystems'][$tkey] ;
         $_SESSION[$system_code]->prepare_public_info_for_arr($recs) ;
       }
      $use_template($recs) ;
    }
   $rec['present']=ob_get_clean() ;
   ?><br><br><?
   show_window_ckeditor_v3($rec,'present',400,array('use_element_name'=>'present','style_file'=>'/style_emails.css')) ;


   //damp_array($objs_refs_info) ;
 }


 function show_mailer_submitter()
{ global $mailer_system ;
  $this->show_mailer_info() ;
  ?><button class=button cmd=clss_33_del_all_email_from_mailer>Очистить очередь рассылки</button>
    <h1>Адреса для рассылки</h1><p class="black bold">Уже находящиеся в очереди рассылки получатели повторно в очередь поставлены не будут.</p><br>
     <? $mailer_system->show_submitter_variants() ; ?>
     <br>Добавить подписчиков из спиcка email:<br><textarea rows="7" cols="100" class=text name="list_emails"></textarea>
     <br><br>
     <button class=button cmd=clss_33_add_email_to_mailer>Добавить адреса в рассылку</button><button class=button cmd=clss_33_start_mailer>Запустить рассылку</button><button class=button cmd=clss_33_stop_mailer>Остановть рассылку</button>
       <?
     }


 function clss_33_show_mailer_turn()
 { global $obj_info,$mailer_system ;
   $this->show_mailer_info() ;
   $options=array('buttons'=>array('delete')) ;
   ?><h1>Очередь писем на отправку</h1><?
   _CLSS(34)->show_list_items($mailer_system->table_zadan,'parent='.$obj_info['pkey'].' and enabled=1 and indx<'.$_SESSION['LS_mailer_max_send_cnt'],$options) ;
   return ;
}

 function clss_33_show_mailer_turn_send()
 { global $obj_info,$mailer_system ;
   $this->show_mailer_info() ;
   $options=array('buttons'=>array('delete')) ;
   ?><h1>Отправленные письма</h1><?
   _CLSS(34)->show_list_items($mailer_system->table_zadan,'parent='.$obj_info['pkey'].' and enabled=0',$options) ;
 }

 function clss_33_show_mailer_turn_error()
 { global $obj_info,$mailer_system ;
   $this->show_mailer_info() ;
   ?><button class=button cmd=clss_33_clear_error_flag>Попытаться отправить еще раз</button><?
   $options=array('buttons'=>array('delete')) ;
   ?><h1>Письма - не удалось отправить</h1><?
   _CLSS(34)->show_list_items($mailer_system->table_zadan,"parent=".$obj_info['pkey']." and error=1",$options) ;
 }

 function clss_33_show_mailer_log()
 { global $obj_info ;
   show_history("links like '%".$obj_info['_reffer']."%' or reffer='".$obj_info['_reffer']."'" ) ;
 }

 function show_mailer_info()
 { global $obj_info,$mailer_system ;
   $info=$mailer_system->get_mailer_stats($obj_info['pkey']) ;
   ?>Всего в очереди рассылок: <strong><? echo array_sum($info) ?></strong> писем => ОТПРАВЛЕНО: <strong><? echo $info[0] ?></strong> , В ОЧЕРЕДИ НА ОТПРАВКУ: <strong><? echo $info[1] ?></strong> , ОШИБКА:  <strong><? echo $info[2] ?></strong><br><br><?
 }


  function clss_33_upload_template_to_mailer()
  { global $obj_info ;
    if (isset($_POST['use_template']))
    { $template_file=_DIR_TO_TEMPLATES_MAIL.'/'.$_POST['use_template'].'.html' ;
       if (!file_exists($template_file)) {?><div class=alert>Не удалось получить доступ к "<?echo $_POST['use_template']?>"</div><? return ;}
       $text=file_get_contents($template_file) ;
       update_rec_in_table($obj_info['tkey'],array('value'=>$text),'pkey='.$obj_info['pkey']) ;
       $obj_info['value']=$text ;
    }
    return('ok') ;
  }



}

?>