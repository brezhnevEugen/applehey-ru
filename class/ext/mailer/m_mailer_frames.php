<?php
include_once($dir_to_engine."admin/c_site.php") ;
include_once($dir_to_engine."admin/i_clss_func.php") ;
include_once($dir_to_engine.'admin/c_menu_tabs.php') ;

// редактор заказов
class c_editor_mailer extends c_editor_obj
{
  function body($options=array()) { global $tkey,$patch_to_admin  ; $this->body_frame_2x(array('fra_tree'=>$patch_to_admin.'editor_mailer_tree.php?tkey='.$tkey,'title'=>$options['title'])) ; }
}


class c_editor_mailer_tree extends c_fra_tree
{

  function body($options=array())
  {?><body id=tree_objects><div id=tree_menu><?$this->top_menu() ;?></div><?
      $this->generate_time_tree('Почтовая рассылка',$this->tkey,array('usl_select'=>'clss=33','on_click_script'=>'editor_mailer_viewer.php','on_click_item'=>array(33=>'fra_viewer.php')));
   ?></body><?
  }
}

class c_editor_mailer_viewer extends c_fra_based
{ public $system_name='mailer_system' ;
  public $top_menu_name='top_menu_list_mailer' ;


 function body(&$options=array()) {$this->body_frame($options) ; }

 // заголовок фрейма
 function show_page_obj_header()
 { $title=($_GET['pkey']=='root')? 'Почтовые рассылки':'Рассылки '.$this->get_time_title($_GET['t1'],$_GET['t2']) ;
   ?><p class='obj_header'><?echo $title?></p><?
 }

 // меню фрейма
 function show_page_obj_menu($obj_info)
 { global $mailer_system ;
      // создаем объект top_menu
       if (!is_object($_SESSION[$this->top_menu_name])) $_SESSION[$this->top_menu_name]=new c_top_menu() ;
       // формируем набор пунктом меню
       $menu_set=array() ;
       // пункты меню для корня дерева
       if ($_GET['pkey']=='root')
       { $menu_set[]=array("name" => 'Новая рассылка',"cmd" => 'new_mailer') ;
         $menu_set[]=array("name" => 'Информация',"cmd" => 'mailer_info') ;
       }
       // или если выбран один из временных интервалов
       else
       { $time_usl=$this->get_time_usl($_GET['t1'],$_GET['t2']) ;
         $arr_status=$mailer_system->get_count_by_status($time_usl) ;
         if ($arr_status['']) { $arr_status[0]=$arr_status[0]+$arr_status[''] ; unset($arr_status['']) ; }

         if (sizeof($arr_status)) foreach($arr_status as $status=>$cnt) {$menu_set[]=array("name" => $_SESSION['list_status_mailer'][$status].'('.$cnt.')','cmd' =>'show_list_items','status'=>$status) ; }
         $menu_set[]=array("name" => 'Все ('.array_sum($arr_status).')',"cmd" =>'show_list_items') ;
       }
       // выводим меню
       $this->cur_menu_item=$_SESSION[$this->top_menu_name]->show($menu_set) ;  // показываем меню
       return($this->cur_menu_item) ;
 }

 function show_list_items()
 { if ($this->start_time or $this->end_time)  $_usl[]=$this->get_time_usl($this->start_time,$this->end_time) ;
   if (isset($this->cur_menu_item['status'])) $_usl[]='status='.$this->cur_menu_item['status'] ;
   $usl=implode(' and ',$_usl) ;
   $title='Рассылки '.$this->get_time_title($this->start_time,$this->end_time) ;
   if (isset($this->cur_menu_item['status'])) $title.=' в состоянии "'.$_SESSION['list_status_mailer'][$this->cur_menu_item['status']].'"' ;
   if ($this->cur_menu_item['mode']=='last_items')
     { $options['count']=$this->cur_menu_item['count'] ;
       $options['order']='c_data desc' ;
       $title.=' - последние '.$this->cur_menu_item['count'] ;
 }
     $options['default_order']='c_data desc' ;
     $options['title']=$title ;
     $options['buttons']=array('save','delete') ;
     _CLSS(33)->show_list_items($this->tkey,$usl,$options) ;
 }

 function new_mailer()
 {  global $mails_theme ;
    ?><div class="black bold">Укажите тему и создайте содержание рассылки. После сохранения рассылки Вы сможете добавить адресатов.</div><br>
       Тема: <input  type="text" class=text size=100 name="mails_theme" value="<?echo $mails_theme?>"><br><br><?
       show_window_ckeditor_v3(array(),'mail_text') ;
    ?><br><button class=button cmd=create_mail mode=before_select_obj_info>Создать новую рассылку</button><?
 }

 function create_mail()
 { global $mails_theme,$mail_text,$TM_mailer,$pkey,$t1,$patch_to_admin,$pkey_by_table ;
   $mail_text=$_POST['obj'][0][0][0]['mail_text'] ;
   $pkey=adding_rec_to_table($TM_mailer,array('clss'=>33,'parent'=>1,'obj_name'=>$mails_theme,'value'=>$mail_text,'status'=>0)) ;
   if ($pkey) $t1=0 ;
   $tkey=$pkey_by_table[$TM_mailer] ;
   header("Location: ".$patch_to_admin."fra_viewer.php?tkey=".$tkey."&pkey=".$pkey."&menu_id=item_2");
 }

 // информация по всем рассылками
 function mailer_info()
 {  global $mailer_system ;
    $info_0=execSQL_row('select parent,count(pkey) as cnt from '.$mailer_system->table_zadan.' where enabled=0 group by parent') ;
    $info_1=execSQL_row('select parent,count(pkey) as cnt from '.$mailer_system->table_zadan.' where enabled=1 group by parent') ;
    damp_array($info_0) ;
    damp_array($info_1) ;
 }

 function help()
 { global $dir_to_modules ;
   $cont=file_get_contents($dir_to_modules.'/mailer/help/index.html') ;
   echo $cont ;
 }

 function client_info()
 { global $mailer_system ;
   $all_cnt=0 ; $all_emails=array() ;
   ?><h1>Информация по клиенткой базе</h1><?
   $emails=$mailer_system->get_subscript_emails() ;
   //echo 'Всего email:<span class=green>'.sizeof($emails).'</span><br>' ;
   //$emails=array_unique($emails) ;
   echo '<h2>Информация по аккаунтам в подсистемах сайта:</h2>' ;
   $emails_cnt=$mailer_system->get_subscript_count() ;
   damp_array($emails_cnt,1,-1) ;
   echo 'Уникальных email:<span class=green>'.sizeof($emails).'</span><br>' ;
 }


 /*
  function clear_delete_mailer()
 {  global $tkey,$descr_obj_tables,$func_dialog,$time_usl,$status_usl,$list_status_mailer ;
 	//echo '$status_usl='.$status_usl.'<br>' ;
 	$list_mailer=execSQL('select *,'.$tkey.' as tkey from '.$descr_obj_tables[$tkey]->table_name.' where clss=82 '.$time_usl.' '.$status_usl.' mail by c_data desc') ;
 	if (!sizeof($list_mailer)) return ;
 	?><h1>Окончательно удаляем все заказы в статусе "Удален"</h1>
 	Эти заказы будут удалены окончательно, без возможности восстаноления:<br><br>
 	<? foreach($list_mailer as $rec) echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Заказ '.$rec['obj_name'].', статус - "<strong>'.$list_status_mailer[$rec['status']].'</strong>"<br>' ;?>
 	<br>
 	<button onclick="exe_cmd('')">Не, ну нах..</button>
    <button class=button cmd=apply_clear_delete_mailer mode=dialog>А мне пох...</button><?
 }

 function apply_clear_delete_mailer()
 {  global $tkey,$descr_obj_tables,$func_dialog,$time_usl,$status_usl,$list_status_mailer,$events_system ;
 	$table_name=$descr_obj_tables[$tkey]->table_name ;
 	$table_name_goods=$descr_obj_tables[$descr_obj_tables[$tkey]->list_clss[120]]->table_name ;
 	$table_name_commnent=$descr_obj_tables[$descr_obj_tables[$tkey]->list_clss[15]]->table_name ;

 	$list_mailer=execSQL('select *,'.$tkey.' as tkey from '.$table_name.' where clss=82 '.$time_usl.' '.$status_usl.' mail by c_data desc') ;
 	if (!sizeof($list_mailer)) return ;
 	foreach($list_mailer as $rec)
 	{ echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Удаляем заказ '.$rec['obj_name'].', статус - "<strong>'.$list_status_mailer[$rec['status']].'</strong>"<br>' ;

 	  $sql='delete from '.$table_name.' where pkey='.$rec['pkey'] ;
 	  $sql_goods='delete from '.$table_name_goods.' where parent='.$rec['pkey'] ;
 	  $sql_comment='delete from '.$table_name_commnent.' where parent='.$rec['pkey'] ;

 	  //echo 'sql='.$sql.'<br>' ;
 	  //echo 'sql='.$sql_goods.'<br>' ;
 	  //echo 'sql='.$sql_comment.'<br>' ;
 	  if (!$result = mysql_query($sql)) SQL_error_message($sql) ;
 	  if (!$result = mysql_query($sql_goods)) SQL_error_message($sql_goods) ;
 	  if (!$result = mysql_query($sql_comment)) SQL_error_message($sql_comment) ;
      // регистрируем событие 'Удаление заказа из БД'
      $events_system->register(array('obj_name'=>'Удаление заказа из БД','reffer'=>'','comment'=>'Заказ '.$rec['obj_name'].', статус - <strong>'.$list_status_mailer[$rec['status']].'</strong>')) ;

 	}

    ?><br><?
    return('ok') ;
 }      */

}





?>