$j.fn.tabs = function(options)
  {  $j(this).wrapInner('<div class=tabs></div>') ;
     $j(this).prepend('<div class=bookmark></div>') ;

     var settings = $j.extend(
             {  'bookmark_tag' : '.btitle'//,
                //'step' : '100'
             }, options);

     var panel_tabs=this  ;
     var panel_tabs_bookmark=$j(this).find('div.bookmark');
     var i=1 ;

     $j(this).find(settings['bookmark_tag']).each(function()
        {   $j(this).attr('num',i) ; $j(this).parent().attr('num',i) ;  i++ ;
            $j(panel_tabs_bookmark).prepend(this);
        });
      $j(panel_tabs_bookmark).find(settings['bookmark_tag']+':last-child').addClass('active') ;
      $j(panel_tabs_bookmark).find(settings['bookmark_tag']).click(function()
        {  $j(panel_tabs_bookmark).find(settings['bookmark_tag']).removeClass('active') ;
           $j(this).addClass('active') ;
           $j(panel_tabs).find('div.tabitem').hide() ;
           $j(panel_tabs).find('div.tabitem[num='+$j(this).attr('num')+']').show() ;
        }) ;
  } ;