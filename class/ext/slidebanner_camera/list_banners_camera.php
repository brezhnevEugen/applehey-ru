<?
  function list_banners_camera(&$list_recs,$options=array())
  { ?><div class="list_banners_camera camera_wrap" id=camera-slideshow><?
    if (sizeof($list_recs)) foreach($list_recs as $rec)
    { $img_info=pc_image_extractor($rec['value']) ;
      ?><div class=item data-src="<?echo $img_info[0][0]?>" data-link="/" data-thumb="/images/ban1s.jpg">
        <div class="camera_caption fadeIn">
          <div class = "container">
            <h2><?echo $rec['manual']?></h2>
          </div>
        </div>
      </div><?
    }?>
    </div>
    <script type="text/javascript">
         ;(function ($j) {
         var o = $j('#camera');
             if (o.length > 0) {
                 if (!(isIE() && (isIE() > 9))) {
                     include('/class/ext/Camera-master/scripts/jquery.mobile.customized.min.js');
                 }

                 include('js/camera.js');

                 $j(document).ready(function () {
                     o.camera({
                         autoAdvance: true,
                         height: '30.859375%',
                         minHeight: '350px',
                         pagination: false,
                         thumbnails: false,
                         playPause: false,
                         hover: false,
                         loader: 'none',
                         navigation: true,
                         navigationHover: false,
                         mobileNavHover: false,
                         fx: 'simpleFade'
                     })
                 });
             }
         })(jQuery);
    </script> <?
  }
?>