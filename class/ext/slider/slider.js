$j.fn.clss_slider = function(options)
{ var slider=this ;

    //$j(slider).html('<div class="inputs">от <input type=text class=from name="from" disabled value="'+options.from+'">
    //                до <input type=text class=to name="to" disabled value="'+options.to+'"></div>
    // <div class=selector><div class=marked></div>
    //                     <div class=marker_from><span></span></div>
    //                     <div class=marker_to><span></span></div>
    // </div>
    // <div class=slider_start_value></div>
    // <div class=slider_end_value></div>') ;

    $j(slider).html(
    '<input type=text class=from name="from" disabled hidden value="'+options.from+'">' +
    '<input type=text class=to name="to" disabled hidden value="'+options.to+'">'+
    '<div class="selector">'+
    	'<div class="marked" style="left: 52px; width: 79px;"></div>'+
    	'<div class="marker_from selected" style="left: 35px;"></div>'+
    	'<div class="marker_to" style="left: 113px;"></div>'+
    '</div>'+
    '<div class="m-slider-values">'+
    	'<span class="slider_start_value">$0</span>'+
    	'<span class="slider_end_value">$5,000</span>'+
        '<span class="m-slider-selected-value">$1,278 to $3,574</span>'+
    '</div>') ;
    //+'<div class=debug></div>') ;

  var slider_width=$j(slider).find('div.selector').width() ;
  var marker_width=$j(slider).find('div.marker_from').width() ;
  var start=options.start ;
  var end=options.end ;
  var from=options.from ;
  var to=options.to ;
  var step=options.step ;
  var slider_proc=slider_width/(end-start);

  $j(slider).find('div.marker_from').draggable({axis:'x',containment:'parent',drag:div_marker_from_drag,stop:div_marker_stop}) ;
  $j(slider).find('div.marker_to').draggable({axis:'x',containment:'parent',drag:div_marker_to_drag,stop:div_marker_stop}) ;
  $j(slider).find('.slider_start_value').html(start) ;
  $j(slider).find('.slider_end_value').html(end) ;
  var x_from=set_start_marker(from) ;
  var x_to=set_end_marker(to) ;
  set_marked_area(x_from,x_to) ;
  update_slider_selected_value() ;

  function div_marker_from_drag(e,UI)
    {  var x_from=UI.position.left ;
       var proc=x_from/slider_width ;
       var from=Math.round((end-start)*proc+start) ;
       from=Math.round(from/step)*step ;

       var x_to=slider.find('div.marker_to').position().left ;
       if (x_from+10>=x_to) return false ; // блокировка выхода за правый маркер

       slider.find('input.from').val(from).attr('used',((from>start)? 1:0));
       slider.find('div.marker_from span').html(from);
       set_marked_area(x_from,x_to) ;
       update_slider_selected_value() ;

       $j(slider).find('div.debug').html('slider_width='+slider_width+'<br>start='+start+'<br>end='+end+'<br>step='+step+'<br>x_from='+x_from+'<br>proc='+proc+'<br>from='+from+'<br>') ;
       return(true) ;
      }

    function div_marker_to_drag(e,UI)
    { var x_to=UI.position.left ;
      var x_to_r=x_to+marker_width ;
      var proc=x_to_r/slider_width ;
      var to=Math.round((end-start)*proc+start) ;
      to=Math.round(to/step)*step ;

      if (x_to_r>slider_width) return false ; // блокировка выхода за правую границу
      var x_from=slider.find('div.marker_from').position().left ;
      if (x_from+marker_width>x_to) return false ; // блокировка выхода за левый маркер

      slider.find('input.to').val(to).attr('used',((to<end)? 1:0));
      slider.find('div.marker_to span').html(to);
      $j(slider).find('div.debug').html('slider_width='+slider_width+'<br>start='+start+'<br>end='+end+'<br>step='+step+'<br>x_from='+x_from+'<br>x_to='+x_to+'<br>proc='+proc+'<br>to='+to+'<br>') ;

      set_marked_area(x_from,x_to) ;
      update_slider_selected_value() ;
      return(true) ;
    }

    function div_marker_stop() {$j('#form_filter').submit();}

    function update_slider_selected_value()
    { var min=slider.find('input.from').val() ;
      var max=slider.find('input.to').val() ;
      slider.find('span.m-slider-selected-value').text(min+' - '+max) ;

    }

    function set_marked_area(x_from,x_to)
    {  $j(slider).find('div.marked').css('left',x_from+marker_width/2+'px') ; // 5 - половина от ширины div#marker_from
       $j(slider).find('div.marked').css('width',x_to-x_from+'px') ;
       //$j(slider).find('div.price_graf div.sel_area').css('left',x_from+5+'px') ; // 5 - половина от ширины div#marker_from
       //$j(slider).find('div.price_graf div.sel_area').css('width',x_to-x_from+'px') ;
    }

    function set_start_marker(pos)
    {  var x=Math.round((pos-start)*slider_proc);
       //x=x-5 ;
       $j(slider).find('div.debug').html('slider_width='+slider_width+'<br>marker_width='+marker_width+'<br>start='+start+'<br>end='+end+'<br>step='+step+'<br>slider_proc='+slider_proc+'<br>x='+x+'<br>') ;
       $j(slider).find('div.marker_from').css('left',x+'px').show() ;
       $j(slider).find('input.from').val(pos).attr('used',((pos>start)? 1:0));
       $j(slider).find('div.marker_from span').html(pos);
       from=pos ;
       return(x) ;
    }

    function set_end_marker(pos)
    {  var x=Math.round((pos-start)*slider_proc)-marker_width ;
       //var x=Math.round((pos-start)*slider_proc) ;
       $j(slider).find('div.debug').html('slider_width='+slider_width+'<br>marker_width='+marker_width+'<br>start='+start+'<br>end='+end+'<br>step='+step+'<br>slider_proc='+slider_proc+'<br>x='+x+'<br>') ;
       $j(slider).find('div.marker_to').css('left',x+'px').show() ;
       $j(slider).find('input.to').val(pos).attr('used',((pos<end)? 1:0));
       if (pos<end) $j(slider).find('input.to').addClass('used') ; $j(slider).find('input.to').removeClass('used') ;
       $j(slider).find('div.marker_to span').html(pos);
       to=pos ;
       return(x) ;
    }
};