<?php
include (_DIR_TO_ENGINE."/c_page_XML.php") ;
class c_page_sitemap extends c_page_XML
{
  public $imp ; 
  public $doc_xml ; 
  public $urlset ;
    
  function on_send_header_before()
  {
    header("Content-type: text/xml;charset=UTF-8") ;
  }


 function show() //
 { // создаем структуру XML
   $this->imp = new DOMImplementation; // Creates an instance of the DOMImplementation class
   $this->doc_xml = $this->imp->createDocument("",""); // Creates a DOMDocument instance
   $this->doc_xml->formatOutput = true;
   $this->doc_xml->encoding = 'UTF-8';
   $this->urlset=$this->add_element($this->doc_xml,'urlset','',array()) ;

   $this->add_url(_PATH_TO_SITE,time()) ; // ГЛАВНАЯ СТРАНИЦА
   if (is_object($_SESSION['goods_system']))  $this->goods_system_export_pages_to_sitemap($this) ; // каталог товаров
   if (is_object($_SESSION['pages_system']))  $this->pages_system_export_pages_to_sitemap($this) ; // страницы сайта
   if (is_object($_SESSION['news_system']))   $this->news_system_export_pages_to_sitemap($this) ; // новости
   if (is_object($_SESSION['art_system']))    $this->art_system_export_pages_to_sitemap($this) ; // статьи

   ob_start() ;
   echo $this->doc_xml->saveXML() ;
   $cont=ob_get_clean() ;
   $res=str_replace('<urlset','<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"',$cont) ;
   echo $res ;
 }

 //-----------------------------------------------------------------------------------------------------------------------------
 // goods_system
 //-----------------------------------------------------------------------------------------------------------------------------
 function goods_system_export_pages_to_sitemap($page_xml)
   { global $goods_system ;
     $list_pages=execSQL('select pkey,r_data from '.$goods_system->table_name.' where '.$goods_system->tree_usl_select.' order by parent,indx') ;
     if (sizeof($list_pages)) foreach($list_pages as $rec) if (isset($goods_system->tree[$rec['pkey']]))
        { $href=_PATH_TO_SITE.$goods_system->tree[$rec['pkey']]->href ;
          $page_xml->add_url($href,$rec['r_data']) ;
        }

     $list_pages=execSQL('select pkey,parent,obj_name,url_name,r_data from '.$goods_system->table_name.' where '.$goods_system->usl_show_items.' order by parent,indx') ;
     if (sizeof($list_pages)) foreach($list_pages as $rec) if (isset($goods_system->tree[$rec['parent']]))
         { $goods_system->prepare_public_info($rec) ;
           $href=_PATH_TO_SITE.$rec['__href'] ;
           $page_xml->add_url($href,$rec['r_data']) ;
         }
   }

 //-----------------------------------------------------------------------------------------------------------------------------
 // pages_system
 //-----------------------------------------------------------------------------------------------------------------------------

 function pages_system_export_pages_to_sitemap($page_xml)
  { global $pages_system ; ;
    $list_pages=execSQL('select pkey,href,r_data from '.$pages_system->table_name.' where '.$pages_system->usl_show_items.' order by parent,indx') ;
    if (sizeof($list_pages)) foreach($list_pages as $rec)
       { $href=(strpos($rec['href'],'http')!==false)? $rec['href']:_PATH_TO_SITE.$rec['href'] ;
         $page_xml->add_url($href,$rec['r_data']) ;
       }
  }

 //-----------------------------------------------------------------------------------------------------------------------------
 // news_system
 //-----------------------------------------------------------------------------------------------------------------------------

  function news_system_export_pages_to_sitemap($page_xml)
   { global $news_system ;
     $list_pages=execSQL('select pkey,parent,obj_name,url_name,r_data from '.$news_system->table_name.' where '.$news_system->usl_show_items.' order by parent,indx') ;
     if (sizeof($list_pages)) foreach($list_pages as $rec) if (isset($news_system->tree[$rec['parent']]))
         { $news_system->prepare_public_info($rec) ;
           $href=_PATH_TO_SITE.$rec['__href'] ;
           $page_xml->add_url($href,$rec['r_data']) ;
         }
   }

 //-----------------------------------------------------------------------------------------------------------------------------
 // art_system
 //-----------------------------------------------------------------------------------------------------------------------------

 function art_system_export_pages_to_sitemap($page_xml)
  { global $art_system ; ;
    $list_pages=execSQL('select pkey,parent,obj_name,url_name,r_data from '.$art_system->table_name.' where '.$art_system->usl_show_items.' order by parent,indx') ;
    if (sizeof($list_pages)) foreach($list_pages as $rec) if (isset($art_system->tree[$rec['parent']]))
      { $art_system->prepare_public_info($rec) ;
        $href=_PATH_TO_SITE.$rec['__href'] ;
        $page_xml->add_url($href,$rec['r_data']) ;
      }
  }

 //-----------------------------------------------------------------------------------------------------------------------------
 //
 //-----------------------------------------------------------------------------------------------------------------------------

 function add_url($href,$lastmod)
 {   $url=$this->add_element($this->urlset,'url') ;
     $this->add_element($url,'loc',$href) ;
     $this->add_element($url,'lastmod',date('Y-m-d\TH:i:s+00:00',$lastmod)) ;
     $this->add_element($url,'changefreq','weekly') ;
 }

 function add_element(&$obj,$name,$value='',$attribute=array())
 {  $element=$this->doc_xml->createElement($name) ;
    if ($value) $element->appendChild($this->doc_xml->createTextNode($value));
    if (sizeof($attribute)) $this->add_attribute($element,$attribute) ;
    $obj->appendChild($element) ;
    return($element) ;
 }

 function add_attribute(&$obj,$arr)
 { if (sizeof($arr)) foreach($arr as $name=>$value)
   { $obj->appendChild($this->doc_xml->createAttribute($name)) ;
     $obj->setAttribute($name,$value) ;
   }
 }

}











?>