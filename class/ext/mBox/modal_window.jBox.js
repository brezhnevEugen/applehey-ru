// постепенное внедрение новых кнопок V2
// class=v2 - используем новый интерфейс
// event=click,keyup - событие, по которому начинается отработка запроса
// ext - использовать ajax.php из указанного расширения сайта
// ext_admin - использовать ajax.php из указанного расширения движка
// success - имя функции, которое будет выполнено после успешного выполения AJAX-запроса. Функции будет передан параметр responseXML
//         - ajax_to_modal_window - функция для показа результата в модальном окне
// остальные атрибуты кнопки являются опциональными и передаются
//
var ajax_script='/ajax.php' ;
var _FE_MODE=0 ;
var jBox_window ;

$j(document).ready(function()
{
    $j('.v1').prepare_form({}) ;
    $j('.v2').prepare_ajax({}) ;
    $j('[is_checked]').ajax_check({}) ;
    if ($j('input[mask]').length) $j('input[mask]').each(function(){$j(this).mask($j(this).attr('mask'),{placeholder:"X"});}) ;

    $j('textarea.source_code').each(function()
    { var code_id=$j(this).attr('code_id') ;
      var text="" ;
      if (code_id) text=$j('#'+code_id).html() ;
      else         text=$j(this).prev().html() ;
      // вычисляем сколько срок в тексте
      var  arr=text.split('\n');
      $j(this).val(text) ;
      $j(this).attr('rows',arr.length+2) ;
    })

    $j('div.jBox-Modal button.cancel').live('click',close_jBox_instances) ;
    $j('img.reload_check_code').live('click',function(){$j('img.checkcode').attr('src',$j('img.checkcode').attr('src'))}) ;
    new jBox('Confirm', {confirmButton: 'Да!',cancelButton: 'Нет'});
    $j('button.cancel').live('click',function(){if (jBox_window!=undefined) jBox_window.close()})
    
}) ;

function send_ajax_request(data,async)
{ if (async==undefined) async=true ;
  $j.ajax({url:'/ajax.php',type:'POST',dataType:'xml',async:async,success:exec_ajax_cmd_success_JQ,data:data});
}

$j.fn.open_modal_panel = function(options)
{
    new jBox('Modal', {openOnInit:true,
                    closeOnBodyClick:false,
                    closeOnMouseleave:false,
                    closeInTitle:true,
                    content:$j(this).attr('panel_id'),
                    title:$j(this).attr('title'),
                    position:{x:'center',y:'center'},
                    width:$j(this).attr('modal_width'),
                    overlay:true,
                    overlayStyles:{color: 'black',opacity: 0.5},
                    overlayFadeDuration: 50,
                    fade: {open: true,close: true},
                    fadeDuration: 500,
                    buttons: [ { title: 'Закрыть'}],
                    //onOpen: function() { if (data.no_close_button!=1) this.addButtons({ title: 'Закрыть' })},
                    onClose: function(){ //if (data.after_close_update_page) location.reload() ;
                                         //if (data.after_close_go_url) document.location=data.after_close_go_url ;
                                       }

                   });
}   ;

$j.fn.prepare_form = function(options)
{ if ($j(this).hasClass('off') || $j(this).hasClass('return')) return ;
  $j(this).each(function(i)
  {   // если объект находиться в контейнене с параметром reffer - добавляем этот параметр в текущий элемент
      if ($j(this).attr('reffer')==undefined && $j(this).closest('[reffer]').length) $j(this).attr('reffer',$j(this).closest('[reffer]').attr('reffer')) ;

      var submitFormOnEnter=($j(this).attr('NoSubmitFormOnEnter'))? false:true ;
      // если задано условие проверки валидности формы - оформляем форму через mForm.Submit
      if ($j(this).attr('validate')=='form')
      {   new mForm.Submit(
                 { form: form[0],
                   ajax:false,
                   validateOnBlur: true,
                   shakeSubmitButton:false,
                   submitFormOnEnter: submitFormOnEnter
                 }) ;
      }
      else // иначе цепляем ajax на соответствующее событие
      {  //if ($j(this).attr('event')=="click") $j(this).live('click',function(){$j(this).open_modal_panel({})}) ;
         if ($j(this).attr('href')!=undefined && $j(this).attr('href')!="" && !$j(this).hasClass('off')) $j(this).live('click',function()
         {   if ($j(this).attr('window')) window.open($j(this).attr('href'),$j(this).attr('window'),'status=1,dependent=1,width=1200,height=600, resizable=1, scrollbars=yes')
             //else $j(location).attr('href','http://'+_MAIN_DOMAIN+$j(this).attr('href'));
             else $j(location).attr('href',$j(this).attr('href'));
         }) ;
         else if ($j(this).attr('update_page')!=undefined && !$j(this).hasClass('off')) $j(this).live('click',function(){ document.location=document.location ;}) ;
         else $j(this).live('click',send_current_form) ;
      }

  });

  function send_current_form()
  { var form=$j(this).closest('form') ;
    if (!form.length)  { $j(this).wrap('<form style="display:inline;"></form>') ; form=$j(this).closest('form') ; }
    $j(form).attr('method','POST') ; // все отпавки форм только через POST

    // добавляем все атрибуты кнопки как скрытые переменные формы
    convert_tags_to_hiddend_vars(this,form) ; // добавляем атрибуты кликнутой кнопки в форму в виде скрытых переменных - они уйдут в POST через AJAX
    // отправляем форму
    form.submit() ;
  }
} ;

$j.fn.prepare_ajax = function(options)
{ $j(this).each(function(i)
  { if ($j(this).hasClass('off') || $j(this).hasClass('return') || $j(this).hasClass('cancel')) return ;
    if (!$j(this).hasClass('ok') && options.reload_form!=1)
  {   // если объект находиться в контейнене с параметром reffer - добавляем этот параметр в текущий элемент
      if ($j(this).attr('reffer')==undefined)
         if ($j(this).closest('[reffer]').length) $j(this).attr('reffer',$j(this).closest('[reffer]').attr('reffer')) ;

      var submitFormOnEnter=($j(this).attr('NoSubmitFormOnEnter')=='')? false:true ;
      // эмулируем отправку форму по ENTER - так как передеача параметра через OPTIONS не работает
      if (submitFormOnEnter) $j(this).closest('form').find('input[type=text],input[type=password]').keydown(send_form_by_enter) ; // поиск по нажатию на enter
      else                   $j(this).closest('form').find('input[type=text],input[type=password]').keydown(enter_as_tab) ; // переход как по TAB

      // если задано условие проверки валидности формы - оформляем форму через mForm.Submit - в этом случае ajax будет отрабатываться по form.submit
      if ($j(this).attr('validate')=='form')
      {   if (this.nodeName=='SELECT' && $j(this).hasClass('data-select'))  new mForm.Element.Select({original:this,max:20,maxBuffer:2,onClose:exec_ajax_cmd_to_data_select});
          if (this.nodeName=='INPUT' && this.type=='checkbox') $j(this).live('change',exec_ajax_cmd) ; // изминение данных - для checkbox
          var form=$j(this).closest('form') ;
          if ($j(form).attr('action')==undefined) $j(form).attr('action',ajax_script) ;
          $j(form).attr('method','POST') ;
          new mForm.Submit(
                 { form: form[0],
                   ajax:true,
                   validateOnBlur: true,
                   submitFormOnEnter: submitFormOnEnter,
                   shakeSubmitButton:false,
                   addOverlay: true,
                   showLoader: true,
                   responseSuccess:'any',
                   onSuccess:exec_ajax_cmd_success_MOO,
                   onError:exec_ajax_cmd_error_MOO,
                   onResponseError:exec_ajax_cmd_error_MOO
                 }) ;
      }
      else // иначе цепляем ajax на соответствующее событие
      {   var event=$j(this).attr('event') ;

          if (this.nodeName=='SELECT' && $j(this).hasClass('data-select'))  new mForm.Element.Select({original:this,max:20,maxBuffer:2,onClose:exec_ajax_cmd_to_data_select}); // для select.data-select
          else if (this.nodeName=='SELECT') $j(this).live('change',exec_ajax_cmd) ; // изминение данных - для select
          else if (this.nodeName=='INPUT' && this.type=='checkbox') $j(this).live('change',exec_ajax_cmd) ; // изминение данных - для checkbox
          else if (this.nodeName=='INPUT' && this.type=='text') $j(this).blur(exec_ajax_cmd) ; // изминение данных - для checkbox
          else if (event=='keyup')  $j(this).live('keyup',exec_ajax_cmd) ; // нажатие кнопки - для input
          //else if (event=='enter')  $j(this).live('keyup',exec_ajax_cmd) ; // нажатие кнопки - для input
          else if (event=='change') $j(this).live('change',exec_ajax_cmd) ; // изминение данных - для select
          else                                 $j(this).live('click',exec_ajax_cmd) ; // по умолчанию - click
      }
      $j(this).addClass('ok') ;
    }
  });

  function exec_ajax_cmd_to_data_select()
  {
      var element=this.original ;
      exec_ajax_cmd(element) ;
  }

  function exec_ajax_cmd(element)
    {
      if (element.nodeName!="SELECT") element=this ;
      if (!$j(element).attr('cmd')) return ;
      var params=convert_tag_attributes_to_array(element) ;
      //if (params['event']=='keyup') alert(element.keyCode) ;
      if (!params['value'] || params['event']=='keydown') params['value']=$j(element).val() ; // последнее условие сделано для корретной отправки данных при изменении значения в input
      if (!params['value'] || params['event']=='keyup') params['value']=$j(element).val() ; // последнее условие сделано для корретной отправки данных при изменении значения в input
      if ($j(element).attr('type')=="text") params['value']=$j(element).val() ; // корректное получение значения input.text
      if ($j(element).attr('type')=="checkbox") params['checked']=($j(element).attr('checked')? 1:0) ;

      if (element.nodeName=="SELECT") params['value']=$j(element).val(); // для выпадающих списков
      // проверяем - если есть форма, то отпралявем через ajax данные формы
      var form=$j(element).closest('form') ;
      //if (form.length && !$j(element).attr('no_send_form'))
      if (form.length && $j(this).attr('no_send_form')==undefined)
      {  $j(form).submit(function(){return false; }); // если есть форма, блокируем её отправку стандартным способом
         $j(form).ajaxSubmit({url:ajax_script,type:'POST',dataType:'xml',cache:false,error:exec_ajax_cmd_error_JQ,success:exec_ajax_cmd_success_JQ,data:params})  ;
      }
      // если кнопка вне формы - переводим все атрибуты кнопки в переменные и тоже на ajax
      else             $j.ajax({url:ajax_script,type:'POST',dataType:'xml',cache:false,error:exec_ajax_cmd_error_JQ,success:exec_ajax_cmd_success_JQ,data:params});
      if ($j(element).attr('type')!='checkbox' && $j(element).attr('type')!='radio') return(false) ;
    }

} ;

  // первая принимающая функция - для ajax через Mootools
  // $j(this.form)  - форма, из которой был сделан запрос
  function exec_ajax_cmd_success_MOO(responseText, responseXML)
  { if (responseXML==null) return(false) ;
    if (responseXML==undefined) return ;
    var data=convert_XML_to_obj(responseXML) ;

    // если передан атрибут и в документе существует блок с таким ID - заменить содержимое блока
    update_panel_by_tag_name(responseXML) ;

    if (data.close_mBox_window) close_jBox_instances('mBox_'+data.close_mBox_window);

    if (data.success) window[data.success](data,responseXML);
    if (data.show_notife)              show_notife2(responseXML);
    if (data.show_modal_window)        show_modal_window(responseXML);
    if (data.update_page==1)           location.reload() ;
    if (data.go_url)                   document.location=data.go_url ;
    if (data.JSCODE)                   $j('body').append(data.JSCODE) ;

    // назначаеим обработчики событий для ввовь созданных элементов
    $j('.v1').prepare_form({}) ;
    $j('.v2').prepare_ajax({}) ;
    $j('[is_checked]').ajax_check({}) ;
    if (_FE_MODE==1) $j(document).fastedit() ;
    if ($j('input[mask]').length) $j('input[mask]').each(function(){$j(this).mask($j(this).attr('mask'));}) ;



    //$j(data).each(function(){if ($j())}) ;

    update_capcha() ;
  }


  // вторая принимающая функция - для ajax через JQuery
  function exec_ajax_cmd_success_JQ(data,status,link)
  {  if (status=='success')
     {  var responseXML = link.responseXML;
        exec_ajax_cmd_success_MOO(data, responseXML)
     }
  }

  function exec_ajax_cmd_error_JQ(request,message,isk)
  {
    //if (isk.message!='undefined')
        alert(message+"\n\n"+isk.message) ;
  }

  function exec_ajax_cmd_error_MOO(response)
  {
    //alert(response) ;
  }

 // если передан атрибут и в документе существует блок с таким ID - заменить содержимое блока
 function update_panel_by_tag_name(XML)
 { var param ;
   // разные режимы изменения элемента

   var xml_doc=XML.childNodes[0] ;
   if (!xml_doc.childNodes.length) xml_doc=XML.childNodes[1] ;
   for(var j=0; j<xml_doc.childNodes.length; j++)
   { var mode='replace_element' ;
     param=xml_doc.childNodes[j] ;
     if (param.nodeType==1)
     { // получаем имя и атрибуты элемента
       var name=param.nodeName ;
       // если имя содержит .HTML - заменяем содержимое элемента
       if(name.indexOf('.HTML') + 1) { name=name.replace(".HTML","") ; mode='update_inner_html' ; }
       if(name.indexOf('.SRC') + 1) { name=name.replace(".SRC","") ; mode='update_src' ; }
       if(name.indexOf('.CLASS') + 1) { name=name.replace(".CLASS","") ; mode='update_class' ; }
       // если элемент с таким id существует
       if ((name!='title' && name!='success' && name!='html' && name!='text') && $j('#'+name)[0]!=undefined)
       { var mode=(param.attributes.getNamedItem('mode')!=null)? param.attributes.getNamedItem('mode').value:mode ;
         //var value=(param.childNodes.length)? param.childNodes[0].nodeValue:"" ;
         //var value=(param.childNodes.length)? param.childNodes[0].innerHTML:"" ;
         //var value=(param.childNodes.length)? param.childNodes[0].textContent:"" ;
         //var value=(param.childNodes.length)? param.innerHTML:"" ;
         var value=(param.childNodes.length)? param.textContent:"" ;

         var element=$j('#'+name) ;
         // если элемент является INPUT или TEXTAREA - обновляем значение элемента
         if (element[0].nodeName=='INPUT' || element[0].nodeName=='TEXTAREA' || element[0].nodeName=='SELECT') mode='update_value' ;
         // если в атрибуте указан режим - испоьзуем этот режим
         if (mode=='append')  { mode='append_to_element' ;  if (element[0].nodeName=='TEXTAREA') mode='append_to_textarea' ; }
         if (mode=='update')    mode='update_inner_html' ;
         if (mode=='replace')   mode='replace_element' ;

         switch(mode)
         { case 'replace_element':     element.replaceWith(value) ; break ;
           case 'append_to_element':   element.append(value) ; //+element[0].scrollHeight+' '+element[0].clientHeight) ;
                                       element.animate({scrollTop:element[0].scrollHeight-element[0].clientHeight});
                                       break ;
           case 'append_to_textarea':  element.val($j('#'+name).val()+"\n"+value) ; break ;
           case 'update_value':        element.val(value) ; break ;
           case 'update_inner_html':   element.html(value) ; break ;
           case 'update_src':          element.attr('src',value) ; break ;
           case 'update_class':        element.addClass(value) ; break ;
         }
       }
     }
   }
 }


// показ уведомления
// add_element($doc,$xml,'success','show_notife',array('type'=>'info','pos_x'=>'center','pos_y'=>'center')) ;

function show_notife(data,responseXML)
{ //var tags=get_xml_tag_attr(responseXML,'success') ;
  var notife_type='error' ; if (data.type!=undefined) notife_type=data.type ;
  var pos_x='right' ; if (data.pos_x!=undefined) pos_x=data.pos_x ;
  var pos_y='top' ; if (data.pos_y!=undefined) pos_y=data.pos_y ;
  var delayClose=1500 ; if (data.delayClose!=undefined) delayClose=data.delayClose ;

  // другие варианты $position="{x:'right',y: ['bottom', 'outside']}" ; $target=$open_element ;
  if (data.text!="") new jBox('Notice', {content: data.text});
  //new mBox.Notice({content:data.text,type:notife_type,position:{x:pos_x,y:pos_y},overlayFadeDuration: 50,delayClose:delayClose});
}

function show_notife2(responseXML)
{ var tags=get_xml_tag_attr(responseXML,'show_notife') ;
  var text=get_xml_tag_value(responseXML,'show_notife') ;
  var params={};
  params.content=text ;
  if (tags.color) params.color=tags.color ;
  if (tags.type=='error') params.color='red' ;
  var pos_x='right' ; if (tags.pos_x!=undefined) pos_x=tags.pos_x ;
  var pos_y='top' ; if (tags.pos_y!=undefined) pos_y=tags.pos_y ;
  params.delayClose=1500 ; if (tags.delayClose!=undefined) params.delayClose=tags.delayClose ;
  params.position={x:pos_x,y:pos_y} ;
  // другие варианты $position="{x:'right',y: ['bottom', 'outside']}" ; $target=$open_element ;
  show_notife3(params) ;
  //new mBox.Notice({content:text,type:notife_type,position:{x:pos_x,y:pos_y},overlayFadeDuration: 50,delayClose:delayClose});
}

function show_notife3(params)
{
  new jBox('Notice',params);
}

// показ результатов ajax  в модальном окне
// data - массив с параметрами окна
// data.html            - текст, который будет выведен в окне
// data.text            - тот же html но очищенный от метатегов - для вывода в случае ошибкт
// data.no_close_button - показывать или нет кнопку "закрыть"
// data.modal_panel_width   -   ширина модального окна
// data.after_close_update_page - обновление страницы/фрейма после закрытия модального окна
// data.after_close_go_url - переход на указанный url после закрытия модального окна
function modal_window(data,responseXML){ajax_to_modal_window(data,responseXML)}
function show_modal_window(responseXML)
{ var tags=get_xml_tag_attr(responseXML,'show_modal_window') ;
  var text=get_xml_tag_value(responseXML,'show_modal_window') ;
  // совместимость со старыми параметрами
  if (tags.width!=undefined)  tags.modal_panel_width=tags.width ;
  if (tags.pos_x!=undefined)  tags.modal_panel_pos_x=tags.pos_x ;
  if (tags.pos_y!=undefined)  tags.modal_panel_pos_y=tags.pos_y ;
  if (tags.target!=undefined) tags.modal_panel_target=tags.target ;
  if (tags.cmd==undefined)    tags.cmd=get_xml_tag_value(responseXML,'cmd');
  if (text=='HTML')           tags.html=get_xml_tag_value(responseXML,'html');

  ajax_to_modal_window(tags,responseXML)
}
function ajax_to_modal_window(data,responseXML)
    {  var width = (data.modal_panel_width)? data.modal_panel_width:500 ;  //  ширина модального окна
       var pos_x=(data.modal_panel_pos_x)? eval(data.modal_panel_pos_x):'center'  ;
       var pos_y=(data.modal_panel_pos_y)? eval(data.modal_panel_pos_y):'center'  ;      // другие варианты $position="{x:'right',y: ['bottom', 'outside']}" ; $target=$open_element ;
       var target=(data.modal_panel_target)? data.modal_panel_target:null  ;      // другие варианты $position="{x:'right',y: ['bottom', 'outside']}" ; $target=$open_element ;
       //var result_panel_id=data.cmd+'_ajax_result' ;
                 // промежуточная панель используется для того, чтобы отработался JS код из html, будет автоматом удалена после mBox_ajax_result.open();
                 //$j('div#mBox_'+data.cmd).next('div').remove() ;
                 //$j('div#mBox_'+data.cmd).remove() ;
                 //$j('div#'+result_panel_id).remove() ;
                 //$j('body').after('<div id='+result_panel_id+'>'+data.html+'</div>') ;
                 jBox_window=   new jBox('Modal', {id : 'mBox_'+data.cmd,
                                    openOnInit:false,
                                    closeOnBodyClick:false,
                                    closeOnMouseleave:false,
                                    closeInTitle:true,
                                    //content:$j('#'+result_panel_id),
                                    content:data.html,
                                    title:data.title,
                                    target:target,
                                    position:{x:pos_x,y:pos_y},
                                    width:width,
                                    overlay:true,
                                    overlayStyles:{color: 'black',opacity: 0.5},
                                    overlayFadeDuration: 50,
                                    fade: {open: true,close: true},
                                    fadeDuration: 500,
                                    closeButton:'overlay',
                                    onOpen: function()
                                    { //if (!data.no_close_button) this.addButtons([{ title: 'Закрыть' }]) ;
                                      //if (data.button_1) this.addButtons([{}]) ;
                                    },
                                    onOpenComplete:function()
                                    { $j(this.content).find('.cancel').click(function() { $j(this).jBox_instances().close() ; }) ;
                                      $j(this.content).find('.v1').prepare_form({}) ;
                                      $j(this.content).find('.v2').prepare_ajax({}) ;
                                      $j('[is_checked]').ajax_check({}) ;
                                      if ($j('input[mask]').length) $j('input[mask]').each(function(){$j(this).mask($j(this).attr('mask'),{placeholder:"X"});}) ;


                                      //var el_id=data.el_id  ;
                                      //$j('div#inner_'+el_id).tinyscrollbar({}) ;
                                      ///$j('img.zoom').hs_zoom({}) ;
                                      //$j('img.zoom2').hs_zoom2({}) ;
                                    },
                                    onClose: function()
                                    { if (data.after_close_update_page) location.reload() ;
                                      if (data.after_close_go_url) document.location=data.after_close_go_url ;
                                      if (data.after_close_func) eval(data.after_close_func+'()')  ;
                                    }
                       });
        jBox_window.open() ;

    }

// возвращает экземпляр модального окна mBox, в котором находиться указанный элемент
function get_jBox_instances(mBox_id)
{  var jBox_instances=null ;
   jBox.instances.each(function(instance)
     { try { if (instance.id=mBox_id) jBox_instances=instance; }
       catch(e) {}
                                   });
   return(jBox_instances) ;
              }

// возвращает экземпляр модального окна mBox, в котором находиться указанный элемент
function close_jBox_instances(mBox_id)
{   if (jBox_window!=undefined) jBox_window.close() ;
    return ;
    jBox.instances.each(function(instance)
     { try { if (mBox_id='mBox_all') instance.close();
             else if (instance.id=mBox_id) instance.close(); }
       catch(e) {}
     });
}

// возвращает экземпляр модального окна mBox, в котором находиться указанный элемент
$j.fn.jBox_instances = function()
{  var jBox_instances=null ;
   var mBox_id='mBox_'+$j(this).closest('.mBox').attr('id') ;
   jBox.instances.each(function(instance)
     { try { if (instance.id=mBox_id) jBox_instances=instance; }
       catch(e) {}
     }) ;
   return(jBox_instances) ;
} ;

function alert_array(data)
  { var acc = [] ;
    $j.each(data, function(index, value){acc.push(index + ': ' + value);});
    alert(JSON.stringify(acc,"",1));
  }

// возвращем массив с атрибутами тега
 // просто вернуть .attributes нельзя, так как в IE в этом свойсве хранятся, помимо атрибутов, также и DOM-свойства объекта. Другого места, где находились бы атрибуты тега, в IE нет.
 // поэтому для выделения тегов разбираем  outerHTML тега
 // можно разобрать outerHTML через регулярное выражение /\b((\w+)=(["'])([^"']+)["'])/ig  при условии, что все значения тегов заключены в ковычки
 // но и тут IE отличился -  его outerHTML не заключает в ковычки атрибуты id,name,value: <input id=test class="demo my input" reffer="1111" cmd="222/888" name=a333 value=444s>
 // что сильно усложняет подбор регулярного выражения
 // в итоге был выбран промежуточный вариант - имена тегов получаем из outerHTML, а значения тегов из .attributes
 function convert_tag_attributes_to_array(tag_obj)
 {   var arr_attr= {} ;
     var text=tag_obj.outerHTML ;
     var myRe = /\b((\w+)=)/ig;
      while ((myArray = myRe.exec(text)) != null)
      {   var name = myArray[2] ;
          var attr=tag_obj.attributes.getNamedItem(name) ;
          if (attr!=null) arr_attr[name] = attr.value ;
   }
    return arr_attr ;
}

// переменосим параметры из <input type=submit......> в сткрытые переменные перед отправкой формы по AJAX
// вызывается из mForm.Submit.js
function convert_tags_to_hiddend_vars(input,form)
{  // удаляем старые временные переменные, если форма отправляется повторно
   $j(form).find('input.temp[type="hidden"]').remove() ;
   // переводим все аттрибуты кликнутого элемента в скрытые переменные
   //ВНИМАНИЕ! в IE в input.attributes будут находиться все свойства объекта, а не только атрибуты тега
   var arr_attr=convert_tag_attributes_to_array(input) ;
   for (key in arr_attr) $j(form).append('<input type="hidden" class="temp" name="'+key+'" value="'+arr_attr[key]+'">') ;
   {   //$j(form).append('<input type="hidden" class="temp" name="'+key+'" value="'+arr_attr[key]+'">') ;
       //var temp_input = new Element('input', {'type': 'hidden','class': 'temp','name': key,'value':arr_attr[key]});
       //temp_input.inject(form) ;
   }
   //$j(form).reInit=1 ;
   //for(var j=0; j<input.attributes.length; j++) $j(form).add($j('<input type="hidden" class="temp" name="'+input.attributes.item(j).nodeName+'" value="'+input.attributes.item(j).nodeValue+'">').get()) ;
   // сохраняем содержимое радектора в форме
   //if (editor)  { $j('textarea[name="'+editor.name+'"]').val(editor.getData()) ;  }
   //if (CKEDITOR!=undefined && CKEDITOR.length) $j.each(CKEDITOR.instances,function(){this.updateElement()})  ;
   //
   //removeEditor() ;
}



function update_capcha()
{  // обновляем капчу по всем документе сразу - чтобы не было разных данных
  //$j(panel).find('img.checkcode').attr('src',$j(panel).find('img.checkcode').attr('src')) ;
  //$j(panel).find('input.checkcode').val('') ;
  $j('img.checkcode').attr('src',$j('img.checkcode').attr('src')) ;
  $j('input[name="_check_code"]').val('') ;
}

// проверка корректности полей формы через AJAX ---------------------------------------------------------------------------------------------------------------------------------

$j.fn.ajax_check = function(options)
{ var settings = $j.extend(
     { position : {x: 'right',y:'center'},
       outside:'x'
     }, options);


 $j(this).each(function(i)
 { var cur_element=this ;
   /*var tooltip_alert=new jBox('Tooltip',
                           { attach: $j('#'+$j(cur_element).attr('id')),
                             position: {x: 'right',y:'center'},
                             outside: 'x',
                             trigger:'click',
                             theme:'TooltipBorder'
                           });
   */
     $j(cur_element).after('<div class="check_result"></div>') ;

   $j(this).live('change',function()
       {  $j.ajax({url:ajax_script,type:'POST',dataType:'xml',cache:false,success:check_success,
                   data:{   ext:$j(this).attr('ext'),
                            cmd:$j(this).attr('is_checked'),
                            element_id:$j(this).attr('id'),
                            value:$j(this).val()
                        }
                  });
       }) ;

   function check_success(data,status,link)
      { if (status=='success')
          { var xml = link.responseXML;
            if (xml==undefined) return ;
        var data=convert_XML_to_obj(xml) ;
        var result = data.result;
        if (!result) {  $j(cur_element).addClass('input_error').removeClass('check_ok') ;
                          var alert_text = $j(cur_element).attr('alert_text');
                          if (!alert_text) alert_text=data.alert_text;
                        var content_class=(data.content_class!=undefined)? data.content_class:'tooltip_alert' ;

                        // через жопу, но только так и работает
                        $j(cur_element).next('div.check_result').show().html(alert_text).addClass(content_class) ;

                        //$j(tooltip_alert).open({content: alert_text,addClass:content_class});
                         $j(cur_element).closest('form').find('input[type="submit"]').attr('disabled',true) ;
                               }
            else        { //if (tooltip_alert!=undefined && tooltip_alert.length) tooltip_alert.destroy() ;
                          $j(cur_element).next('div.check_result').hide() ;
                             $j(cur_element).closest('form').find('input[type="submit"]').attr('disabled',false) ;
                             $j(cur_element).removeClass('input_error').addClass('check_ok') ;
                               }
          }
      }

 }) ;


} ;


function enter_as_tab(e)
{ var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
  if(key == 13) {
       e.preventDefault();
       var inputs = $j(this).closest('form').find(':input:visible');
       inputs.eq( inputs.index(this)+ 1 ).focus();
   }
}

function send_form_by_enter(e)
{
    if(e.keyCode==13)$j(this).closest('form').find('input[type=submit]').click() ;
}



// функции для обработки XML ---------------------------------------------------------------------------------------------------------------------------------

// переносим данные из XML в массив
 function convert_XML_to_obj(XML)
 {  var arr= {} ; var param ;
   // переводим все аттрибуты кликнутого элемента в скрытые переменные
   var xml_doc=XML.childNodes[0] ;
   if (!xml_doc.childNodes.length) xml_doc=XML.childNodes[1] ;
   for(var j=0; j<xml_doc.childNodes.length; j++)
   { param=xml_doc.childNodes[j] ;
     var name=param.nodeName ;
     if (param.childNodes.length) arr[name]=param.childNodes[0].nodeValue ;
     else                         arr[name]="" ;
   }
   return(arr) ;
 }


function get_xml_tag_value(xml,tag_name)
{   var elem = xml.getElementsByTagName(tag_name);
    if (elem.length)
    {   if ($j.browser.msie) return(elem[0].text);
        else                 return(elem[0].textContent);
    }
    else return('');
}

// получаем атрибуеты XML TAG объекта
// тут в IE атрибуты отрабатывают нормально - в атрибутах только атрибуты, без свойтсв
function get_xml_tag_attr(xml,tag_name)
{   var attr = new Object();
    if (xml.getElementsByTagName(tag_name).length)
    { var attr_XML=xml.getElementsByTagName(tag_name).item(0).attributes;
      if (attr_XML.length) for(var j=0; j<attr_XML.length; j++) attr[attr_XML[j].nodeName]=attr_XML[j].nodeValue ;
    }
    return(attr);
}

// получаем атрибуеты XML OBJ объекта
function get_xml_obj_attr(obj)
{   var attr_XML=obj.attributes;
    var attr = new Object();
    for(var j=0; j<attr_XML.length; j++) attr[attr_XML[j].nodeName]=attr_XML[j].nodeValue ;
    return(attr);
}
