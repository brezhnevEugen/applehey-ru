<?
  // верхняя панель навигации
  function panel_list_items_top_navigat(&$system,&$options=array())
  { //damp_array($options) ;
    if ($options['no_any_panel']) return ;
//    if ((!$options['panel_select_pages'] or $options['panel_select_pages']=='bottom') and (!$options['panel_select_usl'] or $options['panel_select_usl']=='bottom')/* and !$options['panel_diapazon_page']*/) return ;
	?><div id=panel_navigat class="top"><?
	  if ($options['panel_select_pages'] and $options['panel_select_pages']!='bottom') 	panel_select_pages($system,$options);
    if ($options['panel_select_usl'] and $options['panel_select_usl']!='bottom') 		panel_select_usl($system,$options,1);
    if ($options['panel_view_mode']  and $options['panel_view_mode']!='bottom' ) 		panel_view_mode();
//	  if ($options['panel_diapazon_page'] and $options['panel_diapazon_page']!='bottom')panel_diapazon_page($system,$options) ;
	?><div class=clear></div></div><?
  }

  // нижняя панель навигации
  function panel_list_items_bottom_navigat(&$system,&$options=array())
  { //damp_array($options);
    if ($options['no_any_panel']) return ;
    if ((!$options['panel_select_pages'] or $options['panel_select_pages']=='top') and (!$options['panel_select_usl'] or $options['panel_select_usl']=='top')/* and !$options['panel_diapazon_page']*/) return ;
	?><div id=panel_navigat class=bottom><?
	  if ($options['panel_select_pages'] and $options['panel_select_pages']!='top') 	panel_select_pages($system,$options);
//	  if ($options['panel_select_usl'] and $options['panel_select_usl']!='top') 		panel_select_usl();
    if ($options['panel_view_mode']     and $options['panel_view_mode']!='top') 		panel_view_mode();
	  //if ($options['panel_diapazon_page'] and $options['panel_diapazon_page']!='top') 	panel_diapazon_page($system,$options) ;
	?><div class=clear></div></div><?
  }

  function panel_select_pages(&$system,$options=array()) // show_page_panel
  {  global $search_text ;
     //damp_array($system->pages_info) ;
     $text=array() ;
     $page_number_rasdel=$system->pages_info['options']['page_number_rasdel'] ;
     $cur_page=$options['current_page'] ;
     list($arr_pages,$to_prev,$to_next,$to_all)=$system->prepare_panel_select_pages($options) ;
     //damp_array($arr_pages) ; damp_array($to_prev) ; damp_array($to_next) ;

     ?><div class=panel_select_pages><?

     if (sizeof($to_prev))  if ($to_prev['href']) echo '<a href="'.$to_prev['href'].'"><img src="'.$to_prev['img_src'].'" alt="" border="0"></a>' ;
                            else                  echo '<img src="'.$to_prev['img_src'].'" alt="" border="0">' ;

     if (sizeof($arr_pages)) { foreach($arr_pages as $rec) $text[]=($rec['selected'])? "<span class=cur_page>".$rec['value'].'</span>':'<a href="'.$rec['href'].'" >'.$rec['value'].'</a>' ;
     						   echo implode(' ',$text) ;
     						 }
     if (sizeof($to_next))  if ($to_next['href']) echo '<a href="'.$to_next['href'].'"><img src="'.$to_next['img_src'].'" alt="" border="0"></a>' ;
                            else                  echo '<img src="'.$to_next['img_src'].'" alt="" border="0">' ;

     ?><a href="<?echo $to_all['href']?>" class="view_all">Посмотреть все товары</a><?

      ?></div><?
   }

   function panel_select_sort()
   {  ?><div class=panel_select_sort><div> сортировать по</div>
            <div class="item">Сначала дешевые</div>
            <div class="item">Сначала дорогие</div>
            <div class="item">По популярности</div>
            <div class="item">По новизне</div>
		</div>
	<?
   }
function panel_select_usl(&$system,$options=array(),$num=1)
   {  global $search_text ;
      //damp_array($system->pages_info['size']) ;
      //damp_array($system->pages_info['sort_type']) ;
      ?>
     <div class=panel_select_usl>
       <? $arr=$system->prepare_panel_page_size($options) ;
        if (sizeof($arr))
        { ?>
          <div class=panel_select_size>
            <div class="title">Выводить по:</div>
              <div class=as_select><?echo $_SESSION['goods_system']->pages_info['size'][$_SESSION['goods_system']->cur_list_size];?>
                <div class=list_items><? foreach ($arr as $rec)  {?><div class="item <? if ($rec['selected']) echo 'active'?>" value="<?echo $rec['value']?>"><?echo $rec['title']?></div><?}?></div>
              </div>
            <div class=clear></div>
          </div><?
        }
          
			$arr=$system->prepare_panel_sort_usl($options) ;  //damp_array($arr) ;
            if (sizeof($arr))
		      { ?><div class=panel_select_sort>Сортировать по: <select name=sort>
			        <? foreach ($arr as $rec)  {?><option <? if ($rec['selected']) echo 'selected'?> value="<?echo $rec['value']?>"><?echo $rec['title']?></option><?}?>
              </select></div><?
		      }
	     if ($search_text) {?><input name="search_text" type="hidden" value="<?echo $search_text?>"><?}?>
		</div>
	<?
   }

  function panel_view_mode()
  { ?>
      <div class=panel_view_mode>
        <div class="title">Вид каталога:</div>
        <div class="v2 mode0<?if (!$_SESSION['list_goods_icons_mode'] or $_SESSION['list_goods_icons_mode']==0) echo ' active'?>" cmd=set_list_mode ext=catalog mode=0></div>
        <div class="v2 mode1<?if ($_SESSION['list_goods_icons_mode']==1) echo ' active'?>" cmd=set_list_mode ext=catalog mode=1></div>
        <div class="v2 mode2<?if ($_SESSION['list_goods_icons_mode']==2) echo ' active'?>" cmd=set_list_mode ext=catalog mode=2></div>
        <div class=clear></div>
      </div>
      <script type="text/javascript">
         $j('div.panel_view_mode div').click(function()
          { var list_mode=0 ;
            if ($j(this).hasClass('mode0')) { $j(this).closest('div#block_main').find('div.list_goods_icons').removeClass('property').removeClass('list').addClass('icon') ; list_mode=0 ; }
            if ($j(this).hasClass('mode1')) { $j(this).closest('div#block_main').find('div.list_goods_icons').removeClass('list').removeClass('icon').addClass('property') ; list_mode=1 ; }
            if ($j(this).hasClass('mode2')) { $j(this).closest('div#block_main').find('div.list_goods_icons').removeClass('icon').removeClass('property').addClass('list') ; list_mode=3 ; }
            $j(this).parent().children('div').removeClass('active') ;
            $j(this).addClass('active') ;
          })
      </script><?
  }


?>