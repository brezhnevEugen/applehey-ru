<?
include(_DIR_TO_ENGINE.'/c_page_XML_AJAX.php') ;
class c_page_ajax extends c_page_XML_AJAX
{


   function set_cur_tab_panel()
   {  $panel_id=$_POST['target'] ;
      check_input_var($panel_id,'string') ;
      $_SESSION['tabs_panels_acive']=$panel_id ;
   }

   function save_div_status()
   {  check_input_var($_POST['target'],'safe_string') ;
      check_input_var($_POST['text'],'safe_string') ;
      check_input_var($_POST['status'],'safe_string') ;
      $_SESSION['div_status'][$_POST['target']]=array('status'=>$_POST['status'],'text'=>$_POST['text']) ;
   }

   function get_sortament_reffer()
   { $size=$_POST['size'] ;
     $color=$_POST['color'] ;
     $reffer=$_POST['reffer'] ;
     if ($color) $this->add_element('sort_color_name.HTML','Цвет: <strong>'.$color.'</strong>') ;
     if ($size)  $this->add_element('sort_size.HTML','Размер: <strong>'.$size.'</strong>') ;
     list($goods_id,$tkey)=explode('.',$reffer) ;
     ob_start() ;
     ?><script type="text/javascript"><?

     if ($_POST['set']=='size' and 0)
     {   ?>$j('div#panel_select_color div.item').removeClass('on');<?
         // получаем все цвета этого размера
         $arr=execSQL_line('select DISTINCT(color_name) from '._TM_SORTAMENT.' where parent='.addslashes($goods_id).' and size="'.addslashes($size).'" and enabled=1') ;
         if (sizeof($arr)) foreach($arr as $value) {?>$j('div#color_<?echo translit($value)?>').addClass('on');<?}
     }
     if ($_POST['set']=='color')
     {   ?>$j('div#panel_select_size div.item').removeClass('on');<?
         // получаем все размеры этого цвета
         $arr=execSQL_line('select DISTINCT(size) from '._TM_SORTAMENT.' where parent='.addslashes($goods_id).' and color_name="'.addslashes($color).'" and enabled=1') ;
         if (sizeof($arr))
         {  $text_arr=array() ;
            foreach($arr as $value) $text_arr[]='div#size_'.translit($value) ;
            ?>$j('<?echo implode(',',$text_arr)?>').addClass('on');<?
         }
     }

     $_usl[]='parent='.addslashes($goods_id) ;
     if ($color) $_usl[]='color_name="'.addslashes($color).'"' ;
     if ($size) $_usl[]='size="'.addslashes($size).'"' ;
     $usl=implode(' and ',$_usl) ;
     $rec_goods=execSQL_van('select * from '._TM_GOODS.' where pkey='.$goods_id.' and enabled=1') ;
     $rec_sort=execSQL('select * from '._TM_SORTAMENT.' where '.$usl.' and enabled=1',array('no_indx'=>1)) ;
     if (sizeof($rec_sort)==1)
     { //$this->add_element('sort_info.HTML',$rec_sort['obj_name']) ;
       $this->add_element('sort_reffer',$rec_sort[0]['_reffer']) ;
       $this->add_element('sort_id',$rec_sort[0]['pkey']) ;
       if ($rec_sort[0]['color_name']) $this->add_element('sort_color_name.HTML','Цвет: <strong>'.$rec_sort[0]['color_name'].'</strong>') ;
       if ($rec_sort[0]['size']) $this->add_element('sort_size.HTML','Размер: <strong>'.$rec_sort[0]['size'].'</strong>') ;
       if ($rec_sort[0]['price'])
        { $rec_goods['price']=$rec_sort[0]['price'] ;
          global $goods_system ;
          $goods_system->prepare_public_info($rec_goods) ;
          $this->add_element('price_goods.HTML',$rec_goods['__price_5']) ;
          //$this->add_element('price_goods.HTML',_TM_GOODS) ;
        }
       //$this->add_element('button_to_cart','',array('sort_id'=>$rec_sort['pkey'])) ;

       ?>$j("#panel_buy_buttons button").attr("sortament_reffer","<? echo $rec_sort[0]['_reffer']?>").attr('disabled',false);<?
     }
     else
     {
       ?>$j("#panel_buy_buttons button").attr("sortament_reffer","").attr('disabled',true);<?
     }
     ?></script><?
     $text=ob_get_clean() ;
     $this->add_element('JSCODE',$text) ;

     if (!$_POST['no_get_photo_color'])
     { ob_start() ;
       $recs_images_to_color=execSQL('select * from '.$_SESSION['TM_goods_image'].' where parent='.addslashes($goods_id).' and color_name="'.addslashes($color).'"') ;
       print_template($recs_images_to_color,'highslide/list_images_highslide',array('id'=>'goods_img_small','clone_small'=>'small','clone_big'=>'source')) ;
       $text=ob_get_clean() ;
       $this->add_element('panel_images.HTML',$text) ;
     }

   }

   function clear_sortament_reffer()
   { ob_start() ;
     ?><script type="text/javascript">
          $j('div#panel_select_color div.item').addClass('on');
          $j('div#panel_select_size div.item').addClass('on');
          $j('div#panel_select_size_ext div.item').addClass('on');
          $j("#panel_buy_buttons button").attr("sortament_reffer","").attr('disabled',true);
       </script><?
     $text=ob_get_clean() ;
     $this->add_element('JSCODE',$text) ;
     $this->add_element('sort_info.HTML','') ;
     $this->add_element('sort_reffer','') ;
     $this->add_element('sort_id','') ;
     $this->add_element('sort_color_name.HTML','') ;
     $this->add_element('sort_size.HTML','') ;


   }

}