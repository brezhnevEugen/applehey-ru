function include(scriptUrl) {
    document.write('<script src="' + scriptUrl + '"></script>');
}

function isIE() {
    var myNav = navigator.userAgent.toLowerCase();
    return (myNav.indexOf('msie') != -1) ? parseInt(myNav.split('msie')[1]) : false;
};

/* cookie.JS
 ========================================================*/
// include('js/jquery.cookie.js');

/* Easing library
 ========================================================*/
include('js/jquery.easing.1.3.js');

/* PointerEvents
 ========================================================*/
;
(function ($j) {
    if(isIE() && isIE() < 11){ 
        include('js/pointer-events.js');
        $j('html').addClass('lt-ie11');
        $j(document).ready(function(){
            PointerEventsPolyfill.initialize({});
        });
    }
})(jQuery); 

/* Stick up menus  менюшка при прокрутке сверху / работает
 ========================================================*/
;
(function ($j) {
    var o = $j('html');
    if (o.hasClass('desktop')) {
        include('js/tmstickup.js');

        $j(document).ready(function () {
            $j('#stuck_container').TMStickUp({})
        });
    }
})(jQuery);

/* ToTop         ссылка с прокруткой наверх / работает
 ========================================================*/
;
(function ($j) {
    var o = $j('html');
    if (o.hasClass('desktop')) {
        include('js/jquery.ui.totop.js');

        $j(document).ready(function () {
            $j().UItoTop({
                easingType: 'easeOutQuart',
                containerClass: 'toTop fa fa-angle-up'
            });
        });
    }
})(jQuery);

/* EqualHeights              ? что за хрень, где колонки
 ========================================================*/
;
(function ($j) {
    var o = $j('[data-equal-group]');
    if (o.length > 0) {
        include('js/jquery.equalheights.js');
    }
})(jQuery);

/* SMOOTH SCROLLIG          делает прокрутку страниц плавной
 ========================================================*/
;
(function ($j) {
    var o = $j('html');
    if (o.hasClass('desktop')) {
        include('js/jquery.mousewheel.min.js');
        include('js/jquery.simplr.smoothscroll.min.js');

        $j(document).ready(function () {
            $j.srSmoothscroll({
                step: 150,
                speed: 800
            });
        });
    }
})(jQuery);

/* Copyright Year               автообновляет год в подвале
 ========================================================*/
;
(function ($j) {
    var currentYear = (new Date).getFullYear();
    $j(document).ready(function () {
        $j("#copyright-year").text((new Date).getFullYear());
    });
})(jQuery);


/* Superfish menus            выпадающее меню / работает
 ========================================================*/
;
(function ($j) {
    include('js/superfish.js');    
})(jQuery);

/* Navbar                     менюшка для мобил / работает
 ========================================================*/
;
(function ($j) {
    include('js/jquery.rd-navbar.js');
})(jQuery);


/* Google Map
 ========================================================*/
// ;
// (function ($j) {
//     var o = document.getElementById("google-map");
//     if (o) {
//         include('//maps.google.com/maps/api/js?sensor=false');
//         include('js/jquery.rd-google-map.js');
//
//         $j(document).ready(function () {
//             var o = $j('#google-map');
//             if (o.length > 0) {
//                 o.googleMap({
//                     styles: [{"featureType":"water","elementType":"all","stylers":[{"hue":"#76aee3"},{"saturation":38},{"lightness":-11},{"visibility":"on"}]},{"featureType":"road.highway","elementType":"all","stylers":[{"hue":"#8dc749"},{"saturation":-47},{"lightness":-17},{"visibility":"on"}]},{"featureType":"poi.park","elementType":"all","stylers":[{"hue":"#c6e3a4"},{"saturation":17},{"lightness":-2},{"visibility":"on"}]},{"featureType":"road.arterial","elementType":"all","stylers":[{"hue":"#cccccc"},{"saturation":-100},{"lightness":13},{"visibility":"on"}]},{"featureType":"administrative.land_parcel","elementType":"all","stylers":[{"hue":"#5f5855"},{"saturation":6},{"lightness":-31},{"visibility":"on"}]},{"featureType":"road.local","elementType":"all","stylers":[{"hue":"#ffffff"},{"saturation":-100},{"lightness":100},{"visibility":"simplified"}]},{"featureType":"water","elementType":"all","stylers":[]}]
//                 });
//             }
//         });
//     }
// })
// (jQuery);

/* WOW  анимация при прокрутке и появлении элементов на странице / работает
 ========================================================*/
;
(function ($j) {
    var o = $j('html');

    if ((navigator.userAgent.toLowerCase().indexOf('msie') == -1 ) || (isIE() && isIE() > 9)) {
        if (o.hasClass('desktop')) {
            include('js/wow.js');

            $j(document).ready(function () {
                new WOW().init();
            });
        }
    }
})(jQuery);

/* Contact Form
 ========================================================*/
// ;
// (function ($j) {
//     var o = $j('#contact-form');
//     if (o.length > 0) {
//         include('js/modal.js');
//         include('js/TMForm.js');
//
//         if($j('#contact-form .recaptcha').length > 0){
//         	include('//www.google.com/recaptcha/api/js/recaptcha_ajax.js');
//         }
//     }
// })(jQuery);

/* Orientation tablet fix
 ========================================================*/
$j(function () {
    // IPad/IPhone
    var viewportmeta = document.querySelector && document.querySelector('meta[name="viewport"]'),
        ua = navigator.userAgent,

        gestureStart = function () {
            viewportmeta.content = "width=device-width, minimum-scale=0.25, maximum-scale=1.6, initial-scale=1.0";
        },

        scaleFix = function () {
            if (viewportmeta && /iPhone|iPad/.test(ua) && !/Opera Mini/.test(ua)) {
                viewportmeta.content = "width=device-width, minimum-scale=1.0, maximum-scale=1.0";
                document.addEventListener("gesturestart", gestureStart, false);
            }
        };

    scaleFix();
    // Menu Android
    if (window.orientation != undefined) {
        var regM = /ipod|ipad|iphone/gi,
            result = ua.match(regM);
        if (!result) {
            $j('.sf-menus li').each(function () {
                if ($j(">ul", this)[0]) {
                    $j(">a", this).toggle(
                        function () {
                            return false;
                        },
                        function () {
                            window.location.href = $j(this).attr("href");
                        }
                    );
                }
            })
        }
    }
});
var ua = navigator.userAgent.toLocaleLowerCase(),
    regV = /ipod|ipad|iphone/gi,
    result = ua.match(regV),
    userScale = "";
if (!result) {
    userScale = ",user-scalable=0"
}
document.write('<meta name="viewport" content="width=device-width,initial-scale=1.0' + userScale + '">');

/* Camera         главный слайдер / параметры
========================================================*/
;(function ($j) {
var o = $j('#camera');
    if (o.length > 0) {
        // выдает ошибку/ временно отключено
        // if (!(isIE() && (isIE() > 9))) {
        //     include('/js/jquery.mobile-1.4.5/jquery.mobile-1.4.5.js');
        // }

 

        $j(document).ready(function () {
            o.camera({
                 autoAdvance: true,
                 height: '51.85365853658537%',
                 minHeight: '300px',
                 pagination: true,
                 thumbnails: false,
                 playPause: false,
                 hover: false,
                 loader: 'none',
                 navigation: false,
                 navigationHover: false,
                 mobileNavHover: false,
                 fx: 'simpleFade'
             })
        });
    }
})(jQuery);

/* Owl Carousel       горизонтальный слайдер для отзывов
========================================================*/
;(function ($j) {
    var o = $j('.owl-carousel');
    if (o.length > 0) {
        include('/js/owl.carousel.min.js');
        $j(document).ready(function () {
            o.owlCarousel({
                margin: 30,
                smartSpeed: 450,
                loop: true,
                dots: true,
                dotsEach: 1,
                nav: false,
                navClass: ['owl-prev fa fa-angle-left', 'owl-next fa fa-angle-right'],
                responsive: {
                    0: { items: 1 },
                    768: { items: 1},
                    980: { items: 1}
                }
            });
        });
    }
})(jQuery);

/* Mailform
=============================================*/
// ;(function ($j) {
//     include('/js/mailform/jquery.form.min.js');
//     include('/js/mailform/jquery.rd-mailform.min.c.js');
// })(jQuery);